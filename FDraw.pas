unit FDraw;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DirectDraw, DDUtil;

const
  SCREEN_WIDTH = 640;
  SCREEN_HEIGHT = 480;
  SCREEN_BPP = 32;
  SPRITE_DIAMETER = 48;
  HELPTEXT : PChar = 'Press Escape to quit.';


type
  TFormDraw = class(TForm)
    procedure FormShow(Sender: TObject);
  private
    FLastTick : DWORD;
    FActive : Boolean;
    FDisplay : TDisplay;
    FLogoSurface : TSurface;
    FTextSurface : TSurface;
  public
    function InitDirectDraw : HRESULT;

  end;

implementation

{$R *.dfm}

{ TForm1 }

function TFormDraw.InitDirectDraw: HRESULT;
var
  hr : HRESULT;
  iSprite : integer;
  FDDPal : IDIRECTDRAWPALETTE;
begin
//  ZeroMemory( @FSprite, SizeOf( TSpriteRecord ) * NUM_SPRITES );

  FDDPal := nil;

  // Initialize all the surfaces we need
  FDisplay := TDisplay.Create;

  hr := FDisplay.CreateWindowedDisplay( Handle, SCREEN_WIDTH, SCREEN_HEIGHT );
  if ( hr <> DD_OK ) then
  begin
    showmessage('CreateWindowedDisplay');
    result := hr;
    exit;
  end;

  // Create and set the palette when in palettized color
  hr := FDisplay.CreatePaletteFromBitmap( FDDPal, 'sp.bmp'  {MakeIntResource( IDB_DIRECTX ) } );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreatePaletteFromBitmap', hr );
    showmessage('CreatePaletteFromBitmap' );
    result := hr;
    exit;
  end;

  FDisplay.SetPalette( FDDPal );



  FDDPal := nil;

  (*
  // Create a surface, and draw a bitmap resource on it.
  hr := FDisplay.CreateSurfaceFromBitmap( FLogoSurface, PAnsiChar('sp.bmp' ) {MAKEINTRESOURCE( IDB_DIRECTX )}, SPRITE_DIAMETER, SPRITE_DIAMETER );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurfaceFromBitmap', hr );
    showmessage('CreateSurfaceFromBitmap' );
    result := hr;
    exit;
  end;
  *)

(*
  // Create a surface, and draw text to it.
  hr := FDisplay.CreateSurfaceFromText( FTextSurface, 0, HELPTEXT, RGB( 0, 0, 0 ), RGB( 255, 255, 0 ) );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'CreateSurfaceFromText', hr );
    ErrorOut( hr, 'CreateSurfaceFromText' );
    result := hr;
    exit;
  end;

  // Set the color key for the logo sprite to black
  hr := FLogoSurface.SetColorKey( 0 );
  if ( hr <> DD_OK ) then
  begin
    //DXTRACE_ERR( 'SetColorKey', hr )
    ErrorOut( hr, 'SetColorKey' );
    result := hr;
    exit;
  end;

  // Init all the sprites.  All of these sprites look the same,
  // using the FDDSLogo surface.
  for iSprite := 0 to NUM_SPRITES - 1 do
  begin
    // Set the sprite's position and velocity
    FSprite[ iSprite ].fPosX := random( SCREEN_WIDTH );
    FSprite[ iSprite ].fPosY := random( SCREEN_HEIGHT );

    FSprite[ iSprite ].fVelX := 500.0 * random / RAND_MAX - 250.0;
    FSprite[ iSprite ].fVelY := 500.0 * random / RAND_MAX - 250.0;
  end;
  *)

  FLastTick := GetTickCount;

  result := DD_OK;
end;

procedure TFormDraw.FormShow(Sender: TObject);
var
  hr : HResult;
begin
  // Initialize DirectDraw
  hr := InitDirectDraw;
  if hr <> DD_OK then
  begin
    showmessage('InitDirectDraw' );
    close;
  end;
end;

end.

