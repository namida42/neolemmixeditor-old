object FormVgaSpecCreator: TFormVgaSpecCreator
  Left = 137
  Top = 215
  BorderStyle = bsDialog
  Caption = 'Convert bitmap to DOS extended graphic file'
  ClientHeight = 169
  ClientWidth = 476
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Filename: TLabel
    Left = 8
    Top = 91
    Width = 32
    Height = 13
    Caption = '&Bitmap'
    FocusControl = EditFileName
  end
  object BtnSelectFile: TButton
    Left = 427
    Top = 88
    Width = 22
    Height = 22
    Caption = '...'
    TabOrder = 2
    OnClick = BtnSelectFileClick
  end
  object Img: TImgView32
    Left = 0
    Top = 0
    Width = 480
    Height = 80
    Bitmap.ResamplerClassName = 'TNearestResampler'
    Color = clGray
    ParentColor = False
    Scale = 0.500000000000000000
    ScrollBars.ShowHandleGrip = True
    ScrollBars.Style = rbsDefault
    ScrollBars.Visibility = svHidden
    OverSize = 0
    TabOrder = 0
  end
  object EditFileName: TEdit
    Left = 56
    Top = 88
    Width = 369
    Height = 21
    TabOrder = 1
    OnExit = EditFileName_Exit
    OnKeyDown = EditFileName_KeyDown
  end
  object BtnCreate: TButton
    Left = 232
    Top = 128
    Width = 163
    Height = 25
    Caption = 'Create extended grapic file'
    TabOrder = 3
    OnClick = BtnCreateClick
  end
  object BtnClose: TButton
    Left = 400
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Close'
    ModalResult = 2
    TabOrder = 4
    OnClick = BtnCloseClick
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 161
    Width = 476
    Height = 8
    Align = alBottom
    TabOrder = 5
  end
end
