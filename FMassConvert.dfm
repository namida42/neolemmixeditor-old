object Form_MassConvert: TForm_MassConvert
  Left = 179
  Top = 171
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Level Mass Conversion'
  ClientHeight = 625
  ClientWidth = 409
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 562
    Width = 80
    Height = 13
    Caption = 'Output Directory:'
  end
  object lbDebugState: TLabel
    Left = 371
    Top = 600
    Width = 3
    Height = 13
  end
  object btnConvert: TButton
    Left = 112
    Top = 584
    Width = 89
    Height = 33
    Caption = 'Convert'
    TabOrder = 0
    OnClick = btnConvertClick
  end
  object btnClose: TButton
    Left = 208
    Top = 584
    Width = 89
    Height = 33
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 1
  end
  object lvConvert: TListView
    Left = 8
    Top = 8
    Width = 393
    Height = 401
    Columns = <
      item
        Caption = 'File'
        Width = 200
      end
      item
        Caption = 'Source Format'
        Width = 100
      end
      item
        Caption = 'Conversion'
        Width = 89
      end>
    HideSelection = False
    MultiSelect = True
    TabOrder = 2
    ViewStyle = vsReport
  end
  object rgFilename: TRadioGroup
    Left = 8
    Top = 464
    Width = 169
    Height = 65
    Caption = 'Output Filename Scheme'
    ItemIndex = 0
    Items.Strings = (
      '[source filename]'
      '[source filename]_convert'
      '[level name]')
    TabOrder = 3
  end
  object btnAddFiles: TButton
    Left = 8
    Top = 424
    Width = 129
    Height = 33
    Caption = 'Add Files'
    TabOrder = 4
    OnClick = btnAddFilesClick
  end
  object btnRemoveFile: TButton
    Left = 144
    Top = 424
    Width = 121
    Height = 33
    Caption = 'Remove Files'
    TabOrder = 5
    OnClick = btnRemoveFileClick
  end
  object btnClearFiles: TButton
    Left = 272
    Top = 424
    Width = 129
    Height = 33
    Caption = 'Clear List'
    TabOrder = 6
    OnClick = btnClearFilesClick
    OnKeyDown = btnClearFilesKeyDown
  end
  object cbDeleteSource: TCheckBox
    Left = 8
    Top = 536
    Width = 209
    Height = 17
    Caption = 'Delete Source File'
    TabOrder = 7
  end
  object ebOutDir: TEdit
    Left = 96
    Top = 560
    Width = 273
    Height = 21
    TabOrder = 8
  end
  object btnSelectDir: TButton
    Left = 376
    Top = 558
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 9
    OnClick = btnSelectDirClick
  end
end
