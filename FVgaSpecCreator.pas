{$include lem_directives.inc}

unit FVgaSpecCreator;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtDlgs, StdCtrls, GR32_Image, LemDosBmp,
  ComCtrls;

type
  TFormVgaSpecCreator = class(TForm)
    BtnSelectFile: TButton;
    Img: TImgView32;
    EditFileName: TEdit;
    BtnCreate: TButton;
    BtnClose: TButton;
    Filename: TLabel;
    ProgressBar1: TProgressBar;
    procedure BtnSelectFileClick(Sender: TObject);
    procedure BtnCreateClick(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure EditFileName_KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EditFileName_Exit(Sender: TObject);
  private
    fCurrentFile: string;
    procedure DoCompressProgress(P, M: Integer);
  public
  end;

procedure ExecuteFormVgaSpecCreator;

implementation

{$R *.dfm}

procedure ExecuteFormVgaSpecCreator;
var
  F: TFormVgaSpecCreator;
begin
  F := TFormVgaSpecCreator.Create(nil);
  try
    F.ShowModal;
  finally
    F.Free;
  end;
end;

procedure TFormVgaSpecCreator.BtnSelectFileClick(Sender: TObject);
var
  Dlg: TOpenPictureDialog;
  Fn: string;
begin
  Dlg := TOpenPictureDialog.Create(nil);
  try
    if Dlg.Execute then
      Fn := Dlg.FileName
    else
      Exit;
  finally
    Dlg.Free;
  end;


  Img.Bitmap.LoadFromFile(Fn);
  fCurrentFile := Fn;
  EditFileName.Text := fCurrentFile;

end;

procedure TFormVgaSpecCreator.BtnCreateClick(Sender: TObject);
var
  Vga: TVgaSpecBitmap;
  Fn: string;
  Dlg: TSaveDialog;
begin
  Dlg := TSaveDialog.Create(nil);
  try
    if Dlg.Execute then
      Fn := Dlg.FileName
    else
      Exit;

  finally
    Dlg.Free;
  end;

  Update;

  Vga := TVgaSpecBitmap.Create;
  try
    Vga.OnCompressProgress := DoCompressProgress;
    Vga.SaveToFile(Img.Bitmap, Fn);
  finally
    Vga.Free;
  end;
end;

procedure TFormVgaSpecCreator.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFormVgaSpecCreator.DoCompressProgress(P, M: Integer);
begin
  ProgressBar1.Max := M;
  ProgressBar1.Position := P;
end;

procedure TFormVgaSpecCreator.EditFileName_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    if CompareText(fCurrentFile, EditFileName.Text) <> 0 then
    begin
      fCurrentFile := EditFileName.Text;
      Img.Bitmap.LoadFromFile(fCurrentFile);
    end;
end;

procedure TFormVgaSpecCreator.EditFileName_Exit(Sender: TObject);
begin
  if CompareText(fCurrentFile, EditFileName.Text) <> 0 then
  begin
    fCurrentFile := EditFileName.Text;
      Img.Bitmap.LoadFromFile(fCurrentFile);
  end;
end;

end.
//lemstrings
