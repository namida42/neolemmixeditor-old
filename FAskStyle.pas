{$include lem_directives.inc}

unit FAskStyle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LemStyles, LemDosStyles, LemLemminiStyles,
  LemCore, StdCtrls;//, ComCtrls;

type
  TFormAskStyle = class(TForm)
    ComboLemmingStyle: TComboBox;
    BtnOK: TButton;
    BtnCancel: TButton;
    procedure Form_Create(Sender: TObject);
  private
    procedure InitStyle(aStyle: TBaseLemmingStyle);
  public
  end;

//function ExecFormAskStyle(var aStyle: TBaseLemmingStyle; AutoLemmini: Boolean = false): Word; // init param!!!!

implementation

{$R *.dfm}

(*function ExecFormAskStyle(var aStyle: TBaseLemmingStyle; AutoLemmini: Boolean = false): Word;
begin
  aStyle := StyleMgr.Styles[0];
  Result := mrOK;
end;*)

{ TFormAskStyle }

procedure TFormAskStyle.Form_Create(Sender: TObject);
var
  i, j: Integer;
  S: TBaseLemmingStyle;
  G: TBaseGraphicSet;
//  It: TTreeNode;
begin
  ComboLemmingStyle.Items.Clear;
  with StyleMgr do
    for i := 0 to StyleList.Count - 1 do
    begin
      S := StyleList[i];
      ComboLemmingStyle.Items.AddObject(S.StyleDescription, S);
    end;
  ComboLemmingStyle.ItemIndex := 0;




{  with StyleMgr do
    for i := 0 to StyleList.Count - 1 do
    begin
      S := StyleList[i];
      It := Tree.Items.AddChild(nil, S.StyleDescription);
      for j := 0 to S.GraphicSetList.Count - 1 do
      begin
        G := S.GraphicSetList[j];
        Tree.Items.AddChild(It, G.GraphicSetName);
      end;

    end;
 }

end;

procedure TFormAskStyle.InitStyle(aStyle: TBaseLemmingStyle);
var
  i: Integer;
begin
  with ComboLemmingStyle do
    for i := 0 to Items.Count - 1 do
      if Items.Objects[i] = aStyle then
      begin
        ItemIndex := i;
        Exit;
      end;

end;

end.

