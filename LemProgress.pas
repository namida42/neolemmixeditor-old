{$include lem_directives.inc}

unit LemProgress;

interface

uses
forms,  UEvents;

type
  TCustomProgressManager = class(TMessageManager)
  private
    fUserCancel: Boolean;
    fState: string;
    fProgressMsg: Cardinal;
    fPosition: Integer;
    fMaximum: Integer;
    fDetail: string;

    fPrevUserCancel: Boolean;
    fPrevState: string;
    fPrevProgressMsg: Cardinal;
    fPrevPosition: Integer;
    fPrevMaximum: Integer;
    fPrevDetail: string;


    procedure SetState(const Value: string);
    procedure SetUserCancel(const Value: Boolean);
  protected
  public
    constructor Create(aMaster: TObject; aProgressMsg: Cardinal);
    destructor Destroy; override;
//    procedure Changed;
    procedure Clear;
    procedure Progress;

    property State: string read fState write fState;
    property UserCancel: Boolean read fUserCancel write fUserCancel;
    property Maximum: Integer read fMaximum write fMaximum;
    property Position: Integer read fPosition write fPosition;
    property Detail: string read fDetail write fDetail;

    property PrevState: string read fPrevState;
    property PrevUserCancel: Boolean read fPrevUserCancel;
    property PrevMaximum: Integer read fPrevMaximum;
    property PrevPosition: Integer read fPrevPosition;
    property PrevDetail: string read fPrevDetail;

    //    prop//  cBusy: Boolean;
    //cPosition: Integer; // done
//    cMaximum: Integer; // todo
  //  cDetail: string;
  end;

  TLMProgress = packed record
    Msg       : Cardinal;
    Progress  : TCustomProgressManager;
    NotUsed   : Integer;
    Result    : Longint;
  end;

implementation

{ TProgress }

procedure TCustomProgressManager.Clear;
begin
    fUserCancel:=False;
    fState:='';;
//    fProgressMsg:= Cardinal;
    fPosition:=0;;
    fMaximum:=0;
    fDetail:='';

    fPrevUserCancel:=False;
    fPRevState:='';;
//    fProgressMsg:= Cardinal;
    fPRevPosition:=0;;
    fPrevMaximum:=0;
    fPrevDetail:='';

end;

constructor TCustomProgressManager.Create(aMaster: TObject; aProgressMsg: Cardinal);
begin
  inherited Create(aMaster);
  fProgressMsg := aProgressMsg;
end;

destructor TCustomProgressManager.Destroy;
begin
  inherited Destroy;
end;

procedure TCustomProgressManager.Progress;
begin
//  Application.ProcessMessages;
  Perform(fProgressMsg, Longint(Self), 0);
    fPrevUserCancel:=fusercancel;
    fPRevState:=fstate;
    fPRevPosition:=fposition;
    fPrevMaximum:=fmaximum;
    fPrevDetail:=fdetail;
end;

procedure TCustomProgressManager.SetState(const Value: string);
begin
  fState := Value;
end;

procedure TCustomProgressManager.SetUserCancel(const Value: Boolean);
begin
  fUserCancel := Value;
end;

end.

