{$include lem_directives.inc}
unit FExtSelect;

{-------------------------------------------------------------------------------
  This unit contains a dialog for selecting a style/graphicset for a new level.
-------------------------------------------------------------------------------}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  LemTypes,
  LemCore,
  LemStrings,
  Kernel_Resource,
  UMasterControls, Buttons, ExtCtrls, ComCtrls, ActnList, USplit,
  VirtualTrees;

type

  TExtSelectForm = class(TForm)
    lbStyles: TListBox;
    btnCancel: TButton;
    btnOK: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure lbStylesClick(Sender: TObject);
    procedure lbStylesDblClick(Sender: TObject);
  private
    fGraphList: array of TBaseGraphicSet;
    SelectedGraph: TBaseGraphicSet;
    { Private declarations }
    function GetResult(var aStyle: TBaseLemmingStyle; var aGraph: TBaseGraphicSet): Boolean;
  public
    { Public declarations }
  end;

function ExecuteExtSelectForm(var aStyle: TBaseLemmingStyle; var aGraph: TBaseGraphicSet; const altTitle: Boolean = false): Boolean;

implementation

{$R *.dfm}


function ExecuteExtSelectForm(var aStyle: TBaseLemmingStyle; var aGraph: TBaseGraphicSet; const altTitle: Boolean = false): Boolean;
{-------------------------------------------------------------------------------
  Execute function for the form.
-------------------------------------------------------------------------------}
var
  F: TExtSelectForm;
begin
  aStyle := nil;
  aGraph := nil;

  F := TExtSelectForm.Create(nil);
  if altTitle then F.Caption := 'Select graphic set';
  try
    F.Position := poMainFormCenter;
    Result := F.ShowModal = mrOK;
    if Result then
      F.GetResult(aStyle, aGraph);
  finally
    F.Free;
  end;
end;

function TExtSelectForm.GetResult(var aStyle: TBaseLemmingStyle; var aGraph: TBaseGraphicSet): Boolean;
begin
  aStyle := StyleMgr.Styles[0];
  aGraph := SelectedGraph;
  Result := aGraph <> nil;
end;

procedure TExtSelectForm.FormCreate(Sender: TObject);
var
  i, i2: Integer;
begin
  lbStyles.Clear;
  with StyleMgr.Styles[0] do
    for i := 0 to GraphicSetList.Count-1 do
      if GraphicSetList[i].IsSpecial then
        lbStyles.Items.Add(GraphicSetList[i].GraphicSetName);
  SetLength(fGraphList, lbStyles.Items.Count);
  i2 := 0;
  with StyleMgr.Styles[0] do
    for i := 0 to GraphicSetList.Count-1 do
      if GraphicSetList[i].IsSpecial then
      begin
        fGraphList[i2] := GraphicSetList[i];
        Inc(i2);
      end;
  btnOk.Enabled := false;
end;

procedure TExtSelectForm.lbStylesClick(Sender: TObject);
var
  i: Integer;
begin
  btnOk.Enabled := lbStyles.ItemIndex <> -1;
  if lbStyles.ItemIndex = -1 then
    SelectedGraph := nil
  else
    SelectedGraph := fGraphList[lbStyles.ItemIndex];
end;

procedure TExtSelectForm.lbStylesDblClick(Sender: TObject);
begin
  if btnOk.Enabled then
    ModalResult := mrOk;
end;

end.


