object FormDosLevelPacks: TFormDosLevelPacks
  Left = 271
  Top = 174
  Width = 633
  Height = 459
  Caption = 'FormDosLevelPacks'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Form_Create
  OnDestroy = Form_Destroy
  PixelsPerInch = 96
  TextHeight = 13
  Position = poMainFormCenter
  object Tree: TVirtualStringTree
    Left = 8
    Top = 8
    Width = 385
    Height = 417
    ChangeDelay = 250
    Header.AutoSizeIndex = 0
    Header.Font.Charset = DEFAULT_CHARSET
    Header.Font.Color = clWindowText
    Header.Font.Height = -11
    Header.Font.Name = 'MS Sans Serif'
    Header.Font.Style = []
    Header.MainColumn = -1
    Header.Options = [hoColumnResize, hoDrag]
    TabOrder = 0
    OnChange = TreeChange
    OnGetText = TreeGetText
    Columns = <>
  end
end
