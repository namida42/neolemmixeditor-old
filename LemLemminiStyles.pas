{$include lem_directives.inc}

unit LemLemminiStyles;

interface

uses
  PngInterface,
  Dialogs,
  Windows, Classes, IniFiles, SysUtils,
  UMisc, GR32, GraphicEx, GifImage,
  LemMisc, LemDosFiles, LemFiles,
  LemTypes, LemCore;

const
  // lemmini interactive objects animation flags
  AFL_QUIET          = 0;
  AFL_TRIGGERED      = 2;
  AFL_CONTINUOUSLY   = 1;
  AFL_ONCE           = 3;

  // lemmini interactive object trigger effect
  TEL_NONE                      = 0;
  TEL_EXIT                      = 8;
  TEL_TRIGGEREDTRAP             = 6;
  TEL_DROWN                     = 5;
//  TEL_DESINTEGRATION            = ;
  TEL_ONEWAY_LEFT               = 3;
  TEL_ONEWAY_RIGHT              = 4;
  TEL_ENTRY                     = 32;

type
  TLemminiStyle = class(TBaseLemmingStyle)
  private
    fSuperLemmini: Boolean;
  protected
    (*
    procedure InternalPrepareLevelLoading; override;
//    procedure Unprepare;
    function InternalGetLevelPackCount: Integer; override;
    function InternalGetLevelPackName(aLevelPack: Integer): string; override;
    function InternalGetSectionCount(aLevelPack: Integer): Integer; override;
    function InternalGetSectionName(aLevelPack, aSection: Integer): string; override;
    function InternalGetLevelCount(aLevelPack, aSection: Integer): Integer; override;
    procedure InternalGetLevelInfo(aLevelPack, aSection, aIndex: Integer; out Info: TLevelInfoRec); override;
    procedure InternalLoadLevel(aLevelPack, aSection, aIndex: Integer; aLevel: TLevel); override;
    *)
  public
    class procedure GetStyleParams(var aParams: TDefaultsRec); override;
    class function GetLevelLoaderClass: TBaseLevelLoaderClass; override;
  published
    property SuperLemmini: Boolean read fSuperLemmini write fSuperLemmini;
  end;

  {-------------------------------------------------------------------------------
    Lemmini GraphicSet
  -------------------------------------------------------------------------------}
  TLemminiGraphicSet = class(TBaseGraphicSet)
  private
    procedure CombineLemminiMask(F: TColor32; var B: TColor32; M: TColor32);
  protected
    //fGraphicSetPath: string; // the directory where all bitmaps + inifile are
  public
    procedure DirectGetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32); override;
    procedure DirectGetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32); override;
    procedure DirectGetBackgroundBitmap(aBitmap: TBitmap32); override;
    procedure DirectReadMetaData; override;
    procedure DirectWriteMetaData(const aFileName: string = ''); override;
  published
    //property GraphicSetPath: string read fGraphicSetPath write fGraphicSetPath;
  end;

  TLemminiLevelLoader = class(TBaseLevelLoader)
  protected
    procedure InternalPrepare; override;
    procedure InternalLoadLevel(aInfo: TLevelInfo; aLevel: TLevel); override;
  end;

procedure LemminiGifToBitmap32(Gif: TGifImage; Bmp: TBitmap32; BackGroundIndex: Byte);
procedure LemminiPngToBitmap32(Png: TPngGraphic; Bmp: TBitmap32; BackGroundIndex: Byte);

implementation

uses
  UFiles, Graphics, UTools;



procedure LemminiPngToBitmap32(Png: TPngGraphic; Bmp: TBitmap32; BackGroundIndex: Byte);
var
  PalPtr: PByte;
  X, Y: Integer;
  PixPtr: PColor32;
  Alpha: Boolean;
begin
  {Bmp.Assign(Png);
  //PixPtr := Bmp.PixelPtr[0, 0];
  with Png do
  begin
    for y := 0 to Height - 1 do
    begin
      PalPtr := ScanLine[y];
      for x := 0 to Width - 1 do
      begin
        if PalPtr^ <> BackGroundIndex then
          Bmp.Pixel[x, y] := Bmp.Pixel[x, y] or $FF000000
        else
          Bmp.Pixel[x, y] := Bmp.Pixel[x, y] and not $FF000000;
        Inc(PalPtr); // inc addr of PalPtr with 1 (typed pointer increment Delphi)
        Inc(PixPtr); // inc addr of PixPtr with 4 (typed pointer increment Delphi)
      end;
    end;
  end; // with Png}

end;

procedure LemminiGifToBitmap32(Gif: TGifImage; Bmp: TBitmap32; BackGroundIndex: Byte);
{-------------------------------------------------------------------------------
  Convert the Lemmini Gif to our 32 bits bitmap.
  Maybe it is not entirely correct to assume that Palindex 0 = transparant,
  but for now it works.
-------------------------------------------------------------------------------}
  { TODO : use the gifimage class }

var
  PalPtr: PByte;
  X, Y: Integer;
  PixPtr: PColor32;
  TransCol: TColor;
  //GifExt: TGIFGraphicControlExtension;

begin
  {Bmp.Assign(Gif.Bitmap);
  PixPtr := Bmp.PixelPtr[0, 0];

  //GifExt := TGifGraphicControlExtension.Create(Gif.Images[0]);
  BackGroundIndex := Gif.Header.BackgroundColorIndex;
  TransCol := Gif.Images[0].GraphicControlExtension.TransparentColor;
  TransCol := ((TransCol and $FF) shl 16) + (TransCol and $FF00) + ((TransCol and $FF0000) shr 16);
  //TransCol := GifExt.Image.GlobalColorMap.Colors[BackgroundIndex];
  //GifExt.Free;

  with Gif do
  begin
    for y := 0 to Height - 1 do
    begin
      PalPtr := Bitmap.ScanLine[y];
      for x := 0 to Width - 1 do
      begin
        if PixPtr^ and $FFFFFF <> TransCol and $FFFFFF then
          PixPtr^ := PixPtr^ or $FF000000
        else
          PixPtr^ := 0; //PixPtr^ and not $FF000000;
        Inc(PalPtr); // inc addr of PalPtr with 1 (typed pointer increment Delphi)
        Inc(PixPtr); // inc addr of PixPtr with 4 (typed pointer increment Delphi)
      end;
    end;
  end; // with Gif}

end;

type
  TLemminiParser = class
  private
    Ini: TMemIniFile;
    FakeSection: string;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFile(const aFileName: string);
    procedure Unload;
    procedure GetArrayValues(aList: TStrings; const ArrayName: string);
    function GetValue(const Ident: string): string;
  published
  end;

{ TLemminiParser }

constructor TLemminiParser.Create;
begin
  inherited Create;
  FakeSection := 'Lemmix';
  Ini := TMemIniFile.Create('');
end;

destructor TLemminiParser.Destroy;
begin
  Ini.Free;
  inherited;
end;

procedure TLemminiParser.GetArrayValues(aList: TStrings; const ArrayName: string);
var
  i: Integer;
  V: string;
begin
  aList.Clear;
  i := 0;
  repeat
    V := Ini.ReadString(FakeSection, ArrayName + '_' + i2s(i), '@');
    if V = '@' then
      Break;
    aList.Add(V);
//    if KeepCount > 0 then

//    deb([v]);
    Inc(i);
  until False;
end;

function TLemminiParser.GetValue(const Ident: string): string;
begin
  Result := Ini.ReadString(FakeSection, Ident, '');
end;

procedure TLemminiParser.LoadFile(const aFileName: string);
var
  L: TStringList;
begin

  L := TStringList.Create;
  try
    L.LoadFromFile(aFileName);
    L.Insert(0, '[' + FakeSection +']');
    Ini.SetStrings(L);
  finally
    L.Free;
  end;
end;

procedure TLemminiParser.Unload;
begin
  Ini.Clear;
end;

{ TLemminiStyle }

class function TLemminiStyle.GetLevelLoaderClass: TBaseLevelLoaderClass;
begin
  Result := TLemminiLevelLoader;
end;

class procedure TLemminiStyle.GetStyleParams(var aParams: TDefaultsRec);
begin
  with aParams do
  begin
    Resolution   := 2.0;
    LevelWidth   := 3200;
    LevelHeight  := 320;
    ViewWidth    := 640;
    ViewHeight   := 320;
    MultiPacks   := True;
  end;
end;


{ TLemminiStyle }

procedure TLemminiGraphicSet.CombineLemminiMask(F: TColor32; var B: TColor32; M: TColor32);
begin
  //if (F <> 0) and (B = 0) then B := F;
//  if (F and COLORMASK <> 0) and (B and COLORMASK = 0) then B := F
  if (F and $FFFFFF) = 0 then B := $0;
end;

procedure TLemminiGraphicSet.DirectGetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  Lemmini bitmaps are collected by name...
-------------------------------------------------------------------------------}
var
  {Gif: TGifImage;//TLemminiGIFGraphic;
  Png: TPngGraphic;}
  Fn: string;
  TB: TBitmap32;
begin
  //Gif := TGifImage.Create;
  //Png := TPngGraphic.Create;
  try
    Fn := IncludeCommonStylePath(fGraphicSetIntName) + '\' + fGraphicSetIntName + 'o_' + i2s(aIndex);
    if FileExists(Fn + '.png') then
    begin
      {Png.LoadFromFile(Fn + '.png');
      LemminiPngToBitmap32(Png, aBitmap, 0);}
      TB := LoadPngFile(fn + '.png');
    end else begin
      {Gif.LoadFromFile(Fn + '.gif');
      LemminiGifToBitmap32(Gif, aBitmap, 0);}
      TB := LoadGifFile(fn + '.gif');
    end;
    aBitmap.Assign(TB);
  finally
    TB.Free;
    //Gif.Free;
    //Png.Free;
  end;
end;

procedure TLemminiGraphicSet.DirectGetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  Direct load a gif-picture and use own conversion to have control over
  transparancy.
-------------------------------------------------------------------------------}
var
  {Gif: TGifImage;//TLemminiGIFGraphic;
  Png: TPngGraphic;}
  Fn: string;
  TB: TBitmap32;
begin
  {Gif := TGifImage.Create;
  Png := TPngGraphic.Create;}
  try
    Fn := IncludeCommonStylePath(fGraphicSetIntName) + '\' + fGraphicSetIntName + '_' + i2s(aIndex);
    if FileExists(Fn + '.png') then
    begin
      TB := LoadPngFile(fn + '.png');
      {Png.LoadFromFile(Fn + '.png');
      LemminiPngToBitmap32(Png, aBitmap, 0);}
    end else begin
      TB := LoadGifFile(fn + '.gif');
      {Gif.LoadFromFile(Fn + '.gif');
      LemminiGifToBitmap32(Gif, aBitmap, 0);}
    end;
    aBitmap.Assign(TB);
  finally
    TB.Free;
    {Gif.Free;
    Png.Free;}
  end;
end;

procedure TLemminiGraphicSet.DirectGetBackgroundBitmap(aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  Direct load a gif-picture and use own conversion to have control over
  transparancy.
-------------------------------------------------------------------------------}
var
  {Gif: TGifImage;//TLemminiGIFGraphic;
  Png: TPngGraphic;}
  TempBitmap : TBitmap32;
  Fn: string;
begin
  {Gif := TGifImage.Create;
  Png := TPngGraphic.Create;
  TempBitmap := TBitmap32.Create;}
  try
    Fn := IncludeCommonStylePath('special\' + fGraphicSetIntName) + '\' + fGraphicSetIntName;
    if FileExists(Fn + '.png') then
    begin
      TempBitmap := LoadPngFile(fn + '.png');
      {Png.LoadFromFile(Fn + '.png');
      aBitmap.Assign(Png);}
      //LemminiPngToBitmap32(Png, aBitmap, 0);
    end else begin
      TempBitmap := LoadGifFile(fn + '.gif');
      {Gif.LoadFromFile(Fn + '.gif');
      aBitmap.Assign(Gif);}
      //LemminiGifToBitmap32(Gif, aBitmap, 0{Gif.BackGroundColorIndex});
    end;
    aBitmap.Assign(TempBitmap);
    {Gif.Free;
    Png.Free;
    Gif := TGifImage.Create;
    Png := TPngGraphic.Create;
    if FileExists(Fn + 'm.png') then
    begin
      Png.LoadFromFile(Fn + 'm.png');
      TempBitmap.Assign(Png);
    end else begin
      Gif.LoadFromFile(Fn + 'm.gif');
      TempBitmap.Assign(Gif);
    end;
    TempBitmap.DrawMode := dmCustom;
    TempBitmap.OnPixelCombine := CombineLemminiMask;
    TempBitmap.DrawTo(aBitmap);}
  finally
    {Gif.Free;
    Png.Free;}
    TempBitmap.Free;
  end;
end;

procedure TLemminiGraphicSet.DirectReadMetaData;
{-------------------------------------------------------------------------------
  Reading the metadata of lemmini is quite easy: 1) Read ini file 2) Read pictures
  The only thing is that a part of the metadata (width, height) is in the data
  itself. So we have to access all the gif-picures.
  To gain some speed we directly read the width en height from the gif-file:
  GIF: $06 Word = Width
       $08 Word = Height

  In case we need it for bitmaps:
  BMP: $12 Cardinal = Width
       $16 Cardinal = Height

-------------------------------------------------------------------------------}
var
//  StylePath, StyleName: string;
  FakeSection: string;
  L: TStringList;
  Ini: TMemIniFile;
  F: string;
  Ix, V: string;
  H: Integer;
  Tiles: Integer;
  i: Integer;
  MO: TMetaObject;
  MT: TMetaTerrain;
  Fil: TFileStream;
  Gif: TGIFImage;
  Png: TPNGGraphic;
  Fn: String;
type
  TWH = packed record
    w:word;
    h:word;
  end;

var
  WH: twh;
  GSPath: string;
begin

  if fIsSpecial then
    GSPath := IncludeTrailingBackslash(IncludeCommonStylePath('special\' + fGraphicSetIntName))
    else
    GSPath := IncludeTrailingBackslash(IncludeCommonStylePath(fGraphicSetIntName));

  if GSPath = '' then raise Exception.Create('DirectReadMetadata path error');
  if fGraphicSetName = '' then raise Exception.Create('DirectReadMetadata name error');



//  fGraphicSetPath := IncludeTrailingBackslash(ExpandFileName(fGraphicSetPath));



  //fPathToData := 'd:\lemmings downloads\lemmini\styles\';
  //fStyleDirName := 'special';
  F := GSPath + fGraphicSetIntName + '.ini'; //'d:\lemmings downloads\lemmini\styles\special\special.ini';
  FakeSection := 'FakeSection';
  // make safe for finally statement
  L := nil;
  Ini := nil;

  L := TStringList.Create;
  try
    L.LoadFromFile(F);
    L.Insert(0, '[' + FakeSection + ']'); // fake insertion to use meminifile
    Ini := TMemIniFile.Create(F);
    Ini.SetStrings(L);
  if IsSpecial then
  begin
    MO := MetaObjectCollection.Add;
    MO.Width := Ini.ReadInteger(FakeSection, 'positionX', 640);
    MO.Height := Ini.ReadInteger(FakeSection, 'positionY', 0);
    MT := MetaTerrainCollection.Add;
  end else begin
    Tiles := Ini.ReadInteger(FakeSection, 'tiles', MinInt); // read number of terrains

    // load metaobjects
    i := 0;
    repeat
      Ix := i2s(i);
      H := Ini.ReadInteger(FakeSection, 'frames_' + Ix, -1);
      if H < 0 then
        Break;
      //deb([ix]);

      {frames_0 = 1
      anim_0   = 0
      type_0   = 8
      sound_0  = 14 }

      MO := MetaObjectCollection.Add;

      H := Ini.ReadInteger(FakeSection, 'anim_' + Ix, -1);
      MO.AnimationFlags := H;

      H := Ini.ReadInteger(FakeSection, 'frames_' + Ix, -1);
      MO.EndAnimationFrameIndex := H - 1;

      { TODO : read triggerarea from mask-gifs }
      // direct read width/height from gif

      fn := GSPath + fGraphicSetIntName + 'o_' + i2s(i);
      if FileExists(fn + '.png') then
      begin
        Png := TPngGraphic.Create;
        try
          Png.LoadFromFile(fn + '.png');
          MO.Width := Png.Width;
          MO.Height := Png.Height div (mo.EndAnimationFrameIndex + 1);
        finally
          Png.Free;
        end;
        //MessageDlg('mom!',mtCustom, [mbOK], 0);
      end else begin
        if FileExists(fn + '.gif') then
        begin
          Gif := TGifImage.Create;
          try
            Gif.LoadFromFile(fn + '.gif');
            MO.Width := Gif.Width;
            MO.Height := Gif.Height div (mo.EndAnimationFrameIndex + 1);
          finally
            Gif.Free;
          end;
        end;
      end;



      Inc(i);
    until False;

    // load metaterrains
    for i := 0 to Tiles - 1 do
    begin
      MT := MetaTerrainCollection.Add;
      // direct read width and height
      fn := GSPath + fGraphicSetIntName + '_' + i2s(i);
      if FileExists(fn + '.png') then
      begin
        Png := TPngGraphic.Create;
        try
          Png.LoadFromFile(fn + '.png');
          MT.Width := Png.Width;
          MT.Height := Png.Height;
        finally
          Png.Free;
        end;
      end else begin
        if FileExists(fn + '.gif') then
        begin
          Gif := TGifImage.Create;
          try
            Gif.LoadFromFile(fn + '.gif');
            MT.Width := Gif.Width;
            MT.Height := Gif.Height;
          finally
            Gif.Free;
          end;
        end;
      end;
    end;
  end;
  finally
    Ini.Free;
    L.Free;
  end;

end;


procedure TLemminiGraphicSet.DirectWriteMetaData(const aFileName: string = '');
var
  Strings: TStringList;
  i: Integer;
  si: string;
  MT: TMetaTerrain;
  MO: TMetaObject;
  H: Integer;

    procedure WriCom(const aComment: string);
    begin
      Strings.Add('# ' + aComment);
    end;

    procedure Wri;
    begin
      Strings.Add('');
    end;

    procedure WriVal(const aName, aValue: string);
    begin
      Strings.Add(aName + ' = ' + aValue);
    end;

begin

  Strings := TStringList.Create;

  WriCom('animation types');
  WriCom('0 - don''t animate');
  WriCom('1 - animate continously');
  WriCom('2 - trap - animate on trigger - else show first pic');
  WriCom('3 - entry animation: animate once at level start');
  Wri;

  WriCom('object types');
  WriCom('0  - passive');
  WriCom('3  - no digging to the left');
  WriCom('4  - no digging to the right');
  WriCom('5  - trap which makes lemmings drown (water/quick sand/mud)');
  WriCom('6  - trap which replaces lemming with death animation');
  WriCom('7  - trap which triggers lemming death animation');
  WriCom('8  - exit');
  WriCom('32 - entry');
  Wri;


  WriVal('bgColor', '0x000033');
  WriVal('debrisColor', '0xe0d0b0');
  WriVal('particleColor', '0x4040e0,0x00b000,0xf0d0d0,0xf0f000,0xf02020,0x808080,0xe0d0b0,0xe0d0b0,0x207000,0xA09070,0x635438,0x402f20,0x70d010,0xa04000,0x602000,0x766666');
  Wri;

  try
    with MetaTerrainCollection.HackedList do
    begin
      WriVal('tiles', i2s(Count));
      Wri;
    end;

    with MetaObjectCollection.HackedList do
    begin
      for i := 0 to Count - 1 do
      begin
        MO := List^[i];
        si := i2s(i);
        {
        frames_0 = 2
        anim_0   = 1
        type_0   = 8
        sound_0  = 14
        }
        WriVal('frames_' + si, i2s(MO.EndAnimationFrameIndex));

        H := 0;

        case MO.AnimationFlags of
          AFX_QUIET          : H := AFX_QUIET          ;
          AFX_TRIGGERED      : H := AFX_TRIGGERED      ;
          AFX_CONTINUOUSLY   : H := AFX_CONTINUOUSLY   ;
          AFX_ONCE           : H := AFX_ONCE           ;
        end;

        WriVal('anim_' + si, i2s(H));

        H := 0;
        case MO.TriggerEffect of
          TEX_NONE           : H := TEL_NONE           ;
          TEX_EXIT           : H := TEL_EXIT           ;
          TEX_TRIGGEREDTRAP  : H := TEL_TRIGGEREDTRAP  ;
          TEX_DROWN          : H := TEL_DROWN          ;
//          TEX_DESINTEGRATION : H := TEL_DESINTEGRATION ;
          TEX_ONEWAY_LEFT    : H := TEL_ONEWAY_LEFT    ;
          TEX_ONEWAY_RIGHT   : H := TEL_ONEWAY_RIGHT   ;
//          TEX_STEEL          : H := TEL_STEEL          ;
        end;

        if MO.AnimationFlags = AFX_ONCE then
          H := TEL_ENTRY; // wrong

        WriVal('type_' + si, i2s(H));
        //Wri('sound_' + si, '0');
        Wri;
      end;
    end;
    Strings.SaveToFile(aFileName);

  finally
    Strings.Free;
  end;


end;

{ TLemminiLevelLoader }

procedure TLemminiLevelLoader.InternalLoadLevel(aInfo: TLevelInfo; aLevel: TLevel);
var
//  Fn: string;
  //Ix: Integer;
  F: TFileStream;
  Sty: TLemminiStyle;
begin
  Sty := TLemminiStyle(Owner);

  //with LevelPacks[aLevelPack].PackSections[aSection].LevelInfos[aIndex] do
  begin
    //Fn := OwnerFile;
//    Ix := OwnerFileIndex;
  end;
  F := TFileStream.Create(aInfo.OwnerFile, fmOpenRead);
  aLevel.BeginUpdate;
  try
  alevel.style := nil;
  aLevel.Style := Sty;
  aLevel.LoadFromLemminiStream(F);
  finally
  aLevel.EndUpdate;
  end;
  F.Free;
end;

procedure TLemminiLevelLoader.InternalPrepare;
{-------------------------------------------------------------------------------
  bit of a mess in alpha.
  nested reading of
  levelpack/section/levelinfo
-------------------------------------------------------------------------------}

var
  Sty: TLemminiStyle;
  LevelPackDirs: TStringList;
  LevelPackDirMask: string;
  SectionNames: TStringList;
  LevelFileNames: TStringList;
  LevelPackIni: string;
  S, Section: string;
  LevelFileName: string;
  Parser, LevelParser: TLemminiParser;
  iPack, iSection, iLevel: Integer;


  LP: TLevelPack;
  LS: TPackSection;
  LI: TLevelInfo;



begin
  Sty := TLemminiStyle(Owner);
  // read levelpackdirs
  LevelPackDirMask := Sty.CommonPath + 'levels\*.*';
  Parser := TLemminiParser.Create;
  LevelParser := TLemminiParser.Create;
  LevelPackDirs := TStringList.Create;
  SectionNames := TStringList.Create;
  LevelFileNames := TStringList.Create;
  try
    CreateFileList(LevelPackDirs, LevelPackDirMask, faDirectory, True);

    // read levelpacks
    for iPack := 0 to LevelPackDirs.Count - 1 do
    begin
      LevelPackIni := LevelPackDirs[iPack] + '\' + 'levelpack.ini';
      //deb(['pack', LevelPackIni]);
      Parser.LoadFile(LevelPackIni);
      S := Parser.GetValue('name');
      // add levelpack
      LP := LevelPacks.Add;
      LP.SourceFileName := LevelPackIni;
      LP.LevelPackName := S;
      // get sections
      // level_0 = Fun
      Parser.GetArrayValues(SectionNames, 'level');
      for iSection := 0 to SectionNames.Count - 1 do
      begin
        Section := SectionNames[iSection];
        //deb(['section', Section]);
        LS := LP.PackSections.Add;
        LS.SectionName := Section;
        LS.SourceFileName := LevelPackIni;

        // fun_0  = lvl0091.ini,0
        Parser.GetArrayValues(LevelFileNames, Section);
        for iLevel := 0 to LevelFileNames.Count - 1 do
        begin
          LevelFileName := LevelPackDirs[iPack] + '\' + SplitString(LevelFileNames[iLevel], 0, ',');
          LevelParser.LoadFile(LevelFileName);
          LI := LS.LevelInfos.Add;
          LI.OwnerFile := LevelFileName;
          LI.TrimmedTitle := LevelParser.GetValue('name');
//          LI.OwnerFileIndex := iLevel;
        end;

      end;

    end;

  finally
    LevelPackDirs.Free;
    Parser.Free;
    LevelParser.Free;
    SectionNames.Free;
    LevelFileNames.Free;
  end;
end;

end.
(*
procedure TLemminiStyle.InternalPrepareLevelLoading;
{-------------------------------------------------------------------------------
  bit of a mess in alpha.
  nested reading of
  levelpack/section/levelinfo
-------------------------------------------------------------------------------}

var
  LevelPackDirs: TStringList;
  LevelPackDirMask: string;
  SectionNames: TStringList;
  LevelFileNames: TStringList;
  LevelPackIni: string;
  S, Section: string;
  LevelFileName: string;
  Parser, LevelParser: TLemminiParser;
  iPack, iSection, iLevel: Integer;


  LP: TLevelPack;
  LS: TPackSection;
  LI: TLevelInfo;



begin
  // read levelpackdirs
  LevelPackDirMask := CommonPath + 'levels\*.*';
  Parser := TLemminiParser.Create;
  LevelParser := TLemminiParser.Create;
  LevelPackDirs := TStringList.Create;
  SectionNames := TStringList.Create;
  LevelFileNames := TStringList.Create;
  try
    CreateFileList(LevelPackDirs, LevelPackDirMask, faDirectory, True);

    // read levelpacks
    for iPack := 0 to LevelPackDirs.Count - 1 do
    begin
      LevelPackIni := LevelPackDirs[iPack] + '\' + 'levelpack.ini';
      deb(['pack', LevelPackIni]);
      Parser.LoadFile(LevelPackIni);
      S := Parser.GetValue('name');
      // add levelpack
      LP := LevelPacks.Add;
      LP.SourceFileName := LevelPackIni;
      LP.LevelPackName := S;
      // get sections
      // level_0 = Fun
      Parser.GetArrayValues(SectionNames, 'level');
      for iSection := 0 to SectionNames.Count - 1 do
      begin
        Section := SectionNames[iSection];
        //deb(['section', Section]);
        LS := LP.PackSections.Add;
        LS.SectionName := Section;
        LS.SourceFileName := LevelPackIni;

        // fun_0  = lvl0091.ini,0
        Parser.GetArrayValues(LevelFileNames, Section);
        for iLevel := 0 to LevelFileNames.Count - 1 do
        begin
          LevelFileName := LevelPackDirs[iPack] + '\' + SplitString(LevelFileNames[iLevel], 0, ',');
          LevelParser.LoadFile(LevelFileName);
          LI := LS.LevelInfos.Add;
          LI.OwnerFile := LevelFileName;
          LI.TrimmedTitle := LevelParser.GetValue('name');
//          LI.OwnerFileIndex := iLevel;
        end;

      end;

    end;

  finally
    LevelPackDirs.Free;
    Parser.Free;
    LevelParser.Free;
    SectionNames.Free;
    LevelFileNames.Free;
  end;
end;

function TLemminiStyle.InternalGetLevelCount(aLevelPack, aSection: Integer): Integer;
begin
end;

procedure TLemminiStyle.InternalGetLevelInfo(aLevelPack, aSection, aIndex: Integer; out Info: TLevelInfoRec);
begin
end;

function TLemminiStyle.InternalGetLevelPackCount: Integer;
{-------------------------------------------------------------------------------
  Look for the number of directories in the levels-directory.
  That is the number of systems
-------------------------------------------------------------------------------}
var
  L: TStringList;
  Mask: string;
begin
  Mask := CommonPath + 'levels\*.*';
  L := TStringList.Create;
  try
    CreateFileList(L, Mask, faDirectory, False);
    Result := L.Count;
  finally
    L.Free;
  end;
end;

function TLemminiStyle.InternalGetLevelPackName(aLevelPack: Integer): string;
{-------------------------------------------------------------------------------
  Look for the number of directories in the levels-directory.
  That is the number of systems
-------------------------------------------------------------------------------}
var
  L: TStringList;
  Mask: string;
  Mem: TStringList;
  ini: tmeminifile;

begin
  Mask := CommonPath + 'levels\*.*';
  L := TStringList.Create;
  try
    CreateFileList(L, Mask, faDirectory, True);
    L.Sorted := True;
    if aLevelPack > L.Count - 1 then
      raise Exception.Create('lemmini sysname error');
    Mem := TStringList.Create;
    Mem.LoadFromFile(L[aLevelPack] + '\levelpack.ini');
    mem.Insert(0, '[LemminiFake]');
    ini:=TMemIniFile.create('');
    ini.SetStrings(mem);
    result:=ini.ReadString('LemminiFake', 'name', '?');
//    Result := Mem.Values['name'];
    ini.free;
    Mem.Free;
  finally
    L.Free;
  end;
end;

function TLemminiStyle.InternalGetSectionCount(aLevelPack: Integer): Integer;
begin
end;

function TLemminiStyle.InternalGetSectionName(aLevelPack, aSection: Integer): string;
begin
end;

procedure TLemminiStyle.InternalLoadLevel(aLevelPack, aSection, aIndex: Integer; aLevel: TLevel);
var
  Fn: string;
  //Ix: Integer;
  F: TFileStream;
begin
  with LevelPacks[aLevelPack].PackSections[aSection].LevelInfos[aIndex] do
  begin
    Fn := OwnerFile;
//    Ix := OwnerFileIndex;
  end;
  F := TFileStream.Create(Fn, fmOpenRead);
  aLevel.BeginUpdate;
  try
  aLevel.Style := Self;
  aLevel.LoadFromLemminiStream(F);
  finally
  aLevel.EndUpdate;
  end;
  F.Free;
end;


