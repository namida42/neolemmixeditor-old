object FormVgaSpecViewer: TFormVgaSpecViewer
  Left = 150
  Top = 299
  Width = 488
  Height = 142
  Caption = 'View DOS Extended Graphic File'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = Form_Create
  DesignSize = (
    480
    115)
  PixelsPerInch = 96
  TextHeight = 13
  object Img: TImgView32
    Left = 0
    Top = 0
    Width = 480
    Height = 80
    Anchors = [akLeft, akTop, akRight, akBottom]
    Bitmap.ResamplerClassName = 'TNearestResampler'
    Color = clGray
    ParentColor = False
    Scale = 1.000000000000000000
    ScrollBars.ShowHandleGrip = True
    ScrollBars.Style = rbsDefault
    ScrollBars.Visibility = svHidden
    OverSize = 0
    TabOrder = 0
  end
  object BtnClose: TButton
    Left = 400
    Top = 88
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = BtnCloseClick
  end
end
