{$include lem_directives.inc}

{-------------------------------------------------------------------------------
  This unit contains:
  o classes for reading and writing of dos levelpacks
-------------------------------------------------------------------------------}

unit LemDosArc;

interface

uses
  Windows, Classes, Contnrs, SysUtils,
  UMisc,
  LemTypes, LemDosCmp;

type
  TDosDatSection = class
  private
    fBitCnt            : Byte;
    fChecksum          : Byte;
    fDecompressedSize  : Integer;
    fCompressedSize    : Integer; // add 10 and you get to the next
    fHeaderPos         : Integer; // this is the exact fileposition
    fDataPos           : Integer;
    fCompressedData    : TMemoryStream;
    fDecompressedData  : TMemoryStream;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    property BitCnt            : Byte read fBitCnt;
    property Checksum          : Byte read fChecksum;
    property DecompressedSize  : Integer read fDecompressedSize;
    property CompressedSize    : Integer read fCompressedSize;
    property HeaderPos         : Integer read fHeaderPos;
    property DataPos           : Integer read fDataPos;
    property CompressedData    : TMemoryStream read fCompressedData;
    property DecompressedData  : TMemoryStream read fDecompressedData;
  published
  end;

  TDosDatSectionList = class(TObjectList)
  private
    function GetItem(Index: Integer): TDosDatSection;
  protected
  public
    function Add(Item: TDosDatSection): Integer;
    procedure Insert(Index: Integer; Item: TDosDatSection);
    property Items[Index: Integer]: TDosDatSection read GetItem; default;
  published
  end;

  // still used but readonly!!!!!!!!!
  // code should use tarchiveex
  TDosDatArchive = class
  private
    fDecompressor : TDosDatDecompressor;
    fCompressor   : TDosDatCompressor;
    fStream       : TStream;
    fSectionList  : TDosDatSectionList;
    fReadOnly     : Boolean;
    procedure InternalOpenRead;
    procedure InternalClose;
    procedure CheckReadonly;
  protected
    procedure RemoveSection(aIndex: Integer);
    procedure InsertFile(aIndex: Integer; const aFileName: string);
    procedure ReplaceFile(aIndex: Integer; const aFileName: string);
    procedure Replace(aIndex: Integer; aStream: TStream);
    procedure AddFile(const aFileName: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromFile(const aFileName: string);
    procedure CloseFile;
    procedure LoadFromStream(aStream: TStream);
    procedure LoadAll;
    procedure ExtractSection(aSectionIndex: Integer; aStream: TStream);
{
    procedure RemoveSection(aIndex: Integer);
    procedure InsertFile(aIndex: Integer; const aFileName: string);
    procedure ReplaceFile(aIndex: Integer; const aFileName: string);
    procedure Replace(aIndex: Integer; aStream: TStream);
    procedure AddFile(const aFileName: string);
}
    function ExtractLevelTitle(aSectionIndex: Integer): string;
    property SectionList: TDosDatSectionList read fSectionList;
    property ReadOnly: Boolean read fReadOnly write fReadOnly;
  published
  end;

  {-------------------------------------------------------------------------------
    Class to (I hope safely) manipulate dos dat archives
  -------------------------------------------------------------------------------}
type
  TDosDatArchiveEx = class
  private
    fDecompressor : TDosDatDecompressor;
    fCompressor   : TDosDatCompressor;
    fSectionList  : TDosDatSectionListEx;
    fFileName     : string;
    procedure InternalReadFromStream(aStream: TStream);
    procedure InternalSaveToStream(aStream: TStream);
    procedure InternalClose;
    procedure SetFileName(const Value: string);
    procedure CheckAdd;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromFile(const aFileName: string = '');
    procedure LoadFromStream(S: TStream);
    procedure SaveToFile(const aFileName: string = '');
    procedure SaveToStream(S: TStream);
    procedure Close;
    function CalcCheckSum: Byte;
  { easy manipulate methods }
    //procedure DeleteSection(aIndex: Integer);
    //procedure AddSection;
    procedure InsertFileAt(const aFileName: string; aIndex: Integer);
    procedure OverwriteFileAt(const aFileName: string; aIndex: Integer);
    procedure InsertStreamAt(aStream: TStream; aIndex: Integer);
    procedure OverwriteStreamAt(aStream: TStream; aIndex: Integer);
    //function InsertStream()
  { properties }
    property Decompressor: TDosDatDecompressor read fDecompressor;
    property Compressor: TDosDatCompressor read fCompressor;
    property SectionList: TDosDatSectionListEx read fSectionList;
    property FileName: string read fFileName write SetFileName;
  end;

implementation


{ TDosDatSection }

constructor TDosDatSection.Create;
begin
  inherited;
  fCompressedData := TMemoryStream.Create;
  fDecompressedData := TMemoryStream.Create;
end;

destructor TDosDatSection.Destroy;
begin
  fCompressedData.Free;
  fDecompressedData.Free;
  inherited;
end;


{ TDosDatSectionList }

function TDosDatSectionList.Add(Item: TDosDatSection): Integer;
begin
  Result := inherited Add(Item);
end;

function TDosDatSectionList.GetItem(Index: Integer): TDosDatSection;
begin
  Result := inherited Get(Index);
end;

procedure TDosDatSectionList.Insert(Index: Integer; Item: TDosDatSection);
begin
  inherited Insert(Index, Item);
end;

{ TDosDatArchive }

procedure TDosDatArchive.AddFile(const aFileName: string);
var
  F: TFileStream;
  //Cmp: TDosDatCompressor;
begin
  CheckReadonly;
  F := TFileStream.Create(aFileName, fmOpenRead);
  try
    fStream.Seek(0, soFromEnd);
    fCompressor.Compress(F, fStream);
    InternalOpenRead;
  finally
    F.Free;
  end;
end;

procedure TDosDatArchive.InsertFile(aIndex: Integer; const aFileName: string);
var
  F: TFileStream;
  Mem: TMemoryStream;
  Cmp: TDosDatCompressor;
  S: TDosDatSection;
  Cnt: Integer;
begin
  if aIndex > fSectionList.Count - 1 then
    AddFile(aFileName);

  CheckReadonly;
  F := TFileStream.Create(aFileName, fmOpenRead);
  Cmp := TDosDatCompressor.Create;
  Mem := TMemoryStream.Create;
  try
    // stream-->mem: copy begin
    S := fSectionList[aIndex];
    Cnt := S.HeaderPos;
    if Cnt > 0 then
    begin
      fStream.Seek(0, soFromBeginning);
      Mem.CopyFrom(fStream, Cnt);
    end;

    // insert file
    Cmp.Compress(F, Mem);




    // stream-->mem: copy rest
    if aIndex <= fSectionList.Count - 1 then
    begin
      fStream.Position := fSectionList[aIndex].HeaderPos;
      Cnt := fStream.Size - fStream.Position;
      if Cnt > 0 then
      begin
        Mem.CopyFrom(fStream, fStream.Size - fStream.Position);
//        fStream.Size := 0;
  //      fStream.CopyFrom(Mem, 0);
      end;
    end;

(*
    // copy rest
    Cnt := fStream.Size - fStream.Position;
    if Cnt > 0 then
    begin
      Mem.CopyFrom(fStream, fStream.Size - fStream.Position);
    end;
  *)


    fStream.Size := 0;
    fStream.CopyFrom(Mem, 0);

    InternalOpenRead;
  finally
    F.Free;
    Cmp.Free;
    Mem.Free;
  end;
end;

procedure TDosDatArchive.ReplaceFile(aIndex: Integer; const aFileName: string);
var
  F: TFileStream;
  Mem: TMemoryStream;
  Cmp: TDosDatCompressor;
  S: TDosDatSection;
  Cnt: Integer;
begin
  if aIndex > fSectionList.Count - 1 then
    AddFile(aFileName);

  CheckReadonly;
  F := TFileStream.Create(aFileName, fmOpenRead);
  Cmp := TDosDatCompressor.Create;
  Mem := TMemoryStream.Create;
  try
    // stream-->mem: copy begin
    S := fSectionList[aIndex];
    Cnt := S.HeaderPos;
    if Cnt > 0 then
    begin
      fStream.Seek(0, soFromBeginning);
      Mem.CopyFrom(fStream, Cnt);
    end;

    // mem: insert file
    Cmp.Compress(F, Mem);

    // stream-->mem: copy rest
    if aIndex + 1 <= fSectionList.Count - 1 then
    begin
      fStream.Position := fSectionList[aIndex + 1].HeaderPos;
      Cnt := fStream.Size - fStream.Position;
      if Cnt > 0 then
      begin
        Mem.CopyFrom(fStream, fStream.Size - fStream.Position);
//        fStream.Size := 0;
  //      fStream.CopyFrom(Mem, 0);
      end;
    end;

    fStream.Size := 0;
    fStream.CopyFrom(Mem, 0);


    InternalOpenRead;
  finally
    F.Free;
    Cmp.Free;
    Mem.Free;
  end;
end;

constructor TDosDatArchive.Create;
begin
  inherited Create;
  fDecompressor := TDosDatDecompressor.Create;
  fCompressor := TDosDatCompressor.Create;
  fSectionList := TDosDatSectionList.Create;
  fReadonly := True;
end;

destructor TDosDatArchive.Destroy;
begin
  InternalClose;
  fDecompressor.Free;
  fCompressor.Free;
  FreeAndNil(fSectionList);
  inherited Destroy;
end;

procedure TDosDatArchive.ExtractSection(aSectionIndex: Integer; aStream: TStream);
var
  //Header: TCompressionHeaderRec;
  //Rd, Wr: Integer;
  A: TDosDatSection;
 // InBuf, OutBuf: PBytes;
begin
  if aSectionIndex > fSectionList.Count - 1 then
    raise Exception.Create('extractsection error');

  A := fSectionList[aSectionIndex];
  fStream.Seek(A.HeaderPos, soFromBeginning);
  fDecompressor.DecompressSection(fStream, aStream);

  exit;

  (*
//  origineel
  A := fSectionList[aSectionIndex];
  Getmem(InBuf, A.CompressedSize);
  GetMem(Outbuf, A.DecompressedSize);
  fStream.Seek(A.Datapos, soFromBeginning);
  Rd := fStream.Read(InBuf^, A.CompressedSize);
  TDosDatDecompressor.Decompress(InBuf, OutBuf, A.BitCnt, A.CompressedSize, A.DecompressedSize);
  Wr := aStream.Write(OutBuf^, A.DecompressedSize);
  FreeMem(InBuf, A.CompressedSize);
  FreeMem(OutBuf, A.DecompressedSize);
  *)
end;

function TDosDatArchive.ExtractLevelTitle(aSectionIndex: Integer): string;
var
  //Header: TCompressionHeaderRec;
 // Rd, Wr: Integer;
  A: TDosDatSection;
  //InBuf, OutBuf: PBytes;
  aStream: TMemoryStream;
//  var
  //  i: Integer;
begin



//  SetLength(Result, 32);
  Result := '';
  if aSectionIndex > fSectionList.Count - 1 then
    raise Exception.Create('extractsection error ' + i2s(ASECTIONindex));

  A := fSectionList[aSectionIndex];
  fStream.Seek(A.HeaderPos, soFromBeginning);
  aStream := TMemoryStream.Create;
  fDecompressor.DecompressSection(fStream, aStream);
  if aStream.Size <> 2048 then
    raise exception.create('extractleveltitleerror');
  Result := StringOfChar(' ', 32);   //lemfiles
  astream.seek(-32, soFromEnd);
//  deb([astream.position]);
//  windlg([astream.size]);
  aStream.readbuffer(Result[1], 32);
//  Result := Trim(Result);
  aStream.Free;

end;

procedure TDosDatArchive.RemoveSection(aIndex: Integer);
var
  Mem: TMemoryStream;
begin
  CheckReadonly;

  if not Between(aIndex, 0, SectionList.Count - 1) then
    Exit;

  // when is last then simply truncate
  if aIndex = SectionList.Count - 1 then
  begin
    fStream.Size := SectionList[aIndex].HeaderPos;
//    fSectionList.Delete(aIndex);
    InternalOpenRead;
    Exit;
  end;

  //it is not the last:
  // copy end to buffer
  Mem := TMemoryStream.Create;
  try
    fStream.Seek(SectionList[aIndex + 1].HeaderPos, soFromBeginning);
    Mem.CopyFrom(fStream, fStream.Size - fStream.Position);

    // truncate
    fStream.Size := SectionList[aIndex].HeaderPos;
//    fStream.Seek(SectionList[aIndex].HeaderPos, soFromBeginning);

    // copy from buffer
    Mem.Seek(0, soFromBeginning);
    fStream.Seek(0, soFromEnd);
    fStream.CopyFrom(Mem, Mem.Size);

    InternalOpenRead;

  finally
    Mem.Free;
  end;

//          uzip
end;


procedure TDosDatArchive.InternalClose;
begin
  fSectionList.Clear;
  FreeAndNil(fStream);
end;

procedure TDosDatArchive.InternalOpenRead;
var
  Header: TCompressionHeaderRec;
  Rd: Integer;
  Sec: TDosDatSection;
  pp: Integer;
begin
//  if fStream.Size = 0 then
  //  apperror('bestand is leeg');
  fSectionList.Clear;
  fStream.Seek(0, soFromBeginning);

  repeat
    pp := fStream.position;
    Rd := fStream.Read(Header, SizeOf(Header));
    if Rd = 0 then
      Break;
    if Rd <> SizeOf(Header) then raise exception.create('header error');
    Header.CompressedSize := System.Swap(Header.CompressedSize);
    Header.Unused2 := System.Swap(Header.Unused2);
    Header.DecompressedSize := System.Swap(Header.DecompressedSize);
    Header.Unused1 := System.Swap(Header.Unused1);
    Sec := TDosDatSection.Create;
    Sec.fDecompressedSize := Header.DecompressedSize + (Header.Unused1 shl 16);
    Sec.fCompressedSize := Header.CompressedSize + (Header.Unused2 shl 16);
    Dec(Sec.fCompressedSize, 10);
    Sec.fBitCnt := Header.BitCnt;
    Sec.fChecksum := Header.Checksum;
    Sec.fheaderpos := pp;
    Sec.fDatapos := fStream.Position;
    fSectionList.Add(Sec);
    fStream.Seek(Sec.fCompressedSize, soFromCurrent);
    //deb(['section', sec.fdatapos]);
  until False;
end;

procedure TDosDatArchive.LoadFromFile(const aFileName: string);
//var
//  Att: Integer;
begin
  InternalClose;

  (*
  Att :=  GetFileAttributes(PChar(aFileName));
  if (FILE_ATTRIBUTE_READONLY or FILE_ATTRIBUTE_HIDDEN) and Att <> 0 then
  else
    fStream := TFileStream.Create(aFileName, fmOpenRead); *)

  fStream := TFileStream.Create(aFileName, fmOpenRead{fmOpenReadWrite});
  try
    InternalOpenRead;
  except
    InternalClose;
    raise;
  end;
end;

procedure TDosDatArchive.CloseFile;
begin
  InternalClose;
end;


procedure TDosDatArchive.LoadFromStream(aStream: TStream);
begin
//  fStream := aStream;
  //InternalOpenRead;
end;


procedure TDosDatArchive.CheckReadonly;
begin
  if fReadOnly then raise Exception.Create('archive is readonly protected');
end;


procedure TDosDatArchive.LoadAll;
var
  i: Integer;
  S: TMemoryStream;
begin
  for i := 0 to SectionList.Count - 1 do
  begin
    S := SectionList[i].fDecompressedData;
    S.Clear;
    ExtractSection(i, S);
  end;
end;

procedure TDosDatArchive.Replace(aIndex: Integer; aStream: TStream);
{-------------------------------------------------------------------------------
  Replaces the current level at <aIndex> with S.
  aStream must be a uncompressed LVL stream.
-------------------------------------------------------------------------------}
var
//  F: TFileStream;
  Mem: TMemoryStream;
  S: TDosDatSection;
  Cnt: Integer;
begin
  CheckReadonly;

  if aStream = nil then
    raise exception.create('dos dat archive replace error 1: stream=nil');
  if aStream.Size <> 2048 then
    raise exception.create('dos dat archive replace error 2: stream.size<>2048');
  if aIndex > fSectionList.Count - 1 then
    raise exception.create('dos dat archive replace error 3: index too big');
  if aIndex < 0 then
    raise exception.create('dos dat archive replace error 4: index below zero');

  aStream.Seek(0, soFromBeginning);

  Mem := TMemoryStream.Create;
  try
    // copy begin
    S := fSectionList[aIndex];
    Cnt := S.HeaderPos;
    if Cnt > 0 then
    begin
      fStream.Seek(0, soFromBeginning);
      Mem.CopyFrom(fStream, Cnt);
    end;

    // insert stream
    fCompressor.Compress(aStream, Mem);

    // copy rest
    if aIndex + 1 < fSectionList.Count - 1 then
    begin
      fStream.Position := fSectionList[aIndex + 1].HeaderPos;
      Cnt := fStream.Size - fStream.Position;
      if Cnt > 0 then
      begin
        Mem.CopyFrom(fStream, fStream.Size - fStream.Position);
        fStream.Size := 0;
        fStream.CopyFrom(Mem, 0);
      end;
    end;

    InternalOpenRead;
  finally
    Mem.Free;
  end;
end;


{ TDosDatArchiveEx }

constructor TDosDatArchiveEx.Create;
begin
  inherited;
  fDecompressor := TDosDatDecompressor.Create;
  fCompressor := TDosDatCompressor.Create;
  fSectionList := TDosDatSectionListEx.Create;
end;

destructor TDosDatArchiveEx.Destroy;
begin
  InternalClose;
  fDecompressor.Free;
  fCompressor.Free;
  FreeAndNil(fSectionList);
  inherited;
end;

procedure TDosDatArchiveEx.InsertFileAt(const aFileName: string; aIndex: Integer);
var
  Sec: TDosDatSectionEx;
begin
  CheckAdd;
  Sec := TDosDatSectionEx.Create;
  try
    Sec.DecompressedData.LoadFromFile(aFileName);
    fCompressor.Compress(Sec.DecompressedData, Sec.CompressedData);
    fSectionList.Insert(aIndex, Sec);
  except
    Sec.Free;
    raise;
  end;
end;

procedure TDosDatArchiveEx.OverwriteFileAt(const aFileName: string; aIndex: Integer);
var
  //Old,
  Sec: TDosDatSectionEx;
begin

//  Old := fSectionList[aIndex];

  Sec := TDosDatSectionEx.Create;
  try
    Sec.DecompressedData.LoadFromFile(aFileName);
    fCompressor.Compress(Sec.DecompressedData, Sec.CompressedData);
  except
    Sec.Free;
    raise;
  end;

  fSectionList.Items[aIndex] := Sec; // replace (free of old is automatic)
//  Old.Free;
end;


procedure TDosDatArchiveEx.InsertStreamAt(aStream: TStream; aIndex: Integer);
{-------------------------------------------------------------------------------
  o Create new section
  o Copy aStream to section
  o Insert section to sectionlist at <aIndex>
-------------------------------------------------------------------------------}
var
  Sec: TDosDatSectionEx;
begin
  CheckAdd;
  Sec := TDosDatSectionEx.Create;
  try
    Sec.DecompressedData.CopyFrom(aStream, 0);
    Sec.DecompressedData.Seek(0, soFromBeginning);
    fCompressor.Compress(Sec.DecompressedData, Sec.CompressedData);
    fSectionList.Insert(aIndex, Sec);
  except
    Sec.Free;
    raise;
  end;
end;

procedure TDosDatArchiveEx.OverwriteStreamAt(aStream: TStream; aIndex: Integer);
{-------------------------------------------------------------------------------
  Overwrite the decompressed data of a section with the data of <aStream>.
-------------------------------------------------------------------------------}
var
  Sec: TDosDatSectionEx;
begin
//  CheckIndex;
//  if (aIndex < 0) or (aIndex > sectionlist.count - 1) then
  //  raise exception.create('index error');

  Sec := TDosDatSectionEx.Create;
  try
    Sec.DecompressedData.CopyFrom(aStream, 0);
    Sec.DecompressedData.Seek(0, soFromBeginning);
    fCompressor.Compress(Sec.DecompressedData, Sec.CompressedData);
  except
    Sec.Free;
    raise;
  end;
  fSectionList.Items[aIndex] := Sec;
end;

procedure TDosDatArchiveEx.InternalClose;
begin
  fSectionList.Clear;
end;

procedure TDosDatArchiveEx.InternalReadFromStream(aStream: TStream);
{-------------------------------------------------------------------------------
  Key method for reading a dos dat-file into sectionlist
-------------------------------------------------------------------------------}
begin
  fDecompressor.LoadSectionList(aStream, fSectionList, True);
end;

procedure TDosDatArchiveEx.InternalSaveToStream(aStream: TStream);
begin
  fCompressor.StoreSectionList(fSectionList, aStream, True);
end;

procedure TDosDatArchiveEx.LoadFromFile(const aFileName: string = '');
{-------------------------------------------------------------------------------
  Use FileName property if <aFileName> is an empty string
-------------------------------------------------------------------------------}
var
  F: TFileStream;
  S: string;
begin
  if aFileName <> '' then
    S := aFileName
  else
    S := fFileName;

  F := TFileStream.Create(S, fmOpenRead);
  try
    LoadFromStream(F);
    fFileName := S;
  finally
    F.Free;
  end;
end;

procedure TDosDatArchiveEx.LoadFromStream(S: TStream);
begin
  InternalClose;
  try
    InternalReadFromStream(S);
  except
    InternalClose;
    raise;
  end;
end;

procedure TDosDatArchiveEx.SaveToFile(const aFileName: string = '');
{-------------------------------------------------------------------------------
  Use FileName property if <aFileName> is an empty string
-------------------------------------------------------------------------------}
var
  F: TFileStream;
  S: string;
begin
  if (fFileName <> '')
  and (aFileName <> '')
  and (CompareText(fFileName, aFileName) = 0) then
    CopyFile(PChar(fFileName), PChar(ReplaceFileExt(fFileName, '.~bk')), False);

  if aFileName <> '' then
    S := aFileName
  else
    S := fFileName;

  if S = '' then
    raise Exception.Create('DosDatArchive, no filename specified');  

  F := TFileStream.Create(S, fmCreate);
  try
    SaveToStream(F);
    fFileName := S;
  finally
    F.Free;
  end;
end;

procedure TDosDatArchiveEx.SaveToStream(S: TStream);
begin
  InternalSaveToStream(S);
end;

procedure TDosDatArchiveEx.SetFileName(const Value: string);
begin
  fFileName := Value;
end;

procedure TDosDatArchiveEx.Close;
begin
  InternalClose;
end;

procedure TDosDatArchiveEx.CheckAdd;
begin
  {if SectionList.Count > 16 then
    raise Exception.Create('dos archives are not allowed by Lemmix to contain more than 16 sections');}
end;

function TDosDatArchiveEx.CalcCheckSum: Byte;
var
  i, m: Integer;
begin
  Result := 0;
  for i := 0 to sectionlist.Count-1 do
  begin


    with sectionlist[i].DecompressedData do
    begin
      for m := 0 to size - 1 do
        result := result xor pbytes(memory)^[m];
    end;

  end;
end;

end.


