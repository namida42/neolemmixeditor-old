//{$define develop}

  // --- CHECK VERSION
  // --- CHECK ccexplore private version in project
  // --- CHECK LEMSTRINGS FOR VERSION + string ---


{$ifdef develop}
 {$O-} // optimization
 {$R-} // range checks
 {$Q-} // overflow checks
 {$D+} // debug info
 {$L+}
 {$YD} // reference info, definitions only
 {$C+} // assertions
{$else}
 {$O+} // optimization
 {$R-} // range checks
 {$Q-} // overflow checks
 {$L-}
 {$Y-} // reference info, definitions only
 {$D-} // reference info, definitions only
 {$C-} // assertions
{$endif}










(*
Strict var-strings 	Sets up string parameter error checking. Corresponds to {$V}.
(If the Open parameters option is selected, this option is not applicable.)
Complete boolean eval 	Evaluates every piece of an expression in Boolean terms,
regardless of whether the result of an operand evaluates as false. Corresponds to {$B}.
Extended syntax 	Enables you to define a function call as a procedure and to
ignore the function result. Also enables PChar support. Corresponds to {$X}.

Typed @ operator 	Controls the type of pointer returned by the @ operator.
Corresponds to {$T}.
Open parameters 	Enables open string parameters in procedure and function declarations.
Corresponds to {$P}. Open parameters are generally safer, and more efficient.
Huge strings	Enables new garbage collected strings.
The string keyword corresponds to the AnsiString type with this option enabled.
Otherwise the string keyword corresponds to the ShortString type. Corresponds to {$H}.

Assignable typed constants	Enable this for backward compatibility with Delphi 1.0 on Windows.
When enabled, the compiler allows assignments to typed constants. Corresponds to {$J}
.
*)
