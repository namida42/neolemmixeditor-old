{$include lem_directives.inc}

unit LemEdit;

interface
uses
  LemEditHistory,
  Dialogs,
  Classes, Types, Messages, ActnList, Math, ClipBrd, SysUtils,
  UEvents, UMisc, UFastStrings,
  UTools,
  LemTypes, LemStrings,
  LemCore;


const
  LM_SELECTIONCHANGING         = LM_EDIT + 1;
  LM_SELECTIONCHANGED          = LM_EDIT + 2;
  LM_OBJECTFILTERCHANGED       = LM_EDIT + 3;
  LM_TERRAINFILTERCHANGED      = LM_EDIT + 4;

  MAX_UNDO_LEVEL = 100;

// message procedure sjablones
(*
// global
  procedure LMLanguageChanged(var Msg: TLMLanguageChanged); message LM_LANGUAGECHANGED;

// level
  procedure LMLevelChanging(var Msg: TLMLevelChanging); message LM_LEVELCHANGING;
  procedure LMLevelChanged(var Msg: TLMLevelChanged); message LM_LEVELCHANGED;
  procedure LMStaticsChanged(var Msg: TLMStaticsChanged); message LM_STATICSCHANGED;
  procedure LMObjectListChanging(var Msg: TLMObjectListChanging); message LM_OBJECTLISTCHANGING;
  procedure LMObjectListChanged(var Msg: TLMObjectListChanged); message LM_OBJECTLISTCHANGED;
  procedure LMTerrainListChanging(var Msg: TLMTerrainListChanging); message LM_TERRAINLISTCHANGING;
  procedure LMTerrainListChanged(var Msg: TLMTerrainListChanged); message LM_TERRAINLISTCHANGED;
  procedure LMSteelListChanging(var Msg: TLMSteelListChanging); message LM_STEELLISTCHANGING;
  procedure LMSteelListChanged(var Msg: TLMSteelListChanged); message LM_STEELLISTCHANGED;
// leveleditor
  procedure LMSelectionChanging(var Msg: TLMSelectionChanging); message LM_SELECTIONCHANGING;
  procedure LMSelectionChanged(var Msg: TLMSelectionChanged); message LM_SELECTIONCHANGED;
  procedure LMObjectFilterChanged(var Msg: TLMObjectFilterChanged); message LM_OBJECTFILTERCHANGED
  procedure LMTerrainFilterChanged(var Msg: TLMTerrainFilterChanged); message LM_TERRAINFILTERCHANGED
*)


type

(*  TSelectorObjectType = (
    sotNone,
    sotTerrain,
    sotInteractiveObject,
    sotSteel
  );
  TSelectorObjectTypes = set of TSelectorObjectType;*)

  TItemHorzAlign = (
    ihaNone,
    ihaLeftSides,
    ihaRightSides,
    ihaCenters,
    ihaTile
  );

  TItemVertAlign = (
    ivaNone,
    ivaTops,
    ivaBottoms,
    ivaCenters,
    ivaTile
  );

//const
  //AllSelectorTypes = [sotTerrain, sotInteractiveObject, sotSteel];



type
  TSelector = class;
  TLevelEditor = class;

  TLMSelectionChanged = packed record
    Msg: Cardinal;
    Item: TSelector;
    Unused: Longint;
    Result: Longint;
  end;

  TLMObjectFilterChanged = TMessage;
  TLMTerrainFilterChanged = TMessage;

  TLevelEditorLink = class(TLevelLink)
  private
    function GetLevelEditor: TLevelEditor;
    procedure SetLevelEditor(const Value: TLevelEditor);
  protected
  public
    property LevelEditor: TLevelEditor read GetLevelEditor write SetLevelEditor;
  end;

  TSelector = class(TCollectionItem)
  private
    fRefItem: TBaseLevelItem;
//    function GetObjectType: TSelectorObjectType;
  protected
  public
//    property ObjectType: TSelectorObjectType read GetObjectType;
    property RefItem: TBaseLevelItem read fRefItem write fRefItem;
  published
  end;

  TSelectorCollection = class(TOwnedCollectionEx)
  private
    fCurrentSet: TLevelItemTypes;
    function GetItem(Index: Integer): TSelector;
    procedure SetItem(Index: Integer; const Value: TSelector);
    function GetCenter: TPoint;
  protected
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
  public
  // default methods
    constructor Create(aOwner: TPersistent);
    function Add: TSelector;
    function Insert(Index: Integer): TSelector;
    property Items[Index: Integer]: TSelector read GetItem write SetItem; default;
    procedure Update(Item: TCollectionItem); override;
    procedure SortByIndex(aList: TList);
  // edit methods
    procedure Move(DeltaX, DeltaY: Integer);
    function FindBaseItem(aItem: TBaseLevelItem): Integer; //classes

    property Center: TPoint read GetCenter;
//    property CurrentSet: TLevelItemTypes read fCurrentSet;
//    property SingleType: TLevelItemType read
  published
  end;

  // extended level for editing
  TLevelEditor = class(TLevel)
  private
    fHistory: TFormatMemoryStreamList;
    fHistoryPosition: Integer;
    fRedoing: Boolean;
    fSelectorCollection  : TSelectorCollection;
    fTerrainFilter: TByteSet;
    fObjectFilter: TByteSet;
    fLoader : TObject;
    fInternalClipboard: String;
    fUseInternalClipboard: Boolean;
    function GetSelectorCount: Integer;
    function GetSelector(Index: Integer): TSelector;
    procedure FillSelectorCollection;
    function GetSelectedTerrain: TTerrain;
    procedure SetSelectedTerrain(const Value: TTerrain);
    function GetSelectedObject: TInteractiveObject;
    procedure SetSelectedObject(const Value: TInteractiveObject);
    function GetSelectedSteel: TSteel;
    procedure SetSelectedSteel(const Value: TSteel);
    function GetSelectedItem: TBaseLevelItem;
    procedure SetSelectedItem(const Value: TBaseLevelItem);

    procedure SetObjectFilter(const Value: TByteSet);
    procedure SetTerrainFilter(const Value: TByteSet);
    function GetAsClipBoardString: string;
    procedure SetAsClipBoardString(const Value: string);
    procedure SetLoader(aLoader: TObject);

  protected
    procedure ClearLevel(DoClearStyle, DoClearGraph: Boolean); override;
    procedure MsgSelectionChanged(Item: TSelector); virtual;

    procedure CheckObjectX(aObject: TInterActiveObject; var X: Integer); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SaveToFile(const aFileName: string; aFormat: TLemmingFileFormat; UpdateMemName: Boolean = true); override;
  { selection manipulation }
    procedure MoveSelection(DeltaX, DeltaY: Integer);
    procedure CloneSelection(DeltaX, DeltaY: Integer);
//    procedure ResizeSteelSelection()
    procedure DeleteSelection;
    //procedure Delete
    procedure ExtendSelection(aList: TList; Toggle: Boolean); overload;
    procedure ExtendSelection(Item: TBaseLevelItem; Toggle: Boolean); overload;
    procedure FilterSelection(aRemoveFilter: TLevelItemTypes);
    procedure AlignSelection(aHorz: TItemHorzAlign; aVert: TItemVertAlign);
    procedure SelectionToFront;
    procedure SelectionToBack;
    procedure DosAlignSelection;
    procedure SelectAll(aTypes: TLevelItemTypes = AllItemTypes);
    procedure AutoSteel;
    procedure CopyToClipBoard;
    procedure PasteFromClipBoard(DeltaX: Integer = 0; DeltaY: Integer = 0; CopyCount: Integer = 1;
      AbsolutePosition: Boolean = False);
    procedure ShiftLevel(DeltaX: Integer; DeltaY: Integer);
    procedure CopyLayoutFromLevel(aSrcLevel: TLevelEditor);

    // Undo stuff
    procedure AddToHistory;
    procedure Undo;
    procedure Redo;
    procedure EndUpdate; override;


//    property Title: string read fTitle write fTitle;
//    procedure SetLoadInfo(IsLevelPack)

//    procedure EditorLoad(const aFileName: string; aStyle: TBaseLemmingStyle)
//    function AllEqual(const PropName: string; Value: Variant): Boolean;

    procedure SelectNextItem(Forwards: Boolean);

    property History: TFormatMemoryStreamList read fHistory;
    property HistoryPosition: Integer read fHistoryPosition write fHistoryPosition;
    property UseInternalClipboard: Boolean read fUseInternalClipboard write fUseInternalClipboard;
    property AsClipBoardString: string read GetAsClipBoardString write SetAsClipBoardString;
  { properties }
    property SelectorCollection: TSelectorCollection read fSelectorCollection;
    property SelectorCount: Integer read GetSelectorCount;
    property Selectors[Index: Integer]: TSelector read GetSelector;

    property SelectedObject: TInteractiveObject read GetSelectedObject write SetSelectedObject;
    property SelectedTerrain: TTerrain read GetSelectedTerrain write SetSelectedTerrain;
    property SelectedSteel: TSteel read GetSelectedSteel write SetSelectedSteel;
    property SelectedItem: TBaseLevelItem read GetSelectedItem write SetSelectedItem;

    property ObjectFilter: TByteSet read fObjectFilter write SetObjectFilter;
    property TerrainFilter: TByteSet read fTerrainFilter write SetTerrainFilter;


    property Loader: TObject read fLoader write SetLoader;
  published
  end;


  TLevelMgr = class
    // list of opened levels (?)
    // WHO is responsible


//    procedure OpenLevelFromDosArchive
  end;


var
  GlobalEditor: TLevelEditor;
  //GlobalSaver: ILevelSaver;

implementation

uses
  LemDosStyles, LemLemminiStyles;

{ TSelector }

(*function TSelector.GetObjectType: TSelectorObjectType;
begin
  Result := sotNone;
  if RefItem = nil then
    Exit;
  if RefItem is TInteractiveObject then
    Result := sotInteractiveObject
  else if RefItem is TTerrain then
    Result := sotTerrain
end;*)

{ TSelectorCollection }

function TSelectorCollection.Add: TSelector;
begin 
  Result := TSelector(inherited Add);
end; 

constructor TSelectorCollection.Create(aOwner: TPersistent); 
begin 
  inherited Create(aOwner, TSelector); 
end; 

function TSelectorCollection.FindBaseItem(aItem: TBaseLevelItem): Integer;
begin
  with HackedList do
    for Result := 0 to Count - 1 do
      if TSelector(List^[Result]).RefItem = aItem then
        Exit;
  Result := -1;
end;

function TSelectorCollection.GetCenter: TPoint;
var
  i: Integer;
  X1, X2, Y1, Y2: Integer;
  B: TBaseLevelItem;
begin
  Result := Point(0, 0);
  if HackedList.Count = 0 then
    Exit;
  X1 := MaxInt;
  X2 := MinInt;
  Y1 := MaxInt;
  Y2 := MinInt;
  with HackedList do
    for i := 0 to Count - 1 do
    begin
      B := TSelector(List^[i]).RefItem;
      X1 := Min(B.XPosition, X1);
      X2 := Max(B.XPosition + B.ItemWidth, X2);
      Y1 := Min(B.YPosition, Y1);
      Y2 := Max(B.YPosition + B.ItemHeight, Y2);
//      Log([x1,y1,x2,y1]);
    end;
  Result.X := (X2 + X1) div 2;
  Result.Y := (Y2 + Y1) div 2;
//  Log([x1,y1,x2,y1]);
//  Log([result.x,result.y]);
//  Log(['-------']);
end;


function TSelectorCollection.GetItem(Index: Integer): TSelector;
begin
  Result := TSelector(inherited GetItem(Index))
end;

function TSelectorCollection.Insert(Index: Integer): TSelector;
begin
  Result := TSelector(inherited Insert(Index))
end;

procedure TSelectorCollection.Move(DeltaX, DeltaY: Integer);
begin
end;

procedure TSelectorCollection.Notify(Item: TCollectionItem; Action: TCollectionNotification);
//var
  //B: TSelector absolute Item;
begin
(*  if B = nil then
    Exit;
  if B.RefItem = nil then
    Exit;
  case Action of
    cnAdded:
      begin
        Include(fCurrentSet, B.RefItem.ItemType);
      end;
    cnExtracting, cnDeleting:
      begin
        Exclude(fCurrentSet, B.RefItem.ItemType);
      end;
  end;
  if CurrentSet = [litTerrain] then Log(['terrain only']); *)
end;

procedure TSelectorCollection.SetItem(Index: Integer; const Value: TSelector);
begin
  inherited SetItem(Index, Value); 
end; 

procedure TSelectorCollection.SortByIndex(aList: TList);
var
  i: Integer;
  B: TBaseLevelItem;
begin
  // first the objects
  with HackedList do
    for i := 0 to Count - 1 do
    begin
      B := TSelector(List^[i]).RefItem;
      case B.ItemType of
        litTerrain:
          begin

          end;
      end;
    end;
end;

procedure TSelectorCollection.Update(Item: TCollectionItem);
begin
  Assert(Owner is TLevelEditor, 'TSelectorCollection.Update Owner error');
  Assert((Item = nil) or (Item is TSelector),  'TSelectorCollection.Update Item class error');
  TLevelEditor(Owner).MsgSelectionChanged(TSelector(Item));
end;

{ TLevelEditorLink }

function TLevelEditorLink.GetLevelEditor: TLevelEditor;
begin
  Assert((Level = nil) or (Level is TLevelEditor), 'TLevelEditorLink.GetLevelEditor error');
  Result := TLevelEditor(Level);
end;

procedure TLevelEditorLink.SetLevelEditor(const Value: TLevelEditor);
begin
  inherited Level := Value;
end;



{ TLevelEditor }

constructor TLevelEditor.Create(aOwner: TComponent);
begin
  inherited;
//  fMgr := TMessageManager.Create(Self);
  fSelectorCollection := TSelectorCollection.Create(Self);
  fInternalClipboard := '';
  fUseInternalClipboard := false;
  fHistory := TFormatMemoryStreamList.Create(true);
  fHistoryPosition := -1;
  fRedoing := false;
//  InteractiveObjectCollection.AfterUpdate := ObjectCollection_AfterUpdate;
//  TerrainCollection.AfterUpdate := TerrainCollection_AfterUpdate;
end;

destructor TLevelEditor.Destroy;
begin
//  fMgr.Free;
  fSelectorCollection.Free;
  fHistory.Free;
  inherited Destroy;
end;

procedure TLevelEditor.SaveToFile(const aFileName: string; aFormat: TLemmingFileFormat; UpdateMemName: Boolean = true);
begin
  fRedoing := true;
  try
    inherited;
  finally
    fRedoing := false;
  end;
end;

procedure TLevelEditor.CopyLayoutFromLevel(aSrcLevel: TLevelEditor);
var
  i: Integer;
  O, sO: TInteractiveObject;
  T, sT: TTerrain;
  S, sS: TSteel;
begin
  // Hopefully this covers everything for every supported style. But I don't care too
  // much if it doesn't work for Lemmix and Lemmini - as long as it does for NeoLemmix,
  // and if possible, SuperLemmini.

  BeginUpdate;

  // Internal stuff
  Graph := aSrcLevel.Graph;

  // Statics. Most of these are NOT copied.
  SetLength(Statics.fWindowOrder, Length(aSrcLevel.Statics.fWindowOrder));
  for i := 0 to Length(Statics.fWindowOrder)-1 do
    Statics.fWindowOrder[i] := aSrcLevel.Statics.fWindowOrder[i];

  Statics.GraphicSet := aSrcLevel.Statics.GraphicSet;
  Statics.GraphicSetEx := aSrcLevel.Statics.GraphicSetEx;
  Statics.AutoSteelOptions := (aSrcLevel.Statics.AutoSteelOptions and $EF) or (Statics.AutoSteelOptions and $10); // don't copy bit4
  Statics.ScreenPosition := aSrcLevel.Statics.ScreenPosition;
  Statics.ScreenYPosition := aSrcLevel.Statics.ScreenYPosition;
  Statics.Width := aSrcLevel.Statics.Width;
  Statics.Height := aSrcLevel.Statics.Height;
  Statics.FallDistance := aSrcLevel.Statics.FallDistance;
  Statics.BoundaryT := aSrcLevel.Statics.BoundaryT;
  Statics.BoundaryL := aSrcLevel.Statics.BoundaryL;
  Statics.BoundaryR := aSrcLevel.Statics.BoundaryR;
  Statics.BoundaryB := aSrcLevel.Statics.BoundaryB;
  Statics.StyleName := aSrcLevel.Statics.StyleName;
  Statics.VgaspecName := aSrcLevel.Statics.VgaspecName;
  Statics.VgaspecX := aSrcLevel.Statics.VgaspecX;
  Statics.VgaspecY := aSrcLevel.Statics.VgaspecY;

  //InteractiveObjectCollection
  InteractiveObjectCollection.Clear;
  for i := 0 to aSrcLevel.InteractiveObjectCollection.Count-1 do
  begin
    O := InteractiveObjectCollection.Add;
    sO := aSrcLevel.InteractiveObjectCollection[i];
    O.Graph := sO.Graph;
    O.ObjectX := sO.ObjectX;
    O.ObjectY := sO.ObjectY;
    O.ObjectID := sO.ObjectID;
    O.ObjectSValue := sO.ObjectSValue;
    O.ObjectLValue := sO.ObjectLValue;
    O.ObjectDrawingFlags := sO.ObjectDrawingFlags;
    O.ItemActive := sO.ItemActive;
  end;

  //TerrainCollection
  TerrainCollection.Clear;
  for i := 0 to aSrcLevel.TerrainCollection.Count-1 do
  begin
    T := TerrainCollection.Add;
    sT := aSrcLevel.TerrainCollection[i];
    T.Graph := sT.Graph;
    T.TerrainX := sT.TerrainX;
    T.TerrainY := sT.TerrainY;
    T.TerrainID := sT.TerrainID;
    T.TerrainDrawingFlags := sT.TerrainDrawingFlags;
    T.ItemActive := sT.ItemActive;
  end;

  //SteelCollection
  SteelCollection.Clear;
  for i := 0 to aSrcLevel.SteelCollection.Count-1 do
  begin
    S := SteelCollection.Add;
    sS := aSrcLevel.SteelCollection[i];
    S.SteelX := sS.SteelX;
    S.SteelY := sS.SteelY;
    S.SteelWidth := sS.SteelWidth;
    S.SteelHeight := sS.SteelHeight;
    S.SteelType := sS.SteelHeight;
    S.ItemActive := sS.ItemActive;
  end;

  EndUpdate;
end;

procedure TLevelEditor.FillSelectorCollection;
{-------------------------------------------------------------------------------
  zet selectie op entrance (default)
-------------------------------------------------------------------------------}

var
  i: Integer;
  S: TSelector;
begin
(*  if TerrainList.Count > 0 then
  begin
    S := fSelectorCollection.Add;
    S.fTerrain := TerrainList.List^[0];
  end; *)

  if InteractiveObjectCollection.Count > 0 then
  begin
    S := fSelectorCollection.Add;
    S.fRefItem := InteractiveObjectCollection[0];
//    S.fObjectType := sotInteractiveObject;
  //  S.fInteractiveObject := InteractiveObjectCollection[0];
  end;
  (*
  for i := 0 to TerrainList.Count - 1 do
  begin
    S := fSelectorCollection.Add;
    S.fTerrain := TerrainList.List^[i];
  end; *)
end;

function TLevelEditor.GetSelector(Index: Integer): TSelector;
begin
  Result := fSelectorCollection[Index];
end;

function TLevelEditor.GetSelectorCount: Integer;
begin
  Result := fSelectorCollection.Count;
end;

function TLevelEditor.GetSelectedTerrain: TTerrain;
begin
  Result := nil;
  if fSelectorCollection.Count > 0 then
    if fSelectorCollection[0].RefItem is TTerrain then // = sotTerrain then
      Result := fSelectorCollection[0].RefItem as TTerrain;//Terrain;
end;

procedure TLevelEditor.SetSelectedTerrain(const Value: TTerrain);
begin
  if GetSelectedTerrain = Value then
    Exit;
  fSelectorCollection.BeginUpdate;
  try
    fSelectorCollection.Clear;
    with fSelectorCollection.Add do
      fRefItem := Value;
  finally
    fSelectorCollection.EndUpdate;
  end;
//  Mgr.Perform(LM_SELECTIONCHANGED, 0, 0);
end;

function TLevelEditor.GetSelectedObject: TInteractiveObject;
begin
  Result := nil;
  if fSelectorCollection.Count > 0 then
    if fSelectorCollection[0].RefItem is TInteractiveObject then //ObjectType = sotInteractiveObject then
      Result := fSelectorCollection[0].RefItem as TInteractiveObject;//InteractiveObject;
end;

procedure TLevelEditor.SetSelectedObject(const Value: TInteractiveObject);
begin
  if GetSelectedObject = Value then
    Exit;
  fSelectorCollection.BeginUpdate;
  try
    fSelectorCollection.Clear;
    with fSelectorCollection.Add do
      fRefItem := Value;
  finally
    fSelectorCollection.EndUpdate;
  end;
end;

function TLevelEditor.GetSelectedSteel: TSteel;
begin
  Result := nil;
  if fSelectorCollection.Count > 0 then
    if fSelectorCollection[0].RefItem is TSteel then
      Result := fSelectorCollection[0].RefItem as TSteel;
end;

procedure TLevelEditor.SetSelectedSteel(const Value: TSteel);
begin
  if GetSelectedSteel = Value then
    Exit;
  fSelectorCollection.BeginUpdate;
  try
    fSelectorCollection.Clear;
    with fSelectorCollection.Add do
    begin
//      fObjectType := sotInteractiveObject;
  //    fInteractiveObject := Value;
      fRefItem := Value;
//      Log([fRefitem.classname]);
    end;
  finally
    fSelectorCollection.EndUpdate;
  end;
end;

function TLevelEditor.GetSelectedItem: TBaseLevelItem;
begin
  Result := nil;
  if fSelectorCollection.Count > 0 then
    Result := fSelectorCollection[0].RefItem;
end;

procedure TLevelEditor.SetSelectedItem(const Value: TBaseLevelItem);
begin
  if GetSelectedItem = Value then
    Exit;
  fSelectorCollection.BeginUpdate;
  try
    fSelectorCollection.Clear;
    with fSelectorCollection.Add do
      fRefItem := Value;
  finally
    fSelectorCollection.EndUpdate;
  end;
end;

procedure TLevelEditor.AddToHistory;
var
  Dst: TFormatMemoryStream;
  i: Integer;
  Discard: Boolean;

  function CompareIterations(S1, S2: TFormatMemoryStream): Boolean; //returns true if identical
  var
    i: Integer;
    p1, p2: ^Byte;
  begin
    Result := false;
    S1.Position := 0;
    S2.Position := 0;
    if S1.Size <> S2.Size then Exit;
    p1 := S1.Memory;
    p2 := S2.Memory;
    for i := 0 to S1.Size-1 do
    begin
      if p1^ <> p2^ then Exit;
      Inc(p1);
      Inc(p2);
    end;
    Result := true;
  end;
begin
  Discard := false;
  if fRedoing then Exit;

  while fHistory.Count > MAX_UNDO_LEVEL do
  begin
    fHistory.Delete(0);
    Dec(fHistoryPosition);
  end;

  for i := fHistory.Count-1 downto fHistoryPosition+1 do
    fHistory.Delete(i);

  Dst := fHistory.Add;
  try
    Dst.Style := Style;
    Dst.Position := 0;
    SaveToStream(Dst, Dst.LevelFormat);
    if fHistory.Count > 1 then
      if CompareIterations(Dst, fHistory[fHistoryPosition]) then
        Discard := true;
  except
    Discard := true;
  end;

  if Discard and (fHistory.Count > 0) then
    fHistory.Delete(fHistory.Count-1)
  else
    Inc(fHistoryPosition);

end;

procedure TLevelEditor.Undo;
var
  Src: TFormatMemoryStream;
  OldFileName: String;
begin
  if fHistoryPosition < 1 then Exit;
  Dec(fHistoryPosition);
  fRedoing := true;
  Src := fHistory[fHistoryPosition];
  Src.Position := 0;
  LoadFromStream(Src, Src.Style, Src.LevelFormat, false);
  fRedoing := false;
end;

procedure TLevelEditor.Redo;
var
  Src: TFormatMemoryStream;
  OldFileName: String;
begin
  if fHistoryPosition = fHistory.Count-1 then Exit;
  Inc(fHistoryPosition);
  fRedoing := true;
  Src := fHistory[fHistoryPosition];
  Src.Position := 0;
  LoadFromStream(Src, Src.Style, Src.LevelFormat, false);
  fRedoing := false;
end;

procedure TLevelEditor.EndUpdate;
begin
  inherited;
  if fUpdateCount = 0 then AddToHistory;
end;

procedure TLevelEditor.MoveSelection(DeltaX, DeltaY: Integer);
{-------------------------------------------------------------------------------
  Move all the objects in the selection.
  The begin-end update pair generates a LM_LEVELCHANGED message.
-------------------------------------------------------------------------------}
var
  i: Integer;
  It: TSelector;
begin
  if DeltaX = 0 then
    if DeltaY = 0 then
      Exit;
  if SelectorCount = 0 then
    Exit;
  BeginUpdate;
  try
    for i := 0 to SelectorCount - 1 do
    begin
      It := Selectors[i];
      //Assert(Assigned(It.RefItem))
      if Assigned(It.RefItem) then
        It.RefItem.Move(DeltaX, DeltaY);
    end;
  finally
    EndUpdate;
  end;
end;

procedure TLevelEditor.CloneSelection(DeltaX, DeltaY: Integer);
{-------------------------------------------------------------------------------
  Creates new copies of all selected items, offset in position by DeltaX / DeltaY.
-------------------------------------------------------------------------------}
var
  i: Integer;
  It: TSelector;
  InitialCount: Integer;
  O, sO: TInteractiveObject;
  T, sT: TTerrain;
  S, sS: TSteel;
begin
  if DeltaX = 0 then
    if DeltaY = 0 then
      Exit;
  if SelectorCount = 0 then
    Exit;
  InitialCount := SelectorCount;
  BeginUpdate;
  try
    for i := 0 to InitialCount - 1 do
    begin
      It := Selectors[i];
      if Assigned(It.RefItem) then
        case It.RefItem.ItemType of
          litObject: begin
                       O := InteractiveObjectCollection.Add;
                       sO := TInteractiveObject(It.RefItem);
                       O.ObjectX := sO.ObjectX + DeltaX;
                       O.ObjectY := sO.ObjectY + DeltaY;
                       O.Graph := sO.Graph;
                       O.ObjectID := sO.ObjectID;
                       O.ObjectSValue := sO.ObjectSValue;
                       O.ObjectLValue := sO.ObjectLValue;
                       O.ObjectDrawingFlags := sO.ObjectDrawingFlags;
                       O.ItemActive := sO.ItemActive;
                       It.RefItem := O;
                     end;
          litTerrain: begin
                        T := TerrainCollection.Add;
                        sT := TTerrain(It.RefItem);
                        T.TerrainX := sT.TerrainX + DeltaX;
                        T.TerrainY := sT.TerrainY + DeltaY;
                        T.Graph := sT.Graph;
                        T.TerrainID := sT.TerrainID;
                        T.TerrainDrawingFlags := sT.TerrainDrawingFlags;
                        T.ItemActive := sT.ItemActive;
                        It.RefItem := T;
                      end;
          litSteel: begin
                      S := SteelCollection.Add;
                      sS := TSteel(It.RefItem);
                      S.SteelX := sS.SteelX + DeltaX;
                      S.SteelY := sS.SteelY + DeltaY;
                      S.SteelWidth := sS.SteelWidth;
                      S.SteelHeight := sS.SteelHeight;
                      S.SteelType := sS.SteelType;
                      S.ItemActive := sS.ItemActive;
                      It.RefItem := S;
                    end;
        end;
    end;
  finally
    EndUpdate;
  end;
end;


(*
{ TLevelEditorLink }

procedure TLevelEditorLink.LMLevelChanging(var Message);
begin
  LevelChanging;
end;

procedure TLevelEditorLink.LMLevelChanged(var Message);
begin
  LevelChanged;
end;

procedure TLevelEditorLink.LMSelectionChanged(var Message);
begin
  SelectionChanged;
end;

procedure TLevelEditorLink.LMInteractiveObjectChanged(var Message);
begin
  InteractiveObjectChanged;
end;

procedure TLevelEditorLink.LMTerrainChanged(var Message);
begin
  TerrainChanged;
end;

procedure TLevelEditorLink.LevelChanging;
begin

end;

procedure TLevelEditorLink.LevelChanged;
begin
  //
end;

procedure TLevelEditorLink.Receive(var Message);
begin
  if (FDisableCount = 0) and (fSlave <> nil) then
  begin
    //Log([tmessage(message).msg]);
    fReceiving := True;
    try
      Self.Dispatch(Message);
    finally
      fReceiving := False;
    end;
  end;
end;

function TLevelEditorLink.GetEditor: TLevelEditor;
begin
  Result := nil;
  if Manager <> nil then
    Result := TLevelEditor(Manager.Master)
end;

function TLevelEditorLink.GetLevel: TLevel;
begin
end;

procedure TLevelEditorLink.SetEditor(const Value: TLevelEditor);
begin
  if Value = nil then
    Manager := nil
  else
    Manager := Value.Mgr;
end;

function TLevelEditorLink.GetLevelEditor: TLevelEditor;
begin
  Result := TLevelEditor(Master);
end;

procedure TLevelEditorLink.SelectionChanged;
begin
  //
end;

procedure TLevelEditorLink.InteractiveObjectChanged;
begin
  //
end;

procedure TLevelEditorLink.TerrainChanged;
begin
  //
end;
*)

procedure TLevelEditor.MsgSelectionChanged(Item: TSelector);
begin
  Mgr.Perform(LM_SELECTIONCHANGED, Longint(Item), 0);
end;

procedure TLevelEditor.DeleteSelection;
var
  i: Integer;
begin
  if SelectorCount = 0 then
    Exit;
  BeginUpdate;
  try
    for i := 0 to SelectorCollection.Count - 1 do
    begin
      if SelectorCollection[i].RefItem.ItemLocked then Continue;
      SelectorCollection[i].RefItem.Free;
      TerrainCollection.ForceAdd;
      SteelCollection.ForceAdd;
      InteractiveObjectCollection.ForceAdd;
    end;
    SelectorCollection.Clear;
  finally
    EndUpdate;
  end;
end;

procedure TLevelEditor.ExtendSelection(aList: TList; Toggle: Boolean);
var
  O: TObject;
  i: Integer;
  Start: Boolean;
  Found: Integer;
begin
  if aList.Count = 0 then
    Exit;
  Start := False;

  fSelectorCollection.BeginUpdate;

  for i := 0 to aList.Count - 1 do
  begin
    O := aList[i];
    Assert(O is TBaseLevelItem, 'TLevelEditor.ExtendSelection object error');
    Found := fSelectorCollection.FindBaseItem(TBaseLevelItem(O));
    if Found < 0 then
    begin
//      if not Start then
  //      fSelectorCollection.BeginUpdate;
      with fSelectorCollection.Add do
        RefItem := TBaseLevelItem(O);
    end
    else if Toggle then
      fSelectorCollection[Found].Free;
  end;

  fSelectorCollection.EndUpdate;
end;

procedure TLevelEditor.ExtendSelection(Item: TBaseLevelItem; Toggle: Boolean);
var
  L: TList;
begin
  L := TList.Create;
  try
    L.Add(Item);
    ExtendSelection(L, Toggle);
  finally
    L.Free;
  end;
end;

procedure TLevelEditor.ClearLevel(DoClearStyle, DoClearGraph: Boolean);
begin
  inherited ClearLevel(DoClearStyle, DoClearGraph);
  SelectorCollection.Clear;
  FillChar(fObjectFilter, SizeOf(fObjectFilter), Chr(255));
  FillChar(fTerrainFilter, SizeOf(fTerrainFilter), Chr(255));
end;

procedure TLevelEditor.SelectAll(aTypes: TLevelItemTypes = AllItemTypes);
var
  i: Integer;
begin
  SelectorCollection.BeginUpdate;
  SelectorCollection.Clear;

  if litTerrain in aTypes then
    for i := 0 to TerrainCollection.Count - 1 do
      if TerrainCollection[i].ItemActive then
      with fSelectorCollection.Add do
        RefItem := TerrainCollection[i];

  if litObject in aTypes then
    for i := 0 to InteractiveObjectCollection.Count - 1 do
      if InteractiveObjectCollection[i].ItemActive then
      with fSelectorCollection.Add do
        RefItem := InteractiveObjectCollection[i];

  if litSteel in aTypes then
    for i := 0 to SteelCollection.Count - 1 do
      if SteelCollection[i].ItemActive then
      with fSelectorCollection.Add do
        RefItem := SteelCollection[i];

  SelectorCollection.EndUpdate;
end;

procedure TLevelEditor.SelectionToFront;

  procedure MovePieces(aList: TBaseLevelItemCollection);
  var
    i, p: Integer;
    Item: TBaseLevelItem;

    function IsInSelection(aPiece: TBaseLevelItem): Boolean;
    begin
      Result := (fSelectorCollection.FindBaseItem(aPiece) >= 0);
    end;
  begin
    p := 0;
    for i := 0 to aList.Count-1 do
    begin
      Item := aList[i-p];
      if not IsInSelection(Item) then Continue;
      Item.Index := aList.Count-1;
      Inc(p);
    end;
  end;
begin
  MovePieces(InteractiveObjectCollection);
  MovePieces(TerrainCollection);
  MovePieces(SteelCollection);
end;

procedure TLevelEditor.SelectionToBack;

  procedure MovePieces(aList: TBaseLevelItemCollection);
  var
    i, p: Integer;
    Item: TBaseLevelItem;

    function IsInSelection(aPiece: TBaseLevelItem): Boolean;
    begin
      Result := (fSelectorCollection.FindBaseItem(aPiece) >= 0);
    end;
  begin
    p := 0;
    for i := 0 to aList.Count-1 do
    begin
      Item := aList[i];
      if not IsInSelection(Item) then Continue;
      Item.Index := p;
      Inc(p);
    end;
  end;
begin
  MovePieces(InteractiveObjectCollection);
  MovePieces(TerrainCollection);
  MovePieces(SteelCollection);
end;

procedure TLevelEditor.AlignSelection(aHorz: TItemHorzAlign; aVert: TItemVertAlign);
var
  i, X, Y: Integer;
  It: TBaseLevelItem;
  FirstX, FirstY: Boolean;
begin
  if (aHorz = ihaNone) and (aVert = ivaNone) then
    Exit;
  if SelectorCollection.Count < 2 then
    Exit;

  X := 0;
  Y := 0;
  case aHorz of
    ihaNone       :;
    ihaLeftSides  : X := MaxInt;
    ihaRightSides : X := MaxInt;
    ihaCenters    : X := MaxInt;
    ihaTile       : X := MaxInt;//SelectorCollection[0].RefItem.XPosition;
  end;

  case aVert of
    ivaNone       : Y := MaxInt;
    ivaTops       : Y := MaxInt;
    ivaBottoms    : Y := MaxInt;
    ivaCenters    : Y := MaxInt;
    ivaTile       : Y := MaxInt;//electorCollection[0].RefItem.YPosition;
  end;

  FirstX := True;
  FirstY := True;

  // calculate
  with SelectorCollection do
    for i := 0 to Count - 1 do
    begin
      It := Items[i].RefItem;
      { x position }
      case aHorz of
        ihaLeftSides    : X := Min(X, It.XPosition);
        ihaRightSides   : X := Min(X, It.XPosition + It.ItemWidth);
        ihaCenters      : X := Min(X, It.XPosition + It.ItemWidth div 2);
        ihaTile         : X := Min(X, It.XPosition);
      end;
      { y position }
      case aVert of
        ivaTops         : Y := Min(Y, It.YPosition);
        ivaBottoms      : Y := Min(Y, It.YPosition + It.ItemHeight);
        ivaCenters      : Y := Min(Y, It.YPosition + It.ItemHeight div 2);
        ivaTile         : Y := Min(Y, It.YPosition);
      end; // case horz
    end;

  // execute alignments
  BeginUpdate;
  try
    with SelectorCollection do
      for i := 0 to Count - 1 do
      begin
        It := Items[i].RefItem;
        case aHorz of
          ihaLeftSides  : It.XPosition := X;
          ihaRightSides : It.XPosition := X - It.ItemWidth;
          ihaCenters    : It.Xposition := X - It.ItemWidth div 2;
          ihaTile       :
            begin
              It.Xposition := X;
              Inc(X, It.ItemWidth);
            end;
        end;
        case aVert of
          ivaTops       : It.YPosition := Y;
          ivaBottoms    : It.YPosition := Y - It.ItemHeight;
          ivaCenters    : It.Yposition := Y - It.ItemHeight div 2;
          ivaTile       :
            begin
              It.Yposition := Y;
              Inc(Y, It.ItemHeight);
            end;
        end;
      end;
  finally
    EndUpdate;
  end;

//
end;

procedure TLevelEditor.DosAlignSelection;
var
  i: Integer;
  It: TBaseLevelItem;
  S: TSteel;
  O: TInteractiveObject;
begin
  BeginUpdate;
  try
    with SelectorCollection do
      for i := 0 to Count - 1 do
      begin
        It := Items[i].RefItem;
        case It.ItemType of
          litObject:
            begin
              O := TInteractiveObject(It);
              O.ObjectX := O.ObjectX and not 7;
//              O.ObjectY := O.ObjectY and not 3;
            end;
          litSteel:
            begin
              S := TSteel(It);
              S.SteelX := S.SteelX and not 3;
              S.SteelY := S.SteelY and not 3;
              S.SteelWidth := Max(S.SteelWidth and not 3, 4);
              S.SteelHeight := Max(S.SteelHeight and not 3, 4);
            end;
        end;
      end;
  finally
    EndUpdate;
  end;

end;


procedure TLevelEditor.FilterSelection(aRemoveFilter: TLevelItemTypes);
var
  S: TSelector;
  i: Integer;
begin
  SelectorCollection.BeginUpdate;
  try
    with SelectorCollection, HackedList do
    begin
      if Count = 0 then
        Exit;
      for i := Count - 1 downto 0 do
      begin
        S := List^[i];
        if S.RefItem.ItemType in aRemoveFilter then
          S.Free;
      end;
    end;
  finally
    SelectorCollection.EndUpdate;
  end;
end;


procedure TLevelEditor.SetObjectFilter(const Value: TByteSet);
begin
  fObjectFilter := Value;
end;

procedure TLevelEditor.SetTerrainFilter(const Value: TByteSet);
begin
  fTerrainFilter := Value;
end;

procedure TLevelEditor.AutoSteel;
begin
//
end;

function TLevelEditor.GetAsClipBoardString: string;
var
  i: Integer;
  L: TStringList;
  B: TBaseLevelItem;
  S: string;
begin
  Result := '';
  if SelectorCount = 0 then
    Exit;
  L := TStringList.Create;
  try
    L.Add('LCF');

    with SelectorCollection do
      for i := 0 to Count - 1 do
      begin
        S := //i2s(Items[i].RefItem.Index) + '@@@' +
          Items[i].RefItem.AsString;
        //Log([items[i].refitem.classname]);
        L.Add(S);
  //      Log([s]); uevents
      end;
    //loglist(l);
    Result := L.Text;
  finally
    L.Free;
  end;
end;

procedure TLevelEditor.SetAsClipBoardString(const Value: string);


var
  L: TStringList;
  i{, j}: Integer;
  S: string;
  Sel: TList;
  B: TBaseLevelItem;
//  DX, DY: Integer;
//  X, Y: Integer;
//  MinB: TBaseLevelItem;
begin
  BeginUpdate;
  try
    L := TStringList.Create;
    Sel := TList.Create;

    try
      L.Text := Value;

      if L.Count = 0 then
        Exit;

      if L[0] <> 'LCF' then
        Exit;

      L.Delete(0);

//      X := MaxInt;
//      Y := MaxInt;
//      MinB := nil;

      ClearLevel(False, False);
//      for j := 1 to CopyCount do
      begin
//        DX := DeltaX * j;
  //      DY := DeltaY * j;

        for i := 0 to L.Count - 1 do
        begin
          S := L[i];
          if FastPosNoCase(S, SObjectClipBoardID, Length(S), Length(SObjectClipBoardID), 1) = 1 then
          begin
            B := InteractiveObjectCollection.Add;
            B.AsString := S;
            Sel.Add(B);
          end
          else if FastPosNoCase(S, STerrainClipBoardID, Length(S), Length(STerrainClipBoardID), 1) = 1 then
          begin
            B := TerrainCollection.Add;
            B.AsString := S;
            Sel.Add(B);
          end
          else if FastPosNoCase(S, SSteelClipBoardID, Length(S), Length(SSteelClipBoardID), 1) = 1 then
          begin
            B := SteelCollection.Add;
            B.AsString := S;
            Sel.Add(B);
          end;


//          X := Min(X, B.XPosition);
//          Y := Min(Y, B.YPosition);

{          if (MinB = nil)
          or (B.XPosition)X < B}

//          B.XPosition := B.XPosition + DX;
//          B.YPosition := B.YPosition + DY;
        end;

      end; // for i

      SelectorCollection.Clear;
//      ExtendSelection(Sel, False);
      //MoveSelection(DeltaX, DeltaY);
    finally
      L.Free;
      Sel.Free;
    end;


  finally
    EndUpdate;
  end;
end;

procedure TLevelEditor.ShiftLevel(DeltaX: Integer; DeltaY: Integer);
var
  i: integer;
  O: TInteractiveObject;
  T: TTerrain;
  S: TSteel;
begin

  BeginUpdate;

  for i := 0 to (InteractiveObjectCollection.Count - 1) do
  begin
    O := InteractiveObjectCollection[i];
    if O.ItemActive then
    begin
      O.XPosition := O.XPosition + DeltaX;
      O.YPosition := O.YPosition + DeltaY;
    end;
  end;

  for i := 0 to (TerrainCollection.Count - 1) do
  begin
    T := TerrainCollection[i];
    if T.ItemActive then
    begin
      T.XPosition := T.XPosition + DeltaX;
      T.YPosition := T.YPosition + DeltaY;
    end;
  end;

  for i := 0 to (SteelCollection.Count - 1) do
  begin
    S := SteelCollection[i];
    if S.ItemActive then
    begin
      S.XPosition := S.XPosition + DeltaX;
      S.YPosition := S.YPosition + DeltaY;
    end;
  end;

  Statics.VgaspecX := Statics.VgaspecX + DeltaX;
  Statics.VgaspecY := Statics.VgaspecY + DeltaY;

  Statics.ScreenPosition := Statics.ScreenPosition + DeltaX;
  Statics.ScreenYPosition := Statics.ScreenYPosition + DeltaY;

  EndUpdate;

end;

procedure TLevelEditor.CopyToClipBoard;
begin
  fInternalClipboard := GetAsClipboardString;
  ClipBoard.AsText := GetAsClipBoardString;
end;

procedure TLevelEditor.PasteFromClipBoard(DeltaX: Integer = 0; DeltaY: Integer = 0; CopyCount: Integer = 1;
  AbsolutePosition: Boolean = False);
{-------------------------------------------------------------------------------
  Paste from internal clipboard format
-------------------------------------------------------------------------------}


var
  L: TStringList;
  i, j: Integer;
  S: string;
  Sel: TList;
  B: TBaseLevelItem;
  DX, DY: Integer;
  X, Y: Integer;
  MinB: TBaseLevelItem;
begin
  BeginUpdate;
  try
    L := TStringList.Create;
    Sel := TList.Create;

    try
      if (fInternalClipboard = '') or not fUseInternalClipboard then
        L.Text := ClipBoard.AsText
        else
        L.Text := fInternalClipboard;

      if L.Count = 0 then
        Exit;

      L.Delete(0);

      X := MaxInt;
      Y := MaxInt;
      MinB := nil;

      for j := 1 to CopyCount do
      begin
        DX := DeltaX * j;
        DY := DeltaY * j;

        for i := 0 to L.Count - 1 do
        begin
          B := nil;
          S := L[i];
          if FastPosNoCase(S, SObjectClipBoardID, Length(S), Length(SObjectClipBoardID), 1) = 1 then
          begin
            B := InteractiveObjectCollection.Add;
            B.AsString := S;
            B.ItemActive := true;
            Sel.Add(B);
          end
          else if FastPosNoCase(S, STerrainClipBoardID, Length(S), Length(STerrainClipBoardID), 1) = 1 then
          begin
            B := TerrainCollection.Add;
            B.AsString := S;
            B.ItemActive := true;
            Sel.Add(B);
          end
          else if FastPosNoCase(S, SSteelClipBoardID, Length(S), Length(SSteelClipBoardID), 1) = 1 then
          begin
            B := SteelCollection.Add;
            B.AsString := S;
            B.ItemActive := true;
            Sel.Add(B);
          end;

          if B <> nil then
          begin

          X := Min(X, B.XPosition);
          Y := Min(Y, B.YPosition);

{          if (MinB = nil)
          or (B.XPosition)X < B}

          B.XPosition := B.XPosition + DX;
          B.YPosition := B.YPosition + DY;
          end
          else //ShowMessage('paste error B = nil');
            raise Exception.Create('paste error B = nil')
        end;

      end; // for i

      SelectorCollection.Clear;
      ExtendSelection(Sel, False);
      //MoveSelection(DeltaX, DeltaY);
    finally
      L.Free;
      Sel.Free;
    end;


  finally
    EndUpdate;
  end;
end;



procedure TLevelEditor.SelectNextItem(Forwards: Boolean);
var
  i: Integer;
  B: TBaseLevelItem;
begin
//  B := SelectedItem;
  if SelectorCollection.HackedList.Count = 0 then
  begin
    if InteractiveObjectCollection.HackedList.Count > 0 then
      SelectedObject := InteractiveObjectCollection.HackedList.List^[0];
  end
  else begin
    B := SelectorCollection[0].RefItem;
    i := B.Index;
    if i >= 0 then
    begin
      case Forwards of
        True:
          if B.Collection.Count > i + 1 then
            SelectedItem := TBaseLevelItem(B.Collection.Items[i + 1]);
        False:
          if i - 1 >= 0 then
            SelectedItem := TBaseLevelItem(B.Collection.Items[i - 1]);
      end;

    end;
  //  if SelectorCollection.HackedList.Count > i + 1 then
    //   SelectorCollection[i + 1].Index := 0;
  //  if
  end;
end;


procedure TLevelEditor.CheckObjectX(aObject: TInterActiveObject; var X: Integer);
begin
//  if LCF_LOADFROMSTREAM and StateFlags = 0 then
  //  X := X and not 7;
end;

procedure TLevelEditor.SetLoader(aLoader: TObject);
begin
  fLoader := aLoader;
//  if floader <> nil then
  //  deb([floader.classname])
end;

end.


  // er moet PRECIES bekend zijn wat er gebeurd is
  {
    insert
    delete
    prop change
    reload
  }

  (*
  TLevelEditorLink = class(TMessageLink)
  private
    procedure LMLevelChanging(var Message); message LM_LEVELCHANGING;
    procedure LMLevelChanged(var Message); message LM_LEVELCHANGED;
    procedure LMSelectionChanged(var Message); message LM_SELECTIONCHANGED;
    procedure LMInteractiveObjectChanged(var Message); message LM_INTERACTIVEOBJECT_CHANGED;
    procedure LMTerrainChanged(var Message); message LM_TERRAIN_CHANGED;
    function GetEditor: TLevelEditor;
    function GetLevel: TLevel;
    procedure SetEditor(const Value: TLevelEditor);
    function GetLevelEditor: TLevelEditor;
  protected
    procedure Receive(var Message); override;
  public
//    constructor Create(aEditor: TLevelEditor; aSlave: TObject);
    procedure LevelChanging; virtual;
    procedure LevelChanged; virtual;
    procedure SelectionChanged; virtual;
    procedure InteractiveObjectChanged; virtual;
    procedure TerrainChanged; virtual;
    property Editor: TLevelEditor read GetEditor write SetEditor;
    property LevelEditor: TLevelEditor read GetLevelEditor;
  published
  end;
  *)

    (* eevoudiger!

     --> item changed()
     --> collection.update()
     --> event --> message --> link virtual method --> control

     -level              levelchange
        -terrainlist     terrainlistchange
          -terrain       terrainchange
        -objectlist      objectlistchange
          object         objectchange


  *)

