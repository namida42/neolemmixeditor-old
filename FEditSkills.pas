{$include lem_directives.inc}

// orig clientheight 257

unit FEditSkills;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ValEdit, UMisc,
  LemMisc,
  LemStyles, LemDosStyles, LemLemminiStyles,
  LemCore,
  LemEdit,
  LemDialogs,
  FNewLevel,
  FBasicLevelEditor, StdCtrls, Mask, UMasterControls, GR32_Image, ExtCtrls,
  Math,
  ComCtrls;

type
  TFormEditSkills = class(TBasicLevelEditorForm)

    Bevel1: TBevel;

    EditClimberCount    : TEditEx;
    EditFloaterCount    : TEditEx;
    EditBlockerCount    : TEditEx;
    EditBomberCount     : TEditEx;
    EditBuilderCount    : TEditEx;
    EditBasherCount     : TEditEx;
    EditMinerCount      : TEditEx;
    EditDiggerCount     : TEditEx;
    EditClonerCount     : TEditEx;

    EditWalkerCount: TEditEx;
    EditSwimmerCount: TEditEx;
    EditGliderCount: TEditEx;
    EditMechanicCount: TEditEx;
    EditStonerCount: TEditEx;
    EditPlatformerCount: TEditEx;
    EditStackerCount: TEditEx;

    //SkillsetText: TMasterLabel;

    CheckBoxSkWalker: TCheckBox;
    CheckBoxSkClimber: TCheckBox;
    CheckBoxSkSwimmer: TCheckBox;
    CheckBoxSkFloater: TCheckBox;
    CheckBoxSkGlider: TCheckBox;
    CheckBoxSkMechanic: TCheckBox;
    CheckBoxSkBomber: TCheckBox;
    CheckBoxSkStoner: TCheckBox;
    CheckBoxSkBlocker: TCheckBox;
    CheckBoxSkPlatformer: TCheckBox;
    CheckBoxSkBuilder: TCheckBox;
    CheckBoxSkStacker: TCheckBox;
    CheckBoxSkBasher: TCheckBox;
    CheckBoxSkMiner: TCheckBox;
    CheckBoxSkDigger: TCheckBox;
    CheckBoxSkCloner: TCheckBox;

    CheckBoxInfiniteWalker: TCheckBox;
    CheckBoxInfiniteClimber: TCheckBox;
    CheckBoxInfiniteSwimmer: TCheckBox;
    CheckBoxInfiniteFloater: TCheckBox;
    CheckBoxInfiniteGlider: TCheckBox;
    CheckBoxInfiniteMechanic: TCheckBox;
    CheckBoxInfiniteBomber: TCheckBox;
    CheckBoxInfiniteStoner: TCheckBox;
    CheckBoxInfiniteBlocker: TCheckBox;
    CheckBoxInfinitePlatformer: TCheckBox;
    CheckBoxInfiniteBuilder: TCheckBox;
    CheckBoxInfiniteStacker: TCheckBox;
    CheckBoxInfiniteBasher: TCheckBox;
    CheckBoxInfiniteMiner: TCheckBox;
    CheckBoxInfiniteDigger: TCheckBox;
    CheckBoxInfiniteCloner: TCheckBox;
    CheckBoxSkFencer: TCheckBox;
    CheckBoxInfiniteFencer: TCheckBox;
    EditFencerCount: TEditEx;

    procedure FormCreate(Sender: TObject);
    procedure Form_Deactivate(Sender: TObject);
    procedure Edit_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit_Exit(Sender: TObject);
    procedure CheckBoxSkillList_Click(Sender: TObject);
    procedure CheckBoxInfiniteList_Click(Sender: TObject);

    procedure SetEnabledOptions;
  private
    procedure LMLevelChanged(var Msg: TLMLevelChanged); message LM_LEVELCHANGED;
    procedure LMStaticsChanged(var Msg: TLMStaticsChanged); message LM_STATICSCHANGED;
    procedure UpdateInterface;
    procedure UpdateVar(Sender: TEditEx);
  protected
    procedure DoAfterSetEditor(Value: TLevelEditor); override;
  public
  end;

implementation

{$R *.dfm}

const
  tag_climbercount    = 1;
  tag_floatercount    = 2;
  tag_bombercount     = 3;
  tag_blockercount    = 4;
  tag_buildercount    = 5;
  tag_bashercount     = 6;
  tag_minercount      = 7;
  tag_diggercount     = 8;
  tag_walkercount     = 9;
  tag_swimmercount    = 10;
  tag_glidercount     = 11;
  tag_mechaniccount   = 12;
  tag_stonercount     = 13;
  tag_platformercount = 14;
  tag_stackercount    = 15;
  tag_clonercount     = 16;
  tag_fencercount     = 17;


{ TEditStaticsForm }

procedure TFormEditSkills.SetEnabledOptions;
var
  NeoLemmix : boolean;
  SuperLemmini : boolean;
  aParams: TDefaultsRec;
begin

if Editor = nil then exit;

with Editor.Statics do
begin

  // Skill Quantities
  EditWalkerCount.Enabled := CheckBoxSkWalker.Checked and CheckBoxInfiniteWalker.Checked;
  EditClimberCount.Enabled := CheckBoxSkClimber.Checked and CheckBoxInfiniteClimber.Checked;
  EditSwimmerCount.Enabled := CheckBoxSkSwimmer.Checked and CheckBoxInfiniteSwimmer.Checked;
  EditFloaterCount.Enabled := CheckBoxSkFloater.Checked and CheckBoxInfiniteFloater.Checked;
  EditGliderCount.Enabled := CheckBoxSkGlider.Checked and CheckBoxInfiniteGlider.Checked;
  EditMechanicCount.Enabled := CheckBoxSkMechanic.Checked and CheckBoxInfiniteMechanic.Checked;
  EditBomberCount.Enabled := CheckBoxSkBomber.Checked and CheckBoxInfiniteBomber.Checked;
  EditStonerCount.Enabled := CheckBoxSkStoner.Checked and CheckBoxInfiniteStoner.Checked;
  EditBlockerCount.Enabled := CheckBoxSkBlocker.Checked and CheckBoxInfiniteBlocker.Checked;
  EditPlatformerCount.Enabled := CheckBoxSkPlatformer.Checked and CheckBoxInfinitePlatformer.Checked;
  EditBuilderCount.Enabled := CheckBoxSkBuilder.Checked and CheckBoxInfiniteBuilder.Checked;
  EditStackerCount.Enabled := CheckBoxSkStacker.Checked and CheckBoxInfiniteStacker.Checked;
  EditBasherCount.Enabled := CheckBoxSkBasher.Checked and CheckBoxInfiniteBasher.Checked;
  EditMinerCount.Enabled := CheckBoxSkMiner.Checked and CheckBoxInfiniteMiner.Checked;
  EditDiggerCount.Enabled := CheckBoxSkDigger.Checked and CheckBoxInfiniteDigger.Checked;
  EditClonerCount.Enabled := CheckBoxSkCloner.Checked and CheckBoxInfiniteCloner.Checked;
  EditFencerCount.Enabled := CheckBoxSkFencer.Checked and CheckBoxInfiniteFencer.Checked;

  CheckBoxInfiniteWalker.Enabled := CheckBoxSkWalker.Checked;
  CheckBoxInfiniteClimber.Enabled := CheckBoxSkClimber.Checked;
  CheckBoxInfiniteSwimmer.Enabled := CheckBoxSkSwimmer.Checked;
  CheckBoxInfiniteFloater.Enabled := CheckBoxSkFloater.Checked;
  CheckBoxInfiniteGlider.Enabled := CheckBoxSkGlider.Checked;
  CheckBoxInfiniteMechanic.Enabled := CheckBoxSkMechanic.Checked;
  CheckBoxInfiniteBomber.Enabled := CheckBoxSkBomber.Checked;
  CheckBoxInfiniteStoner.Enabled := CheckBoxSkStoner.Checked;
  CheckBoxInfiniteBlocker.Enabled := CheckBoxSkBlocker.Checked;
  CheckBoxInfinitePlatformer.Enabled := CheckBoxSkPlatformer.Checked;
  CheckBoxInfiniteBuilder.Enabled := CheckBoxSkBuilder.Checked;
  CheckBoxInfiniteStacker.Enabled := CheckBoxSkStacker.Checked;
  CheckBoxInfiniteBasher.Enabled := CheckBoxSkBasher.Checked;
  CheckBoxInfiniteMiner.Enabled := CheckBoxSkMiner.Checked;
  CheckBoxInfiniteDigger.Enabled := CheckBoxSkDigger.Checked;
  CheckBoxInfiniteCloner.Enabled := CheckBoxSkCloner.Checked;
  CheckBoxInfiniteFencer.Enabled := CheckBoxSkFencer.Checked;

end;
end;

procedure TFormEditSkills.LMLevelChanged(var Msg: TLMLevelChanged);
begin
  UpdateInterface;
end;

procedure TFormEditSkills.LMStaticsChanged(var Msg: TLMStaticsChanged);
begin
  UpdateInterface;
end;

procedure TFormEditSkills.UpdateInterface;
begin
  if Editor = nil then
    Exit;
  with Editor, Statics do
  begin
    EditClimberCount.Text    := i2s(ClimberCount);
    EditFloaterCount.Text    := i2s(FloaterCount);
    EditBomberCount.Text     := i2s(BomberCount);
    EditBlockerCount.Text    := i2s(BlockerCount);
    EditBuilderCount.Text    := i2s(BuilderCount);
    EditBasherCount.Text     := i2s(BasherCount);
    EditMinerCount.Text      := i2s(MinerCount);
    EditDiggerCount.Text     := i2s(DiggerCount);
    EditWalkerCount.Text     := i2s(WalkerCount);
    EditSwimmerCount.Text    := i2s(SwimmerCount);
    EditGliderCount.Text     := i2s(GliderCount);
    EditMechanicCount.Text   := i2s(MechanicCount);
    EditStonerCount.Text     := i2s(StonerCount);
    EditPlatformerCount.Text := i2s(PlatformerCount);
    EditStackerCount.Text    := i2s(StackerCount);
    EditClonerCount.Text     := i2s(ClonerCount);
    EditFencerCount.Text     := i2s(FencerCount);

    CheckBoxSkFencer.Checked := (SkillTypes and 65536 <> 0);
    CheckBoxSkWalker.Checked := (SkillTypes and 32768 <> 0);
    CheckBoxSkClimber.Checked := (SkillTypes and 16384 <> 0);
    CheckBoxSkSwimmer.Checked := (SkillTypes and 8192 <> 0);
    CheckBoxSkFloater.Checked := (SkillTypes and 4096 <> 0);
    CheckBoxSkGlider.Checked := (SkillTypes and 2048 <> 0);
    CheckBoxSkMechanic.Checked := (SkillTypes and 1024 <> 0);
    CheckBoxSkBomber.Checked := (SkillTypes and 512 <> 0);
    CheckBoxSkStoner.Checked := (SkillTypes and 256 <> 0);
    CheckBoxSkBlocker.Checked := (SkillTypes and 128 <> 0);
    CheckBoxSkPlatformer.Checked := (SkillTypes and 64 <> 0);
    CheckBoxSkBuilder.Checked := (SkillTypes and 32 <> 0);
    CheckBoxSkStacker.Checked := (SkillTypes and 16 <> 0);
    CheckBoxSkBasher.Checked := (SkillTypes and 8 <> 0);
    CheckBoxSkMiner.Checked := (SkillTypes and 4 <> 0);
    CheckBoxSkDigger.Checked := (SkillTypes and 2 <> 0);
    CheckBoxSkCloner.Checked := (SkillTypes and 1 <> 0);

    CheckBoxInfiniteFencer.Checked := (InfiniteSkills and 65536 = 0);
    CheckBoxInfiniteWalker.Checked := (InfiniteSkills and 32768 = 0);
    CheckBoxInfiniteClimber.Checked := (InfiniteSkills and 16384 = 0);
    CheckBoxInfiniteSwimmer.Checked := (InfiniteSkills and 8192 = 0);
    CheckBoxInfiniteFloater.Checked := (InfiniteSkills and 4096 = 0);
    CheckBoxInfiniteGlider.Checked := (InfiniteSkills and 2048 = 0);
    CheckBoxInfiniteMechanic.Checked := (InfiniteSkills and 1024 = 0);
    CheckBoxInfiniteBomber.Checked := (InfiniteSkills and 512 = 0);
    CheckBoxInfiniteStoner.Checked := (InfiniteSkills and 256 = 0);
    CheckBoxInfiniteBlocker.Checked := (InfiniteSkills and 128 = 0);
    CheckBoxInfinitePlatformer.Checked := (InfiniteSkills and 64 = 0);
    CheckBoxInfiniteBuilder.Checked := (InfiniteSkills and 32 = 0);
    CheckBoxInfiniteStacker.Checked := (InfiniteSkills and 16 = 0);
    CheckBoxInfiniteBasher.Checked := (InfiniteSkills and 8 = 0);
    CheckBoxInfiniteMiner.Checked := (InfiniteSkills and 4 = 0);
    CheckBoxInfiniteDigger.Checked := (InfiniteSkills and 2 = 0);
    CheckBoxInfiniteCloner.Checked := (InfiniteSkills and 1 = 0);

    SetEnabledOptions;

    //SkillsetText.Caption := IntToHex(Editor.Statics.SkillTypes, 4);

  end;
end;

procedure TFormEditSkills.UpdateVar(Sender: TEditEx);
var
  S: string;
begin
  if Editor = nil then
    Exit;
  S := Sender.Text;
  with Editor.Statics do
    case Sender.Tag of
      tag_climbercount   : ClimberCount := s2i(S);
      tag_floatercount   : FloaterCount := s2i(S);
      tag_bombercount    : BomberCount := s2i(S);
      tag_blockercount   : BlockerCount := s2i(S);
      tag_buildercount   : BuilderCount := s2i(S);
      tag_bashercount    : BasherCount := s2i(S);
      tag_minercount     : MinerCount := s2i(S);
      tag_diggercount    : DiggerCount := s2i(S);
      tag_walkercount    : WalkerCount := s2i(S);
      tag_swimmercount   : SwimmerCount := s2i(S);
      tag_glidercount    : GliderCount := s2i(S);
      tag_mechaniccount  : MechanicCount := s2i(S);
      tag_stonercount    : StonerCount := s2i(S);
      tag_platformercount: PlatformerCount := s2i(S);
      tag_stackercount   : StackerCount := s2i(S);
      tag_clonercount    : ClonerCount := s2i(S);
      tag_fencercount    : FencerCount := s2i(S);
    end;
  Sender.Modified := False;

end;

procedure TFormEditSkills.Edit_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  E: TEditEx absolute Sender;
begin

  case Key of
    VK_RETURN:
      if Editor <> nil then
        if Sender is TEditEx then
          UpdateVar(E);
    VK_ESCAPE:
      begin
//        if Editor <> nil then
  //        if Sender is TEditEx then
    //        Log(['cancel']);
      end;
  end;

end;

procedure TFormEditSkills.FormCreate(Sender: TObject);
var
  R: TRect;
//  Ani: TLemmingAnimation;
const
  IntChars = ['0'..'9', '-'];
  HexChars = ['0'..'9', 'A'..'F', 'a'..'f'];
begin
  {EditTitle.Tag             := tag_title;
  EditAuthor.Tag            := tag_editauthor;
  EditScreenStart.Tag       := tag_screenstart;
  EditGraphicSet.Tag        := tag_graphicset;
  EditSuperLemming.Tag      := tag_superlemming;
  EditMusic.Tag             := tag_editmusic;

  EditReleaseRate.Tag       := tag_releaserate;
  EditLemmingsCount.Tag     := tag_lemmingscount;
  EditRescueCount.Tag       := tag_rescuecount;
  EditTimeLimit.Tag         := tag_timelimit;
  EditTimeLimitSecs.Tag     := tag_timelimitsecs;}
  EditClimberCount.Tag      := tag_climbercount;
  EditFloaterCount.Tag      := tag_floatercount;
  EditBomberCount.Tag       := tag_bombercount;
  EditBlockerCount.Tag      := tag_blockercount;
  EditBuilderCount.Tag      := tag_buildercount;
  EditBasherCount.Tag       := tag_bashercount;
  EditMinerCount.Tag        := tag_minercount;
  EditDiggerCount.Tag       := tag_diggercount;
  EditWalkerCount.Tag       := tag_walkercount;
  EditSwimmerCount.Tag      := tag_swimmercount;
  EditGliderCount.Tag       := tag_glidercount;
  EditMechanicCount.Tag     := tag_mechaniccount;
  EditStonerCount.Tag       := tag_stonercount;
  EditPlatformerCount.Tag   := tag_platformercount;
  EditStackerCount.Tag      := tag_stackercount;
  EditClonerCount.Tag       := tag_clonercount;
  EditFencerCount.Tag       := tag_fencercount;
  {EditOTRank.Tag            := tag_editotr;
  EditOTLevel.Tag           := tag_editotl;
  EditLevelWidth.Tag        := tag_editwidth;
  EditFallDistance.Tag      := tag_falldist;

  EditBoundaryT.Tag         := tag_boundT;
  EditBoundaryL.Tag         := tag_boundL;
  EditBoundaryR.Tag         := tag_boundR;
  EditBoundaryB.Tag         := tag_boundB;}


//  EditTitle.ValidChars           ;
  {EditScreenStart.ValidChars     := IntChars;
  EditGraphicSet.ValidChars      := IntChars;
  EditReleaseRate.ValidChars     := IntChars;
  EditLemmingsCount.ValidChars   := IntChars;
  EditRescueCount.ValidChars     := IntChars;
  EditTimeLimit.ValidChars       := IntChars;}
  EditClimberCount.ValidChars    := IntChars;
  EditFloaterCount.ValidChars    := IntChars;
  EditBlockerCount.ValidChars    := IntChars;
  EditBomberCount.ValidChars     := IntChars;
  EditBuilderCount.ValidChars    := IntChars;
  EditBasherCount.ValidChars     := IntChars;
  EditMinerCount.ValidChars      := IntChars;
  EditDiggerCount.ValidChars     := IntChars;
  EditWalkerCount.ValidChars     := IntChars;
  EditSwimmerCount.ValidChars    := IntChars;
  EditGliderCount.ValidChars     := IntChars;
  EditMechanicCount.ValidChars   := IntChars;
  EditStonerCount.ValidChars     := IntChars;
  EditPlatformerCount.ValidChars := IntChars;
  EditStackerCount.ValidChars    := IntChars;
  EditClonerCount.ValidChars     := IntChars;
  EditFencerCount.ValidChars     := IntChars;
  {EditSuperLemming.ValidChars    := HexChars;
  EditMusic.ValidChars           := IntChars;
  EditOTRank.ValidChars          := IntChars;
  EditOTLevel.ValidChars         := IntChars;
  EditLevelWidth.ValidChars      := IntChars;
  EditFallDistance.ValidChars    := IntChars;

  EditBoundaryT.ValidChars       := IntChars;
  EditBoundaryL.ValidChars       := IntChars;
  EditBoundaryR.ValidChars       := IntChars;
  EditBoundaryB.ValidChars       := IntChars;}


{  with Image321 do
  begin
    Ani := PictureMgr.WalkerAnimation;
    R := Ani.CalcFrameRect(0);
    bitmap.width := RectWidth(R);
    bitmap.height := RectHeight(R);
    bitmap.Draw(R, R, Ani.Bitmap);
  end;     }

end;

procedure TFormEditSkills.Form_Deactivate(Sender: TObject);
var
  E: TEditEx;
begin
//exit;
  if ActiveControl is TEditEx then
  begin
    E := TEditEx(ActiveControl);
    if Assigned(E.OnExit) then
      E.OnExit(E);
  end;
end;

procedure TFormEditSkills.Edit_Exit(Sender: TObject);
begin
  if TEditEx(Sender).Modified then
    try
      UpdateVar(TEditEx(Sender));
    except
      TEditEx(Sender).SetFocus;
      raise;
    end;
end;

procedure TFormEditSkills.DoAfterSetEditor(Value: TLevelEditor);
begin
  UpdateInterface;
end;

procedure TFormEditSkills.CheckBoxSkillList_Click(Sender: TObject);
begin
  if TCheckBox(Sender).Checked then
    Editor.Statics.SkillTypes := Editor.Statics.SkillTypes or TCheckBox(Sender).Tag
    else
    Editor.Statics.SkillTypes := Editor.Statics.SkillTypes and (not TCheckBox(Sender).Tag);
  //SkillsetText.Caption := IntToHex(Editor.Statics.SkillTypes, 4);
  SetEnabledOptions;
end;

procedure TFormEditSkills.CheckBoxInfiniteList_Click(Sender: TObject);
begin
  if TCheckBox(Sender).Checked then
    Editor.Statics.InfiniteSkills := Editor.Statics.InfiniteSkills and (not TCheckBox(Sender).Tag)
    else
    Editor.Statics.InfiniteSkills := Editor.Statics.InfiniteSkills or TCheckBox(Sender).Tag;
  SetEnabledOptions;
end;

end.
