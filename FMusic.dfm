object FMusicForm: TFMusicForm
  Left = 350
  Top = 298
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Music'
  ClientHeight = 321
  ClientWidth = 217
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 181
    Height = 13
    Caption = 'Select a music track (NeoCustLemmix)'
  end
  object ListBox1: TListBox
    Left = 8
    Top = 24
    Width = 201
    Height = 233
    ItemHeight = 13
    Items.Strings = (
      '<Standard rotation>'
      'Can-Can'
      'Lemming 1 (DOS 1)'
      'Tim 2 (DOS 8)'
      'Lemming 2 (DOS 2)'
      'Tim 8 (DOS 15)'
      'Tim 3 (DOS 13)'
      'Tim 5 (DOS 12)'
      'How Much Is That Doggie'
      'Tim 6 (Dance of the Reed Flutes)'
      'Lemming 3 (DOS 3)'
      'Tim 7 (Rondo Alla Turca)'
      'Tim 9 (London Bridge)'
      'Tim 1 (DOS 7)'
      'Forest Green'
      'Tim 4 (DOS 10)'
      'Ten Green Bottles'
      'Coming Round The Mountain'
      'Oh No! More Lemmings! 1'
      'Oh No! More Lemmings! 2'
      'Oh No! More Lemmings! 3'
      'Oh No! More Lemmings! 4'
      'Oh No! More Lemmings! 5'
      'Oh No! More Lemmings! 6'
      'LPII Frenzy'
      'LPII Gimmick')
    TabOrder = 0
  end
  object Button1: TButton
    Left = 8
    Top = 288
    Width = 97
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 112
    Top = 288
    Width = 97
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object cbSMS: TCheckBox
    Left = 8
    Top = 264
    Width = 201
    Height = 17
    Caption = 'Master System Music Pack'
    TabOrder = 3
    OnClick = cbSMSClick
  end
end
