{$include lem_directives.inc}


unit FProgress;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TProgressForm = class(TForm)
    Txt: TStaticText;
    Memo1: TMemo;
  private
  public
  end;

procedure StartProgress;
procedure EndProgress;
procedure Progress(const S: string);

implementation

{$R *.dfm}

var
  _ProgressForm: TProgressForm;

procedure StartProgress;
begin
  if _ProgressForm = nil then
    _ProgressForm := TProgressForm.Create(Application);
end;

procedure EndProgress;
begin
  FreeAndNil(_ProgressForm);
end;

procedure Progress(const S: string);
begin
  if _ProgressForm <> nil then
  with _ProgressForm do
  begin
    Memo1.Lines.Add(S);
    Update;
  end;
end;

end.

