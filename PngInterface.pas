unit PngInterface;

// Quick and shitty interface between TPngObject and TBitmap32.
// It does fix the differing color orders (TPngObject uses ABGR,
// TBitmap32 uses ARGB).
//
// TPngObjects or TBitmap32s are created by the functions. Don't
// pre-create them.
//
// LoadPngFile(<filename>) - Loads a PNG image and returns a TBitmap32
// SavePngFile(<filename>, <TBitmap32>) - Saves a TBitmap32 as a PNG file
// PngToBitmap32(<TPngObject>) - Creates a TBitmap32 from a TPngObject
// Bitmap32ToPng(<TBitmap32>) - Creates a TPngObject from a TBitmap32
// LeadZeroStr(<string>, <min length>) - Obvious. Only in here to avoid putting it everywhere or
//                                       creating a unit for a single function; everything that needs
//                                       it also uses PngInterface anyway.

interface

uses
  Classes, SysUtils, GR32, PngImage, GIFImage;

  function LoadGifFile(fn: String): TBitmap32;
  function LoadPngFile(fn: String): TBitmap32;
  procedure SavePngFile(fn: String; Bmp: TBitmap32);
  function PngToBitmap32(Png: TPngObject): TBitmap32;
  function Bitmap32ToPng(Bmp: TBitmap32): TPngObject;
  function LeadZeroStr(aValue: Integer; aLen: Integer): String;

implementation

function LoadGifFile(fn: String): TBitmap32;
var
  TempGif: TGifImage;
  x, y: Integer;
  b: Byte;
begin
  TempGif := TGifImage.Create;
  Result := TBitmap32.Create;
  TempGif.LoadFromFile(fn);
  Result.SetSize(TempGif.Width, TempGif.Height);
  for y := 0 to Result.Height-1 do
    for x := 0 to Result.Width-1 do
    begin
      b := TempGif.Images[0].Pixels[x, y];
      if b <> TempGif.BackgroundColorIndex then
      begin
        Result.Pixel[x, y] := TempGif.GlobalColorMap.Colors[b];
        Result.Pixel[x, y] := ((Result.Pixel[x, y] and $FF0000) shr 16)
                            + (Result.Pixel[x, y] and $FF00)
                            + ((Result.Pixel[x, y] and $FF) shl 16)
                            + $FF000000;
      end else
        Result.Pixel[x, y] := 0;
    end;
end;

function LoadPngFile(fn: String): TBitmap32;
var
  TempPng: TPngObject;
begin
  TempPng := TPngObject.Create;
  TempPng.LoadFromFile(fn);
  Result := PngToBitmap32(TempPng);
  TempPng.Free;
end;

procedure SavePngFile(fn: String; Bmp: TBitmap32);
var
  TempPng: TPngObject;
begin
  TempPng := Bitmap32ToPng(Bmp);
  TempPng.SaveToFile(fn);
  TempPng.Free;
end;

function PngToBitmap32(Png: TPngObject): TBitmap32;
var
  x, y: Integer;
  c: TColor32;
  r, g, b, a: Byte;
  ASL: pByteArray;
  Alpha: Boolean;
  TCol: Byte;
  XC: pByte;
  TRNS: TCHUNKtRNS;
begin
  ASL := nil; // Just gets rid of a compile-time warning. ASL won't be referenced if it hasn't been initialized anyway, since
              // the only line that references it has the same IF condition as the line that initializes it.
  Result := TBitmap32.Create;
  Result.SetSize(Png.Width, Png.Height);
  Alpha := (Png.AlphaScanline[0] <> nil);
  for y := 0 to Png.Height-1 do
  begin
    if Alpha then
      ASL := Png.AlphaScanline[y];
    for x := 0 to Png.Width-1 do
    begin
      c := Png.Pixels[x, y];
      r := c and $FF;
      g := (c and $FF00) shr 8;
      b := (c and $FF0000) shr 16;
      if Alpha then
        a := ASL^[x]
      else
        a := 255;
      if a = 0 then
        Result.Pixel[x, y] := 0
      else
        Result.Pixel[x, y] := (a shl 24)
                            + (r shl 16)
                            + (g shl 8)
                            + b;
    end;
  end;

  // handle 8 bit PNG files - fuck the 1/2/4 bit ones
  if (PNG.TransparencyMode = ptmBit) and (PNG.Header.BitDepth = 8) then
  begin
    if (Png.Header.ColorType = COLOR_PALETTE)  and (Png.Chunks.ItemFromClass(TChunktRNS) = nil) then
      Png.CreateAlpha;
    TRNS := Png.Chunks.ItemFromClass(TChunktRNS) as TChunktRNS;
    for y := 0 to PNG.Height-1 do
    begin
      XC := Png.Scanline[y];
      for x := 0 to PNG.Width-1 do
      begin
        if XC^ = TRNS.TransparentColor then
          Result.Pixel[x, y] := 0
        else
          Result.Pixel[x, y] := $FF000000 or (Result.Pixel[x, y] and $FFFFFF);
        Inc(XC);
      end;
    end;
  end;
end;

function Bitmap32ToPng(Bmp: TBitmap32): TPngObject;
var
  x, y: Integer;
  r, g, b, a: Byte;
  c: TColor32;
  ASL: pByteArray;
begin
  Result := TPngObject.CreateBlank(COLOR_RGBALPHA, 8, Bmp.Width, Bmp.Height);
  for y := 0 to Bmp.Height-1 do
  begin
    ASL := Result.AlphaScanline[y];
    for x := 0 to Bmp.Width-1 do
    begin
      c := Bmp.Pixel[x, y];
      r := (c and $FF0000) shr 16;
      g := (c and $FF00) shr 8;
      b := c and $FF;
      a := (c and $FF000000) shr 24;
      Result.Pixels[x, y] := (b shl 16)
                           + (g shl 8)
                           + r;
      ASL^[x] := a;
    end;
  end;
end;

function LeadZeroStr(aValue: Integer; aLen: Integer): String;
begin
  Result := IntToStr(aValue);
  while Length(Result) < aLen do
    Result := '0' + Result;
end;

end.