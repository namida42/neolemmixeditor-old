{$include lem_directives.inc}

program NeoLemmixEditor;

uses
  LemCore,
  Forms,
  LemStartUp;

{$R *.res}
{$R EditorStuff.res}

begin
  Application.Initialize;
  Application.ShowHint := True;
  InitializeCore;
  StartupEditor;
  Application.Title := 'NeoLemmix Editor';
  Application.Run;
  FinalizeCore;
end.

