{$include lem_directives.inc}

unit LemStrings;

interface

uses
  UMisc,
  LemTypes;

{-------------------------------------------------------------------------------
  system hard string consts
-------------------------------------------------------------------------------}
const
  SExeNameStr = 'NeoLemmix Editor';
  SVersion = 'V10.13.13:2a1543e';
  SAboutStr = 'NeoLemmix Editor ' + SVersion + CrLf +
              '   by Eric Langedijk (EricLang) and Namida Verasche (namida)';
  SThanksStr = {$ifdef exp}'EXPERIMENTAL VERSION!' + CrLf +{$endif}
               'Thanks to...' + CrLf +
               'DMA for creating the original lemmings game.' + CrLf +
               'ccexplore for game-mechanics and testing.' + CrLf +
               'Alex A. Denisov and contributors for Graphics32 units.' + CrLf +
               'Mike Lischke for the GraphicEx unit.' + CrLf +
               'Erik Turner for the ZLibEx unit.' + CrLf +
               'Peter Morris for the FastStrings unit.' + CrLf +
               'The Lemmings Forums' + CrLf + CrLf +
               'Volker Oth, ccexplore and Mindless for sharing sourcecode ' + CrLf +
               'and technical information about lemmings: without them Lemmix would never have existed.';

const
  (*
  SDirt      = 'dirt';
  SFire      = 'fire';
  SMarble    = 'marble';
  SPillar    = 'pillar';
  SCrystal   = 'crystal';
  SBrick     = 'brick';
  SRock      = 'rock';
  SSnow      = 'snow';
  SBubble    = 'bubble';
  SXMas      = 'snow';

  SSpecial   = 'special';
  *)

{-------------------------------------------------------------------------------
  consts for directories and file masks
-------------------------------------------------------------------------------}
  SLowResChar  = 's';           // used in bitmap archive
  SHighResChar = 'b';           // used in bitmap archive
  SObjectChar  = 'o';           // used in bitmap archive
  SArcStyleExt = '.lsf';        // extension for bitmap archive (almost obsolete)
  SCompiledGraphExt = '.lga';   // extension for Lemmix Graphic Archive
  SCompiledSysExt = '.lsa';
  STerrainPrefix = 'ter_';
  SObjectPrefix = 'obj_';
  SCompiledGraphBin = 'graph.bin';
  SCompiledGraphTxt = 'graph.txt';

  SResolutionChars: array[TResolution] of Char = (
    SLowResChar,
    SHighResChar
  );

  (*
  SStyleIds: array[0..9] of string = (
    SDirt,
    SFire,
    SMarble,
    SPillar,
    SCrystal,
    SBrick,
    SRock,
    SSnow,
    SBubble,
    SXMas
  );
  *)


//  SStyleTerrainMask := StyleName + '\' + StyleName + SLowResChar + SObjectChar + '_' + Id + '_000.bmp';

{-------------------------------------------------------------------------------
  constants for internal copy paste as text
-------------------------------------------------------------------------------}
  SObjectClipBoardID = 'obj';
  STerrainClipBoardID = 'ter';
  SSteelClipBoardID = 'ste';

{-------------------------------------------------------------------------------
  constants for new levels
-------------------------------------------------------------------------------}
  SNewDirtLevel = 'LCF' + CrLf +
                  'obj=0,1600,176,4' + CrLf +
                  'obj=7,1600,160,0' + CrLf +
                  'obj=1,1508,138,4' + CrLf +
                  'ter=10,1501,186,0';


  SAthlete = 'Athlete';                
  SLemmingActionStrings: array[TBasicLemmingAction] of string = (
    'Walker',
    'Jumper',
    'Digger',
    'Climber',
    'Drowner',
    'Hoister',
    'Builder',
    'Basher',
    'Miner',
    'Faller',
    'Floater',
    'Splatter',
    'Exiter',
    'Vaporizer',
    'Blocker',
    'Shrugger',
    'Ohnoer',
    'Exploder'
  );


{-------------------------------------------------------------------------------
  Translatable strings
-------------------------------------------------------------------------------}

resourcestring
  // default graphic set strings
{SGraphicSetDirt                           = 'Dirt';
  SGraphicSetFire                           = 'Fire';
  SGraphicSetMarble                         = 'Marble';
  SGraphicSetPillar                         = 'Pillar';
  SGraphicSetCrystal                        = 'Crystal';
  SGraphicSetBrick                          = 'Brick';
  SGraphicSetRock                           = 'Rock';
  SGraphicSetSnow                           = 'Snow';
  SGraphicSetBubble                         = 'Bubble'; }

  // strings of objects graphic set 0 (Dirt)
	SObject_G00_Exit                          = 'Exit';
  SObject_G00_Start           	            = 'Start';
  SObject_G00_WavingGreenFlag	              = 'Waving green flag';
  SObject_G00_OneWayBlockLeft               = 'One-way block left';
  SObject_G00_OneWayBlockRight              = 'One-way block right';
  SObject_G00_Water               	        = 'Water';
  SObject_G00_BearTrap          	          = 'Bear trap';
  SObject_G00_ExitDecorationFlames          = 'Exit flames';
  SObject_G00_RockSquishingTrap             = 'Rock squishing trap';
  SObject_G00_WavingBlueFlag           	    = 'Waving blue flag';
  SObject_G00_10_TonSquishingTrap           = '10 ton squishing trap';

  // strings of objects graphic set 1(Fire)
	SObject_G01_Exit                          = 'Exit';
	SObject_G01_Start                         = 'Start';
	SObject_G01_WavingGreenFlag               = 'Waving green flag';
	SObject_G01_OneWayBlockLeft               = 'One-way block left';
	SObject_G01_OneWayBlockRight              = 'One-way block right';
	SObject_G01_RedLava                       = 'Red lava';
	SObject_G01_ExitFlames                    = 'Exit decoration, flames';
	SObject_G01_FirePitTrap                   = 'Fire pit trap';
	SObject_G01_FireShooterTrapFromLeft       = 'Fire shooter trap from left';
	SObject_G01_WavingBlueFlag                = 'Waving blue flag';
	SObject_G01_FireShooterTrapFromRight      = 'Fire shooter trap from right';

  // strings of objects graphic set 2 (Marble)
  SObject_G02_Exit                          = 'Exit';
  SObject_G02_Start                         = 'Start';
  SObject_G02_WavingGreenFlag               = 'Waving green flag';
  SObject_G02_OneWayBLockLeft               = 'One-way block left';
  SObject_G02_OneWayBlockRight              = 'One-way block right';
  SObject_G02_GreenLiquid                   = 'Green liquid';
  SObject_G02_ExitDecorationFlames          = 'Exit flames';
  SObject_G02_WavingBlueFlag                = 'Waving blue flag';
  SObject_G02_PillarSquishingTrap           = 'Pillar squishing trap';
  SObject_G02_SpinningDeathTrap             = 'Spinning death trap';

  // strings of objects graphic set 3 (Pillar)
  SObject_G03_Exit                          = 'Exit';
  SObject_G03_Start                         = 'Start';
  SObject_G03_WavingGreenFlag               = 'Waving green flag';
  SObject_G03_OneWayBlockLeft               = 'One-way block pointing left';
  SObject_G03_OneWwayBlockRight             = 'One-way block pointing right';
  SObject_G03_Water                         = 'Gater';
  SObject_G03_ExitFlames                    = 'Exit Flames';
  SObject_G03_WavingBlueFlag                = 'Waving blue flag';
  SObject_G03_SpinnyRopeTrap                = 'Pinny rope trap';
  SObject_G03_SpikesFromLeftTrap            = 'Spikes from left trap';
  SObject_G03_SpikesFromRightTrap           = 'Spikes from right trap';

  SCaptionReleaseRate = 'ReleaseRate';
  SCaptionBuilders = 'Builders';

{-------------------------------------------------------------------------------
  exceptions.
  _d = format integer
  _s = format string
  changing the name when format-params is the safest way,
  because we have a compiler check then
-------------------------------------------------------------------------------}
  SErrorReleaseRate_d                       = 'Invalid release rate (%d)';//:#13 value must lie between (%d) and (%d)';
  SErrorLemmingsCount_d                     = 'Invalid lemmings count (%d)';
  SErrorRescueCount_d                       = 'Invalid rescue count (%d)';
  SErrorTimeLimit_d                         = 'Invalid time limit (%d)';
  SErrorClimberCount_d                      = 'Invalid climber count (%d)';
  SErrorFloaterCount_d                      = 'Invalid floater count (%d)';
  SErrorBomberCount_d                       = 'Invalid bomber count (%d)';
  SErrorBlockerCount_d                      = 'Invalid blocker count (%d)';
  SErrorBuilderCount_d                      = 'Invalid builder count (%d)';
  SErrorBasherCount_d                       = 'Invalid basher count (%d)';
  SErrorMinerCount_d                        = 'Invalid miner count (%d)';
  SErrorDiggerCount_d                       = 'Invalid digger count (%d)';
  SErrorSkillCount_d                        = 'Invalid skill count (%d)';

  SErrorMusic_d                             = 'Invalid music track (%d)';
  SErrorGraphicSet_d                        = 'Invalid graphic set (%d)';

  SErrorSValue_d                            = 'Invalid S Value (%d)';
  SErrorLValue_d                            = 'Invalid L Value (%d)';

//  SErrorLemmini


  SVgaSpecDimension_dd            = 'The width and/or height of this bitmap are invalid (%d x %d).' + CrLf +
                                    'Only bitmaps of 960 x 160 pixels are valid ' +
                                    'when creating a Dos extended graphic bitmap';
  SVgaSpecTooMuchColors_d         = 'This bitmap has two many colors (%d).' + CrLf +
                                    'The maximum allowed number of colors is 7 ' +
                                    'when creating a Dos extended graphic bitmap';


implementation

end.

