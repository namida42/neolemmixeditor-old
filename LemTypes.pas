{$include lem_directives.inc}

unit LemTypes;

interface

uses
  Classes;

const
  // dos interactive object object id's
//  dos_oid_exit
  OID_EXIT                  = 0;
  OID_ENTRY                 = 1;

  // dos interactive object animation flags
  AFD_QUIET                 = 0;
  AFD_TRIGGERED             = 1; // animation triggered by lemming
  AFD_CONTINUOUSLY          = 2;
  AFD_ONCE                  = 3; // entry

  // dos interactive object trigger effect
  TED_NONE                  = 0;
  TED_EXIT                  = 1;
  TED_TRIGGEREDTRAP         = 4;
  TED_DROWN                 = 5;
  TED_DESINTEGRATION        = 6;
  TED_ONEWAY_LEFT           = 7;
  TED_ONEWAY_RIGHT          = 8;
  TED_STEEL                 = 9;


  // lemmix interactive object animation flags
  AFX_QUIET                     = 0; // as dos
  AFX_TRIGGERED                 = 1; // as dos
  AFX_CONTINUOUSLY              = 2; // as dos
  AFX_ONCE                      = 3; // as dos

  // lemmix interactive object trigger effect
  TEX_NONE                      = 0; // as dos
  TEX_EXIT                      = 1; // as dos
  TEX_TRIGGEREDTRAP             = 4; // as dos
  TEX_DROWN                     = 5; // as dos
  TEX_DESINTEGRATION            = 6; // as dos
  TEX_ONEWAY_LEFT               = 7; // as dos
  TEX_ONEWAY_RIGHT              = 8; // as dos
  TEX_STEEL                     = 9; // as dos

  // values for the (4 pixel resolution) Dos Object Map (for triggereffects)
  DOM_NONE             = 128 + 0;
  DOM_EXIT             = 128 + 1;
  DOM_FORCELEFT        = 128 + 2; // left arm of blocker
  DOM_FORCERIGHT       = 128 + 3; // right arm of blocker
//  DOM_TRAP             = 128 + 4; // triggered trap
  DOM_WATER            = 128 + 5; // causes drowning
  DOM_FIRE             = 128 + 6; // causes vaporizing
  DOM_ONEWAYLEFT       = 128 + 7;
  DOM_ONEWAYRIGHT      = 128 + 8;
  DOM_STEEL            = 128 + 9;
  DOM_BLOCKER          = 128 + 10; // the middle part of blocker

type
  DOM_TRAPRANGE = 0..127; // used for triggered traps

const
//  clBRICK = $00000001; // color32 code for bricks, we (could) use the alpha channel for this
  clBrick32 = {FF}$00FF00FF;
  clMask32  = $00FF00FF;

  // lemming animation type
  LEM_ANIMATIONTYPE_ALWAYSLOOP                = 0; // always repeat
  LEM_ANIMATIONTYPE_ONCE                      = 1; // one loop
  LEM_ANIMATION_COUNTLOOP                     = 2; // more loops
  LEM_ANIMATION_CUSTOM                        = 3; // different



type
  TLemmingFileFormat = (
    lffAutoDetect,              // Autodetect fileformat
    lffLVL,                     // WinLemmings format
    lffLemmini                  // Lemmini format
  );


  TLemSystem = (
    lsNoSystem,
    lsDosOrig,
    lsDosOhNo,
    lsDosChristmas91,
    lsDosChristmas92,
    lsDosHoliday93,
    lsDosHoliday94,
    lsReserved7,
    lsReserved8,
    lsLemmini,
    lsReserved10,
    lsReserved11,
    lsReserved12,
    lsReserved13,
    lsReserved14,
    lsReserved15
  );

  // tresolution is obsolete
  TResolution = (
    _LoRes,
    _HiRes
  );

  TBasicLemmingAction = (
//    baNone,
    baWalking,
    baJumping,
    baDigging,      //assignable
    baClimbing,     //assignable
    baDrowning,
    baHoisting,
    baBuilding,     //assignable
    baBashing,      //assignable
    baMining,       //assignable
    baFalling,
    baFloating,     //assignable
    baSplatting,
    baExiting,
    baVaporizing,
    baBlocking,     //assignable
    baShrugging,
    baOhnoing,
    baExplosion     //assignable
  );


const
//  all one higher then the basic lemming action (shit programming)
    bla_None = 0;
    bla_Walking = 1;
    bla_Jumping = 2;
    bla_Digging = 3;      //assignable
    bla_Climbing =4;    //assignable
    bla_Drowning =5;
    bla_Hoisting =6;
    bla_Building =7;    //assignable
    bla_Bashing =8;     //assignable
    bla_Mining =9;      //assignable
    bla_Falling = 10;
    bla_Floating = 11;    //assignable
    bla_Splatting = 12;
    bla_Exiting = 13;
    bla_Vaporizing = 14;
    bla_Blocking =15;    //assignable
    bla_Shrugging =16;
    bla_Ohnoing =17;
    bla_Explosion =18;    //assignable


//  TAssignableSkill

       (*
  TBasicLemmingJob = (
    bjClimb,
    bjFloat,
    bjExplode,
    bjBlock,
    bjBuild,
    bjBash,
    bjMine,
    bjDig
  ); *)

(*type
  TAcquiredSkill = bjClimb.. bjFloat;
{    asClimb,
    asFloat
  ); }
  TAcquiredSkills = set of TAcquiredSkill; *)


  // the dosbuttons
type
  TButtonSkill = (
    bskSlower,
    bskFaster,
    bskClimber,
    bskUmbrella,
    bskExplode,
    bskStopper,
    bskBuilder,
    bskBasher,
    bskMiner,
    bskDigger,
    bskPause,
    bskNuke
  );

  // all one higher then the button skills
const
    bsk_None = 0;
    bsk_Slower = 1;
    bsk_Faster =2;
    bsk_Climber = 3;
    bsk_Umbrella =4;
    bsk_Explode =5;
    bsk_Stopper =6;
    bsk_Builder =7;
    bsk_Basher = 8;
    bsk_Miner =9;
    bsk_Digger = 10;
    bsk_Pause = 11;
    bsk_Nuke = 12;



{  TInfoType = (
    itCursor,
    itOut,
    itIn,
    itTime
  ); }

{-------------------------------------------------------------------------------
  dos animations ordered by their appearance in main.dat
  the constants below show the exact order
-------------------------------------------------------------------------------}
const
  WALKING             = 0;
  JUMPING             = 1;
  WALKING_RTL         = 2;
  JUMPING_RTL         = 3;
  DIGGING             = 4;
  CLIMBING            = 5;
  CLIMBING_RTL        = 6;
  DROWNING            = 7;
  HOISTING            = 8;
  HOISTING_RTL        = 9;
  BRICKLAYING         = 10;
  BRICKLAYING_RTL     = 11;
  BASHING             = 12;
  BASHING_RTL         = 13;
  MINING              = 14;
  MINING_RTL          = 15;
  FALLING             = 16;
  FALLING_RTL         = 17;
//  PREUMBRELLA         = 18;
  UMBRELLA            = 18;
//  PREUMBRELLA_RTL     = 20;
  UMBRELLA_RTL        = 19;
  SPLATTING           = 20;
  EXITING             = 21;
  FRIED               = 22;
  BLOCKING            = 23;
  SHRUGGING           = 24;
  SHRUGGING_RTL       = 25;
  OHNOING             = 26;
  EXPLOSION           = 27;

  RTLS = [
    WALKING_RTL,
    JUMPING_RTL,
    CLIMBING_RTL,
    HOISTING_RTL,
    BRICKLAYING_RTL,
    BASHING_RTL,
    MINING_RTL,
    FALLING_RTL,
//  PREUMBRELLA_RTL,
    UMBRELLA_RTL,
    SHRUGGING_RTL
  ];

  AssignableButtonSkills = [
    bskClimber,
    bskUmbrella,
    bskExplode,
    bskStopper,
    bskBuilder,
    bskBasher,
    bskMiner,
    bskDigger
  ];


  // dos game mechanics constants
  mcMaxLemmingStep                     = 6;
  mcRequiredDepthForTransitionToFaller = 6;//3

  mcFallDepthPerFrame                  = 3;
  mcFloatDepthPerFrame                 = 1;
  mcFallCountBeforeTransitionToFloater = 20; //;16
  mcMaxFallDistanceCount                = 60;
  mcInitialExplosionTimerValue         = 79;

  HEAD_MIN_Y = -5;
  LEMMING_MIN_X = 0;
  LEMMING_MAX_X = 1647;
  LEMMING_MAX_Y = 163;



const
  ButtonSkillToAction: array[TButtonSkill] of TBasicLemmingAction = (
    baWalking, // error
    baWalking, // error
    baClimbing,
    baFloating,
    baExplosion,
    baBlocking,
    baBuilding,
    baBashing,
    baMining,
    baDigging,
    baWalking, // error
    baWalking // error
  );


  ActionToButtonSkill: array[TBasicLemmingAction] of TButtonSkill = (
        bskClimber,
        bskClimber,
    bskDigger,     //assignable
    bskClimber,     //assignable
        bskClimber,
        bskClimber,
    bskBuilder,     //assignable
    bskBasher,      //assignable
    bskMiner,       //assignable
        bskClimber,
    bskUmbrella,     //assignable
        bskClimber,
        bskClimber,
        bskClimber,
    bskStopper,     //assignable
        bskClimber,
        bskClimber,
    bskExplode     //assignable
  );

{  HighlightableSkills = [
    bskClimber,
    bskUmbrella,
    bskExplode,
    bskStopper,
    bskBuilder,
    bskBasher,
    bskMiner,
    bskDigger
  ]; }


const
  LTR = False;
  RTL = True;

const
  AnimationIndices : array[TBasicLemmingAction, LTR..RTL] of Integer = (
    (WALKING, WALKING_RTL),                   // baWalk,
    (JUMPING, JUMPING_RTL),                   // baJumping,
    (DIGGING, DIGGING),                       // baDigging,
    (CLIMBING, CLIMBING_RTL),                 // baClimbing,
    (DROWNING, DROWNING),                     // baDrowning,
    (HOISTING, HOISTING_RTL),                 // baHoisting,
    (BRICKLAYING, BRICKLAYING_RTL),           // baBricklaying,
    (BASHING, BASHING_RTL),                   // baBashing,
    (MINING, MINING_RTL),                      // baMining,
    (FALLING, FALLING_RTL),                   // baFalling,
//  (PREUMBRELLA, PREUMBRELLA_RTL),           // baPreumbrella,
    (UMBRELLA, UMBRELLA_RTL),                 // baUmbrella,
    (SPLATTING, SPLATTING),                   // baSplatting,
    (EXITING, EXITING),                       // baExiting,
    (FRIED, FRIED),                           // baFried,
    (BLOCKING, BLOCKING),                     // baBlocking,
    (SHRUGGING, SHRUGGING_RTL),               // baShrugging,
    (OHNOING, OHNOING),                       // baOhnoing,
    (EXPLOSION, EXPLOSION)                    // baExplosion

  );

const
  AssignableSkills = [
    baDigging,      //assignable
    baClimbing,     //assignable
    baBuilding,     //assignable
    baBashing,      //assignable
    baMining,       //assignable
    baFloating,     //assignable
    baBlocking,     //assignable
    baExplosion     //assignable
  ];  



(*
const
  // constants for basic lemming actions
  JOB_WALKING         = 0;
  JOB_CLIMBING        = 1;
  JOB_FALLING         = 2;
  JOB_FLOATING        = 3;
  JOB_EXPLODING       = 4;
  JOB_BLOCKING        = 5;
  JOB_BUILDING        = 6;
  JOB_BASHING         = 7;
  JOB_MINING          = 8;
  JOB_DIGGING         = 9;

  SKILL_CLIMB         = Bit0;
  SKILL_FLOAT         = Bit1;
  *)


{-------------------------------------------------------------------------------
  The next variables are the indices in the StyleList of the StyleMgr.
  This is only done for the default supported styles.
-------------------------------------------------------------------------------}
var
  STYLE_DOSORIG         : Integer = -1;
  STYLE_DOSOHNO         : Integer = -1;
  STYLE_DOSCHRISTMAS91  : Integer = -1;
  STYLE_DOSCHRISTMAS92  : Integer = -1;
  STYLE_DOSHOLIDAY93    : Integer = -1;
  STYLE_DOSHOLIDAY94    : Integer = -1;
  STYLE_WINLORES        : Integer = -1;
  STYLE_WINHIRES        : Integer = -1;
  STYLE_LEMMINI         : Integer = -1;

  STYLE_CUSTLEMM        : Integer = -1;





const
  MaxBytes = MaxListSize * 4;

type
  PBytes = ^TBytes;
  TBytes = array[0..MaxBytes - 1] of Byte;

implementation

end.

