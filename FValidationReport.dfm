object FormValidationReport: TFormValidationReport
  Left = 148
  Top = 190
  Width = 593
  Height = 332
  Caption = 'Level errors'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 237
    Height = 13
    Caption = 'The following problems were detected in this level:'
  end
  object BtnOK: TButton
    Left = 496
    Top = 256
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object lbLevelErrors: TListBox
    Left = 8
    Top = 24
    Width = 561
    Height = 225
    ItemHeight = 13
    TabOrder = 1
  end
  object btnFix: TButton
    Left = 8
    Top = 256
    Width = 73
    Height = 25
    Caption = 'Fix Issue'
    TabOrder = 2
    OnClick = btnFixClick
  end
  object Button1: TButton
    Left = 88
    Top = 256
    Width = 73
    Height = 25
    Caption = 'Fix All'
    TabOrder = 3
    OnClick = Button1Click
  end
end
