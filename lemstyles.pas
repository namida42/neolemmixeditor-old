{$include lem_directives.inc}

unit LemStyles;

{-------------------------------------------------------------------------------

-------------------------------------------------------------------------------}

interface

uses
  Windows, Classes, IniFiles, SysUtils,
  UMisc, GR32, Graphics, GraphicEx,

  LemDosStyles, LemLemminiStyles,

  LemDosFiles, LemFiles,
  LemTypes, LemCore;

type
  TStyleConverter = class
  private
    fDestStyle: TBaseLemmingStyle;
    fSourceStyle: TBaseLemmingStyle;
  protected
  public
    property SourceStyle: TBaseLemmingStyle read fSourceStyle write fSourceStyle;
    property DestStyle: TBaseLemmingStyle read fDestStyle write fDestStyle;
    procedure ChristmasToLemmini;
  published
  end;

  TSystemCloner = class
  private
  protected
  public
//  constructor Create(aSourceStyle: TBaseLemmingStyle);
  published
  end;

//function LemmingsOriginal: T

procedure RegisterStyleClasses;

var
  DosOrigStyle        : TDosOrigStyle;
  DosOhNoStyle        : TDosOhNoStyle;
  DosChristmas91Style : TDosChristmas91Style;
  DosChristmas92Style : TDosChristmas92Style;
  DosHoliday93Style   : TDosHoliday93Style;
  DosHoliday94Style   : TDosHoliday94Style;
  LemminiStyle        : TLemminiStyle;
  CustLemmStyle       : TCustLemmStyle;

implementation

uses
  gifimage;

procedure RegisterStyleClasses;
begin
  // styles
  RegisterClass(TDosOrigStyle);
  RegisterClass(TDosOhNoStyle);
  RegisterClass(TDosChristmas91Style);
  RegisterClass(TDosChristmas92Style);
  RegisterClass(TDosHoliday93Style);
  RegisterClass(TDosHoliday94Style);
  RegisterClass(TCustLemmStyle);
  RegisterClass(TLemminiStyle);

  // graphic sets
  RegisterClass(TDosGraphicSet);
  RegisterClass(TLemminiGraphicSet);
end;

(*style:
StyleName=DosOrig
StyleDescription=Dos Original
StyleClass=TDosOrigStyle
CommonPath=D:\LemOrig\

graphicset:
GraphicSetClass=TDosOrigGraphicSet
GraphicSetName=Spec0
GraphicSetId=0
GraphicSetIdExt=1
GraphicSetArchive=Compile\DosOrig_Dirt.lga
MetaInfoFile=D:\LemOrig\GROUND0O.DAT
GraphicFile=D:\LemOrig\VGAGR0.DAT
GraphicExtFile=D:\LemOrig\VGASPEC0.DAT*)


{ TStyleConverter }

procedure TStyleConverter.ChristmasToLemmini;
var
  C: TDosHoliday93Style;
  L: TLemminiStyle;
  LPath, fn: string;
  CG: TDosGraphicSet;
  LG: TLemminiGraphicSet;
  i: Integer;
  Bmp, TargetBmp: TBitmap32;
  ibp: tbitmap;
  r, tr: trect;
  gif: TGifimage;
  Sec, Lev: Integer;
  Lvl: TLVLFileRec;
  title: string;
  Level: TLevel;
  lvlix:integer;

  levelpackfile: TStringList;

  lvlini, secstr:string;
  CS: TMemoryStream;

begin
(*


  cs := tmemorystream.Create;
  levelpackfile := tstringlist.create;


  with levelpackfile do
  begin
    add('name = Christmas Lemmings 93');
    add('maxFallDistance = 126');
    add('codeSeed = ABCDEFGH');
    add('music_0  = cancan.mod');
    add('level_0 = Flurry');
    add('level_1 = Blizzard');
  end;


  level:=tlevel.create(nil);
  ibp:=tbitmap.create;
  TargetBmp := TBitmap32.create;
  gif:=TGIFImage.create;
  L := TLemminiStyle(StyleMgr[STYLE_LEMMINI]);
  C := TDosXMas93Style(StyleMgr[STYLE_DOSXMAS93]);

  CG := TDosGraphicSet(C.GraphicSets[1]);
  LG := TLemminiGraphicSet.Create(L);
  LG.GraphicSetPath := 'styles\xsnow';
  lg.graphicsetname:='xsnow';
//  L.GraphicSetList.Add(LG);





  CG.EnsureMetaData;

//  cg.SaveToStream(cs);
  //cs.seek(0, sofrombeginning);
  LG.MetaObjectCollection.Assign(cg.MetaObjectCollection);
  LG.MetaTerrainCollection.Assign(cg.MetaTerrainCollection);
//  lg
  LG.DirectWriteMetaData('d:\xx\christmas.ini');

  LPath := L.CommonPath + 'styles\' + lg.GraphicSetName + '\';
  forcedirectories(lpath);

  lvlix := 0;
  level.style := c;
  // write levels
  for Sec := 1 to 2 do
    for Lev := 1 to 16 do
    begin
      title := C.GetLevel(lvl, sec, lev);
      deb([sec, lev, title]);
      level.LoadFromLVLRec(lvl);
      lvlini := 'd:\xx\lvl' + LeadZeroStr(sec, 1) + LeadZeroStr(Lev, 3) + '.ini';
      level.savetofile(lvlini, lffLemmini);
      inc(lvlix);
      if sec=1 then secstr:='flurry_' else secstr := 'blizzard_';
//      levelpackfile.add('#comment about this level');
      levelpackfile.add(secstr + i2s(Lev - 1) + ' = ' + extractfilename(lvlini) + ',0');
    end;


  // write terrain bitmaps
  for i := 0 to CG.MetaTerrainCollection.Count - 1 do
  begin
    Bmp := CG.TerrainBitmaps[i];
    targetbmp.masteralpha := 0;//clear(0);
    targetbmp.setsize(bmp.width * 2, bmp.height * 2);
    targetbmp.draw(targetbmp.BoundsRect, bmp.BoundsRect, bmp);
    ibp.assign(targetbmp);
    gif.assign(ibp);
//    gif.BackgroundColorIndex := 0;
    fn := lpath + lg.GraphicSetName + '_' + i2s(i) + '.gif';
//    if not fileexists(fn) then
    begin
      gif.savetofile(fn);
      deb(['write', fn]);
    end ;// else begin end;
//      deb(['skip', fn]);
  end;
  // write object bitmaps
  for i := 0 to CG.MetaObjectCollection.Count - 1 do
  begin
    Bmp := CG.ObjectBitmaps[i];
    targetbmp.masteralpha := 0;//clear(0);
    targetbmp.setsize(bmp.width * 2, bmp.height * 2);
    targetbmp.draw(targetbmp.BoundsRect, bmp.BoundsRect, bmp);
    ibp.assign(targetbmp);
    gif.assign(ibp);
    fn := lpath + lg.GraphicSetName + 'o_' + i2s(i) + '.gif';
    if not fileexists(fn) then
    begin
      gif.savetofile(fn);
      deb(['write', fn]);
    end  else begin end;
//      deb(['skip', fn]);
  end;

  targetbmp.free;
  gif.free;
  ibp.free;
  level.free;
//  for i :=
  levelpackfile.SaveToFile('d:\xx\levelpack.ini');
  levelpackfile.free;
  cs.free;




*)
end;

end.
//lemdosstyles
