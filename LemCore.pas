{$include lem_directives.inc}

unit LemCore;

interface

uses
  Dialogs,
  PngImage,
  Windows, Classes, Contnrs, Graphics, SysUtils, Messages,
  Math, IniFiles, TypInfo,
  UClasses, UEvents, UMisc, UTools, GR32, GR32_OrdinalMaps,
  LemTypes, LemDosCmp, LemDosArc, LemMisc, LemFiles, LemProgress,
  GraphicEx, StrUtils;

var
  LemmixDebugMode: Boolean;

const
  // level message base constants (see types section for message records )
  LM_BASE                      = WM_USER + 1000;
  LM_LEVEL                     = WM_USER + 1100; // messages for level
  LM_EDIT                      = WM_USER + 1200; // messages for leveleditor

type
  ITranslatable = interface
  ['{E47A03B3-D596-4972-8BC6-40CACD9C2D5E}']
//    procedure DoTranslate;
  end;

const LM_LANGUAGECHANGED           = LM_BASE + 1;
type  TLMLanguageChanged           = TMessage;

const LM_PROGRESS                  = LM_BASE + 2; //

// procedure LMLanguageChanged(var Msg: TLMLanguageChanged); message LM_LANGUAGECHANGED;

  //color constant to check coordinate out of range
const
  CL_AIR                    = 0;
  CL_OUTER                  = $FFFFFFFF;


  // level messages
const
  LM_LEVELCHANGING             = LM_LEVEL + 1;
  LM_LEVELCHANGED              = LM_LEVEL + 2;
  LM_OBJECTLISTCHANGING        = LM_LEVEL + 3; // not used
  LM_OBJECTLISTCHANGED         = LM_LEVEL + 4;
  LM_TERRAINLISTCHANGING       = LM_LEVEL + 5; // not used
  LM_TERRAINLISTCHANGED        = LM_LEVEL + 6;
  LM_STEELLISTCHANGING         = LM_LEVEL + 7; // not used
  LM_STEELLISTCHANGED          = LM_LEVEL + 8;
//  LM_STATICSCHANGING
  LM_STATICSCHANGED            = LM_LEVEL + 10;


const
  // validation constants
  MIN_RELEASERATE              = 00; //1;
  MAX_RELEASERATE              = 99; //99;
  MIN_LEMMINGSCOUNT            = 1;
  MAX_LEMMINGSCOUNT            = 65535;
  MIN_RESCUECOUNT              = 0;//'//1;
  MAX_RESCUECOUNT              = 65535;
  MIN_TIMELIMIT                = 0;//1;   // = 0
  MAX_TIMELIMIT                = 99;//20;  // = 255
  MIN_MUSIC                    = 0;
  MAX_MUSIC                    = 255;
  MIN_CLIMBERCOUNT             = 0;
  MAX_CLIMBERCOUNT             = 99;      //skillmax=250
  MIN_FLOATERCOUNT             = 0;
  MAX_FLOATERCOUNT             = 99;
  MIN_BOMBERCOUNT              = 0;
  MAX_BOMBERCOUNT              = 99;
  MIN_BLOCKERCOUNT             = 0;
  MAX_BLOCKERCOUNT             = 99;
  MIN_BUILDERCOUNT             = 0;
  MAX_BUILDERCOUNT             = 99;
  MIN_BASHERCOUNT              = 0;
  MAX_BASHERCOUNT              = 99;
  MIN_MINERCOUNT               = 0;
  MAX_MINERCOUNT               = 99;
  MIN_DIGGERCOUNT              = 0;
  MAX_DIGGERCOUNT              = 99;

  MIN_TERRAINX                 = -256;
  MAX_TERRAINX                 = 32000;

  DF_ERASE                     = Bit0;
  DF_UPSIDEDOWN                = Bit1;
  DF_NOOVERWRITE               = Bit2;
  DF_ONLYONTERRAIN             = Bit3;

  // level internal change flags (used by the internal message system)
  // lscf=level static change flag
{  LSCF_RELEASERATE
  LSCF_LEMMINGSCOUNT
  LSCF_RESCUECOUNT
  LSCF_TIMELIMIT
  LSCF_CLIMBERCOUNT
  LSCF_FLOATERCOUNT
  LSCF_BOMBERCOUNT
  LSCF_BLOCKERCOUNT
  LSCF_BUILDERCOUNT
  LSCF_BASHERCOUNT
  LSCF_MINERCOUNT
  LSCF_DIGGERCOUNT }

  LCF_LOADFROMFILE        = Bit0;
  LCF_LOADFROMSTREAM      = Bit1;
  LCF_STYLECHANGED        = Bit2;
  LCF_GRAPHCHANGED        = Bit3;



type
  // do not change terraindrawingflag! enum bits are compatible with win lvl specification.
  TTerrainDrawingFlag = (
//    tdfDummy,       // never used! only here for direct write from lvl, maybe delete this!
// dont used anymore just shift one more
    tdfErase,       // 1 bit 0
    tdfInvert,      // 2 bit 1
    tdfNoOverwrite,  // 4 bit 2
    tdfInvisible,
    tdfFake,
    tdfFlip,
    tdfNoOneWay,
    tdfRotate
  );
  TTerrainDrawingFlags = set of TTerrainDrawingFlag;

  TObjectDrawingFlag = (
    odfOnlyShowOnTerrain,
    odfInvert,
    odfNoOverwrite,
    odfFaceLeft,
    odfFake,
    odfInvisible,
    odfFlip,
    odfRotate
  );
  TObjectDrawingFlags = set of TObjectDrawingFlag;

  // type that every levelitem is obligated to fill in correctly when created.
  // the type is made to "case" the classes and make sets
  TLevelItemType = (
    litNone,
    litBase,
    litObject,
    litTerrain,
    litSteel,
    litBackground
  );
  TLevelItemTypes = set of TLevelItemType;

  // used by bitmap building
  TRenderOption = (
//    roInitialScreenPosRect,
    roCollisionGridBuilding,
    roSteelNoOverwrite,
    roShowTriggerAreas
  );
  TRenderOptions = set of TRenderOption;

//  TValidLevelType = (litObject..litSteel];

  // forward
  TBaseLemmingStyle       = class; // messages

  TBaseGraphicSet         = class;
  TBaseGraphicSetClass    = class of TBaseGraphicSet;
  TBaseGraphicSetList     = class;

  TBaseAnimationSet       = class;
  TBaseAnimationSetClass  = class of TBaseAnimationSet;

  TBaseLevelLoaderClass   = class of TBaseLevelLoader;
  TBaseLevelLoader        = class;

  TInteractiveObject  = class;
  TTerrain            = class;
  TSteel              = class;
  TLevel              = class;

  // message records in order of numbering
(*  TLMProgress = packed record
    Msg             : Cardinal;
    ProgressObject  : TObject;
    NotUsed         : Integer;
    Result          : Longint;
  end; *)

  TLMLevelChanging = TMessage;
//  TLMLevelChanged  = TMessage;
  TLMLevelChanged = packed record
    Msg      : Cardinal;
    NotUsed  : Integer;
    LevelChangeFlags : Cardinal; { LCF_XXXX }
    Result   : Longint;
  end;

  TLMStaticsChanging = TMessage;
  TLMStaticsChanged = packed record
    Msg        : Cardinal;
    ChangeInfo : Integer;
    NotUsed    : Integer;
    Result     : Longint;
  end;

  TLMObjectListChanging = packed record
    Msg      : Cardinal;
    Obj      : TInteractiveObject;
    NotUsed  : Integer;
    Result   : Longint;
  end;

  TLMObjectListChanged = packed record
    Msg      : Cardinal;
    Obj      : TInteractiveObject;
    NotUsed  : Integer;
    Result   : Longint;
  end;

  TLMTerrainListChanging = packed record
    Msg      : Cardinal;
    Terrain  : TTerrain;
    NotUsed  : Integer;
    Result   : Longint;
  end;

  TLMTerrainListChanged = packed record
    Msg      : Cardinal;
    Terrain  : TTerrain;
    NotUsed  : Integer;
    Result   : Longint;
  end;

  TLMTSteelListChanging = packed record
    Msg      : Cardinal;
    Steel    : TSteel;
    NotUsed  : Integer;
    Result   : Longint;
  end;

  TLMSteelListChanged = packed record
    Msg      : Cardinal;
    Steel    : TSteel;
    NotUsed  : Integer;
    Result   : Longint;
  end;

  // custom ancestor for items in a level
  TLevelItem = class(TPersistent)
  private
    fLevel         : TLevel;
    fUpdateCount   : Integer;
    fOnChange      : TNotifyEvent;
  protected
    property UpdateCount: Integer read fUpdateCount;
  public
    constructor Create(aOwner: TLevel);
    procedure Changed; virtual;
    procedure BeginUpdate; virtual;
    procedure EndUpdate; virtual;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  end;

  TLevelStaticsRec = record
    rReleaseRate                : Integer;
    rLemmingsCount              : Integer;
    rRescueCount                : Integer;
    rTimeLimit                  : Integer;
    rClimberCount               : Integer;
    rFloaterCount               : Integer;
    rBomberCount                : Integer;
    rBlockerCount               : Integer;
    rBuilderCount               : Integer;
    rBasherCount                : Integer;
    rMinerCount                 : Integer;
    rDiggerCount                : Integer;
    rGraphicSet                 : Integer;
    rGraphicSetEx               : Integer;
    rScreenPosition             : Integer;
    rTitle                      : String32;
  end;

  // static level things
  TLevelStatics = class(TLevelItem)
  private
    fReleaseRate                : Integer;
    fLemmingsCount              : Integer;
    fRescueCount                : Integer;
    fTimeLimit                  : Integer;
    fClimberCount               : Integer;
    fFloaterCount               : Integer;
    fBomberCount                : Integer;
    fBlockerCount               : Integer;
    fBuilderCount               : Integer;
    fBasherCount                : Integer;
    fMinerCount                 : Integer;
    fDiggerCount                : Integer;

    fWalkerCount                : Integer;
    fSwimmerCount               : Integer;
    fGliderCount                : Integer;
    fMechanicCount              : Integer;
    fStonerCount                : Integer;
    fPlatformerCount            : Integer;
    fStackerCount               : Integer;
    fClonerCount                : Integer;

    fFencerCount                : Integer;

    fSkillSet                   : Cardinal;
    fInfiniteSkills             : Cardinal;

    fGraphicSet                 : Integer;
    fGraphicSetEx               : Integer;

    fAutoSteelOptions           : Integer;
    fMusicTrack                 : Integer;
    fSuperLemming               : Integer;

    fScreenPosition             : Integer;
    fScreenYPosition            : Integer;

    fLevelWidth                 : Integer;
    fLevelHeight                : Integer;
    fFallDist                   : Integer;

    fBoundaryT  : Integer;
    fBoundaryL  : Integer;
    fBoundaryR  : Integer;
    fBoundaryB  : Integer;

    fHasTimeLimit               : Boolean;
    fLockedRR                   : Boolean;

    fStyleName                  : ShortString;
    fVgaspecName                : ShortString;

    fBackgroundIndex            : Integer;


    fVgaspecX                   : Integer;
    fVgaspecY                   : Integer;

    fPostSecretRank             : Integer;
    fPostSecretLevel            : Integer;
    fBnsRank                    : Integer;
    fBnsLevel                   : Integer;
    fClockStart                 : Integer;
    fClockEnd                   : Integer;
    fClockCount                 : Integer;
    fLevelID                    : LongWord;

    function GetLevelWidth: Integer;
    function GetLevelHeight: Integer;
  { validation methods }
    function ValidReleaseRate(Value: Integer): Boolean;
    function ValidLemmingsCount(Value: Integer): Boolean;
    function ValidRescueCount(Value: Integer): Boolean;
    function ValidTimeLimit(Value: Integer): Boolean;
    function ValidClimberCount(Value: Integer): Boolean;
    function ValidFloaterCount(Value: Integer): Boolean;
    function ValidBomberCount(Value: Integer): Boolean;
    function ValidBlockerCount(Value: Integer): Boolean;
    function ValidBuilderCount(Value: Integer): Boolean;
    function ValidBasherCount(Value: Integer): Boolean;
    function ValidMinerCount(Value: Integer): Boolean;
    function ValidDiggerCount(Value: Integer): Boolean;

    function ValidSkillCount(Value: Integer): Boolean;

    function ValidGraphicSet(Value: Integer): Boolean;
    function ValidMusic(Value: Integer): Boolean;
    // checking methods
    procedure CheckReleaseRate(var Value: Integer);
    procedure CheckLemmingsCount(var Value: Integer);
    procedure CheckRescueCount(var Value: Integer);
    procedure CheckTimeLimit(var Value: Integer);
    procedure CheckClimberCount(var Value: Integer);
    procedure CheckFloaterCount(var Value: Integer);
    procedure CheckBomberCount(var Value: Integer);
    procedure CheckBlockerCount(var Value: Integer);
    procedure CheckBuilderCount(var Value: Integer);
    procedure CheckBasherCount(var Value: Integer);
    procedure CheckMinerCount(var Value: Integer);
    procedure CheckDiggerCount(var Value: Integer);

    procedure CheckSkillCount(var Value: Integer);

    procedure CheckMusic(var Value: Integer);
    procedure CheckGraphicSet(var Value: Integer);
    // property access
    procedure SetReleaseRate(Value: Integer);
    procedure SetLemmingsCount(Value: Integer);
    procedure SetRescueCount(Value: Integer);
    procedure SetTimeLimit(Value: Integer);
    procedure SetClimberCount(Value: Integer);
    procedure SetFloaterCount(Value: Integer);
    procedure SetBomberCount(Value: Integer);
    procedure SetBlockerCount(Value: Integer);
    procedure SetBuilderCount(Value: Integer);
    procedure SetBasherCount(Value: Integer);
    procedure SetMinerCount(Value: Integer);
    procedure SetDiggerCount(Value: Integer);

    procedure SetWalkerCount(Value: Integer);
    procedure SetSwimmerCount(Value: Integer);
    procedure SetGliderCount(Value: Integer);
    procedure SetMechanicCount(Value: Integer);
    procedure SetStonerCount(Value: Integer);
    procedure SetPlatformerCount(Value: Integer);
    procedure SetStackerCount(Value: Integer);
    procedure SetClonerCount(Value: Integer);

    procedure SetFencerCount(Value: Integer);

    procedure SetMusic(Value: Integer);
    procedure SetGraphicSet(Value: Integer);
    procedure SetGraphicSetEx(Value: Integer);
    procedure SetScreenPosition(Value: Integer);
    procedure SetScreenYPosition(Value: Integer);
  protected
    procedure Clear;
  public
    fWindowOrder                : Array of Word;
    constructor Create(aOwner: TLevel);
    procedure Changed; overload; override;
    procedure ChangedInf(aChangeInfo: Integer); overload;
  published
    property ReleaseRate     : Integer read fReleaseRate write SetReleaseRate;
    property LockedRR        : Boolean read fLockedRR write fLockedRR;
    property LemmingsCount   : Integer read fLemmingsCount write SetLemmingsCount;
    property RescueCount     : Integer read fRescueCount write SetRescueCount;
    property TimeLimit       : Integer read fTimeLimit write SetTimeLimit;
    property WalkerCount     : Integer read fWalkerCount write SetWalkerCount;
    property ClimberCount    : Integer read fClimberCount write SetClimberCount;
    property SwimmerCount    : Integer read fSwimmerCount write SetSwimmerCount;
    property FloaterCount    : Integer read fFloaterCount write SetFloaterCount;
    property GliderCount     : Integer read fGliderCount write SetGliderCount;
    property MechanicCount   : Integer read fMechanicCount write SetMechanicCount;
    property BomberCount     : Integer read fBomberCount write SetBomberCount;
    property StonerCount     : Integer read fStonerCount write SetStonerCount;
    property BlockerCount    : Integer read fBlockerCount write SetBlockerCount;
    property PlatformerCount : Integer read fPlatformerCount write SetPlatformerCount;
    property BuilderCount    : Integer read fBuilderCount write SetBuilderCount;
    property StackerCount    : Integer read fStackerCount write SetStackerCount;
    property BasherCount     : Integer read fBasherCount write SetBasherCount;
    property MinerCount      : Integer read fMinerCount write SetMinerCount;
    property DiggerCount     : Integer read fDiggerCount write SetDiggerCount;
    property ClonerCount     : Integer read fClonerCount write SetClonerCount;
    property FencerCount     : Integer read fFencerCount write SetFencerCount;
    property GraphicSet      : Integer read fGraphicSet write SetGraphicSet;
    property GraphicSetEx    : Integer read fGraphicSetEx write SetGraphicSetEx;
    property AutoSteelOptions: Integer read fAutoSteelOptions write fAutoSteelOptions;
    property MusicTrack      : Integer read fMusicTrack write SetMusic;
    property SuperLemming    : Integer read fSuperLemming write fSuperLemming;
    property ScreenPosition  : Integer read fScreenPosition write SetScreenPosition;
    property ScreenYPosition : Integer read fScreenYPosition write SetScreenYPosition;
    property SkillTypes      : Cardinal read fSkillSet write fSkillSet;
    property InfiniteSkills  : Cardinal read fInfiniteSkills write fInfiniteSkills;
    property Width           : Integer read GetLevelWidth write fLevelWidth;
    property Height          : Integer read GetLevelHeight write fLevelHeight;
    property FallDistance    : Integer read fFallDist write fFallDist;
    property HasTimeLimit    : Boolean read fHasTimeLimit write fHasTimeLimit;
    property BoundaryT       : Integer read fBoundaryT write fBoundaryT;
    property BoundaryL       : Integer read fBoundaryL write fBoundaryL;
    property BoundaryR       : Integer read fBoundaryR write fBoundaryR;
    property BoundaryB       : Integer read fBoundaryB write fBoundaryB;
    property StyleName       : ShortString read fStyleName write fStyleName;
    property VgaspecName     : ShortString read fVgaspecName write fVgaspecName;
    property VgaspecX        : Integer read fVgaspecX write fVgaspecX;
    property VgaspecY        : Integer read fVgaspecY write fVgaspecY;

    property BackgroundIndex : Integer read fBackgroundIndex write fBackgroundIndex;

    property PostSecretRank  : Integer read fPostSecretRank write fPostSecretRank;
    property PostSecretLevel : Integer read fPostSecretLevel write fPostSecretLevel;
    property BnsRank         : Integer read fBnsRank write fBnsRank;
    property BnsLevel        : Integer read fBnsLevel write fBnsLevel;
    property ClockStart      : Integer read fClockStart write fClockStart;
    property ClockEnd        : Integer read fClockEnd write fClockEnd;
    property ClockCount      : Integer read fClockCount write fClockCount;
    property LevelID         : LongWord read fLevelID write fLevelID;
  end;

  {-------------------------------------------------------------------------------
    Custom ancestor item for level. In the first place it has the shared
    properties of objects, terrain and steel.
  -------------------------------------------------------------------------------}
  TBaseLevelItem = class(TCollectionItem)
  private
    fOwnerLevel: TLevel; // backlink naar level, initiated on create
    fItemType: TLevelItemType; // case-able enum. not very oop but handy.
    fItemActive: Boolean;
    fItemLocked: Boolean;
    fGraph               : TBaseGraphicSet;
    fGraphID             : Integer;
    function GetBounds: TRect;
//    function GetOwnerLevel: TLevel;
  protected
    function GetAsString: string; virtual;
    procedure SetAsString(const Value: string); virtual;
    function GetIdentifier: Integer; virtual;
    function GetXPosition: Integer; virtual;
    function GetYPosition: Integer; virtual;

    procedure SetIdentifier(Value: Integer); virtual;
    procedure SetXPosition(Value: Integer); virtual;
    procedure SetYPosition(Value: Integer); virtual;

    function GetItemHeight: Integer; virtual;
    function GetItemWidth: Integer; virtual;
    procedure SetItemHeight(Value: Integer); virtual;
    procedure SetItemWidth(Value: Integer); virtual;
    function DoGetItemType: TLevelItemType; virtual;
    procedure SetIndex(Value: Integer); override;
    procedure PropertyChanged;
    function IsActive: Boolean;
    procedure CheckActive;
  public
    constructor Create(aCollection: TCollection); override;
    procedure Move(DeltaX, DeltaY: Integer);
//    property Level: TLevel read fLevel;
    property OwnerLevel: TLevel read fOwnerLevel;//GetOwnerLevel;
    property Identifier: Integer read GetIdentifier write SetIdentifier;
    property XPosition: Integer read GetXPosition write SetXPosition;
    property YPosition: Integer read GetYPosition write SetYPosition;
    property ItemWidth: Integer read GetItemWidth write SetItemWidth;
    property ItemHeight: Integer read GetItemHeight write SetItemHeight;
    property ItemActive: Boolean read fItemActive write fItemActive;
    property ItemLocked: Boolean read fItemLocked write fItemLocked;
    property Bounds: TRect read GetBounds;
    property AsString: string read GetAsString write SetAsString;
    property ItemType: TLevelItemType read fItemType;
    property Graph: TBaseGraphicSet read fGraph write fGraph;
    procedure SetSValue(Value: Integer); virtual;
    procedure SetLValue(Value: Integer); virtual;
  published
  end;

  // abstract ancestor: collection that has to have TLevel as owner
  TLevelCollection = class(TOwnedCollectionEx)
  private
  protected
    fLevel: TLevel;
  public
    constructor Create(aOwner: TPersistent; aItemClass: TCollectionItemClass);
    property Level: TLevel read fLevel;
  published
  end;

  TBaseLevelItemCollection = class(TLevelCollection)
  private
    fTargetItemCount : Integer;
    function GetItem(Index: Integer): TBaseLevelItem;
    procedure SetItem(Index: Integer; const Value: TBaseLevelItem);
    procedure AdjustItemCount(aValue: Integer);
  protected
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
  public
    constructor Create(aOwner: TPersistent; aItemClass: TCollectionItemClass);
    function Add: TBaseLevelItem;
    function ForceAdd: TBaseLevelItem;
    procedure Clear;
    procedure FullClear;
    property Items[Index: Integer]: TBaseLevelItem read GetItem write SetItem; default;
    property TargetItemCount: Integer read fTargetItemCount write AdjustItemCount;
  published
  end;

  {-------------------------------------------------------------------------------
    interactive objects: entrance, exit, trap etc.
  -------------------------------------------------------------------------------}
  TInteractiveObject = class(TBaseLevelItem)
  private
    procedure SetObjectX(Value: Integer);
    procedure SetObjectY(Value: Integer);
    procedure SetObjectID(Value: Integer);
    procedure SetObjectDrawingFlags(Value: TObjectDrawingFlags);
    procedure SetObjectSValue(Value: Integer);
    procedure SetObjectLValue(Value: Integer);
    function GetBitmap: TBitmap32;
  protected
    fObjectX            : Integer;
    fObjectY            : Integer;
    fObjectID           : Integer;
    fObjectSValue       : Integer;
    fObjectLValue       : Integer;
    fObjectDrawingFlags : TObjectDrawingFlags;
    fIsActive           : Boolean;
    function GetAsString: string; override;
    procedure SetAsString(const Value: string); override;
    function GetXPosition: Integer; override;
    function GetYPosition: Integer; override;
    procedure SetXPosition(Value: Integer); override;
    procedure SetYPosition(Value: Integer); override;

    function GetItemHeight: Integer; override;
    function GetItemWidth: Integer; override;
    function DoGetItemType: TLevelItemType; override;
    function GetIdentifier: Integer; override;
    procedure SetIdentifier(Value: Integer); override;
  public
    function ValidObjectID(Value: Integer): Boolean;
    //function ValidSValue(Value: Integer): Boolean;
    //function ValidLValue(Value: Integer): Boolean;
    property Bitmap: TBitmap32 read GetBitmap;
    procedure GetFrameBitmap(aFrame: Integer; aBitmap: TBitmap32);
    procedure SetSValue(Value: Integer); override;
    procedure SetLValue(Value: Integer); override;
  published
    property ObjectX: Integer read fObjectX write SetObjectX;
    property ObjectY: Integer read fObjectY write SetObjectY;
    property ObjectID: Integer read fObjectID write SetObjectID;
    property ObjectSValue: Integer read fObjectSValue write SetSValue;
    property ObjectLValue: Integer read fObjectLValue write SetLValue;
    property ObjectDrawingFlags: TObjectDrawingFlags read fObjectDrawingFlags write SetObjectDrawingFlags;
  end;

  TCollectionUpdateEvent = procedure(Sender: TObject; Item: TCollectionItem) of object;

  TInteractiveObjectCollection = class(TBaseLevelItemCollection)
  private
    function GetItem(Index: Integer): TInteractiveObject;
    procedure SetItem(Index: Integer; const Value: TInteractiveObject);
  protected
//    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(aOwner: TPersistent);
    function Add: TInteractiveObject;
    function Insert(Index: Integer): TInteractiveObject;
    //function ActiveCount: Integer;
    property Items[Index: Integer]: TInteractiveObject read GetItem write SetItem; default;
  published
  end;

  // terrain piece belonging to a level
  TTerrain = class(TBaseLevelItem)
  private
    procedure SetTerrainX(Value: Integer);
    procedure SetTerrainY(Value: Integer);
    procedure SetTerrainDrawingFlags(const Value: TTerrainDrawingFlags);
    procedure SetTerrainID(const Value: Integer);
    function GetBitmap: TBitmap32;
  protected
    fTerrainX            : Integer;
    fTerrainY            : Integer;
    fTerrainID           : Integer;
    fTerrainDrawingFlags : TTerrainDrawingFlags;
    function GetAsString: string; override; // copy-paste
    procedure SetAsString(const Value: string); override; // copy-paste
    function GetXPosition: Integer; override;
    function GetYPosition: Integer; override;
    procedure SetXPosition(Value: Integer); override;
    procedure SetYPosition(Value: Integer); override;
    function GetItemHeight: Integer; override;
    function GetItemWidth: Integer; override;
    function DoGetItemType: TLevelItemType; override;
    function GetIdentifier: Integer; override;
    procedure SetIdentifier(Value: Integer); override;
  public
    constructor Create(aCollection: TCollection); override;
    function ValidTerrainID(Value: Integer): Boolean;
    property Bitmap: TBitmap32 read GetBitmap;
  published
    property TerrainX: Integer read fTerrainX write SetTerrainX;
    property TerrainY: Integer read fTerrainY write SetTerrainY;
    property TerrainID: Integer read fTerrainID write SetTerrainID;
    property TerrainDrawingFlags: TTerrainDrawingFlags read fTerrainDrawingFlags write SetTerrainDrawingFlags;
  end;

  TTerrainCollection = class(TBaseLevelItemCollection)
  private
    function GetItem(Index: Integer): TTerrain;
    procedure SetItem(Index: Integer; const Value: TTerrain);
  protected
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(aOwner: TPersistent);
    function Add: TTerrain;
    function Insert(Index: Integer): TTerrain;
    property Items[Index: Integer]: TTerrain read GetItem write SetItem; default;
  published
  end;

  TSteel = class(TBaseLevelItem)
  private
    procedure SetSteelHeight(const Value: Integer);
    procedure SetSteelWidth(const Value: Integer);
    procedure SetSteelX(const Value: Integer);
    procedure SetSteelY(const Value: Integer);
  protected
    fSteelX      : Integer;
    fSteelY      : Integer;
    fSteelWidth  : Integer;
    fSteelHeight : Integer;
    fSteelType   : Integer;
    function GetAsString: string; override;
    procedure SetAsString(const Value: string); override;
    function GetXPosition: Integer; override;
    function GetYPosition: Integer; override;
    procedure SetXPosition(Value: Integer); override;
    procedure SetYPosition(Value: Integer); override;
    procedure SetSteelType(Value: Integer);
    function GetItemHeight: Integer; override;
    function GetItemWidth: Integer; override;
    procedure SetItemHeight(Value: Integer); override;
    procedure SetItemWidth(Value: Integer); override;
    function DoGetItemType: TLevelItemType; override;
  public
  published
    property SteelX      : Integer read fSteelX write SetSteelX;
    property SteelY      : Integer read fSteelY write SetSteelY;
    property SteelWidth  : Integer read fSteelWidth write SetSteelWidth;
    property SteelHeight : Integer read fSteelHeight write SetSteelHeight;
    property SteelType : Integer read fSteelType write SetSteelType;
  end;

  TSteelCollection = class(TBaseLevelItemCollection{TOwnedCollectionEx})
  private
    function GetItem(Index: Integer): TSteel;
    procedure SetItem(Index: Integer; const Value: TSteel);
  protected
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(aOwner: TPersistent);
    function Add: TSteel;
    function Insert(Index: Integer): TSteel;
    property Items[Index: Integer]: TSteel read GetItem write SetItem; default;
  published
  end;

  // link for messaging
  TLevelLink = class(TMessageLink)
  private
    function GetLevel: TLevel;
    procedure SetLevel(const Value: TLevel);
  protected
  public
    property Level: TLevel read GetLevel write SetLevel;
  end;


  {-------------------------------------------------------------------------------
    A sort of "minimum global information" record, so that we do not need to
    really load levels, but only retrieve some info before really loading.
  -------------------------------------------------------------------------------}
  TLevelInfoRec = record // should become obsolete
//    iStyle     : TBaseLemmingStyle;//?

    iSection     : Integer; // 0-based section number
    iSectionName : string;  //

    iLevel     : Integer; // 0-based level number in section
    iTitle     : string;

    // DOS
    iOwnerFile : string; // dos levelpack (in which file is this level stored)
    iOwnerFileIndex: Integer; // dos levelpack index (only dos)

  end;

  TLevelSaveParams = record
    //sStyle: TBaseLemmingStyle; // nil = currentstyle or autodetect
    //sFormat: TLemmingFile
//    sCompression: TLevelCompression;
  end;

  {-------------------------------------------------------------------------------
    TLevel:
     o everything that's needed for a level to get from disk or stream
     o It also has all information to play the game
     o It has all information for editing
     o It has all validation
  -------------------------------------------------------------------------------}
  TLevel = class(TComponent)
  private
    fMgr                       : TMessageManager;
    fFileName                  : string;
    fCurrentFormat             : TLemmingFileFormat;
    fEditing                   : Boolean;
    fStateFlags                : Cardinal;
    fChangeFlags               : Cardinal;
    fCheckSum                  : Integer;
    fStyle                     : TBaseLemmingStyle; // style ref
    fGraph                     : TBaseGraphicSet; // graph ref, must be a graphicset of fStyle
    fChanged                   : Boolean;
    procedure CacheSizes;
    procedure AdjustResolution(Factor: Single);
//    procedure AdjustResolution(Factor: Single);
    procedure SetFileName(const Value: string);
    procedure InternalValidate;
    procedure SetEditing(const Value: Boolean);
    procedure IncludeStates(aFlags: Cardinal);
    procedure ExcludeStates(aFlags: Cardinal);
    procedure IncludeChanges(aFlags: Cardinal);
    procedure SetStyle(const Value: TBaseLemmingStyle);
    procedure SetGraph(const Value: TBaseGraphicSet);
    function GetCompatibleGraph(aGraph: TBaseGraphicSet; aStyle: TBaseLemmingStyle): TBaseGraphicSet;
  protected
    fUpdateCount               : Integer;
    procedure ClearLevel(DoClearStyle, DoClearGraph: Boolean); virtual;
    procedure DoBeforeLoad; virtual;
    procedure DoAfterLoad; virtual;
    procedure Mgr_BeforeSendMessage(Sender: TObject; var DoSend: Boolean);
    procedure MsgLevelChanging; virtual;
    procedure MsgLevelChanged; virtual;
    procedure MsgListChanged(Which: TCollection; Item: TCollectionItem); virtual;
    procedure MsgStaticsChanged(aChangeInfo: Integer = 0); virtual;

  { checks }
    procedure CheckObjectX(aObject: TInterActiveObject; var X: Integer); virtual;
  public
    Statics                     : TLevelStatics;
    InteractiveObjectCollection : TInteractiveObjectCollection;
    TerrainCollection           : TTerrainCollection;
    SteelCollection             : TSteelCollection;
    LevelName                   : string;
    LevelAuthor                 : string;
    LevelMainLevel              : string;
    LevelMusicFile              : string;
    LevelLock: Integer;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  { load and save }
    function DoAutoDetect(aStream: TStream): TLemmingFileFormat;
    //function DoAutoDetectStyle(aStream: TStream; var aStyle: Integer; ): TLemmingFileFormat;

    procedure LoadFromFile(const aFileName: string; aStyle: TBaseLemmingStyle; aFormat: TLemmingFileFormat); overload;
    procedure LoadFromStream(aStream: TStream; aStyle: TBaseLemmingStyle; aFormat: TLemmingFileFormat; ResetFileName: Boolean = true); overload;

    procedure LoadFromDosArchiveFile(const aFileName: string; aIndex: Integer; aStyle: TBaseLemmingStyle);
    procedure LoadFromDosArchive(aArchive: TDosDatArchive; aIndex: Integer; aStyle: TBaseLemmingStyle);

    // hide these 3(?)
    procedure LoadFromLVLStream(aStream: TStream);
    procedure LoadFromNeoLVLStream(aStream: TStream);
    procedure LoadFromNewNeoLVLStream(aStream: TStream);
    procedure LoadFromLVLRec(const LVL: TLVLFileRec); overload;
    procedure LoadFromLVLRec(const LVL: TNeoLVLRec); overload;
    procedure LoadFromLemminiStream(aStream: TStream);

    procedure UpgradeFormat(var Buf: TNeoLVLRec);


    procedure SaveToFile(const aFileName: string; aFormat: TLemmingFileFormat; UpdateMemName: Boolean = true); virtual;
    procedure SaveToStream(aStream: TStream; aFormat: TLemmingFileFormat);
    procedure SaveToLVLStream(aStream: TStream);
    procedure SaveToTradLVLStream(aStream: TStream);
    procedure SaveToNeoLVLStream(aStream: TStream);
    procedure SaveToLVLFileRec(var Buf: TLVLFileRec);
    procedure SaveToNeoLVLFileRec(var Buf: TNeoLVLRec);

    procedure SaveToLemminiStream(aStream: TStream);
    function SaveToLemminiString: string;

    procedure BeginUpdate;
    procedure EndUpdate; virtual;
    function IsUpdating: Boolean;

    procedure LockLevel;
    procedure UnlockLevel;
    function IsLocked: Boolean;
    procedure Changed;
    procedure CloseLevel;
    function GetAdjustedFileName(const aFilename: string; aFormat: TLemmingFileFormat): string;
  { properties }
    property Mgr: TMessageManager read fMgr;
    property IOC: TInteractiveObjectCollection read InteractiveObjectCollection;
    property TNC: TTerrainCollection read TerrainCollection;
    property SLC: TSteelCollection read SteelCollection;
    property FileName: string read fFileName write SetFileName;
    property CurrentFormat: TLemmingFileFormat read fCurrentFormat;

    property Style: TBaseLemmingStyle read fStyle write SetStyle;
    property Graph: TBaseGraphicSet read fGraph write SetGraph;

    property IsChanged: Boolean read fChanged write fChanged; // yeah, there are cases where we need to override this
    
    property StateFlags: Cardinal read fStateFlags;
  published
    //property Resolution: TResolution read fResolution write fResolution;
    property Editing: Boolean read fEditing write SetEditing;
  end;

  {-------------------------------------------------------------------------------
    Basic Level Validator
  -------------------------------------------------------------------------------}
  TCustomLevelValidator = class
  private
  protected
  public
    class function ValidReleaseRate(Value: Integer): Boolean; virtual; abstract;
    class function ValidLemmingsCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidRescueCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidTimeLimit(Value: Integer): Boolean; virtual; abstract;
    class function ValidClimberCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidFloaterCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidBomberCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidBlockerCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidBuilderCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidBasherCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidMinerCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidDiggerCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidSkillCount(Value: Integer): Boolean; virtual; abstract;
    class function ValidGraphicSet(Value: Integer): Boolean; virtual; abstract;
    class function ValidMusic(Value: Integer): Boolean; virtual; abstract;
    class function ValidOTRank(Value: Integer): Boolean; virtual; abstract;
    class function ValidOTLevel(Value: Integer): Boolean; virtual; abstract;

    class function ValidateLevel(aLevel: TLevel; aReport: TStrings): Boolean; virtual;
//    class function Valid


  published
  end;

  // palette helper class for picturemgr (born from win lemmings)
  TLemmingPalette = class
  private
  protected
  public
    Colors      : TColorArray;
    ColorCount  : Integer;
    Description : string;
    destructor Destroy; override;
  published
  end;

  TLemmingPaletteList = class(TObjectList)
  private
    function GetItem(Index: Integer): TLemmingPalette;
  protected
  public
    function Add(Item: TLemmingPalette): Integer;
    procedure Insert(Index: Integer; Item: TLemmingPalette);
    property Items[Index: Integer]: TLemmingPalette read GetItem; default;
    property List;
  published
  end;

  TDefaultsRec = record
    Resolution      : Single;
    LevelWidth      : Integer; // the total width of the level
    LevelHeight     : Integer;
    ViewWidth       : Integer; // the width of the screen
    ViewHeight      : Integer;
    MultiPacks      : Boolean;
  end;

  {-------------------------------------------------------------------------------
    Nested classes for levelpack system:
    system
      levelpack (DosOrig=Default: only for multi levelpacks like Lemmini)
        section (FUN)
          levelinfo (just dig)
      levelpack
        section
          levelinfo
  -------------------------------------------------------------------------------}
  TPackSection = class;
  TLevelPack = class;

  TLevelInfo = class(TCollectionItem)
  private
    function GetOwnerSection: TPackSection;
  protected
    fTrimmedTitle            : string; // the title: this is only for display purposes.
    fDosLevelPackFileName    : string; // dos file where we can find this level (levelxxx.dat)
    fDosLevelPackIndex       : Integer; // dos file position in doslevelpackfilename(in fact: which decompression section)
    fDosIsOddTableClone      : Boolean; // true if this is a cloned level with other title and skills
    fDosOddTableFileName     : string; // only dosorig: oddtable.dat
    fDosOddTableIndex        : Integer; // were can we find this other title and skills in the dosoddtable file
    fOwnerFile               : string; // lemmini: lemmini ini file containing this level
  public
    property OwnerSection: TPackSection read GetOwnerSection;
  published
    property TrimmedTitle: string read fTrimmedTitle write fTrimmedTitle;
    property DosLevelPackFileName: string read fDosLevelPackFileName write fDosLevelPackFileName;
    property DosLevelPackIndex: Integer read fDosLevelPackIndex write fDosLevelPackIndex;
    property DosIsOddTableClone: Boolean read fDosIsOddTableClone write fDosIsOddTableClone;
    property DosOddTableFileName: string read fDosOddTableFileName write fDosOddTableFileName;
    property DosOddTableIndex: Integer read fDosOddTableIndex write fDosOddTableIndex;
    property OwnerFile: string read fOwnerFile write fOwnerFile;
  end;

  TLevelInfos = class(TOwnedCollectionEx)
  private
    function GetItem(Index: Integer): TLevelInfo;
    procedure SetItem(Index: Integer; const Value: TLevelInfo);
  protected
  public
    constructor Create(aOwner: TPersistent); // owner = TPackSection
    function Add: TLevelInfo;
    function Insert(Index: Integer): TLevelInfo;
    property Items[Index: Integer]: TLevelInfo read GetItem write SetItem; default;
  published
  end;

  TPackSection = class(TCollectionItem)
  private
    fLevelInfos: TLevelInfos;
    fSectionName: string;
    fSourceFileName: string;
    function GetOwnerPack: TLevelPack;
  protected
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property OwnerPack: TLevelPack read GetOwnerPack;
//    property LevelInfo[Index: Integer] read
  published
    property SourceFileName: string read fSourceFileName write fSourceFileName;
    property SectionName: string read fSectionName write fSectionName;
    property LevelInfos: TLevelInfos read fLevelInfos write fLevelInfos; // tricky?
  end;

  TPackSections = class(TOwnedCollectionEx)
  private
    function GetItem(Index: Integer): TPackSection;
    procedure SetItem(Index: Integer; const Value: TPackSection);
  protected
  public
    constructor Create(aOwner: TPersistent); // owner = TLevelPack
    function Add: TPackSection;
    function Insert(Index: Integer): TPackSection;
    property Items[Index: Integer]: TPackSection read GetItem write SetItem; default;
  published
  end;

  TLevelPack = class(TCollectionItem)
  private
    fPackSections: TPackSections;
    fLevelPackName: string;
    fSourceFileName: string;
    function GetPackSection(Index: Integer): TPackSection;
  protected
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property PackSection[Index: Integer]: TPackSection read GetPackSection; default;
  published
    property SourceFileName: string read fSourceFileName write fSourceFileName stored True; // only lemmini
    property LevelPackName: string read fLevelPackName write fLevelPackName;
    property PackSections: TPackSections read fPackSections write fPackSections; // tricky?
  end;

  TLevelPacks = class(TOwnedCollectionEx)
  private
    function GetItem(Index: Integer): TLevelPack;
    procedure SetItem(Index: Integer; const Value: TLevelPack);
  protected
  public
    constructor Create(aOwner: TPersistent);
    function Add: TLevelPack;
    function Insert(Index: Integer): TLevelPack;

    procedure SaveToStream(S: TStream);
    procedure SaveToTxtFile(const aFileName: string);

    property Items[Index: Integer]: TLevelPack read GetItem write SetItem; default;
  published
  end;

  {-------------------------------------------------------------------------------
    TBaseLemmingStyle is in fact a lemming-system. (maybe rename)
    Supported styles must inherit from this class.
    Published properties are streamed to inifile by stylemgr.
    GraphicSets can be compiled
    LevelPacks (and maybe more) can be compiled
    Keep this compile-things in the public section
  -------------------------------------------------------------------------------}
  TBaseLemmingStyleClass = class of TBaseLemmingStyle;
  TBaseLemmingStyle = class(TPersistent)
  private
  { property access graphics }
    function GetGraphicSetCount: Integer;
    function GetGraphicSet(aIndex: Integer): TBaseGraphicSet;
  protected
    fTestEXE: string;
    fGraphicSetList   : TBaseGraphicSetList; // available graphics for this system
    fAnimationSet     : TBaseAnimationSet; // available lemming animations for this system
    fLevelLoader      : TBaseLevelLoader; // available levels for this system
  { published props }
    fStyleName        : string;
    fStyleDescription : string;
    fCommonPath       : string;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    class procedure GetStyleParams(var aParams: TDefaultsRec); virtual;
    class function GetParams: TDefaultsRec;  // wrapper
    class function GetLevelLoaderClass: TBaseLevelLoaderClass; virtual;
    class function GetAnimationSetClass: TBaseAnimationSetClass; virtual;

    function FindGraphicSet(aGraphicSetId: Integer; SilentFail: Boolean = false): TBaseGraphicSet;
    function FindGraphicSetName(aGraphicSetName: String; SilentFail: Boolean = false): TBaseGraphicSet;
    procedure DebugGraphs;
    function IncludeCommonPath(const S: string): string;
    function GraphByName(const aGraphName: string): TBaseGraphicSet;
    // class function GetDefaultGraphicSetClass: TBaseGraphicSetClass; virtual;

  { properties graphic sets }
    property GraphicSetList: TBaseGraphicSetList read fGraphicSetList;
    property GraphicSetCount: Integer read GetGraphicSetCount;
    property GraphicSets[aIndex: Integer]: TBaseGraphicSet read GetGraphicSet; default;

  { animation graphics }
    property AnimationSet: TBaseAnimationSet read fAnimationSet;

  { properties levels }
    property LevelLoader: TBaseLevelLoader read fLevelLoader;

  published
    property StyleName: string read fStyleName write fStyleName;
    property StyleDescription: string read fStyleDescription write fStyleDescription;
    property CommonPath: string read fCommonPath write fCommonPath;
    property TestEXE: string read fTestEXE write fTestEXE;
  end;

  TLemmingStyleList = class(TObjectList)
  private
    function GetItem(Index: Integer): TBaseLemmingStyle;
  protected
  public
    function Add(Item: TBaseLemmingStyle): Integer;
    procedure Insert(Index: Integer; Item: TBaseLemmingStyle);
    property Items[Index: Integer]: TBaseLemmingStyle read GetItem; default;
  published
  end;

  {-------------------------------------------------------------------------------
    Supported styles and graphicsets go in the singleton StyleMgr
  -------------------------------------------------------------------------------}
  TStyleMgr = class(TPersistent)
  private
    fCurrentSection : string; // temp needed for foreachproperty event
    fCurrentIni     : TMemIniFile; // temp needed for foreachproperty event
    procedure ScanWriteStyleProperty(aObject: TObject; const aPropName: string; aPropType: TTypeKind);
    procedure ScanWriteGraphProperty(aObject: TObject; const aPropName: string; aPropType: TTypeKind);
    function GetStyle(aIndex: Integer): TBaseLemmingStyle;
  protected
    fStyleNameList : TStringList;
    fStyleList     : TLemmingStyleList;
  public
    constructor Create;
    destructor Destroy; override;
    //procedure RegisterLevelExtension(aLevelClass)
    procedure SaveToIni(const aFilename: string);
    procedure LoadFromIni(const aFilename: string);
    procedure SetGlobalIndicesAndVars;
    function StyleIndex(aStyle: TBaseLemmingStyle): Integer;
    function FindStyleByName(const aStyleName: string): TBaseLemmingStyle;
    function StyleByName(const aStyleName: string): TBaseLemmingStyle;
    function GraphByName(const aStyleName, aGraphName: string): TBaseGraphicSet;
    procedure LoadStyleInfo;
    property StyleList: TLemmingStyleList read fStyleList;
    property Styles[aIndex: Integer]: TBaseLemmingStyle read GetStyle; default;

//    property DosOrigStyle: TBaseLemmingStyle;

  published
  end;

  {-------------------------------------------------------------------------------
    TMetaObject en TMetaTerrain are the 2 most important building stones of the
    lemmings game. In Dos they were stored in the ground??.dat files.
    Most properties come from the dos-ground.dat-files description

    animationflags: its seems that:
      0 = do not animate
      1 = animate triggered by lemming
      2 = animate continuously
      3 = animate once on beginning (entry)

  -------------------------------------------------------------------------------}
  TMetaObject = class(TCollectionItem)
  private
  protected
  { properties that are in ye ol' dos files (ground?.dat) }
    fAnimationFlags               : Integer; // check dos doc
    fStartAnimationFrameIndex     : Integer; // animation
    fEndAnimationFrameIndex       : Integer; // number of animations, rename if sure!!!
       { TODO : rename this property, it is count }

    fWidth                        : Integer;
    fHeight                       : Integer;
    fAnimationFrameDataSize       : Integer; // the datasize in bytes of one frame
    fMaskOffsetFromImage          : Integer; // the datalocation of the mask-bitmap relative to the animation
    fTriggerLeft                  : Integer; // encoded in dos!!!
    fTriggerTop                   : Integer; // encoded in dos!!!
    fTriggerWidth                 : Integer; // encoded in dos!!!
    fTriggerHeight                : Integer; // encoded in dos!!!
    fTriggerEffect                : Integer; // see dos doc
    fAnimationFramesBaseLoc       : Integer; // location of first frame in file
    fPreviewFrameIndex            : Integer; // index of preview
    fTrapSoundEffect              : Integer; // see dos doc
    fTriggerNext                  : Integer;
  public
    procedure Assign(Source: TPersistent); override;
  published
  { dos derived }
    property AnimationFlags           : Integer read fAnimationFlags write fAnimationFlags;
    property StartAnimationFrameIndex : Integer read fStartAnimationFrameIndex write fStartAnimationFrameIndex;
    property EndAnimationFrameIndex   : Integer read fEndAnimationFrameIndex write fEndAnimationFrameIndex;
    property Width                    : Integer read fWidth write fWidth;
    property Height                   : Integer read fHeight write fHeight;
    property AnimationFrameDataSize   : Integer read fAnimationFrameDataSize write fAnimationFrameDataSize;
    property MaskOffsetFromImage      : Integer read fMaskOffsetFromImage write fMaskOffsetFromImage;
    property TriggerLeft              : Integer read fTriggerLeft write fTriggerLeft;
    property TriggerTop               : Integer read fTriggerTop write fTriggerTop;
    property TriggerWidth             : Integer read fTriggerWidth write fTriggerWidth;
    property TriggerHeight            : Integer read fTriggerHeight write fTriggerHeight;
    property TriggerNext              : Integer read fTriggerNext write fTriggerNext;
    property TriggerEffect            : Integer read fTriggerEffect write fTriggerEffect;
    property AnimationFramesBaseLoc   : Integer read fAnimationFramesBaseLoc write fAnimationFramesBaseLoc;
    property PreviewFrameIndex        : Integer read fPreviewFrameIndex write fPreviewFrameIndex;
    property TrapSoundEffect          : Integer read fTrapSoundEffect write fTrapSoundEffect;
  end;

  TMetaObjectCollection = class(TOwnedCollectionEx)
  private
    function GetItem(Index: Integer): TMetaObject;
    procedure SetItem(Index: Integer; const Value: TMetaObject);
  protected
  public
    constructor Create(aOwner: TPersistent);
    function Add: TMetaObject;
    function Insert(Index: Integer): TMetaObject;
    property Items[Index: Integer]: TMetaObject read GetItem write SetItem; default;
  published
  end;

  TMetaTerrain = class(TCollectionItem)
  private
    fWidth: Integer;
    fHeight: Integer;
    fImageLocation: Integer; // dos-history
    fMaskLocation: Integer;
    fUnknown: Integer;
  protected
  public
    procedure Assign(Source: TPersistent); override;
  published
    property Width: Integer read fWidth write fWidth;
    property Height: Integer read fHeight write fHeight;
    property ImageLocation: Integer read fImageLocation write fImageLocation;
    property MaskLocation: Integer read fMaskLocation write fMaskLocation;
    property Unknown: Integer read fUnknown write fUnknown;
  end;

  TMetaTerrainCollection = class(TOwnedCollectionEx)
  private
    function GetItem(Index: Integer): TMetaTerrain;
    procedure SetItem(Index: Integer; const Value: TMetaTerrain);
  protected
  public
    constructor Create(aOwner: TPersistent);
    function Add: TMetaTerrain;
    function Insert(Index: Integer): TMetaTerrain;
    property Items[Index: Integer]: TMetaTerrain read GetItem write SetItem; default;
  published
  end;

  {-------------------------------------------------------------------------------
    Virtualized graphic set.
  -------------------------------------------------------------------------------}
  TGraphicSetState = (
    gsForceDirectAccess,
    gsCompiling
  );
  TGraphicSetStates = set of TGraphicSetState;

  TBaseGraphicSet = class(TPersistent)
  private
    fForceDirectAccess: Boolean; // needed when compiling
    fFetchPrepared: Boolean;
  { internal }
    procedure GetArchivedTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32);
    procedure GetArchivedBackGroundBitmap(aBitmap: TBitmap32);
    procedure GetArchivedObjectBitmap(aIndex: Integer; aBitmap: TBitmap32);
    procedure GetArchivedMetaData;
    function DoReadMetaData: Boolean;
    function DoReadData: Boolean;
  { property access }
    procedure SetMetaObjectCollection(const Value: TMetaObjectCollection);
    procedure SetMetaTerrainCollection(const Value: TMetaTerrainCollection);
    function GetFullArchiveName: string;
  protected
  { stream props }
    fMetaObjectCollection       : TMetaObjectCollection; // metadata interactive objects
    fMetaTerrainCollection      : TMetaTerrainCollection; // metadata terrains
    fGraphicSetId               : Integer; // must be used for identification when loading level
    fGraphicSetIdExt            : Integer; // must be used for identification of extended graphics
    fGraphicSetOrderNumber      : Integer; // ordering number, as an extra layer for resolving order conflicts
    fGraphicSetName             : string; // must be used for identification when loading level
    fGraphicSetArchive          : string; // filename of compiled version of this graphicset, must be in subdir (compile)
//    fBrickColor                 : TColor32;
    fBackgroundColor            : TColor32;
    fGraphicSetIntName: string;
    fGraphicSetAltName: string;
  { runtime props }
    fObjectBitmapCache          : TObjectList;
    fTerrainBitmapCache         : TObjectList;
    fBackGroundBitmapCache      : TBitmap32; // in the first place for extended graphics
    fActivated                  : Boolean; // true if metadata is read once
    fOwnerStyle                 : TBaseLemmingStyle;
    fIniProps                   : TStringList;
    fIsSpecial                  : Boolean;
    fNil                        : String; //dummy string, kludgy workaround for blanking the graphic set archive
    function IncludeCommonStylePath(const S: string): string;
    function GetInternalName: string;
  public
    constructor Create(aOwnerStyle: TBaseLemmingStyle); virtual; // watch out, it's virtual!
    destructor Destroy; override;
  { key read methods }
    procedure DirectReadMetaData; virtual; // must be overridden
    procedure DirectReadData; virtual; // can be overridden
    procedure DirectGetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32); virtual; // must be overridden
    procedure DirectGetObjectMaskBitmap(aIndex: Integer; aBitmap: TBitmap32); virtual; // must be overridden
    procedure DirectGetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32); virtual; // must be overridden
    procedure DirectGetBackGroundBitmap(aBitmap: TBitmap32); virtual; // must be overridden
  { key write methods }
    procedure DirectWriteMetaData(const aFileName: string = ''); virtual; abstract;
  { public methods for key methods }
    procedure EnsureMetaData;
    procedure EnsureData;

    function GetObjectBitmapRef(aIndex: Integer): TBitmap32;
    function GetTerrainBitmapRef(aIndex: Integer): TBitmap32;
    function GetBackGroundBitmapRef: TBitmap32;

    procedure GetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32);
    procedure GetBackGroundBitmap(aBitmap: TBitmap32);
    procedure GetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32);
    procedure GetObjectFrame(aIndex, aFrame: Integer; aBitmap: TBitmap32);

    procedure SaveToFile(const aFileName: string);
    procedure SaveToStream(S: TStream);
    procedure SaveToTxtFile(const aFileName: string);
    procedure LoadFromFile(const aFileName: string);
    procedure LoadFromStream(S: TStream);

    function GetCompleteFileName(const aFileName: string): string;
    //function CompatibleWith(aGraph: TBaseGraphicSet): Boolean;

  { public properties }
    property OwnerStyle: TBaseLemmingStyle read fOwnerStyle;
    //property Params: TStrings read fParams write SetParams; // obsolete for now
    property TerrainBitmaps[aIndex: Integer]: TBitmap32 read GetTerrainBitmapRef;
    property ObjectBitmaps[aIndex: Integer]: TBitmap32 read GetObjectBitmapRef;
    property ForceDirectAccess: Boolean read fForceDirectAccess write fForceDirectAccess;

    property ObjectBitmapCache: TObjectList read fObjectBitmapCache;
    property TerrainBitmapCache: TObjectList read fTerrainBitmapCache;
    property FullArchiveName: string read GetFullArchiveName;
    property GraphicSetArchive: string read fNil write fGraphicSetArchive;
  published
    property MetaObjectCollection: TMetaObjectCollection read fMetaObjectCollection write SetMetaObjectCollection;
    property MetaTerrainCollection: TMetaTerrainCollection read fMetaTerrainCollection write SetMetaTerrainCollection;
    property GraphicSetId: Integer read fGraphicSetId write fGraphicSetId;
    property GraphicSetIdExt: Integer read fGraphicSetIdExt write fGraphicSetIdExt;
    property GraphicSetName: string read fGraphicSetName write fGraphicSetName;
    property GraphicSetAltName: string read fGraphicSetAltName write fGraphicSetAltName;
    property GraphicSetOrderNumber: Integer read fGraphicSetOrderNumber write fGraphicSetOrderNumber;
    property GraphicSetInternalName: string read GetInternalName write fGraphicSetIntName;
//    property BrickColor: TColor32 read fBrickColor write fBrickColor;
    property BackgroundColor: TColor32 read fBackgroundColor write fBackgroundColor;
    property IsSpecial: Boolean read fIsSpecial write fIsSpecial;
  end;

  TBaseGraphicSetList = class(TObjectList)
  private
    function GetItem(Index: Integer): TBaseGraphicSet;
  protected
  public
    function Add(Item: TBaseGraphicSet): Integer;
    procedure Insert(Index: Integer; Item: TBaseGraphicSet);
    function FindByName(const aName: string): TBaseGraphicSet;
    property Items[Index: Integer]: TBaseGraphicSet read GetItem; default;
  published
  end;

  {-------------------------------------------------------------------------------
    Metadata on animations
  -------------------------------------------------------------------------------}
  TMetaAnimationRec = record
    mFrameCount      : Integer; // number of frames
    mWidth           : Integer; // width of a single frame picture
    mHeight          : Integer; // height of a single frame
    mDescription     : string;  // for fun
    mImageLocation   : Integer; // dos only
    mBitsPerPixel    : Integer; // dos only

  { game machanics helpers, so we do not have to hard code it in the game }
    mAnimationType   : Integer; // 0 = loop;  1 = once (state change if finished)
    mFootX           : Integer;
    mFootY           : Integer;

    mHotX            : Integer;
    mHotY            : Integer;

    mAssociatedState : Integer; // which state does this animation represent?
    mMaskFrame       : Integer; // frameindex after which we put a mask in the world
    mTriggerHeight   : Integer; // height wich triggers collisions with objects


  { mask things }
    mHasMask         : Boolean;
  end;

  TMetaAnimation = class(TCollectionItem)
  private
    fFrameCount     : Integer; // number of frames
    fWidth          : Integer; // width of a single frame picture
    fHeight         : Integer; // height of a single frame
    fDescription    : string;  // for fun
    fImageLocation  : Integer; // dos only
    fBitsPerPixel   : Integer; // dos only

  { game machanics helpers, so we do not have to hard code it in the game }
    fAnimationType   : Integer; // 0 = loop;  1 = once (state change if finished)
    fFootX           : Integer;
    fFootY           : Integer;

    fHotX            : Integer;
    fHotY            : Integer;

    fAssociatedState : Integer; // which state does this animation represent?
    fMaskFrame       : Integer; // frameindex after which we put a mask in the world


    fTriggerHeight   : Integer; // height wich triggers colissions with objects


  { mask things }
    fHasMask         : Boolean;

//    fMask
//    fRec: TMetaAnimationRec;

  protected
  public
  published



    property FrameCount       : Integer read fFrameCount write fFrameCount;
    property Width            : Integer read fWidth write fWidth;
    property Height           : Integer read fHeight write fHeight; // height of a single frame
    property BitsPerPixel     : Integer read fBitsPerPixel write fBitsPerPixel;
    property ImageLocation    : Integer read fImageLocation write fImageLocation;
    property Description      : string read fDescription write fDescription;
  { mechanics }
    property AnimationType    : Integer read fAnimationType write fAnimationType;
    property FootX            : Integer read fFootX write fFootX;
    property FootY            : Integer read fFootY write fFootY;
    property TriggerHeight    : Integer read fTriggerHeight write fTriggerHeight;


    (*
    property FrameCount       : Integer read fRec.mFrameCount write fRec.mFrameCount;
    property Width            : Integer read fRec.mWidth write fRec.mWidth;
    property Height           : Integer read fRec.mHeight write fRec.mHeight;
    property BitsPerPixel     : Integer read fRec.mBitsPerPixel write fRec.mBitsPerPixel;
    property ImageLocation    : Integer read fRec.mImageLocation write fRec.mImageLocation;
    property Description      : string read  fRec.mDescription write fRec.mDescription;
  { mechanics }
    property AnimationType    : Integer read fRec.mAnimationType write fRec.mAnimationType;
    property FootX            : Integer read fRec.mFootX write fRec.mFootX;
    property FootY            : Integer read fRec.mFootY write fRec.mFootY;
    property TriggerHeight    : Integer read fRec.mTriggerHeight write fRec.mTriggerHeight;
      *)
  end;

  TMetaAnimations = class(TOwnedCollectionEx)
  private
    function GetItem(Index: Integer): TMetaAnimation;
    procedure SetItem(Index: Integer; const Value: TMetaAnimation);
  protected
  public
    constructor Create(aOwner: TPersistent);
    function Add: TMetaAnimation;
    function Insert(Index: Integer): TMetaAnimation;
    property Items[Index: Integer]: TMetaAnimation read GetItem write SetItem; default;
  published
  end;

  {-------------------------------------------------------------------------------
    Basic animations
  -------------------------------------------------------------------------------}
  TBaseAnimationSet = class(TPersistent)
  private
//    procedure SetBrickColor(aColor: TColor32);
    function GetAnimationBitmapRef(Index: Integer): TBitmap32;
    function GetMaskBitmapRef(Index: Integer): TBitmap32;
  protected
    fMetaAnimations     : TMetaAnimations;
    fAnimationCache     : TObjectList; // list of bitmaps
//    fMaskCache          : TObjectList;

    fCountDownDigits    : TBitmap32;
    fExplosionMask      : TBitmap32;
    fBashMasks          : TBitmap32;
    fBashMasksRTL       : TBitmap32;
    fMineMasks           : TBitmap32;
    fMineMasksRTL        : TBitmap32;


    fOwnerStyle         : TBaseLemmingStyle;
    fMetaDataActivated  : Boolean;

//    fBrickColor         : TColor32;
  { internals }
    procedure InternalReadMetaData; virtual; abstract;
    procedure InternalReadData; virtual; abstract;
    procedure InternalClearData; virtual; abstract;
  public
    constructor Create(aOwnerStyle: TBaseLemmingStyle); virtual; // watch out, it's virtual!
    destructor Destroy; override;

    procedure EnsureMetaData;
    procedure ReadData;
    procedure ClearData;

    property MetaAnimations: TMetaAnimations read fMetaAnimations;

    property AnimationBitmaps[Index: Integer]: TBitmap32 read GetAnimationBitmapRef;
    property MaskBitmaps[Index: Integer]: TBitmap32 read GetMaskBitmapRef;
    procedure GetAnimationFrame(aIndex, aFrame: Integer; aBitmap: TBitmap32);

    property OwnerStyle: TBaseLemmingStyle read fOwnerStyle;
//    property BrickColor: TColor32 read fBrickColor write SetBrickColor;


    property AnimationCache: TObjectList read fAnimationCache; // direct access
    property CountDownDigits: TBitmap32 read fCountDownDigits;
//masks
    property ExplosionMask: TBitmap32 read fExplosionMask;

      // temporary solution 4 bashmasks
    property BashMasks: TBitmap32 read fBashMasks; // 4 times 16x10
    property BashMasksRTL: TBitmap32 read fBashMasksRTL; // 4 times 16x10

    property MineMasks: TBitmap32 read fMineMasks; // 2 times 16x13
    property MineMasksRTL: TBitmap32 read fMineMasksRTL; // 2 times 16x13

  published
  end;

  {-------------------------------------------------------------------------------
    This one loads all levelinfo, not very smart but it's a beginning
  -------------------------------------------------------------------------------}
  TBaseLevelLoader = class(TOwnedPersistent)
  private
    function GetLevelPackCount: Integer;
    //procedure Prepare
  protected
    fLevelPacks       : TLevelPacks;
    fPrepared         : Boolean;
    procedure InternalPrepare; virtual; abstract;
    function InternalGetLevelInfo(aPack, aSection, aLevel: Integer): TLevelInfo; virtual; abstract;
    procedure InternalLoadLevel(aInfo: TLevelInfo; aLevel: TLevel); virtual; abstract;
    procedure InternalLoadSingleLevel(aPack, aSection, aLevelIndex: Integer; aLevel: TLevel); virtual; abstract;
  public
    constructor Create(aOwner: TPersistent);
    destructor Destroy; override;

    procedure Prepare;
    procedure LoadLevel(aInfo: TLevelInfo; aLevel: TLevel);
    procedure LoadSingleLevel(aPack, aSection, aLevelIndex: Integer; aLevel: TLevel);

    property LevelPacks: TLevelPacks read fLevelPacks;
    property LevelPackCount: Integer read GetLevelPackCount;
  published
  end;

  // cache of pictures and animations
  TStyleCache = class
  private
  protected
  public
    GraphicSetId    : Integer;
    IsCached        : Boolean;
    TerrainBitmaps  : TObjectList;
    ObjectBitmaps   : TObjectList; // this contains only the first frames of moving objects!
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
  published
  end;

  TStyleCacheList = class(TObjectList)
  private
    function GetItem(Index: Integer): TStyleCache;
  protected
  public
    function Add(Item: TStyleCache): Integer;
    procedure Insert(Index: Integer; Item: TStyleCache);
    property Items[Index: Integer]: TStyleCache read GetItem; default;
    property List;
  published
  end;

  TPictureManagerOption = (
    pmoArchives // use own archiveformat for styles
  );
  TPictureManagerOptions = set of TPictureManagerOption;

  // singleton housekeeper of all pictures ()
  TPictureMgr = class
  private
    fErrorBitmap: TBitmap32;
    fResolution: TResolution;
    function GetCacheForGraphicSet(aSet: Integer): TStyleCache;
    function ForceCacheForGraphicSet(aGraphicSet: Integer): TStyleCache;
    function GetTerrainBitmap32Ref(aGraphicSet, aTerrainId: Integer): TBitmap32;
    function GetTerrainBitmapCount(aGraphicSet: Integer): Integer;
    function GetObjectBitmap32Ref(aGraphicSet, aObjectId: Integer): TBitmap32;
    function GetObjectBitmapCount(aGraphicSet: Integer): Integer;
    procedure SetResolution(const Value: TResolution);
    function GetExtendedGraphicsBitmap32Ref(aExtendedGraphicSet: Integer): TBitmap32;
  protected
    StyleCacheList: array[TGraphicSetRange] of TStyleCache; // caching of the default styles
    ExtendedGraphicsCache: array[0..3] of TBitmap32; // caching of extended graphics
  public
    FEPalette     : TLemmingPalette; {fe.pal}
    Level0Palette : TLemmingPalette; {level0.pal}
    Level1Palette : TLemmingPalette; {level1.pal}
    Level2Palette : TLemmingPalette; {level2.pal}
    Level3Palette : TLemmingPalette; {level3.pal}
    Level4Palette : TLemmingPalette; {level3.pal}
    RockPalette   : TLemmingPalette; {brick.pal}
    BrickPalette  : TLemmingPalette; {brick.pal}
    BubblePalette : TLemmingPalette; {bubble.pal}
    SnowPalette   : TLemmingPalette; {snow.pal}
//    WalkerAnimation: TLemmingAnimation;
//    WalkerAnimationR: TLemmingAnimation;
//    FallerAnimation: TLemmingAnimation;
    constructor Create;
    destructor Destroy; override;
    procedure LoadGraphics;
    procedure UnLoadGraphics;
    procedure CacheGraphicSet(aCache: TStyleCache; aGraphicSet: Integer);
    procedure GetTerrainBitmap32(aGraphicSet, aTerrainId: Integer; Bmp: TBitmap32);
    procedure GetObjectBitmap32(aGraphicSet, aObjectId: Integer; Bmp: TBitmap32);
    function TerrainIsSteel(aGraphicSet, aTerrainId: Integer): Boolean;
    procedure SkinTest(aBitmap: TBitmap32);

    property TerrainBitmaps[aGraphicSet, aTerrainId: Integer]: TBitmap32 read GetTerrainBitmap32Ref;
    property TerrainBitmapCount[aGraphicSet: Integer]: Integer read GetTerrainBitmapCount;
    property ObjectBitmaps[aGraphicSet, aObjectId: Integer]: TBitmap32 read GetObjectBitmap32Ref;
    property ObjectBitmapCount[aGraphicSet: Integer]: Integer read GetObjectBitmapCount;

    property ExtendedGraphicsBitmap[aExtendedGraphicSet: Integer]: TBitmap32 read GetExtendedGraphicsBitmap32Ref;

  published
    //property Resolution: TResolution read fResolution write SetResolution;
  end;

  TArchiveCompilerStateRec = record
    cState: string; // fetching metadata, fetching bitmaps etc.
    cBusy: Boolean;
    cPosition: Integer; // done
    cMaximum: Integer; // todo
    cDetail: string;
  end;

  TCompileProgressEvent = procedure(Sender: TObject; const aState: TArchiveCompilerStateRec) of object;

  TArchiveCompiler = class
  private
    StateRec: TArchiveCompilerStateRec;
    fOnProgress: TCompileProgressEvent;
    fMgr: TCustomProgressManager;
    procedure ClearStateRec;
    procedure DoProgress;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    procedure CompileArchive(aStyle: TBaseLemmingStyle; aGraph: TBaseGraphicSet);
    procedure CompileMetaFile(aStyle: TBaseLemmingStyle; aGraph: TBaseGraphicSet; AddToArchive: Boolean);
    property Mgr: TCustomProgressManager read fMgr;
  published
    property OnProgress: TCompileProgressEvent read fOnProgress write fOnProgress;
  end;

  TRenderItemEvent = procedure(Sender: TObject; B: TBaseLevelItem; var DoRender: Boolean) of object;
  TLevelRenderer = class(TComponent)
  private
    TempBitmap            : TBitmap32; // used for drawing
    SteelBitmap           : TBitmap32;
    TriggerBitmap         : TBitmap32;
    fRenderOptions        : TRenderOptions;
    fInternalLevelBitmap  : TBitmap32;
    fRenderX              : Integer;
    fRenderY              : Integer;
    fRenderTypes          : TLevelItemTypes;
    fOnRenderItem         : TRenderItemEvent;
    fCollisionGrid        : TByteMap;
    fPreview: Boolean;
  { internal }
    procedure ResizeSteelBitmap(aWidth, aHeight: Integer);
    procedure ResizeTriggerBitmap(aWidth, aHeight: Integer);
  { terrain drawing combines }
    procedure CombineTerrainNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineTerrainErase(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineTerrainOverwrite(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineTerrainEraseNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineTerrainGhosted(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineTerrainNoOverwriteOWW(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineTerrainOverwriteOWW(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineTerrainGhostedOWW(F: TColor32; var B: TColor32; M: TColor32);
    function GetTerrainCombineEvent(aFlags: TTerrainDrawingFlags; OWI: Boolean = false): TPixelCombineEvent;
  { object drawing combines }
    procedure CombineObjectNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineObjectOnlyOnTerrain(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineObjectOneWayArrow(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineObjectOverwrite(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineObjectGhosted(F: TColor32; var B: TColor32; M: TColor32);
    function GetObjectCombineEvent(aFlags: TObjectDrawingFlags): TPixelCombineEvent;
  { steel drawing combines }
    procedure CombineSteelNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineSteelOverwrite(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineNegSteelNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
    procedure CombineNegSteelOverwrite(F: TColor32; var B: TColor32; M: TColor32);
    function GetSteelCombineEvent: TPixelCombineEvent;
  { triggerarea combines }
    procedure CombineTriggerArea(F: TColor32; var B: TColor32; M: TColor32);
  { collisiongrid drawing combines }
    procedure CombineGridAndTerrain(F: TColor32; var B: TColor32; M: TColor32);
  { bitmap building }
    procedure DrawInteractiveObject(aLevel: TLevel; aObject: TInteractiveObject; aFrame: Integer = 0);
    procedure DrawTerrain(aTerrain: TTerrain; OWI: Boolean = false);
    procedure DrawSteel(aLevel: TLevel; aSteel: TSteel);
    procedure DrawTriggerArea(O: TInteractiveObject; MO: TMetaObject);

    procedure SetRenderOptions(const Value: TRenderOptions);
  protected
    fLevelBitmap: TBitmap32;
  public
    EraseColor : TColor32;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;


    //procedure GetDraggerBitmap(aLevel: TLevel; Bmp: TBitmap; aList: TList);
    function HitTest(aLevel: TLevel; X, Y: Integer; aList: TList;
      aTypes: TLevelItemTypes = [litObject, litTerrain, litSteel]; aMaxCount: Integer = 1): Integer;


    procedure RenderLevel(aLevel: TLevel);
    procedure RenderToFile(aLevel: TLevel; const aFileName: String);

  { specialized/optimized rendering before play}
    procedure RenderWorld(aLevel: TLevel);
    procedure RenderAnimatedObject(aLevel: TLevel; aObject: TInteractiveObject; aFrame: Integer; aTarget: TBitmap32 = nil);
    procedure RenderAnimatedObjectDst(aLevel: TLevel; aObject: TInteractiveObject; aFrame: Integer; aTarget: TBitmap32;
      const aRect: TRect);

    procedure RenderDosPreview(aLevel: TLevel);

  published
    property RenderOptions: TRenderOptions read fRenderOptions write SetRenderOptions;
    property RenderTypes: TLevelItemTypes read fRenderTypes write fRenderTypes;// default [litObject, liTerrain, litSteel];
    property LevelBitmap: TBitmap32 read fLevelBitmap write fLevelBitmap;
    property CollisionGrid: TByteMap read fCollisionGrid write fCollisionGrid;
    property Preview: Boolean read fPreview write fPreview;
  end;


function FileNameToFormat(const aFileName: string): TLemmingFileFormat;

function ConvertCoord(FromResolution, ToResolution: Single; N: Integer): Integer;


procedure InitializeCore;
procedure FinalizeCore;

var
  PicMgr     : TPictureMgr;
  StyleMgr  : TStyleMgr;
  MessageMgr     : TMessageManager;

const
  AllItemTypes = [litObject, litTerrain, litSteel];

{var
  SupportedStyles: array[TSupportedStyles] of TBaseLemmingStyle;}
function AppPath: string;
function CompilePath: string;
function SortGraphicSets(P1, P2: Pointer): Integer;


implementation

uses
    gr32_blend,
  lemapp,
  UFastStrings,
  LemStrings,
  ZLibEx, UZip,
  lemstyles, lemdosstyles, lemlemministyles,
  FNewLevel,
    LemDosFiles;

var
  _AppPath: string;
  _CompilePath: string;

function AppPath: string;
begin
  if _AppPath = '' then
    _AppPath := ApplicationPath;
  Result := _AppPath;
end;

function CompilePath: string;
begin
  if _CompilePath = '' then
    _CompilePath := AppPath + 'Compile\';
  Result := _CompilePath;
end;

const
  COLORMASK = $00FFFFFF;

type
  ELevelError = class(Exception);

procedure LevelError(const S: string; const Args: array of const);
var
  Msg: string;
begin
  Msg := SafeFormatStr(S, Args);
  raise ELevelError.Create(Msg);
end;

function FileNameToFormat(const aFileName: string): TLemmingFileFormat;
var
  Ext: string;
begin
  Ext := ExtractFileExt(aFileName);
  Result := lffAutoDetect;
  if CompareText(Ext, '.ini') = 0 then
    Result := lffLemmini
  else if CompareText(Ext, '.lvl') = 0 then
    Result := lffLVL;
end;

function ConvertCoord(FromResolution, ToResolution: Single; N: Integer): Integer;
begin
  Result := N;
  if FromResolution <> ToResolution then
    Result := Trunc(N * (ToResolution/FromResolution));
end;

procedure InitializeCore;
begin
  {$ifdef develop}
  LemmixDebugMode := True;
  {$endif}
  RegisterStyleClasses;
  MessageMgr := TMessageManager.Create(nil);
  PicMgr := TPictureMgr.Create;
  StyleMgr := TStyleMgr.Create;
  StyleMgr.LoadStyleInfo;
  //StyleMgr.LoadFromIni(AppPath + 'NeoLemmixStyles.ini');
end;

procedure FinalizeCore;
begin
  //StyleMgr.SaveToIni(AppPath + 'NeoLemmixStyles.ini');
  FreeAndNil(StyleMgr);
  FreeAndNil(PicMgr);
  FreeAndNil(MessageMgr);
end;

{-------------------------------------------------------------------------------
  Internal classes for streaming
  o GraphicSets
  o LevelPacks
-------------------------------------------------------------------------------}
type
  TBaseGraphicSetWrapper = class(TComponent)
  private
    fGraph: TBaseGraphicSet;
  public
    procedure SaveToFile(const aFileName: string);
    procedure SaveToTxtFile(const aFileName: string);
    procedure SaveToStream(S: TStream);
    procedure LoadFromFile(const aFileName: string);
    procedure LoadFromStream(S: TStream);
  published
    property Graph: TBaseGraphicSet read fGraph write fGraph;
  end;

  TLevelPacksWrapper = class(TComponent)
  private
    fLevelPacks: TLevelPacks;
  public
    procedure SaveToTxtFile(const aFileName: string);
    procedure SaveToStream(S: TStream);
  published
    property LevelPacks: TLevelPacks read fLevelPacks write fLevelPacks;
  end;

{ TBaseGraphicSetWrapper }

procedure TBaseGraphicSetWrapper.LoadFromFile(const aFileName: string);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFileName, fmOpenRead);
  try
    LoadFromStream(F);
  finally
    F.Free;
  end;
end;

procedure TBaseGraphicSetWrapper.LoadFromStream(S: TStream);
begin
  S.ReadComponent(Self);
end;

procedure TBaseGraphicSetWrapper.SaveToFile(const aFileName: string);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFileName, fmCreate);
  try
    SaveToStream(F);
  finally
    F.Free;
  end;
end;

procedure TBaseGraphicSetWrapper.SaveToStream(S: TStream);
begin
  S.WriteComponent(Self);
end;

procedure TBaseGraphicSetWrapper.SaveToTxtFile(const aFileName: string);
begin
  ComponentToTextFile(Self, aFileName);
end;

{ TLevelPacksWrapper }

procedure TLevelPacksWrapper.SaveToStream(S: TStream);
begin
  S.WriteComponent(Self);
end;

procedure TLevelPacksWrapper.SaveToTxtFile(const aFileName: string);
begin
  ComponentToTextFile(Self, aFileName);
end;

{ TLevelItem }

procedure TLevelItem.BeginUpdate;
begin
  Inc(fUpdateCount);
end;

procedure TLevelItem.Changed;
begin
  if (fUpdateCount = 0) and Assigned(fOnChange) then
    fOnChange(Self);
end;

constructor TLevelItem.Create(aOwner: TLevel);
begin
  inherited Create;
  fLevel := aOwner;
end;

procedure TLevelItem.EndUpdate;
begin
  Assert(fUpdateCount > 0, 'TLevelItem Unpaired EndUpdate');
  Dec(fUpdateCount);
end;

{ TLevelStatics }

constructor TLevelStatics.Create(aOwner: TLevel);
begin
  inherited;
  Clear;
end;

procedure TLevelStatics.Changed;
begin
  if fLevel <> nil then
    fLevel.MsgStaticsChanged;
end;

procedure TLevelStatics.ChangedInf(aChangeInfo: Integer);
begin
  if fLevel <> nil then
    fLevel.MsgStaticsChanged(aChangeInfo);
end;


procedure TLevelStatics.Clear;
var
  i: Integer;
begin
  Randomize;
  fReleaseRate               := 50;
  fLemmingsCount             := 40;
  fRescueCount               := 20;
  fTimeLimit                 := 5;
  fClimberCount              := 10;
  fFloaterCount              := 10;
  fBomberCount                := 10;
  fBlockerCount               := 10;
  fBuilderCount               := 10;
  fBasherCount                := 10;
  fMinerCount                 := 10;
  fDiggerCount                := 10;
  fWalkerCount                := 10;
  fSwimmerCount               := 10;
  fGliderCount                := 10;
  fMechanicCount              := 10;
  fStonerCount                := 10;
  fPlatformerCount            := 10;
  fStackerCount               := 10;
  fClonerCount                := 10;
  fFencerCount                := 10;
  fScreenPosition             := 632;
  fScreenYPosition            := 0;
  fGraphicSet                 := 0;
  fGraphicSetEx               := 0;
  fSuperLemming               := $0000;
  fAutoSteelOptions           := $E2;
  fSkillSet                   := 0;
  fInfiniteSkills             := 0;
  fMusicTrack                 := 0;
  fLevelWidth                 := 0;
  fLevelHeight                := 0;
  fFallDist                   := 126;
  fBoundaryT                  := 8;
  fBoundaryL                  := 0;
  fBoundaryR                  := -16;
  fBoundaryB                  := 20;
  fHasTimeLimit               := true;
  fVgaspecX                   := 304;
  fVgaspecY                   := 0;
  SetLength(fWindowOrder, 0);
  fPostSecretRank := 0;
  fPostSecretLevel := 0;
  fBnsRank := 0;
  fBnsLevel := 0;
  fClockStart := 0;
  fClockEnd := 0;
  fClockCount := 0;
  fLevelID := Random($7FFFFFFE) + Random($7FFFFFFF) + 1;
  fBackgroundIndex := 0;
  Changed;
end;

function TLevelStatics.GetLevelWidth: Integer;
begin
  //if fLevelWidth = 0 then fLevelWidth := 3200;
  Result := fLevelWidth;
end;

function TLevelStatics.GetLevelHeight: Integer;
begin
  //if fLevelHeight = 0 then fLevelHeight := 320;
  Result := fLevelHeight;
end;

function TLevelStatics.ValidReleaseRate(Value: Integer): Boolean;
begin
  Result := (Value >= MIN_RELEASERATE) and (Value <= MAX_RELEASERATE);
end;

function TLevelStatics.ValidLemmingsCount(Value: Integer): Boolean;
begin
  Result := (Value >= MIN_LEMMINGSCOUNT) and (Value <= MAX_LEMMINGSCOUNT);
end;

function TLevelStatics.ValidRescueCount(Value: Integer): Boolean;
begin
  Result := (Value >= MIN_RESCUECOUNT) and (Value <= MAX_RESCUECOUNT);
end;

function TLevelStatics.ValidTimeLimit(Value: Integer): Boolean;
begin
  Result := ((Value mod 256) >= MIN_TIMELIMIT) and
            ((Value shr 8) >= 0) and ((Value shr 8) <= 59);
end;

function TLevelStatics.ValidSkillCount(Value: Integer): Boolean;
begin
  Result := ((Value >= MIN_CLIMBERCOUNT) {and (Value <= MAX_CLIMBERCOUNT)) or (Value = 200});
end;

function TLevelStatics.ValidClimberCount(Value: Integer): Boolean;
begin
  Result := ((Value >= MIN_CLIMBERCOUNT) and (Value <= MAX_CLIMBERCOUNT)) or (Value = 200);
end;

function TLevelStatics.ValidFloaterCount(Value: Integer): Boolean;
begin
  Result := ((Value >= MIN_FLOATERCOUNT) and (Value <= MAX_FLOATERCOUNT)) or (Value = 200);
end;

function TLevelStatics.ValidBomberCount(Value: Integer): Boolean;
begin
  Result := ((Value >= MIN_BOMBERCOUNT) and (Value <= MAX_BOMBERCOUNT)) or (Value = 200);
end;

function TLevelStatics.ValidBlockerCount(Value: Integer): Boolean;
begin
  Result := ((Value >= MIN_BLOCKERCOUNT) and (Value <= MAX_BLOCKERCOUNT)) or (Value = 200);
end;

function TLevelStatics.ValidBuilderCount(Value: Integer): Boolean;
begin
  Result := ((Value >= MIN_BUILDERCOUNT) and (Value <= MAX_BUILDERCOUNT)) or (Value = 200);
end;

function TLevelStatics.ValidBasherCount(Value: Integer): Boolean;
begin
  Result := ((Value >= MIN_BASHERCOUNT) and (Value <= MAX_BASHERCOUNT)) or (Value = 200);
end;

function TLevelStatics.ValidMinerCount(Value: Integer): Boolean;
begin
  Result := ((Value >= MIN_MINERCOUNT) and (Value <= MAX_MINERCOUNT)) or (Value = 200);
end;

function TLevelStatics.ValidDiggerCount(Value: Integer): Boolean;
begin
  Result := ((Value >= MIN_DIGGERCOUNT) and (Value <= MAX_DIGGERCOUNT)) or (Value = 200);
end;

function TLevelStatics.ValidMusic(Value: Integer): Boolean;
begin
  Result := (Value >= MIN_MUSIC) and (Value <= MAX_MUSIC);
end;

function TLevelStatics.ValidGraphicSet(Value: Integer): Boolean;
begin

  // I really should be a better programmer!
  Result :=
    (fLevel <> nil) and
    (fLevel.Style <> nil) and
    (Value >= 0) and
    (Value <= fLevel.Style.GetGraphicSetCount - 1);
//  (Value >= GRAPHICSET_DIRT) and (Value <= GRAPHICSET_BUBBLE);
end;

procedure TLevelStatics.CheckReleaseRate(var Value: Integer);
begin
  if not ValidReleaseRate(Value) then
    LevelError(SErrorReleaseRate_d, [Value]);
end;

procedure TLevelStatics.CheckLemmingsCount(var Value: Integer);
begin
  if not ValidLemmingsCount(Value) then
    LevelError(SErrorLemmingsCount_d, [Value]);
end;

procedure TLevelStatics.CheckRescueCount(var Value: Integer);
begin
  if not ValidRescueCount(Value) then
    LevelError(SErrorRescueCount_d, [Value]);
end;

procedure TLevelStatics.CheckTimeLimit(var Value: Integer);
begin
  if not ValidTimeLimit(Value) then
    LevelError(SErrorTimeLimit_d, [Value]);
end;

procedure TLevelStatics.CheckSkillCount(var Value: Integer);
begin
  if not ValidSkillCount(Value) then
    LevelError(SErrorSkillCount_d, [Value]);
end;

procedure TLevelStatics.CheckClimberCount(var Value: Integer);
begin
  if not ValidClimberCount(Value) then
    LevelError(SErrorClimberCount_d, [Value]);
end;

procedure TLevelStatics.CheckFloaterCount(var Value: Integer);
begin
  if not ValidFloaterCount(Value) then
    LevelError(SErrorFloaterCount_d, [Value]);
end;

procedure TLevelStatics.CheckBomberCount(var Value: Integer);
begin
  if not ValidBomberCount(Value) then
    LevelError(SErrorBomberCount_d, [Value]);
end;

procedure TLevelStatics.CheckBlockerCount(var Value: Integer);
begin
  if not ValidBlockerCount(Value) then
    LevelError(SErrorBlockerCount_d, [Value]);
end;

procedure TLevelStatics.CheckBuilderCount(var Value: Integer);
begin
  if not ValidBuilderCount(Value) then
    LevelError(SErrorBuilderCount_d, [Value]);
end;

procedure TLevelStatics.CheckBasherCount(var Value: Integer);
begin
  if not ValidBasherCount(Value) then
    LevelError(SErrorBasherCount_d, [Value]);
end;

procedure TLevelStatics.CheckMinerCount(var Value: Integer);
begin
  if not ValidMinerCount(Value) then
    LevelError(SErrorMinerCount_d, [Value]);
end;

procedure TLevelStatics.CheckDiggerCount(var Value: Integer);
begin
  if not ValidDiggerCount(Value) then
    LevelError(SErrorDiggerCount_d, [Value]);
end;

procedure TLevelStatics.CheckMusic(var Value: Integer);
begin
  if not ValidMusic(Value) then
    LevelError(SErrorMusic_d, [Value]);
end;

procedure TLevelStatics.CheckGraphicSet(var Value: Integer);
begin
  if not ValidGraphicSet(Value) then
    LevelError(SErrorGraphicSet_d, [Value]);
end;

procedure TLevelStatics.SetReleaseRate(Value: Integer);
begin
  if Value > 99 then Value := 99;
  if fReleaseRate = Value then Exit;
  CheckReleaseRate(Value);
  fReleaseRate := Value;
  Changed;
end;

procedure TLevelStatics.SetLemmingsCount(Value: Integer);
begin
  if fLemmingsCount = Value then Exit;
  CheckLemmingsCount(Value);
  fLemmingsCount := Value;
  Changed;
end;

procedure TLevelStatics.SetRescueCount(Value: Integer);
begin
  if fRescueCount = Value then Exit;
  CheckRescueCount(Value);
  fRescueCount := Value;
  Changed;
end;

procedure TLevelStatics.SetTimeLimit(Value: Integer);
begin
  if fTimeLimit = Value then Exit;
  CheckTimeLimit(Value);
  fTimeLimit := Value;
  Changed;
end;

procedure TLevelStatics.SetWalkerCount(Value: Integer);
begin
  if fWalkerCount = Value then Exit;
  CheckSkillCount(Value);
  fWalkerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetSwimmerCount(Value: Integer);
begin
  if fSwimmerCount = Value then Exit;
  CheckSkillCount(Value);
  fSwimmerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetGliderCount(Value: Integer);
begin
  if fGliderCount = Value then Exit;
  CheckSkillCount(Value);
  fGliderCount := Value;
  Changed;
end;

procedure TLevelStatics.SetMechanicCount(Value: Integer);
begin
  if fMechanicCount = Value then Exit;
  CheckSkillCount(Value);
  fMechanicCount := Value;
  Changed;
end;

procedure TLevelStatics.SetStonerCount(Value: Integer);
begin
  if fStonerCount = Value then Exit;
  CheckSkillCount(Value);
  fStonerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetPlatformerCount(Value: Integer);
begin
  if fPlatformerCount = Value then Exit;
  CheckSkillCount(Value);
  fPlatformerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetStackerCount(Value: Integer);
begin
  if fStackerCount = Value then Exit;
  CheckSkillCount(Value);
  fStackerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetClonerCount(Value: Integer);
begin
  if fClonerCount = Value then Exit;
  CheckSkillCount(Value);
  fClonerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetFencerCount(Value: Integer);
begin
  if fFencerCount = Value then Exit;
  CheckSkillCount(Value);
  fFencerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetClimberCount(Value: Integer);
begin
  if fClimberCount = Value then Exit;
  CheckSkillCount(Value);
  fClimberCount := Value;
  Changed;
end;

procedure TLevelStatics.SetFloaterCount(Value: Integer);
begin
  if fFloaterCount = Value then Exit;
  CheckSkillCount(Value);
  fFloaterCount := Value;
  Changed;
end;

procedure TLevelStatics.SetBomberCount(Value: Integer);
begin
  if fBomberCount = Value then Exit;
  CheckSkillCount(Value);
  fBomberCount := Value;
  Changed;
end;

procedure TLevelStatics.SetBlockerCount(Value: Integer);
begin
  if fBlockerCount = Value then Exit;
  CheckSkillCount(Value);
  fBlockerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetBuilderCount(Value: Integer);
begin
  if fBuilderCount = Value then Exit;
  CheckSkillCount(Value);
  fBuilderCount := Value;
  Changed;
end;

procedure TLevelStatics.SetBasherCount(Value: Integer);
begin
  if fBasherCount = Value then Exit;
  CheckSkillCount(Value);
  fBasherCount := Value;
  Changed;
end;

procedure TLevelStatics.SetMinerCount(Value: Integer);
begin
  if fMinerCount = Value then Exit;
  CheckSkillCount(Value);
  fMinerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetDiggerCount(Value: Integer);
begin
  if fDiggerCount = Value then Exit;
  CheckSkillCount(Value);
  fDiggerCount := Value;
  Changed;
end;

procedure TLevelStatics.SetMusic(Value: Integer);
begin
  if fMusicTrack = Value then Exit;
  CheckMusic(Value);
  fMusicTrack := Value;
  Changed;
end;

procedure TLevelStatics.SetGraphicSet(Value: Integer);
begin
  if fGraphicSet = Value then Exit;
  //CheckGraphicSet(Value);
  fGraphicSet := Value;
  Changed;
end;

procedure TLevelStatics.SetGraphicSetEx(Value: Integer);
begin
  if fGraphicSetEx = Value then Exit;
  fGraphicSetEx := Value;
  ChangedInf(1);
end;

procedure TLevelStatics.SetScreenPosition(Value: Integer);
begin
  if fScreenPosition = Value then Exit;
  fScreenPosition := Value;
  if GraphicSetEx <> 0 then
    ChangedInf(2) // this is because
  else
    Changed; // mmm can be smarter but it works for now
end;

procedure TLevelStatics.SetScreenYPosition(Value: Integer);
begin
  if fScreenYPosition = Value then Exit;
  fScreenYPosition := Value;
  if GraphicSetEx <> 0 then
    ChangedInf(2) // this is because
  else
    Changed; // mmm can be smarter but it works for now
end;

{ TBaseLevelItem }

procedure TBaseLevelItem.CheckActive;
begin
end;

constructor TBaseLevelItem.Create(aCollection: TCollection);
begin
  fItemType := DoGetItemType;
  fItemLocked := false; // just in case
  Assert(fItemType >= litObject, 'ItemType error 1');
  if aCollection <> nil then
    if aCollection.Owner is TLevel then
      fOwnerLevel := TLevel(aCollection.Owner);
  Assert(fOwnerLevel <> nil, 'baselevelitem create error 2');
  inherited Create(aCollection);
//  Assert(fItemType >= litObject, 'ItemType error 2');
end;

function TBaseLevelItem.DoGetItemType: TLevelItemType;
begin
  Result := litBase;
end;

function TBaseLevelItem.GetAsString: string;
begin
  Result := '';
end;

function TBaseLevelItem.GetBounds: TRect;
var
  X, Y: Integer;
begin
  X := XPosition;
  Y := YPosition;
  Result := Rect(X, Y, X + ItemWidth, Y + ItemHeight);
end;

procedure TBaseLevelItem.SetSValue(Value: Integer);
begin
end;

procedure TBaseLevelItem.SetLValue(Value: Integer);
begin
end;

function TBaseLevelItem.GetIdentifier: Integer;
begin
  Result := 0;
end;

function TBaseLevelItem.GetItemHeight: Integer;
begin
  Result := 0;
end;

function TBaseLevelItem.GetItemWidth: Integer;
begin
  Result := 0;
end;

{function TBaseLevelItem.GetOwnerLevel: TLevel;
var
  O: TOwnedCollection;
begin
  Result := nil;
  if (Collection is TOwnedCollection) then
  begin
    O := TOwnedCollection(Collection);
  end
  else
    Exit;

  if O.Owner is TLevel then
    Result := TLevel(O.Owner);
end;}

function TBaseLevelItem.GetXPosition: Integer;
begin
  Result := 0;
end;

function TBaseLevelItem.GetYPosition: Integer;
begin
  Result := 0;
end;

function TBaseLevelItem.IsActive: Boolean;
begin
  Result := True;
  if fOwnerLevel = nil then
    raise Exception.Create('tbaselevelitem level active error');
  if fOwnerLevel.Style = nil then
    raise Exception.Create('tbaselevelitem level style error');
  if Graph = nil then
    raise Exception.Create('tbaselevelitem level graph error')
end;

procedure TBaseLevelItem.Move(DeltaX, DeltaY: Integer);
begin
  //deb(['a', xposition,yposition]);
  XPosition := XPosition + DeltaX;
  YPosition := YPosition + DeltaY;
  //deb(['b', xposition,yposition]);
end;

procedure TBaseLevelItem.PropertyChanged;
begin
  if Collection is TOwnedCollectionEx then
  begin
    TOwnedCollectionEx(Collection).ItemChanged(Self);
  end;
  Changed(False);
end;

procedure TBaseLevelItem.SetAsString(const Value: string);
begin
  //
end;

procedure TBaseLevelItem.SetIdentifier(Value: Integer);
begin

end;

procedure TBaseLevelItem.SetIndex(Value: Integer);
begin
  if Collection = nil then
    Exit;
  if Collection.Count = 0 then
    Exit;
  Restrict(Value, 0, Collection.Count - 1);
  inherited SetIndex(Value);
end;

procedure TBaseLevelItem.SetItemHeight(Value: Integer);
begin

end;

procedure TBaseLevelItem.SetItemWidth(Value: Integer);
begin

end;

procedure TBaseLevelItem.SetXPosition(Value: Integer);
begin
end;

procedure TBaseLevelItem.SetYPosition(Value: Integer);
begin
end;

{ TLevelCollection }

constructor TLevelCollection.Create(aOwner: TPersistent; aItemClass: TCollectionItemClass);
begin
  Assert(aOwner is TLevel, 'levelcollection create owner error');
  inherited Create(aOwner, aItemClass);
  fLevel := TLevel(aOwner);
end;

{ TBaseLevelItemCollection }

constructor TBaseLevelItemCollection.Create(aOwner: TPersistent; aItemClass: TCollectionItemClass);
begin
  inherited Create(aOwner, aItemClass);
  fTargetItemCount := -1;
end;

procedure TBaseLevelItemCollection.Notify(Item: TCollectionItem; Action: TCollectionNotification);
begin
{  case Action of
    cnAdded
    cnDeleting
  end;} //utools
//
end;

{ TInteractiveObject }

{function TInteractiveObject.GetLevel: TLevel;
begin
  Result := nil;
  if Collection = nil then Exit;
  if Collection.Owner = nil then Exit;
  if Collection.Owner is TLevel then
  begin
    Result := TLevel(Collection.Owner)
  end;
end;}

function TInteractiveObject.GetXPosition: Integer;
begin
  Result := fObjectX;
end;

function TInteractiveObject.GetYPosition: Integer;
begin
  Result := fObjectY;
end;

procedure TInteractiveObject.SetXPosition(Value: Integer);
begin
  SetObjectX(Value);
end;

procedure TInteractiveObject.SetYPosition(Value: Integer);
begin
  SetObjectY(Value);
end;

procedure TInteractiveObject.SetSValue(Value: Integer);
begin
  SetObjectSValue(Value);
end;

procedure TInteractiveObject.SetLValue(Value: Integer);
begin
  SetObjectLValue(Value);
end;

procedure TInteractiveObject.SetObjectDrawingFlags(Value: TObjectDrawingFlags);
begin
  if fObjectDrawingFlags = Value then Exit;
  fObjectDrawingFlags := Value;
  Changed(False);
end;

procedure TInteractiveObject.SetObjectID(Value: Integer);
begin
  if fObjectID = Value then Exit;
//  fCachedHeight := 0;
//  fCachedWidth := 0;
  fObjectID := Value;
  Changed(False);
end;

procedure TInteractiveObject.SetObjectX(Value: Integer);
begin
  if fObjectX = Value then
    Exit;
  if ItemLocked then Exit;
  if fOwnerLevel <> nil then
    fOwnerLevel.CheckObjectX(Self, Value);
  fObjectX := Value;
  Changed(False);  //gr32
end;

procedure TInteractiveObject.SetObjectY(Value: Integer);
begin
  if fObjectY = Value then Exit;
  if ItemLocked then Exit;
  fObjectY := Value;
  Changed(False);
end;

procedure TInteractiveObject.SetObjectSValue(Value: Integer);
begin
  if fObjectSValue = Value then Exit;
  if ItemLocked then Exit;
  if Value < 0 then Value := 0;
  if Value > 15 then Value := 15;
  fObjectSValue := Value;
  Changed(False);
end;

procedure TInteractiveObject.SetObjectLValue(Value: Integer);
begin
  if fObjectLValue = Value then Exit;
  if ItemLocked then Exit;
  if Value < 0 then Value := 0;
  if Value > 255 then Value := 255;
  fObjectLValue := Value;
  Changed(False);
end;

function TInteractiveObject.GetItemWidth: Integer;
begin
  CheckActive;
  if odfRotate in fObjectDrawingFlags then
    Result := Graph.MetaObjectCollection[ObjectId].Height
  else
    Result := Graph.MetaObjectCollection[ObjectId].Width;
end;

function TInteractiveObject.GetItemHeight: Integer;
begin
  CheckActive;
  if odfRotate in fObjectDrawingFlags then
    Result := Graph.MetaObjectCollection[ObjectId].Width
  else
    Result := Graph.MetaObjectCollection[ObjectId].Height;
end;

function TInteractiveObject.ValidObjectID(Value: Integer): Boolean;
var
  G: Integer;
begin
  Result := False;
  CheckActive;
  Result := Between(Value, 0, Graph.MetaObjectCollection.HackedList.Count - 1);

  {
  if Collection is TInteractiveObjectCollection then
    if Collection.Owner is TLevel then
    begin
      G := TLevel(Collection.Owner).Statics.GraphicSet;
      Result := Between(Value, 0, PictureMgr.ObjectBitmapCount[G] - 1);
    end;
  }
end;

function TInteractiveObject.DoGetItemType: TLevelItemType;
begin
  Result := litObject;
end;

function TInteractiveObject.GetAsString: string;
begin
  Result := SObjectClipBoardID + '=' +
            fGraph.GraphicSetInternalName + ',' +
            i2s(fObjectID) + ',' +
            i2s(fObjectX) + ',' +
            i2s(fObjectY) + ',' +
            i2s(Byte(fObjectDrawingFlags)) + ',' +
            i2s(fObjectSValue) + ',' +
            i2s(fObjectLValue);
end;

procedure TInteractiveObject.SetAsString(const Value: string);
var
  L: TStringList;
  S: string;
  H, X, Y, F, Sv, Lv: Integer;
  ODF: TObjectDrawingFlags;
begin
  if Value = '' then
    Exit;
  L := TStringList.Create;
  L.Add(Value);
  try
    S := L.Values[SObjectClipBoardID];
    if S = '' then
      Exit;
    H := StrToIntDef(SplitString(S, 1, ','), MinInt);
    X := StrToIntDef(SplitString(S, 2, ','), MinInt);
    Y := StrToIntDef(SplitString(S, 3, ','), MinInt);
    F := StrToIntDef(SplitString(S, 4, ','), MinInt);
    Sv := StrToIntDef(SplitString(S, 5, ','), MinInt);
    Lv := StrToIntDef(SplitString(S, 6, ','), MinInt);
    fGraph := StyleMgr.Styles[0].FindGraphicSetName(SplitString(S, 0, ','));
    if ValidObjectID(H) then
      if X <> MinInt then
      if Y <> MinInt then
        if F <> MinInt then
      begin
        ObjectID := H;
        ObjectX := X;
        ObjectY := Y;
        ObjectSValue := Sv;
        ObjectLValue := Lv;
        ODF := [];
        if F and Bit0 <> 0 then
          Include(ODF, odfOnlyShowOnTerrain);
        if F and Bit1 <> 0 then
          Include(ODF, odfInvert);
        if F and Bit2 <> 0 then
          Include(ODF, odfNoOverwrite);
        if F and Bit3 <> 0 then
          Include(ODF, odfFaceLeft);
        if F and Bit4 <> 0 then
          Include(ODF, odfFake);
        if F and Bit5 <> 0 then
          Include(ODF, odfInvisible);
        if F and Bit6 <> 0 then
          Include(ODF, odfFlip);
        if F and Bit7 <> 0 then
          Include(ODF, odfRotate);
        ObjectDrawingFlags := ODF;
      end;
  finally
    L.Free;
  end;
end;

function TInteractiveObject.GetIdentifier: Integer;
begin
  Result := fObjectID;
end;

procedure TInteractiveObject.SetIdentifier(Value: Integer);
begin
  if ItemLocked then Exit;
  SetObjectID(Value);
end;

function TInteractiveObject.GetBitmap: TBitmap32;
begin
  CheckActive;
  Result := Graph.ObjectBitmaps[ObjectId];
end;

procedure TInteractiveObject.GetFrameBitmap(aFrame: Integer; aBitmap: TBitmap32);
begin
  CheckActive;
  Graph.GetObjectFrame(ObjectId, aFrame, aBitmap);
end;

procedure TBaseLevelItemCollection.AdjustItemCount(aValue: Integer);
var
  i: Integer;
begin
  fTargetItemCount := aValue;
  if aValue = -1 then Exit;
  if Count > fTargetItemCount then
  begin
    for i := Count-1 downto 0 do
    begin
      if Count <= fTargetItemCount then break;
      if Items[i].ItemActive = false then Delete(i);
    end;
    while Count > fTargetItemCount do
      Delete(Count-1);
  end;
  if Count < fTargetItemCount then
    ForceAdd;
end;

function TBaseLevelItemCollection.Add: TBaseLevelItem;
var
  i: Integer;
begin
  Result := nil;
  if Count >= fTargetItemCount then
  begin
    for i := 0 to (Count - 1) do
    begin
      if Items[i].ItemActive = false then
      begin
        Result := Items[i];
        Exit;
      end;
    end;
    if fTargetItemCount = -1 then Result := TBaseLevelItem(inherited Add);
  end else begin
    Result := TBaseLevelItem(inherited Add);
    Exit;
  end;
end;

function TBaseLevelItemCollection.ForceAdd: TBaseLevelItem;
begin
  while Count < fTargetItemCount do
    Result := TBaseLevelItem(inherited Add);
end;

procedure TBaseLevelItemCollection.Clear;
begin
  inherited Clear;
  ForceAdd;
end;

procedure TBaseLevelItemCollection.FullClear;
begin
  inherited Clear;
  if fTargetItemCount <> -1 then ForceAdd;
end;

function TBaseLevelItemCollection.GetItem(Index: Integer): TBaseLevelItem;
begin
  Result := TBaseLevelItem(inherited GetItem(Index))
end;

procedure TBaseLevelItemCollection.SetItem(Index: Integer; const Value: TBaseLevelItem);
begin
  inherited SetItem(Index, Value);
end;


{ TInteractiveObjectCollection }

function TInteractiveObjectCollection.Add: TInteractiveObject;
begin
  Result := TInteractiveObject(inherited Add);
end;

constructor TInteractiveObjectCollection.Create(aOwner: TPersistent); 
begin
  inherited Create(aOwner, TInteractiveObject);
  //fTargetItemCount := LVL_MAXOBJECTCOUNT;
end;

function TInteractiveObjectCollection.GetItem(Index: Integer): TInteractiveObject;
begin
  Result := TInteractiveObject(inherited GetItem(Index))
end;

function TInteractiveObjectCollection.Insert(Index: Integer): TInteractiveObject;
begin
  Result := TInteractiveObject(inherited Insert(Index))
end;

procedure TInteractiveObjectCollection.SetItem(Index: Integer; const Value: TInteractiveObject);
begin
  inherited SetItem(Index, Value);
end;

procedure TInteractiveObjectCollection.Update(Item: TCollectionItem);
begin
//  inherited Update(Item);
  Assert(Owner is TLevel, 'TInteractiveObjectCollection.Update Owner error');
  Assert((Item = nil) or (Item is TInteractiveObject),  'TInteractiveObjectCollection.Update Item class error');
  with TLevel(Owner) do
    MsgListChanged(Self, Item);
end;

{ TTerrain }

{function TTerrain.GetLevel: TLevel;
begin
  Result := nil;
  if Collection = nil then Exit;
  if Collection.Owner = nil then Exit;
  if Collection.Owner is TLevel then
  begin
    Result := TLevel(Collection.Owner)
  end;
end;}

function TTerrain.GetXPosition: Integer;
begin
  Result := fTerrainX;
end;

function TTerrain.GetYPosition: Integer;
begin
  Result := fTerrainY;
end;

procedure TTerrain.SetXPosition(Value: Integer);
begin
  SetTerrainX(Value);
end;

procedure TTerrain.SetYPosition(Value: Integer);
begin
  SetTerrainY(Value);
end;


procedure TTerrain.SetTerrainDrawingFlags(const Value: TTerrainDrawingFlags);
begin
  if fTerrainDrawingFlags = Value then Exit;
  if ItemLocked then Exit;
  fTerrainDrawingFlags := Value;
  Changed(False);
end;

procedure TTerrain.SetTerrainX(Value: Integer);
begin
  if fTerrainX = Value then Exit;
  if ItemLocked then Exit;
  fTerrainX := Value;
  Changed(False); //PropertyChanged;
end;

procedure TTerrain.SetTerrainY(Value: Integer);
begin
  if fTerrainY = Value then Exit;
  if ItemLocked then Exit;
  fTerrainY := Value;
  Changed(False);
end;

procedure TTerrain.SetTerrainID(const Value: Integer);
begin
  if fTerrainID = Value then Exit;
  if ItemLocked then Exit;
  fTerrainID := Value;
  Changed(False);
end;

function TTerrain.GetItemHeight: Integer;
begin
  CheckActive;
  if tdfRotate in TerrainDrawingFlags then
    Result := Graph.MetaTerrainCollection[TerrainId].Width
  else
    Result := Graph.MetaTerrainCollection[TerrainId].Height;
end;

function TTerrain.GetItemWidth: Integer;
begin
  CheckActive;
  if tdfRotate in TerrainDrawingFlags then
    Result := Graph.MetaTerrainCollection[TerrainId].Height
  else
    Result := Graph.MetaTerrainCollection[TerrainId].Width;    //classes
end;


function TTerrain.ValidTerrainID(Value: Integer): Boolean;
var
  G: Integer;
begin
  Result := False;
  CheckActive;
  Result := Between(Value, 0, Graph.MetaTerrainCollection.Count - 1);
end;

function TTerrain.DoGetItemType: TLevelItemType;
begin
  Result := litTerrain;
end;

function TTerrain.GetAsString: string;
begin
  Result := STerrainClipBoardID + '=' +
            fGraph.GraphicSetInternalName + ',' +
            i2s(fTerrainID) + ',' +
            i2s(fTerrainX) + ',' +
            i2s(fTerrainY) + ',' +
            i2s(Integer(Byte(fTerrainDrawingFlags)));
end;

procedure TTerrain.SetAsString(const Value: string);
var
  L: TStringList;
  S: string;
  H, X, Y, F: Integer;
  TDF: TTerrainDrawingFlags;
begin
  if Value = '' then
    Exit;
  L := TStringList.Create;
  L.Add(Value);
  try
    S := L.Values[STerrainClipBoardID];
    if S = '' then
      Exit;
    H := StrToIntDef(SplitString(S, 1, ','), MinInt);
    X := StrToIntDef(SplitString(S, 2, ','), MinInt);
    Y := StrToIntDef(SplitString(S, 3, ','), MinInt);
    F := StrToIntDef(SplitString(S, 4, ','), MinInt);
    fGraph := StyleMgr.Styles[0].FindGraphicSetName(SplitString(S, 0, ','));
    if ValidTerrainID(H) then
    if X <> MinInt then
      if Y <> MinInt then
        if F <> MinInt then
      begin
        TerrainID := H;
        TerrainX := X;
        TerrainY := Y;
        TDF := [];
        if F and Bit0 <> 0 then //?????????????????
          Include(TDF, tdfErase);
        if F and Bit1 <> 0 then
          Include(TDF, tdfInvert);
        if F and Bit2 <> 0 then
          Include(TDF, tdfNoOverwrite);
        if F and Bit3 <> 0 then
          Include(TDF, tdfInvisible);
        if F and Bit4 <> 0 then
          Include(TDF, tdfFake);
        if F and Bit5 <> 0 then
          Include(TDF, tdfFlip);
        if F and Bit6 <> 0 then
          Include(TDF, tdfNoOneWay);
        if F and Bit7 <> 0 then
          Include(TDF, tdfRotate);
        TerrainDrawingFlags := TDF;
      end;
  finally
    L.Free;
  end;
end;

function TTerrain.GetIdentifier: Integer;
begin
  Result := fTerrainID;
end;

procedure TTerrain.SetIdentifier(Value: Integer);
begin
  if ItemLocked then Exit;
  SetTerrainID(Value);
end;

constructor TTerrain.Create(aCollection: TCollection);
begin
  inherited Create(aCollection);
end;

function TTerrain.GetBitmap: TBitmap32;
begin
  CheckActive;
  Result := Graph.TerrainBitmaps[TerrainId]
end;

{ TTerrainCollection }

function TTerrainCollection.Add: TTerrain;
begin
  Result := TTerrain(inherited Add); 
end; 

constructor TTerrainCollection.Create(aOwner: TPersistent); 
begin 
  inherited Create(aOwner, TTerrain);
  //fTargetItemCount := LVL_MAXTERRAINCOUNT;
end; 

function TTerrainCollection.GetItem(Index: Integer): TTerrain; 
begin 
  Result := TTerrain(inherited GetItem(Index)) 
end; 

function TTerrainCollection.Insert(Index: Integer): TTerrain;
begin
  Result := TTerrain(inherited Insert(Index))
end;

procedure TTerrainCollection.SetItem(Index: Integer; const Value: TTerrain);
begin
  inherited SetItem(Index, Value);
end;

procedure TTerrainCollection.Update(Item: TCollectionItem);
begin
  Assert(Owner is TLevel, 'TTerrainCollection.Update Owner error');
  Assert((Item = nil) or (Item is TTerrain),  'TTerrainCollection.Update Item class error');
  with TLevel(Owner) do
    MsgListChanged(Self, Item);
end;

{ TSteel }

procedure TSteel.SetSteelType(Value: Integer);
begin
  if fSteelType = Value then Exit;
  if ItemLocked then Exit;
  fSteelType := Value;
  Changed(false);
end;

function TSteel.DoGetItemType: TLevelItemType;
begin
  Result := litSteel;
end;

function TSteel.GetAsString: string;
begin
  Result := SSteelClipBoardID + '=' +
            i2s(fSteelX) + ',' +
            i2s(fSteelY) + ',' +
            i2s(fSteelWidth) + ',' +
            i2s(fSteelHeight) + ',' +
            i2s(fSteelType);
end;

procedure TSteel.SetAsString(const Value: string);
var
  L: TStringList;
  S: string;
  X, Y, W, H, N: Integer;
begin
  if Value = '' then
    Exit;
  L := TStringList.Create;
  L.Add(Value);
  try
    S := L.Values[SSteelClipBoardID];
    if S = '' then
      Exit;
    X := StrToIntDef(SplitString(S, 0, ','), MinInt);
    Y := StrToIntDef(SplitString(S, 1, ','), MinInt);
    W := StrToIntDef(SplitString(S, 2, ','), MinInt);
    H := StrToIntDef(SplitString(S, 3, ','), MinInt);
    N := StrToIntDef(SplitString(S, 4, ','), MinInt);
    if X <> MinInt then
      if Y <> MinInt then
        if W <> MinInt then
        if H <> MinInt then
      begin
        SteelX := X;
        SteelY := Y;
        SteelWidth := W;
        SteelHeight := H;
      end;
    fSteelType := N;
  finally
    L.Free;
  end;
end;

function TSteel.GetItemHeight: Integer;
begin
  Result := SteelHeight;
end;

function TSteel.GetItemWidth: Integer;
begin
  Result := SteelWidth;
end;

function TSteel.GetXPosition: Integer;
begin
  Result := SteelX;
end;

function TSteel.GetYPosition: Integer;
begin
  Result := SteelY;
end;

procedure TSteel.SetSteelHeight(const Value: Integer);
begin
  if ItemLocked then Exit;
  if fSteelHeight = Value then Exit;
  fSteelHeight := Value;
  Changed(False);
end;

procedure TSteel.SetSteelWidth(const Value: Integer);
begin
  if ItemLocked then Exit;
  if fSteelWidth = Value then Exit;
  fSteelWidth := Value;
  Changed(False);
end;

procedure TSteel.SetSteelX(const Value: Integer);
begin
  if fSteelX = Value then Exit;
  if ItemLocked then Exit;
  fSteelX := Value;
  Changed(False);
end;

procedure TSteel.SetSteelY(const Value: Integer);
begin
  if fSteelY = Value then Exit;
  if ItemLocked then Exit;
  fSteelY := Value;
  Changed(False);
end;

procedure TSteel.SetXPosition(Value: Integer);
begin
  SteelX := Value;
end;

procedure TSteel.SetYPosition(Value: Integer);
begin
  SteelY := Value;
end;

procedure TSteel.SetItemHeight(Value: Integer);
begin
  SteelHeight := Value;
end;

procedure TSteel.SetItemWidth(Value: Integer);
begin
  SteelWidth := Value;
end;

{ TSteelCollection }

function TSteelCollection.Add: TSteel;
begin 
  Result := TSteel(inherited Add);
end; 

constructor TSteelCollection.Create(aOwner: TPersistent); 
begin 
  inherited Create(aOwner, TSteel);
  //fTargetItemCount := LVL_MAXSTEELCOUNT;
end; 

function TSteelCollection.GetItem(Index: Integer): TSteel; 
begin 
  Result := TSteel(inherited GetItem(Index)) 
end; 

function TSteelCollection.Insert(Index: Integer): TSteel; 
begin 
  Result := TSteel(inherited Insert(Index)) 
end; 

procedure TSteelCollection.SetItem(Index: Integer; const Value: TSteel); 
begin 
  inherited SetItem(Index, Value); 
end;

procedure TSteelCollection.Update(Item: TCollectionItem);
begin
  Assert(Owner is TLevel, 'TSteelCollection.Update Owner error');
  Assert((Item = nil) or (Item is TSteel),  'TSteelCollection.Update Item class error');
  with TLevel(Owner) do
    MsgListChanged(Self, Item);
end;

{ TLevelLink }

function TLevelLink.GetLevel: TLevel;
begin
  Result := nil;
  if Manager <> nil then
    Result := TLevel(Manager.Master)
end;

procedure TLevelLink.SetLevel(const Value: TLevel);
begin
  if Value = nil then
    Manager := nil
  else
    Manager := Value.Mgr;
end;

{ TLevel }

procedure TLevel.BeginUpdate;
{-------------------------------------------------------------------------------
  If Level is in updating-state, no change notifications will be send: all
  collections will be mute.
  If updatecount is zero it triggers a LM_LEVELCHANGING.
-------------------------------------------------------------------------------}
begin
  if fUpdateCount = 0 then
  begin
    fChangeFlags := 0;
    MsgLevelChanging;
  end;
  Inc(fUpdateCount);
  Statics.BeginUpdate;
  InteractiveObjectCollection.BeginUpdate;
  TerrainCollection.BeginUpdate;
  fChanged := true;
end;

procedure TLevel.EndUpdate;
{-------------------------------------------------------------------------------
  To be called in pairs with BeginUpdate.
  If updatecount reaces zero it triggers a LM_LEVELCHANGED.
-------------------------------------------------------------------------------}
begin
  Statics.EndUpdate;
  InteractiveObjectCollection.EndUpdate;
  TerrainCollection.EndUpdate;
  if fUpdateCount > 0 then
  begin
    Dec(fUpdateCount);
    if fUpdateCount = 0 then
    begin
      MsgLevelChanged;
    end;
  end;
end;

procedure TLevel.Changed;
begin
  MsgLevelChanged;
end;

procedure TLevel.ClearLevel(DoClearStyle, DoClearGraph: Boolean);
//var
//  i: integer;
begin
  Statics.Clear;
  InteractiveObjectCollection.Clear;
  TerrainCollection.Clear;
  SteelCollection.Clear;
  LevelName := '';
  LevelAuthor := '';
  LevelMainLevel := '';
  LevelMusicFile := '';
  if DoClearStyle then Style := nil;
  if DoClearGraph then Graph := nil;
end;

constructor TLevel.Create(aOwner: TComponent);
{-------------------------------------------------------------------------------
  Create internal objects
-------------------------------------------------------------------------------}
begin
  inherited Create(aOwner);
  fMgr := TMessageManager.Create(Self);
  fMgr.BeforeSendMessage := Mgr_BeforeSendMessage;
//  fResolution := GlobalResolution; //
  Statics := TLevelStatics.Create(Self);
  InteractiveObjectCollection := TInteractiveObjectCollection.Create(Self);
  TerrainCollection := TTerrainCollection.Create(Self);
  SteelCollection := TSteelCollection.Create(Self);
end;

destructor TLevel.Destroy;
begin
  Statics.Free;
  InteractiveObjectCollection.Free;
  TerrainCollection.Free;
  SteelCollection.Free;
  fMgr.Free;
  inherited Destroy;
end;

procedure TLevel.DoAfterLoad;
begin

end;

procedure TLevel.DoBeforeLoad;
begin
end;

procedure TLevel.LoadFromFile(const aFileName: string; aStyle: TBaseLemmingStyle; aFormat: TLemmingFileFormat);
var
  F: TFileStream;
begin
  F := nil; // for a safe finally
  BeginUpdate;
  IncludeStates(LCF_LOADFROMFILE);
  try
    fFilename := aFilename;
    F := TFileStream.Create(fFileName, fmOpenRead);
    LoadFromStream(F, aStyle, aFormat);
    if (aFileName <> '') and (aFormat <> lffLemmini) then
      fFileName := aFileName
    else
      fFileName := '';
    IncludeChanges(LCF_LOADFROMFILE);
  finally
    F.Free;
    ExcludeStates(LCF_LOADFROMFILE);
    EndUpdate;
    fChanged := false;
  end;
end;

procedure TLevel.LoadFromStream(aStream: TStream; aStyle: TBaseLemmingStyle;
  aFormat: TLemmingFileFormat; ResetFileName: Boolean = true);

//var
  //oldcursor:tcursor;
var
  i: byte;

  procedure ValidatePieces;
  var
    i: Integer;
    O: TInteractiveObject;
    T: TTerrain;
    MadeChanges: Boolean;
  begin
    MadeChanges := false;

    for i := 0 to InteractiveObjectCollection.Count-1 do
    begin
      O := InteractiveObjectCollection[i];
      if O.ObjectID >= O.Graph.MetaObjectCollection.Count then
      begin
        O.Graph := StyleMgr[0].FindGraphicSetName('placeholder');
        O.ObjectID := 0;
        MadeChanges := true;
      end;
    end;

    for i := 0 to TerrainCollection.Count-1 do
    begin
      T := TerrainCollection[i];
      if T.TerrainID >= T.Graph.MetaTerrainCollection.Count then
      begin
        T.Graph := StyleMgr[0].FindGraphicSetName('placeholder');
        T.TerrainID := 0;
        MadeChanges := true;
      end;
    end;

    if MadeChanges then
      ShowMessage('Some objects or terrains in this level file refer to pieces that appear to not exist in their graphic sets.' + #13 +
                  'These have been changed to placeholder pieces. The level will not be playable until these are deleted, or' + #13 +
                  'replaced with references to pieces that do exist.');
  end;

  procedure ApplyWindowOrder;
  var
    i, i2: Integer;
    OrigCount: Integer;
    SrcO, DstO: TInteractiveObject;

    aLevel: TLevel;
    WindowOrder: array of Integer;
  begin
    aLevel := self;
    SetLength(WindowOrder, Length(aLevel.Statics.fWindowOrder));
    for i := 0 to Length(WindowOrder)-1 do
      WindowOrder[i] := aLevel.Statics.fWindowOrder[i];

    OrigCount := aLevel.InteractiveObjectCollection.Count;

    for i := 0 to Length(WindowOrder)-1 do
    begin
      SrcO := aLevel.InteractiveObjectCollection[WindowOrder[i]];
      DstO := aLevel.InteractiveObjectCollection.Add;

      // And here's the fun part, because the editor doesn't have a working TInteractiveObject.Assign
      DstO.ObjectX := SrcO.ObjectX;
      DstO.ObjectY := SrcO.ObjectY;
      DstO.ObjectID := SrcO.ObjectID;
      DstO.ObjectSValue := SrcO.ObjectSValue;
      DstO.ObjectLValue := SrcO.ObjectLValue;
      DstO.ObjectDrawingFlags := SrcO.ObjectDrawingFlags;

      DstO.fIsActive := SrcO.fIsActive;
      DstO.fOwnerLevel := SrcO.fOwnerLevel;
      DstO.fItemType := SrcO.fItemType;
      DstO.fItemActive := SrcO.fItemActive;
      DstO.fItemLocked := SrcO.fItemLocked;

      DstO.fGraph := SrcO.fGraph;
      DstO.fGraphID := SrcO.fGraphID;
    end;

    for i := OrigCount-1 downto 0 do
      for i2 := 0 to Length(WindowOrder)-1 do
        if WindowOrder[i2] = i then
        begin
          aLevel.InteractiveObjectCollection.Delete(i);
          Break;
        end;
  end;

begin

  BeginUpdate;
  IncludeStates(LCF_LOADFROMSTREAM);
  try

    try

      if aFormat = lffAutoDetect then
      begin
        aFormat := DoAutoDetect(aStream);
      end;

      case aFormat of
        lffAutoDetect:
          begin
            raise Exception.Create('load autodetect error');
          end;
        lffLVL:
          begin
            fStyle := aStyle;//StyleMgr.StyleList[aStyle];
            Graph := nil;
            aStream.Seek(0, soFromBeginning);
            aStream.Read(i, 1);
            aStream.Seek(0, soFromBeginning);
            case i of
              0 : LoadFromLVLStream(aStream);
            1..3: LoadFromNeoLVLStream(aStream);
              else LoadFromNewNeoLVLStream(aStream);
              end;
            fCurrentFormat := aFormat;
            if (fStateFlags and LCF_LOADFROMFILE = 0) and ResetFileName then
              fFileName := '';
          end;
        lffLemmini:
          begin
            Style := aStyle;//StyleMgr.StyleList[aStyle];
            LoadFromLemminiStream(aStream);
            fCurrentFormat := aFormat;
            if (fStateFlags and LCF_LOADFROMFILE = 0) and ResetFileName then
              fFileName := '';
          end;
      end;

      if ((Style is TLemminiStyle) and (TLemminiStyle(Style).SuperLemmini))
      or ((Style is TCustLemmStyle) and (TCustLemmStyle(Style).NeoLemmix)) then
      begin
        if (Statics.Width = 0) then Statics.Width := Style.GetParams.LevelWidth;
        if (Statics.Height = 0) then Statics.Height := Style.GetParams.LevelHeight;
      end else begin
        Statics.Width := Style.GetParams.LevelWidth;
        Statics.Height := Style.GetParams.LevelHeight;
      end;

      ValidatePieces;

      ApplyWindowOrder;

      IncludeChanges(LCF_LOADFROMSTREAM);

    except
      ClearLevel(True, True);
      raise;
    end;


  finally
    ExcludeStates(LCF_LOADFROMSTREAM);
    EndUpdate;
  end;


end;

function TLevel.DoAutoDetect(aStream: TStream): TLemmingFileFormat;
var
  S: String[5];// '# LVL';
begin
  Result := lffAutoDetect;
  S := '12345';
  if aStream.Size = 2048 then
    Result := lffLVL
  else begin
    aStream.Seek(0, soFromBeginning);
    if aStream.Read(S[1], 5) = 5 then
      if CompareText(S, '# LVL') = 0 then
        Result := lffLemmini;

    aStream.Seek(0, soFromBeginning)
  end;
end;

procedure TLevel.UpgradeFormat(var Buf: TNeoLVLRec);
var
  i: Integer;
begin
  while Buf.FormatTag < 3 do
  begin
    case Buf.FormatTag of
      1: begin
           Buf.MusicNumber := Buf.ScreenPosition mod 256;
           Buf.ScreenPosition := Buf.ScreenYPosition;
           Buf.ScreenYPosition := 0;
           Buf.FormatTag := 2;
         end;
      2: begin
           Buf.StyleName   := '                ';
           Buf.VgaspecName := '                ';
           for i := 0 to 31 do
             Buf.WindowOrder[i] := 0;
           for i := 0 to 63 do
             Buf.Objects[i].AsInt64 := Buf.Objects[i * 2].AsInt64;
           for i := 64 to 127 do
             Buf.Objects[i].AsInt64 := 0;
           Buf.VgaspecX := 304;
           Buf.VgaspecY := 0;
           Buf.FormatTag := 3;
         end;
    end;
  end;
end;

procedure TLevel.LoadFromNewNeoLVLStream(aStream: TStream);
var
  Buf: TNeoLVLHeader;
  SubHead: TNeoLVLSubHeader;
  i, i2, H: Integer;
  b: Byte;
  w: Word;

  O: TNewNeoLVLObject;
  T: TNewNeoLVLTerrain;
  S: TNewNeoLVLSteel;
  Obj: TInteractiveObject;
  Terrain: TTerrain;
  Steel: TSteel;
  Str: String;

  TempGraph: TBaseGraphicSet;

  Par: TDefaultsRec;

  LRes: Integer;
  HasSubHeader: Boolean;

    function Map(N: Integer): Integer;
    begin
      Result := N;
    end;

begin
  BeginUpdate;
  try
    ClearLevel(False, True);
    aStream.Seek(0, soFromBeginning);
    aStream.Read(Buf, SizeOf(Buf));
    //if R <> NEO_LVL_SIZE then
    //  raise Exception.Create('TNeoLevel read error 1');

    //UpgradeFormat(Buf);

    Style.GetStyleParams(par);

    {-------------------------------------------------------------------------------
      Get the statics. This is easy
    -------------------------------------------------------------------------------}
    with Statics do
    begin
      ReleaseRate      := Buf.ReleaseRate;
      LockedRR := (Buf.LevelOptions2 and $01) <> 0;
      LemmingsCount    := Buf.LemmingsCount;
      RescueCount      := Buf.RescueCount;
      TimeLimit        := (Buf.TimeLimit div 60) + ((Buf.TimeLimit mod 60) shl 8);
      if TimeLimit mod 256 > 99 then
      begin
        TimeLimit := 15203; // 253 mins 23 secs... why? o_O
        HasTimeLimit := false;
      end;

      BackgroundIndex := Buf.BgIndex;

        SkillTypes := Buf.Skillset;
        if Buf.LevelOptions2 and $02 <> 0 then
          SkillTypes := SkillTypes or $10000;

        WalkerCount := Buf.WalkerCount;
        ClimberCount := Buf.ClimberCount;
        SwimmerCount := Buf.SwimmerCount;
        FloaterCount := Buf.FloaterCount;
        GliderCount := Buf.GliderCount;
        MechanicCount := Buf.MechanicCount;
        BomberCount := Buf.BomberCount;
        StonerCount := Buf.StonerCount;
        BlockerCount := Buf.BlockerCount;
        PlatformerCount := Buf.PlatformerCount;
        BuilderCount := Buf.BuilderCount;
        StackerCount := Buf.StackerCount;
        BasherCount := Buf.BasherCount;
        MinerCount := Buf.MinerCount;
        DiggerCount := Buf.DiggerCount;
        ClonerCount := Buf.ClonerCount;
        FencerCount := Buf.FencerCount;

        if WalkerCount > 99 then InfiniteSkills := InfiniteSkills or $8000;
        if ClimberCount > 99 then InfiniteSkills := InfiniteSkills or $4000;
        if SwimmerCount > 99 then InfiniteSkills := InfiniteSkills or $2000;
        if FloaterCount > 99 then InfiniteSkills := InfiniteSkills or $1000;
        if GliderCount > 99 then InfiniteSkills := InfiniteSkills or $800;
        if MechanicCount > 99 then InfiniteSkills := InfiniteSkills or $400;
        if BomberCount > 99 then InfiniteSkills := InfiniteSkills or $200;
        if StonerCount > 99 then InfiniteSkills := InfiniteSkills or $100;
        if BlockerCount > 99 then InfiniteSkills := InfiniteSkills or $80;
        if PlatformerCount > 99 then InfiniteSkills := InfiniteSkills or $40;
        if BuilderCount > 99 then InfiniteSkills := InfiniteSkills or $20;
        if StackerCount > 99 then InfiniteSkills := InfiniteSkills or $10;
        if BasherCount > 99 then InfiniteSkills := InfiniteSkills or $8;
        if MinerCount > 99 then InfiniteSkills := InfiniteSkills or $4;
        if DiggerCount > 99 then InfiniteSkills := InfiniteSkills or $2;
        if ClonerCount > 99 then InfiniteSkills := InfiniteSkills or $1;
        if FencerCount > 99 then InfiniteSkills := InfiniteSkills or $10000;

      LRes := Buf.Resolution;
      if LRes = 0 then LRes := 8;

      ScreenPosition   := Map((Buf.ScreenPosition * 8) div LRes) + 160;
      ScreenYPosition  := Map((Buf.ScreenYPosition * 8) div LRes) + 80;
      {GraphicSet       := Buf.GraphicSet;
      GraphicSetEx     := Buf.GraphicSetEx;}
      MusicTrack       := 0;
      AutoSteelOptions := (Buf.LevelOptions or $61) and $EF; // always disable Oddtable flag
      (*Bit 0 - Ignored, should generally be set to on
Bit 1 - Enable autosteel
Bit 2 - Ignore the level's steel
Bit 3 - Use simple autosteel formula
Bit 4 - Turns oddtabling on
Bit 5 - Ignored, should generally be set to on
Bit 6 - Ignored, should generally be set to on
Bit 7 - One-way inversion (see terrain section for info)*)
      SuperLemming     := 0000;

      Width            := (Buf.Width * 8) div LRes;
      Height           := (Buf.Height * 8) div LRes;

      VgaspecX         := (Buf.VgaspecX * 8) div LRes;
      VgaspecY         := (Buf.VgaspecY * 8) div LRes;

      LevelID          := Buf.LevelID;

      {for i := 0 to 31 do
        fWindowOrder[i] := Buf.WindowOrder[i]; // Window order is not in the header in new format}
      SetLength(fWindowOrder, 0);
    end;
    {-------------------------------------------------------------------------------
      Get the level description. not so very difficult :)
    -------------------------------------------------------------------------------}
//    LevelName := TrimRight(Buf.LevelName); lemdosstyles
//    Deb([levelname]);

      LevelName := TrimRight(Buf.LevelName);
      LevelAuthor := TrimRight(Buf.LevelAuthor);

      Statics.StyleName := Trim(Lowercase(Buf.StyleName));
      Statics.VgaspecName := Trim(Lowercase(Buf.VgaspecName));
      Statics.GraphicSet := 255;
      Statics.GraphicSetEx := 255;

      {if (Trim(Buf.StyleName) <> '') and (Statics.GraphicSet <> 255) then
        for i := 0 to Style.GraphicSetList.Count - 1 do
          if (Style.GraphicSetList[i].GraphicSetInternalName = Trim(Lowercase(Buf.StyleName)))
          and (Style.GraphicSetList[i].IsSpecial = false) then
          begin
            Statics.GraphicSet := Style.GraphicSetList[i].GraphicSetId;
            Break;
          end;}

      if Trim(Lowercase(Buf.VgaspecName)) = 'none' then
        Statics.GraphicSetEx := 0;
      {else if (Trim(Buf.VgaspecName) <> '') and (Statics.GraphicSetEx <> 255) then
        begin
          for i := 0 to Style.GraphicSetList.Count - 1 do
          if (Trim(Lowercase(Style.GraphicSetList[i].GraphicSetInternalName)) = Trim(Lowercase(Buf.VgaspecName)))
          and (Style.GraphicSetList[i].IsSpecial) then
          begin
            Statics.GraphicSetEx := Style.GraphicSetList[i].GraphicSetIdExt;
            Break;
          end;
        end;}

    {if Statics.GraphicSet <> 255 then
      Graph := Style.FindGraphicSet(Statics.GraphicSet)
    else}
      Graph := Style.FindGraphicSetName(Statics.StyleName);

//    Graph
    Graph.EnsureMetaData;

    //Okay, so that's the end of the parts that are the same as older NeoLemmix format

    TerrainCollection.TargetItemCount := -1;
    InteractiveObjectCollection.TargetItemCount := -1;
    SteelCollection.TargetItemCount := -1;

    HasSubHeader := false;
    b := 0;
    aStream.Read(b, 1);
    while (b <> 0) do
    begin
      case b of
        1: begin // object
             aStream.Read(O, SizeOf(O));
             if ((O.ObjectFlags and 128) = 0) then Continue;
             Obj := InteractiveObjectCollection.Add;
             with Obj do
             begin
               ItemActive := true;
               H        := O.XPos;  // 16 bits little endian
               ObjectX  := (Map(H) * 8) div LRes;
               H        := O.YPos; // 16 bits little endian
               ObjectY  := (Map(H) * 8) div LRes;
               fObjectID := O.ObjectID; // need to bypass the valid ID check here
               fGraphID := O.Graph;
               ObjectSValue := O.SValue;
               ObjectLValue := O.LValue;

               if O.ObjectFlags and $01 <> 0 then
                 Include(fObjectDrawingFlags, odfNoOverwrite);
               if O.ObjectFlags and $02 <> 0 then
                 Include(fObjectDrawingFlags, odfOnlyShowOnTerrain);
               if O.ObjectFlags and $04 <> 0 then
                 Include(fObjectDrawingFlags, odfInvert);
               if O.ObjectFlags and $08 <> 0 then
                 Include(fObjectDrawingFlags, odfFaceLeft);
               if O.ObjectFlags and $10 <> 0 then
                 Include(fObjectDrawingFlags, odfFake);
               if O.ObjectFlags and $20 <> 0 then
                 Include(fObjectDrawingFlags, odfInvisible);
               if O.ObjectFlags and $40 <> 0 then
                 Include(fObjectDrawingFlags, odfFlip);
               if O.ObjectFlags and $100 <> 0 then
                 Include(fObjectDrawingFlags, odfRotate);

               if O.Locked and 1 <> 0 then ItemLocked := true;
             end;
           end;
        2: begin // terrain
             aStream.Read(T, SizeOf(T));
             if ((T.TerrainFlags and 128) = 0) then Continue;
             Terrain := TerrainCollection.Add;
             with Terrain do
             begin
               ItemActive := true;
               H := T.XPos;
               TerrainX := (Map(H) * 8) div LRes;
               H := T.YPos;
               TerrainY := (Map(H) * 8) div LRes;
               if (T.TerrainFlags and $1) <> 0 then
                 Include(fTerrainDrawingFlags, tdfNoOverwrite);
               if (T.TerrainFlags and $2) <> 0 then
                 Include(fTerrainDrawingFlags, tdfErase);
               if (T.TerrainFlags and $4) <> 0 then
                 Include(fTerrainDrawingFlags, tdfInvert);
               if (T.TerrainFlags and $8) <> 0 then
                 Include(fTerrainDrawingFlags, tdfFlip);
               if (T.TerrainFlags and $10) <> 0 then
                 Include(fTerrainDrawingFlags, tdfNoOneWay);
               if (T.TerrainFlags and $20) <> 0 then
                 Include(fTerrainDrawingFlags, tdfRotate);
               fTerrainId := T.TerrainID; // need to bypass the valid ID check here
               fGraphID := T.Graph;


               if T.Locked and 1 <> 0 then ItemLocked := true;
             end;
           end;
        3: begin // steel
             aStream.Read(S, SizeOf(S));
             if ((S.SteelFlags and 128) = 0) then Continue;
             Steel := SteelCollection.Add;
             with Steel do
             begin
               ItemActive := true;
               H           := S.XPos;
               SteelX      := (Map(H) * 8) div LRes;
               H           := S.YPos;
               SteelY      := (Map(H) * 8) div LRes;
               H           := S.SteelWidth + 1;
               SteelWidth  := (Map(H) * 8) div LRes;
               H           := S.SteelHeight + 1;
               SteelHeight := (Map(H) * 8) div LRes;
               SteelType := S.SteelFlags and $7F;

               if S.Locked and 1 <> 0 then ItemLocked := true;
             end;
           end;
        4: begin
             w := $FFFF;
             aStream.Read(w, 2);
             while w <> $FFFF do
             begin
               SetLength(Statics.fWindowOrder, Length(Statics.fWindowOrder) + 1);
               Statics.fWindowOrder[Length(Statics.fWindowOrder) - 1] := w;
               w := $FFFF;
               aStream.Read(w, 2);
             end;
           end;
        5: begin
             aStream.Read(SubHead, SizeOf(SubHead));
             Statics.ScreenPosition := SubHead.ScreenStartX + 160;
             Statics.ScreenYPosition := SubHead.ScreenStartY + 80;
             LevelMusicFile := Trim(SubHead.MusicName);
             Statics.PostSecretRank := 0;
             Statics.PostSecretLevel := 0;
             Statics.BnsRank := 0;
             Statics.BnsLevel := 0;
             Statics.ClockStart := 0;
             Statics.ClockEnd := 0;
             Statics.ClockCount := 0;
             HasSubHeader := true;
           end;
        6: begin
             aStream.Read(w, 2);
             for i := 0 to w-1 do
             begin
               Str := '                ';
               aStream.Read(Str[1], 16);
               Str := Trim(Str);
               TempGraph := StyleMgr.Styles[0].FindGraphicSetName(Str);
               for i2 := 0 to InteractiveObjectCollection.Count-1 do
                 if InteractiveObjectCollection[i2].fGraphID = i then
                   InteractiveObjectCollection[i2].Graph := TempGraph;
               for i2 := 0 to TerrainCollection.Count-1 do
                 if TerrainCollection[i2].fGraphID = i then
                   TerrainCollection[i2].Graph := TempGraph;
             end;
           end;
        else raise Exception.Create('Unsupported data section in level file (' + IntToStr(b) + '). If this is a valid level,' + #13 + 'you may need to update your copy of NeoLemmix Editor.');
      end;
      b := 0;
      aStream.Read(b, 1);
    end;
    if not HasSubHeader then
    begin
      case Buf.MusicNumber of
        0, 253: LevelMusicFile := '';
        254: LevelMusicFile := 'frenzy';
        255: LevelMusicFile := 'gimmick';
        else LevelMusicFile := '?' + IntToStr(Buf.MusicNumber);
      end;

      Statics.PostSecretRank := 0;
      Statics.PostSecretLevel := 0;
      Statics.BnsRank := 0;
      Statics.BnsLevel := 0;
      Statics.ClockStart := 0;
      Statics.ClockEnd := 0;
      Statics.ClockCount := 0;
    end;

    for i := 0 to InteractiveObjectCollection.Count-1 do
      if InteractiveObjectCollection[i].Graph = nil then
        InteractiveObjectCollection[i].Graph := Graph;

    for i := 0 to TerrainCollection.Count-1 do
      if TerrainCollection[i].Graph = nil then
        TerrainCollection[i].Graph := Graph;
  finally
    EndUpdate;
  end;
end;

procedure TLevel.LoadFromNeoLVLStream(aStream: TStream);
{-------------------------------------------------------------------------------
  Translate a LVL file and fill the collections.
  For technical details see documentation. A few important notes:
  o LVL files are "little endian". So sometimes a swap or shift is needed.
  o Empty objects/terrain/steel can be in the file, so we just place a "continue" in the loop
-------------------------------------------------------------------------------}
var
  R: Integer;
  Buf: TNeoLVLRec;
  H, i: Integer; Int32, A, B, C: Integer;
  k1, k2: Integer;
  Z0, Z1, Z2: Byte;
  W: Word;
  O: TNeoLVLObject;
  T: TNeoLVLTerrain;
  S: TNeoLVLSteel;
  Obj: TInteractiveObject;
  Terrain: TTerrain;
  Steel: TSteel;
  WR: TWordRec;
  Reso: Single;
  Par: TDefaultsRec;
  {sktemp: array[0..15] of byte;
  skt2: array[0..7] of byte;}

    function Map(N: Integer): Integer;
    begin
      Result := Trunc(N * Par.Resolution);
    end;


begin
  BeginUpdate;
  try
    ClearLevel(False, True);
    R := aStream.Read(Buf, NEO_LVL_SIZE);
    if R <> NEO_LVL_SIZE then
      raise Exception.Create('TNeoLevel read error 1');

    UpgradeFormat(Buf);

    Style.GetStyleParams(par);

    {-------------------------------------------------------------------------------
      Get the statics. This is easy
    -------------------------------------------------------------------------------}
    with Statics do
    begin
      ReleaseRate      := Buf.ReleaseRate;
      LemmingsCount    := Buf.LemmingsCount;
      RescueCount      := Buf.RescueCount;
      TimeLimit        := (Buf.TimeLimit div 60) + ((Buf.TimeLimit mod 60) shl 8);
      if TimeLimit mod 256 > 99 then
      begin
        TimeLimit := 15203;
        HasTimeLimit := false;
      end;

        SkillTypes := Buf.Skillset;
        WalkerCount := Buf.WalkerCount;
        ClimberCount := Buf.ClimberCount;
        SwimmerCount := Buf.SwimmerCount;
        FloaterCount := Buf.FloaterCount;
        GliderCount := Buf.GliderCount;
        MechanicCount := Buf.MechanicCount;
        BomberCount := Buf.BomberCount;
        StonerCount := Buf.StonerCount;
        BlockerCount := Buf.BlockerCount;
        PlatformerCount := Buf.PlatformerCount;
        BuilderCount := Buf.BuilderCount;
        StackerCount := Buf.StackerCount;
        BasherCount := Buf.BasherCount;
        MinerCount := Buf.MinerCount;
        DiggerCount := Buf.DiggerCount;
        ClonerCount := Buf.ClonerCount;

        if WalkerCount > 99 then InfiniteSkills := InfiniteSkills or $8000;
        if ClimberCount > 99 then InfiniteSkills := InfiniteSkills or $4000;
        if SwimmerCount > 99 then InfiniteSkills := InfiniteSkills or $2000;
        if FloaterCount > 99 then InfiniteSkills := InfiniteSkills or $1000;
        if GliderCount > 99 then InfiniteSkills := InfiniteSkills or $800;
        if MechanicCount > 99 then InfiniteSkills := InfiniteSkills or $400;
        if BomberCount > 99 then InfiniteSkills := InfiniteSkills or $200;
        if StonerCount > 99 then InfiniteSkills := InfiniteSkills or $100;
        if BlockerCount > 99 then InfiniteSkills := InfiniteSkills or $80;
        if PlatformerCount > 99 then InfiniteSkills := InfiniteSkills or $40;
        if BuilderCount > 99 then InfiniteSkills := InfiniteSkills or $20;
        if StackerCount > 99 then InfiniteSkills := InfiniteSkills or $10;
        if BasherCount > 99 then InfiniteSkills := InfiniteSkills or $8;
        if MinerCount > 99 then InfiniteSkills := InfiniteSkills or $4;
        if DiggerCount > 99 then InfiniteSkills := InfiniteSkills or $2;
        if ClonerCount > 99 then InfiniteSkills := InfiniteSkills or $1;

      ScreenPosition   := Map(Buf.ScreenPosition) + 160;
      ScreenYPosition  := Map(Buf.ScreenYPosition) + 80;
      GraphicSet       := Buf.GraphicSet;
      GraphicSetEx     := Buf.GraphicSetEx;

      case Buf.MusicNumber of
        0, 253: LevelMusicFile := '';
        254: LevelMusicFile := 'frenzy';
        255: LevelMusicFile := 'gimmick';
        else LevelMusicFile := '?' + IntToStr(Buf.MusicNumber);
      end;
      
      AutoSteelOptions := Buf.LevelOptions;
      SuperLemming     := 0000;

      Width            := 1584 + Buf.WidthAdjust;
      Height           := 160 + Buf.HeightAdjust;

      VgaspecX         := Buf.VgaspecX;
      VgaspecY         := Buf.VgaspecY;

      SetLength(fWindowOrder, 0);
      for i := 0 to 31 do
      begin
        if Buf.WindowOrder[i] and $80 = 0 then Continue;
        SetLength(fWindowOrder, Length(fWindowOrder) + 1);
        fWindowOrder[Length(fWindowOrder)-1] := Buf.WindowOrder[i] and $7F;
      end;
    end;
    {-------------------------------------------------------------------------------
      Get the level description. not so very difficult :)
    -------------------------------------------------------------------------------}
//    LevelName := TrimRight(Buf.LevelName); lemdosstyles
//    Deb([levelname]);

      Statics.StyleName := Trim(Lowercase(Buf.StyleName));
      Statics.VgaspecName := Trim(Lowercase(Buf.VgaspecName));

      if (Trim(Buf.StyleName) <> '') and (Statics.GraphicSet <> 255) then
        for i := 0 to Style.GraphicSetList.Count - 1 do
          if (LowerCase(Style.GraphicSetList[i].GraphicSetInternalName) = Trim(Lowercase(Buf.StyleName)))
          and (Style.GraphicSetList[i].IsSpecial = false) then
          begin
            Statics.GraphicSet := Style.GraphicSetList[i].GraphicSetId;
            Break;
          end;

      if Trim(Lowercase(Buf.VgaspecName)) = 'none' then
        Statics.GraphicSetEx := 0
      else if (Trim(Buf.VgaspecName) <> '') and (Statics.GraphicSetEx <> 255) then
        begin
          for i := 0 to Style.GraphicSetList.Count - 1 do
          if (Trim(Lowercase(Style.GraphicSetList[i].GraphicSetInternalName)) = Trim(Lowercase(Buf.VgaspecName)))
          and (Style.GraphicSetList[i].IsSpecial) then
          begin
            Statics.GraphicSetEx := Style.GraphicSetList[i].GraphicSetIdExt;
            Break;
          end;
        end;

    if Statics.GraphicSet <> 255 then
      Graph := Style.FindGraphicSet(Statics.GraphicSet)
    else
      Graph := Style.FindGraphicSetName(Statics.StyleName);

//    Graph
    Graph.EnsureMetaData;

    {-------------------------------------------------------------------------------
      Get the objects.
    -------------------------------------------------------------------------------}
    InteractiveObjectCollection.TargetItemCount := -1;
    InteractiveObjectCollection.FullClear;
    for i := 0 to 127 do
    begin
      O := Buf.Objects[i];
      Obj := InteractiveObjectCollection.Add;
      with Obj do
      begin
        if ((O.ObjectFlags and 128) <> 0) and (O.XPos <> -32768) then
          ItemActive := true
          else
          ItemActive := false;
        H        := O.XPos;  // 16 bits little endian
        ObjectX  := Map(H);
        H        := O.YPos; // 16 bits little endian
        //if H > 32767 then H := -65536 + H;
        ObjectY  := Map(H);
        Graph := Self.Graph;
        ObjectID := O.ObjectID;
        ObjectSValue := O.SValue;
        ObjectLValue := O.LValue;

        //if i = 8 then
          //windlg(bit8str(O.Modifier));

        if O.ObjectFlags and $01 <> 0 then
          Include(fObjectDrawingFlags, odfNoOverwrite);
        if O.ObjectFlags and $02 <> 0 then
          Include(fObjectDrawingFlags, odfOnlyShowOnTerrain);
        if O.ObjectFlags and $04 <> 0 then
          Include(fObjectDrawingFlags, odfInvert);
        if O.ObjectFlags and $08 <> 0 then
          Include(fObjectDrawingFlags, odfFaceLeft);
        if O.ObjectFlags and $10 <> 0 then
          Include(fObjectDrawingFlags, odfFake);
        if O.ObjectFlags and $20 <> 0 then
          Include(fObjectDrawingFlags, odfInvisible);
        if O.ObjectFlags and $40 <> 0 then
          Include(fObjectDrawingFlags, odfFlip);
      end;
    end;

    {-------------------------------------------------------------------------------
      Get the terrain.
    -------------------------------------------------------------------------------}
    TerrainCollection.TargetItemCount := -1;
    TerrainCollection.FullClear;
    for i := 0 to 999 do
    begin
      T := Buf.Terrain[i];
      {if T.AsDWORD = $FFFFFFFF then
        Continue;}
      Terrain := TerrainCollection.Add;
      with Terrain do
      begin
        if ((T.TerrainFlags and 128) <> 0) and (T.XPos <> -32768) then
          ItemActive := true
          else
          ItemActive := false;
        H := T.XPos;
        TerrainX := Map(H);
        H := T.YPos;
        TerrainY := Map(H);
        if (T.TerrainFlags and $1) <> 0 then
          Include(fTerrainDrawingFlags, tdfNoOverwrite);
        if (T.TerrainFlags and $2) <> 0 then
          Include(fTerrainDrawingFlags, tdfErase);
        if (T.TerrainFlags and $4) <> 0 then
          Include(fTerrainDrawingFlags, tdfInvert);
        if (T.TerrainFlags and $8) <> 0 then
          Include(fTerrainDrawingFlags, tdfFlip);
        if (T.TerrainFlags and $10) <> 0 then
          Include(fTerrainDrawingFlags, tdfNoOneWay);
        Graph := self.Graph;
        TerrainId := T.TerrainID;
      end;
    end;

    {-------------------------------------------------------------------------------
      Get the steel.
    -------------------------------------------------------------------------------}
    SteelCollection.TargetItemCount := -1;
    SteelCollection.FullClear;
    for i := 0 to 127 do
    begin
      S := Buf.Steel[i];
      {if S.AsDWORD = 0 then
        Continue;}
      //Deb(['steel', i]);
      Steel := SteelCollection.Add;
      with Steel do
      begin
        if ((S.SteelFlags and 128) <> 0) and (S.XPos <> -32768) then
          ItemActive := true
          else
          ItemActive := false;
        H           := S.XPos;
        SteelX      := Map(H);
        H           := S.YPos;
        SteelY      := Map(H);
        H           := S.SteelWidth + 1;
        SteelWidth  := Map(H);
        H           := S.SteelHeight + 1;
        SteelHeight := Map(H);
        SteelType := S.SteelFlags and $7F;
      end;
    end;

    {-------------------------------------------------------------------------------
      Get the level description. not so very difficult :)
    -------------------------------------------------------------------------------}
    LevelName := TrimRight(Buf.LevelName);
    LevelAuthor := TrimRight(Buf.LevelAuthor);

    {-------------------------------------------------------------------------------
      Adjusting + caching
    -------------------------------------------------------------------------------}
//    if fResolution = HiRes then
  //    AdjustResolution(2);
    CacheSizes;
    IncludeChanges(LCF_LOADFROMSTREAM);
  finally
    EndUpdate;
    //MsgLevelChanged;
  end;
//  DoAfterLoad;
end;




procedure TLevel.LoadFromLVLStream(aStream: TStream);
{-------------------------------------------------------------------------------
  Translate a LVL file and fill the collections.
  For technical details see documentation. A few important notes:
  o LVL files are "little endian". So sometimes a swap or shift is needed.
  o Empty objects/terrain/steel can be in the file, so we just place a "continue" in the loop
-------------------------------------------------------------------------------}
var
  R: Integer;
  Buf: TLVLFileRec;
  H, i: Integer; Int32, A, B, C: Integer;
  k1, k2: Integer;
  Z0, Z1, Z2: Byte;
  W: Word;
  O: TLVLObject;
  T: TLVLTerrain;
  S: TLVLSteel;
  Obj: TInteractiveObject;
  Terrain: TTerrain;
  Steel: TSteel;
  WR: TWordRec;
  Reso: Single;
  Par: TDefaultsRec;
  sktemp: array[0..15] of byte;
  skt2: array[0..7] of byte;
  NeoLemmix: Boolean;

    function Map(N: Integer): Integer;
    begin
      Result := Trunc(N * Par.Resolution);
    end;


begin
  BeginUpdate;
  try
    ClearLevel(False, True);
    R := aStream.Read(Buf, 2048);
    if R <> 2048 then
      raise Exception.Create('TLevel read error 1');

    //Style := StyleMgr.StyleList[STYLE_WINLORES];

    //if

    Style.GetStyleParams(par);

    NeoLemmix := TBaseDosStyle(Style).NeoLemmix;
    if Style is TLemminiStyle then NeoLemmix := true;

    {-------------------------------------------------------------------------------
      Get the statics. This is easy
    -------------------------------------------------------------------------------}
    with Statics do
    begin
      ReleaseRate      := Buf.ReleaseRate;
      LemmingsCount    := Buf.LemmingsCount + (Buf._LemmingsCountNotUsed shl 8);
      RescueCount      := Buf.RescueCount + (Buf._RescueCountNotUsed shl 8);
      TimeLimit        := Buf.TimeLimit + (Buf._TimeLimitNotUsed shl 8);
      if not NeoLemmix then TimeLimit := TimeLimit mod 256;
      if NeoLemmix and (TimeLimit mod 256 > 99) then
      begin
        TimeLimit := 15203;
        HasTimeLimit := false;
      end;
      InfiniteSkills := 0;
      if (Buf._GraphicSetExNotUsed and $40 = 0) or (not NeoLemmix) then
      begin
        ClimberCount     := Buf.ClimberCount;
        if ClimberCount > 99 then InfiniteSkills := InfiniteSkills or $4000;
        FloaterCount     := Buf.FloaterCount;
        if FloaterCount > 99 then InfiniteSkills := InfiniteSkills or $1000;
        BomberCount      := Buf.BomberCount;
        if BomberCount > 99 then InfiniteSkills := InfiniteSkills or $0200;
        BlockerCount     := Buf.BlockerCount;
        if BlockerCount > 99 then InfiniteSkills := InfiniteSkills or $0080;
        BuilderCount     := Buf.BuilderCount;
        if BuilderCount > 99 then InfiniteSkills := InfiniteSkills or $0020;
        BasherCount      := Buf.BasherCount;
        if BasherCount > 99 then InfiniteSkills := InfiniteSkills or $0008;
        MinerCount       := Buf.MinerCount;
        if MinerCount > 99 then InfiniteSkills := InfiniteSkills or $0004;
        DiggerCount      := Buf.DiggerCount;
        if DiggerCount > 99 then InfiniteSkills := InfiniteSkills or $0002;
        WalkerCount := 0;
        SwimmerCount := 0;
        GliderCount := 0;
        MechanicCount := 0;
        StonerCount := 0;
        PlatformerCount := 0;
        StackerCount := 0;
        ClonerCount := 0;
        SkillTypes := $52AE;
      end else begin
        SkillTypes := (Buf._MinerNotUsed shl 8) + Buf._DiggerNotUsed;
        skt2[0] := Buf.ClimberCount;
        skt2[1] := Buf.FloaterCount;
        skt2[2] := Buf.BomberCount;
        skt2[3] := Buf.BlockerCount;
        skt2[4] := Buf.BuilderCount;
        skt2[5] := Buf.BasherCount;
        skt2[6] := Buf.MinerCount;
        skt2[7] := Buf.DiggerCount;
        k2 := 0;
        for k1 := 0 to 15 do
          if (SkillTypes and Trunc(Power(2, (15-k1))) <> 0) and (k2 < 8) then
          begin
          sktemp[k1] := skt2[k2];
          inc(k2);
          if sktemp[k1] > 99 then
            InfiniteSkills := InfiniteSkills or Trunc(Power(2, (15-k1)));
          end else
            sktemp[k1] := 0;
        WalkerCount := sktemp[0];
        ClimberCount := sktemp[1];
        SwimmerCount := sktemp[2];
        FloaterCount := sktemp[3];
        GliderCount := sktemp[4];
        MechanicCount := sktemp[5];
        BomberCount := sktemp[6];
        StonerCount := sktemp[7];
        BlockerCount := sktemp[8];
        PlatformerCount := sktemp[9];
        BuilderCount := sktemp[10];
        StackerCount := sktemp[11];
        BasherCount := sktemp[12];
        MinerCount := sktemp[13];
        DiggerCount := sktemp[14];
        ClonerCount := sktemp[15];
      end;
      ScreenPosition   := System.Swap(Buf.ScreenPosition) + 160;
      ScreenYPosition  := 80;
      GraphicSet       := Buf.GraphicSet;
      GraphicSetEx     := Buf.GraphicSetEx;
      MusicTrack       := Buf._GraphicSetNoUsed;
      AutoSteelOptions := Buf._GraphicSetExNotUsed and $EF;
      SuperLemming     := (Buf.Reserved1 shl 8) + Buf.Reserved2;
    end;
    {-------------------------------------------------------------------------------
      Get the level description. not so very difficult :)
    -------------------------------------------------------------------------------}
//    LevelName := TrimRight(Buf.LevelName); lemdosstyles
//    Deb([levelname]);

      Graph := Style.FindGraphicSet(Statics.GraphicSet);// GraphicSetList[Statics.GraphicSet]

//    Graph
    Graph.EnsureMetaData;

    {-------------------------------------------------------------------------------
      Get the objects.
    -------------------------------------------------------------------------------}
    if NeoLemmix then
      InteractiveObjectCollection.TargetItemCount := -1
      else
      InteractiveObjectCollection.TargetItemCount := 32;
    InteractiveObjectCollection.FullClear;
    for i := 0 to LVL_MAXOBJECTCOUNT - 1 do
    begin
      O := Buf.Objects[i];
      Obj := InteractiveObjectCollection.Add;
      with Obj do
      begin
        if O.AsInt64 <> 0 then
          ItemActive := true
          else
          ItemActive := false;
        H        := Integer(O.B0) shl 8 + Integer(O.B1) - 16;  // 16 bits little endian
        ObjectX  := Map(H);
        if not NeoLemmix then ObjectX := (ObjectX div 8) * 8;
        H        := Integer(O.B2) shl 8 + Integer(O.B3); // 16 bits little endian
        if H > 32767 then H := -65536 + H;
        ObjectY  := Map(H);
        Graph := self.Graph;
        ObjectID := Integer(O.B5 and 31);
        if NeoLemmix then ObjectSValue := O.Modifier and 15;
        if NeoLemmix then ObjectLValue := O.B4;

        //if i = 8 then
          //windlg(bit8str(O.Modifier));

        if O.Modifier and $80 <> 0 then
          Include(fObjectDrawingFlags, odfNoOverwrite);
        if O.Modifier and $40 <> 0 then
          Include(fObjectDrawingFlags, odfOnlyShowOnTerrain);
        if (O.Modifier and $20 <> 0) and NeoLemmix then
          Include(fObjectDrawingFlags, odfFaceLeft);
        if (O.Modifier and $10 <> 0) and NeoLemmix then
          Include(fObjectDrawingFlags, odfFake);
//        if fObjectDrawingFlags = [odfNoOverwrite, odfOnlyShowOnTerrain] then
  //        fObjectDrawingFlags := [odfOnlyShowOnTerrain];

        {
        case O.Modifier of
          $80: Include(fObjectDrawingFlags, odfNoOverwrite);
          $40: Include(fObjectDrawingFlags, odfOnlyShowOnTerrain);
        end;
        }

        if O.DisplayMode = $8F then
          Include(fObjectDrawingFlags, odfInvert);
      end;
    end;

    {-------------------------------------------------------------------------------
      Get the terrain.
    -------------------------------------------------------------------------------}
    if NeoLemmix then
      TerrainCollection.TargetItemCount := -1
      else
      TerrainCollection.TargetItemCount := 400;
    TerrainCollection.FullClear;
    for i := 0 to LVL_MAXTERRAINCOUNT - 1 do
    begin
      T := Buf.Terrain[i];
      {if T.AsDWORD = $FFFFFFFF then
        Continue;}
      Terrain := TerrainCollection.Add;
      with Terrain do
      begin
        if T.AsDWORD <> $FFFFFFFF then
          ItemActive := true
          else
          ItemActive := false;
        H := Integer(T.B0 and 15) shl 8 + Integer(T.B1) - 16; // 9 bits
        TerrainX := Map(H);
        TerrainDrawingFlags := TTerrainDrawingFlags(Byte(T.B0 shr 5)); // the bits are compatible  after the shl
        H := Integer(T.B2) shl 1 + Integer(T.B3 and $80) shr 7;
        //Dec(H, 4); ??
        //if H > 256 then Dec(H, 512); ??
        if H >= 256 then
          Dec(H, 512);
        Dec(H, 4);
        //Deb([filename, i, h]);
        TerrainY := Map(H);
        Graph := self.Graph;
        TerrainId := T.B3 and 63; // max = 63.  bit7 belongs to ypos
        if T.B0 and $10 <> 0 then TerrainId := TerrainId + 64;
      end;
    end;

    {-------------------------------------------------------------------------------
      Get the steel.
    -------------------------------------------------------------------------------}
    if NeoLemmix then
      SteelCollection.TargetItemCount := -1
      else
      SteelCollection.TargetItemCount := 32;
    SteelCollection.FullClear;
    for i := 0 to LVL_MAXSTEELCOUNT - 1 do
    begin
      S := Buf.Steel[i];
      {if S.AsDWORD = 0 then
        Continue;}
      //Deb(['steel', i]);
      Steel := SteelCollection.Add;
      with Steel do
      begin
        if S.AsDWORD <> 0 then
          ItemActive := true
          else
          ItemActive := false;
        H           := ((Integer(S.B0) shl 1) + (Integer(S.B1 and Bit7) shr 7)) * 4 - 16;  // 9 bits
        if NeoLemmix then H           := H - (S.B3 shr 6);
        SteelX      := Map(H);
        H           := Integer(S.B1 and not Bit7) * 4;  // bit 7 belongs to steelx
        if NeoLemmix then H           := H - ((S.B3 shr 4) mod 4);
        SteelY      := Map(H);
        H           := Integer(S.B2 shr 4) * 4 + 4;  // first nibble bits 4..7 is width in units of 4 pixels (and then add 4)
        if NeoLemmix then H           := H - ((S.B3 shr 2) mod 4);
        SteelWidth  := Map(H);
        H           := Integer(S.B2 and $F) * 4 + 4;  // second nibble bits 0..3 is height in units of 4 pixels (and then add 4)
        if NeoLemmix then H           := H - (S.B3 mod 4);
        SteelHeight := Map(H);
      end;
    end;

    {-------------------------------------------------------------------------------
      Get the level description. not so very difficult :)
    -------------------------------------------------------------------------------}
    LevelName := TrimRight(Buf.LevelName);

    {-------------------------------------------------------------------------------
      Adjusting + caching
    -------------------------------------------------------------------------------}
//    if fResolution = HiRes then
  //    AdjustResolution(2);
    CacheSizes;
    IncludeChanges(LCF_LOADFROMSTREAM);
  finally
    EndUpdate;
    //MsgLevelChanged;
  end;
//  DoAfterLoad;
end;




procedure TLevel.LoadFromLVLRec(const LVL: TLVLFileRec);
var
  M: TMemoryStream;
  i: byte;
begin
  M := TMemoryStream.Create;
  try
    M.WriteBuffer(LVL, 2048);
    M.Seek(0, soFromBeginning);
    LoadFromLVLStream(M);
    IncludeChanges(LCF_LOADFROMSTREAM);
  finally
    M.Free;
  end;
end;

procedure TLevel.LoadFromLVLRec(const LVL: TNeoLVLRec);
var
  M: TMemoryStream;
  i: byte;
begin
  M := TMemoryStream.Create;
  try
    M.WriteBuffer(LVL, 10240);
    M.Seek(0, soFromBeginning);
    LoadFromNeoLVLStream(M);
    IncludeChanges(LCF_LOADFROMSTREAM);
  finally
    M.Free;
  end;
end;



procedure TLevel.LoadFromLemminiStream(aStream: TStream);
var
  List: TStringList;
  N, V: string;
  i, H, q: Integer;
  T: TTerrain;
  O, Ot: TInteractiveObject;
  S: TSteel;
  TDF: TTerrainDrawingFlags;
  ODF: TObjectDrawingFlags;
  fSuperLemmini, fLoadToLemmix: Boolean;
  NOWWCount : Integer;

    // get integer from lemmini ini file
    function GetInt(const aName: string): Integer;
    begin
      Result := StrToIntDef(List.Values[aName], -1);
      if Result = -1 then
        if aName = 'numToRecue' then
           Result := StrToIntDef(List.Values['numToRescue'], -1);

      //Log([aName, Result]);
    end;

    function CheckSuperLemming: Integer;
    begin
      Result := 0;
      if (Trim(UpperCase(List.Values['superlemming'])) = 'TRUE') then Result := $1;
    end;

    // get style
    function GetStyleNo: Integer;
    var
      i: Integer;
      aStyle: string;
    begin
      aStyle := List.Values['style'];
      Result := Style.graphbyname(aStyle).GraphicSetId;
    end;

    procedure GetGraphicSet;
    var
      i: Integer;
      aStyle: string;
    begin
      aStyle := List.Values['style'];
      //if fStyle = nil then
        //fStyle := StyleMgr.StyleList[STYLE_LEMMINI];
      fGraph := fStyle.FindGraphicSetName(aStyle);
      fGraph.EnsureMetaData;
    end;


begin
  fSuperLemmini := true;
  fLoadToLemmix := true;

  NOWWCount := 0;
  List := TStringList.Create;
  BeginUpdate;
  try
    ClearLevel(False, True);
    List.NameValueSeparator := '=';
    List.LoadFromStream(aStream);
    {-------------------------------------------------------------------------------
      o First a little adjusting. TStringList is too dumb to recognize name/value pairs
        when spaces are involved.
      o Then delete all comments
    -------------------------------------------------------------------------------}
    for i := 0 to List.Count - 1 do
    begin
      V := List[i];
      repeat
        if FastPos(V, ' =', Length(V), 2, 1) = 0 then
          if FastPos(V, '= ', Length(V), 2, 1) = 0 then
            Break;
         V := FastReplace(V, ' =', '=');
         V := FastReplace(V, '= ', '=');
      until False;

      H := Pos('#', V);
      if H >= 1 then
      begin
//        Log(['voor', V]);
        V := Copy(V, 1, H - 1);
  //      Log(['na', V]);
      end;

      List[i] := V;//FastReplace(List[i], ' = ', '=');
    end;

    GetGraphicSet;

    with Statics do
    begin
      GraphicSet := fGraph.fGraphicSetId;
      ReleaseRate      := GetInt('releaseRate');
      LemmingsCount    := GetInt('numLemmings');
      RescueCount      := GetInt('numToRecue');

      if (GetInt('timeLimitSeconds') <> -1) {and fSuperLemmini} then
      begin
        TimeLimit        := GetInt('timeLimitSeconds');
        TimeLimit := ((TimeLimit mod 60) shl 8) + (TimeLimit div 60);
      end else if (GetInt('timeLimit') <> -1) then
        TimeLimit        := GetInt('timeLimit')
      else begin
        TimeLimit := 15203;
        HasTimeLimit := false;
      end;

      InfiniteSkills := 0;

      if (Trim(UpperCase(List.Values['numClimbers'])) = 'INFINITY') then
      begin
        InfiniteSkills := InfiniteSkills or $4000;
        ClimberCount := 0;
      end else
        ClimberCount     := GetInt('numClimbers');

      if (Trim(UpperCase(List.Values['numFloaters'])) = 'INFINITY') then
      begin
        InfiniteSkills := InfiniteSkills or $1000;
        FloaterCount := 0;
      end else
        FloaterCount     := GetInt('numFloaters');

      if (Trim(UpperCase(List.Values['numBombers'])) = 'INFINITY') then
      begin
        InfiniteSkills := InfiniteSkills or $0200;
        BomberCount := 0;
      end else
        BomberCount      := GetInt('numBombers');

      if (Trim(UpperCase(List.Values['numBlockers'])) = 'INFINITY') then
      begin
        InfiniteSkills := InfiniteSkills or $0080;
        BlockerCount := 0;
      end else
        BlockerCount     := GetInt('numBlockers');

      if (Trim(UpperCase(List.Values['numBuilders'])) = 'INFINITY') then
      begin
        InfiniteSkills := InfiniteSkills or $0020;
        BuilderCount := 0;
      end else
        BuilderCount     := GetInt('numBuilders');

      if (Trim(UpperCase(List.Values['numBashers'])) = 'INFINITY') then
      begin
        InfiniteSkills := InfiniteSkills or $0008;
        BasherCount := 0;
      end else
        BasherCount      := GetInt('numBashers');

      if (Trim(UpperCase(List.Values['numMiners'])) = 'INFINITY') then
      begin
        InfiniteSkills := InfiniteSkills or $0004;
        MinerCount := 0;
      end else
        MinerCount       := GetInt('numMiners');

      if (Trim(UpperCase(List.Values['numDiggers'])) = 'INFINITY') then
      begin
        InfiniteSkills := InfiniteSkills or $0002;
        DiggerCount := 0;
      end else
        DiggerCount      := GetInt('numDiggers');

      if (GetInt('xPosCenter') <> -1) and fSuperLemmini then
        ScreenPosition   := GetInt('xPosCenter') div 2
        else
        ScreenPosition   := (GetInt('xPos') div 2) + 160;

      if (GetInt('yPosCenter') <> -1) and fSuperLemmini then
        ScreenYPosition   := GetInt('yPosCenter') div 2
        else
        ScreenYPosition   := 80;

      if ScreenPosition = -1 then ScreenPosition := 0;

      GraphicSet       := Graph.GraphicSetId;

      if fSuperLemmini and (GetInt('width') <> -1) then
        Width := GetInt('width') div 2
      else
        Width := 1600;

      if fSuperLemmini and (GetInt('height') <> -1) then
        Height := GetInt('height') div 2
      else
        Height := 160;

      if fSuperLemmini then
      begin
        case s2i(Trim(List.Values['autosteelMode'])) of
          0 : AutoSteelOptions := 0;
          1 : AutoSteelOptions := 10;
          2 : AutoSteelOptions := 2;
        end;

        if (GetInt('specialStylePositionX') <> -1) then
          VgaspecX := GetInt('specialStylePositionX') div 2
          else
          VgaspecX := 302;

        if (GetInt('specialStylePositionY') <> -1) then
          VgaspecY := GetInt('specialStylePositionY') div 2
          else
          VgaspecY := 0;

      end

    end;

    {-------------------------------------------------------------------------------
      Get the objects

      # Objects
      # id, xpos, ypos, paint mode (), upside down (0,1)
      # paint modes: 8=VIS_ON_TERRAIN, 4=NO_OVERWRITE, 0=FULL (only one value possible)
      object_0 = 6, 1664, 64, 4, 0
      object_1 = 5, 1152, 288, 0, 0
    -------------------------------------------------------------------------------}
    i := 0;
    repeat
      V := List.Values['object_' + i2s(i)];
      if V = '' then
        Break;
      // paint modes: 8=VIS_ON_TERRAIN, 4=NO_OVERWRITE, 0=FULL (only one value possible)
      O := InteractiveObjectCollection.Add;
      O.Graph := Graph;
      O.ObjectID := StrToIntDef(SplitString(V, 0, ','), -1);
      O.ObjectX  := StrToIntDef(SplitString(V, 1, ','), -1);
      O.ObjectY  := StrToIntDef(SplitString(V, 2, ','), -1);
      if fLoadToLemmix then
      begin
        O.ObjectX := O.ObjectX div 2;
        O.ObjectY := O.ObjectY div 2;
      end;
      O.ItemActive := true;
      ODF := [];
      H := StrToIntDef(SplitString(V, 3, ','), -1);
      if H = -1 then
        raise Exception.Create('Lemming ini file object drawingflag error');
      if H and 8 <> 0 then
        Include(ODF, odfOnlyShowOnTerrain);
      if H and 4 <> 0 then
        Include(ODF, odfNoOverwrite);
      if (H and 2 <> 0) and fSuperLemmini then
        Include(ODF, odfInvisible);
      H := StrToIntDef(SplitString(V, 4, ','), -1);
      if H = -1 then
        raise Exception.Create('Lemming ini file upside down indicator error');
      if (H and 1) <> 0 then
        Include(ODF, odfInvert);
      if fSuperLemmini then
      begin
        if ((H and 2) <> 0) then
          Include(ODF, odfFake);
        if ((H and 8) <> 0) then
          Include(ODF, odfFlip);
      end;
      if fSuperLemmini then
      begin
        H := StrToIntDef(SplitString(V, 5, ','), -1);
        if H = 1 then
          Include(ODF, odfFaceLeft);
      end;
      O.ObjectDrawingFlags := ODF;
      if O.ObjectID < 0 then
      begin
        O.ItemActive := false;
        O.ObjectID := 0;
      end;
      Inc(i);
    until False;

    {-------------------------------------------------------------------------------
      Get the terrain

      # Terrain
      # id, xpos, ypos, modifier
      # modifier: 8=NO_OVERWRITE, 4=UPSIDE_DOWN, 2=REMOVE (combining allowed, 0=FULL)
      terrain_0 = 7, 1360, 130, 0
      terrain_1 = 7, 1420, 130, 0
    -------------------------------------------------------------------------------}
    i := 0;
    repeat
      V := List.Values['terrain_' + i2s(i)];
      if V = '' then
        Break;
      T := TerrainCollection.Add;
      T.Graph := Graph;
      T.TerrainId := StrToIntDef(SplitString(V, 0, ','), -1);
      T.TerrainX  := StrToIntDef(SplitString(V, 1, ','), -1);
      T.TerrainY := StrToIntDef(SplitString(V, 2, ','), -1);
      if fLoadToLemmix then
      begin
        T.TerrainX := T.TerrainX div 2;
        T.TerrainY := T.TerrainY div 2;
      end;
      T.ItemActive := true;
      H := StrToIntDef(SplitString(V, 3, ','), -1);
      if H = -1 then
        raise Exception.Create('Lemming ini file terrain drawingflag error (' + i2s(i) + ')');
      // 8=NO_OVERWRITE, 4=UPSIDE_DOWN, 2=REMOVE
      TDF := [];
      if (H and 64 <> 0) and fSuperLemmini then
      begin
        Include(TDF, tdfNoOneWay);
        Inc(NOWWCount);
      end;
      if (H and 32 <> 0) and fSuperLemmini then
        Include(TDF, tdfFlip);
      if H and 8 <> 0 then
        Include(TDF, tdfNoOverwrite);
      if H and 4 <> 0 then
        Include(TDF, tdfInvert);
      if H and 2 <> 0 then
        Include(TDF, tdfErase);
      T.TerrainDrawingFlags := TDF;
      //Log(['terrain', i]);
      if T.TerrainID < 0 then
      begin
        T.ItemActive := false;
        T.TerrainID := 0;
      end;
      Inc(i);
    until False;

    if NOWWCount > (TerrainCollection.Count div 2) then
    begin
      Statics.AutoSteelOptions := Statics.AutoSteelOptions or $80;
      for i := 0 to TerrainCollection.Count - 1 do
      begin
        if tdfNoOneWay in TerrainCollection[i].TerrainDrawingFlags then
          Exclude(TDF, tdfNoOneWay)
          else
          Include(TDF, tdfNoOneWay);
      end;
    end;

    {-------------------------------------------------------------------------------
      Get the steel

      #Steel
      # id, xpos, ypos, width, height
      steel_0 = 1328, 80, 48, 128
      steel_1 = 1256, 80, 48, 128
    -------------------------------------------------------------------------------}
    i := 0;
    repeat
      V := List.Values['steel_' + i2s(i)];
      if V = '' then
        Break;
      S := SteelCollection.Add;
      S.SteelX  := StrToIntDef(SplitString(V, 0, ','), -1);
      S.SteelY := StrToIntDef(SplitString(V, 1, ','), -1);
      S.SteelWidth := StrToIntDef(SplitString(V, 2, ','), -1);
      S.SteelHeight := StrToIntDef(SplitString(V, 3, ','), -1);
      if fLoadToLemmix then
      begin
        S.SteelX := S.SteelX div 2;
        S.SteelY := S.SteelY div 2;
        S.SteelWidth := S.SteelWidth div 2;
        S.SteelHeight := S.SteelHeight div 2;
      end;
      if fSuperLemmini and (StrToIntDef(SplitString(V, 4, ','), -1) = 1) then S.SteelType := 1;
      S.ItemActive := true;
      Inc(i);
    until False;

    {Get the locks}
    V := List.Values['object_lock'];
    if V <> '' then
    begin
      i := 0;
      repeat
        if StrToIntDef(SplitString(V, i, ','), -1) <> -1 then
          InteractiveObjectCollection[StrToInt(SplitString(V, i, ','))].ItemLocked := true;
        inc(i);
      until SplitString(V, i, ',') = '';
    end;

    V := List.Values['terrain_lock'];
    if V <> '' then
    begin
      i := 0;
      repeat
        if StrToIntDef(SplitString(V, i, ','), -1) <> -1 then
          TerrainCollection[StrToInt(SplitString(V, i, ','))].ItemLocked := true;
        inc(i);
      until SplitString(V, i, ',') = '';
    end;

    V := List.Values['steel_lock'];
    if V <> '' then
    begin
      i := 0;
      repeat
        if StrToIntDef(SplitString(V, i, ','), -1) <> -1 then
          SteelCollection[StrToInt(SplitString(V, i, ','))].ItemLocked := true;
        inc(i);
      until SplitString(V, i, ',') = '';
    end;

    {-------------------------------------------------------------------------------
      Get the name

      #Name
      name =  Worra load of old blocks!
    -------------------------------------------------------------------------------}
    LevelName := Trim(List.Values['name']);

    if LevelName <> '' then
    begin
    LevelName := FastReplace(LevelName, '\ ', ' ');
    LevelName := FastReplace(LevelName, '\#', '#');
    LevelName := FastReplace(LevelName, '\=', '=');
    LevelName := FastReplace(LevelName, '\:', ':');
    LevelName := FastReplace(LevelName, '\!', '!');
    LevelName := FastReplace(LevelName, '\\', '\');
    end;

    LevelMusicFile := Trim(List.Values['music']);

    if LevelMusicFile <> '' then
    begin
    LevelMusicFile := FastReplace(LevelMusicFile, '\ ', ' ');
    LevelMusicFile := FastReplace(LevelMusicFile, '\#', '#');
    LevelMusicFile := FastReplace(LevelMusicFile, '\=', '=');
    LevelMusicFile := FastReplace(LevelMusicFile, '\:', ':');
    LevelMusicFile := FastReplace(LevelMusicFile, '\!', '!');
    LevelMusicFile := FastReplace(LevelMusicFile, '\\', '\');
    end;

    LevelAuthor := Trim(List.Values['author']);

    if LevelAuthor <> '' then
    begin
    LevelAuthor := FastReplace(LevelAuthor, '\ ', ' ');
    LevelAuthor := FastReplace(LevelAuthor, '\#', '#');
    LevelAuthor := FastReplace(LevelAuthor, '\=', '=');
    LevelAuthor := FastReplace(LevelAuthor, '\:', ':');
    LevelAuthor := FastReplace(LevelAuthor, '\!', '!');
    LevelAuthor := FastReplace(LevelAuthor, '\\', '\');
    end;

    V := List.Values['entranceOrder'];
    SetLength(Statics.fWindowOrder, 0);
    if fSuperLemmini and (V <> '') then
    begin
      i := 0;
      while StrToIntDef(SplitString(V, i, ','), -1) <> -1 do
      begin
        q := -1;
        for h := 0 to InteractiveObjectCollection.Count - 1 do
        begin
          if InteractiveObjectCollection[h].ObjectID = 1 then Inc(q);
          if q = StrToIntDef(SplitString(V, i, ','), -1) then
          begin
            SetLength(Statics.fWindowOrder, i+1);
            Statics.fWindowOrder[i] := h;
            Break;
          end;
          //if h = 127 then break;
        end;
        Inc(i);
        //if i = 128 then break;
      end;
    end;

    //if fResolution = LoRes then
      //AdjustResolution(0.5);
    CacheSizes;
  finally
    List.Free;
    EndUpdate;
  end;
end;

procedure TLevel.SaveToLemminiStream(aStream: TStream);
begin
end;

function TLevel.SaveToLemminiString: string;
begin
end;


procedure TLevel.SaveToFile(const aFileName: string; aFormat: TLemmingFileFormat; UpdateMemName: Boolean = true);
var
  F: TFileStream;
  Ts: String;
  WasChanged: Boolean;
begin
  InternalValidate;
  F := TFileStream.Create(aFileName, fmCreate);
  WasChanged := fChanged;
  BeginUpdate;
  try
    Ts := fFileName;
    fFileName := aFileName;
    SaveToStream(F, aFormat);
    if UpdateMemName then
      fChanged := false
    else begin
      fFileName := Ts;
      fChanged := WasChanged;
    end;
  finally
    F.Free;
    EndUpdate;
  end;
end;



procedure TLevel.SaveToNeoLVLFileRec(var Buf: TNeoLVLRec);
// obsolete procedure, no longer used
begin
end;




procedure TLevel.SaveToLVLFileRec(var Buf: TLVLFileRec);
var
  R: Integer;
  Int16: SmallInt; Int32: Integer;
  U8: Byte;
  H, i: Integer;
  M: Byte;
  W: Word;
  O: ^TLVLObject;
  T: ^TLVLTerrain;
  S: ^TLVLSteel;
  Obj: TInteractiveObject;
  Terrain: TTerrain;
  Steel: TSteel;
  //sktemp: array[0..15] of byte;
  //skt2: array[0..7] of byte;
  //k1, k2: Integer;
  //NeoLemmix: Boolean;
  g2: Integer;


  ParFrom, ParTo: TDefaultsRec;
  //MapNeeded: Boolean;

    function Map(N: Integer): Integer;
    begin
      {if not MapNeeded then
        Result := N;
      if MapNeeded then }
        Result := ConvertCoord(ParFrom.Resolution, ParTo.Resolution, N)
       // Trunc(N * (ParTo.Resolution/ParFrom.Resolution));

    end;

{    function Map(Coord: Integer): Integer;
    begin
      Result := Coord;//MapInt(Coord, fResolution, LoRes);
    end; }


begin


  fStyle.GetStyleParams(ParFrom);
  TBaseDosStyle.GetStyleParams(ParTo); // pre-conversion and validation here!!!
//  MapNeeded := Parto.Resolution <> ParFrom.Resolution;

  FillChar(Buf, SizeOf(Buf), 0);
  FillChar(Buf.Terrain, Sizeof(Buf.Terrain), $FF);
  FillChar(Buf.LevelName, 32, ' ');

  //NeoLemmix := TBaseDosStyle(fStyle).NeoLemmix;


  {-------------------------------------------------------------------------------
    Set the statics.
  -------------------------------------------------------------------------------}
  with Statics do
  begin
     Buf.ReleaseRate                    := ReleaseRate;
     Buf.LemmingsCount                  := LemmingsCount mod 256;
     Buf._LemmingsCountNotUsed          := LemmingsCount div 256;
     Buf.RescueCount                    := RescueCount mod 256;
     Buf._RescueCountNotUsed            := RescueCount div 256;
     Buf.TimeLimit                      := TimeLimit mod 256;
     Buf.ClimberCount                   := ClimberCount;
     Buf.FloaterCount                   := FloaterCount;
     Buf.BomberCount                    := BomberCount;
     Buf.BlockerCount                   := BlockerCount;
     Buf.BuilderCount                   := BuilderCount;
     Buf.BasherCount                    := BasherCount;
     Buf.MinerCount                     := MinerCount;
     Buf.DiggerCount                    := DiggerCount;
     Buf.ScreenPosition := (Map(ScreenPosition) div 8) * 8;
     Buf.ScreenPosition                 := System.Swap(Buf.ScreenPosition);
     Buf.GraphicSet                     := GraphicSet;
     Buf._GraphicSetNoUsed              := MusicTrack;
     Buf.GraphicSetEx                   := GraphicSetEx;
     Buf._GraphicSetExNotUsed := AutoSteelOptions and $10;
     Buf.Reserved1                      := SuperLemming shr 8;
     Buf.Reserved2                      := SuperLemming mod 256;
  end;

  {-------------------------------------------------------------------------------
    Set the objects.
  -------------------------------------------------------------------------------}
  g2 := InteractiveObjectCollection.Count;
  if g2 > 32 then
  begin
    MessageDlg('Warning: The level has more than 32 objects. Some objects may' + #13 + 'be cut out from the saved level. You should remove' + #13 +
                 'excess objects and save the level again.', mtcustom, [mbok], 0);
    g2 := 32;
  end;
  for i := 0 to (g2 - 1) do
  begin
    Obj := InteractiveObjectCollection[i];
    O := @Buf.Objects[i];
    with Obj do
    if Obj.ItemActive then
    begin
      // set xpos: revert the misery
      Int16 := Map(ObjectX);
      Inc(Int16, 16);
      W := Word(Int16);
      W := W and (not 7);
      W := System.Swap(W);
      O^.XPos := W;
      // set ypos: revert the misery
      Int16 := Map(ObjectY);
      W := Word(Int16);
      W := System.Swap(W);
      O^.Ypos := W;
      // set object id
      TWordRec(O^.ObjectID).Hi := Byte(ObjectID);
      // set modifier
      if odfNoOverwrite in ObjectDrawingFlags then
        O^.Modifier := $80
      else if odfOnlyShowOnTerrain in ObjectDrawingFlags then
        O^.Modifier := $40;
      // set displaymode
      if odfInvert in ObjectDrawingFlags then
        O^.DisplayMode := $8F
      else
        O^.DisplayMode := $0F; {default}
    end else
      O^.AsInt64 := 0;
  end;

  {-------------------------------------------------------------------------------
    set the terrain
  -------------------------------------------------------------------------------}
  g2 := TerrainCollection.Count;
  if g2 > 400 then
  begin
    MessageDlg('Warning: The level has more than 400 terrains. Some terrains may' + #13 + 'be cut out from the saved level. You should remove' + #13 +
                 'excess terrains and save the level again.', mtcustom, [mbok], 0);
    g2 := 400;
  end;
  for i := 0 to (g2 - 1) do
  begin
    Terrain := TerrainCollection[i];
    T := @Buf.Terrain[i];
    with Terrain do
    if Terrain.ItemActive then
    begin
      // GET: TerrainX := Integer(T.B0 and 15) shl 8 + Integer(T.B1) - 16;

      H := Map(TerrainX);
      Inc(H, 16);
      T.B0 := Byte(H shr 8) and $0F;
      T.B1 := Byte(H);// + 16;



      // GET: TerrainDrawingFlags := TTerrainDrawingFlags(Byte(T.B0 shr 5));
      M := Byte(TerrainDrawingFlags) shl 5;
      T.B0 := T.B0 or M;
      // GET:
        (*
        H := Integer(T.B2) shl 1 + Integer(T.B3 and $80) shr 7;
        if H >= 256 then
          Dec(H, 512);
        Dec(H, 4);
        TerrainY := Map(H);
        *)
      H := Map(TerrainY);
      Inc(H, 4);
      if H < 0 then
        Inc(H, 512);
      T.B3 := Byte((H or Bit8) shl 7); // still don't know if this is right this "or bit8"
//      H := H and not Bit8;
      H := H shr 1;
      T.B2 := Byte(H);
      // GET: TerrainId := T.B3 and 63; // max = 63.
      T.B3 := T.B3 or (Byte(TerrainID) and 63);
      if TerrainID > 63 then T.B0 := T.B0 + $10;
    end else
      T.AsDWORD := $FFFFFFFF;
  end;

  {-------------------------------------------------------------------------------
    set the steel.
  -------------------------------------------------------------------------------}
  g2 := SteelCollection.Count;
  if g2 > 32 then
  begin
      MessageDlg('Warning: The level has more than 32 steels. Some steels may' + #13 + 'be cut out from the saved level. You should remove' + #13 +
                 'excess steels and save the level again.', mtcustom, [mbok], 0);
    g2 := 32;
  end;
  for i := 0 to (g2 - 1) do
  begin
    Steel := SteelCollection[i];
    S := @Buf.Steel[i];
    with Steel do
    if Steel.ItemActive then
    begin
      // GET: SteelX := ((Integer(B0) shl 1) + (Integer(B1 and Bit7) shr 7)) * 4 - 16;
      S^.B3 := 0;
      Int32 := Map(SteelX);
      Int32 := Int32 and (not 3);
      Int32 := ((Int32 + 16) div 4);
      S^.B1 := Byte((Int32 or Bit8) shl 7); // still don't know this "or bit8"
//      Int32 := Int32 and not Bit8; <-- I THINK THIS WAS WRONG!!
      Int32 := Int32 shr 1;
      S^.B0 := Byte(Int32);
      // GET: SteelY := Integer(S.B1 and not Bit7) * 4;
      Int32 := Map(SteelY);
      Int32 := Int32 div 4;
      S^.B1 := S^.B1 or (Byte(Int32) and not Bit7);
      // GET: SteelWidth  := Integer(S.B2 shr 4) * 4 + 4;
      Int32 := (Map(SteelWidth) - 4);
      Int32 := Int32 div 4;
      S^.B2 := Byte(Int32) shl 4;
      // GET: SteelHeight := Integer(S.B2 and $F) * 4 + 4;
      Int32 := (Map(SteelHeight) - 4);
      Int32 := Int32 div 4;
      S^.B2 := S^.B2 or (Byte(Int32) and not $F0); // highest bit set already
    end else
      S^.AsDWORD := 0;
  end;

  {-------------------------------------------------------------------------------
    set level description
  -------------------------------------------------------------------------------}
  System.Move(LevelName[1], Buf.LevelName, Length(LevelName));

end;

procedure TLevel.SaveToLVLStream(aStream: TStream);
begin
  if TCustLemmStyle(Style).NeoLemmix then
    SaveToNeoLVLStream(aStream)
    else
    SaveToTradLVLStream(aStream);
end;

procedure TLevel.SaveToTradLVLStream(aStream: TStream);
var
  Buf: TLVLFileRec;
begin
  SaveToLVLFileRec(Buf);
  aStream.Write(Buf, SizeOf(Buf));
end;

{procedure TLevel.SaveToNeoLVLStream(aStream: TStream); // Old code
var
  Buf: TNeoLVLRec;
begin
  SaveToNeoLVLFileRec(Buf);
  aStream.Write(Buf, SizeOf(Buf));
end;}

procedure TLevel.SaveToNeoLVLStream(aStream: TStream);
var
  Buf: TNeoLVLHeader;
  SubHead: TNeoLVLSubHeader;
  O: TNewNeoLVLObject;
  T: TNewNeoLVLTerrain;
  S: TNewNeoLVLSteel;
  Obj: TInteractiveObject;
  Terrain: TTerrain;
  Steel: TSteel;
  kstr: String;

  SetList: array of String;

  i: Integer;

  Int16: Integer;
  w: Word;
  b: Byte;

  Str: String;


  ParFrom, ParTo: TDefaultsRec;

    function Map(N: Integer): Integer;
    begin
        Result := N;
    end;

    function GetSetIndex(aName: String): Integer;
    begin
      Result := -1;
      repeat
        Inc(Result);
        if Result = Length(SetList) then SetLength(SetList, Result+1);
        if SetList[Result] = '' then SetList[Result] := aName;
      until SetList[Result] = aName;
    end;


begin

  // Index all the graphic sets that are used
  for i := 0 to InteractiveObjectCollection.Count-1 do
  begin
    Obj := InteractiveObjectCollection[i];
    Obj.fGraphID := GetSetIndex(Obj.Graph.GraphicSetInternalName);
  end;

  for i := 0 to TerrainCollection.Count-1 do
  begin
    Terrain := TerrainCollection[i];
    Terrain.fGraphID := GetSetIndex(Terrain.Graph.GraphicSetInternalName);
  end;


  fStyle.GetStyleParams(ParFrom);
  TBaseDosStyle.GetStyleParams(ParTo); // pre-conversion and validation here!!!
//  MapNeeded := Parto.Resolution <> ParFrom.Resolution;

  FillChar(Buf, SizeOf(Buf), 0);
  FillChar(Buf.LevelName, 32, ' ');
  FillChar(Buf.LevelAuthor, 16, ' ');
  FillChar(Buf.StyleName, 16, ' ');
  FillChar(Buf.VgaspecName, 16, ' ');
  FillChar(Buf.FreeBytes, 32, ' ');


  {-------------------------------------------------------------------------------
    Set the statics.
  -------------------------------------------------------------------------------}
  with Statics do
  begin
     Buf.FormatTag                      := 4;
     Buf.ReleaseRate                    := ReleaseRate;
     if LockedRR then
       Buf.LevelOptions2 := Buf.LevelOptions2 or $01;
     Buf.LemmingsCount                  := LemmingsCount;
     Buf.RescueCount                    := RescueCount;
     if HasTimeLimit then
       Buf.TimeLimit                      := ((TimeLimit mod 256) * 60) + (TimeLimit div 256)
       else
       Buf.TimeLimit := 6000;

        Buf.WalkerCount := WalkerCount;
        Buf.ClimberCount := ClimberCount;
        Buf.SwimmerCount := SwimmerCount;
        Buf.FloaterCount := FloaterCount;
        Buf.GliderCount := GliderCount;
        Buf.MechanicCount := MechanicCount;
        Buf.BomberCount := BomberCount;
        Buf.StonerCount := StonerCount;
        Buf.BlockerCount := BlockerCount;
        Buf.PlatformerCount := PlatformerCount;
        Buf.BuilderCount := BuilderCount;
        Buf.StackerCount := StackerCount;
        Buf.BasherCount := BasherCount;
        Buf.MinerCount := MinerCount;
        Buf.DiggerCount := DiggerCount;
        Buf.ClonerCount := ClonerCount;
        Buf.FencerCount := FencerCount;

        if InfiniteSkills and $8000 <> 0 then Buf.WalkerCount := 200;
        if InfiniteSkills and $4000 <> 0 then Buf.ClimberCount := 200;
        if InfiniteSkills and $2000 <> 0 then Buf.SwimmerCount := 200;
        if InfiniteSkills and $1000 <> 0 then Buf.FloaterCount := 200;
        if InfiniteSkills and $800 <> 0 then Buf.GliderCount := 200;
        if InfiniteSkills and $400 <> 0 then Buf.MechanicCount := 200;
        if InfiniteSkills and $200 <> 0 then Buf.BomberCount := 200;
        if InfiniteSkills and $100 <> 0 then Buf.StonerCount := 200;
        if InfiniteSkills and $80 <> 0 then Buf.BlockerCount := 200;
        if InfiniteSkills and $40 <> 0 then Buf.PlatformerCount := 200;
        if InfiniteSkills and $20 <> 0 then Buf.BuilderCount := 200;
        if InfiniteSkills and $10 <> 0 then Buf.StackerCount := 200;
        if InfiniteSkills and $8 <> 0 then Buf.BasherCount := 200;
        if InfiniteSkills and $4 <> 0 then Buf.MinerCount := 200;
        if InfiniteSkills and $2 <> 0 then Buf.DiggerCount := 200;
        if InfiniteSkills and $1 <> 0 then Buf.ClonerCount := 200;
        if InfiniteSkills and $10000 <> 0 then Buf.FencerCount := 200;

        Buf.Skillset := SkillTypes and $FFFF;
        if SkillTypes and $10000 <> 0 then Buf.LevelOptions2 := Buf.LevelOptions2 or 2;

     Buf.ScreenPosition                 := Map(ScreenPosition) - 160;
     Buf.ScreenYPosition                := Map(ScreenYPosition) - 80;
     //Buf.GraphicSet                     := GraphicSet;
     Buf.MusicNumber                    := 0; // we don't use this anymore
     //Buf.GraphicSetEx                   := GraphicSetEx;
     Buf.LevelOptions                   := (AutoSteelOptions or $61) and $EF;

     Buf.Width                    := Width;
     Buf.Height                   := Height;

     Buf.VgaspecX := VgaspecX;
     Buf.VgaspecY := VgaspecY;

     Buf.Resolution := 8;

     if LevelID = 0 then
      begin
        Randomize;
        LevelID := Random($7FFFFFFE) + Random($7FFFFFFF) + 1;
      end;

     Buf.LevelID := LevelID;

     Buf.BgIndex := BackgroundIndex;

     {for i := 0 to 31 do
       Buf.WindowOrder[i] := fWindowOrder[i];}
  end;

  {-------------------------------------------------------------------------------
    set level description
  -------------------------------------------------------------------------------}
  System.Move(LevelName[1], Buf.LevelName, Length(LevelName));
  System.Move(LevelAuthor[1], Buf.LevelAuthor, Length(LevelAuthor));

  kstr := LowerCase(LeftStr(fGraph.GraphicSetInternalName, 16));
  System.Move(kstr[1], Buf.StyleName, Length(kstr));

  kstr := 'none';
  System.Move(kstr[1], Buf.VgaspecName, Length(kstr));

  if pos(':', SVersion) = 0 then
    kstr := 'NeoLemmix Editor ' + SVersion
  else
    kstr := 'NeoLemmix Editor ' + LeftStr(SVersion, pos(':', SVersion)-1) + '-exp';
  System.Move(kstr[1], Buf.FreeBytes, Length(kstr));

  aStream.Write(Buf, SizeOf(Buf));

  for i := 0 to InteractiveObjectCollection.Count-1 do
  begin
    Obj := InteractiveObjectCollection[i];
    if Obj.Graph.MetaObjectCollection[Obj.ObjectID].TriggerEffect = 32 then Continue;
    with Obj do
    if Obj.ItemActive then
    begin
      b := 1;
      aStream.Write(b, 1);

      FillChar(O, SizeOf(O), 0);

      // set xpos: revert the misery
      Int16 := Map(ObjectX);
      //W := Word(Int16);
      O.XPos := Int16;
      // set ypos: revert the misery
      Int16 := Map(ObjectY);
      //W := Word(Int16);
      O.YPos := Int16;
      // set object id
      O.ObjectID := ObjectID;
      O.LValue := ObjectLValue;
      O.SValue := ObjectSValue;
      // set modifier
      if odfNoOverwrite in ObjectDrawingFlags then
        O.ObjectFlags := $1
      else if odfOnlyShowOnTerrain in ObjectDrawingFlags then
        O.ObjectFlags := $2;
      if odfInvert in ObjectDrawingFlags then
        O.ObjectFlags := O.ObjectFlags + $4;
      if odfFaceLeft in ObjectDrawingFlags then
        O.ObjectFlags := O.ObjectFlags + $8;
      if odfFake in ObjectDrawingFlags then
        O.ObjectFlags := O.ObjectFlags + $10;
      if odfInvisible in ObjectDrawingFlags then
        O.ObjectFlags := O.ObjectFlags + $20;
      if odfFlip in ObjectDrawingFlags then
        O.ObjectFlags := O.ObjectFlags + $40;
      if odfRotate in ObjectDrawingFlags then
        O.ObjectFlags := O.ObjectFlags + $100;

      O.ObjectFlags := O.ObjectFlags + $80;

      O.Graph := fGraphID;

      if Obj.ItemLocked then O.Locked := 1;
      aStream.Write(O, SizeOf(O));
    end;
  end;


  for i := 0 to TerrainCollection.Count-1 do
  begin
    Terrain := TerrainCollection[i];
    with Terrain do
    if Terrain.ItemActive then
    begin
      b := 2;
      aStream.Write(b, 1);

      FillChar(T, SizeOf(T), 0);

      Int16 := Map(TerrainX);
      T.XPos := Int16;

      Int16 := Map(TerrainY);
      T.YPos := Int16;

      T.TerrainID := TerrainID;

      if tdfNoOverwrite in TerrainDrawingFlags then
        T.TerrainFlags := $1
      else if tdfErase in TerrainDrawingFlags then
        T.TerrainFlags := $2;
      if tdfInvert in TerrainDrawingFlags then
        T.TerrainFlags := T.TerrainFlags + $4;
      if tdfFlip in TerrainDrawingFlags then
        T.TerrainFlags := T.TerrainFlags + $8;
      if tdfNoOneWay in TerrainDrawingFlags then
        T.TerrainFlags := T.TerrainFlags + $10;
      if tdfRotate in TerrainDrawingFlags then
        T.TerrainFlags := T.TerrainFlags + $20;

      T.TerrainFlags := T.TerrainFlags + $80;

      T.Graph := fGraphID;

      if Terrain.ItemLocked then T.Locked := 1;

      aStream.Write(T, SizeOf(T));
    end;
  end;


  for i := 0 to SteelCollection.Count-1 do
  begin
    Steel := SteelCollection[i];
    with Steel do
    if Steel.ItemActive then
    begin
      b := 3;
      aStream.Write(b, 1);

      FillChar(S, SizeOf(S), 0);

      S.XPos := SteelX;
      S.YPos := SteelY;
      S.SteelWidth := SteelWidth - 1;
      S.SteelHeight := SteelHeight - 1;
      S.SteelFlags := SteelType;
      S.SteelFlags := S.SteelFlags + $80;

      if Steel.ItemLocked then S.Locked := 1;

      aStream.Write(S, SizeOf(S));
    end
  end;

  b := 5;
  aStream.Write(b, 1);
  FillChar(SubHead, SizeOf(SubHead), 0);
  FillChar(SubHead.MusicName, 16, 32);
  SubHead.ScreenStartX := Map(Statics.ScreenPosition) - 160;
  SubHead.ScreenStartY := Map(Statics.ScreenYPosition) - 80;
  kstr := MidStr(LevelMusicFile, 1, 16);
  System.Move(kstr[1], SubHead.MusicName, Length(kstr));
  SubHead.PostSecRank := 0;
  SubHead.PostSecLevel := 0;
  SubHead.BnsRank := 0;
  SubHead.BnsLevel := 0;
  SubHead.ClockStart := 0;
  SubHead.ClockEnd := 0;
  SubHead.ClockCount := 0;
  aStream.Write(SubHead, SizeOf(SubHead));

  b := 6;
  aStream.Write(b, 1);
  w := Length(SetList);
  aStream.Write(w, 2);
  for i := 0 to w-1 do
  begin
    Str := SetList[i];
    while Length(Str) < 16 do
      Str := Str + ' ';
    aStream.Write(Str[1], 16);
  end;

  b := 0;
  aStream.Write(b, 1); //end of file marker

end;


procedure TLevel.CacheSizes;
var
  i: Integer;
begin
  (*
  with InteractiveObjectCollection do
    for i := 0 to Count - 1 do
    begin
      with Items[i] do
      begin
        with PictureMgr.ObjectBitmaps[Statics.GraphicSet, ObjectID] do
        begin
          fCachedWidth := Width;
          fCachedHeight := Height;
        end;
      end;
    end;

  with TerrainCollection do
    for i := 0 to Count - 1 do
    begin
      with Items[i] do
      begin
        with PictureMgr.TerrainBitmaps[Statics.GraphicSet, TerrainID] do
        begin
          fCachedWidth := Width;
          fCachedHeight := Height;
        end;
      end;
    end;
  *)
end;

procedure TLevel.AdjustResolution(Factor: Single);
var
  i: Integer;
begin
  if Factor = 1 then
    Exit;
  BeginUpdate;
  try
    with Statics do
    begin
      ScreenPosition := Trunc(ScreenPosition * Factor);
      ScreenYPosition := Trunc(ScreenYPosition * Factor);
      Width := Trunc(Width * Factor);
      Height := Trunc(Height * Factor);
      VgaspecX := Trunc(VgaspecX * Factor);
      VgaspecY := Trunc(VgaspecY * Factor);
      FallDistance := Trunc(FallDistance * Factor);
    end;

    with InteractiveObjectCollection do
      for i := 0 to Count - 1 do
        with Items[i] do
        begin
          ObjectX := Trunc(ObjectX * Factor);
          ObjectY := Trunc(ObjectY * Factor);
        end;

    with TerrainCollection do
      for i := 0 to Count - 1 do
        with Items[i] do
        begin
          TerrainX := Trunc(TerrainX * Factor);
          TerrainY:= Trunc(TerrainY * Factor);
        end;

    with SteelCollection do
      for i := 0 to Count - 1 do
        with Items[i] do
        begin
          SteelX := Trunc(SteelX * Factor);
          SteelY:= Trunc(SteelY * Factor);
          SteelWidth := Trunc(SteelWidth * Factor);
          SteelHeight := Trunc(SteelHeight * Factor);
        end;

  finally
    EndUpdate;
  end;
end;

procedure TLevel.MsgLevelChanging;
begin
  Mgr.Perform(LM_LEVELCHANGING, 0, 0);
end;

procedure TLevel.MsgLevelChanged;
begin
  Mgr.Perform(LM_LEVELCHANGED, 0, fChangeFlags);
end;

procedure TLevel.MsgListChanged(Which: TCollection; Item: TCollectionItem);
begin
  if Which = InteractiveObjectCollection then
    Mgr.Perform(LM_OBJECTLISTCHANGED, Longint(Item), 0)
  else if Which = TerrainCollection then
    Mgr.Perform(LM_TERRAINLISTCHANGED, Longint(Item), 0)
  else if Which = SteelCollection then
    Mgr.Perform(LM_STEELLISTCHANGED, Longint(Item), 0)
end;

procedure TLevel.Mgr_BeforeSendMessage(Sender: TObject; var DoSend: Boolean);
{-------------------------------------------------------------------------------
  If we're updating (that is when we are loading a level from stream) we
  don't send any messages.
-------------------------------------------------------------------------------}
begin
  DoSend := fUpdateCount = 0;
end;

procedure TLevel.CheckObjectX(aObject: TInterActiveObject; var X: Integer);
begin
  //lemedit
end;

function TLevel.IsUpdating: Boolean;
begin
  Result := fUpdateCount > 0;
end;

procedure TLevel.SaveToStream(aStream: TStream; aFormat: TLemmingFileFormat);
begin
  case aFormat of
    lffAutoDetect :;
    lffLemmini:
      begin
        SaveToLemminiStream(aStream);
        fCurrentFormat := aFormat;
        //LoadFromLemminiStream(aStream);
      end;
    lffLVL:
      begin
        SaveToLVLStream(aStream);
        fCurrentFormat := aFormat;
        //LoadFromLVLStream(aStream);
      end;
  end;
end;

procedure TLevel.SetFileName(const Value: string);
begin
  if CompareText(fFileName, Value) = 0 then
    Exit;
  fFileName := Value;
end;

procedure TLevel.MsgStaticsChanged(aChangeInfo: Integer);
begin
  Mgr.Perform(LM_STATICSCHANGED, aChangeInfo, 0);
end;

procedure TLevel.CloseLevel;
begin
  BeginUpdate;
  try
    ClearLevel(True, True);
    FileName := '';
  finally
    EndUpdate;
  end;
end;

procedure TLevel.InternalValidate;
begin

  {if InteractiveObjectCollection.Count > LVL_MAXOBJECTCOUNT then
    LevelError('too much objects', []);
  if TerrainCollection.Count > LVL_MAXTERRAINCOUNT then
    LevelError('too much terrain', []);
  if SteelCollection.Count > LVL_MAXSTEELCOUNT then
    LevelError('too much steel', []);}

end;

procedure TLevel.SetEditing(const Value: Boolean);
begin
  if fEditing = Value then
    Exit;
  fEditing := Value;
end;

procedure TLevel.ExcludeStates(aFlags: Cardinal);
begin
  fStateFlags := fStateFlags and not aFlags;
end;

procedure TLevel.IncludeStates(aFlags: Cardinal);
begin
  fStateFlags := fStateFlags or aFlags;
end;

procedure TLevel.IncludeChanges(aFlags: Cardinal);
begin
  fChangeFlags := fChangeFlags or aFlags;
end;

function TLevel.GetAdjustedFileName(const aFilename: string; aFormat: TLemmingFileFormat): string;
var
  Ext: string;


    function GetExt: string;
    begin
      Result := '';
      case aFormat of
        lffLemmini: Result := '.ini';
        lffLVL: Result := '.lvl';
      end;
    end;
begin
  // add default extension if there is none
  Result := aFileName;
  Ext := ExtractFileExt(Result);
  if Ext = '' then
    Result := Result + GetExt;//ReplaceFileExt(Result, GetExt);
end;

procedure TLevel.SetStyle(const Value: TBaseLemmingStyle);
{-------------------------------------------------------------------------------
  When changing style, check if we can find a compatible graphicset.
  If not then exception.
-------------------------------------------------------------------------------}


{    procedure ConvertCoords(aOldStyle, aNewStyle: TBaseLemmingStyle);
    var
      i: Integer;
      OldRes, NewRes: Single;
    begin
      OldRes := aOldGraph.

    end; }

var
  OldStyle: TBaseLemmingStyle;
  TempGraph: TBaseGraphicSet;
begin
  if fStyle = Value then
    Exit;
  BeginUpdate;

  try
    {TempGraph := Graph;
    OldStyle := fStyle;
    if Assigned(Value) and Assigned(fStyle) and Assigned(fGraph) then
      TempGraph := GetCompatibleGraph(fGraph, Value);
    Graph := TempGraph;}

    OldStyle := fStyle;
    fStyle := Value;

    if OldStyle <> nil then
      if Style <> nil then
      AdjustResolution(Style.Getparams.Resolution/OldStyle.GetParams.Resolution);
//    ConvertCoords(OldGraph, Graph);

    IncludeChanges(LCF_STYLECHANGED);
  finally
    EndUpdate;
  end;
end;

procedure TLevel.SetGraph(const Value: TBaseGraphicSet);
begin
  if fGraph = Value then
    Exit;
  BeginUpdate;
  try
    fGraph := Value;
    if fGraph <> nil then
    begin
      fGraph.EnsureMetaData;
      Statics.GraphicSet := fGraph.GraphicSetId;
      //Statics.GraphicSetEx := fGraph.GraphicSetIdExt;
    end;
    IncludeChanges(LCF_GRAPHCHANGED);
  finally
    EndUpdate;
  end;
end;

function TLevel.GetCompatibleGraph(aGraph: TBaseGraphicSet; aStyle: TBaseLemmingStyle): TBaseGraphicSet;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to aStyle.GraphicSetList.Count - 1 do
  begin
    Result := aStyle.GraphicSets[i];
    if CompareText(Result.GraphicSetInternalName, aGraph.GraphicSetInternalName) = 0 then
      Exit;
  end;
  Result := nil;
  raise Exception.Create('No compatible graphicset found');
end;

function TLevel.IsLocked: Boolean;
begin
  Result := LevelLock > 0;
end;

procedure TLevel.LockLevel;
begin
  Inc(LevelLock);

end;

procedure TLevel.UnlockLevel;
begin

  Dec(LevelLock);
end;

procedure TLevel.LoadFromDosArchive(aArchive: TDosDatArchive; aIndex: Integer; aStyle: TBaseLemmingStyle);
var
  Mem: TMemoryStream;
begin
  Mem := TMemoryStream.Create;
  try
    aArchive.ExtractSection(aIndex, Mem);
    Mem.Seek(0, soFromBeginning);
    LoadFromStream(Mem, aStyle, lffLVL);
   finally
     Mem.Free;
   end;
end;


procedure TLevel.LoadFromDosArchiveFile(const aFileName: string; aIndex: Integer; aStyle: TBaseLemmingStyle);
var
  Arc: TDosDatArchive;
begin
  Arc := TDosDatArchive.Create;
  try
    Arc.LoadFromFile(aFileName);
    LoadFromDosArchive(Arc, aIndex, aStyle);
  finally
    Arc.Free;
  end;
end;


{ TCustomLevelValidator }

class function TCustomLevelValidator.ValidateLevel(aLevel: TLevel; aReport: TStrings): Boolean;
{-------------------------------------------------------------------------------
  This is a temporary solution. LVL checking
-------------------------------------------------------------------------------}

var
  i: Integer;
  O: TInteractiveObject;
  T: TTerrain;
  S: TSteel;
  EntryFound, ExitFound: Boolean;

    procedure Err(const S: string);
    begin
      Result := False;
      //deb([s]);
      aReport.Add(S)
//      raise Exception.Create(S);
    end;

    procedure OErr(aIndex: Integer; const S: string);
    begin
      Err('Object ' + i2s(aIndex) + ': ' + S);
    end;

    procedure TErr(aIndex: Integer; const S: string);
    begin
      Err('Terrain ' + i2s(aIndex) + ': ' + S);
    end;

    procedure SErr(aIndex: Integer; const S: string);
    begin
      Err('Steel ' + i2s(aIndex) + ': ' + S);
    end;

    


    function Map(N: Integer): Integer;
    begin
      Result := N;//MapInt(N, aLevel.Resolution, LoRes);
    end;

begin

  Assert(Assigned(aLevel), 'validatelevel level = nil');
  Result := True;
  EntryFound := False;
  ExitFound := False;
  aReport.Add('Note: This is just a guide. It might not catch every possible problem.');
  with aLevel do
  begin

    //if Map(Statics.ScreenPosition) mod 8 <> 0 then Err('Screenstart is not a multiply of 8');

    with InteractiveObjectCollection, HackedList do
    begin
      //if Count > LVL_MAXOBJECTCOUNT then Err('Too many objects');
      for i := 0 to Count - 1 do
      begin
        O := List^[i];
        if not O.ItemActive then Continue;
        //-32768 to +32767
        if Map(O.ObjectX) < -32768 then OErr(i, 'ObjectX too small');
        if Map(O.ObjectX) > 32767 then OErr(i, 'ObjectX too large');
        if Map(O.ObjectY) < -32768 then OErr(i, 'ObjectY too small');
        if Map(O.ObjectY) > 32767 then OErr(i, 'ObjectY too large');
        if (aLevel.Style is TCustLemmStyle) and not (TCustLemmStyle(aLevel.Style).NeoLemmix) then
        begin
          if Map(O.ObjectX) mod 8 <> 0 then OErr(i, 'ObjectX is not a multiple of 8');
          if Map(O.ObjectY) mod 4 <> 0 then OErr(i, 'Warning: ObjectY not multiply of 4');
        end;


        if not O.ValidObjectID(O.ObjectID) then OErr(i, 'Object ID is not valid');
        if O.ObjectID = 1 then
          EntryFound := True;
        if O.ObjectID = 0 then
          ExitFound := True;

      end;
    end;

    if not EntryFound then
      Err('No Lemming Entry found');

    if not ExitFound then
      Err('No Lemming Exit found');

    with TerrainCollection, HackedList do
    begin
      //if Count > LVL_MAXTERRAINCOUNT then Err('Too much terrain pieces');
      for i := 0 to Count - 1 do
      begin
        T := List^[i];
        if not T.ItemActive then Continue;
        if Map(T.TerrainX) < -32768 then TErr(i, 'TerrainX too small');
        if Map(T.TerrainX) > 32767 then TErr(i, 'TerrainX too big');
        if Map(T.TerrainY) < -32768 then TErr(i, 'TerrainY too small');
        if Map(T.TerrainY) > 32767 then TErr(i, 'TerrainY too big');
        if not T.ValidTerrainID(T.TerrainID) then TErr(i, 'Terrain ID is not valid');
      end;
    end;

    with SteelCollection, HackedList do
    begin
      //if Count > LVL_MAXSTEELCOUNT then Err('Too much steel');
      for i := 0 to Count - 1 do
      begin
        S := List^[i];
        if not S.ItemActive then Continue;
        if Map(S.SteelX) < -32768 then SErr(i, 'SteelX too small');
        if Map(S.SteelX) > 32767 then SErr(i, 'SteelX too big');
        if Map(S.SteelY) < -32768 then SErr(i, 'SteelY too small');
        if Map(S.SteelY) > 32767 then SErr(i, 'SteelY too big');
        if Map(S.SteelWidth) < 1 then SErr(i, 'SteelWidth too small');
        if Map(S.SteelWidth) > 256 then SErr(i, 'SteelWidth too big');
        if Map(S.SteelHeight) < 1 then SErr(i, 'SteelHeight too small');
        if Map(S.SteelHeight) > 256 then SErr(i, 'SteelHeight too big');
        if (aLevel.Style is TCustLemmStyle) and not (TCustLemmStyle(aLevel.Style).NeoLemmix) then
        begin
          if Map(S.SteelX) mod 4 <> 0 then SErr(i, 'SteelX not divisible by 4');
          if Map(S.SteelY) mod 4 <> 0 then SErr(i, 'SteelY not divisible by 4');
          if Map(S.SteelWidth) mod 4 <> 0 then SErr(i, 'SteelWidth not divisible by 4');
          if Map(S.SteelHeight) mod 4 <> 0 then SErr(i, 'SteelHeight not divisible by 4');
        end;
      end;
    end;
  end;
end;


{ TLemmingPalette }

destructor TLemmingPalette.Destroy;
begin
  Colors := nil;
  inherited;
end;

{ TLemmingPaletteList }

function TLemmingPaletteList.Add(Item: TLemmingPalette): Integer;
begin
  Result := inherited Add(Item);
end;

function TLemmingPaletteList.GetItem(Index: Integer): TLemmingPalette;
begin
  Result := inherited Get(Index);
end;

procedure TLemmingPaletteList.Insert(Index: Integer; Item: TLemmingPalette);
begin
  inherited Insert(Index, Item);
end;

{ TLevelInfo }

function TLevelInfo.GetOwnerSection: TPackSection;
begin
  Result := TPackSection(Collection.Owner);
end;


{ TLevelInfos }

function TLevelInfos.Add: TLevelInfo; 
begin 
  Result := TLevelInfo(inherited Add);
end; 

constructor TLevelInfos.Create(aOwner: TPersistent); 
begin 
  inherited Create(aOwner, TLevelInfo); 
end; 

function TLevelInfos.GetItem(Index: Integer): TLevelInfo;
begin 
  Result := TLevelInfo(inherited GetItem(Index)) 
end; 

function TLevelInfos.Insert(Index: Integer): TLevelInfo; 
begin
  Result := TLevelInfo(inherited Insert(Index))
end;

procedure TLevelInfos.SetItem(Index: Integer; const Value: TLevelInfo);
begin
  inherited SetItem(Index, Value);
end;

{ TPackSection }

constructor TPackSection.Create(Collection: TCollection);
begin
  inherited;
  fLevelInfos := TLevelInfos.Create(Self);
end;

destructor TPackSection.Destroy;
begin
  fLevelInfos.Free;
  inherited;
end;

function TPackSection.GetOwnerPack: TLevelPack;
begin
  Result := TLevelPack(Collection.Owner);
end;

{ TPackSections }

function TPackSections.Add: TPackSection;
begin
  Result := TPackSection(inherited Add);
end;

constructor TPackSections.Create(aOwner: TPersistent);
begin
  inherited Create(aOwner, TPackSection);
end;

function TPackSections.GetItem(Index: Integer): TPackSection;
begin
  Result := TPackSection(inherited GetItem(Index))
end;

function TPackSections.Insert(Index: Integer): TPackSection;
begin
  Result := TPackSection(inherited Insert(Index))
end;

procedure TPackSections.SetItem(Index: Integer; const Value: TPackSection);
begin
  inherited SetItem(Index, Value);
end;

{ TLevelPack }

constructor TLevelPack.Create(Collection: TCollection);
begin
  inherited;
  fPackSections := TPackSections.Create(Self);
end;

destructor TLevelPack.Destroy;
begin
  fPackSections.Free;
  inherited;
end;


(*function TLevelPack.GetPackSection(Index: Integer): TPackSection;
begin
  Result := fPackSections[Index];
end;*)

function TLevelPack.GetPackSection(Index: Integer): TPackSection;
begin
  Result := fPackSections[Index];
end;

{ TLevelPacks }

function TLevelPacks.Add: TLevelPack;
begin
  Result := TLevelPack(inherited Add);
end; 

constructor TLevelPacks.Create(aOwner: TPersistent);
begin
  inherited Create(aOwner, TLevelPack);
end; 

function TLevelPacks.GetItem(Index: Integer): TLevelPack; 
begin 
  Result := TLevelPack(inherited GetItem(Index))
end;

function TLevelPacks.Insert(Index: Integer): TLevelPack;
begin
  Result := TLevelPack(inherited Insert(Index))
end;

procedure TLevelPacks.SaveToStream(S: TStream);
var
  Wrapper: TLevelPacksWrapper;
begin
  Wrapper := TLevelPacksWrapper.Create(nil);
  try
    Wrapper.LevelPacks := Self;
    Wrapper.SaveToStream(S);
  finally
    Wrapper.Free;
  end;
end;

procedure TLevelPacks.SaveToTxtFile(const aFileName: string);
var
  Wrapper: TLevelPacksWrapper;
begin
  Wrapper := TLevelPacksWrapper.Create(nil);
  try
    Wrapper.LevelPacks := Self;
    Wrapper.SaveToTxtFile(aFileName);
  finally
    Wrapper.Free;
  end;
end;

procedure TLevelPacks.SetItem(Index: Integer; const Value: TLevelPack);
begin 
  inherited SetItem(Index, Value); 
end;

{ TBaseLemmingStyle }

constructor TBaseLemmingStyle.Create;
var
  aLevelLoaderClass: TBaseLevelLoaderClass;
  aAnimationSetClass: TBaseAnimationSetClass;
begin
  inherited Create;
  fGraphicSetList := TBaseGraphicSetList.Create;
  aLevelLoaderClass := GetLevelLoaderClass;
  if aLevelLoaderClass <> nil then fLevelLoader := aLevelLoaderClass.Create(Self);
  aAnimationSetClass := GetAnimationSetClass;
  if aAnimationSetClass <> nil then fAnimationSet := aAnimationSetClass.Create(Self);
end;

destructor TBaseLemmingStyle.Destroy;
begin
  fGraphicSetList.Free;
  if fAnimationSet <> nil then fAnimationSet.Free;
  if fLevelLoader <> nil then fLevelLoader.Free;
  inherited Destroy;
end;

{class function TBaseLemmingStyle.GetID: Integer;
begin
  raise Exception.Create(ClassName + '.GetID not implemented');
//  Result := -1;
end;}

function TBaseLemmingStyle.IncludeCommonPath(const S: string): string;
begin
  if ExtractFileDrive(S) = '' then
    Result := ExtractFilePath(ParamStr(0)) + CommonPath + S
  else
    Result := S;
end;

class function TBaseLemmingStyle.GetParams: TDefaultsRec;
begin
  GetStyleParams(Result);
end;

function TBaseLemmingStyle.FindGraphicSetName(aGraphicSetName: String; SilentFail: Boolean = false): TBaseGraphicSet;
var
  i: Integer;
  Sty: TBaseLemmingStyle;

  function CheckIfCorrectSet(aGraph: TBaseGraphicSet): Boolean;
  begin
    Result := false;
    if aGraph.IsSpecial then Exit;
    if Lowercase(aGraphicSetName) = Lowercase(aGraph.GraphicSetInternalName) then Result := true;
    if Lowercase(aGraphicSetName) = Lowercase(aGraph.GraphicSetAltName) then Result := true;
  end;
begin
  Result := nil;
  for i := 0 to GraphicSetCount - 1 do
  begin
    Result := GraphicSets[i];
    //deb([i, result.GraphicSetName, result.GraphicSetId, result.GraphicSetIdExt]);
    if CheckIfCorrectSet(Result) then
    begin
      Result.EnsureMetaData;
      Exit;
    end;
  end;
  if LeftStr(LowerCase(aGraphicSetName), 5) = 'vgagr' then
    for i := 0 to GraphicSetCount-1 do
    begin
      Result := GraphicSets[i];
      if (Result.GraphicSetOrderNumber = StrToIntDef(Trim(MidStr(aGraphicSetName, 6, Length(aGraphicSetName))), -1))
      and (Result.IsSpecial = false) then Exit;
    end;
  if SilentFail then
    Result := nil
  else begin
    ShowMessage('Graphic set "' + aGraphicSetName + '" could not be found. Please choose a graphic set to replace it with.');
    Sty := self;
    ExecuteNewLevelForm(Sty, Result, true, aGraphicSetName);
  end;
end;

function TBaseLemmingStyle.FindGraphicSet(aGraphicSetId: Integer; SilentFail: Boolean = false): TBaseGraphicSet;
  procedure Get(aName: String);
  begin
    Result := FindGraphicSetName(aName, SilentFail);
  end;
begin
  Result := nil;

  case aGraphicSetId of
    0: Get('dirt');
    1: Get('fire');
    2: Get('marble');
    3: Get('pillar');
    4: Get('crystal');
    5: Get('brick');
    6: Get('rock');
    7: Get('snow');
    8: Get('bubble');
    9: Get('xmas');
    10: Get('tree');
    11: Get('purple');
    12: Get('psychedelic');
    13: Get('metal');
    14: Get('desert');
    15: Get('sky');
    16: Get('circuit');
    17: Get('martian');
    18: Get('lab');
    19: Get('sega');
    20: Get('dirt_md');
    21: Get('fire_md');
    22: Get('marble_md');
    23: Get('pillar_md');
    24: Get('crystal_md');
    25: Get('horror');
    else if not SilentFail then raise Exception.Create('Unknown graphic set ID');
  end;
end;

procedure TBaseLemmingStyle.DebugGraphs;
var
  i: Integer;
  G: TBaseGraphicSet;
begin
  for i := 0 to GraphicSetCount - 1 do
  begin
    G := GraphicSets[i];
    //deb([i, g.GraphicSetName, g.GraphicSetId, g.GraphicSetIdExt]);
    //if Result.GraphicSetIdExt = Ex then
      //Exit;
  end;

end;

function TBaseLemmingStyle.GetGraphicSet(aIndex: Integer): TBaseGraphicSet;
begin
  Result := fGraphicSetList[aIndex];
end;

function TBaseLemmingStyle.GetGraphicSetCount: Integer;
begin
  Result := fGraphicSetList.Count;
end;

class procedure TBaseLemmingStyle.GetStyleParams(var aParams: TDefaultsRec);
begin
  raise Exception.Create(ClassName + '.GetStyleParams not implemented');
end;

function TBaseLemmingStyle.GraphByName(const aGraphName: string): TBaseGraphicSet;
var
  i: Integer;
begin
  for i := 0 to fGraphicSetList.Count - 1 do
  begin
    Result := fGraphicSetList.List^[i];
    if CompareText(Result.GraphicSetInternalName, aGraphName) = 0 then
      Exit;
  end;
  raise Exception.Create(ClassName + ' (' + StyleName + ') : GraphicSet not found (' + aGraphName + ')');
end;

class function TBaseLemmingStyle.GetLevelLoaderClass: TBaseLevelLoaderClass;
begin
  Result := nil;
end;

class function TBaseLemmingStyle.GetAnimationSetClass: TBaseAnimationSetClass;
begin
  Result := nil;
end;

{ TLemmingStyleList }

function TLemmingStyleList.Add(Item: TBaseLemmingStyle): Integer;
begin
  Result := inherited Add(Item);
end;

function TLemmingStyleList.GetItem(Index: Integer): TBaseLemmingStyle;
begin
  Result := inherited Get(Index);
end;

procedure TLemmingStyleList.Insert(Index: Integer; Item: TBaseLemmingStyle);
begin
  inherited Insert(Index, Item);
end;


{ TStyleMgr }

constructor TStyleMgr.Create;
begin
  inherited Create;
  fStyleNameList := TStringList.Create;
  fStyleList := TLemmingStyleList.Create;
end;

destructor TStyleMgr.Destroy;
begin
  fStyleNameList.Free;
  fStyleList.Free;
  inherited;
end;

procedure TStyleMgr.ScanWriteStyleProperty(aObject: TObject; const aPropName: string; aPropType: TTypeKind);
var
  V: Variant;
const
  PropTypes = [tkInteger, tkChar, tkEnumeration, tkFloat, tkString, tkSet, tkWChar, tkLString, tkWString];
begin
  if not (aPropType in PropTypes) then
    Exit;
  V := GetPropValue(aObject, aPropName);
  fCurrentIni.WriteString(fCurrentSection, aPropName, V);
end;

procedure TStyleMgr.ScanWriteGraphProperty(aObject: TObject; const aPropName: string; aPropType: TTypeKind);
var
  V: Variant;
//  S: string;

    function ii(const a: cardinal): string;
    begin
      result := inttostr(a);
    end;

const
  PropTypes = [tkInteger, tkChar, tkEnumeration, tkFloat, tkString, tkSet, tkWChar, tkLString, tkWString];
begin
  //deb([aPropName, ord(aproptype)]);
//  deb([getenumname(typeinfo(TTypeKind), integer(aProptype)), aPropName]);
  if not (aPropType in PropTypes) then
    Exit;
  V := GetPropValue(aObject, aPropName, False);

//  S := GetPropValue(aObject, aPropName);

  if aPropType = tkInteger then
    fCurrentIni.WriteString(fCurrentSection, aPropName, ii(v))
 else
    fCurrentIni.WriteString(fCurrentSection, aPropName, v);
end;

procedure TStyleMgr.SaveToIni(const aFilename: string);
var
  i, j: Integer;
  C: TClass;
  N, V: string;
  Ini: TMemIniFile;
  Sty: TBaseLemmingStyle;
  Gra: TBaseGraphicSet;

begin
  Ini := TMemIniFile.Create(aFileName);
  Ini.Clear;
  fCurrentIni := Ini;
  try
    // write styles
    for i := 0 to StyleList.Count - 1 do
    begin
      Sty := StyleList[i];
      fCurrentSection := 'style_' + i2s(i);
      Ini.WriteString(fCurrentSection, 'StyleClass', Sty.ClassName);
      ForEachProperty(Sty, ScanWriteStyleProperty);
    end;

    // write graphicsets
    for i := 0 to StyleList.Count - 1 do
    begin
      Sty := StyleList[i];
      for j := 0 to Sty.GraphicSetList.Count - 1 do
      begin
        Gra := Sty.GraphicSetList[j];
        fCurrentSection := Sty.StyleName + '_' + i2s(j);
        Ini.WriteString(fCurrentSection, 'GraphicSetClass', Gra.ClassName);
        ForEachProperty(Gra, ScanWriteGraphProperty);
      end;
    end;



    // classes
(*    for i := 0 to ClassCount - 1 do
    begin
      N := ClassList[i];
      C := SafeGetClass(N);
      if C = nil then
        raise Exception.Create('style class error');
      V := C.ClassName;
      M.WriteString('StyleClasses', N, V);
    end; *)

(*    // instances
    for i := 0 to fStyleNameList.Count - 1 do
    begin
      N := fStyleNameList[i];
      C := TClass(fStyleNameList.Objects[i]);
      V := C.ClassName;
      M.WriteString('Loaded Styles', N, V);
    end; *)
    Ini.UpdateFile;
  finally
    fCurrentIni := nil;
    Ini.Free;
  end;
end;

procedure TStyleMgr.LoadFromIni(const aFilename: string);
{-------------------------------------------------------------------------------
  Read the styles and graphicsets from inifile.
  Then create the classes. published properties are read.
-------------------------------------------------------------------------------}
var
  Ini: TMemIniFile;

    function CreateStyleFromSection(const aSection: string): TBaseLemmingStyle;
    var
      List: TStringList;
      i: Integer;
      aClassName, PropName, PropVal: string;
    begin
      Result := nil;
      List := TStringList.Create;
      Ini.ReadSection(aSection, List);
      try
        // read classname and create instance
        aClassName := Ini.ReadString(aSection, 'StyleClass', '');
        Result := TBaseLemmingStyleClass(FindClass(aClassName)).Create;
        // fill published props
        for i := 0 to List.Count - 1 do
        begin
          PropName := List[i];
          PropVal := Ini.ReadString(aSection, PropName, '');
          if (PropVal <> '') and IsPublishedProp(Result, PropName) then
            SetPropValue(Result, PropName, PropVal);
        end;

      finally
        List.Free;
      end;
    end;

    procedure CreateGraphicSetsForStyle(aOwner: TBaseLemmingStyle);
    var
      i, j: Integer;
      aSec: string;
      PropName, PropVal, aClassName: string;
      aClass: TClass;
      Inst: TBaseGraphicSet;
      List: TStringList;
    begin
      List := TStringList.Create;
      try
        i := 0;
        repeat
          aSec := aOwner.StyleName + '_' + i2s(i);
          if not Ini.SectionExists(aSec) then
            Break;
          aClassName := Ini.ReadString(aSec, 'GraphicSetClass', '');
          Inst := TBaseGraphicSetClass(FindClass(aClassName)).Create(aOwner);
          Ini.ReadSection(aSec, List);
          for j := 0 to List.Count - 1 do
          begin
            PropName := List[j];
            PropVal := Ini.ReadString(aSec, PropName, '');
            if (PropVal <> '') and IsPublishedProp(Inst, PropName) then
              SetPropValue(Inst, PropName, PropVal);
            //windlg([propname, propval]);
          end;
          aOwner.GraphicSetList.Add(Inst);
          Inc(i);
        until False;
      finally
        List.Free;
      end;
    end;

var
  StyleNo: Integer;
  aSec: string;
  Sty: TBaseLemmingStyle;
//  DoWarning: Boolean;

begin
//  DoWarning := False;
  Ini := TMemIniFile.Create(aFileName);
  try
    StyleNo := 0;
    repeat
      aSec := 'style_' + i2s(StyleNo);
      if not Ini.SectionExists(aSec) then
        Break;
      Sty := CreateStyleFromSection(aSec);
      if Sty.CommonPath <> '' then
        Sty.CommonPath := IncludeTrailingBackSlash(Sty.CommonPath);
      fStyleList.Add(Sty);
      CreateGraphicSetsForStyle(Sty);
      Inc(StyleNo);
    until False;
    SetGlobalIndicesAndVars;
  finally
    Ini.Free;
  end;
end;

// SORTING FUNCTIONS for next procedure
   function SortGraphicSets(P1, P2: Pointer): Integer;
    var
      G1, G2: TBaseGraphicSet;
      G1ID, G2ID: Integer;
    begin
      G1 := TBaseGraphicSet(P1);
      G2 := TBaseGraphicSet(P2);

      if G1.IsSpecial and (not G2.IsSpecial) then
      begin
        Result := 1;
        exit;
      end else if G2.IsSpecial and (not G1.IsSpecial) then
      begin
        Result := -1;
        exit;
      end;

      if G1.IsSpecial then
      begin
        G1ID := G1.GraphicSetIdExt;
        G2ID := G2.GraphicSetIdExt;
      end else begin
        G1ID := G1.GraphicSetId;
        G2ID := G2.GraphicSetId;
      end;

      if G1ID > G2ID then
      begin
        Result := 1;
        exit;
      end else if G2ID > G1ID then
      begin
        Result := -1;
        exit;
      end;

      if G1.GraphicSetOrderNumber > G2.GraphicSetOrderNumber then
      begin
        Result := 1;
        exit;
      end else if G2.GraphicSetOrderNumber > G1.GraphicSetOrderNumber then
      begin
        Result := -1;
        exit;
      end;

      Result := CompareStr(G1.GraphicSetName, G2.GraphicSetName);
    end;
// END SORTING FUNCTIONS

procedure TStyleMgr.LoadStyleInfo;
var
  MainINI, TempINI: TMemIniFile;
  StyleNo: Integer;
  EditorPath, aSec: String;
  Sty: TBaseLemmingStyle;

    function CreateStyleFromSection(const aSection: string): TBaseLemmingStyle;
    var
      List: TStringList;
      i: Integer;
      aClassName, PropName, PropVal: string;
    begin
      Result := nil;
      List := TStringList.Create;
      MainIni.ReadSection(aSection, List);
      try
        // read classname and create instance
        aClassName := MainIni.ReadString(aSection, 'StyleClass', '');
        Result := TBaseLemmingStyleClass(FindClass(aClassName)).Create;
        // fill published props
        for i := 0 to List.Count - 1 do
        begin
          PropName := List[i];
          PropVal := MainIni.ReadString(aSection, PropName, '');
          if (PropVal <> '') and IsPublishedProp(Result, PropName) then
            SetPropValue(Result, PropName, PropVal);
        end;

      finally
        List.Free;
      end;
    end;

    procedure ExtractPlaceholderSet;
    var
      R: TResourceStream;
    begin
      R := TResourceStream.Create(HINSTANCE, 'PLACEHOLDER', 'ARCHIVE');
      try
        R.SaveToFile(EditorPath + 'styles\placeholder.dat');
      finally
        R.Free;
      end;
    end;

    procedure CreateLemmixGraphicSets(aOwner: TBaseLemmingStyle);
    var
      SearchRec: TSearchRec;
      GraphicSet: TDosGraphicSet;
      GSNum: Integer;
      FnString: String;
      HasINI: Boolean;
      i: Integer;
      lw: LongWord;
      AlreadyDone: Boolean;
      TempFileStream: TFileStream;
    begin
      if FileExists(ExtractFilePath(ParamStr(0)) + Sty.CommonPath + 'styles.ini') then
        HasINI := true;

      if TCustLemmStyle(aOwner).NeoLemmix then
      begin
        if FindFirst(EditorPath + aOwner.CommonPath + 'x_*.dat', faAnyFile, SearchRec) = 0 then
        begin
          repeat
            FnString := ExtractFileName(SearchRec.Name);
            FnString := LeftStr(FnString, Length(FnString) - 4);
            FnString := RightStr(FnString, Length(FnString) - 2);

            // Yay, thanks to literally nothing but me being too lazy to convert all VGASPECs to NeoLemmix format
            // ages back, and again when introducing NXP, we pretty much have to bother detecting whether it's a
            // NeoLemmix-format or DOS-format VGASPEC. At least I figured out a quicker way than attempting to load
            // the file.
            TempFileStream := TFileStream.Create(EditorPath + aOwner.CommonPath + SearchRec.Name, fmOpenRead);
            TempFileStream.Position := $0006;
            TempFileStream.Read(lw, 4);
            // Endian difference...
            lw := ((lw and $FF000000) shr 24) +
                  ((lw and $00FF0000) shr 8) +
                  ((lw and $0000FF00) shl 8) +
                  ((lw and $000000FF) shl 24);
            if lw = TempFileStream.Size then
            begin
              GraphicSet := TDosGraphicSet.Create(aOwner);
            end else begin
              GraphicSet := TBaseNeoGraphicSet.Create(aOwner);
              TBaseNeoGraphicSet(GraphicSet).GraphicSetFile := {EditorPath + aOwner.CommonPath +} 'x_' + FnString + '.dat';
            end;
            TempFileStream.Free;

            GraphicSet.GraphicExtFile := {EditorPath + aOwner.CommonPath +} 'x_' + FnString + '.dat';
            GraphicSet.GraphicSetName := FnString;
            GraphicSet.GraphicSetInternalName := FnString;
            GraphicSet.GraphicSetIdExt := 255;
            GraphicSet.IsSpecial := true;
            GraphicSet.GraphicSetOrderNumber := -1;
            aOwner.GraphicSetList.Add(GraphicSet);
          until FindNext(SearchRec) <> 0;
          FindClose(SearchRec);
        end;
      end;

      //fuck

      // And any other DAT files - also NeoLemmix only
      if TCustLemmStyle(aOwner).NeoLemmix then
      begin

        if not FileExists(EditorPath + aOwner.CommonPath + 'placeholder.dat') then
          ExtractPlaceholderSet;

        if FindFirst(EditorPath + aOwner.CommonPath + '*.dat', faAnyFile, SearchRec) = 0 then
        begin
          repeat
            FnString := ExtractFileName(SearchRec.Name);
            if LeftStr(LowerCase(FnString), 2) = 'x_' then Continue;

            FnString := LeftStr(FnString, Length(FnString) - 4);

            GraphicSet := TBaseNeoGraphicSet.Create(aOwner);
            GraphicSet.MetaInfoFile := {EditorPath + aOwner.CommonPath +} FnString + '.dat';
            GraphicSet.GraphicFile := GraphicSet.MetaInfoFile;
            GraphicSet.GraphicExtFile := GraphicSet.MetaInfoFile;
            TBaseNeoGraphicSet(GraphicSet).GraphicSetFile := GraphicSet.MetaInfoFile;
            GraphicSet.GraphicSetName := FnString;
            GraphicSet.GraphicSetInternalName := FnString;
            GraphicSet.GraphicSetId := 255;
            GraphicSet.IsSpecial := false;
            GraphicSet.GraphicSetOrderNumber := -1;
            {try
              GraphicSet.DirectReadMetaData; //needs to be read to identify normal or special graphic set
              GraphicSet.DoReadData;
            except
              GraphicSet.Free;
              Continue;
            end;
            if TBaseNeoGraphicSet(GraphicSet).Invalid then
            begin
              GraphicSet.Free;
              Continue;
            end;
            if GraphicSet.IsSpecial then
            begin
              GraphicSet.GraphicSetInternalName := RightStr(GraphicSet.GraphicSetInternalName, Length(GraphicSet.GraphicSetInternalName) - 2);
              GraphicSet.GraphicSetName := GraphicSet.GraphicSetInternalName;

              AlreadyDone := false;
              for i := aOwner.GraphicSetList.Count-1 downto 0 do
                if (aOwner.GraphicSetList[i].GraphicSetInternalName = GraphicSet.GraphicSetInternalName) and (aOwner.GraphicSetList[i].IsSpecial) then
                begin
                  aOwner.GraphicSetList.Delete(i);
                  AlreadyDone := true;
                end;
              if not AlreadyDone then
              begin
                GraphicSet.Free;
                Continue;
              end;
            end;}

            aOwner.GraphicSetList.Add(GraphicSet);
          until FindNext(SearchRec) <> 0;
          FindClose(SearchRec);
        end;
      end;




      // Load data from the INI, if it exists
      if HasINI then
      begin
        TempINI := TMemIniFile.Create(EditorPath + Sty.CommonPath + 'styles.ini');
        for i := 0 to aOwner.GraphicSetList.Count-1 do
        begin
          GraphicSet := TDosGraphicSet(aOwner.GraphicSetList[i]);
          if not TempINI.SectionExists(GraphicSet.GraphicSetInternalName) then Continue;
          with GraphicSet do
          begin
            GraphicSetName := TempINI.ReadString(GraphicSetInternalName, 'Name', GraphicSetName);
            GraphicSetAltName := TempINI.ReadString(GraphicSetInternalName, 'AltName', '');
            if (GraphicSetID <> 255) and (GraphicSetIDExt <> 255) then GraphicSetInternalName := TempINI.ReadString(GraphicSetInternalName, 'InternalName', GraphicSetInternalName);
            if (GraphicSetID = 255) or (GraphicSetIDExt = 255) then GraphicSetOrderNumber := TempINI.ReadInteger(GraphicSetInternalName, 'Order', GraphicSetOrderNumber);
          end;
        end;
        TempINI.Free;
      end;

      aOwner.GraphicSetList.Sort(SortGraphicSets);
    end;

    function CreateStyle: TCustLemmStyle;
    var
      List: TStringList;
      i: Integer;
      PropName, PropVal: string;
    begin
      Result := nil;
      Result := TCustLemmStyle.Create;
      Result.CommonPath := 'styles/';
      Result.NeoLemmix := true;
      Result.StyleDescription := 'NeoLemmix';
      Result.StyleName := 'NeoLemmix';
      Result.TestEXE := 'NeoLemmix.exe';
    end;

begin
  EditorPath := ExtractFilePath(ParamStr(0));

  Sty := CreateStyle;
  CreateLemmixGraphicSets(Sty);
  fStyleList.Add(Sty);


end;


function TStyleMgr.FindStyleByName(const aStyleName: string): TBaseLemmingStyle;
var
  i: Integer;
  S: TBaseLemmingStyle;
begin
  for i := 0 to fStyleList.Count - 1 do
  begin
    Result := fStyleList.List^[i];
    if CompareText(LowerCase(Result.StyleName), LowerCase(aStyleName)) = 0 then
      Exit;
  end;
  Result := nil;
end;

function TStyleMgr.StyleByName(const aStyleName: string): TBaseLemmingStyle;
var
  i: Integer;
  S: TBaseLemmingStyle;
begin
  Result := FindStyleByName(aStyleName);
  if Result = nil then
  raise Exception.Create('Style not found (' + aStyleName + ')');
end;

function TStyleMgr.GraphByName(const aStyleName, aGraphName: string): TBaseGraphicSet;
begin
  Result := StyleByName(aStyleName).GraphByName(aGraphName);
end;


function TStyleMgr.GetStyle(aIndex: Integer): TBaseLemmingStyle;
begin
  Result := fStyleList[aIndex];
end;

function TStyleMgr.StyleIndex(aStyle: TBaseLemmingStyle): Integer;
begin
  for Result := 0 to fStyleList.Count - 1 do
    if aStyle = fStyleList[Result] then
      Exit;
  Result := -1;
end;

procedure TStyleMgr.SetGlobalIndicesAndVars;
var
  i: Integer;
  Sty: TBaseLemmingStyle;
begin
  for i := 0 to StyleList.Count - 1 do
  begin
    Sty := StyleList[i];
    if (Sty is TDosOrigStyle)        and (STYLE_DOSORIG        = -1) then STYLE_DOSORIG         := i else
    if (Sty is TDosOhNoStyle)        and (STYLE_DOSOHNO        = -1) then STYLE_DOSOHNO         := i else
    if (Sty is TDosChristmas91Style) and (STYLE_DOSCHRISTMAS91 = -1) then STYLE_DOSCHRISTMAS91  := i else
    if (Sty is TDosChristmas92Style) and (STYLE_DOSCHRISTMAS92 = -1) then STYLE_DOSCHRISTMAS92  := i else
    if (Sty is TDosHoliday93Style)   and (STYLE_DOSHOLIDAY93   = -1) then STYLE_DOSHOLIDAY93    := i else
    if (Sty is TDosHoliday94Style)   and (STYLE_DOSHOLIDAY94   = -1) then STYLE_DOSHOLIDAY94    := i else
    if (Sty is TLemminiStyle)        and (STYLE_LEMMINI        = -1) then STYLE_LEMMINI         := i else
    if (Sty is TCustLemmStyle)       and (STYLE_CUSTLEMM       = -1) then STYLE_CUSTLEMM        := i;
    end;


  if STYLE_DOSORIG >= 0 then DosOrigStyle                := fStyleList[STYLE_DOSORIG       ] as  TDosOrigStyle       ;
  if STYLE_DOSOHNO >= 0 then DosOhNoStyle                := fStyleList[STYLE_DOSOHNO       ] as  TDosOhNoStyle       ;
  if STYLE_DOSCHRISTMAS91 >= 0 then DosChristmas91Style  := fStyleList[STYLE_DOSCHRISTMAS91] as  TDosChristmas91Style;
  if STYLE_DOSCHRISTMAS92 >= 0 then DosChristmas92Style  := fStyleList[STYLE_DOSCHRISTMAS92] as  TDosChristmas92Style;
  if STYLE_DOSHOLIDAY93 >= 0 then DosHoliday93Style      := fStyleList[STYLE_DOSHOLIDAY93  ] as  TDosHoliday93Style  ;
  if STYLE_DOSHOLIDAY94 >= 0 then DosHoliday94Style      := fStyleList[STYLE_DOSHOLIDAY94  ] as  TDosHoliday94Style  ;
  if STYLE_LEMMINI >= 0 then LemminiStyle                := fStyleList[STYLE_LEMMINI       ] as  TLemminiStyle       ;
  if STYLE_CUSTLEMM >= 0 then CustLemmStyle              := fStyleList[STYLE_CUSTLEMM      ] as  TCustLemmStyle;
end;


{ TMetaObject }

procedure TMetaObject.Assign(Source: TPersistent);
var
  M: TMetaObject absolute Source;

    (*
    function Map(N: Integer): Integer;
    begin
{      if OwnerCollection.Owner is TBaseGraphicSet then
         if M.OwnerCollection.Owner is TBaseGraphicSet then
           Result := ConvertCoord(OwnerStyle.GetParams)}
    end; *)

begin
  if Source is TMetaObject then
  begin
    fAnimationFlags               := M.fAnimationFlags;
    fStartAnimationFrameIndex     := M.fStartAnimationFrameIndex;
    fEndAnimationFrameIndex       := M.fEndAnimationFrameIndex;
    fWidth                        := M.fWidth;
    fHeight                       := M.fHeight;
    fAnimationFrameDataSize       := M.fAnimationFrameDataSize;
    fMaskOffsetFromImage          := M.fMaskOffsetFromImage;
    fTriggerLeft                  := M.fTriggerLeft;
    fTriggerTop                   := M.fTriggerTop;
    fTriggerWidth                 := M.fTriggerWidth;
    fTriggerHeight                := M.fTriggerHeight;
    fTriggerEffect                := M.fTriggerEffect;
    fAnimationFramesBaseLoc       := M.fAnimationFramesBaseLoc;
    fPreviewFrameIndex            := M.fPreviewFrameIndex;
    fTrapSoundEffect              := M.fTrapSoundEffect;
  end
  else inherited Assign(Source);
end;


{ TMetaObjectCollection }

function TMetaObjectCollection.Add: TMetaObject;
begin
  Result := TMetaObject(inherited Add);
end;

constructor TMetaObjectCollection.Create(aOwner: TPersistent);
begin
  inherited Create(aOwner, TMetaObject);
end;

function TMetaObjectCollection.GetItem(Index: Integer): TMetaObject;
begin
  Result := TMetaObject(inherited GetItem(Index))
end;

function TMetaObjectCollection.Insert(Index: Integer): TMetaObject;
begin
  Result := TMetaObject(inherited Insert(Index))
end;

procedure TMetaObjectCollection.SetItem(Index: Integer; const Value: TMetaObject);
begin
  inherited SetItem(Index, Value);
end;

{ TMetaTerrain }

procedure TMetaTerrain.Assign(Source: TPersistent);
var
  T: TMetaTerrain absolute Source;
begin
  if Source is TMetaTerrain then
  begin
    fWidth := T.fWidth;
    fHeight := T.fHeight;
    fImageLocation := T.fImageLocation;
    fMaskLocation := T.fMaskLocation;
  end
  else inherited Assign(Source);
end;

{ TMetaTerrainCollection }

function TMetaTerrainCollection.Add: TMetaTerrain;
begin 
  Result := TMetaTerrain(inherited Add); 
end; 

constructor TMetaTerrainCollection.Create(aOwner: TPersistent); 
begin 
  inherited Create(aOwner, TMetaTerrain);
end;

function TMetaTerrainCollection.GetItem(Index: Integer): TMetaTerrain;
begin
  Result := TMetaTerrain(inherited GetItem(Index))
end;

function TMetaTerrainCollection.Insert(Index: Integer): TMetaTerrain;
begin
  Result := TMetaTerrain(inherited Insert(Index))
end;

procedure TMetaTerrainCollection.SetItem(Index: Integer; const Value: TMetaTerrain);
begin
  inherited SetItem(Index, Value);
end;

{ TBaseGraphicSet }

procedure TBaseGraphicSet.DirectReadMetaData;
begin
  raise Exception.Create(ClassName + '.DirectReadMetaData abstract');
end;

procedure TBaseGraphicSet.DirectReadData;
{-------------------------------------------------------------------------------
  Force cached read objects and bitmaps.
  This will be slow for most graphicsets.
-------------------------------------------------------------------------------}
var
  i: Integer;
begin
  EnsureMetaData;
  with MetaObjectCollection, HackedList do
    for i := 0 to Count - 1 do
    begin
      GetObjectBitmapRef(i);
    end;
  with MetaTerrainCollection, HackedList do
    for i := 0 to Count - 1 do
    begin
      GetTerrainBitmapRef(i);
    end;
end;


function TBaseGraphicSet.DoReadMetaData: Boolean;
{-------------------------------------------------------------------------------
  This method reads the metadata.
  If the metadata is archived then we get it from there. Otherwise it is
  loaded with DirectReadMetaData.
-------------------------------------------------------------------------------}
begin

  Result := True;
  if fForceDirectAccess or (fGraphicSetArchive = '') or not FileExists(CompilePath + fGraphicSetArchive) then
  begin
(*    if (OwnerStyle.CommonPath = '') then
      raise Exception.Create('Path for graphicset is empty');
//      or not DirectoryExists(CommonPath) then *)
    DirectReadMetaData;
  end
  else
    GetArchivedMetaData;
end;

function TBaseGraphicSet.DoReadData: Boolean;
begin
  Result := False;
end;

constructor TBaseGraphicSet.Create(aOwnerStyle: TBaseLemmingStyle);
begin
  inherited Create;
  fOwnerStyle := aOwnerStyle;
  fIniProps := TStringList.Create;
//  fParams := TStringList.Create;
  fMetaObjectCollection := TMetaObjectCollection.Create(Self);
  fMetaTerrainCollection := TMetaTerrainCollection.Create(Self);
  fObjectBitmapCache := TObjectList.Create;
  fTerrainBitmapCache := TObjectList.Create;
  fNil := '';
end;

destructor TBaseGraphicSet.Destroy;
begin
  //fParams.Free;
  fMetaObjectCollection.Free;
  fMetaTerrainCollection.Free;
  fObjectBitmapCache.Free;
  fTerrainBitmapCache.Free;
  fBackGroundBitmapCache.Free;
  fIniProps.Free;
  inherited;
end;

procedure TBaseGraphicSet.DirectGetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  This is the ultimate deepest method to obtain a object bitmap from a style.
  It has to be overridden. The bitmap has to contain all animations.
-------------------------------------------------------------------------------}
begin
  raise Exception.Create(ClassName + '.DirectGetObjectBitmap is abstract');
end;

procedure TBaseGraphicSet.DirectGetObjectMaskBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  This is the key method to obtain a object bitmap mask from a graphic set.
  It has to be overridden.
-------------------------------------------------------------------------------}
begin

end;

procedure TBaseGraphicSet.DirectGetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  This is the key method to obtain a object bitmap from a graphic set.
  It has to be overridden.
-------------------------------------------------------------------------------}
begin
  raise Exception.Create(ClassName + '.DirectGetTerrainBitmap is abstract');
end;

procedure TBaseGraphicSet.DirectGetBackGroundBitmap(aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  if the graphicset has a background then it must be overridden. That is:
  when level.statics.graphicsetex is set.
-------------------------------------------------------------------------------}
begin
  raise Exception.Create(ClassName + '.DirectGetBackGroundBitmap is abstract');
end;

{procedure TBaseGraphicSet.SetParams(const Value: TStrings);
begin
  fParams.Assign(Value);
end;}

procedure TBaseGraphicSet.EnsureMetaData;
begin
  if not fActivated then
    fActivated := DoReadMetaData;
end;

procedure TBaseGraphicSet.EnsureData;
begin
  //
end;


procedure TBaseGraphicSet.GetArchivedTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  Extract bitmap from Archive into a memory stream.
  Then read aBitmap from the stream.
-------------------------------------------------------------------------------}
var
  Arc: TArchive;
  Mem: TMemoryStream;
  Fn: string;
begin
  Mem := nil;
  Arc := nil;

  Fn := STerrainPrefix + LeadZeroStr(aIndex, 3) + '.bmp';
  Mem := TMemoryStream.Create;
  try
    Arc := TArchive.Create;
    try
      Arc.OpenArchive(CompilePath + fGraphicSetArchive, amOpen);
      //deblist(arc.archivelist);
      Arc.ExtractFile(Fn, Mem);
    finally
      Arc.Free;
    end;
    Mem.Position := 0;
    aBitmap.LoadFromStream(Mem);
//    ReplaceColor(aBitmap, clBlack32, 0);
  finally
    Mem.Free;
  end;
end;

procedure TBaseGraphicSet.GetArchivedBackGroundBitmap(aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  Extract bitmap from Archive into a memory stream.
  Then read aBitmap from the stream.
-------------------------------------------------------------------------------}
var
  Arc: TArchive;
  Mem: TMemoryStream;
  Fn: string;
begin
  Mem := nil;
  Arc := nil;

  Fn := 'back.bmp';
  Mem := TMemoryStream.Create;
  try
    Arc := TArchive.Create;
    try
      Arc.OpenArchive(CompilePath + fGraphicSetArchive, amOpen);
      //deblist(arc.archivelist);
      Arc.ExtractFile(Fn, Mem);
    finally
      Arc.Free;
    end;
    Mem.Position := 0;
    aBitmap.LoadFromStream(Mem); //ReplaceColor(aBitmap, clBlack32, 0);
  finally
    Mem.Free;
  end;
end;


procedure TBaseGraphicSet.GetArchivedObjectBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  Extract bitmap from Archive into a memory stream.
  Then read aBitmap from the stream.
-------------------------------------------------------------------------------}
var
  Arc: TArchive;
  Mem: TMemoryStream;
  Fn: string;
begin
  Mem := nil;
  Arc := nil;

  Fn := SObjectPrefix + LeadZeroStr(aIndex, 3) + '.bmp';
  Mem := TMemoryStream.Create;
  try
    Arc := TArchive.Create;
    try
      Arc.OpenArchive(CompilePath + fGraphicSetArchive, amOpen);
      Arc.ExtractFile(Fn, Mem);
    finally
      Arc.Free;
    end;
    Mem.Position := 0;
    aBitmap.LoadFromStream(Mem);
    //ReplaceColor(aBitmap, clBlack32, 0);
  finally
    Mem.Free;
  end;
end;


procedure TBaseGraphicSet.GetArchivedMetaData;
{-------------------------------------------------------------------------------
  Load graph.bin file from the archive. Then read the contents from the stream
  with the standard delphi object streaming mechanism.
-------------------------------------------------------------------------------}
var
  Arc: TArchive;
  Mem: TMemoryStream;
  Fn: string;
begin
  Mem := nil;
  Arc := nil;

  Fn := 'graph.bin';
  Mem := TMemoryStream.Create;
  try
    Arc := TArchive.Create;
    try                                                  
      Arc.OpenArchive(CompilePath + fGraphicSetArchive, amOpen);
      Arc.ExtractFile(Fn, Mem);
    finally
      Arc.Free;
    end;
    Mem.Position := 0;
    LoadFromStream(Mem);
  finally
    Mem.Free;
  end;
end;


function TBaseGraphicSet.GetObjectBitmapRef(aIndex: Integer): TBitmap32;
{-------------------------------------------------------------------------------
  Returns a reference to the cached object bitmap (all animations in one).
  If the cached bitmap is unassigned then the bitmap is loaded by a call to
  DirectGetObjectBitmap.
  If the index is invalid, a errorbitmap is returned.
-------------------------------------------------------------------------------}
var
  Cnt: Integer;
begin
  EnsureMetaData;
  Cnt := fMetaObjectCollection.HackedList.Count;

  // invalid index
  if (aIndex < 0) or (aIndex >= Cnt) then
  begin
    Result := PicMgr.fErrorBitmap;
    Exit;
  end;

  with fObjectBitmapCache do
  begin
    if Count = 0 then
      Count := Cnt;
    Result := List^[aIndex];
    if Result = nil then
    begin
      Result := TBitmap32.Create;
      List^[aIndex] := Result;
      if fForceDirectAccess or (fGraphicSetArchive = '') then
        DirectGetObjectBitmap(aIndex, Result)
      else
        GetArchivedObjectBitmap(aIndex, Result);
    end;
  end;
end;

function TBaseGraphicSet.GetTerrainBitmapRef(aIndex: Integer): TBitmap32;
{-------------------------------------------------------------------------------
  Returns a reference to the cached terrain bitmap.
  There are a few possibilities:
  1. The bitmap is cached, then we return this
  2. The bitmap is unassigned:
     a. We either get the bitmap from the compiled archive (if there is one)
     b. Or load it by a call to DirectGetTerrainBitmap.
  3. If the index is invalid, a errorbitmap is returned.
-------------------------------------------------------------------------------}
var
  Cnt: Integer;
  T: TMetaTerrain;
begin
  EnsureMetaData;
  Cnt := fMetaTerrainCollection.HackedList.Count;

  if (aIndex < 0) or (aIndex >= Cnt) then
  begin
    Result := PicMgr.fErrorBitmap;
    Exit;
  end;

  with fTerrainBitmapCache do
  begin
    if Count = 0 then
      Count := Cnt;
    Result := List^[aIndex];
    if Result = nil then
    begin
      Result := TBitmap32.Create;
      { TODO : if error then keep stable! }
      List^[aIndex] := Result;
      if fForceDirectAccess or (fGraphicSetArchive = '') then
        DirectGetTerrainBitmap(aIndex, Result)
      else
        GetArchivedTerrainBitmap(aIndex, Result)
    end;
  end;

//  T := fMetaTerrainCollection.HackedList.List^[aIndex];
 // if (T.Width = 0) then
  //begin

 // end;
end;

function TBaseGraphicSet.GetBackGroundBitmapRef: TBitmap32;
begin
  EnsureMetaData;
  if fBackGroundBitmapCache = nil then
  begin
    fBackGroundBitmapCache := TBitmap32.Create;
    Result := fBackGroundBitmapCache;
    if fForceDirectAccess or (fGraphicSetArchive = '') then
      DirectGetBackGroundBitmap(Result)
    else
      GetArchivedBackGroundBitmap(Result);
  end;
  Result := fBackGroundBitmapCache;
end;

procedure TBaseGraphicSet.GetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32);
begin
  aBitmap.Assign(GetTerrainBitmapRef(aIndex));
end;

procedure TBaseGraphicSet.GetBackGroundBitmap(aBitmap: TBitmap32);
begin
  aBitmap.Assign(GetBackGroundBitmapRef);
end;


procedure TBaseGraphicSet.GetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32);
begin
  aBitmap.Assign(GetObjectBitmapRef(aIndex));
end;

procedure TBaseGraphicSet.GetObjectFrame(aIndex, aFrame: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  Copy a frame to aBitmap.
  o First get the bitmap with all animations
  o Then calculate the rectangle for the frame with help from metadata
  o Then copy the rectangle
-------------------------------------------------------------------------------}
var
  B: TBitmap32;
  X, Y, W, H: Integer;
  Src, Dst: TRect;
begin
  B := GetObjectBitmapRef(aIndex);
  with MetaObjectCollection[aIndex] do
  begin
    H := Height;
    W := Width;
  end;
  X := 0;
  Y := H * aFrame;
  Src := Rect(X, Y, X + W, Y + H);
  Dst := ZeroTopLeftRect(Src);
  aBitmap.SetSize(W, H);
  B.DrawTo(aBitmap, Dst, Src);
end;

procedure TBaseGraphicSet.SetMetaObjectCollection(const Value: TMetaObjectCollection);
begin
  fMetaObjectCollection.Assign(Value);
end;

procedure TBaseGraphicSet.SetMetaTerrainCollection(const Value: TMetaTerrainCollection);
begin
  fMetaTerrainCollection.Assign(Value);
end;

procedure TBaseGraphicSet.SaveToFile(const aFileName: string);
var
  Wrapper: TBaseGraphicSetWrapper;
begin
  Wrapper := TBaseGraphicSetWrapper.Create(nil);
  try
    Wrapper.Graph := Self;
    Wrapper.SaveToFile(aFileName);
  finally
    Wrapper.Free;
  end;
end;

procedure TBaseGraphicSet.SaveToStream(S: TStream);
var
  Wrapper: TBaseGraphicSetWrapper;
begin
  Wrapper := TBaseGraphicSetWrapper.Create(nil);
  try
    Wrapper.Graph := Self;
    Wrapper.SaveToStream(S);
  finally
    Wrapper.Free;
  end;
end;

procedure TBaseGraphicSet.SaveToTxtFile(const aFileName: string);
var
  Wrapper: TBaseGraphicSetWrapper;
begin
  Wrapper := TBaseGraphicSetWrapper.Create(nil);
  try
    Wrapper.Graph := Self;
    Wrapper.SaveToTxtFile(aFileName);
  finally
    Wrapper.Free;
  end;
end;

procedure TBaseGraphicSet.LoadFromStream(S: TStream);
var
  Wrapper: TBaseGraphicSetWrapper;
  OldGS: string;
begin
  Wrapper := TBaseGraphicSetWrapper.Create(nil);
  try
    OldGS := fGraphicSetArchive;
    Wrapper.Graph := Self;
    Wrapper.LoadFromStream(S);
    fGraphicSetArchive := OldGS;
  finally
    Wrapper.Free;
  end;
end;

procedure TBaseGraphicSet.LoadFromFile(const aFileName: string);
var
  Wrapper: TBaseGraphicSetWrapper;
begin
  Wrapper := TBaseGraphicSetWrapper.Create(nil);
  try
    Wrapper.Graph := Self;
    Wrapper.LoadFromFile(aFileName);
  finally
    Wrapper.Free;
  end;
end;

function TBaseGraphicSet.GetInternalName: string;
begin
  if fGraphicSetIntName <> '' then
    Result := fGraphicSetIntName
    else
    Result := LowerCase(fGraphicSetName);
end;

function TBaseGraphicSet.IncludeCommonStylePath(const S: string): string;
{-------------------------------------------------------------------------------
  Adds a filename to the commonpath of
-------------------------------------------------------------------------------}
begin
  {if ExtractFileDrive(S) = '' then
  begin
    Assert(fOwnerStyle <> nil, 'error ownerstyle');
    Result := fOwnerStyle.CommonPath + S;
  end
  else
    Result := S;}
  Result := ExtractFilePath(ParamStr(0)) + fOwnerStyle.fCommonPath + S;
end;

function TBaseGraphicSet.GetCompleteFileName(const aFileName: string): string;
var
  Drv: string;
begin
  Result := aFileName;
  Drv := ExtractFileDrive(Result);
  if fOwnerStyle = nil then
  begin
    if Drv = '' then
      Result := ExpandFileName(aFileName)
  end
  else begin
    Result := IncludeTrailingBackslash(fOwnerStyle.CommonPath) + Result;
  end;
end;

function TBaseGraphicSet.GetFullArchiveName: string;
{ TODO : make more flexi with include function }
begin
  Result := CompilePath + fGraphicSetArchive;
end;


{ TBaseGraphicSetList }

function TBaseGraphicSetList.Add(Item: TBaseGraphicSet): Integer;
begin
  Result := inherited Add(Item);
end;

function TBaseGraphicSetList.FindByName(const aName: string): TBaseGraphicSet;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    Result := Items[i];
    if CompareText(Result.GraphicSetName, aName) = 0 then
      Exit;
  end;
  Result := nil;
  raise Exception.Create('unknown graphicset (' + aName + ')');
end;

function TBaseGraphicSetList.GetItem(Index: Integer): TBaseGraphicSet;
begin
  Result := inherited Get(Index);
end;

procedure TBaseGraphicSetList.Insert(Index: Integer; Item: TBaseGraphicSet);
begin
  inherited Insert(Index, Item);
end;

{ TMetaAnimations }

function TMetaAnimations.Add: TMetaAnimation;
begin
  Result := TMetaAnimation(inherited Add);
end;

constructor TMetaAnimations.Create(aOwner: TPersistent);
begin
  inherited Create(aOwner, TMetaAnimation);
end;

function TMetaAnimations.GetItem(Index: Integer): TMetaAnimation;
begin
  Result := TMetaAnimation(inherited GetItem(Index))
end;

function TMetaAnimations.Insert(Index: Integer): TMetaAnimation;
begin
  Result := TMetaAnimation(inherited Insert(Index))
end;

procedure TMetaAnimations.SetItem(Index: Integer; const Value: TMetaAnimation);
begin
  inherited SetItem(Index, Value);
end;

{ TBaseAnimationSet }

constructor TBaseAnimationSet.Create(aOwnerStyle: TBaseLemmingStyle);
begin
  inherited Create;
  fOwnerStyle := aOwnerStyle;
  fMetaAnimations := TMetaAnimations.Create(Self);
  fAnimationCache := TObjectList.Create;
//fMaskCache := TObjectList.Create;
  fCountDownDigits := TBitmap32.Create;
  fExplosionMask := TBitmap32.Create;
  fBashMasks           := TBitmap32.Create;
  fBashMasksRTL       := TBitmap32.Create;
  fMineMasks          := TBitmap32.Create;
  fMineMasksRTL        := TBitmap32.Create;
end;

destructor TBaseAnimationSet.Destroy;
begin
  fMetaAnimations.Free;
  fAnimationCache.Free;
//fMaskCache.Free;
  fCountDownDigits.Free;
  fExplosionMask.Free;
  fBashMasks.Free;
  fBashMasksRTL.Free;
  fMineMasks.Free;
  fMineMasksRTL.Free;
  inherited;
end;

procedure TBaseAnimationSet.EnsureMetaData;
begin
  if not fMetaDataActivated then
  begin
    InternalReadMetaData;
    fMetaDataActivated := True;
  end;
end;

function TBaseAnimationSet.GetAnimationBitmapRef(Index: Integer): TBitmap32;
begin
{ TODO : opti }
  Result := TBitmap32(fAnimationCache[Index]);
//  Result := fAnimationCache.List^[Index];
end;

function TBaseAnimationSet.GetMaskBitmapRef(Index: Integer): TBitmap32;
begin
{ TODO : opti }
  Result := TBitmap32(fAnimationCache[Index]);
end;

procedure TBaseAnimationSet.GetAnimationFrame(aIndex, aFrame: Integer; aBitmap: TBitmap32);
begin
  aBitmap.Width := fMetaAnimations[aIndex].Width;
//  aBitmap.Height :=
  //fAnimationCache[aIndex]
end;

procedure TBaseAnimationSet.ReadData;
var
  i: Integer;
begin
  InternalReadData;

  for i := 0 to fAnimationCache.Count - 1 do
  begin
//    ReplaceColor(TBitmap32(fAnimationCache[i]), clBRICK32, fBrickColor);
  end;

end;

procedure TBaseAnimationSet.ClearData;
begin
  InternalClearData;
end;


{procedure TBaseAnimationSet.SetBrickColor(aColor: TColor32);
var
  i: Integer;
begin
  if fBrickColor = aColor then
    Exit;


  for i := 0 to fAnimationCache.Count - 1 do
    ReplaceColor(TBitmap32(fAnimationCache[i]), fBrickColor, aColor);

  fBrickColor := aColor;

end;}

{ TBaseLevelLoader }

constructor TBaseLevelLoader.Create(aOwner: TPersistent);
begin
  inherited Create(aOwner);
  fLevelPacks := TLevelPacks.Create(Self);
end;

destructor TBaseLevelLoader.Destroy;
begin
  fLevelPacks.Free;
  inherited;
end;


function TBaseLevelLoader.GetLevelPackCount: Integer;
begin
  Result := fLevelPacks.Count
end;

procedure TBaseLevelLoader.LoadLevel(aInfo: TLevelInfo; aLevel: TLevel);
begin
  InternalLoadLevel(aInfo, aLevel);
end;

procedure TBaseLevelLoader.LoadSingleLevel(aPack, aSection, aLevelIndex: Integer; aLevel: TLevel);
begin
  InternalLoadSingleLevel(aPack, aSection, aLevelIndex, aLevel);
end;

procedure TBaseLevelLoader.Prepare;
begin
  if not fPrepared then
  begin
    InternalPrepare;
    fPrepared := True;
  end;
end;


{ TStyleCache }

procedure TStyleCache.Clear;
begin
  GraphicSetId := -1;
  TerrainBitmaps.Clear;
  ObjectBitmaps.Clear;
end;

constructor TStyleCache.Create;
begin
  inherited Create;
  GraphicSetId := -1;
  TerrainBitmaps := TObjectList.Create;
  ObjectBitmaps := TObjectList.Create;
end;

destructor TStyleCache.Destroy;
begin
  TerrainBitmaps.Free;
  ObjectBitmaps.Free;
  inherited;
end;

{ TStyleCacheList }

function TStyleCacheList.Add(Item: TStyleCache): Integer;
begin
  Result := inherited Add(Item);
end;

function TStyleCacheList.GetItem(Index: Integer): TStyleCache;
begin
  Result := inherited Get(Index);
end;

procedure TStyleCacheList.Insert(Index: Integer; Item: TStyleCache);
begin
  inherited Insert(Index, Item);
end;

{ TPictureMgr }

procedure TPictureMgr.CacheGraphicSet(aCache: TStyleCache; aGraphicSet: Integer);
{-------------------------------------------------------------------------------
  Lowest level routine for extracting all bitmaps from a style archive and
  caching it.
  Only the bitmaps with the appropriate resolution are extracted.
-------------------------------------------------------------------------------}
var
  i: Integer;
  StyleName, ArcName, BitmapName, Id, A: string;
  P, S, F: string;
  ArcItem: TArchiveObject;
  Bmp: TBitmap32;
  Archive: TArchive;
  TerCount, ObjCount: Integer;
  TerList, ObjList: TIntegerList;
  Mem: TMemoryStream;
  t: cardinal;
  RC: Char;
begin
(*  StyleName := SStyleIds[aGraphicSet];
  RC := SResolutionChars[fResolution]; // change this into seperate archives
  ArcName := AppPath + 'styles\' + StyleName + SArcStyleExt;
  BitmapName := StyleName + RC + '_%s' + '.bmp';
  TerCount := 0;
  ObjCount := 0;

  Mem := TMemoryStream.Create;
  TerList := TIntegerList.Create;
  ObjList := TIntegerList.Create;
  Archive := TArchive.Create;
  try
    Archive.OpenArchive(ArcName, amOpen);
    // first index the terrains and objecs looping through the archived files
    for i := 0 to Archive.ArchiveList.Count - 1 do
    begin
      A := Archive.ArchiveList[i];
      if FastPosNoCase(A, RC + '_', Length(A), 2, 1) > 0 then
      begin
        TerList.Add(i); // Log(['is terrain', a])
      end
      else if FastPosNoCase(A, RC + 'o_', Length(A), 3, 1) > 0 then
        if FastPosNoCase(A, '000.bmp', Length(A), 7, 1) > 0 then
        begin
         ObjList.Add(i); // Log(['is firrstobject', a])
        end;
    end;

    // now create the cache bitmaps
    for i := 0 to TerList.Count - 1 do
      aCache.TerrainBitmaps.Add(TBitmap32.Create);
    for i := 0 to ObjList.Count - 1 do
      aCache.ObjectBitmaps.Add(TBitmap32.Create);

    // now read the terrain bitmaps from the archive
    for i := 0 to TerList.Count - 1 do
    begin
      Bmp := aCache.TerrainBitmaps.List^[i];
      Mem.Clear;
      Archive.ExtractFile(TerList[i], Mem);
      Mem.Position := 0;
      Bmp.LoadFromStream(Mem); //      Log([archive.archivelist[terlist[i]]]);
//      SkinTest(Bmp);
    end;

    // now read the object bitmaps from the archive
    for i := 0 to ObjList.Count - 1 do
    begin
      Bmp := aCache.ObjectBitmaps.List^[i];
      Mem.Clear;
      Archive.ExtractFile(ObjList[i], Mem);
      Mem.Position := 0;
      Bmp.LoadFromStream(Mem); //Log([archive.archivelist[objlist[i]]]);
//      SkinTest(Bmp);
    end;


  finally
    Archive.Free;
    TerList.Free;
    ObjList.Free;
    Mem.Free;
  end;


  //t:=gettickcount-t;
//  windlg([t]);
//  t:=gettickcount;
*)

(*
//-----------------------------------------------------
    exit;
//-----------------------------------------------------
    hier zit nog nuttige code voor filenames............

  if StyleCache.GraphicSetId = aGraphicSet then
    Exit;

  StyleCache.Clear;
  StyleCache.GraphicSetId := aGraphicSet;

  P := AppPath + 'convert\styles\';
  StyleName := SStyleIds[aGraphicSet];
  S := P + StyleName + '\' + StyleName + SLowResChar + '_%s' + '.bmp';

  i := 0;
  repeat
    Id := LeadZeroStr(i, 3);
    F := Format(S, [Id]);
    if not FileExists(F) then
      Break;
    //else
      //Log([F]);
    Bmp := TBitmap32.Create;
    Bmp.LoadFromFile(F);
    StyleCache.TerrainBitmaps.Add(Bmp);
//                        Log([i]);
    Inc(i);
  until False;

  //t:=gettickcount-t;
//  windlg([t])
  *)
end;

constructor TPictureMgr.Create;
var
  i: Integer;
begin
  inherited;

  fErrorBitmap := TBitmap32.Create;
  fErrorBitmap.Width := 40;
  fErrorBitmap.Height := 40;
  fErrorBitmap.Clear;
  fErrorBitmap.Line(0, 0, fErrorBitmap.Width - 1, fErrorBitmap.Height - 1, clWhite32);
  fErrorBitmap.Line(0, fErrorBitmap.Width - 1, 0, fErrorBitmap.Height - 1, clWhite32);

  // create stylecache for every graphic set
  for i := Low(TGraphicSetRange) to High(TGraphicSetRange) do
    StyleCacheList[i] := TStyleCache.Create;

  for i := 0 to 3 do
    ExtendedGraphicsCache[i] := TBitmap32.Create;
  // create sys animations
//  WalkerAnimation := TLemmingAnimation.Create(8, 16, 10);
//  WalkerAnimationR := TLemmingAnimation.Create(8, 16, 10);
//  FallerAnimation := TLemmingAnimation.Create(8, 8, 10);
  // load them
//  LoadGraphics;
end;

destructor TPictureMgr.Destroy;
var
  i: Integer;
begin
  UnloadGraphics;
//  WalkerAnimation.Free;
//  WalkerAnimationR.Free;
//  FallerAnimation.Free;
  for i := Low(TGraphicSetRange) to High(TGraphicSetRange) do
    StyleCacheList[i].Free;
  for i := 0 to 3 do
    ExtendedGraphicsCache[i].Free;

  fErrorBitmap.Free;
  inherited;
end;

procedure TPictureMgr.LoadGraphics;
var
  i, Ani: Integer;
  B: TBitmap32;
  S: string;
  Path: string;
const
//  SWalker = 'Sprites\Walker_%s_8x10.bmp';
  SWalker = 'sys\lems_000_%s.bmp';

begin
  Path := ApplicationPath;
  (*

  // sprite
  B := TBitmap32.Create;

  for i := 0 to 7 do
  begin
    S := LeadZeroStr(i, 3);
    B.LoadFromFile(Format(SWalker, [S]));
    WalkerAnimation.SetFrameBitmap(i, B);
//    Log(['walker loaded', i]);
  end;

  for i := 8 to 15 do
  begin
    S := LeadZeroStr(i, 3);
    B.LoadFromFile(Format(SWalker, [S]));
    WalkerAnimationR.SetFrameBitmap(i-8, B);
  //  Log(['walker reverse loaded', i]);
  end;

  Ani := 0;
  for i := 16 to 23 do
  begin
    S := LeadZeroStr(i, 3);
    B.LoadFromFile(Format(SWalker, [S]));
    FallerAnimation.SetFrameBitmap(Ani, B);
    Inc(Ani);
  end;

  B.Free;
  WalkerAnimation.Bitmap.SaveToFile('d:\anitest.bmp');
  WalkerAnimationR.Bitmap.SaveToFile('d:\anitestr.bmp');
  FallerAnimation.Bitmap.SaveToFile('d:\fallerani.bmp');
  *)

  // palettes
  FEPalette := TLemmingPalette.Create;
  Level0Palette := TLemmingPalette.Create;
  Level1Palette := TLemmingPalette.Create;
  Level2Palette := TLemmingPalette.Create;
  Level3Palette := TLemmingPalette.Create;
  Level4Palette := TLemmingPalette.Create;
  RockPalette := TLemmingPalette.Create;
  BrickPalette := TLemmingPalette.Create;
  BubblePalette := TLemmingPalette.Create;
  SnowPalette := TLemmingPalette.Create;

  {$ifdef develop}
  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\FE.PAL');
    FEPalette.Colors := Copy(fColors);
    FEPalette.ColorCount := Length(FEPalette.Colors);
    Free;
  end;

  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\LEVEL0.PAL');
    Level0Palette.Colors := Copy(fColors);
    Level0Palette.ColorCount := Length(Level0Palette.Colors);
    Free;
  end;

  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\LEVEL1.PAL');
    Level1Palette.Colors := Copy(fColors);
    Level1Palette.ColorCount := Length(Level1Palette.Colors);
    Free;
  end;

  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\LEVEL2.PAL');
    Level2Palette.Colors := Copy(fColors);
    Level2Palette.ColorCount := Length(Level2Palette.Colors);
    Free;
  end;

  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\LEVEL3.PAL');
    Level3Palette.Colors := Copy(fColors);
    Level3Palette.ColorCount := Length(Level3Palette.Colors);
    Free;
  end;

  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\LEVEL4.PAL');
    Level4Palette.Colors := Copy(fColors);
    Level4Palette.ColorCount := Length(Level4Palette.Colors);
    Free;
  end;

  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\ROCK.PAL');
    RockPalette.Colors := Copy(fColors);
    RockPalette.ColorCount := Length(RockPalette.Colors);
    Free;
  end;

  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\BRICK.PAL');
    BrickPalette.Colors := Copy(fColors);
    BrickPalette.ColorCount := Length(BrickPalette.Colors);
    Free;
  end;

  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\BUBBLE.PAL');
    BubblePalette.Colors := Copy(fColors);
    BubblePalette.ColorCount := Length(BubblePalette.Colors);
    Free;
  end;

  with TPALFile.Create do
  begin
    LoadFromFile(Path + 'WinPal\Snow.PAL');
    SnowPalette.Colors := Copy(fColors);
    SnowPalette.ColorCount := Length(SnowPalette.Colors);
    Free;
  end;
  {$endif}


end;

procedure TPictureMgr.UnLoadGraphics;
var
  i: Integer;
begin
(*  for i := 0 to 7 do
    Walker[i].Free; *)
  FEPalette.Free;
  Level0Palette.Free;
  Level1Palette.Free;
  Level2Palette.Free;
  Level3Palette.Free;
  Level4Palette.Free;
  RockPalette.Free;
  BrickPalette.Free;
  BubblePalette.Free;
  SnowPalette.Free;
end;

function TPictureMgr.GetCacheForGraphicSet(aSet: Integer): TStyleCache;
begin
  if aSet in TheGraphicSets then
    Result := StyleCacheList[aSet]
  else raise Exception.Create('getcache for graphics set error')
end;

function TPictureMgr.ForceCacheForGraphicSet(aGraphicSet: Integer): TStyleCache;
begin
  Result := GetCacheForGraphicSet(aGraphicSet);
  if not Result.IsCached then
  begin
    CacheGraphicSet(Result, aGraphicSet);
    Result.IsCached := True;
  end;
end;

procedure TPictureMgr.GetObjectBitmap32(aGraphicSet, aObjectId: Integer; Bmp: TBitmap32);
begin
  Bmp.Assign(ObjectBitmaps[aGraphicSet, aObjectId]);
end;

procedure TPictureMgr.GetTerrainBitmap32(aGraphicSet, aTerrainId: Integer; Bmp: TBitmap32);
begin
  Bmp.Assign(TerrainBitmaps[aGraphicSet, aTerrainId]);
end;

function TPictureMgr.GetTerrainBitmap32Ref(aGraphicSet, aTerrainId: Integer): TBitmap32;
{-------------------------------------------------------------------------------
  This is a "unsafe" getting of the bitmap, so we assert this during development.
-------------------------------------------------------------------------------}
var
  Cache: TStyleCache;
begin
  Cache := ForceCacheForGraphicSet(aGraphicSet);
//  Assert((aTerrainId >= 0) and (aTerrainId < Cache.TerrainBitmaps.Count), 'GetTerrainBitMapRef Id error ' + i2s(aTerrainId));
  if (aTerrainId < 0) or (aTerrainId > Cache.TerrainBitmaps.Count - 1) then
  begin
    Result := fErrorBitmap
  end
  else begin
    Result := Cache.TerrainBitmaps.List^[aTerrainId];
  end;
end;

function TPictureMgr.GetTerrainBitmapCount(aGraphicSet: Integer): Integer;
begin
  Result := ForceCacheForGraphicSet(aGraphicSet).TerrainBitmaps.Count;
end;

function TPictureMgr.GetObjectBitmap32Ref(aGraphicSet, aObjectId: Integer): TBitmap32;
var
  Cache: TStyleCache;
begin
  Cache := ForceCacheForGraphicSet(aGraphicSet);
//  Assert((aObjectId >= 0) and (aObjectId < Cache.ObjectBitmaps.Count), 'GetObjectBitMapRef Id error ' + i2s(aObjectId));
  if (aObjectId < 0) or (aObjectId > Cache.ObjectBitmaps.Count - 1) then
  begin
    Result := fErrorBitmap;
  end
  else begin
    Result := Cache.ObjectBitmaps.List^[aObjectId];
  end;
end;

function TPictureMgr.GetObjectBitmapCount(aGraphicSet: Integer): Integer;
begin
  Result := ForceCacheForGraphicSet(aGraphicSet).ObjectBitmaps.Count;
end;

function TPictureMgr.TerrainIsSteel(aGraphicSet, aTerrainId: Integer): Boolean;
begin
  Result := False;//
end;

procedure TPictureMgr.SetResolution(const Value: TResolution);
var
  i: Integer;
begin
  if fResolution = Value then Exit;
  fResolution := Value;
  for i := Low(TGraphicSetRange) to High(TGraphicSetRange) do
    StyleCacheList[i].IsCached := False;
end;

procedure TPictureMgr.SkinTest(aBitmap: TBitmap32);
var
  P: PColor32;
  C: TColor32;
  I: Integer;
begin
  with aBitmap do
  begin
    P := PixelPtr[0, 0];
    for I := 0 to Width * Height - 1 do
    begin
      C := P^ and $00FFFFFF; // get RGB without alpha

//      if C = TrColor then // is this pixel "transparent"?
  //      P^ := C; // write RGB with "transparent" alpha back into the SrcBitmap

//      P^ := P^ and not $000000FF;
//      P^ := P^ and not $0000FF00;
      P^ := P^ and not $00FF0000;

      Inc(P); // proceed to the next pixel
    end;
  end;
end;

function TPictureMgr.GetExtendedGraphicsBitmap32Ref(aExtendedGraphicSet: Integer): TBitmap32;
var
  Arc: TArchive;
  M: TMemoryStream;
i,  Ix: Integer;
  Fn: string;
begin
  Result := ExtendedGraphicsCache[aExtendedGraphicSet - 1];
  if Result.Width = 0 then
  begin
    M := TMemoryStream.Create;
    Arc := TArchive.Create;
    try
      Arc.OpenArchive(ApplicationPath + 'styles\special.lsf', amOpen);
      Fn := 'specs_' + LeadZeroStr(aExtendedGraphicSet, 3) + '.bmp';
//                                      windlg(fn);
  //    for i := 0 to arc.archivelist.count -1 do deb([arc.archivelist[i]]);
      Ix := Arc.ArchiveList.IndexOf(Fn);
      if Ix < 0 then raise exception.create('spec error');
      M.Clear;
      Arc.ExtractFile(Ix, M);
      M.Position := 0;
      Result.LoadFromStream(M);
    finally
      Arc.Free;
      M.Free;
    end;
  end;
end;

{ TLevelRenderer }

procedure TLevelRenderer.DrawInteractiveObject(aLevel: TLevel; aObject: TInteractiveObject; aFrame: Integer = 0);
{-------------------------------------------------------------------------------
  Add interactive object to levelbitmap.
  o Get the bitmap
  o Reset the drawmode of the tempbitmap (it's changed by Assign)
  o Get the right pixelcombine event
  o Invert if flag says so
  o Draw
-------------------------------------------------------------------------------}
var
  MO: TMetaObject;
  RemoveTerrainDrawFlag: Boolean;
begin
  with aObject do
  begin
    if aObject.ItemActive = false then Exit;
    //PictureMgr.GetObjectBitmap32(aLevel.Statics.GraphicSet, ObjectID, TempBitmap);
//    TempBitmap.Assign(aObject.Bitmap);
    MO := aObject.Graph.fMetaObjectCollection[aObject.ObjectID];
    case MO.fTriggerEffect of
       13: if odfFaceLeft in ObjectDrawingFlags then aFrame := 1;
       14: aFrame := (fObjectSValue + (fObjectLValue * 16)) + 1;
       15: aFrame := 1;
       17: aFrame := 1;
       21: if odfFaceLeft in ObjectDrawingFlags then aFrame := 1;
       end;
    if (MO.TriggerEffect in [7, 8, 19]) and TCustLemmStyle(aLevel.Style).NeoLemmix then
    begin
      RemoveTerrainDrawFlag := not (odfOnlyShowOnTerrain in ObjectDrawingFlags);
      ObjectDrawingFlags := ObjectDrawingFlags + [odfOnlyShowOnTerrain];
    end else
      RemoveTerrainDrawFlag := false;
    if aFrame >= MO.fEndAnimationFrameIndex then
    begin
      aFrame := MO.fEndAnimationFrameIndex - 1;
      if (MO.TriggerEffect = 14) and (aFrame > 16) then
        aFrame := 16; // fencer secrecy
    end;
    if aFrame < 0 then aFrame := 0;

    aObject.GetFrameBitmap(aFrame, TempBitmap);
    TempBitmap.DrawMode := dmCustom;
    TempBitmap.OnPixelCombine := GetObjectCombineEvent(ObjectDrawingFlags);

    if MO.fTriggerEffect in [7, 8, 19] then
      if litTerrain in fRenderTypes then
        TempBitmap.OnPixelCombine := CombineObjectOneWayArrow;

    if odfRotate in ObjectDrawingFlags then
      TempBitmap.Rotate90;
    if odfInvert in ObjectDrawingFlags then
      TempBitmap.FlipVert;
    if odfFlip in ObjectDrawingFlags then
      TempBitmap.FlipHorz;
    TempBitmap.DrawTo(fInternalLevelBitmap, ObjectX, ObjectY);

    if RemoveTerrainDrawFlag then ObjectDrawingFlags := ObjectDrawingFlags - [odfOnlyShowOnTerrain];

  end;
end;

procedure TLevelRenderer.DrawTerrain(aTerrain: TTerrain; OWI: Boolean = false);
{-------------------------------------------------------------------------------
  Add terrain to levelbitmap.
  o Get the bitmap
  o Reset the drawmode of the tempbitmap (it's changed by Assign)
  o get the right pixelcombine event
  o invert if flag says so
  o Draw
-------------------------------------------------------------------------------}
begin
  with aTerrain do
  if aTerrain.ItemActive then
  begin
    //PictureMgr.GetTerrainBitmap32(aLevel.Statics.GraphicSet, TerrainID, TempBitmap);
    TempBitmap.Assign(aTerrain.Bitmap);
    TempBitmap.DrawMode := dmCustom;
    if ((aTerrain.Graph.MetaTerrainCollection[aTerrain.TerrainID].Unknown and 1) <> 0)
    and ((OwnerLevel.Statics.AutoSteelOptions and 2) <> 0) then
    begin
      if tdfNoOneWay in TerrainDrawingFlags then
        OWI := false
      else
        OWI := true;
    end;
    TempBitmap.OnPixelCombine := GetTerrainCombineEvent(TerrainDrawingFlags, OWI);
//    if not assigned(tempbitmap.OnPixelCombine) then
  //    tempbitmap.drawmode:=dmtransparent;
    if tdfRotate in TerrainDrawingFlags then
      TempBitmap.Rotate90;
    if tdfInvert in TerrainDrawingFlags then
      TempBitmap.FlipVert;
    if tdfFlip in TerrainDrawingFlags then
      TempBitmap.FlipHorz;
    TempBitmap.DrawTo(fInternalLevelBitmap, TerrainX, TerrainY);
  end;
end;

procedure TLevelRenderer.DrawSteel(aLevel: TLevel; aSteel: TSteel);
{-------------------------------------------------------------------------------
  Draw steel under or on top of the material. If on top the transparent.
-------------------------------------------------------------------------------}
var
  SrcRect, DstRect: TRect;
  IsNegSteel: Integer;
begin
  IsNegSteel := 0;
  with aSteel do
  if aSteel.ItemActive then
  begin
    if TCustLemmStyle(aLevel.Style).NeoLemmix or TLemminiStyle(aLevel.Style).SuperLemmini then
      IsNegSteel := aSteel.SteelType
      else
      IsNegSteel := 0;
    ResizeSteelBitmap(SteelWidth, SteelHeight);
 //   TempBitmap.OnPixelCombine := GetSteelCombineEvent();
    if roSteelNoOverwrite in fRenderOptions then
    begin
      SteelBitmap.DrawMode := dmCustom;
      if IsNegSteel = 1 then
        SteelBitmap.OnPixelCombine := CombineNegSteelNoOverwrite
        else
        SteelBitmap.OnPixelCombine := CombineSteelNoOverwrite;
//      SteelBitmap.MasterAlpha := 255;
    end
    else begin
      SteelBitmap.DrawMode := dmCustom;
      if IsNegSteel = 1 then
        SteelBitmap.OnPixelCombine := CombineNegSteelOverwrite
        else
        SteelBitmap.OnPixelCombine := CombineSteelOverwrite;
//      SteelBitmap.MasterAlpha := 255;
      //SteelBitmap.DrawMode := dmBlend;
      //SteelBitmap.MasterAlpha := 64;
    end;
    SrcRect := Rect(0, 0, SteelWidth, SteelHeight);
    DstRect := SrcRect;
    RectMove(DstRect, SteelX, SteelY);
//    TempBitmap.DrawTo(fInternalLevelBitmap, SteelX, SteelY);
    SteelBitmap.DrawTo(fInternalLevelBitmap, DstRect, SrcRect);
  end;
end;

procedure TLevelRenderer.DrawTriggerArea(O: TInteractiveObject; MO: TMetaObject);
var
  SrcRect, DstRect: TRect;
  X, Y, W, H: Integer;
begin

  if not (odfRotate in O.ObjectDrawingFlags) then
  begin

    if odfFlip in O.ObjectDrawingFlags then
      X := O.ObjectX + (MO.fWidth-1) - MO.TriggerLeft - (MO.TriggerWidth-1)
    else
      X := O.ObjectX + MO.TriggerLeft;

    if odfInvert in O.ObjectDrawingFlags then
    begin
      Y := O.ObjectY + (MO.fHeight-1) - MO.TriggerTop - (MO.TriggerHeight-1);
      if not (MO.TriggerEffect in [7, 8, 9, 19]) then Y := Y + 10;
    end else
      Y := O.ObjectY + MO.TriggerTop;

    W := MO.TriggerWidth;
    H := MO.TriggerHeight;

  end else begin

    if odfFlip in O.ObjectDrawingFlags then
      X := O.ObjectX + MO.TriggerTop
    else
      X := O.ObjectX + (MO.Height - MO.TriggerTop - MO.TriggerHeight);

    if odfInvert in O.ObjectDrawingFlags then
    begin
      Y := O.ObjectY + MO.fWidth - MO.TriggerLeft - MO.TriggerWidth;
      //if not (MO.TriggerEffect in [7, 8, 9, 19]) then Y := Y + 10;  // no idea why this needs to be excluded, but it works.
    end else
      Y := O.ObjectY + MO.TriggerLeft;

    if not (MO.TriggerEffect in [7, 8, 9, 19]) then
    begin
      X := X + 4;
      Y := Y + 5;
    end;

    W := MO.TriggerHeight;
    H := MO.TriggerWidth;

  end;

  ResizeTriggerBitmap(W, H);

  SrcRect := Rect(0, 0, W, H);
  DstRect := Rect(X, Y, X + W, Y + H);
  Triggerbitmap.OnPixelCombine := CombineTriggerArea;
  TriggerBitmap.DrawTo(fInternalLevelBitmap, DstRect, SrcRect);
end;

procedure TLevelRenderer.RenderLevel(aLevel: TLevel);
{-------------------------------------------------------------------------------
  Build bitmap from ground up, using the RenderOptions property.
  o The collection items are direct accessed by the HackedList.List for speed.
    As a bonus the Count property is hacked too.
-------------------------------------------------------------------------------}
var
  i: Integer;
  T: TTerrain;
  O: TInteractiveObject;
  S: TSteel;
  OldDrawMode: TDrawMode;
  OldPixelCombine: TPixelCombineEvent;
  LevWidth, LevHeight: Integer;
  Rec: TDefaultsRec;
  MO: TMetaObject;
  PrevIx: Integer;
  TempBmp: TBitmap32;
//ftest

    function MustRender(B: TBaseLevelItem): Boolean;
    begin
      Result := True;
      if Assigned(fOnRenderItem) then
        fOnRenderItem(Self, B, Result);
    end;

    procedure DrawBackground;
    var
      i: Integer;
      FoundBgIndex: Integer;
      x, y: Integer;
      BMP: TBitmap32;
    begin
      FoundBgIndex := 0;
      for i := 0 to aLevel.Graph.MetaObjectCollection.Count do
        if i = aLevel.Graph.MetaObjectCollection.Count then
          Exit
        else if aLevel.Graph.MetaObjectCollection[i].TriggerEffect = 32 then
        begin
          if FoundBgIndex = aLevel.Statics.BackgroundIndex then
          begin
            BMP := aLevel.Graph.ObjectBitmaps[i];
            Break;
          end else
            Inc(FoundBgIndex);
        end;
      y := 0;
      repeat
        x := 0;
        repeat
          BMP.DrawTo(TempBMP, x, y);
          x := x + BMP.Width;
        until x >= TempBMP.Width;
        y := y + BMP.Height;
      until y >= TempBMP.Height;
    end;


begin

{  if fLevelBitmap = nil
  then fInternalLevelBitmap := MainBitmap
  else }

  if fLevelBitmap=nil then exit;

  fInternalLevelBitmap := fLevelBitmap;

  fInternalLevelBitmap.BeginUpdate;

  OldDrawMode := fInternalLevelBitmap.DrawMode;
  OldPixelCombine := fInternalLevelBitmap.OnPixelCombine;
  TempBMP := TBitmap32.Create;
  try

    if (aLevel = nil) or (aLevel.Style = nil) or (aLevel.Graph = nil) then
    begin
      fInternalLevelBitmap.Clear(0);
      fInternalLevelBitmap.SetSize(0, 0);
      Exit;
    end;

    aLevel.Style.GetStyleParams(Rec);

    if ((aLevel.Style is TLemminiStyle) and (TLemminiStyle(aLevel.Style).SuperLemmini))
    or ((aLevel.Style is TCustLemmStyle) and (TCustLemmStyle(aLevel.Style).NeoLemmix)) then
    begin
      LevWidth := aLevel.Statics.Width;
      LevHeight := aLevel.Statics.Height;
    end else begin
      LevWidth := Rec.LevelWidth;
      LevHeight := Rec.LevelHeight;
    end;


    fInternalLevelBitmap.SetSize(LevWidth, LevHeight);
    fInternalLevelBitmap.Clear(0);
    TempBMP.SetSize(LevWidth, LevHeight);
    TempBMP.Clear(aLevel.Graph.BackgroundColor or $FF000000);


    (*
    if roCollisionGridBuilding in RenderOptions then
    begin
      CollisionGrid.Width := LevWidth;
      CollisionGrid.Height := LevHeight;
      CollisionGrid.Clear(0);
    end;*)

    with aLevel do
    begin

      // Background is first of all
      if litBackground in fRenderTypes then
        DrawBackground;

      // first we draw the terrain
      if litTerrain in fRenderTypes then
        with TerrainCollection, HackedList do
          for i := 0 to Count - 1 do
          begin
            T := List^[i];
            DrawTerrain({aLevel, }T, (aLevel.Statics.AutoSteelOptions and $80 <> 0));
          end;

      if roCollisionGridBuilding in RenderOptions then
        CollisionGrid.ReadFrom(fInternalLevelBitmap, ctUniformRGB);

      // then the objects (1. drawn on terrain. 2. the rest)
      if litObject in fRenderTypes then
      begin
        with InteractiveObjectCollection, HackedList do
        begin
          for i := 0 to Count - 1 do
          begin
            O := List^[i];
            MO := O.Graph.MetaObjectCollection[O.ObjectID];
            if (odfOnlyShowOnTerrain in O.ObjectDrawingFlags) or (MO.TriggerEffect in [7, 8, 19]) then
            begin
              DrawInteractiveObject(aLevel, O, MO.PreviewFrameIndex);
            end;
          end;
          for i := 0 to Count - 1 do
          begin
            O := List^[i];
            MO := O.Graph.MetaObjectCollection[O.ObjectID];
            if not ((odfOnlyShowOnTerrain in O.ObjectDrawingFlags) or (MO.TriggerEffect in [7, 8, 19])) then
              DrawInteractiveObject(aLevel, O, MO.PreviewFrameIndex);
          end;
        end;
      end;

      // then the steel (only for editing of course)
      if litSteel in fRenderTypes then
        for i := 0 to SteelCollection.Count - 1 do
        begin
          S := SteelCollection[i];
          DrawSteel(aLevel, S);
        end;

      // then finally triggerarea's    lemgame
      if roShowTriggerAreas in fRenderOptions then
      begin

        with InteractiveObjectCollection, HackedList do
          for i := 0 to Count - 1 do
          begin
            {if i > 15 then
              Break;}
            O := List^[i];
            MO := O.Graph.MetaObjectCollection[O.ObjectID];
            if (MO.TriggerEffect <> 0) and O.ItemActive then
              DrawTriggerArea(O, MO);
          end;
      end;

      fInternalLevelBitmap.DrawMode := dmCustom;
      fInternalLevelBitmap.OnPixelCombine := CombineTerrainOverwrite;  // it works :P
      fInternalLevelBitmap.DrawTo(TempBMP);

      fInternalLevelBitmap.Assign(TempBMP);
    end;

  finally
    fInternalLevelBitmap.DrawMode := OldDrawMode;
    fInternalLevelBitmap.OnPixelCombine := OldPixelCombine;
    fInternalLevelBitmap.EndUpdate;
    fInternalLevelBitmap.Changed;
    TempBMP.Free;
  end;

//  if Assigned(fAfterBuild) then
  //  fAfterBuild(Self);
end;

procedure TLevelRenderer.RenderWorld(aLevel: TLevel);
var
  x, y, i: Integer;
  T: TTerrain;
  O: TInteractiveObject;
  S: TSteel;
  LevWidth, LevHeight: Integer;
  Rec: TDefaultsRec;
  MO: TMetaObject;
  r:trect;
//ftest

begin
  fInternalLevelBitmap := fLevelBitmap;
  fInternalLevelBitmap.BeginUpdate;
  try

    aLevel.Style.GetStyleParams(Rec);

    if ((aLevel.Style is TLemminiStyle) and (TLemminiStyle(aLevel.Style).SuperLemmini))
    or ((aLevel.Style is TCustLemmStyle) and (TCustLemmStyle(aLevel.Style).NeoLemmix)) then
    begin
      LevWidth := aLevel.Statics.Width;
      LevHeight := aLevel.Statics.Height;
    end else begin
      LevWidth := Rec.LevelWidth;
      LevHeight := Rec.LevelHeight;
    end;

    fInternalLevelBitmap.SetSize(LevWidth, LevHeight);
    fInternalLevelBitmap.Clear(0);

    with aLevel do
    begin

      // first we draw the terrain
      if litTerrain in fRenderTypes then
        with TerrainCollection, HackedList do
          for i := 0 to Count - 1 do
          begin
            T := List^[i];
            DrawTerrain({aLevel, }T, (aLevel.Statics.AutoSteelOptions and $80 <> 0));
          end;

      if roCollisionGridBuilding in RenderOptions then
      begin
        CollisionGrid.ReadFrom(fInternalLevelBitmap, ctUniformRGB);
        with CollisionGrid do
        for y := 0 to Height - 1 do
          for x := 0 to Width - 1 do
            Value[x, y] := Byte(Boolean(Value[x, y] > 0));
      end;

      // then the objects (1. drawn on terrain. 2. the rest)
      if litObject in fRenderTypes then
      begin

        with InteractiveObjectCollection, HackedList do
        begin
          for i := 0 to Count - 1 do
          begin
            O := List^[i];
            MO := O.Graph.MetaObjectCollection[O.ObjectID];
            if odfOnlyShowOnTerrain in O.ObjectDrawingFlags then
              if MO.EndAnimationFrameIndex <= 1 then
//              if mo.AnimationFlags = amx_
                  DrawInteractiveObject(aLevel, O);
          end;

          for i := 0 to Count - 1 do
          begin
            O := List^[i];
            MO := O.Graph.MetaObjectCollection[O.ObjectID];
            if not (odfOnlyShowOnTerrain in O.ObjectDrawingFlags) then
            begin
//              if O.ObjectID = OID_ENTRY then
  //              DrawInteractiveObject(aLevel, O, MO.StartAnimationFrameIndex)
//              else
              if (MO.EndAnimationFrameIndex <= 1) then
//              or (MO.AnimationFlags = AFX_TRIGGERED) then
                DrawInteractiveObject(aLevel, O);

(*
//               test paint triggerarea
              if mo.TriggerEffect = 1 then
              BEGIN
              with o, mo do
              begin
                R.Left := ObjectX + TriggerLeft;
                R.Top := ObjectY + TriggerTop;
                R.Right := R.Left + TriggerWidth;
                R.Bottom := R.Top + TriggerHeight;
              end;
                fInternalLevelBitmap.FrameRectS(R, clred32);
                //fInternalLevelBitmap.FillRectS(R, clred32);
              END;
*)

            end;
          end;
        end;

      end;

    end;

  finally
    fInternalLevelBitmap.EndUpdate;
    fInternalLevelBitmap.Changed;
  end;
end;

procedure TLevelRenderer.RenderToFile(aLevel: TLevel; const aFileName: String);
var
  OutBitmap: TPngObject;
  X, Y: Integer;
  C: TColor32;
  R, G, B: Byte;
  {Rec: TDefaultsRec;}
  LevWidth, LevHeight: Integer;
begin
  RenderLevel(aLevel);
  {aLevel.Style.GetStyleParams(Rec);
  LevWidth := Rec.LevelWidth;
  LevHeight := Rec.LevelHeight;
  if (aLevel.Style is TLemminiStyle) and TLemminiStyle(aLevel.Style).SuperLemmini then LevWidth := aLevel.Statics.Width;}

  LevWidth := fInternalLevelBitmap.Width;
  LevHeight := fInternalLevelBitmap.Height;

  OutBitmap := TPngObject.CreateBlank(COLOR_RGB, 8, LevWidth, LevHeight);
  for Y := 0 to (LevHeight - 1) do
    for X := 0 to (LevWidth - 1) do
    begin
      if fInternalLevelBitmap.Pixel[X, Y] and $FF000000 <> 0 then
      begin
        C := fInternalLevelBitmap.Pixel[X, Y];
        //MessageDlg(IntToHex(C, 8), mtCustom, [mbok], 0);
        R := C shr 16;
        G := C shr 8;
        B := C;
        C := (B shl 16) + (G shl 8) + (R) + ($FF000000);
        OutBitmap.Pixels[X, Y] := C;
      end;
    end;

  OutBitmap.CompressionLevel := 9;

  if RightStr(aFileName, 4) <> '.png' then
    OutBitmap.SaveToFile(aFileName + '.png')
    else
    OutBitmap.SaveToFile(aFileName);
  OutBitmap.free;
end;

procedure TLevelRenderer.RenderAnimatedObject(aLevel: TLevel; aObject: TInteractiveObject; aFrame: Integer; aTarget: TBitmap32 = nil);
var
  R: TRect;
begin
  with aObject do
  begin
    if aTarget <> nil then
      fInternalLevelBitmap := aTarget;
    try

    aObject.GetFrameBitmap(aFrame, TempBitmap);
    TempBitmap.DrawMode := dmCustom;
    TempBitmap.OnPixelCombine := GetObjectCombineEvent(ObjectDrawingFlags);
    if odfRotate in ObjectDrawingFlags then
      TempBitmap.Rotate90;
    if odfInvert in ObjectDrawingFlags then
      TempBitmap.FlipVert;
    if odfFlip in ObjectDrawingFlags then
      TempBitmap.FlipHorz;
    TempBitmap.DrawTo(fInternalLevelBitmap, ObjectX, ObjectY)

    finally
      if aTarget <> nil then
        fInternalLevelBitmap := fLevelBitmap;
    end;

  end;
end;

procedure TLevelRenderer.RenderAnimatedObjectDst(aLevel: TLevel;
  aObject: TInteractiveObject; aFrame: Integer; aTarget: TBitmap32;
  const aRect: TRect);
var
  R: TRect;
begin
  with aObject do
  begin
    if aTarget <> nil then
      fInternalLevelBitmap := aTarget;
    try

    aObject.GetFrameBitmap(aFrame, TempBitmap);
    TempBitmap.DrawMode := dmCustom;
    TempBitmap.OnPixelCombine := GetObjectCombineEvent(ObjectDrawingFlags);
    if odfRotate in ObjectDrawingFlags then
      TempBitmap.Rotate90;
    if odfInvert in ObjectDrawingFlags then
      TempBitmap.FlipVert;
    if odfFlip in ObjectDrawingFlags then
      TempBitmap.FlipHorz;
    TempBitmap.DrawTo(fInternalLevelBitmap, aRect, TempBitmap.BoundsRect)

    finally
      if aTarget <> nil then
        fInternalLevelBitmap := fLevelBitmap;
    end;

  end;
end;


{procedure TLevelRenderer.GetDraggerBitmap(aLevel: TLevel; Bmp: TBitmap; aList: TList);
var
  i: Integer;
  T: TTerrain;
  O: TInteractiveObject;
  S: TSteel;
  OldDrawMode: TDrawMode;
  OldPixelCombine: TPixelCombineEvent;
  LevWidth, LevHeight: Integer;
  LocalBitmap, OldBitmap: TBitmap32;
  Rec: TDefaultsRec;

      procedure GetArea;
      var
        R: TRect;
        i: Integer;
        First: Boolean;
        O: TObject;
        B: TBaseLevelItem;
      begin
        R := EmptyRect;
        First := True;
        for i := 0 to aList.Count - 1 do
        begin
          O := aList.Items[i];
          if O is TBaseLevelItem then
          begin
            B := TBaseLevelItem(O);
            if First then
            begin
              First := False;
              R := B.Bounds;
            end
            else begin
              R.Left := Min(R.Left, B.XPosition);
              R.Top := Min(R.Top, B.YPosition);
              R.Right := Max(R.Right, B.XPosition + B.ItemWidth);
              R.Bottom := Max(R.Bottom, B.YPosition + B.ItemHeight);
            end;
          end;
        end;
      end;

begin
  if aLevel = nil then
    Exit;
  if aList = nil then
    Exit;
  if aList.Count = 0 then
    Exit;

  LocalBitmap := TBitmap32.Create;
  OldBitmap := fInternalLevelBitmap;
  fInternalLevelBitmap := LocalBitmap;

  OldDrawMode := fInternalLevelBitmap.DrawMode;
  OldPixelCombine := fInternalLevelBitmap.OnPixelCombine;

  windlg('whats this');
  try
    aLevel.Style.GetStyleParams(Rec);
    if ((aLevel.Style is TLemminiStyle) and (TLemminiStyle(aLevel.Style).SuperLemmini))
    or ((aLevel.Style is TCustLemmStyle) and (TCustLemmStyle(aLevel.Style).NeoLemmix)) then
    begin
      LevWidth := aLevel.Statics.Width;
      LevHeight := aLevel.Statics.Height;
    end else begin
      LevWidth := Rec.LevelWidth;
      LevHeight := Rec.LevelHeight;
    end;
//    LevWidth := Defaults[fResolution].LevelWidth;
//    LevHeight := Defaults[fResolution].LevelHeight;

    fInternalLevelBitmap.SetSize(LevWidth, LevHeight);
    fInternalLevelBitmap.Clear(0);


//    Log(['render']);
    with aLevel do
    begin
      // first we draw the terrain
      if litTerrain in fRenderTypes then
        for i := 0 to TerrainCollection.Count - 1 do
        begin
          T := TerrainCollection[i];//.List.List^[i];
          DrawTerrain(T);
        end;
      (*
      // then the collision grid: it only maps terrain and steel
      if roCollisionGridBuilding in RenderOptions then
      begin
        fInternalLevelBitmap.Drawmode := dmCustom;
        fInternalLevelBitmap.OnPixelCombine := CombineGridAndTerrain;
        fInternalLevelBitmap.DrawTo(Grid, 0, 0);
      end;
      *)
      // then the objects
      if litObject in fRenderTypes then
        for i := 0 to InteractiveObjectCollection.Count - 1 do
        begin
          O := InteractiveObjectCollection[i];
          DrawInteractiveObject(aLevel, O);
        end;
      // and finally the steel (only for editing off course)
      if litSteel in fRenderTypes then
        for i := 0 to SteelCollection.Count - 1 do
        begin
          S := SteelCollection[i];
          DrawSteel(aLevel, S);
        end;

    end;

  finally
    fInternalLevelBitmap.DrawMode := OldDrawMode;
    fInternalLevelBitmap.OnPixelCombine := OldPixelCombine;
  end;

end;}


constructor TLevelRenderer.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  EraseColor := 0{clBlack32};
//  EraseColor := Color32(0, 0, 255);
//  MainBitmap := TBitmap32.Create;
//  MainBitmap.OuterColor := CL_OUTER;
//  Grid := TBitmap32.Create;
  SteelBitmap := TBitmap32.Create;
  SteelBitmap.DrawMode := dmBlend;

  TriggerBitmap := TBitmap32.Create;
  TriggerBitmap.DrawMode := dmCustom;//dmBlend;

  TempBitmap := TBitmap32.Create;
  TempBitmap.DrawMode := dmBlend;

  fRenderOptions := [];
  //[lsoTerrain, lsoObjects, lsoInitialScreenPosRect]
  fRenderTypes := [litTerrain, litObject, litBackground];
(*    roObjects,
    roSteelRects,
    roCollisionGridBuilding]; *)
end;

destructor TLevelRenderer.Destroy;
begin
//  MainBitmap.Free;
  //Grid.Free;
  SteelBitmap.Free;
  TriggerBitmap.Free;
  TempBitmap.Free;
  inherited;
end;

procedure TLevelRenderer.ResizeSteelBitmap(aWidth, aHeight: Integer);
begin
  with SteelBitmap do
  begin
    if SetSize(Max(aWidth, Width), Max(aHeight, Height)) then
      Clear(clBlue32);
  end;
end;

procedure TLevelRenderer.ResizeTriggerBitmap(aWidth, aHeight: Integer);
begin
  with TriggerBitmap do
  begin
    if SetSize(Max(aWidth, Width), Max(aHeight, Height)) then
      Clear(clFuchsia32);
  end;
end;

procedure TLevelRenderer.CombineTerrainErase(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Erase existing terrain with foreground
-------------------------------------------------------------------------------}
begin
  if F <> 0 then B := 0;//EraseColor; GRAPHICEX gr32
//  if F <> 0 then B := clred32;//EraseColor; GRAPHICEX gr32
//  if (F and COLORMASK <> 0) and (B and COLORMASK <> 0) then B := EraseColor;
end;

procedure TLevelRenderer.CombineTerrainNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Do not overwrite existing terrain with foreground.
-------------------------------------------------------------------------------}
begin
  if (F <> 0) and (B = 0) then B := F and $FFFFFF or $01000000;
//  if (F and COLORMASK <> 0) and (B and COLORMASK = 0) then B := F
end;

procedure TLevelRenderer.CombineTerrainOverwrite(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Just plot the foreground pixels that are not transparant
-------------------------------------------------------------------------------}
begin
  if F <> 0 then B := F and $FFFFFF or $01000000; // if F and COLORMASK <> 0 then B := F;
end;

procedure TLevelRenderer.CombineTerrainNoOverwriteOWW(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Do not overwrite existing terrain with foreground.
-------------------------------------------------------------------------------}
begin
  if (F <> 0) and (B = 0) then B := F and $FFFFFF or $09000000;
//  if (F and COLORMASK <> 0) and (B and COLORMASK = 0) then B := F
end;

procedure TLevelRenderer.CombineTerrainOverwriteOWW(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Just plot the foreground pixels that are not transparant
-------------------------------------------------------------------------------}
begin
  if F <> 0 then B := F and $FFFFFF or $09000000; // if F and COLORMASK <> 0 then B := F;
end;

procedure TLevelRenderer.CombineTerrainGhostedOWW(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Specialized pixelcombine. This is for SuperLemmini invisible pieces.
-------------------------------------------------------------------------------}
var
  cr, cg, cb: Integer;
begin
  if F <> 0 then
  begin
    cr := ((F shr 16) mod 256) * 3;
    cg := ((F shr 8) mod 256) * 3;
    cb := (F mod 256) * 3;
    cr := (cr + (((B shr 16) mod 256) * 5)) div 8;
    cg := (cg + (((B shr 8) mod 256) * 5)) div 8;
    cb := (cb + ((B mod 256) * 5)) div 8;
    B := (cr shl 16) + (cg shl 8) + cb;
    B := B or $09000000;
  end;
  //if F and COLORMASK <> 0 then B := F;
end;

procedure TLevelRenderer.CombineTerrainEraseNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  this is a fake one: erase but no overwrite (?)
  Shit just happens. We use the nooverwrite flag and NOT hte erase flag
-------------------------------------------------------------------------------}
begin
  if (F <> 0) and (B = 0) then B := F and $FFFFFF;
end;

procedure TLevelRenderer.CombineTerrainGhosted(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Specialized pixelcombine. This is for SuperLemmini invisible pieces.
-------------------------------------------------------------------------------}
var
  cr, cg, cb: Integer;
begin
  if F <> 0 then
  begin
    cr := ((F shr 16) mod 256) * 3;
    cg := ((F shr 8) mod 256) * 3;
    cb := (F mod 256) * 3;
    cr := (cr + (((B shr 16) mod 256) * 5)) div 8;
    cg := (cg + (((B shr 8) mod 256) * 5)) div 8;
    cb := (cb + ((B mod 256) * 5)) div 8;
    B := (cr shl 16) + (cg shl 8) + cb;
  end;
  //if F and COLORMASK <> 0 then B := F;
end;

function TLevelRenderer.GetTerrainCombineEvent(aFlags: TTerrainDrawingFlags; OWI: Boolean = false): TPixelCombineEvent;
var
  OneWayFlag: Boolean;
begin
  Result := nil;
  OneWayFlag := (tdfNoOneWay in aFlags) xor OWI;
  if (tdfInvisible in aFlags) or (tdfFake in aFlags) then
  begin
    if OneWayFlag then
      Result := CombineTerrainGhosted
      else
      Result := CombineTerrainGhostedOWW;
  end else if [tdfErase, tdfNoOverwrite] * aFlags = [tdfErase, tdfNoOverwrite] then
  begin
    if OneWayFlag then     // Acts like only No Overwrite is set, so treat as such, no need for dedicated routines
      Result := CombineTerrainNoOverwrite
      else
      Result := CombineTerrainNoOverwriteOWW;
  end else if tdfErase in aFlags then
    Result := CombineTerrainErase
  else if tdfNoOverwrite in aFlags then
  begin
    if OneWayFlag then
      Result := CombineTerrainNoOverwrite
      else
      Result := CombineTerrainNoOverwriteOWW;
  end else
  begin
    if OneWayFlag then
      Result := CombineTerrainOverwrite
      else
      Result := CombineTerrainOverwriteOWW; // #EL can be nil and dmTransparent
  end;
end;

procedure TLevelRenderer.CombineObjectNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Specialized pixelcombine. do not overwrite existing terrain with foreground
-------------------------------------------------------------------------------}
begin
  if (F <> 0) and (B = 0) then B := F
  //if (F and COLORMASK <> 0) and (B and COLORMASK = 0) then B := F
end;

procedure TLevelRenderer.CombineObjectOnlyOnTerrain(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Specialized pixelcombine. only display on terrain.
-------------------------------------------------------------------------------}
begin
  if (F <> 0) and (B and $01000000 <> 0) then B := F;
  //if (F and COLORMASK <> 0) and (B and COLORMASK <> 0) then B := F;
end;

procedure TLevelRenderer.CombineObjectOneWayArrow(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Specialized pixelcombine. only display on one way terrain.
-------------------------------------------------------------------------------}
begin
  if (F <> 0) and (B and $08000000 <> 0) then B := F;
  //if (F and COLORMASK <> 0) and (B and COLORMASK <> 0) then B := F;
end;

procedure TLevelRenderer.CombineObjectOverwrite(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Specialized pixelcombine. just plot the foreground pixels
-------------------------------------------------------------------------------}
begin
  if F <> 0 then B := F;
  //if F and COLORMASK <> 0 then B := F;
end;

procedure TLevelRenderer.CombineObjectGhosted(F: TColor32; var B: TColor32; M: TColor32);
{-------------------------------------------------------------------------------
  Specialized pixelcombine. This is for SuperLemmini invisible objects.
-------------------------------------------------------------------------------}
var
  cr, cg, cb: Integer;
begin
  if F <> 0 then
  begin
    cr := ((F shr 16) mod 256) * 2;
    cg := ((F shr 8) mod 256) * 2;
    cb := (F mod 256) * 2;
    cr := (cr + (((B shr 16) mod 256) * 2)) div 5;
    cg := (cg + (((B shr 8) mod 256) * 2)) div 5;
    cb := (cb + ((B mod 256) * 2)) div 5;
    B := (cr shl 16) + (cg shl 8) + cb;
  end;
  //if F and COLORMASK <> 0 then B := F;
end;

function TLevelRenderer.GetObjectCombineEvent(aFlags: TObjectDrawingFlags): TPixelCombineEvent;
{-------------------------------------------------------------------------------
  Look at the flags and decide what pixelcombineevent there is needed.
-------------------------------------------------------------------------------}
begin
  Result := nil;
  if odfInvisible in aFlags then
  begin
    Result := CombineObjectGhosted;
  end else
  if odfNoOverwrite in aFlags then
  begin
    Result := CombineObjectNoOverwrite;
    if odfOnlyShowOnTerrain in aFlags then
      if litTerrain in fRenderTypes then
        Result := CombineObjectOnlyOnTerrain
      else
        Result := CombineObjectOverwrite;
  end

  else if odfOnlyShowOnTerrain in aFlags then
  begin
    if litTerrain in fRenderTypes then
      Result := CombineObjectOnlyOnTerrain
    else
      Result := CombineObjectOverwrite;
  end

  else
    Result := CombineObjectOverwrite;
end;

procedure TLevelRenderer.CombineSteelNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
begin
  //if (F <> 0) and (B = 0) then B := F;
//  if (F and COLORMASK <> 0) and (B and COLORMASK = 0) then B := F
  if B = 0 then B := F;
end;

procedure TLevelRenderer.CombineSteelOverwrite(F: TColor32; var B: TColor32; M: TColor32);
begin
  //if F <> 0 then B := F;
//  if F and COLORMASK <> 0 then B := F;
  if B = 0 then
    B := F
  else
    B := BlendRegEx(F, B, 128);
end;

procedure TLevelRenderer.CombineNegSteelNoOverwrite(F: TColor32; var B: TColor32; M: TColor32);
begin
  //if (F <> 0) and (B = 0) then B := F;
//  if (F and COLORMASK <> 0) and (B and COLORMASK = 0) then B := F
  F := F xor $FFFFFF;
  if B = 0 then B := F;
end;

procedure TLevelRenderer.CombineNegSteelOverwrite(F: TColor32; var B: TColor32; M: TColor32);
begin
  //if F <> 0 then B := F;
//  if F and COLORMASK <> 0 then B := F;
  F := F xor $FFFFFF;
  if B = 0 then
    B := F
  else
    B := BlendRegEx(F, B, 128);
end;

function TLevelRenderer.GetSteelCombineEvent: TPixelCombineEvent;
begin
  if roSteelNoOverwrite in fRenderOptions then
    Result := CombineSteelNoOverwrite
  else
    Result := CombineSteelOverwrite;
end;

procedure TLevelRenderer.CombineTriggerArea(F: TColor32; var B: TColor32; M: TColor32);
begin
  B := BlendRegEx(F, B, {128}196)
end;


procedure TLevelRenderer.CombineGridAndTerrain(F: TColor32; var B: TColor32; M: TColor32);
begin
  if F <> 0 then B := 255;
end;


procedure TLevelRenderer.SetRenderOptions(const Value: TRenderOptions);
begin
  if fRenderOptions = Value then
    Exit;
  fRenderOptions := Value;
end;

function TLevelRenderer.HitTest(aLevel: TLevel; X, Y: Integer; aList: TList;
      aTypes: TLevelItemTypes = [litObject, litTerrain, litSteel]; aMaxCount: Integer = 1): Integer;
{-------------------------------------------------------------------------------
  o Check which terrain/objects are under a coordinate and put them in the list.
    The function returns the index of the first terrain/object found.
  o Hacked access to the collectionitems is made, so it is as fast as possible.
-------------------------------------------------------------------------------}
var
  i: Integer;
  T: TTerrain;
  O: TInteractiveObject;
  S: TSteel;
  Bmp: TBitmap32;
  G: Integer;
  Pt: TPoint;
begin
  Result := -1;
  if aMaxCount < 1 then
    Exit;
  if aList = nil then
    Exit;
  G := aLevel.Statics.GraphicSet;
  Pt := Point(X, Y);

  // if Steel in foreground
  if not (roSteelNoOverwrite in fRenderOptions) then
    if litSteel in aTypes then
      with aLevel.SteelCollection, HackedList do
        for i := Count - 1 downto 0 do
        begin
          S := List^[i];
          if S.ItemActive then
          if PtInRect(S.Bounds, Pt) then
          begin
            aList.Add(S);
            if Result = -1 then Result := i;
            if aList.Count >= aMaxCount then Exit;
          end;
        end;

  // objects first
  if litObject in aTypes then
    with aLevel.InteractiveObjectCollection, HackedList do
    for i := Count - 1 downto 0 do
    begin
      O := List^[i];
      if O.ItemActive then
      if PtInRect(O.Bounds, Pt) then
      begin
        aList.Add(O);
        if Result = -1 then Result := i;
        if aList.Count >= aMaxCount then Exit;
      end;
  end;

  // front
  if litTerrain in aTypes then
    with aLevel.TerrainCollection, HackedList do
      for i := Count - 1 downto 0 do
      begin
        T := List^[i];
        //if not (tdfNoOverwrite in T.TerrainDrawingFlags) then
        if T.ItemActive then
        if PtInRect(T.Bounds, Pt) then
        begin
          aList.Add(T);
          if Result = -1 then Result := i;
          if aList.Count >= aMaxCount then Exit;
        end;
      end;

  // back
  {if litTerrain in aTypes then
    with aLevel.TerrainCollection, HackedList do
      for i := Count - 1 downto 0 do
      begin
        T := List^[i];
        if tdfNoOverwrite in T.TerrainDrawingFlags then
        if T.ItemActive then
        if PtInRect(T.Bounds, Pt) then
        begin
          aList.Add(T);
          if Result = -1 then Result := i;
          if aList.Count >= aMaxCount then Exit;
        end;
      end;}

  // if Steel in background
  if roSteelNoOverwrite in fRenderOptions then
    if litSteel in aTypes then
      with aLevel.SteelCollection, HackedList do
        for i := Count - 1 downto 0 do
        begin
          S := List^[i];
          if S.ItemActive then
          if PtInRect(S.Bounds, Pt) then
          begin
            aList.Add(S);
            if Result = -1 then Result := i;
            if aList.Count >= aMaxCount then Exit;
          end;
        end;

end;

procedure TLevelRenderer.RenderDosPreview(aLevel: TLevel);
{-------------------------------------------------------------------------------
  scaled preview
-------------------------------------------------------------------------------}
var
  i: Integer;
  T: TTerrain;
  O: TInteractiveObject;
  S: TSteel;
  OldDrawMode: TDrawMode;
  OldPixelCombine: TPixelCombineEvent;
  LevWidth, LevHeight: Integer;
  Rec: TDefaultsRec;
  MO: TMetaObject;
//ftest

    function MustRender(B: TBaseLevelItem): Boolean;
    begin
      Result := True;
      if Assigned(fOnRenderItem) then
        fOnRenderItem(Self, B, Result);
    end;


begin

{  if fLevelBitmap = nil
  then fInternalLevelBitmap := MainBitmap
  else }

  if fLevelBitmap=nil then exit;

  fInternalLevelBitmap := fLevelBitmap;

  fInternalLevelBitmap.BeginUpdate;

  OldDrawMode := fInternalLevelBitmap.DrawMode;
  OldPixelCombine := fInternalLevelBitmap.OnPixelCombine;
  try

    if (aLevel = nil) or (aLevel.Style = nil) or (aLevel.Graph = nil) then
    begin
      fInternalLevelBitmap.Clear(0);
      fInternalLevelBitmap.SetSize(0, 0);
      Exit;
    end;

    aLevel.Style.GetStyleParams(Rec);

    if ((aLevel.Style is TLemminiStyle) and (TLemminiStyle(aLevel.Style).SuperLemmini))
    or ((aLevel.Style is TCustLemmStyle) and (TCustLemmStyle(aLevel.Style).NeoLemmix)) then
    begin
      LevWidth := aLevel.Statics.Width;
      LevHeight := aLevel.Statics.Height;
    end else begin
      LevWidth := Rec.LevelWidth;
      LevHeight := Rec.LevelHeight;
    end;

    fInternalLevelBitmap.SetSize(LevWidth, LevHeight);
    fInternalLevelBitmap.Clear(0);


    (*
    if roCollisionGridBuilding in RenderOptions then
    begin
      CollisionGrid.Width := LevWidth;
      CollisionGrid.Height := LevHeight;
      CollisionGrid.Clear(0);
    end;*)

    with aLevel do
    begin

      // first we draw the terrain
      if litTerrain in fRenderTypes then
        with TerrainCollection, HackedList do
          for i := 0 to Count - 1 do
          begin
            T := List^[i];
            DrawTerrain({aLevel, }T, (aLevel.Statics.AutoSteelOptions and $80 <> 0));
          end;

      if roCollisionGridBuilding in RenderOptions then
        CollisionGrid.ReadFrom(fInternalLevelBitmap, ctUniformRGB);

      // then the objects (1. drawn on terrain. 2. the rest)
      if litObject in fRenderTypes then
      begin
        with InteractiveObjectCollection, HackedList do
        begin
          for i := 0 to Count - 1 do
          begin
            O := List^[i];
            if odfOnlyShowOnTerrain in O.ObjectDrawingFlags then
              DrawInteractiveObject(aLevel, O);
          end;
          for i := 0 to Count - 1 do
          begin
            O := List^[i];
            if not (odfOnlyShowOnTerrain in O.ObjectDrawingFlags) then
              DrawInteractiveObject(aLevel, O);
          end;
        end;
      end;

      // then the steel (only for editing of course)
      if litSteel in fRenderTypes then
        for i := 0 to SteelCollection.Count - 1 do
        begin
          S := SteelCollection[i];
          DrawSteel(aLevel, S);
        end;

      // then finally triggerarea's    lemgame
      if roShowTriggerAreas in fRenderOptions then
      begin

        with InteractiveObjectCollection, HackedList do
          for i := 0 to Count - 1 do
          begin
            {if i >= 15 then
              Break;}
            O := List^[i];
            MO := aLevel.Graph.MetaObjectCollection[O.ObjectID];
            if MO.TriggerEffect <> 0 then
              DrawTriggerArea(O, MO);
          end;
      end;



    end;

  finally
    fInternalLevelBitmap.DrawMode := OldDrawMode;
    fInternalLevelBitmap.OnPixelCombine := OldPixelCombine;
    fInternalLevelBitmap.EndUpdate;
    fInternalLevelBitmap.Changed;
  end;

//  if Assigned(fAfterBuild) then
  //  fAfterBuild(Self);
end;

{ TArchiveCompiler }

procedure TArchiveCompiler.ClearStateRec;
begin
  with StateRec do
  begin
    cState := '';
    cBusy := False;
    cPosition := 0;
    cMaximum := 0;
    cDetail := '';
  end;
end;

procedure TArchiveCompiler.CompileArchive(aStyle: TBaseLemmingStyle; aGraph: TBaseGraphicSet);
{-------------------------------------------------------------------------------

-------------------------------------------------------------------------------}
var
  Bmp: TBitmap32;
  i: Integer;
{  CompilePath, }TempPath, ArcName: string;
  Arc: TArchive;
  ToDo: Integer;
  Done: Integer;

//    procedure

  OldAccess: Boolean;

    procedure SetState(const aState: string = '';
                       aPosition: Integer = -1;
                       aMaximum: Integer = -1;
                       const aDetail: string = '');
    begin
      with StateRec do
      begin
        if aState <> '' then
          cState := aState;
        if aPosition <> -1 then
          cPosition := aPosition;
        cBusy := True;
        if aMaximum <> -1 then
          cMaximum := aMaximum;
        if aDetail <> '' then
          cDetail := aDetail;
      end;
      DoProgress;
    end;

    procedure CheckProgress;
    begin
      Mgr.Progress;
      if Mgr.UserCancel then
        raise Exception.Create('Compilation aborted by user');
    end;

begin
  Mgr.Clear;
//  ClearStateRec;
  //SetState('Start Compilation of ' + aStyle.StyleName + '.' + aGraph.GraphicSetName, -1, -1);
  Mgr.State := 'Start Compilation of ' + aStyle.StyleName + '.' + aGraph.GraphicSetName;
  CheckProgress;


  TempPath := CompilePath + '@Temp\';
//  CompilePath := CompilePath;
  ForceDirectories(TempPath);
  //ForceDirectories(CompilePath);

  Done := 0;
  ToDo := 0;

  with aGraph do
  begin
    GraphicSetArchive := '';
    OldAccess := ForceDirectAccess;
    ForceDirectAccess := True;
    try
      //SetState('fetching metadata', -1, -1);
      if not IsSpecial then Mgr.State := 'Fetching metadata';
      CheckProgress;
      if (not IsSpecial) or (aStyle is TLemminiStyle) then EnsureMetaData;
      Arc := nil;
      Bmp := nil;
      Bmp := TBitmap32.Create;
      Arc := TArchive.Create;
      Arc.ZipOptions := [];
      try
        ArcName := CompilePath + aStyle.StyleName + '_' + aGraph.GraphicSetName + SCompiledGraphExt;
        Mgr.State := 'Creating archive ' + ArcName;
        //SetState('creating archive ' + ArcName);
        CheckProgress;
        Arc.OpenArchive(ArcName, amCreate);

      if not IsSpecial then
      begin
        Mgr.Maximum := MetaObjectCollection.Count + MetaTerrainCollection.Count;
        //ToDo := MetaObjectCollection.Count + MetaTerrainCollection.Count;
        Mgr.Position := 0; //graphics
        SetState('Fetching object bitmaps', Done, ToDo);
        for i := 0 to MetaObjectCollection.Count - 1 do
        begin
          DirectGetObjectBitmap(i, Bmp); //ReplaceColor(Bmp, clBlack32, 0);
          Bmp.SaveToFile(TempPath + 'obj_' + LeadZeroStr(i, 3) + '.bmp');
          Arc.AddFile(TempPath + 'obj_' + LeadZeroStr(i, 3) + '.bmp');
          //deb(['obj',i]);
          Mgr.Position := Mgr.Position + 1;//Inc(Done);
          Mgr.Detail := 'Object ' + i2s(i);
          //SetState('', Done, ToDo, 'object ' + i2s(i));
          CheckProgress;
        end;
        SetState('Fetching terrain bitmaps', Done, ToDo);
        for i := 0 to MetaTerrainCollection.Count - 1 do
        begin
          DirectGetTerrainBitmap(i, Bmp); //ReplaceColor(Bmp, clBlack32, 0);
          Bmp.SaveToFile(TempPath + 'ter_' + LeadZeroStr(i, 3) + '.bmp');
          Arc.AddFile(TempPath + 'ter_' + LeadZeroStr(i, 3) + '.bmp');
          Mgr.Position := Mgr.Position + 1;
          //Inc(Done);
          //SetState('', Done, ToDo, 'terrain ' + i2s(i));
          Mgr.Detail := 'Terrain ' + i2s(i);
          CheckProgress;
           //deb(['ter',i]);
        end;

      end else begin

      if aStyle is TLemminiStyle then
      begin
        Mgr.State := 'Fetching backgroundbitmap';
        CheckProgress;
        DirectGetBackGroundBitmap(Bmp);
        Bmp.SaveToFile(TempPath + 'back.bmp');
        Arc.AddFile(TempPath + 'back.bmp');
      end else begin
        if GraphicSetIdExt > 0 then
        begin
          Mgr.State := 'Fetching backgroundbitmap';
          CheckProgress;
          //SetState('fetching backgroundbitmap', Done, ToDo);
          DirectGetBackGroundBitmap(Bmp);
          Bmp.SaveToFile(TempPath + 'back.bmp');
          Arc.AddFile(TempPath + 'back.bmp');
          //deb(['back']);
        end;
      end;


      end;

        aGraph.SaveToTxtFile(TempPath + SCompiledGraphTxt);
        Arc.AddFile(TempPath + SCompiledGraphTxt);
        aGraph.SaveToFile(TempPath + SCompiledGraphBin);
        Arc.AddFile(TempPath + SCompiledGraphBin);

        GraphicSetArchive := ExtractFileName(ArcName);

        Mgr.State := 'Ready';
        CheckProgress;
        //SetState('ready', Done, ToDo);

      finally
        Bmp.Free;
        Arc.Free;
      end;
    finally
      ForceDirectAccess := OldAccess;
    end;
  end;
end;

procedure TArchiveCompiler.CompileMetaFile(aStyle: TBaseLemmingStyle; aGraph: TBaseGraphicSet; AddToArchive: Boolean);
var
  TempPath: string;
  Arc: TArchive;
begin
  TempPath := ApplicationPath + 'Temp\';
  aGraph.DirectReadMetaData;
  aGraph.SaveToFile(TempPath + 'graph.bin');
  aGraph.SaveToTxtFile(TempPath + 'graph.txt');

  if AddToArchive then
  begin
    Arc := TArchive.Create;
    Arc.ZipOptions := [];
    Arc.OpenArchive(CompilePath + aGraph.GraphicSetArchive, amOpen);

    if Arc.ArchiveList.IndexOf('graph.bin') >= 0 then
      Arc.RemoveFile('graph.bin');
    if Arc.ArchiveList.IndexOf('graph.txt') >= 0 then
      Arc.RemoveFile('graph.txt');

    Arc.AddFile(TempPath + 'graph.txt');
    Arc.AddFile(TempPath + 'graph.bin');
    Arc.Free;
  end;
end;


constructor TArchiveCompiler.Create;
begin
  inherited;
  fMgr := TCustomProgressManager.Create(Self, LM_PROGRESS);
end;

destructor TArchiveCompiler.Destroy;
begin
  fMgr.Free;
  inherited;
end;

procedure TArchiveCompiler.DoProgress;
begin
  if Assigned(fOnProgress) then fOnProgress(Self, StateRec);
end;

end.
