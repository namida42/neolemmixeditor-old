{$include lem_directives.inc}

unit LemApp;

interface

uses
  Windows, Classes, Graphics, Forms, UFastStrings, UMisc, UTools, SysUtils,
  IniFiles;
//  LemCore,
  //LemEdit, FMapEditor;


  //tijdelijk gecentraliseerd
const
  ObjColor: TColor = 239 + 254 shl 8 + 237 shl 16;
  TerColor: TColor = 252 + 239 shl 8 + 226 shl 16;
  StlColor: TColor = 240 + 240 shl 8 + 240 shl 16;

  ObjColorH: TColor = 227 + 254 shl 8 + 222 shl 16;
  TerColorH: TColor = 247 + 217 shl 8 + 187 shl 16;
  StlColorH: TColor = clBlue;//240 + 240 shl 8 + 240 shl 16;

//RGB(227, 254, 222);
//RGB(247, 217, 187);
//clblue

type
  ISetting = interface
    ['{2EF21245-80F2-4151-BA8E-6607BE16254D}']
    function GetSection: string;
  end;

  TObjectMemIniFile = class(TMemIniFile)
  private
  protected
  public
    procedure WriteObject(aObject: TObject; const aSection: string);
  published
  end;

  TAbstractSettings = class(TPersistent)
  private
  protected
    procedure ResetToDefault; virtual;
    function GetSection: string; virtual;
    property Section: string read GetSection;
  public
    constructor Create;
  end;

  TGlobalSettings = class(TAbstractSettings)
  private
    fLanguage: string;
  protected
    procedure ResetToDefault; override;
  public
  published
    property Language: string read fLanguage write fLanguage;
  end;

  THistorySettings = class(TAbstractSettings)
  private
    fEditorLastOpenDir: string;
    fEditorLastSaveDir: string;
    fEditorLastOpenFilterIndex: Integer;
    fEditorLastSaveFilterIndex: Integer;
  protected
    procedure ResetToDefault; override;
  public
  published
    property EditorLastOpenDir: string read fEditorLastOpenDir write fEditorLastOpenDir;
    property EditorLastSaveDir: string read fEditorLastSaveDir write fEditorLastSaveDir;
    property EditorLastOpenFilterIndex: Integer read fEditorLastOpenFilterIndex write fEditorLastOpenFilterIndex;
    property EditorLastSaveFilterIndex: Integer read fEditorLastSaveFilterIndex write fEditorLastSaveFilterIndex;
  end;


  TRecentFilesSettings = class(TAbstractSettings)
  private
    fFileStrings: TStringList;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    property FileStrings: TStringList read fFileStrings;
  published
  end;

  TScreenSettings = class(TAbstractSettings)
  private
    fEditorScale: Single; // scale of image on editor form
  protected
  public
  published
    property EditorScale: Single read fEditorScale write fEditorScale;
  end;

  TEditorSettings = class(TAbstractSettings)
  private
    fSteelSelectorColor   : Cardinal;
    fTerrainSelectorColor: Cardinal;
    fObjectSelectorColor: Cardinal;
    fDirectDragOnSelect: Boolean;
    fClearSelectionOnSelect: Boolean;
    fClearSelectionOnAirClick: Boolean;
    fWarningOnDirectKeyboardChanges: Boolean;
    fMapHeight: Integer;
    fLastFile: string;
    fJavaPath: string;
    fAltPasteMode: Boolean;
    fNoPromptOnNewPiece: Boolean;
    fOneWayDefault: Boolean;
    fNoOverwriteDefault: Boolean;
    fZoomSetting: Integer;

    //levelpack editor
    fDosPackPreviewStyleName: string;
    fDosPackReadOnly: Boolean;
    fDosPackPreviewOn: Boolean;

    fDefaultArrowKeysDistance: Integer;
    fDefaultArrowKeysCtrlDistance: Integer;

  protected
    procedure ResetToDefault; override;
  public
  published
    property JavaPath: string read fJavaPath write fJavaPath;
    property AltPasteMode: Boolean read fAltPasteMode write fAltPasteMode;
    property MapHeight: Integer read fMapHeight write fMapHeight;
    property LastFile: string read fLastFile write fLastFile;
  // colors
    property ObjectSelectorColor: Cardinal read fObjectSelectorColor write fObjectSelectorColor;
    property TerrainSelectorColor: Cardinal read fTerrainSelectorColor write fTerrainSelectorColor;
    property SteelSelectorColor: Cardinal read fSteelSelectorColor write fSteelSelectorColor;
  // mouse
    property DirectDragOnSelect: Boolean read fDirectDragOnSelect write fDirectDragOnSelect;
    property ClearSelectionOnSelect: Boolean read fClearSelectionOnSelect write fClearSelectionOnSelect;
    property ClearSelectionOnAirClick: Boolean read fClearSelectionOnAirClick write fClearSelectionOnAirClick;
    property NoPromptOnNewPiece: Boolean read fNoPromptOnNewPiece write fNoPromptOnNewPiece;
    property OneWayDefault: Boolean read fOneWayDefault write fOneWayDefault;
    property NoOverwriteDefault: Boolean read fNoOverwriteDefault write fNoOverwriteDefault;
    property ZoomSetting: Integer read fZoomSetting write fZoomSetting;
  // messages/warnings
    property WarningOnDirectKeyboardChanges: Boolean read fWarningOnDirectKeyboardChanges write fWarningOnDirectKeyboardChanges;

    property DosPackPreviewStyleName: string read fDosPackPreviewStyleName write fDosPackPreviewStyleName;
    property DosPackReadOnly: Boolean read fDosPackReadOnly write fDosPackReadOnly;
    property DosPackPreviewOn: Boolean read fDosPackPreviewOn write fDosPackPreviewOn;


    property DefaultArrowKeysDistance: Integer read fDefaultArrowKeysDistance write fDefaultArrowKeysDistance default 8;
    property DefaultArrowKeysCtrlDistance: Integer read fDefaultArrowKeysCtrlDistance write fDefaultArrowKeysCtrlDistance default 1;
//    property DefaultArrowKeysCtrlShiftDistance: Integer read fDefaultArrowKeysCtrlShiftDistance write fDefaultArrowKeysCtrlShiftDistance default 1;



    //    property WarningOnDeleteSelection: Boolean
//    property WarningOnSave
  //  property WarningOnSaveAs
  end;

  (*
  TRegisteredStylesSettings = class(TAbstractSettings) { "Styles" }
  private
  protected
  public
  published
    property Lemmini
  end;
  *)

  TPlayerSettings = class(TAbstractSettings)
  private
  protected
  public
  published
    
  end;

  TFormSettingItem = class(TCollectionItem)
  private
    fSection     : string;
    fStayOnTop   : Boolean;
    fHeight      : Integer;
    fLeft        : Integer;
    fWidth       : Integer;
    fTop         : Integer;
    fVisible     : Boolean;
    fMaximized   : TWindowState;
  protected
  public
    property Section: string read fSection write fSection;
  published
    property Visible: Boolean read fVisible write fVisible;
    property Left: Integer read fLeft write fLeft;
    property Top: Integer read fTop write fTop;
    property Width: Integer read fWidth write fWidth;
    property Height: Integer read fHeight write fHeight;
    property StayOnTop: Boolean read fStayOnTop write fStayOnTop;
    property WinState: TWindowState read fMaximized write fMaximized;
  end;

  TFormSettingsCollection = class(TCollectionEx)
  private
    function GetItem(Index: Integer): TFormSettingItem;
    procedure SetItem(Index: Integer; const Value: TFormSettingItem);
  protected
  public
    constructor Create;
    function Add: TFormSettingItem;
    function Insert(Index: Integer): TFormSettingItem;
    property Items[Index: Integer]: TFormSettingItem read GetItem write SetItem; default;
  published
  end;


  TApp = class(TPersistent)
  private
    fIniName                 : string;
    fGlobalSettings          : TGlobalSettings;
    fHistorySettings         : THistorySettings;
    fScreenSettings          : TScreenSettings;
    fEditorSettings          : TEditorSettings;
    fFormSettingsCollection  : TFormSettingsCollection;
    fRecentFilesSettings     : TRecentFilesSettings;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    procedure SaveForm(aForm: TForm);
    function ReadForm(aForm: TForm): Boolean; overload;
    procedure ReadForm(aForm: TForm; aSetting: TFormSettingItem); overload;

    property HistorySettings: THistorySettings read fHistorySettings;
    property ScreenSettings: TScreenSettings read fScreenSettings;
    property EditorSettings: TEditorSettings read fEditorSettings;
    property FormSettingsCollection: TFormSettingsCollection read fFormSettingsCollection;
    property RecentFilesSettings: TRecentFilesSettings read fRecentFilesSettings;
  published
  end;

var
  App: TApp;

implementation


{ TObjectMemIniFile }

procedure TObjectMemIniFile.WriteObject(aObject: TObject; const aSection: string);
begin
  //
end;

{ TAbstractSettings }

constructor TAbstractSettings.Create;
begin
  inherited Create;
  ResetToDefault;
end;

function TAbstractSettings.GetSection: string;
{-------------------------------------------------------------------------------
  Automatic section-string generation from classname
-------------------------------------------------------------------------------}
begin
  Result := CutLeft(FastReplace(ClassName, 'Settings', ''), 1);
  Assert(Result <> '', 'Settings.GetSection error');
end;

procedure TAbstractSettings.ResetToDefault;
begin
  //
end;

{ TGlobalSettings }

procedure TGlobalSettings.ResetToDefault;
begin
  fLanguage := 'ENGLISH';
end;

{ THistorySettings }

procedure THistorySettings.ResetToDefault;
begin
  inherited;
end;

{ TEditorMouseSettings }

procedure TEditorSettings.ResetToDefault;
begin
  fClearSelectionOnAirClick := True;
  fDirectDragOnSelect := True;
  fDefaultArrowKeysDistance := 1;
  fDefaultArrowKeysCtrlDistance := 8;
  fAltPasteMode := false;
  fNoPromptOnNewPiece := false;
  fOneWayDefault := true;
  fNoOverwriteDefault := true;
end;

{ TFormSettingsCollection }

function TFormSettingsCollection.Add: TFormSettingItem;
begin
  Result := TFormSettingItem(inherited Add);
end; 

constructor TFormSettingsCollection.Create;
begin 
  inherited Create(TFormSettingItem);
end; 

function TFormSettingsCollection.GetItem(Index: Integer): TFormSettingItem; 
begin 
  Result := TFormSettingItem(inherited GetItem(Index)) 
end; 

function TFormSettingsCollection.Insert(Index: Integer): TFormSettingItem; 
begin 
  Result := TFormSettingItem(inherited Insert(Index)) 
end; 

procedure TFormSettingsCollection.SetItem(Index: Integer; const Value: TFormSettingItem);
begin 
  inherited SetItem(Index, Value);
end;


{ TAppSettings }

constructor TApp.Create;
var
  Ini: TMemIniFile; S: TStringList;
  aValue: string;
  i: Integer;
begin
  inherited Create;
  fIniName := ApplicationPath + 'NeoLemmixEditor.ini';
  fHistorySettings := THistorySettings.Create;
  fScreenSettings := TScreenSettings.Create;
  fEditorSettings := TEditorSettings.Create;
  fFormSettingsCollection := TFormSettingsCollection.Create;
  fRecentFilesSettings := TRecentFilesSettings.Create;

  fEditorSettings.ResetToDefault;
  if FileExists(fIniName) then
  begin
    IniToObject(fIniName, fHistorySettings.Section, fHistorySettings, False);
    IniToObject(fIniName, fScreenSettings.Section, fScreenSettings, False);
    IniToObject(fIniName, fEditorSettings.Section, fEditorSettings, False);

    S := TStringList.Create;
    Ini := TMemIniFile.Create(fIniName);
    try
      Ini.ReadSection('RecentFiles', S);
      for i := 0 to S.Count - 1 do
      begin
        aValue := Ini.ReadString('RecentFiles', S[i], '');
        //windlg(avalue);
        fRecentFilesSettings.FileStrings.Add(aValue);
      end;
    finally
      S.Free;
      Ini.Free;
    end;
  end;

end;

destructor TApp.Destroy;
begin
  ObjectToIni(fIniName, fHistorySettings.Section, fHistorySettings, False);
  ObjectToIni(fIniName, fScreenSettings.Section, fScreenSettings, False);
  ObjectToIni(fIniName, fEditorSettings.Section, fEditorSettings, False);
  FreeAndNil(fHistorySettings);
  FreeAndNil(fScreenSettings);
  FreeAndNil(fEditorSettings);
  FreeAndNil(fFormSettingsCollection);
  FreeAndNil(fRecentFilesSettings);
  inherited Destroy;
end;

function TApp.ReadForm(aForm: TForm): Boolean;
var
  Inst: ISetting;
  S: string;
  Item: TFormSettingItem;
begin
  Result := False; // ??
  if Supports(aForm, ISetting, Inst) then
  begin
    Result := True;
    S := Inst.GetSection;
    Item := TFormSettingItem.Create(nil);
    try

      IniToObject(fIniName, S, Item);
      if Item.Height > 0 then BEGIN
      aForm.WindowState := Item.WinState;
      aForm.Visible := Item.Visible;
      if aForm.WindowState = wsNormal then
      begin
      aForm.Left := Item.Left;
      aForm.Top := Item.Top;
      aForm.Width := Item.Width;
      aForm.Height := Item.Height;
      end;
      if Item.StayOnTop then
         aForm.FormStyle := fsStayOnTop; END;
    finally
      Item.Free;
    end;
  end;
end;

procedure TApp.ReadForm(aForm: TForm; aSetting: TFormSettingItem);
var
  Inst: ISetting;
  S: string;
begin
  if Supports(aForm, ISetting, Inst) then
  begin
    S := Inst.GetSection;
    IniToObject(fIniName, S, aSetting);
  end;
end;

procedure TApp.SaveForm(aForm: TForm);
var
  Inst: ISetting;
  S: string;
  Item: TFormSettingItem;
begin
  if Supports(aForm, ISetting, Inst) then
  begin
    S := Inst.GetSection;
    Item := TFormSettingItem.Create(nil);
    try
      Item.Visible := aForm.Visible;
      Item.WinState := aForm.WindowState;
      Item.Left := aForm.Left;
      Item.Top := aForm.Top;
      Item.Width := aForm.Width;
      Item.Height := aForm.Height;
      Item.StayOnTop := (aForm.FormStyle = fsStayOnTop);
      ObjectToIni(fIniName, S, Item);
    finally
      Item.Free;
    end;
  end;
end;


{ TRecentFilesSettings }

constructor TRecentFilesSettings.Create;
begin
  inherited;
  fFileStrings := TStringList.Create;
  fFileStrings.Duplicates := dupIgnore;
end;

destructor TRecentFilesSettings.Destroy;
begin
  fFileStrings.Free;
  inherited;
end;

initialization
  App := TApp.Create;
finalization
  FreeAndNil(App);
end.


(*
function TRecentFilesSettings.GetFileInfo(aIndex: Integer): string;
begin
  Result := SplitString(Strings[aIndex], 2, ',');
end;

function TRecentFilesSettings.GetFileName(aIndex: Integer): string;
begin
  Result := UnQuotedStr(SplitString(Strings[aIndex], 0, ','));
end;

function TRecentFilesSettings.GetFileType(aIndex: Integer): string;
begin
  Result := SplitString(Strings[aIndex], 1, ',');
end;

procedure TRecentFilesSettings.LoadFromStrings(aStrings: TStrings);
var
  i: Integer;
  aName, aValue: string;
begin
  i := 0;
  repeat
    windlg(astrings[0]);
    aName := 'file_' + i2s(i);
    aValue := aStrings.Values[aName];
    if aValue = '' then
      Break;
  until False;
end;

procedure TRecentFilesSettings.SaveToStrings(aStrings: TStrings);
var
  i: Integer;
begin
  aStrings.Clear;
  for i := 0 to fStrings.Count - 1 do
  begin
    aStrings.Values['file_' + i2s(i)] := fStrings[i];
  end;
end;

procedure TRecentFilesSettings.SetFileInfo(aIndex: Integer;
  const Value: string);
begin

end;

procedure TRecentFilesSettings.SetFileName(aIndex: Integer;
  const Value: string);
begin

end;

procedure TRecentFilesSettings.SetFileType(aIndex: Integer;
  const Value: string);
begin

end;


