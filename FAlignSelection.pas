{$include lem_directives.inc}

unit FAlignSelection;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls,
  LemEdit, Buttons;

type
  TAlignSelectionForm = class(TForm)
    RadioGroupHorz: TRadioGroup;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    RadioGroupVert: TRadioGroup;
  private
  public
  end;

function ExecuteAlignSelectionForm(var Horz: TItemHorzAlign; var Vert: TItemVertAlign) : Boolean;

implementation

{$R *.dfm}

function ExecuteAlignSelectionForm(var Horz: TItemHorzAlign; var Vert: TItemVertAlign) : Boolean;
var
  F: TAlignSelectionForm;
begin
  Horz := ihaNone;
  Vert := ivaNone;
  F := TAlignSelectionForm.Create(nil);
  try
    F.Position := poMainFormCenter;
    Result := F.ShowModal = mrOK;
    if Result then
    begin
      Assert(F.RadioGroupHorz.ItemIndex >= Ord(Low(TItemHorzAlign)), 'align selection radiogroup error 1');
      Assert(F.RadioGroupHorz.ItemIndex <= Ord(High(TItemHorzAlign)), 'align selection radiogroup error 2');
      Assert(F.RadioGroupVert.ItemIndex >= Ord(Low(TItemVertAlign)), 'align selection radiogroup error 3');
      Assert(F.RadioGroupVert.ItemIndex <= Ord(High(TItemVertAlign)), 'align selection radiogroup error 4');
      Horz := TItemHorzAlign(F.RadioGroupHorz.ItemIndex);
      Vert := TItemVertAlign(F.RadioGroupVert.ItemIndex);
    end;
  finally
    FreeAndNil(F);
  end;
end;

end.

