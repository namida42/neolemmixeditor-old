{$include lem_directives.inc}
unit FNewLevel;

{-------------------------------------------------------------------------------
  This unit contains a dialog for selecting a style/graphicset for a new level.
-------------------------------------------------------------------------------}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  LemTypes,
  LemCore,
  LemStrings,
  Kernel_Resource,
  UMasterControls, Buttons, ExtCtrls, ComCtrls, ActnList, USplit,
  VirtualTrees;

type

  TNewLevelForm = class(TForm)
    lbStyles: TListBox;
    btnCancel: TButton;
    btnOK: TButton;
    lblChooseGS: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure lbStylesClick(Sender: TObject);
    procedure lbStylesDblClick(Sender: TObject);
  private
    SelectedGraph: TBaseGraphicSet;
    { Private declarations }
    function GetResult(var aStyle: TBaseLemmingStyle; var aGraph: TBaseGraphicSet): Boolean;
  public
    { Public declarations }
  end;

function ExecuteNewLevelForm(var aStyle: TBaseLemmingStyle; var aGraph: TBaseGraphicSet; const altTitle: Boolean = false; CurStyle: String = ''): Boolean;

implementation

{$R *.dfm}


function ExecuteNewLevelForm(var aStyle: TBaseLemmingStyle; var aGraph: TBaseGraphicSet; const altTitle: Boolean = false; CurStyle: String = ''): Boolean;
{-------------------------------------------------------------------------------
  Execute function for the form.
-------------------------------------------------------------------------------}
var
  F: TNewLevelForm;
begin
  aStyle := nil;
  aGraph := nil;

  F := TNewLevelForm.Create(nil);
  if altTitle then F.Caption := 'Select graphic set';
  if CurStyle <> '' then F.lblChooseGS.Caption := 'Select a graphic set (replacing "' + CurStyle + '")';
  try
    F.Position := poMainFormCenter;
    Result := F.ShowModal = mrOK;
    if Result then
      F.GetResult(aStyle, aGraph);
  finally
    F.Free;
  end;
end;

function TNewLevelForm.GetResult(var aStyle: TBaseLemmingStyle; var aGraph: TBaseGraphicSet): Boolean;
begin
  aStyle := StyleMgr.Styles[0];
  aGraph := SelectedGraph;
  Result := aGraph <> nil;
end;

procedure TNewLevelForm.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  lbStyles.Clear;
  with StyleMgr.Styles[0] do
    for i := 0 to GraphicSetList.Count-1 do
      if not (GraphicSetList[i].IsSpecial or (GraphicSetList[i].GraphicSetName = 'placeholder')) then
        lbStyles.AddItem(GraphicSetList[i].GraphicSetName, GraphicSetList[i]);
  btnOk.Enabled := false;
end;

procedure TNewLevelForm.lbStylesClick(Sender: TObject);
var
  i: Integer;
begin
  btnOk.Enabled := lbStyles.ItemIndex <> -1;
  if lbStyles.ItemIndex = -1 then
    SelectedGraph := nil
  else
    SelectedGraph := TBaseGraphicSet(lbStyles.Items.Objects[lbStyles.ItemIndex]);
end;

procedure TNewLevelForm.lbStylesDblClick(Sender: TObject);
begin
  if btnOk.Enabled then
    ModalResult := mrOk;
end;

end.


