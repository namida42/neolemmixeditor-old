unit FEditDosLevelPack;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VirtualTrees, ActiveX,
  UMisc, UMasterForms,
  LemTypes, LemCOre, LemDosCmp, LemDosArc,
  LemApp, LemEdit, LemStyles,
  LemDosStyles,
  FAskStyle, FBasicLevelEditor, FValidationReport, ActnList, Menus, ComCtrls;

type
  TAddMode = (
    amReplace,
    amAfterSelected,
    amBeforeSelected,
    amAppend
  );

type
  TFormEditDosLevelPack = class(TBasicLevelEditorForm)
    Tree: TVirtualStringTree;
    MainMenu: TMainMenu;
    ActionList: TActionList;
    StatusBar: TStatusBar;
    PopupMenu: TPopupMenu;
    procedure Form_Create(Sender: TObject);
    procedure Form_Destroy(Sender: TObject);
    procedure TreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure TreeChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    fDosArchive   : TDosDatArchiveEx;
//    fFileName     : string;
    fCurrentStyle : TBaseLemmingStyle;
    fLoadedIndex  : Integer;

  { file ... }
//    ActOpenWithEditor   : TAction;
    ActSave             : TAction;
    ActSaveAs           : TAction;

//    ActSaveEditorLevel      : TAction;

    ActSaveEditorAppend     : TAction;
    ActSaveEditorAfter      : TAction;
    ActSaveEditorBefore     : TAction;
    ActSaveEditorReplace    : TAction;

    ActAddFileAppend    : TAction;
    ActAddFileAfter     : TAction;
    ActAddFileBefore    : TAction;
    ActAddFileReplace   : TAction;
    ActRemoveCurrent    : TAction;

    ActMoveLevelUp      : TAction;
    ActMoveLevelDown    : TAction;
  { options ... }
//    ActToggleAutoPreview: TAction;
    ActFixErrorAll: TAction;
    ActChangeCurrentStyle: TAction;

    procedure SetFileName(const Value: string);
    procedure FillTree;
    function AutoDetectStyle(const aFileName: string): TBaseLemmingStyle;
    procedure Form_DropFiles(Sender: TObject; const DroppedFiles: TStrings);
  { action event handlers }

  { internal }
    function GetFileName: string;
  public
    ActNewLevelPack: TAction;
    procedure SaveInPack;
    property FileName: string read GetFileName write SetFileName;
  end;

function FormEditDosLevelPack: TFormEditDosLevelPack;
function FormEditDosLevelPackActive: Boolean;

implementation

{$R *.dfm}

uses
  FMapEditor;

var
  _Form: TFormEditDosLevelPack;

function FormEditDosLevelPackActive: Boolean;
begin
  Result := _Form <> nil;
end;

function FormEditDosLevelPack: TFormEditDosLevelPack;
var
  i: Integer;
begin
  with Screen do

  for i := 0 to CustomFormCount - 1 do
    if CustomForms[i] is TFormEditDosLevelPack then
    begin
      Result := TFormEditDosLevelPack(CustomForms[i]);
      //Result.CurrentStyle := aStyle;
      Exit;
    end;


  Result := TFormEditDosLevelPack.Create(Application);
//  Result.CurrentStyle := aStyle;

end;

{ TFormDosLevelPacks }

procedure TFormEditDosLevelPack.FillTree;
begin
  with fDosArchive do
  begin
    Tree.RootNodeCount := 0; //virtualtrees
//    fDosArchive.LoadFromFile;
    Tree.RootNodeCount := SectionList.Count;
//    Tree.Invalidate;
  end;
end;

procedure TFormEditDosLevelPack.SetFileName(const Value: string);
var
  Sty: TBaseLemmingStyle;
begin
  Tree.BeginUpdate;
  try
//    fFileName := Value;
    Caption := 'Edit DOS levelpack ' + '[' + ExtractFileName(Value) + ']';
    fDosArchive.FileName := Value;//LoadFromFile(fFileName);
    fDosArchive.LoadFromFile;
    Sty := AutoDetectStyle(Value);
    if Sty <> nil then
      fCurrentStyle := Sty;
//    FillTree;
  finally
    FillTree;
    Tree.EndUpdate;
  end;
end;

procedure TFormEditDosLevelPack.Form_Create(Sender: TObject);
var
  S: string;
  Sty: TBaseLemmingStyle;
begin
  _Form := Self;
  fDosArchive := TDosDatArchiveEx.Create;
  fLoadedIndex := -1;
  AcceptDraggedFiles := True;
  OnDropFiles := Form_DropFiles;
  Editor := GlobalEditor;

  Sty := StyleMgr[0];
  fCurrentStyle := Sty;
end;

procedure TFormEditDosLevelPack.Form_Destroy(Sender: TObject);
begin
  if fCurrentStyle <> nil then
    App.EditorSettings.DosPackPreviewStyleName := fCurrentStyle.StyleName
  else
    App.EditorSettings.DosPackPreviewStyleName := 'NeoLemmix';

  _Form := nil;
  fDosArchive.Free;
end;

procedure TFormEditDosLevelPack.TreeGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  B: PBytes;
  S: string;
  i: byte;
begin
  if Integer(Node^.Index) > fdosarchive.sectionlist.count-1 then
  begin
    celltext := '<ERROR>';
    exit;
  end;

  with fdosarchive.sectionlist[node^.index].DecompressedData do
  begin
    B := Memory;
    Seek(0, soFromBeginning);
    Read(i, 1);
    S := stringofchar(' ', 32);
    case i of
      0: move(B^[2048-32], s[1], 32);
      4: move(B^[80], s[1], 32);
      else move(B^[64], s[1], 32);
      end;
  end;
  CellText := Trim(S);
end;

procedure TFormEditDosLevelPack.TreeChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  S: TStream;
begin
  if Editor.IsLocked then Exit;
  if node<>tree.FocusedNode then exit;

  S := fdosarchive.sectionlist[node^.index].DecompressedData;
  S.Seek(0, soFromBeginning);
  Assert(fCurrentStyle <> nil);

  //Editor.LoadFromDosArchive(fDosArchive, Node^.Index, fCurrentStyle);
  Editor.LoadFromStream(fDosArchive.SectionList[Node^.Index].DecompressedData,
    fCurrentStyle, lffLVL);

  fLoadedIndex := node^.index; // lemedit
  Editor.Loader := Self;

end;

procedure TFormEditDosLevelPack.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if Editor.Loader = Self then
    Editor.Loader := nil;

  Action := caFree;
end;

function TFormEditDosLevelPack.AutoDetectStyle(const aFileName: string): TBaseLemmingStyle;
var
  i: Integer;
  P: string;
begin
  Result := StyleMgr[0];
end;


procedure TFormEditDosLevelPack.Form_DropFiles(Sender: TObject;
  const DroppedFiles: TStrings);
begin
  Filename := DroppedFiles[0];
end;

procedure TFormEditDosLevelPack.SaveInPack;
//var
  //S: TMemoryStream;
begin
  raise exception.create('not implemented');
end;

function TFormEditDosLevelPack.GetFileName: string;
begin
  Result := fDosArchive.FileName;
end;


end.

