object FormEditDosLevelPack: TFormEditDosLevelPack
  Left = 148
  Top = 223
  Width = 317
  Height = 367
  ActiveControl = Tree
  Caption = 'DOS Levelpack'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  Menu = MainMenu
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = Form_Create
  OnDestroy = Form_Destroy
  PixelsPerInch = 96
  TextHeight = 13
  object Tree: TVirtualStringTree
    Left = 0
    Top = 0
    Width = 301
    Height = 309
    Align = alClient
    ChangeDelay = 250
    Header.AutoSizeIndex = 0
    Header.Font.Charset = DEFAULT_CHARSET
    Header.Font.Color = clWindowText
    Header.Font.Height = -11
    Header.Font.Name = 'MS Sans Serif'
    Header.Font.Style = []
    Header.MainColumn = -1
    Header.Options = [hoColumnResize, hoDrag]
    PopupMenu = PopupMenu
    TabOrder = 0
    TreeOptions.MiscOptions = [toFullRepaintOnResize, toInitOnSave, toToggleOnDblClick, toWheelPanning]
    OnChange = TreeChange
    OnGetText = TreeGetText
    Columns = <>
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 309
    Width = 301
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object MainMenu: TMainMenu
    Left = 56
    Top = 16
  end
  object ActionList: TActionList
    Left = 16
    Top = 16
  end
  object PopupMenu: TPopupMenu
    Left = 96
    Top = 16
  end
end
