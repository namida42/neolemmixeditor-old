unit FGSDownload;

interface

uses
  StrUtils, LemNeoOnline, LemDosStyles, LemCore,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFDownloadForm = class(TForm)
    lbSets: TListBox;
    btnDownload: TButton;
    btnExit: TButton;
    Label1: TLabel;
    btnUpdate: TButton;
    procedure lbSetsClick(Sender: TObject);
    procedure btnDownloadClick(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
  public
    constructor Create(aOwner: TComponent); override;
    function GetGraphicSets: Boolean;
  end;

var
  FDownloadForm: TFDownloadForm;

implementation

{$R *.dfm}

constructor TFDownloadForm.Create(aOwner: TComponent);
begin
  inherited;
  OnlineEnabled := true;
  if not GetGraphicSets then
    raise Exception.Create('Obtain graphic set list failed.');
end;

function TFDownloadForm.GetGraphicSets: Boolean;
var
  SL: TStringList;
  i: Integer;
  S: String;

  function CheckIfAlreadyHas(aName: String): Boolean;
  var
    i: Integer;
  begin
    Result := false;
    aName := Lowercase(aName);
    for i := 0 to StyleMgr.Styles[0].GraphicSetCount-1 do
      if Lowercase(StyleMgr.Styles[0].GraphicSetList[i].GraphicSetInternalName) = aName then
      begin
        Result := true;
        Exit;
      end;
  end;

  function GetNameFromLine(aLine: String): String;
  var
    i: Integer;
  begin
    Result := '';
    for i := 1 to Length(aLine) do
      if aLine[i] = '=' then
        Break
      else
        Result := Result + aLine[i];
  end;
begin
  Result := false;
  OnlineEnabled := true;
  SL := TStringList.Create;
  try
    if not DownloadToStringList('http://www.neolemmix.com/styles.php', SL) then
      Exit;
    for i := 0 to SL.Count-1 do
    begin
      if LeftStr(SL[i], 1) = '#' then Continue;
      if LeftStr(SL[i], 2) = 'x_' then Continue;
      if SL[i] = '' then Continue;
      S := GetNameFromLine(SL[i]);
      if CheckIfAlreadyHas(S) then Continue;
      lbSets.Items.Add(S);
    end;
    Result := true;
  finally
    SL.Free;
  end;
end;

procedure TFDownloadForm.lbSetsClick(Sender: TObject);
begin
  btnDownload.Enabled := lbSets.ItemIndex <> -1;
end;

procedure TFDownloadForm.btnDownloadClick(Sender: TObject);
var
  S: String;
  TempStream: TMemoryStream;

  GS: TBaseNeoGraphicSet;
begin
  if lbSets.ItemIndex = -1 then Exit;
  S := Lowercase(lbSets.Items[lbSets.ItemIndex]);
  TempStream := TMemoryStream.Create;
  try
    if not DownloadToStream('http://www.neolemmix.com/styles/' + S + '.dat', TempStream) then
    begin
      ShowMessage('Download failed.');
      Exit;
    end;
    lbSets.Items.Delete(lbSets.ItemIndex);
    lbSets.ItemIndex := -1;
  except
    TempStream.Free;
    ShowMessage('Download failed due to an exception.');
    Exit;
  end;

  TempStream.SaveToFile(AppPath + StyleMgr.Styles[0].CommonPath + S + '.dat');
  TempStream.Free;

  GS := TBaseNeoGraphicSet.Create(StyleMgr.Styles[0]);
  GS.MetaInfoFile := S + '.dat';
  GS.GraphicFile := S + '.dat';
  GS.GraphicExtFile := S + '.dat';
  GS.GraphicSetFile := S + '.dat';
  GS.GraphicSetInternalName := S;
  GS.GraphicSetName := S;
  GS.GraphicSetId := 255;
  GS.IsSpecial := false;
  GS.GraphicSetOrderNumber := -1;

 
  StyleMgr.Styles[0].GraphicSetList.Add(GS);
  StyleMgr.Styles[0].GraphicSetList.Sort(SortGraphicSets);
end;

procedure TFDownloadForm.btnUpdateClick(Sender: TObject);
begin
  CheckForStyleUpdates(true);
end;

end.
