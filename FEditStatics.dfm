object FormEditStatics: TFormEditStatics
  Left = 283
  Top = 280
  BorderStyle = bsToolWindow
  Caption = 'Level properties'
  ClientHeight = 270
  ClientWidth = 320
  Color = clBtnFace
  DragKind = dkDock
  DragMode = dmAutomatic
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDeactivate = Form_Deactivate
  PixelsPerInch = 96
  TextHeight = 13
  object ML1: TMasterLabel
    Left = 8
    Top = 120
    Width = 65
    Height = 17
    Caption = 'Release rate'
    FocusControl = EditReleaseRate
  end
  object MasterLabel1: TMasterLabel
    Left = 8
    Top = 144
    Width = 65
    Height = 17
    Caption = 'Lemmings'
    FocusControl = EditLemmingsCount
  end
  object MasterLabel2: TMasterLabel
    Left = 8
    Top = 168
    Width = 65
    Height = 17
    Caption = 'Rescue'
    FocusControl = EditRescueCount
  end
  object MasterLabel12: TMasterLabel
    Left = 8
    Top = 32
    Width = 65
    Height = 17
    Caption = 'Title'
    FocusControl = EditTitle
  end
  object MasterLabel19: TMasterLabel
    Left = 8
    Top = 56
    Width = 65
    Height = 17
    Caption = 'Author'
    FocusControl = EditAuthor
  end
  object MasterLabel30: TMasterLabel
    Left = 8
    Top = 80
    Width = 65
    Height = 17
    Caption = 'Music File'
    FocusControl = EditMusicFile
  end
  object MasterLabel13: TMasterLabel
    Left = 192
    Top = 120
    Width = 65
    Height = 17
    Caption = 'Start'
    FocusControl = EditScreenStart
  end
  object MasterLabel20: TMasterLabel
    Left = 192
    Top = 144
    Width = 65
    Height = 17
    Caption = 'Size'
    FocusControl = EditLevelWidth
  end
  object MasterLabel28: TMasterLabel
    Left = 264
    Top = 120
    Width = 8
    Height = 17
    Caption = ','
    FocusControl = EditScreenStartY
  end
  object MasterLabel29: TMasterLabel
    Left = 264
    Top = 144
    Width = 8
    Height = 17
    Caption = 'x'
    FocusControl = EditLevelHeight
  end
  object Bevel1: TBevel
    Left = 8
    Top = 108
    Width = 305
    Height = 9
    Shape = bsTopLine
  end
  object Bevel2: TBevel
    Left = 8
    Top = 214
    Width = 305
    Height = 9
    Shape = bsTopLine
  end
  object MasterLabel16: TMasterLabel
    Left = 8
    Top = 8
    Width = 65
    Height = 17
    Caption = 'Primary Style'
    FocusControl = EditStyle
  end
  object lblID: TLabel
    Left = 144
    Top = 247
    Width = 14
    Height = 13
    Caption = 'ID:'
  end
  object CheckBoxTimeLimit: TCheckBox
    Left = 8
    Top = 192
    Width = 65
    Height = 17
    Caption = 'Time Limit'
    TabOrder = 1
    OnClick = CheckBoxTimeLimit_Click
  end
  object EditLemmingsCount: TEditEx
    Left = 88
    Top = 144
    Width = 40
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 4
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditRescueCount: TEditEx
    Left = 88
    Top = 168
    Width = 40
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 5
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditTimeLimit: TEditEx
    Left = 88
    Top = 192
    Width = 40
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 7
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditTimeLimitSecs: TEditEx
    Left = 138
    Top = 192
    Width = 40
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 6
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditReleaseRate: TEditEx
    Left = 88
    Top = 120
    Width = 40
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 3
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditTitle: TEditEx
    Left = 88
    Top = 32
    Width = 200
    Height = 21
    Ctl3D = True
    MaxLength = 32
    ParentCtl3D = False
    TabOrder = 8
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditAuthor: TEditEx
    Left = 88
    Top = 56
    Width = 112
    Height = 21
    Ctl3D = True
    MaxLength = 16
    ParentCtl3D = False
    TabOrder = 9
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditMusicFile: TEditEx
    Left = 88
    Top = 80
    Width = 112
    Height = 21
    Ctl3D = True
    MaxLength = 64
    ParentCtl3D = False
    TabOrder = 2
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object BtnSelectMusic: TButton
    Left = 210
    Top = 80
    Width = 21
    Height = 21
    Caption = '...'
    TabOrder = 12
    OnClick = BtnSelectMusic_Click
  end
  object EditScreenStart: TEditEx
    Left = 220
    Top = 120
    Width = 40
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 13
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditScreenStartY: TEditEx
    Left = 272
    Top = 120
    Width = 40
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 14
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditStyle: TEditEx
    Left = 88
    Top = 8
    Width = 201
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 0
    CaseSensitive = False
    IsPickList = False
  end
  object BtnSelectStyle: TButton
    Left = 294
    Top = 8
    Width = 21
    Height = 21
    Caption = '...'
    TabOrder = 15
    OnClick = BtnSelectStyle_Click
  end
  object BtnResetID: TButton
    Left = 240
    Top = 243
    Width = 64
    Height = 21
    Caption = 'Reset ID'
    TabOrder = 16
    OnClick = BtnResetID_Click
  end
  object EditLevelWidth: TEditEx
    Left = 220
    Top = 144
    Width = 40
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 17
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object EditLevelHeight: TEditEx
    Left = 272
    Top = 144
    Width = 40
    Height = 21
    Ctl3D = True
    MaxLength = 6
    ParentCtl3D = False
    TabOrder = 18
    OnChange = Edit_Change
    OnExit = Edit_Exit
    OnKeyDown = Edit_KeyDown
    CaseSensitive = False
    IsPickList = False
  end
  object CheckBoxAutoSteel: TCheckBox
    Left = 8
    Top = 224
    Width = 70
    Height = 17
    Caption = 'Autosteel'
    TabOrder = 11
    OnClick = CheckBoxAutoSteel_Click
  end
  object CheckBoxIgnSteel: TCheckBox
    Left = 183
    Top = 224
    Width = 130
    Height = 17
    Caption = 'Disable Manual Steel'
    TabOrder = 19
    OnClick = CheckBoxIgnSteel_Click
  end
  object CheckBoxSimpleSteel: TCheckBox
    Left = 78
    Top = 224
    Width = 100
    Height = 17
    Caption = 'Simple Autosteel'
    TabOrder = 20
    OnClick = CheckBoxSimpleSteel_Click
  end
  object CheckBoxOWWInvert: TCheckBox
    Left = 8
    Top = 244
    Width = 121
    Height = 17
    Caption = 'One-Way Inversion'
    TabOrder = 10
    OnClick = CheckBoxOWWInvert_Click
  end
  object CheckBoxLockRR: TCheckBox
    Left = 132
    Top = 122
    Width = 61
    Height = 17
    Caption = 'Lock'
    TabOrder = 21
    OnClick = CheckBoxLockRRClick
  end
  object btnBackground: TButton
    Left = 208
    Top = 184
    Width = 105
    Height = 25
    Caption = 'Select Background'
    TabOrder = 22
    OnClick = btnBackgroundClick
  end
end
