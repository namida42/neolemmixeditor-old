object FormDosDecompress: TFormDosDecompress
  Left = 41
  Top = 185
  ActiveControl = EditFileName
  BorderStyle = bsDialog
  Caption = 'Decompress Sections from DOS Lemmings file'
  ClientHeight = 311
  ClientWidth = 536
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 232
    Top = 8
    Width = 53
    Height = 13
    Caption = 'Source File'
  end
  object Label2: TLabel
    Left = 232
    Top = 56
    Width = 75
    Height = 13
    Caption = 'Output directory'
  end
  object CheckListBoxArchive: TCheckListBox
    Left = 8
    Top = 8
    Width = 217
    Height = 297
    ItemHeight = 13
    TabOrder = 0
    OnKeyDown = CheckListBoxArchiveKeyDown
  end
  object BtnSelectFile: TButton
    Left = 496
    Top = 24
    Width = 20
    Height = 21
    Caption = '...'
    TabOrder = 2
    OnClick = BtnSelectFileClick
  end
  object BtnClose: TButton
    Left = 456
    Top = 280
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 8
    OnClick = BtnCloseClick
  end
  object EditOutputDir: TEdit
    Left = 232
    Top = 72
    Width = 259
    Height = 21
    TabOrder = 3
  end
  object EditFileName: TEdit
    Left = 232
    Top = 24
    Width = 259
    Height = 21
    TabOrder = 1
    OnExit = EditFileNameExit
    OnKeyDown = EditFileNameKeyDown
  end
  object BtnSelectDir: TButton
    Left = 496
    Top = 72
    Width = 21
    Height = 21
    Caption = '...'
    TabOrder = 4
  end
  object RadioGroupGen: TRadioGroup
    Left = 232
    Top = 128
    Width = 201
    Height = 105
    Caption = ' Filename generation when extracting '
    ItemIndex = 0
    Items.Strings = (
      'Automatic'
      'Prompt for filename')
    TabOrder = 5
  end
  object CheckBoxOverwriteExisting: TCheckBox
    Left = 232
    Top = 248
    Width = 201
    Height = 17
    Caption = 'Overwrite existing files'
    TabOrder = 6
  end
  object BtnExtractFiles: TButton
    Left = 296
    Top = 280
    Width = 155
    Height = 25
    Caption = 'Extract selected files'
    TabOrder = 7
    OnClick = BtnExtractFilesClick
  end
  object SaveDialog: TSaveDialog
    Left = 464
    Top = 128
  end
end
