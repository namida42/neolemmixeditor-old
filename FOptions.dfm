object FormOptions: TFormOptions
  Left = 198
  Top = 218
  Width = 612
  Height = 215
  Caption = 'Settings'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 596
    Height = 176
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Editor'
      object CbClearSelectionOnAirClick: TCheckBox
        Left = 24
        Top = 16
        Width = 257
        Height = 17
        Caption = 'Clear selection with single mouse click in air'
        TabOrder = 0
        OnClick = CbClearSelectionOnAirClickClick
      end
      object cbAltPasteMode: TCheckBox
        Left = 24
        Top = 112
        Width = 225
        Height = 17
        Caption = 'Alternate paste mode (requires restart)'
        TabOrder = 1
        OnClick = CbAltPasteModeClick
      end
      object cbNoPromptNewPiece: TCheckBox
        Left = 24
        Top = 40
        Width = 233
        Height = 17
        Caption = 'Don'#39't ask for piece type when adding piece'
        TabOrder = 2
        OnClick = cbNoPromptNewPieceClick
      end
      object cbOneWayDefault: TCheckBox
        Left = 24
        Top = 64
        Width = 225
        Height = 17
        Caption = 'Set one-way-capable on terrain by default'
        TabOrder = 3
        OnClick = cbOneWayDefaultClick
      end
      object RadioGroup1: TRadioGroup
        Left = 296
        Top = 16
        Width = 209
        Height = 89
        Caption = 'Zoom Position'
        Items.Strings = (
          'Center of selection / Center of view'
          'Center of selection / Mouse position'
          'Center of view'
          'Mouse position')
        TabOrder = 4
        OnClick = RadioGroup1Click
      end
      object cbNoOverwriteDefault: TCheckBox
        Left = 24
        Top = 88
        Width = 225
        Height = 17
        Caption = 'Set No Overwrite on objects by default'
        TabOrder = 5
        OnClick = cbNoOverwriteDefaultClick
      end
    end
  end
end
