unit FMassConvert;

interface

uses
  LemCore, LemDosStyles, LemLemminiStyles, LemEdit, LemDialogs, LemTypes,
  UFastStrings,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Menus, Contnrs, FileCtrl, StrUtils;

type

  TConvertItem = class
    private
      fSrcFileName: String;
      fSrcFormat: TLemmingFileFormat;
      fStatus: Integer;
    public
      constructor Create(aFile: String; aStyle: TLemmingFileFormat);
      function GetStatusText: String;
      property SrcFileName: String read fSrcFileName write fSrcFileName;
      property SrcFormat: TLemmingFileFormat read fSrcFormat write fSrcFormat;
      property Status: Integer read fStatus write fStatus;
      property StatusText: String read GetStatusText;
  end;

  TConvertList = class(TObjectList)
  private
    function GetItem(Index: Integer): TConvertItem;
  protected
  public
    function Add(Item: TConvertItem): Integer;
    procedure Insert(Index: Integer; Item: TConvertItem);
    property Items[Index: Integer]: TConvertItem read GetItem; default;
    property List;
  published
  end;

  TForm_MassConvert = class(TForm)
    btnConvert: TButton;
    btnClose: TButton;
    lvConvert: TListView;
    rgFilename: TRadioGroup;
    btnAddFiles: TButton;
    btnRemoveFile: TButton;
    btnClearFiles: TButton;
    cbDeleteSource: TCheckBox;
    Label2: TLabel;
    ebOutDir: TEdit;
    btnSelectDir: TButton;
    lbDebugState: TLabel;
    procedure btnClearFilesClick(Sender: TObject);
    procedure btnRemoveFileClick(Sender: TObject);
    procedure btnAddFilesClick(Sender: TObject);
    procedure btnConvertClick(Sender: TObject);
    procedure btnSelectDirClick(Sender: TObject);
    procedure btnClearFilesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    fDebugState: Integer;
    fEditor: TLevelEditor; // uses a seperate, invisible editor to avoid interfering with the main one
    fConvertList: TConvertList;
    procedure UpdateList;
  public
    constructor Create(aOwner: TComponent{; aEditor: TLevelEditor}); override;
    destructor Destroy; override;
  end;

procedure LaunchConversion;

const
  cnvReady = 0;
  cnvPass = 1;
  cnvGSFail = 2;
  cnvXGFail = 3;

  cnvUnknown = 255; // for unidentified errors

implementation

{$R *.dfm}

//TConvertList

function TConvertList.Add(Item: TConvertItem): Integer;
begin
  Result := inherited Add(Item);
end;

function TConvertList.GetItem(Index: Integer): TConvertItem;
begin
  Result := inherited Get(Index);
end;

procedure TConvertList.Insert(Index: Integer; Item: TConvertItem);
begin
  inherited Insert(Index, Item);
end;

//TConvertItem

constructor TConvertItem.Create(aFile: String; aStyle: TLemmingFileFormat);
begin
  fSrcFileName := aFile;
  fSrcFormat := aStyle;
  fStatus := 0;
end;

function TConvertItem.GetStatusText: String;
begin
  case fStatus of
    cnvReady: Result := 'Ready...';
    cnvPass: Result := 'Done!';
    cnvGSFail: Result := 'No matching graphic set';
    cnvXGFail: Result := 'No matching VGASPEC';

    cnvUnknown: Result := 'Unknown error occurred';
    else Result := '(Unknown status!)';
  end;
end;

//Form

procedure LaunchConversion;
var
  ConvForm: TForm_MassConvert;
begin
  ConvForm := TForm_MassConvert.Create(nil);
  ConvForm.ShowModal;
  ConvForm.Free;
end;

constructor TForm_MassConvert.Create(aOwner: TComponent{; aEditor: TLevelEditor});
begin
  inherited Create(aOwner);
  fEditor := TLevelEditor.Create(self);
  fConvertList := TConvertList.Create;
  fDebugState := 0;
end;

destructor TForm_MassConvert.Destroy;
begin
  fEditor.Free;
  fConvertList.Free;
  inherited;
end;

procedure TForm_MassConvert.UpdateList;
var
  i: Integer;
  LI: TListItem;
begin
  lvConvert.Items.Clear;
  for i := 0 to fConvertList.Count-1 do
  begin
    LI := lvConvert.Items.Add;
    LI.Caption := ExtractFileName(fConvertList[i].SrcFileName);
    if fConvertList[i].SrcFormat = lffLemmini then
      LI.SubItems.Add('Lemmini or SuperLemmini')
    else
      LI.SubItems.Add('Lemmix or NeoLemmix');
    LI.SubItems.Add(fConvertList[i].StatusText);
  end;
end;

procedure TForm_MassConvert.btnClearFilesClick(Sender: TObject);
begin
  fConvertList.Clear;
  UpdateList;
end;

procedure TForm_MassConvert.btnRemoveFileClick(Sender: TObject);
var
  i: Integer;
begin
  for i := lvConvert.Items.Count-1 downto 0 do //simple workaround for the shifting downwards indexes as they're deleted :P
    if lvConvert.Items[i].Selected then
      fConvertList.Delete(i);
  UpdateList;
end;

procedure TForm_MassConvert.btnAddFilesClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  aStyle: TBaseLemmingStyle;
  TempItem: TConvertItem;
  i: Integer;
begin
  OpenDlg := TOpenDialog.Create(self);
  OpenDlg.Filter := 'All supported levels (Lemmix, NeoLemmix, Lemmini, SuperLemmini)|*.lvl;*.ini|Lemmix / NeoLemmix Levels|*.lvl|Lemmini / SuperLemmini levels|*.ini';
  OpenDlg.Title := 'Select level file(s)...';
  OpenDlg.Options := [ofFileMustExist, ofAllowMultiSelect, ofHideReadOnly];
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;

  for i := 0 to OpenDlg.Files.Count-1 do
    if Uppercase(ExtractFileExt(OpenDlg.Files[i])) = '.INI' then
      fConvertList.Add(TConvertItem.Create(OpenDlg.Files[i], lffLemmini))
    else
      fConvertList.Add(TConvertItem.Create(OpenDlg.Files[i], lffLVL));

  if ebOutDir.Text = '' then ebOutDir.Text := ExtractFilePath(OpenDlg.Files[0]);

  UpdateList;
  OpenDlg.Free;
end;

procedure TForm_MassConvert.btnConvertClick(Sender: TObject);
var
  i, i2, i3, tx: Integer;
  TempBool: Boolean;
  fn: String;
begin

  i := 0;
  if (cbDeleteSource.Checked) then i := MessageDlg('Are you sure you want to delete the source files?', mtCustom, [mbYes, mbNo], 0);
  if i = mrNo then cbDeleteSource.Checked := false;

  if (RightStr(ebOutDir.Text, 1) <> '\')
  and (RightStr(ebOutDir.Text, 1) <> '/') then
    ebOutDir.Text := ebOutDir.Text + '\';

  TempBool := false;
  for i := 0 to fConvertList.Count-1 do
    if (ExtractFilePath(fConvertList[i].SrcFileName) = ebOutDir.Text) then
      TempBool := true;

  i := 0;
  if TempBool and (not cbDeleteSource.Checked) and (rgFileName.ItemIndex = 0) then i := MessageDlg('Your chosen combination of output directory and filenaming may result in some' + #13 + 'source files being overwritten. Continue?',
                                                                                                   mtCustom, [mbYes, mbNo], 0);
  if i = mrNo then Exit;

  for i2 := 0 to fConvertList.Count-1 do
  begin

  try
    if fConvertList[i2].Status = cnvPass then Continue;

    fEditor.Style := StyleMgr.Styles[0];
    fEditor.LoadFromFile(fConvertList[i2].SrcFileName, fEditor.Style, fConvertList[i2].SrcFormat);


    TempBool := false;
    for i := 0 to fEditor.Style.GraphicSetList.Count do
      if (not fEditor.Style.GraphicSetList[i].IsSpecial)
      and (fEditor.Style.GraphicSetList[i].GraphicSetInternalName = fEditor.Graph.GraphicSetInternalName) then
      begin
        TempBool := true;
        fEditor.Graph := fEditor.Style.GraphicSetList[i];
        Break;
      end;

    if not TempBool then
    begin
      fConvertList[i2].Status := cnvGSFail;
      Continue;
    end;

    fn := ChangeFileExt(ExtractFileName(fConvertList[i2].SrcFileName), '');
    if rgFileName.ItemIndex = 1 then fn := fn + '_convert';
    if rgFileName.ItemIndex = 2 then fn := fEditor.LevelName;

    fn := FastReplace(fn, '<', '_');
    fn := FastReplace(fn, '>', '_');
    fn := FastReplace(fn, ':', '_');
    fn := FastReplace(fn, '"', '_');
    fn := FastReplace(fn, '/', '_');
    fn := FastReplace(fn, '\', '_');
    fn := FastReplace(fn, '|', '_');
    fn := FastReplace(fn, '?', '_');
    fn := FastReplace(fn, '*', '_');
    fn := Trim(fn);

    if fEditor.Style is TLemminiStyle then
      fn := fn + '.ini'
    else
      fn := fn + '.lvl';

    fn := ebOutDir.Text + fn;

    if cbDeleteSource.Checked then DeleteFile(fConvertList[i2].SrcFileName);

    if fEditor.Style is TLemminiStyle then
      fEditor.SaveToFile(fn, lffLemmini, false)
    else
      fEditor.SaveToFile(fn, lffLVL, false);

    fConvertList[i2].fStatus := cnvPass;
  except
    fConvertList[i2].fStatus := cnvUnknown;
  end;

  end;

  UpdateList;
end;

procedure TForm_MassConvert.btnSelectDirClick(Sender: TObject);
var
  dn: String;
begin
  dn := ebOutDir.Text;
  if SelectDirectory('Select output folder', {ebOutDir.Text} '', dn) then
    ebOutDir.Text := dn;
end;

procedure TForm_MassConvert.btnClearFilesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_F5) and (fDebugState = 0) then fDebugState := 1;
  if (Key = VK_F4) and (ssShift in Shift) and (fDebugState = 1) then fDebugState := 2;
  if (Key = VK_F5) and (fDebugState = 2) then
  begin
    ShowMessage('Hidden options unlocked. Remember, these are hidden for a reason. Do' + #13 +
                'not use any hidden options unless you know exactly what they do, or have' + #13 +
                'been advised to use them by namida or another NeoLemmix expert.');

    // None of these actually exist at the moment.

    fDebugState := 3;
  end;
  if fDebugState <> 0 then lbDebugState.Caption := IntToStr(fDebugState);
end;

end.
