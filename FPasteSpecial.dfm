object PasteSpecialForm: TPasteSpecialForm
  Left = 325
  Top = 280
  Width = 217
  Height = 162
  Caption = 'Paste'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  Position = poMainFormCenter
  object MasterLabel1: TMasterLabel
    Left = 8
    Top = 8
    Width = 104
    Height = 17
    Caption = 'Horizontal distance'
  end
  object MasterLabel2: TMasterLabel
    Left = 8
    Top = 32
    Width = 104
    Height = 17
    Caption = 'Vertical distance'
  end
  object MasterLabel3: TMasterLabel
    Left = 8
    Top = 56
    Width = 104
    Height = 17
    Caption = 'Copies'
  end
  object EditHorz: TEditEx
    Left = 120
    Top = 8
    Width = 64
    Height = 21
    TabOrder = 0
    Text = '0'
    CaseSensitive = False
    IsPickList = False
  end
  object EditVert: TEditEx
    Left = 120
    Top = 32
    Width = 64
    Height = 21
    TabOrder = 1
    Text = '0'
    CaseSensitive = False
    IsPickList = False
  end
  object EditCopies: TEditEx
    Left = 120
    Top = 56
    Width = 64
    Height = 21
    TabOrder = 2
    Text = '1'
    CaseSensitive = False
    IsPickList = False
  end
  object BitBtn1: TBitBtn
    Left = 112
    Top = 88
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
    NumGlyphs = 2
  end
  object BitBtn2: TBitBtn
    Left = 32
    Top = 88
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
    NumGlyphs = 2
  end
end
