object FormAskStyle: TFormAskStyle
  Left = 304
  Top = 279
  Width = 290
  Height = 135
  Caption = 'Select Style'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = Form_Create
  PixelsPerInch = 96
  TextHeight = 13
  object ComboLemmingStyle: TComboBox
    Left = 8
    Top = 8
    Width = 241
    Height = 21
    ItemHeight = 13
    TabOrder = 0
  end
  object BtnOK: TButton
    Left = 176
    Top = 48
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object BtnCancel: TButton
    Left = 96
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
