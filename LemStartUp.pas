{$include lem_directives.inc}

unit LemStartup;

interface

uses
  Forms, FMapEditor,
  {$ifdef develop}
  ULog,
  {$endif}
  UMisc,
  LemCore,
  LemEdit;

procedure StartupEditor;

implementation

uses
  Sysutils, Dialogs,
  LemFiles;

procedure StartupEditor;
begin
  {$ifdef develop}
  SetDebProc(ULog.Deb);
  {$endif}

  Application.ShowMainForm := False;
  GlobalEditor := TLevelEditor.Create(Application);
  Application.CreateForm(TMapEditorForm, GlobalEditorForm);
  GlobalEditorForm.Editor := GlobalEditor;
  //  Application.MainForm := MapEditorForm;
  Application.ShowMainForm := True;
end;


end.

