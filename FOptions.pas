unit FOptions;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LemApp, StdCtrls, ComCtrls,
  UMisc, ExtCtrls;

type
  TFormOptions = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    CbClearSelectionOnAirClick: TCheckBox;
    cbAltPasteMode: TCheckBox;
    cbNoPromptNewPiece: TCheckBox;
    cbOneWayDefault: TCheckBox;
    RadioGroup1: TRadioGroup;
    cbNoOverwriteDefault: TCheckBox;
    procedure CbClearSelectionOnAirClickClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CbAltPasteModeClick(Sender: TObject);
    procedure cbNoPromptNewPieceClick(Sender: TObject);
    procedure cbOneWayDefaultClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure cbNoOverwriteDefaultClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function ExecuteFormOptions: Word;


implementation

{$R *.dfm}

function ExecuteFormOptions: Word;
var
  F: TFormOptions;
begin
  F := TFormOptions.Create(nil);
  try
    Result := F.ShowModal;
  finally
    F.Free;
  end;
end;

procedure TFormOptions.CbClearSelectionOnAirClickClick(Sender: TObject);
begin
  App.EditorSettings.ClearSelectionOnAirClick := CbClearSelectionOnAirClick.Checked;
end;

procedure TFormOptions.FormCreate(Sender: TObject);
begin
  CbClearSelectionOnAirClick.Checked := App.EditorSettings.ClearSelectionOnAirClick;
  cbAltPasteMode.Checked := App.EditorSettings.AltPasteMode;
  cbNoPromptNewPiece.Checked := App.EditorSettings.NoPromptOnNewPiece;
  cbOneWayDefault.Checked := App.EditorSettings.OneWayDefault;
  cbNoOverwriteDefault.Checked := App.EditorSettings.NoOverwriteDefault;
  RadioGroup1.ItemIndex := App.EditorSettings.ZoomSetting;
  //EditDefaultDistance.Text := i2s(App.EditorSettings.DefaultArrowKeysDistance)
end;


procedure TFormOptions.CbAltPasteModeClick(Sender: TObject);
begin
  App.EditorSettings.AltPasteMode := cbAltPasteMode.Checked;
end;

procedure TFormOptions.cbNoPromptNewPieceClick(Sender: TObject);
begin
  App.EditorSettings.NoPromptOnNewPiece := cbNoPromptNewPiece.Checked;
end;

procedure TFormOptions.cbOneWayDefaultClick(Sender: TObject);
begin
  App.EditorSettings.OneWayDefault := cbOneWayDefault.Checked;
end;

procedure TFormOptions.RadioGroup1Click(Sender: TObject);
begin
  App.EditorSettings.ZoomSetting := RadioGroup1.ItemIndex;
end;

procedure TFormOptions.cbNoOverwriteDefaultClick(Sender: TObject);
begin
  App.EditorSettings.NoOverwriteDefault := cbNoOverwriteDefault.Checked;
end;

end.

