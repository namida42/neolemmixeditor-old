{$include lem_directives.inc}

unit FLevelMatrix;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UMx, UMxStrings, UMisc,
  LemApp,
  FBasicLevelEditor,
  LemMisc, LemCore, LemEdit, GR32, GR32_Image, StdCtrls, ComCtrls;

type
  TLevelListEditorForm = class(TBasicLevelEditorForm)
    lvLevelItems: TListView;
    procedure FormCreate(Sender: TObject);
    procedure lvLevelItemsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);

  private
    fListViewWndProc: TWndMethod;
    procedure ListViewWndProc(var Msg: TMessage);
    procedure LMLevelChanged(var Msg: TLMLevelChanged); message LM_LEVELCHANGED;
    procedure LMSelectionChanged(var Msg: TLMSelectionChanged); message LM_SELECTIONCHANGED;
    procedure LMObjectListChanged(var Msg: TLMObjectListChanged); message LM_OBJECTLISTCHANGED;
    procedure LMTerrainListChanged(var Msg: TLMTerrainListChanged); message LM_TERRAINLISTCHANGED;
    procedure LMSteelListChanged(var Msg: TLMSteelListChanged); message LM_STEELLISTCHANGED;

    procedure SetList;

  protected
    procedure DoAfterSetEditor(Value: TLevelEditor); override;
  public
  end;

var
  LevelListEditorForm: TLevelListEditorForm;

implementation

{$R *.dfm}

{ TLevelListEditorForm }

procedure TLevelListEditorForm.ListViewWndProc(var Msg: TMessage);
begin
  // Some override code to always show vertical and never show horizontal scrollbars.
  ShowScrollBar(lvLevelItems.Handle, SB_HORZ, false);
  ShowScrollBar(lvLevelItems.Handle, SB_VERT, true);
  FListViewWndProc(Msg); // process message
end;

procedure TLevelListEditorForm.SetList;
var
  i: Integer;
  ListIndex: Integer;

  procedure SetItem(aPiece: String; aX, aY: Integer; aInfo: String);
  var
    UseItem: TListItem;
  begin
    if ListIndex < lvLevelItems.Items.Count then
      UseItem := lvLevelItems.Items[ListIndex]
    else
      UseItem := lvLevelItems.Items.Add;
    UseItem.Caption := aPiece;
    UseItem.SubItems.Clear;
    UseItem.SubItems.Add(IntToStr(aX));
    UseItem.SubItems.Add(IntToStr(aY));
    UseItem.SubItems.Add(aInfo);
    Inc(ListIndex);
  end;

  function GetObjectInfoString(aObject: TInteractiveObject): String;
  var
    MO: TMetaObject;
  begin
    if aObject.ObjectID >= aObject.Graph.MetaObjectCollection.Count then
      Result := '(Invalid object)'
    else begin
      MO := aObject.Graph.MetaObjectCollection[aObject.ObjectID];
      case MO.TriggerEffect of
        0: Result := 'No effect';
        1: begin
             Result := 'Exit';
             if (MO.AnimationFlags and 1) <> 0 then Result := Result + ' (triggered)';
           end;
        2: Result := 'One-Way-Left Field';
        3: Result := 'One-Way-Right Field';
        4: Result := 'Triggered Trap';
        5: Result := 'Water';
        6: Result := 'Fire';
        7: Result := 'One-Way-Left Arrows';
        8: Result := 'One-Way-Right Arrows';
        11: Result := 'Teleporter';
        12: Result := 'Receiver';
        13: begin
              Result := '-----';
              if (aObject.ObjectLValue and 1) <> 0 then Result[1] := 'C';
              if (aObject.ObjectLValue and 2) <> 0 then Result[2] := 'S';
              if (aObject.ObjectLValue and 4) <> 0 then Result[3] := 'F';
              if (aObject.ObjectLValue and 8) <> 0 then Result[3] := 'G';
              if (aObject.ObjectLValue and 16) <> 0 then Result[4] := 'D';
              if (aObject.ObjectLValue and 64) <> 0 then Result[5] := 'Z';
              if (aObject.ObjectLValue and 128) <> 0 then Result[5] := 'G';
              if (aObject.ObjectLValue and 32) <> 0 then Result := Result + '+Blocker';
              if Result = '-----' then
                Result := 'Lemming'
              else if Result = '-----+Blocker' then
                Result := 'Lemming (Blocker)'
              else
                Result := 'Lemming (' + Result + ')';
            end;
        14: begin
              case aObject.ObjectSValue of
                0: Result := 'Climber';
                1: Result := 'Floater';
                2: Result := 'Bomber';
                3: Result := 'Blocker';
                4: Result := 'Builder';
                5: Result := 'Basher';
                6: Result := 'Miner';
                7: Result := 'Digger';
                8: Result := 'Walker';
                9: Result := 'Swimmer';
                10: Result := 'Glider';
                11: Result := 'Disarmer';
                12: Result := 'Stoner';
                13: Result := 'Platformer';
                14: Result := 'Stacker';
                15: Result := 'Cloner';
              end;
              Result := 'Pickup ' + Result;
            end;
        15: Result := 'Exit (locked)';
        16: Result := 'Secret Trigger';
        17: Result := 'Unlock Button';
        18: Result := 'Radiation';
        19: Result := 'One-Way-Down Arrows';
        20: Result := 'Updraft';
        21: Result := 'Splitter';
        22: Result := 'Slowfreeze';
        23: begin
              Result := '-----';
              if (aObject.ObjectLValue and 1) <> 0 then Result[1] := 'C';
              if (aObject.ObjectLValue and 2) <> 0 then Result[2] := 'S';
              if (aObject.ObjectLValue and 4) <> 0 then Result[3] := 'F';
              if (aObject.ObjectLValue and 8) <> 0 then Result[3] := 'G';
              if (aObject.ObjectLValue and 16) <> 0 then Result[4] := 'D';
              if (aObject.ObjectLValue and 64) <> 0 then Result[5] := 'Z';
              if (aObject.ObjectLValue and 128) <> 0 then Result[5] := 'G';
              if Result = '-----' then
                Result := 'Entrance'
              else
                Result := 'Entrance (' + Result + ')';
            end;
        24: Result := 'Triggered Animation';
        26: Result := 'Anti-Splat Pad';
        27: Result := 'Splat Pad';
        28: Result := 'Two-Way Teleporter';
        29: Result := 'Standalone Teleporer';
        30: Result := 'Moving Background';
        31: Result := 'Single-Use Trap';
        else Result := '(Unknown type)';
      end;
    end;
  end;

  function GetTerrainInfoString(aTerrain: TTerrain): String;
  var
    MT: TMetaTerrain;

    procedure AddInfo(aInfo: String);
    begin
      if Result <> '' then
        Result := Result + ', ';
      Result := Result + aInfo;
    end;
  begin
    if aTerrain.TerrainID >= aTerrain.Graph.MetaTerrainCollection.Count then
      Result := '(Invalid piece)'
    else begin
      Result := '';
      MT := aTerrain.Graph.MetaTerrainCollection[aTerrain.TerrainID];
      if tdfErase in aTerrain.TerrainDrawingFlags then
        AddInfo('Eraser')
      else if tdfInvisible in aTerrain.TerrainDrawingFlags then
        AddInfo('Invisible')
      else if tdfFake in aTerrain.TerrainDrawingFlags then
        AddInfo('Fake')
      else if ((MT.Unknown and 1) <> 0) and ((Editor.Statics.AutoSteelOptions and 2) <> 0) then
        AddInfo('Steel');
    end;
  end;

  function GetSteelInfoString(aSteel: TSteel): String;
  begin
    Result := IntToStr(aSteel.SteelWidth) + 'x' + IntToStr(aSteel.SteelHeight);
    case aSteel.SteelType of
      1: Result := Result + ', Negative';
      2: Result := Result + ', One-Way-Left';
      3: Result := Result + ', One-Way-Right';
      4: Result := Result + ', One-Way-Down';
    end;
  end;

begin
  if Editor = nil then Exit;
  ListIndex := 0;

  lvLevelItems.OnSelectItem := nil;

  with Editor.InteractiveObjectCollection do
    for i := 0 to Count-1 do
      if Items[i].ItemActive then
      begin
        SetItem( 'Object ' + IntToStr(i),
                 Items[i].ObjectX,
                 Items[i].ObjectY,
                 GetObjectInfoString(Items[i]) );
        LvLevelItems.Items[ListIndex-1].Selected := Editor.SelectorCollection.FindBaseItem(Items[i]) >= 0;
      end;

  with Editor.TerrainCollection do
    for i := 0 to Count-1 do
      if Items[i].ItemActive then
      begin
        SetItem( 'Terrain ' + IntToStr(i),
                 Items[i].TerrainX,
                 Items[i].TerrainY,
                 GetTerrainInfoString(Items[i]) );
        LvLevelItems.Items[ListIndex-1].Selected := Editor.SelectorCollection.FindBaseItem(Items[i]) >= 0;
      end;

  with Editor.SteelCollection do
    for i := 0 to Count-1 do
      if Items[i].ItemActive then
      begin
        SetItem( 'Steel ' + IntToStr(i),
                 Items[i].SteelX,
                 Items[i].SteelY,
                 GetSteelInfoString(Items[i]) );
        LvLevelItems.Items[ListIndex-1].Selected := Editor.SelectorCollection.FindBaseItem(Items[i]) >= 0;       
      end;

  while lvLevelItems.Items.Count > ListIndex do
    lvLevelItems.Items.Delete(ListIndex);

  lvLevelItems.OnSelectItem := lvLevelItemsSelectItem;
end;

procedure TLevelListEditorForm.LMLevelChanged(var Msg: TLMLevelChanged);
begin
  SetList;
end;

procedure TLevelListEditorForm.LMSelectionChanged(var Msg: TLMSelectionChanged);
begin
  SetList;
end;

procedure TLevelListEditorForm.LMObjectListChanged(var Msg: TLMObjectListChanged);
begin
  SetList;
end;

procedure TLevelListEditorForm.LMSteelListChanged(var Msg: TLMSteelListChanged);
begin
  SetList;
end;

procedure TLevelListEditorForm.LMTerrainListChanged(var Msg: TLMTerrainListChanged);
begin
  SetList;
end;

procedure TLevelListEditorForm.DoAfterSetEditor(Value: TLevelEditor);
begin
  inherited;
  SetList;
end;

procedure TLevelListEditorForm.FormCreate(Sender: TObject);
begin
  fListViewWndProc := lvLevelItems.WindowProc; // save old window proc
  lvLevelItems.WindowProc := ListViewWndProc;
end;

procedure TLevelListEditorForm.lvLevelItemsSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
var
  i, n: Integer;
  CheckItem: TBaseLevelItem;

  function FindItem(aListIndex: Integer): TBaseLevelItem;
  var
    RefCollection: TBaseLevelItemCollection;
  begin
    RefCollection := Editor.InteractiveObjectCollection;

    if aListIndex >= Editor.InteractiveObjectCollection.Count then
    begin
      RefCollection := Editor.TerrainCollection;
      aListIndex := aListIndex - Editor.InteractiveObjectCollection.Count;
    end;

    if aListIndex >= Editor.TerrainCollection.Count then
    begin
      RefCollection := Editor.SteelCollection;
      aListIndex := aListIndex - Editor.TerrainCollection.Count;
    end;

    Result := RefCollection[aListIndex];
  end;
begin
  if Editor = nil then Exit;

  Editor.SelectorCollection.BeginUpdate;

  try
    for i := 0 to lvLevelItems.Items.Count-1 do
    begin
      CheckItem := FindItem(i);
      n := Editor.SelectorCollection.FindBaseItem(CheckItem);
      if (n >= 0) and not lvLevelItems.Items[i].Selected then
        Editor.SelectorCollection.Delete(n);
      if (n < 0) and lvLevelItems.Items[i].Selected then
        with Editor.SelectorCollection.Add do
          RefItem := CheckItem;
    end;
  finally
    Editor.SelectorCollection.EndUpdate;
  end;
end;

end.

