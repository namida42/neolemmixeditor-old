object FDownloadForm: TFDownloadForm
  Left = 215
  Top = 324
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Download Graphic Sets'
  ClientHeight = 433
  ClientWidth = 305
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 107
    Height = 13
    Caption = 'Available Graphic Sets'
  end
  object lbSets: TListBox
    Left = 16
    Top = 40
    Width = 273
    Height = 289
    ItemHeight = 13
    TabOrder = 0
    OnClick = lbSetsClick
  end
  object btnDownload: TButton
    Left = 96
    Top = 336
    Width = 113
    Height = 25
    Caption = 'Download'
    Enabled = False
    TabOrder = 1
    OnClick = btnDownloadClick
  end
  object btnExit: TButton
    Left = 96
    Top = 400
    Width = 113
    Height = 25
    Caption = 'Exit'
    ModalResult = 1
    TabOrder = 2
  end
  object btnUpdate: TButton
    Left = 96
    Top = 368
    Width = 113
    Height = 25
    Caption = 'Update All'
    TabOrder = 3
    OnClick = btnUpdateClick
  end
end
