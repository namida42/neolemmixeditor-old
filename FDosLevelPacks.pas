unit FDosLevelPacks;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, VirtualTrees,
  UMisc,
  LemTypes, LemDosCmp, LemDosArc,
  LemEdit, LemStyles;

type
  TFormDosLevelPacks = class(TForm)
    Tree: TVirtualStringTree;
    procedure Form_Create(Sender: TObject);
    procedure Form_Destroy(Sender: TObject);
    procedure TreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure TreeChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
  private
    fDosArchive: TDosDatArchive;
    fFileName: string;
    procedure SetFileName(const Value: string);
    procedure FillTree;
  public
    property FileName: string read fFileName write SetFileName;
  end;

implementation

{$R *.dfm}

{ TFormDosLevelPacks }

procedure TFormDosLevelPacks.FillTree;
var
  i: Integer;
  s: string;
  B: PBytes;
begin
  with fDosArchive do
  begin
//    windlg('start');
    LoadAll;


    Tree.RootNodeCount := SectionList.Count;
//    Tree.AddChild(nil, nil);
  //  windlg('ready');

    (*
    for i := 0 to SectionList.Count - 1 do
    begin
      //s := extractleveltitle(i);
      //windlg(s);
      with sectionlist[i].DecompressedData do
      begin
        B := Memory;
        s := stringofchar(' ', 32);
        move(B^[2048-31], s[1], 32);
//        windlg(s);
      end;
    end;
    *)


  end;
end;

procedure TFormDosLevelPacks.SetFileName(const Value: string);
begin
  fFileName := Value;
  fDosArchive.LoadFromFile(fFileName);
  FillTree;
end;

procedure TFormDosLevelPacks.Form_Create(Sender: TObject);
begin
  fDosArchive := TDosDatArchive.Create;
end;

procedure TFormDosLevelPacks.Form_Destroy(Sender: TObject);
begin
  fDosArchive.Free;
end;

procedure TFormDosLevelPacks.TreeGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  B: PBytes;
  S: string;
  i: byte;
begin
  with fdosarchive.sectionlist[node^.index].DecompressedData do
  begin
    B := Memory;
    Seek(0, soFromBeginning);
    Read(i, 1);
    S := stringofchar(' ', 32);
    case i of
      0: move(B^[2048-31], s[1], 32);
      else move(B^[65], s[1], 32);
      end;
  end;
  CellText := Trim(S);
end;

procedure TFormDosLevelPacks.TreeChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  S: TStream;
begin
  if GlobalEditor.IsLocked then Exit;
  if node<>tree.FocusedNode then exit;

  S := fdosarchive.sectionlist[node^.index].DecompressedData;
  S.Seek(0, soFromBeginning);
  GlobalEditor.LoadFromStream(S, custlemmstyle, lffLVL);
end;

end.

