object AlignSelectionForm: TAlignSelectionForm
  Left = 212
  Top = 176
  BorderStyle = bsDialog
  Caption = 'AlignSelectionForm'
  ClientHeight = 211
  ClientWidth = 237
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object RadioGroupHorz: TRadioGroup
    Left = 8
    Top = 8
    Width = 105
    Height = 169
    Caption = 'Horizontal'
    ItemIndex = 0
    Items.Strings = (
      '&1. No Change'
      '&2. Left Sides'
      '&3. Right Sides'
      '&4. Centers'
      '&5. Tile')
    TabOrder = 0
  end
  object BitBtn1: TBitBtn
    Left = 78
    Top = 184
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    NumGlyphs = 2
  end
  object BitBtn2: TBitBtn
    Left = 158
    Top = 184
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    NumGlyphs = 2
  end
  object RadioGroupVert: TRadioGroup
    Left = 128
    Top = 8
    Width = 105
    Height = 169
    Caption = 'Vertical'
    ItemIndex = 0
    Items.Strings = (
      '&a. No Change'
      '&b. Tops'
      '&c. Bottoms'
      '&d. Centers'
      '&e. Tile')
    TabOrder = 3
  end
end
