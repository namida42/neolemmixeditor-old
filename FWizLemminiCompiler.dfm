object WizardLemminiCompiler: TWizardLemminiCompiler
  Left = 92
  Top = 233
  Width = 535
  Height = 374
  Caption = 'WizardLemminiCompiler'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  Position = poMainFormCenter
  object Button1: TButton
    Left = 448
    Top = 312
    Width = 75
    Height = 25
    Caption = 'next'
    TabOrder = 0
  end
  object Button2: TButton
    Left = 360
    Top = 312
    Width = 75
    Height = 25
    Caption = 'prev'
    TabOrder = 1
  end
  object Button3: TButton
    Left = 272
    Top = 312
    Width = 75
    Height = 25
    Caption = 'cancel'
    TabOrder = 2
  end
  object EditEx1: TEditEx
    Left = 32
    Top = 32
    Width = 321
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    Text = 'EditEx1'
    CaseSensitive = False
    IsPickList = False
  end
  object BitBtn1: TBitBtn
    Left = 360
    Top = 32
    Width = 21
    Height = 19
    Caption = '...'
    TabOrder = 4
  end
  object CheckListBox1: TCheckListBox
    Left = 32
    Top = 64
    Width = 225
    Height = 233
    ItemHeight = 13
    TabOrder = 5
  end
  object Button4: TButton
    Left = 328
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Button4'
    TabOrder = 6
    OnClick = Button4Click
  end
end
