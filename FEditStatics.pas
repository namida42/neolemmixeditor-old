{$include lem_directives.inc}

unit FEditStatics;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ValEdit, UMisc,
  LemMisc,
  LemStyles, LemDosStyles, LemLemminiStyles,
  LemCore,
  LemEdit,
  LemDialogs,
  FNewLevel, FExtSelect, FMusic,
  FEditSkills,
  FPickTerrain,
  FBasicLevelEditor, StdCtrls, Mask, UMasterControls, GR32_Image, ExtCtrls,
  Math,
  ComCtrls;

type
  TFormEditStatics = class(TBasicLevelEditorForm)
    EditTitle           : TEditEx;
    EditAuthor          : TEditEx;
    EditMusicFile       : TEditEx;
    EditScreenStart     : TEditEx;
    EditScreenStartY    : TEditEx;
    Bevel1: TBevel;
    Bevel2: TBevel;


    EditReleaseRate     : TEditEx;
    EditLemmingsCount   : TEditEx;
    EditRescueCount     : TEditEx;
    EditTimeLimit       : TEditEx;
    EditTimeLimitSecs   : TEditEx;

    ML1: TMasterLabel;
    MasterLabel1: TMasterLabel;
    MasterLabel2: TMasterLabel;
    MasterLabel12: TMasterLabel;
    MasterLabel13: TMasterLabel;
    MasterLabel16: TMasterLabel;
    MasterLabel19: TMasterLabel;
    MasterLabel20: TMasterLabel;
    MasterLabel28: TMasterLabel;
    MasterLabel29: TMasterLabel;
    MasterLabel30: TMasterLabel;
    EditStyle: TEditEx;
    BtnSelectStyle: TButton;
    BtnSelectMusic: TButton;
    BtnResetID: TButton;
    EditLevelWidth: TEditEx;
    EditLevelHeight: TEditEx;

    CheckBoxAutoSteel: TCheckBox;
    CheckBoxIgnSteel: TCheckBox;
    CheckBoxSimpleSteel: TCheckBox;

    CheckBoxOWWInvert: TCheckBox;

    CheckBoxTimeLimit: TCheckBox;
    CheckBoxLockRR: TCheckBox;
    lblID: TLabel;
    btnBackground: TButton;


    procedure Edit_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure Form_Deactivate(Sender: TObject);
    procedure Edit_Exit(Sender: TObject);
    procedure Edit_Change(Sender: TObject);
    procedure BtnSelectStyle_Click(Sender: TObject);
    procedure BtnSelectMusic_Click(Sender: TObject);
    procedure BtnResetID_Click(Sender: TObject);
    procedure CheckBoxAutoSteel_Click(Sender: TObject);
    procedure CheckBoxIgnSteel_Click(Sender: TObject);
    procedure CheckBoxSimpleSteel_Click(Sender: TObject);
    procedure CheckBoxTimeLimit_Click(Sender: TObject);
    procedure CheckBoxOWWInvert_Click(Sender: TObject);

    procedure CheckBoxLockRRClick(Sender: TObject);
    procedure btnBackgroundClick(Sender: TObject);
  private
    procedure LMLevelChanged(var Msg: TLMLevelChanged); message LM_LEVELCHANGED;
    procedure LMStaticsChanged(var Msg: TLMStaticsChanged); message LM_STATICSCHANGED;
    procedure UpdateInterface;
    procedure UpdateVar(Sender: TEditEx; Silent: Boolean = false);
  protected
    procedure DoAfterSetEditor(Value: TLevelEditor); override;
  public
    SkillsForm: TFormEditSkills;
  end;

implementation

{$R *.dfm}

  // if change here then validate all eventhandlers (keypress!)
const
  tag_releaserate     = 1;
  tag_lemmingscount   = 2;
  tag_rescuecount     = 3;
  tag_timelimit       = 4;
  tag_graphicset      = 14;
  tag_screenstart     = 16;
  tag_screenstarty    = 42;
  tag_title           = 20;
  tag_superlemming    = 21;
  tag_timelimitsecs   = 22;
  tag_editmusic       = 23;
  tag_editotr         = 24;
  tag_editotl         = 25;
  tag_editauthor      = 33;
  tag_editwidth       = 34;
  tag_editheight      = 41;
  tag_falldist        = 35;
  tag_editmainlevel   = 36;
  tag_musicfile       = 45;
  tag_editpsr         = 46;
  tag_editpsl         = 47;

  tag_boundT = 37;
  tag_boundL = 38;
  tag_boundR = 39;
  tag_boundB = 40;



function CalcPerc(Max, Rescue: Integer): integer;
begin
  if (Max = 0) or (Rescue = 0) then
    Result := 0
  else begin
    Result := (Rescue * 100) div Max;
  end;
end;

function CalcPercStr(Max, Rescue: Integer): string;
begin
  Result := i2s(CalcPerc(Max, Rescue)) + '%';
end;

{ TEditStaticsForm }

procedure TFormEditStatics.LMLevelChanged(var Msg: TLMLevelChanged);
begin
  UpdateInterface;
end;

procedure TFormEditStatics.LMStaticsChanged(var Msg: TLMStaticsChanged);
begin
  UpdateInterface;
end;

procedure TFormEditStatics.UpdateInterface;
begin
  if Editor = nil then
    Exit;
  with Editor, Statics do
  begin

    EditTitle.Text := Levelname;
    EditAuthor.Text := LevelAuthor;
    EditMusicFile.Text := LevelMusicFile;
    EditScreenStart.Text := i2s(ScreenPosition);
    EditScreenStartY.Text := i2s(ScreenYPosition);

    if (Style <> nil) and (Graph <> nil) then
      EditStyle.Text := Editor.Graph.GraphicSetName;// StyleName; lemdialogs

    CheckBoxAutoSteel.Checked := (AutoSteelOptions and $2) <> 0;
    CheckBoxIgnSteel.Checked := (AutoSteelOptions and $4) <> 0;
    CheckBoxSimpleSteel.Checked := (AutoSteelOptions and $8) <> 0;
    CheckBoxOWWInvert.Checked := (AutoSteelOptions and $80) = 0;

    CheckBoxLockRR.Checked := LockedRR;

    EditReleaseRate.Text     := i2s(ReleaseRate);
    EditLemmingsCount.Text   := i2s(LemmingsCount);
    EditRescueCount.Text     := i2s(RescueCount);

    if HasTimeLimit then
    begin
      EditTimeLimit.Text       := i2s(TimeLimit mod 256);
      EditTimeLimitSecs.Text   := i2s(TimeLimit shr 8);
      CheckBoxTimeLimit.Checked := true;
    end else begin
      EditTimeLimit.Text       := '-';
      EditTimeLimitSecs.Text   := '-';
      CheckBoxTimeLimit.Checked := false;
    end;

    EditLevelWidth.Text      := i2s(Width);
    EditLevelHeight.Text     := i2s(Height);

    lblID.Caption := 'ID: ' + IntToHex(LevelID, 8);

  end;
end;

procedure TFormEditStatics.UpdateVar(Sender: TEditEx; Silent: Boolean = false);
var
  S: string;
  min: Integer;
begin
  if Editor = nil then
    Exit;
  if not Silent then
    Editor.BeginUpdate;
  try
  S := Sender.Text;
  with Editor.Statics do
    case Sender.Tag of
      tag_title          : Editor.LevelName := TrimRight(S);
      tag_editauthor     : Editor.LevelAuthor := TrimRight(S);
      tag_screenstart    : Editor.Statics.ScreenPosition := s2i(S);
      tag_screenstarty   : Editor.Statics.ScreenYPosition := s2i(S);
      tag_releaserate    : ReleaseRate := s2i(S);
      tag_lemmingscount  : LemmingsCount := s2i(S);
      tag_rescuecount    : RescueCount:= s2i(S);
      tag_timelimit      : TimeLimit := ((TimeLimit shr 8) shl 8) + s2i(S);
      tag_timelimitsecs  : TimeLimit := (TimeLimit mod 256) + (s2i(S) shl 8);
      tag_editwidth      : Width := max(s2i(S), 1);
      tag_editheight     : Height := max(s2i(S), 1);
      tag_musicfile      : Editor.LevelMusicFile := Trim(S);
    end;
    if not Silent then
      Sender.Modified := False;
  finally
    if not Silent then
      Editor.EndUpdate;
  end;
end;

procedure TFormEditStatics.Edit_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  E: TEditEx absolute Sender;
begin

  case Key of
    VK_RETURN:
      if Editor <> nil then
        if Sender is TEditEx then
          UpdateVar(E);
    VK_ESCAPE:
      begin
//        if Editor <> nil then
  //        if Sender is TEditEx then
    //        Log(['cancel']);
      end;
  end;

end;

procedure TFormEditStatics.FormCreate(Sender: TObject);
var
  R: TRect;
//  Ani: TLemmingAnimation;
const
  IntChars = ['0'..'9', '-'];
  HexChars = ['0'..'9', 'A'..'F', 'a'..'f'];
begin
  EditTitle.Tag             := tag_title;
  EditAuthor.Tag            := tag_editauthor;
  EditMusicFile.Tag         := tag_musicfile;
  EditScreenStart.Tag       := tag_screenstart;
  EditScreenStartY.Tag      := tag_screenstarty;

  EditReleaseRate.Tag       := tag_releaserate;
  EditLemmingsCount.Tag     := tag_lemmingscount;
  EditRescueCount.Tag       := tag_rescuecount;
  EditTimeLimit.Tag         := tag_timelimit;
  EditTimeLimitSecs.Tag     := tag_timelimitsecs;

  EditLevelWidth.Tag        := tag_editwidth;
  EditLevelHeight.Tag       := tag_editheight;


//  EditTitle.ValidChars           ;
  EditScreenStart.ValidChars     := IntChars;
  EditScreenStartY.ValidChars    := IntChars;
  EditReleaseRate.ValidChars     := IntChars;
  EditLemmingsCount.ValidChars   := IntChars;
  EditRescueCount.ValidChars     := IntChars;
  EditTimeLimit.ValidChars       := IntChars;

  EditLevelWidth.ValidChars      := IntChars;
  EditLevelHeight.ValidChars     := IntChars;

  SkillsForm := nil;
end;

procedure TFormEditStatics.Form_Deactivate(Sender: TObject);
var
  E: TEditEx;
begin
//exit;
  if ActiveControl is TEditEx then
  begin
    E := TEditEx(ActiveControl);
    if Assigned(E.OnExit) then
      E.OnExit(E);
  end;
end;

procedure TFormEditStatics.Edit_Exit(Sender: TObject);
begin
  if TEditEx(Sender).Modified then
    try
      UpdateVar(TEditEx(Sender));
    except
      TEditEx(Sender).SetFocus;
      raise;
    end;
end;

procedure TFormEditStatics.Edit_Change(Sender: TObject);
begin
  try
    UpdateVar(TEditEx(Sender), true);
  except
    // just ignore it if it happens here
  end;
end;

procedure TFormEditStatics.DoAfterSetEditor(Value: TLevelEditor);
begin
  UpdateInterface;
end;

procedure TFormEditStatics.BtnResetID_Click(Sender: TObject);
begin
  if MessageDlg('Resetting the level''s ID will cause replay checks to detect this as' + #13 +
                'a different level. Are you sure you want to reset it?', mtCustom, [mbYes, mbNo], 0) = mrYes then
  begin
    Editor.BeginUpdate;
    try
      Editor.Statics.LevelID := 0;
    finally
      Editor.EndUpdate;
    end;
  end;
end;

procedure TFormEditStatics.BtnSelectStyle_Click(Sender: TObject);
var
  S: TBaseLemmingStyle;
  G: TBaseGraphicSet;
  OK: Boolean;
  i: Integer;
  BgCount: Integer;
begin

  S := Editor.Style;
  OK := ExecuteNewLevelForm(S, G, true);
  if not OK then
    Exit;

  if (S <> Editor.Style) then
  begin
    i := mrYes;
    if (Editor.Style is TLemminiStyle) xor (S is TLemminiStyle) then
      i := MessageDlg('Converting between Lemmix and Lemmini styles may result in loss of some information.' + CrLf +
                      'It is recommended to save and reload the level after the conversion, as the editor may not' + CrLf +
                      'accurately represent the new format''s limitations otherwise. Do you wish to continue?', mtCustom, [mbYes, mbNo], 0)
    else if (Editor.Style is TCustLemmStyle) and TCustLemmStyle(Editor.Style).NeoLemmix and not TCustLemmStyle(S).NeoLemmix then
      i := MessageDlg('Converting from a NeoLemmix style to a traditional Lemmix style may result in loss of some information.' + CrLf +
                      'It is recommended to save and reload the level after the conversion, as the editor may not accurately' + CrLf +
                      'represent the new format''s limitations otherwise. Do you wish to continue?', mtCustom, [mbYes, mbNo], 0)
    else if (Editor.Style is TLemminiStyle) and TLemminiStyle(Editor.Style).SuperLemmini and not TLemminiStyle(S).SuperLemmini then
      i := MessageDlg('Converting from a SuperLemmini style to a traditional Lemmini style may result in loss of some information.' + CrLf +
                      'It is recommended to save and reload the level after the conversion, as the editor may not accurately' + CrLf +
                      'represent the new format''s limitations otherwise. Do you wish to continue?', mtCustom, [mbYes, mbNo], 0);
    if i = mrNo then Exit;
  end;

  Editor.BeginUpdate;

  try
  Editor.Style := S;
  Editor.Graph := G;

  if Editor.Style is TLemminiStyle then
  begin
    Editor.InteractiveObjectCollection.TargetItemCount := -1;
    Editor.TerrainCollection.TargetItemCount := -1;
    Editor.SteelCollection.TargetItemCount := -1;
  end else begin
    if TCustLemmStyle(Editor.Style).NeoLemmix then
    begin
      Editor.InteractiveObjectCollection.TargetItemCount := -1;
      Editor.TerrainCollection.TargetItemCount := -1;
      Editor.SteelCollection.TargetItemCount := -1;
    end else begin
      Editor.InteractiveObjectCollection.TargetItemCount := 32;
      Editor.TerrainCollection.TargetItemCount := 400;
      Editor.SteelCollection.TargetItemCount := 32;
    end;
  end;

  BgCount := 0;
  for i := 0 to G.MetaObjectCollection.Count-1 do
    if G.MetaObjectCollection[i].TriggerEffect = 32 then
      Inc(BgCount);

  if BgCount < 1 then
    BgCount := 1; // avoid negative values being ultimately written

  if Editor.Statics.BackgroundIndex >= BgCount then
    Editor.Statics.BackgroundIndex := BgCount - 1;

  finally
    Editor.EndUpdate;
  end;

//  editor.Style := stylemgr.Styles[style_dosohno];
end;

procedure TFormEditStatics.BtnSelectMusic_Click(Sender: TObject);
var
  MForm: TFMusicForm;
  function GetMusicName(aNum: Integer): String;
  begin
    if aNum = 0 then
      Result := ''
    else if (aNum >= 1) and (aNum <= 17) then
      Result := 'orig_' + LeadZeroStr(aNum, 2)
    else if (aNum >= 18) and (aNum <= 23) then
      Result := 'ohno_' + LeadZeroStr(aNum-17, 2)
    else if aNum = 24 then
      Result := 'frenzy'
    else if aNum = 25 then
      Result := 'gimmick';
  end;
begin
  MForm := TFMusicForm.Create(self, Editor.Statics.MusicTrack);
  try
    if MForm.ShowModal = mrOK then
    begin
      EditMusicFile.Text := GetMusicname(MForm.MusicNumber);
      Editor.BeginUpdate;
      try
        Editor.LevelMusicFile := GetMusicName(MForm.MusicNumber);
      finally
        Editor.EndUpdate;
      end;
    end;
  finally
    MForm.Free;
  end;
end;

{procedure TFormEditStatics.CheckBoxSuperLemming_Click(Sender: TObject);
begin
  if CheckBoxSuperLemming.Checked then Editor.Statics.SuperLemming := $FFFF;
end;  }

procedure TFormEditStatics.CheckBoxAutoSteel_Click(Sender: TObject);
begin
  Editor.BeginUpdate;
  try
  if CheckBoxAutoSteel.Checked then
    Editor.Statics.AutoSteelOptions := Editor.Statics.AutoSteelOptions or $2
    else
    Editor.Statics.AutoSteelOptions := Editor.Statics.AutoSteelOptions and (not $2);
  finally
  Editor.EndUpdate;
  end;
end;

procedure TFormEditStatics.CheckBoxIgnSteel_Click(Sender: TObject);
begin
  Editor.BeginUpdate;
  try
  if CheckBoxIgnSteel.Checked then
    Editor.Statics.AutoSteelOptions := Editor.Statics.AutoSteelOptions or $4
    else
    Editor.Statics.AutoSteelOptions := Editor.Statics.AutoSteelOptions and (not $4);
  finally
  Editor.EndUpdate;
  end;
end;

procedure TFormEditStatics.CheckBoxSimpleSteel_Click(Sender: TObject);
begin
  Editor.BeginUpdate;
  try
  if CheckBoxSimpleSteel.Checked then
    Editor.Statics.AutoSteelOptions := Editor.Statics.AutoSteelOptions or $8
    else
    Editor.Statics.AutoSteelOptions := Editor.Statics.AutoSteelOptions and (not $8);
  finally
  Editor.EndUpdate;
  end;
end;

procedure TFormEditStatics.CheckBoxOWWInvert_Click(Sender: TObject);
begin
  Editor.BeginUpdate;
  try
  if not CheckBoxOWWInvert.Checked then
    Editor.Statics.AutoSteelOptions := Editor.Statics.AutoSteelOptions or $80
    else
    Editor.Statics.AutoSteelOptions := Editor.Statics.AutoSteelOptions and (not $80);
  finally
  Editor.EndUpdate;
  end;
end;

procedure TFormEditStatics.CheckBoxTimeLimit_Click(Sender: TObject);
begin
  Editor.BeginUpdate;
  try
  Editor.Statics.HasTimeLimit := CheckBoxTimeLimit.Checked;
  finally
  Editor.EndUpdate;
  end;  
end;


procedure TFormEditStatics.CheckBoxLockRRClick(Sender: TObject);
begin
  Editor.Statics.LockedRR := CheckBoxLockRR.Checked;
end;

procedure TFormEditStatics.btnBackgroundClick(Sender: TObject);
var
  BgId: Integer;
  Graph: TBaseGraphicSet;

  function BgIdToObjId(aValue: Integer): Integer;
  var
    i: Integer;
  begin
    for i := 0 to Editor.Graph.MetaObjectCollection.Count-1 do
    begin
      if Editor.Graph.MetaObjectCollection[i].TriggerEffect <> 32 then
        Continue;

      if aValue = 0 then
      begin
        Result := i;
        Exit;
      end;

      Dec(aValue);
    end;

    Result := -1;
  end;

  function ObjIdToBgId(aValue: Integer): Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := 0 to Editor.Graph.MetaObjectCollection.Count-1 do
    begin
      if i = aValue then
      begin
        if Editor.Graph.MetaObjectCollection[i].TriggerEffect <> 32 then
          Result := -1;
        Exit;
      end;

      if Editor.Graph.MetaObjectCollection[i].TriggerEffect = 32 then
        Inc(Result);
    end;

    Result := -1;
  end;
begin
  BgId := BgIdToObjId(Editor.Statics.BackgroundIndex);
  if BgId = -1 then
    BgId := 0;
  Graph := Editor.Graph;
  if not PickListObject(Graph, BgId, false) then
    Exit;

  BgId := ObjIdToBgId(BgId);
  if BgId = -1 then
    ShowMessage('This is not a background image object.')
  else begin
    Editor.BeginUpdate;
    Editor.Statics.BackgroundIndex := BgId;
    Editor.EndUpdate;
  end;
end;

end.

