{$include lem_directives.inc}

unit LemDosStyles;

interface

uses
  Windows, Classes, IniFiles, SysUtils, Dialogs,
  utools,  UFiles, UMisc, GR32, GraphicEx, UFastStrings,
  LemDosCmp, LemDosBmp, LemDosArc, LemDosFiles, LemFiles,
  LemTypes, LemCore;

type
  TBaseDosStyle = class(TBaseLemmingStyle) // do not register!
  private
    fNil: string;
  protected
    fOddTableFile: string;
    fMainDataFile: string;
    fNeoLemmix: boolean;

    // palettes
  public
    constructor Create; override;
    class procedure GetStyleParams(var aParams: TDefaultsRec); override;
    class function GetAnimationSetClass: TBaseAnimationSetClass; override;
    procedure CreateClone(const aClonePath: string);
    property OddTableFile: string read fNil write fOddTableFile; // only used in dos orig but for less code put here
    property MainDataFile: string read fNil write fMainDataFile; // default main.dat
  published
    property NeoLemmix: Boolean read fNeoLemmix write fNeoLemmix;
    
  end;

  TDosOrigStyle = class(TBaseDosStyle)
  public
    class function GetLevelLoaderClass: TBaseLevelLoaderClass; override;
  end;

  TDosOhNoStyle = class(TBaseDosStyle)
  public
    class function GetLevelLoaderClass: TBaseLevelLoaderClass; override;
  end;

  TDosChristmas91Style = class(TBaseDosStyle)
  public
    class function GetLevelLoaderClass: TBaseLevelLoaderClass; override;
  end;

  TDosChristmas92Style = class(TBaseDosStyle)
  public
    class function GetLevelLoaderClass: TBaseLevelLoaderClass; override;
  end;

  TDosHoliday93Style = class(TBaseDosStyle)
  public
    class function GetLevelLoaderClass: TBaseLevelLoaderClass; override;
  end;

  TDosHoliday94Style = class(TBaseDosStyle)
  public
    class function GetLevelLoaderClass: TBaseLevelLoaderClass; override;
  end;

  TCustLemmStyle = class(TBaseDosStyle)
    class function GetLevelLoaderClass: TBaseLevelLoaderClass; override;
  end;



  {-------------------------------------------------------------------------------
    implementation of dos graphicsets, used by all dos classical lemmings
  -------------------------------------------------------------------------------}
  TDosGraphicSet = class(TBaseGraphicSet)
  private
    fGraphicFile     : string; // ground?.dat
    fMetaInfoFile    : string; // vgagr?.dat
    fGraphicExtFile  : string; // vgaspec?.dat
    fPaletteCustom   : TDOSGroundVGAPaletteArray8; // metadata read from vgagr
    fPaletteStandard : TDOSGroundVGAPaletteArray8; // metadata read from vgagr
    fPalettePreview  : TDOSGroundVGAPaletteArray8; // metadata read from vgagr
    fPalette         : TArrayOfColor32;
    procedure DirectReadSpec(aBitmap: TBitmap32);
    procedure DirectReadSpecPalette;
  protected
  { methods for streaming of dos-palettes }
    fExtPal          : Byte;
    fAdjTrig         : Boolean;
    fAutoSteel       : Boolean;
    fExtLoc          : Boolean;
    procedure ReadPaletteCustom(Stream: TStream);
    procedure ReadPaletteStandard(Stream: TStream);
    procedure ReadPalettePreview(Stream: TStream);
    procedure WritePaletteCustom(Stream: TStream);
    procedure WritePaletteStandard(Stream: TStream);
    procedure WritePalettePreview(Stream: TStream);
    procedure DoReadPalette(var Pal: TDosGroundVGAPaletteArray8; Stream: TStream);
    procedure DoWritePalette(const Pal: TDosGroundVGAPaletteArray8; Stream: TStream);
    procedure DefineProperties(Filer: TFiler); override;

  public
    procedure DirectReadMetaData; override;
    procedure DirectGetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32); override;
    procedure DirectGetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32); override;
    procedure DirectGetObjectMaskBitmap(aIndex: Integer; aBitmap: TBitmap32); override;
    procedure DirectGetBackGroundBitmap(aBitmap: TBitmap32); override;

    procedure DirectWriteMetaData(const aFileName: string = ''); override;

    property PaletteCustom: TDOSGroundVGAPaletteArray8 read fPaletteCustom;
    property PaletteStandard: TDOSGroundVGAPaletteArray8 read fPaletteStandard;
    property PalettePreview: TDOSGroundVGAPaletteArray8 read fPalettePreview;
    property Palette: TArrayOfColor32 read fPalette;

    property AutoSteelEnabled: Boolean read fAutoSteel write fAutoSteel;

  published
    property MetaInfoFile: string read fMetaInfoFile write fMetaInfoFile;
    property GraphicFile: string read fGraphicFile write fGraphicFile;
    property GraphicExtFile: string read fGraphicExtFile write fGraphicExtFile;
  end;

  // Neolemmix Graphic Sets

  TBaseNeoGraphicSet = class (TDosGraphicSet)
    protected
      fFile: string;

      fLoadedSections: Boolean;

      fResolution      : Byte;
      fInvalid: Boolean;

      fMetaDataStream: TMemoryStream;
      fGraphicsStream: TMemoryStream;

      procedure DirectReadMetaData; override;
      procedure DirectGetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32); override;
      procedure DirectGetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32); override;
      procedure DirectGetObjectMaskBitmap(aIndex: Integer; aBitmap: TBitmap32); override;
      procedure DirectGetBackGroundBitmap(aBitmap: TBitmap32); override;
      procedure GetSections;

      function LoadHeader(aStream: TStream): NeoLemmixHeader;
      function GetNextSection(srcStream: TStream; dstStream: TStream): Byte;
      function DecodeGraphic(aStream: TStream): TBitmap32; overload;
      function DecodeGraphic(aStream: TStream; Loc: LongWord): TBitmap32; overload;

      procedure SetGraphicFile(aValue: String);
      function GetGraphicFile: String;
    public
      constructor Create(aOwnerStyle: TBaseLemmingStyle);
      destructor Destroy;
      property Resolution: Byte read fResolution write fResolution;
      property Invalid: Boolean read fInvalid;
    published
      property GraphicSetFile: string read GetGraphicFile write SetGraphicFile;
  end;

  {-------------------------------------------------------------------------------
    Basic animationset for dos.
  -------------------------------------------------------------------------------}
  TBaseDosAnimationSet = class(TBaseAnimationSet)
  private
    fAnimationPalette: TDosGroundVGAPaletteArray8;
    procedure SetAnimationPalette(const Value: TDosGroundVGAPaletteArray8);
  protected
    procedure InternalReadMetaData; override;
    procedure InternalReadData; override;
    procedure InternalClearData; override;
  public
    constructor Create(aOwnerStyle: TBaseLemmingStyle); override;
    property AnimationPalette: TDosGroundVGAPaletteArray8 read fAnimationPalette write SetAnimationPalette;
  published
  end;

  {-------------------------------------------------------------------------------
    This record is used by the dos-game player.
    TBaseDosLevelLoader provides the mechanism.
  -------------------------------------------------------------------------------}
  TDosGamePlayInfoRec = record
    dValid         : Boolean;
    dPack          : Integer; // this is a dummy for dos
    dSection       : Integer;
    dLevel         : Integer; // zero based!
    dSectionName   : string;
  end;

  {-------------------------------------------------------------------------------
    Basic levelloader for dos.
    We virtualized it until this class was able to load all levelinfo's for
    all dos styles, overriding only 3 methods.
  -------------------------------------------------------------------------------}
  TBaseDosLevelLoader = class(TBaseLevelLoader)
  protected
  { these methods must be overridden by derived dos loaders }
    procedure GetSections(aSectionNames: TStrings); virtual;
    procedure GetEntry(aSection, aLevel: Integer; var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer); virtual;
    function GetLevelCount(aSection: Integer): Integer; virtual;
    function GetSectionCount: Integer; virtual;
  { overridden from base loader }
    procedure InternalLoadLevel(aInfo: TLevelInfo; aLevel: TLevel); override;
    procedure InternalLoadSingleLevel(aPack, aSection, aLevelIndex: Integer; aLevel: TLevel); override;
    procedure InternalPrepare; override;
    function EasyGetSectionName(aSection: Integer): string;
  public
    //For the time being it is not needed to virtualize this into a higher class.
    function FindFirstLevel(var Rec: TDosGamePlayInfoRec): Boolean;
    function FindNextLevel(var Rec : TDosGamePlayInfoRec): Boolean;
    function FindLevel(var Rec : TDosGamePlayInfoRec): Boolean;
  end;

  TDosOrigLevelLoader = class(TBaseDosLevelLoader)
  protected
    procedure GetSections(aSectionNames: TStrings); override;
    procedure GetEntry(aSection, aLevel: Integer; var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer); override;
    function GetLevelCount(aSection: Integer): Integer; override;
  end;

  TDosOhNoLevelLoader = class(TBaseDosLevelLoader)
  protected
    procedure GetSections(aSectionNames: TStrings); override;
    procedure GetEntry(aSection, aLevel: Integer; var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer); override;
    function GetLevelCount(aSection: Integer): Integer; override;
  end;

  TDosChristmas91LevelLoader = class(TBaseDosLevelLoader)
  protected
    procedure GetSections(aSectionNames: TStrings); override;
    procedure GetEntry(aSection, aLevel: Integer; var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer); override;
    function GetLevelCount(aSection: Integer): Integer; override;
  end;

  TDosChristmas92LevelLoader = class(TBaseDosLevelLoader)
  protected
    procedure GetSections(aSectionNames: TStrings); override;
    procedure GetEntry(aSection, aLevel: Integer; var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer); override;
    function GetLevelCount(aSection: Integer): Integer; override;
  end;

  TDosHoliday93LevelLoader = class(TBaseDosLevelLoader)
  protected
    procedure GetSections(aSectionNames: TStrings); override;
    procedure GetEntry(aSection, aLevel: Integer; var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer); override;
    function GetLevelCount(aSection: Integer): Integer; override;
  end;

  TDosHoliday94LevelLoader = class(TBaseDosLevelLoader)
  protected
    procedure GetSections(aSectionNames: TStrings); override;
    procedure GetEntry(aSection, aLevel: Integer; var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer); override;
    function GetLevelCount(aSection: Integer): Integer; override;
  end;

  TCustLemmLevelLoader = class(TBaseDosLevelLoader)
  protected
    procedure GetSections(aSectionNames: TStrings); override;
    procedure GetEntry(aSection, aLevel: Integer; var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer); override;
    function GetLevelCount(aSection: Integer): Integer; override;
  end;

  TDosDigitFontSide = (
    dfsLeft,
    dfsRight
  );

//  TExtractedInfo

  {-------------------------------------------------------------------------------
    The MainDatExtractor extracts data from the main.dat file.
    In this phase it is needed when creating/testing a dos original clone.
    Later this can be virtualized as graphicsets, animationssets etc.
    It only works (for sure) for dosorig now.
    Totally unprotected and not OOP! But good for testing.
  -------------------------------------------------------------------------------}
  TMainDatExtractor = class
  private
    procedure SetMainFile(const Value: string);

  protected
  public
    fPlanar: TDosPlanarBitmap;
    fArc   : TDosDatArchive;
    fMem   : TMemoryStream;
    fMainFile: string;
    fMems  : array[0..6] of TMemoryStream;

    constructor Create(const aMainFile: string);
    destructor Destroy; override;
    procedure DoExtract(aSection: Integer);
    procedure ExtractSkillPanel(Bmp: TBitmap32; const aPal: TDosGroundVGAPaletteArray8);
    procedure ExtractSkillPanelDigits(Bmp: TBitmap32);
    procedure ExtractBrownBackground(Bmp: TBitmap32);




    property Planar: TDosPlanarBitmap read fPlanar;
    property Arc   : TDosDatArchive read fArc;
    property Mem   : TMemoryStream read fMem;
    property MainFile: string read fMainFile write SetMainFile;


//    property SkillDigitBitmap[Ch: Char; Side: TDosDigitFontSide]: TBitmap32 read GetSkillDigitBitmap;

//    fSkillFont     : array['0'..'9', 0..1] of TBitmap32;

  published
  end;








  (*
  TBaseDosLevelValidator = class(TCustomLevelValidator)
  public
    // statics validation methods
    class function ValidReleaseRate(Value: Integer): Boolean; override;
    class function ValidLemmingsCount(Value: Integer): Boolean; override;
    class function ValidRescueCount(Value: Integer): Boolean; override;
    class function ValidTimeLimit(Value: Integer): Boolean; override;
    class function ValidClimberCount(Value: Integer): Boolean; override;
    class function ValidFloaterCount(Value: Integer): Boolean; override;
    class function ValidBomberCount(Value: Integer): Boolean; override;
    class function ValidBlockerCount(Value: Integer): Boolean; override;
    class function ValidBuilderCount(Value: Integer): Boolean; override;
    class function ValidBasherCount(Value: Integer): Boolean; override;
    class function ValidMinerCount(Value: Integer): Boolean; override;
    class function ValidDiggerCount(Value: Integer): Boolean; override;

  end;
  *)


implementation

uses
  LemMisc, LemGraphics;

(* DOS table for loading levels in order see naamloos.txt or lemmings forum

There is an indirection table for levels. So when on level (say 4), it might tell you to load level 15 instead.

Below is the table I have. The numbers have to be divided by 2. If the bottom bit is set,
then its a duplicate, and it gets the new skills from an "odd table", which is loaded in from "ODDTABLE.DAT".

These are levels that have new skills to make them easier.
So if the indirection table has the bottom bit set, its a duplicate level,
with overidden skills. The PD version, doesnt use an indrection table, and while
the data disk does(Oh NO!), it doesnt use the odd table.

; Fun Levels
      db      147,155,157,149,151,153,159,14,22,54
      db      70,16,29,32,38,42,48,72,84,104
      db      138,23,68,96,98,116,78,100,108,134

; Tricky Levels
      db      1,30,36,50,52,56,58,80,102,120
      db      128,130,136,5,148,152,154,156,160,7
      db      11,13,15,17,19,21,25,27,33,31

; Taxing Levels
      db      37,39,41,43,45,47,49,51,53,55
      db      57,59,61,63,3,65,67,69,71,73
      db      75,77,79,81,83,85,87,89,35,111

; Mayhem Levels
      db      91,93,95,97,99,101,103,105,107,109
      db      112,113,115,117,119,121,123,125,127,150
      db      129,9,131,133,135,137,139,141,143,145
*)
// onebased entries
// fun 8 = entry 7 oddtable 7 (one based)
// fun 1 = 147/2 = 73 (0 based level009.dat)
// fun 2 = 155/2 = 77

type

  TDosOrigLevelOrderTable = array[1..4, 1..30] of Byte;  {fun..mayhem, 1..30} // encoded indices
  TDosOhNoLevelOrderTable = array[1..5, 1..20] of Byte;  {tame..havoc, 1..20} // raw order

const
  DosOrigSectionNames: array[1..4] of string = ('fun', 'tricky', 'taxing', 'mayhem');

  SectionTable: TDosOrigLevelOrderTable = (
    (
       147, 155, 157, 149, 151, 153, 159,  14,  22,  54,
        70,  16,  29,  32,  38,  42,  48,  72,  84, 104,
       138,  23,  68,  96,  98, 116,  78, 100, 108, 134
    ),
    (
         1,  30,  36,  50,  52,  56,  58,  80, 102, 120,
       128, 130, 136,   5, 148, 152, 154, 156, 160,   7,
        11,  13,  15,  17,  19,  21,  25,  27,  33,  31
    ),
    (
       37, 39, 41, 43, 45, 47, 49, 51, 53, 55,
       57, 59, 61, 63,  3, 65, 67, 69, 71, 73,
       75, 77, 79, 81, 83, 85, 87, 89, 35,111
    ),
    (
        91,  93,  95,  97,  99, 101, 103, 105, 107, 109,
       112, 113, 115, 117, 119, 121, 123, 125, 127, 150,
       129,   9, 131, 133, 135, 137, 139, 141, 143, 145
    )
  );

  {-------------------------------------------------------------------------------
    My own little system for OhNo: div 10 = file, mod 10 is index.
    So 117 means file 11, index 7.
    How did I find out this ??? I think I just compared and compared.
    Ohno original does about the same trick based on div 8 I think.
  -------------------------------------------------------------------------------}
  OhNoTable: TDosOhNoLevelOrderTable = (
    // tame 1..8     : 10.0, 10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7
    // tame 9..16    : 11.0, 11.1, 11.2, 11.3, 11.4, 11.5, 11.6, 11.7
    // tame 17..20   : 12.0, 12.1, 12.2, 12.3 (missing in windows)
    (
       100, 101, 102, 103, 104, 105, 106, 107,
       110, 111, 112, 113, 114, 115, 116, 117,
       120, 121, 122, 123
    ),

    // crazy 1..8    : 0.1, 1.0, 1.4, 2.0, 2.1, 3.0, 3.1, 3.5 (repaired typo crazy 1: 0.0 to the correct 0.1)
    // crazy 9..16   : 5.1, 5.4, 5.7, 6.7, 0.4, 1.5, 1.6, 2.6
    // crazy 17..20  : 3.4, 3.7, 4.3, 6,4
    (
         1,  10,  14,  20,  21,  30,  31, 35,
        51,  54,  57,  67,   4,  15,  16, 26,
        34,  37,  43,  64
    ),

    // wild 1..8     : 9.2, 7.0, 7.1, 7.2, 7.3, 7.5, 8.6, 0.7
    // wild 9..16    : 2.2, 2.5, 3.3, 3.6, 4.0, 4.2, 4.4, 5.0 (repaired typo wild 15: 4.5 to the correct 4.4)
    // wild 17..20   : 6.3, 6.5, 6.6, 4.7
    (
      92,70,71,72,73,75,86,7,
      22,25,33,36,40,42,44,50,
      63,65,66,47
    ),

    // wicked 1..8   : 9.6, 4.6, 9.0, 9.1, 0.5, 9.4, 6.1, 0.6
    // wicked 9..16  : 1.2, 8.7, 4.1, 5.5, 6.0, 6.2, 7.7, 8.1,
    // wicked 17..20 : 9.7, 9.3, 8.0, 7.6
    (
      96,46,90,91,5,94,61,6,
      12,87,41,55,60,62,77,81,
      97,93,80,76
    ),

    // havoc 1..8    : 7.4, 5.3, 3.2, 2.7, 2.4, 2.3, 5.2, 1.7
    // havoc 9..16   : 1.1, 0.3, 0.2, 0.0, 4.5, 8.5, 1.3, 8.2
    // havoc 17..20  : 8.3, 5.6, 8.4, 9.5
    (
      74,53,32,27,24,23,52,17,
      11,3,2,0,45,85,13,82,
      83,56,84,95
    )
  );

(*  DosXMas93SectionNames: array[1..2] of string = ('flurry', 'blizzard');

  SectionTable: TDosOrigLevelOrderTable = (
    (
       0147, 155, 157, 149, 151, 153, 159,  14,  22,  54,
        70,  16,  29,  32,  38,  42,  48,  72,  84, 104,
       138,  23,  68,  96,  98, 116,  78, 100, 108, 134
    ),
    (
         1,  30,  36,  50,  52,  56,  58,  80, 102, 120,
       128, 130, 136,   5, 148, 152, 154, 156, 160,   7,
        11,  13,  15,  17,  19,  21,  25,  27,  33,  31
    ),
    (
       37, 39, 41, 43, 45, 47, 49, 51, 53, 55,
       57, 59, 61, 63,  3, 65, 67, 69, 71, 73,
       75, 77, 79, 81, 83, 85, 87, 89, 35,111
    ),
    (
        91,  93,  95,  97,  99, 101, 103, 105, 107, 109,
       112, 113, 115, 117, 119, 121, 123, 125, 127, 150,
       129,   9, 131, 133, 135, 137, 139, 141, 143, 145
    )
  );    *)


// NeoLemmix Graphic Sets

constructor TBaseNeoGraphicSet.Create(aOwnerStyle: TBaseLemmingStyle);
begin
  inherited;
  fMetaDataStream := TMemoryStream.Create;
  fGraphicsStream := TMemoryStream.Create;

  fAutoSteel := true; //NeoLemmix graphic sets ALWAYS support autosteel
  fGraphicSetName := ChangeFileExt(ExtractFileName(fFile), '');
  fLoadedSections := false;
  Resolution := 8; //backwards compatibility
  fInvalid := false;
end;

destructor TBaseNeoGraphicSet.Destroy;
begin
  fMetaDataStream.Destroy;
  fGraphicsStream.Destroy;
  inherited Destroy;
end;

function TBaseNeoGraphicSet.GetGraphicFile: String;
begin
  Result := fOwnerStyle.IncludeCommonPath(fFile);
end;

procedure TBaseNeoGraphicSet.SetGraphicFile(aValue: String);
begin
  fLoadedSections := false;
  fFile := aValue;
end;

procedure TBaseNeoGraphicSet.GetSections;
var
  DataStream: TMemoryStream;
  Decompressor: TDosDatDecompressor;
  Fail: Boolean;
begin
  if fLoadedSections then Exit;
  fLoadedSections := true;

  Fail := false;

  Decompressor := TDosDatDecompressor.Create;

    DataStream := TMemoryStream.Create;
    DataStream.LoadFromFile(GraphicSetFile);

  fMetaDataStream.Clear;
  fGraphicsStream.Clear;

  try
    fMetaDataStream.Seek(0, soFromBeginning);
    fGraphicsStream.Seek(0, soFromBeginning);

    Decompressor.DecompressSection(DataStream, fMetaDataStream);
    if DataStream.Position >= DataStream.Size then fInvalid := true;
    Decompressor.DecompressSection(DataStream, fGraphicsStream);
  finally
    DataStream.Free;
    Decompressor.Free;
  end;
end;

procedure TBaseNeoGraphicSet.DirectReadMetaData;
var
  gsHeader: NeoLemmixHeader;
  gsTerrain: NeoLemmixTerrainData;
  gsObject: NeoLemmixObjectData;
  TempBMP: TBitmap32;

  TempTerrain: TMetaTerrain;
  TempObject: TMetaObject;

  TempStream: TMemoryStream;

  i: Integer;
  b: Byte;
  lw: LongWord;
begin

  if GraphicSetFile = '' then
  begin
    inherited;
    exit;
  end;

  GetSections;
  gsHeader := LoadHeader(fMetaDataStream);
  fResolution := gsHeader.Resolution;

  SetLength(fPalette, 32);

  fPalette[0] := $FF000000;
  fPalette[1] := $FF4040E0;
  fPalette[2] := $FF00B000;
  fPalette[3] := $FFF0D0D0;
  fPalette[4] := $FFB0B000;
  fPalette[5] := $FFF02020;
  fPalette[6] := $FF808080;
  fPalette[7] := $FFFFFFFF;
  if gsHeader.IsUpdated <> 1 then
  begin
    fPalette[8] := gsHeader.KeyColors[0].ARGB or $FF000000;
    fPalette[9] := fPalette[8];
    for i := 10 to 15 do
      fPalette[i] := $FF000000;
    fBackgroundColor := $FF000000;
  end else begin
    for i := 8 to 15 do
      fPalette[i] := gsHeader.KeyColors[i-8].ARGB or $FF000000;
    fBackgroundColor := fPalette[10];
  end;
  fPalette[7] := fPalette[8];

  for i := 16 to 31 do
    fPalette[i] := fPalette[i-16];

  // Unlike in the engine, we need to clear existing data here
  fMetaTerrainCollection.Clear;
  fMetaObjectCollection.Clear;

  // Now load the pieces
  TempStream := TMemoryStream.Create;
  fMetaDataStream.Seek(0, soFromBeginning);
  try
    TempStream.Seek(0, soFromBeginning);
    b := GetNextSection(fMetaDataStream, TempStream);
    while b <> dtEof do
    begin
      TempStream.Seek(0, soFromBeginning);
      case b of
        dtTerrain: begin
                     TempStream.Read(gsTerrain, SizeOf(gsTerrain));
                     TempTerrain := fMetaTerrainCollection.Add;
                     TempTerrain.ImageLocation := gsTerrain.BaseLoc;
                     if (gsTerrain.TerrainFlags and 1) <> 0 then
                       TempTerrain.Unknown := 1
                     else
                       TempTerrain.Unknown := 0;
                   end;
        dtObject:  begin
                     TempStream.Read(gsObject, SizeOf(gsObject));
                     TempObject := fMetaObjectCollection.Add;
                     if gsObject.TriggerEff = 23 then
                     begin
                       TempObject.AnimationFlags := 3;
                       TempObject.StartAnimationFrameIndex := 1;
                     end else if (gsObject.ObjectFlags and 1) <> 0 then
                       TempObject.AnimationFlags := 1
                     else
                       TempObject.AnimationFlags := 2;
                     TempObject.EndAnimationFrameIndex := gsObject.FrameCount;
                     Tempobject.AnimationFramesBaseLoc := gsObject.BaseLoc;
                     TempObject.PreviewFrameIndex := gsObject.PreviewFrame;
                     TempObject.TriggerNext := gsObject.KeyFrame;
                     TempObject.TriggerEffect := gsObject.TriggerEff;
                     TempObject.TriggerLeft := (gsObject.PTriggerX * 8) div fResolution;
                     TempObject.TriggerTop := (gsObject.PTriggerY * 8) div fResolution;
                     TempObject.TriggerWidth := (gsObject.PTriggerW * 8) div fResolution;
                     TempObject.TriggerHeight := (gsObject.PTriggerH * 8) div fResolution;
                     {TempObject.TriggerPointX := gsObject.STriggerX;
                     TempObject.TriggerPointY := gsObject.STriggerY;
                     TempObject.TriggerPointW := gsObject.STriggerW;
                     TempObject.TriggerPointH := gsObject.STriggerH;}
                     TempObject.TrapSoundEffect := gsObject.TriggerSound;
                   end;
        dtSound: begin
                   TempStream.Position := TempStream.Position + 5;
                 end;
		// NEED TO ADD (AND HANDLE) dtLemming! //
      end;
      TempStream.Seek(0, soFromBeginning);
      b := GetNextSection(fMetaDataStream, TempStream);
    end;
  finally
    TempStream.Free;
  end;

  if IsSpecial then
  begin
    GraphicSetId := 0;
    GraphicSetIdExt := 255;
    GraphicFile := '';
    MetaInfoFile := '';
  end else begin
    GraphicSetId := 255;
    GraphicSetIdExt := 0;
    GraphicExtFile := '';
  end;
end;

{procedure TBaseNeoGraphicSet.DoReadData;
var
  i, i2: Integer;
  TempBMP: TBitmap32;
  TempBmps: TBitmaps;
  mw, mh: Integer;
begin

  if GraphicSetFile = '' then
  begin
    inherited;
    exit;
  end;

  GetSections;

  //Terrain
  for i := 0 to MetaTerrains.Count-1 do
  begin
    TempBMP := DecodeGraphic(fGraphicsStream, MetaTerrains[i].ImageLocation);
    TerrainBitmaps.Add(TempBMP);
    MetaTerrains[i].Width := TempBMP.Width;
    MetaTerrains[i].Height := TempBMP.Height;
  end;

  //Objects
  for i := 0 to MetaObjects.Count-1 do
  begin
    TempBmps := TBitmaps.Create;
    mw := 0;
    mh := 0;
    for i2 := 0 to MetaObjects[i].AnimationFrameCount-1 do
    begin
      if i2 = 0 then
        TempBMP := DecodeGraphic(fGraphicsStream, MetaObjects[i].AnimationFramesBaseLoc)
      else
        TempBMP := DecodeGraphic(fGraphicsStream);
      if TempBMP.Width > mw then mw := TempBMP.Width;
      if TempBMP.Height > mh then mh := TempBMP.Height;
      TempBmps.Add(TempBMP);
    end;
    MetaObjects[i].Width := mw;
    MetaObjects[i].Height := mh;
    TempBMP := TBitmap32.Create;
    TempBMP.SetSize(mw, mh*MetaObjects[i].AnimationFrameCount);
    for i2 := 0 to TempBmps.Count-1 do
      TempBmps[i2].DrawTo(TempBMP, 0, mh*i2);
    TempBmps.Free;
    ObjectBitmaps.Add(TempBmp);
  end;

  //Vgaspec - directly copied from LemDosGraphicSet
  LoadVgaspec;

end;}

{procedure TBaseNeoGraphicSet.LoadVgaspec;
var
  SpecBmp: TVgaSpecBitmap;
  DataStream: TMemoryStream;
begin
  if GraphicSetIdExt <> 0 then
      begin
        SpecBmp := TVgaSpecBitmap.Create;
        try
          if FileExists(GraphicExtFile) then
          begin
            DataStream := TMemoryStream.Create;
            DataStream.LoadFromFile(GraphicExtFile);
          end else
          DataStream := CreateDataStream(GraphicExtFile, ldtLemmings);
          if fNeoEncrypt.CheckEncrypted(DataStream) then
            fNeoEncrypt.LoadStream(DataStream);
          try
            SpecBmp.LoadFromStream(DataStream, SpecialBitmap);
          finally
            DataStream.Free;
          end;
        finally
          SpecBmp.Free;
        end;
      end;
end;}


procedure TBaseNeoGraphicSet.DirectGetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32);
var
  TempBMP: TBitmap32;
begin
  //if aBitmap <> nil then aBitmap.Free;
  TempBMP := DecodeGraphic(fGraphicsStream, MetaTerrainCollection[aIndex].ImageLocation);
  aBitmap.Assign(TempBMP);
  TempBMP.Free;
  MetaTerrainCollection[aIndex].Width := aBitmap.Width;
  MetaTerrainCollection[aIndex].Height := aBitmap.Height;
end;

procedure TBaseNeoGraphicSet.DirectGetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32);
var
  TempBMP, TempBMP2: TBitmap32;
  mw, mh: Integer;
  i2: Integer;

  procedure AddFencerPickup;
  var
    FencerImg: TBitmap32;
    DrawColors: array[0..4] of Integer;
    x, y: Integer;
  const
    FENCER_IMAGE: array[0..99] of Integer =
            ( 5, 5, 5, 5, 0, 2, 5, 5, 5, 5,
              5, 5, 0, 0, 2, 2, 1, 0, 5, 5,
              5, 0, 0, 0, 2, 1, 1, 1, 0, 5,
              5, 0, 0, 0, 1, 1, 3, 0, 0, 5,
              0, 0, 0, 1, 3, 3, 4, 0, 0, 0,
              0, 0, 0, 1, 1, 1, 4, 4, 4, 4,
              5, 0, 0, 3, 3, 3, 4, 0, 0, 5,
              5, 0, 3, 3, 0, 3, 0, 0, 0, 5,
              5, 5, 1, 0, 0, 1, 1, 0, 5, 5,
              5, 5, 5, 5, 0, 0, 5, 5, 5, 5
            ); // furthest outside rows / colums not included as no change needed on them
  begin
    // It is assumed that the standard pickup graphic is used.
    // Any that don't use this graphic will need to be done manually. But it's been recommended against using these anyway.
    DrawColors[0] := TempBmp.PixelS[5, 13]; // background
    DrawColors[1] := TempBmp.PixelS[5, 14]; // lemming skin
    DrawColors[2] := TempBmp.PixelS[3, 15]; // lemming hair
    DrawColors[3] := TempBmp.PixelS[7, 17]; // lemming body
    DrawColors[4] := TempBmp.PixelS[5, 1]; // gray

    TempBmp2 := TBitmap32.Create;
    FencerImg := TBitmap32.Create;
    FencerImg.SetSize(TempBmp.Width, 12);
    FencerImg.Clear(0);
    TempBmp.DrawTo(FencerImg, FencerImg.BoundsRect, FencerImg.BoundsRect);
    MetaObjectCollection[aIndex].EndAnimationFrameIndex := MetaObjectCollection[aIndex].EndAnimationFrameIndex + 1;

    for y := 0 to 9 do
      for x := 0 to 9 do
        if FENCER_IMAGE[x + (y * 10)] = 5 then
          Continue
        else
          FencerImg.PixelS[x+1, y+1] := DrawColors[FENCER_IMAGE[x + (y * 10)]];

    TempBmp2.SetSize(TempBmp.Width, TempBmp.Height + 12);
    TempBmp2.Clear(0);
    TempBmp.DrawTo(TempBmp2);
    FencerImg.DrawTo(TempBmp2, 0, TempBmp.Height);
    TempBmp.Assign(TempBmp2);
    FencerImg.Free;
    TempBmp2.Free;
  end;
begin
  //if aBitmap <> nil then aBitmap.Free;
    mw := 0;
    mh := 0;
    for i2 := 0 to MetaObjectCollection[aIndex].EndAnimationFrameIndex-1 do
    begin
      if i2 = 0 then
        TempBMP2 := DecodeGraphic(fGraphicsStream, MetaObjectCollection[aIndex].AnimationFramesBaseLoc)
      else
        TempBMP2 := DecodeGraphic(fGraphicsStream);
      if TempBMP2.Width > mw then mw := TempBMP2.Width;
      if TempBMP2.Height > mh then mh := TempBMP2.Height;
      TempBMP2.Free;
    end; // because this format loads very quickly anyway, and I'm too lazy to write a function to *just* get the size
    MetaObjectCollection[aIndex].Width := mw;
    MetaObjectCollection[aIndex].Height := mh;
    TempBMP := TBitmap32.Create;
    TempBMP.SetSize(mw, mh*MetaObjectCollection[aIndex].EndAnimationFrameIndex);
    TempBMP.Clear(0);
    for i2 := 0 to MetaObjectCollection[aIndex].EndAnimationFrameIndex-1 do
    begin
      if i2 = 0 then
        TempBMP2 := DecodeGraphic(fGraphicsStream, MetaObjectCollection[aIndex].AnimationFramesBaseLoc)
      else
        TempBMP2 := DecodeGraphic(fGraphicsStream);
      TempBmp2.DrawTo(TempBMP, 0, mh*i2);
      TempBmp2.Free;
    end;
    if (MetaObjectCollection[aIndex].TriggerEffect = 14) and (MetaObjectCollection[aIndex].EndAnimationFrameIndex = 17) then
      AddFencerPickup;
  aBitmap.Assign(TempBmp);
  TempBmp.Free;
end;

procedure TBaseNeoGraphicSet.DirectGetObjectMaskBitmap(aIndex: Integer; aBitmap: TBitmap32);
var
  x, y: Integer;
begin
  DirectGetObjectBitmap(aIndex, aBitmap);
  for y := 0 to aBitmap.Height-1 do
    for x := 0 to aBitmap.Width-1 do
      if aBitmap.Pixel[x, y] and $FF000000 <> $FF000000 then aBitmap.Pixel[x, y] := 0;
end;

procedure TBaseNeoGraphicSet.DirectGetBackGroundBitmap(aBitmap: TBitmap32);
begin
  DirectGetTerrainBitmap(0, aBitmap);
end;

function TBaseNeoGraphicSet.LoadHeader(aStream: TStream): NeoLemmixHeader;
var
  b: Byte;
  TempStream: TMemoryStream;
begin
  aStream.Seek(0, soFromBeginning);
  TempStream := TMemoryStream.Create;
  repeat
    TempStream.Seek(0, soFromBeginning);
    b := GetNextSection(aStream, TempStream);
  until b in [dtHeader, dtEof];
  TempStream.Seek(0, soFromBeginning);
  if b = dtHeader then TempStream.Read(Result, SizeOf(Result));
  TempStream.Free;
end;

function TBaseNeoGraphicSet.GetNextSection(srcStream: TStream; dstStream: TStream): Byte;
var
  b: Byte;
  i: Integer;
  p: Integer;
begin
  b := 0;
  i := 0;
  while b <> $FF do
    srcStream.Read(b, 1);
  srcStream.Read(Result, 1);
  case Result of
    dtEof: Exit;
    dtComment: begin
                 p := srcStream.Position;
                 srcStream.Read(b, 1);
                 while b <> $FF do
                 begin
                   Inc(i);
                   srcStream.Read(b, 1);
                 end;
                 srcStream.Position := p;
               end;
    dtHeader: i := SizeOf(NeoLemmixHeader);
    dtObject: i := SizeOf(NeoLemmixObjectData);
    dtTerrain: i := SizeOf(NeoLemmixTerrainData);
    dtSound: i := 5; //not used by editor so no point defining a structure
  end;
  while i > 0 do
  begin
    srcStream.Read(b, 1);
    dstStream.Write(b, 1);
    i := i - 1;
  end;
end;

function TBaseNeoGraphicSet.DecodeGraphic(aStream: TStream; Loc: LongWord): TBitmap32;
begin
  aStream.Seek(Loc, soFromBeginning);
  Result := DecodeGraphic(aStream);
end;

function TBaseNeoGraphicSet.DecodeGraphic(aStream: TStream): TBitmap32;
var
  x, y, W, H: LongWord;
  a, r, g, b: Byte;
  TempBMP: TBitmap32;
begin
  Result := TBitmap32.Create;
  aStream.Read(W, 4);
  aStream.Read(H, 4);
  Result.SetSize(W, H);
  for y := 0 to H-1 do
    for x := 0 to W-1 do
    begin
      aStream.Read(a, 1);
      if a = 0 then
        Result.Pixel[x, y] := 0
      else begin
        aStream.Read(r, 1);
        aStream.Read(g, 1);
        aStream.Read(b, 1);
        Result.Pixel[x, y] := (a shl 24) + (r shl 16) + (g shl 8) + b;
      end;
    end;

  if Resolution = 8 then Exit;

  TempBMP := TBitmap32.Create;
  TempBMP.SetSize((W * 8) div Resolution, (H * 8) div Resolution);
  TempBMP.Clear(0);
  Result.DrawTo(TempBMP, TempBMP.BoundsRect);
  Result.Free;
  Result := TempBMP;
end;

{ TBaseDosStyle }

constructor TBaseDosStyle.Create;
begin
  inherited;
  fMainDataFile := '';
  fNil := '';
end;

procedure TBaseDosStyle.CreateClone(const aClonePath: string);
var
  L: TStringList;
  P: string;
begin
  P := IncludeTrailingBackslash(aClonePath);
  if CompareText(P, CommonPath) = 0 then
    Exit;
  L := TStringList.Create;
  try
    CreateFileList(L, CommonPath + '*.*', faAllFiles and not faDirectory, False);
  finally
    L.Free;
  end;
end;

class function TBaseDosStyle.GetAnimationSetClass: TBaseAnimationSetClass;
begin
  Result := TBaseDosAnimationSet;
end;

class procedure TBaseDosStyle.GetStyleParams(var aParams: TDefaultsRec);
begin
  with aParams do
  begin
    Resolution   := 1.0;
    LevelWidth   := 1584;
    LevelHeight  := 160;
    ViewWidth    := 320;
    ViewHeight   := 160;
    MultiPacks   := False;
  end;
end;

{ TDosOrigStyle }

class function TDosOrigStyle.GetLevelLoaderClass: TBaseLevelLoaderClass;
begin
  Result := TDosOrigLevelLoader;
end;

{ TDosOhNoStyle }

class function TDosOhNoStyle.GetLevelLoaderClass: TBaseLevelLoaderClass;
begin
  Result := TDosOhNoLevelLoader;
end;

{ TDosChristmas91Style }

class function TDosChristmas91Style.GetLevelLoaderClass: TBaseLevelLoaderClass;
begin
  Result := TDosChristmas91LevelLoader;
end;
{ TDosChristmas92Style }

class function TDosChristmas92Style.GetLevelLoaderClass: TBaseLevelLoaderClass;
begin
  Result := TDosChristmas92LevelLoader;
end;

{ TDosHoliday93Style }

class function TDosHoliday93Style.GetLevelLoaderClass: TBaseLevelLoaderClass;
begin
  Result := TDosHoliday93LevelLoader;
end;

{ TDosHoliday94Style }

class function TDosHoliday94Style.GetLevelLoaderClass: TBaseLevelLoaderClass;
begin
  Result := TDosHoliday94LevelLoader;
end;

{ TCustLemmStyle }

class function TCustLemmStyle.GetLevelLoaderClass: TBaseLevelLoaderClass;
begin
  Result := TCustLemmLevelLoader;
end;

{ TDosGraphicSet }

procedure TDosGraphicSet.DirectGetObjectBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  We need a few tools to extract bitmaps from dos-files.
  o Extract the second section (objects) from the vgagr?.dat with the decompressor
  o Extract from the imagelocation all planar bitmaps and convert this to tbitmap32
  o Paste the bitmaps into aBitmap
  o See documents for detailed info
-------------------------------------------------------------------------------}
var
  Arc: TDosDatArchive;
  Mem: TMemoryStream;
  PlanarBitmap: TDosPlanarBitmap;
  Bmp: TBitmap32;
  O: TMetaObject;
  Loc, Y, i: Integer;
  {$ifdef develop}
  S: TStringList;
  {$endif}
begin
//  if GraphicFile = '' then

  EnsureMetaData;
  Arc := nil;
  Mem := nil;
  PlanarBitmap := nil;
  Bmp := nil;

  {$ifdef develop}
  S := TStringList.Create;
  {$endif}

  try
    Bmp := TBitmap32.Create;
    Arc := TDosDatArchive.Create;
    Mem := TMemoryStream.Create;
    PlanarBitmap := TDosPlanarBitmap.Create;
    O := MetaObjectCollection[aIndex];
    aBitmap.SetSize(O.Width, O.Height * O.EndAnimationFrameIndex);
    Arc.LoadFromFile(IncludeCommonStylePath(GraphicFile));
    Arc.ExtractSection(1, Mem);

    Y := 0;
    Loc := O.AnimationFramesBaseLoc;
    for i := 0 to O.EndAnimationFrameIndex - 1 do
    begin
      PlanarBitmap.LoadFromStream(Mem, Bmp, Loc, O.Width, O.Height, 4 + fExtPal,
        fPalette
        {fPaletteStandard, fPaletteCustom}
        {$ifdef develop},S {$endif} );
      if (4+fExtPal) <> 19 then
        PlanarBitmap.MaskFromStream(Mem, Bmp, Loc + O.MaskOffsetFromImage, O.Width, O.Height);

      {$ifdef develop}
      S.SaveToFile(AppPath + 'temp\' + 'obj_' + i2s(aIndex) + '_' + i2s(i) + '.txt');
      {$endif}

      Bmp.DrawTo(aBitmap, 0, Y);
      //Bmp.SaveToFile(apppath + 'temp\' + 'obj_' + LeadZeroStr(aIndex, 2) + '_' + LeadZeroStr(i, 2) + '.bmp');
      Inc(Y, O.Height);
      Inc(Loc, O.AnimationFrameDataSize);
    end;

  finally
    Arc.Free;
    Mem.Free;
    PlanarBitmap.Free;
    Bmp.Free;
    {$ifdef develop}
    S.Free;
    {$endif}
  end;
end;

procedure TDosGraphicSet.DirectGetObjectMaskBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  A bit the same as extracting the object bitmap
  o Extract the second section (objects) from the vgagr?.dat with the decompressor
  o Extract from the imagemasklocation the planar mask bitmap (1 bpp)
  o See documents for detailed info


  for every frame there is a mask (at "delta" maskoffset)
  it is a on/off bitmap of the frame.
  for now we let this and come back to it later
-------------------------------------------------------------------------------}
var
  Arc: TDosDatArchive;
  Mem: TMemoryStream;
  PlanarBitmap: TDosPlanarBitmap;
  O: TMetaObject;
  Loc, Y, i: Integer;
  Pal: TDOSGroundVGAPaletteArray8;
begin
//  if GraphicFile = '' then

  EnsureMetaData;

  Pal := fPaletteCustom;
  with Pal[0] do
  begin
    R := 0;
    G := 0;
    B := 0;
  end;
  with Pal[1] do
  begin
    R := 63;
    G := 63;
    B := 63;
  end;

  Arc := nil;
  Mem := nil;
  PlanarBitmap := nil;

  try
    Arc := TDosDatArchive.Create;
    Mem := TMemoryStream.Create;
    PlanarBitmap := TDosPlanarBitmap.Create;
    O := MetaObjectCollection[aIndex];
    aBitmap.SetSize(O.Width, O.Height * O.EndAnimationFrameIndex);
    Arc.LoadFromFile(IncludeCommonStylePath(GraphicFile));
    Arc.ExtractSection(1, Mem);
    Loc := O.AnimationFramesBaseLoc +
//      O.EndAnimationFrameIndex *
//O.AnimationFrameDataSize //+//grumbo!
      O.MaskOffsetFromImage;
//deb(['o framesize', o.animationframedatasize]);
    PlanarBitmap.LoadFromStream(Mem, aBitmap, Loc, O.Width, O.Height, 1,
      Pal, fPaletteCustom);

    ShowImageForm(aBitmap, 1);

  finally
    Arc.Free;
    Mem.Free;
    PlanarBitmap.Free;
  end;
end;

procedure TDosGraphicSet.DirectGetBackGroundBitmap(aBitmap: TBitmap32);
begin
  DirectReadSpec(aBitmap);
end;

procedure TDosGraphicSet.DirectGetTerrainBitmap(aIndex: Integer; aBitmap: TBitmap32);
{-------------------------------------------------------------------------------
  We need a few tools to extract bitmaps from dos-files.
  o Extract the first section (terrains) from the vgagr?.dat with the decompressor
  o Extract from the imagelocation a planar bitmap and convert this to tbitmap32
  o See documents for detailed info
-------------------------------------------------------------------------------}
var
  Arc: TDosDatArchive;
  Mem: TMemoryStream;
  PlanarBitmap: TDosPlanarBitmap;
  T: TMetaTerrain;
  {$ifdef dos_savebitmaps_as_text_when_loading}
  Txt: TStringList;
  {$endif}
begin
  {$ifdef dos_savebitmaps_as_text_when_loading}
  Txt := TStringList.Create;
  {$endif}
  Arc := nil;
  Mem := nil;
  PlanarBitmap := nil;
  EnsureMetaData;
  try
    Arc := TDosDatArchive.Create;
    Mem := TMemoryStream.Create;
    PlanarBitmap := TDosPlanarBitmap.Create;
    T := MetaTerrainCollection[aIndex];
    Arc.LoadFromFile(IncludeCommonStylePath(GraphicFile));
    Arc.ExtractSection(0, Mem);
    PlanarBitmap.LoadFromStream(Mem, aBitmap, T.ImageLocation, T.Width, T.Height, 4 + fExtPal,
      fPalette{Standard, fPaletteCustom});
    if (4+fExtPal) <> 19 then
      PlanarBitmap.MaskFromStream(Mem, aBitmap, T.MaskLocation, T.Width, T.Height);

    {$ifdef dos_savebitmaps_as_text_when_loading}
    PlanarBitmap.LoadFromStream(Mem, aBitmap, T.ImageLocation, T.Width, T.Height, 4,
      fPaletteStandard, fPaletteCustom, Txt);
    Txt.SaveToFile(AppPath + 'Temp\' + 'ter_' + LeadZeroStr(aIndex, 3) + '.txt');
    {$else}
    //PlanarBitmap.LoadFromStream(Mem, aBitmap, T.ImageLocation, T.Width, T.Height, 4,
      //fPaletteStandard, fPaletteCustom);
    {$endif}

  finally
    Arc.Free;
    Mem.Free;
    PlanarBitmap.Free;
    {$ifdef dos_savebitmaps_as_text_when_loading}
    Txt.Free;
    {$endif}
  end;
end;

procedure TDosGraphicSet.DirectReadSpec(aBitmap: TBitmap32);
var
  Spec: TVgaSpecBitmap;
begin
  Spec := TVgaSpecBitmap.Create;
  Spec.LoadFromFile(IncludeCommonStylePath(GraphicExtFile), aBitmap);
  Spec.Free;
end;

procedure TDosGraphicSet.DirectReadSpecPalette;
var
  Spec: TVgaSpecBitmap;
begin
  Spec := TVgaSpecBitmap.Create;
  Spec.LoadPaletteFromFile(IncludeCommonStylePath(GraphicExtFile), fPaletteCustom);
  Spec.Free;
end;


procedure TDosGraphicSet.DirectReadMetaData;
{-------------------------------------------------------------------------------
  Read metadata from the ground??.dat file.

  N.B: If extended graphicset then:
    o read palette from vgaspec?.dat file
    o (for now) read objects as always, which probably means the dirt-objects (zero index)
    o don't read terrains
-------------------------------------------------------------------------------}
var
  G: TDosGround;
  i: Integer;
  O: PDOSGroundObject;
  MO: TMetaObject;
  T: PDOSGroundTerrain;
  MT: TMetaTerrain;

    procedure logpal;
    var
      i: Integer;
    begin
      for i := 0 to 7 do
        with fpalettecustom[i] do
          deb([r,g,b]);
    end;

    procedure AssemblePalette;
    {-------------------------------------------------------------------------------
      Concatenate the fixed palette and the loaded custompalette.
      Then copy the first customcolor to the last fixed color (bridges, minimap).
      For special graphics we use a hardcoded color for now.
    -------------------------------------------------------------------------------}
    var
      i: Integer;
    begin
      SetLength(fPalette, 32);
      for i := 0 to 7 do
        fPalette[i] := DosPaletteEntryToColor32(DosInlevelPalette[i]);
      for i := 8 to 15 do
        fPalette[i] := DosPaletteEntryToColor32(fPaletteCustom[i - 8]);
      for i := 16 to 23 do
        fPalette[i] := DosPaletteEntryToColor32(fPaletteStandard[i - 16]);
      for i := 24 to 31 do
        fPalette[i] := DosPaletteEntryToColor32(fPalettePreview[i - 24]);
      if GraphicSetIdExt > 0 then
        fPalette[8] := Color32(124, 124, 0, 0);
      fPalette[7] := fPalette[8];
      //fBrickColor := fPalette[7];
    end;

var
  sc:  TDosVgaColorRec;
  Fn: string;
  TempColor : TColor32;
begin
  if IsSpecial then exit;
  G := TDosGround.Create;
  try
    //if not F
    G.LoadFromFile(IncludeCommonStylePath(fMetaInfoFile));
    with G.GroundRec do
    begin

      // save palettes voor direct access bitmap creating
      fPaletteCustom   := VGA_PaletteCustom;
      fPaletteStandard := VGA_PaletteStandard;
      fPalettePreview  := VGA_PalettePreview;

//      logpal;

      //BrickColor := DosPaletteEntryToColor32(fPaletteCustom[0]);

      if fGraphicSetIdExt > 0 then
      begin
        //sc := fPaletteCustom[7];
        DirectReadSpecPalette;
        //fPaletteCustom[0] := sc;
  //      logpal;
      end;

      AssemblePalette;

      // objects
      for i := 0 to 31 do
      begin
        if i < 16 then
          O := @ObjectInfoArray[i]
          else
          O := @G.GroundRec2.ObjectInfoArray[i-16];
//        deb([ObjectInfoArray[i].owidth]);
        if O^.oWidth = 0 then
          Break;
        MO := MetaObjectCollection.Add;
        with MO do
        begin
          if i = 0 then
          begin
           //check extended properties
           if ((O^.oAnimation_flags and $0100) <> 0) then fExtPal := 1 else fExtPal := 0;
           if ((O^.oAnimation_flags and $0200) <> 0) then fAdjTrig := true else fAdjTrig := false;
           if ((O^.oAnimation_flags and $0400) <> 0) then fAutoSteel := true else fAutoSteel := false;
           if ((O^.oAnimation_flags and $0800) <> 0) then
           begin
             TempColor := fPalette[1];
             fPalette[1] := fPalette[5];
             fPalette[5] := TempColor;
           end;
           if ((O^.oAnimation_flags and $2000) <> 0) then fExtLoc := true else fExtLoc := false;
           if ((O^.oAnimation_flags and $4000) <> 0) then fExtPal := 15;
           O^.oAnimation_flags := O^.oAnimation_flags and 3
          end;
        AnimationFlags            := O^.oAnimation_flags;
        StartAnimationFrameIndex := O^.oStart_animation_frame_index;
        EndAnimationFrameIndex   := O^.oEnd_animation_frame_index;
        AnimationFrameDataSize   := O^.oAnimation_frame_data_size;
        MaskOffsetFromImage      := O^.oMask_offset_from_image;
        Width                    := O^.oWidth;
        Height                   := O^.oHeight;
        TriggerLeft              := O^.oTrigger_left * 4; // encoded
        TriggerTop               := O^.oTrigger_top * 4 - 4; // encoded
        TriggerWidth             := O^.oTrigger_width * 4; // encoded
        TriggerHeight            := O^.oTrigger_height * 4; // encoded

        if fAdjTrig then
          begin
           TriggerLeft := TriggerLeft - ((O^.oUnknown3 shr 6) mod 4);
           TriggerTop := TriggerTop - ((O^.oUnknown3 shr 4) mod 4);
           TriggerWidth := TriggerWidth - ((O^.oUnknown3 shr 2) mod 4);
           TriggerHeight := TriggerHeight - ((O^.oUnknown3) mod 4);
           TriggerNext := (O^.oUnknown3 shr 8) mod 32;
          end;



        TriggerEffect            := O^.oTrigger_effect_id;
        AnimationFramesBaseLoc   := O^.oAnimation_frames_base_loc;
        if fExtLoc then AnimationFramesBaseLoc := AnimationFramesBaseLoc + (O^.oUnknown1 shl 16);
        PreviewFrameIndex        :=
          (O^.oPreview_image_location - O^.oAnimation_frames_base_loc) div O^.oAnimation_frame_data_size;
        TrapSoundEffect              := O^.oTrap_sound_effect_id;
        end;
      end;

      // terrains
      if fGraphicSetIdExt = 0 then
      begin
        for i := 0 to 127 do
        begin
          if i < 64 then
            T := @TerrainInfoArray[i]
            else
            T := @G.GroundRec2.TerrainInfoArray[i-64];
          if T^.tWidth = 0 then
            Break;
          MT := MetaTerrainCollection.Add;
          with MT do
          begin
            Width := T^.tWidth;
            Height := T^.tHeight;
            ImageLocation := T.tImage_loc;
            MaskLocation := T.tMask_loc;
            if fExtLoc then
            begin
              ImageLocation := ImageLocation + (((T.tUnknown1 mod 256) shr 1) shl 16);
              MaskLocation := MaskLocation + ((T.tUnknown1 div 256) shl 16);
            end;
            Unknown := T.tUnknown1;
          end;
        end;
      end
      // special graphics
      else begin
        (*
        MT := MetaTerrainCollection.Add;
        with MT do
        begin
          Width := 960;//T^.tWidth;
          Height := 160;//T^.tHeight;
          ImageLocation := 0;//T.tImage_loc;
        end;
        *)
      end;

    end;
  finally
    G.Free;
  end;
end;

procedure TDosGraphicSet.DirectWriteMetaData(const aFileName: string);
{-------------------------------------------------------------------------------
  Write metadata to ground?.dat and vgagr?.dat
-------------------------------------------------------------------------------}
var
  i, iFrame: Integer;
  Bmp: TBitmap32;
  dummy:tpalette32;
  testcolors:integer;
  FrameBmp: TBitmap32;
  Planar: TDosPlanarBitmap;
  Mem: TMemoryStream;
  CmpStream: TFileStream;
  Compressor: TDosDatCompressor;
  Pal32: TColor32Array8;
  DosPal: TDOSGroundVGAPaletteArray8;
  DosPal16: TDOSGroundVGAPaletteArray16;
  ColorsUsed: Integer;
  MO: TMetaObject;
  ColorList: TIntegerList;
  OldP: Integer;
begin
  // check graphicsetid
  // write bitmaps
  // for obects, also write the mask
  EnsureMetaData;

  // create file + objects
  CmpStream := TFileStream.Create(AppPath + 'vgagrE.dat', fmCreate);
  FrameBmp := TBitmap32.Create;
  Compressor := TDosDatCompressor.Create;
  Planar := TDosPlanarBitmap.Create;
  Mem := TMemoryStream.Create;//(AppPath + 'vgagrE.dat', fmCreate);
  ColorList := TIntegerList.Create;


  try
  FillChar(DosPal, SizeOf(DosPal), 0);

  Planar.MergePals({DosInLevelPalette}fPaletteStandard, DosPal, DosPal16);

{  DosPal16[7].R := 56;
  DosPal16[7].G := 32;
  DosPal16[7].B := 8; }
  {-------------------------------------------------------------------------------
    Terrains 1. Get all colors in a palette
  -------------------------------------------------------------------------------}
  ColorsUsed := 8;
  for i := 0 to MetaTerrainCollection.Count - 1 do
  begin
    Bmp := TerrainBitmaps[i];
    //deb(['start getting palette for bitmap', i]);
    //Planar.CalculateCustomPalette32FromBitmap(Bmp, Pal32, ColorsUsed);
    Planar.CalculateDosPalette16FromBitmap(Bmp, DosPal16, ColorsUsed);
    GetDistinctColors(Bmp, ColorList);
      //deb(['colorlistcount after terrain', i, colorlist.count]);
  end;

//  deb([colorlist.count]);
  //exit;

  {-------------------------------------------------------------------------------
    Terrains 2. Make planar bitmaps of the terrains
  -------------------------------------------------------------------------------}
  for i := 0 to MetaTerrainCollection.Count - 1 do
  begin
    Bmp := TerrainBitmaps[i];
    //Planar.SaveToStream(Bmp, VgaStream, 4, fPaletteStandard, DosPal);
      //deb(['start encoding terrain', i, Mem.position]);
    Planar.SaveToStream(Bmp, Mem, 4, DosPal16);
  end;

  {-------------------------------------------------------------------------------
    Terrains 3. Compress into first sections
  -------------------------------------------------------------------------------}
    //deb(['compressing terrains']);
  Mem.Seek(0, soFromBeginning);
  Compressor.Compress(Mem, CmpStream);


//  for i:=0 to 15 do
  //  with dospal16[i] do
    //  deb(['palentry', i, r,g,b]);

  {-------------------------------------------------------------------------------
    Objects 1. Get all the colors
  -------------------------------------------------------------------------------}
  Mem.Clear;
  for i := 0 to MetaObjectCollection.Count - 1 do
  begin
    Bmp := ObjectBitmaps[i];
//    TestColors := GetBitmapPalette(Bmp, Dummy);
//    deb(['testcolors', testcolors]);
//    ShowImageForm(bmp);
    Planar.CalculateDosPalette16FromBitmap(Bmp, DosPal16, ColorsUsed);
    GetDistinctColors(Bmp, ColorList);
      //deb(['colorlistcount after object', i, colorlist.count]);
//    windlg([i]);
  end;

  for i := 0 to colorlist.count-1 do
    with tcolor32entry(colorlist[i]) do
    begin
      //deb(['colorlist org values', i, r,g,b]);
      //deb(['colorlist values', i, r div 4,g div 4,b div 4, hexstr(colorlist[i])]);
    end;


//  exit;
  {-------------------------------------------------------------------------------
    Objects 2. Make planar bitmaps and create masks
  -------------------------------------------------------------------------------}
  for i := 0 to MetaObjectCollection.Count - 1 do
  begin
    MO := MetaObjectCollection[i];
    for iFrame := 0 to MO.EndAnimationFrameIndex - 1 do
    begin
      GetObjectFrame(i, iFrame, FrameBmp);
        //deb (['start encoding object frame', i, iFrame, mem.position]);
      Planar.SaveToStream(FrameBmp, Mem, 4, DosPal16);
      OldP := mem.position;
      Planar.SaveToStream(FrameBmp, Mem, 1, DosPal16);
      //deb(['masksize', mem.position-oldp]);
    end;
  end;

  //deb(['compressing objects']);
  Mem.Seek(0, soFromBeginning);
  Compressor.Compress(Mem, CmpStream);

  finally
  // free objects
  Compressor.Free;
  CmpStream.Free;
  Planar.Free;
  Mem.Free;
  FrameBmp.Free;
  ColorList.Free;
  end;

end;

procedure TDosGraphicSet.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  with Filer do
  begin
    DefineBinaryProperty('PaletteCustom', ReadPaletteCustom, WritePaletteCustom, True);
    DefineBinaryProperty('PaletteStandard', ReadPaletteStandard, WritePaletteStandard, True);
    DefineBinaryProperty('PalettePreview', ReadPalettePreview, WritePalettePreview, True);
  end;
end;

(*
procedure TDosGraphicSet.ReadPaletteCustom(Reader: TReader);
begin
  DoReadPalette(fPaletteCustom, Reader);
end;

procedure TDosGraphicSet.ReadPalettePreview(Reader: TReader);
begin
  DoReadPalette(fPalettePreview, Reader);
end;

procedure TDosGraphicSet.ReadPaletteStandard(Reader: TReader);
begin
  DoReadPalette(fPaletteStandard, Reader);
end;

procedure TDosGraphicSet.WritePaletteCustom(Writer: TWriter);
begin
  DoWritePalette(fPaletteCustom, Writer);
end;

procedure TDosGraphicSet.WritePalettePreview(Writer: TWriter);
begin
  DoWritePalette(fPalettePreview, Writer);
end;

procedure TDosGraphicSet.WritePaletteStandard(Writer: TWriter);
begin
  DoWritePalette(fPaletteStandard, Writer);
end;

procedure TDosGraphicSet.DoReadPalette(var Pal: TDosGroundVGAPaletteArray8; Reader: TReader);
begin
  Reader.Read(Pal, SizeOf(Pal));
end;

procedure TDosGraphicSet.DoWritePalette(const Pal: TDosGroundVGAPaletteArray8; Writer: TWriter);
begin
  Writer.Write(Pal, SizeOf(Pal));
end;
*)

procedure TDosGraphicSet.DoReadPalette(var Pal: TDosGroundVGAPaletteArray8; Stream: TStream);
begin
  Stream.ReadBuffer(Pal, SizeOf(Pal));
end;

procedure TDosGraphicSet.DoWritePalette(const Pal: TDosGroundVGAPaletteArray8; Stream: TStream);
begin
  Stream.WriteBuffer(Pal, SizeOf(Pal));
end;

procedure TDosGraphicSet.ReadPaletteCustom(Stream: TStream);
begin
  DoReadPalette(fPaletteCustom, Stream);
end;

procedure TDosGraphicSet.ReadPalettePreview(Stream: TStream);
begin
  DoReadPalette(fPalettePreview, Stream);
end;

procedure TDosGraphicSet.ReadPaletteStandard(Stream: TStream);
begin
  DoReadPalette(fPaletteStandard, Stream);
end;

procedure TDosGraphicSet.WritePaletteCustom(Stream: TStream);
begin
  DoWritePalette(fPaletteCustom, Stream);
end;

procedure TDosGraphicSet.WritePalettePreview(Stream: TStream);
begin
  DoWritePalette(fPalettePreview, Stream);
end;

procedure TDosGraphicSet.WritePaletteStandard(Stream: TStream);
begin
  DoWritePalette(fPaletteStandard, Stream);
end;

{ TBaseDosAnimationSet }

procedure TBaseDosAnimationSet.InternalReadMetaData;
{-------------------------------------------------------------------------------
  We dont have to read. It's fixed in this order in the main.dat.
  maybe move to inifile
-------------------------------------------------------------------------------}

    procedure DoAdd(aImageLocation: Integer; const aDescription: string;
      aFrameCount, aWidth, aHeight, aBPP: Integer;
      aFootX, aFootY, aTriggerHeight: Integer;
      aAnimType: Integer = 0);
    begin
      with fMetaAnimations.Add do
      begin
        ImageLocation := aImageLocation;
        Description := aDescription;
        FrameCount := aFrameCount;
        Width := aWidth;
        Height := aHeight;
        BitsPerPixel := aBPP;

        FootX := aFootX;
        FootY := aFootY;
        TriggerHeight := aTriggerHeight;
        AnimationType := aAnimType;

      end;

    end;
begin
  // foot positions from ccexpore's emails

  //    addr   name                   frames width height bpp ---   x   y size

  DoAdd($0000, 'walking'              ,   8, 16, 10, 2,      8, 10,  4);
  DoAdd($0140, 'jumping'              ,   1, 16, 10, 2,      8, 10,  4,  1);
  DoAdd($0168, 'walking (rtl)'        ,   8, 16, 10, 2,      8, 10,  4);
  DoAdd($02A8, 'jumping (rtl)'        ,   1, 16, 10, 2,      8, 10,  4);
  DoAdd($02D0, 'digging'              ,  16, 16, 14, 3,      8, 12,  4);
  DoAdd($0810, 'climbing'             ,   8, 16, 12, 2,      8, 12,  4);
  DoAdd($0990, 'climbing (rtl)'       ,   8, 16, 12, 2,      8, 12,  4);
  DoAdd($0B10, 'drowning'             ,  16, 16, 10, 2,      8, 10,  4,  1);
  DoAdd($0D90, 'post-climb'           ,   8, 16, 12, 2,      8, 12,  4,  1);
  DoAdd($0F10, 'post-climb (rtl)'     ,   8, 16, 12, 2,      8, 12,  4,  1);
  DoAdd($1090, 'brick-laying'         ,  16, 16, 13, 3,      8, 13,  4);
  DoAdd($1570, 'brick-laying (rtl)'   ,  16, 16, 13, 3,      8, 13,  4);
  DoAdd($1A50, 'bashing'              ,  32, 16, 10, 3,      8, 10,  4);
  DoAdd($21D0, 'bashing (rtl)'        ,  32, 16, 10, 3,      8, 10,  4);
  DoAdd($2950, 'mining'               ,  24, 16, 13, 3,      8, 13,  4);
  DoAdd($30A0, 'mining (rtl)'         ,  24, 16, 13, 3,      8, 13,  4);
  DoAdd($37F0, 'falling'              ,   4, 16, 10, 2,      8, 10,  4);
  DoAdd($3890, 'falling (rtl)'        ,   4, 16, 10, 2,      8, 10,  4);
//DoAdd($3930, 'pre-umbrella (r)'     ,   4, 16, 16, 3,      8, 16,  4);
//DoAdd($3AB0, 'umbrella (r)'         ,   4, 16, 16, 3,      8, 16,  4);
  DoAdd($3930, 'umbrella'             ,   8, 16, 16, 3,      8, 16,  4);
//DoAdd($3C30, 'pre-umbrella (l)'     ,   4, 16, 16, 3,      8, 16,  4);
//DoAdd($3DB0, 'umbrella (l)'         ,   4, 16, 16, 3,      8, 16,  4);
  DoAdd($3C30, 'umbrella (rtl)'       ,   8, 16, 16, 3,      8, 16,  4);
  DoAdd($3F30, 'splatting'            ,  16, 16, 10, 2,      8, 10,  4,  1);
  DoAdd($41B0, 'exiting'              ,   8, 16, 13, 2,      8, 13,  4,  1);
  DoAdd($4350, 'fried'                ,  14, 16, 14, 4,      8, 14,  4,  1);
  DoAdd($4970, 'blocking'             ,  16, 16, 10, 2,      8, 10,  4);
  DoAdd($4BF0, 'shrugging'            ,   8, 16, 10, 2,      8, 10,  4,  1);
  DoAdd($4D30, 'shrugging (rtl)'      ,   8, 16, 10, 2,      8, 10,  4,  1);
  DoAdd($4E70, 'oh-no-ing'            ,  16, 16, 10, 2,      8, 10,  4,  1);
  DoAdd($50F0, 'explosion'            ,   1, 32, 32, 3,     16, 25,  4,  1);

end;

procedure TBaseDosAnimationSet.InternalReadData;
var
  Fn: string;
  Mem: TMemoryStream;
  Bmp: TBitmap32;
  MaskBitmap: TBitmap32;
  TempBitmap: TBitmap32;
  Arc: TDosDatArchive;
  iAnimation, iFrame: Integer;
  Planar: TDosPlanarBitmap;
  MA: TMetaAnimation;
  MemPos: Integer;
  X, Y: Integer;
  st: tstringlist;
  DosPal: TDosGroundVGAPaletteArray8;
  Pal: TArrayOfColor32;
  StartAddress: Integer;
  Sty: TBaseDosStyle;

begin
  Assert(OwnerStyle is TBaseDosStyle);
  Sty := TBaseDosStyle(OwnerStyle);
//  Pal := DosInLevelPalette;
//  Pal[7] := Color32ToDosPaletteEntry(clRed32);



  Pal := DosPaletteToArrayOfColor32(fAnimationPalette);
  Pal[7] := clBrick32;
//  Pal[1] := clred32;

  st:=tstringlist.create;
  EnsureMetaData;

  Fn := Sty.IncludeCommonPath(Sty.MainDataFile);
  Mem := TMemoryStream.Create;
  Planar := TDosPlanarBitmap.Create;
  TempBitmap := TBitmap32.Create;
  Arc := TDosDatArchive.Create;
  try
    // extract the first section from main.dat, which contains the animations
    Arc.LoadFromFile(Fn);
    Arc.ExtractSection(0, Mem);

    with fMetaAnimations.HackedList do
      for iAnimation := 0 to Count - 1 do
      begin
        Y := 0;
        MA := List^[iAnimation];
        with MA do
        begin
          Bmp := TBitmap32.Create;
          Bmp.Width := Width;
          Bmp.Height := FrameCount * Height;
          Bmp.Clear(0);
          fAnimationCache.Add(Bmp);
//          fMaskCache.Add(nil);
          Mem.Seek(ImageLocation, soFromBeginning);
          for iFrame := 0 to FrameCount - 1 do
          begin
            Planar.LoadFromStream(Mem, TempBitmap, -1, Width, Height, BitsPerPixel, Pal);
            TempBitmap.DrawTo(Bmp, 0, Y);
            Inc(Y, Height);
//            TempBitmap.SaveToFile
          end;
          //ReplaceColor(Bmp, clBRICK, BrickColor);
          {$ifdef develop}
          Bmp.SaveToFile(AppPath + 'temp\'+'anim_'+leadzerostr(ianimation, 2) + '.bmp');
          {$endif}
        end;
      end;

      // countdown digits
      fCountDownDigits.SetSize(8, 5 * 8);
      fCountDownDigits.Clear(0);
      Mem.Clear;
      Arc.ExtractSection(1, Mem);
      DosPal := DosInLevelPalette;
      DosPal[1] := DosPal[3];
      Y := 0;
      StartAddress := $0154; // address of digit 5
      for iFrame := 1 to 5 do
      begin
        Planar.LoadFromStream(Mem, TempBitmap, StartAddress, 8, 8, 1, DosPal, DosPal);
        TempBitmap.DrawTo(fCountDownDigits, 0, Y);
        Inc(Y, 8);
        Inc(StartAddress, 8);
      end;

      // masks, edit local palette to get decently colored mask with 1 BPP
      Pal[1] := clMask32;

      // temporary solution 4 bashmasks
      StartAddress := $0;
      BashMasks.SetSize(16, 10 * 4);
      BashMasks.Clear(0);
      Y := 0;
      for iFrame := 1 to 4 do
      begin
        Planar.LoadFromStream(Mem, TempBitmap, StartAddress, 16, 10, 1, Pal);
        TempBitmap.DrawTo(BashMasks, 0, Y);
        Inc(Y, 10);
        Inc(StartAddress, 160 div 8);
      end;

      // temporary solution 4 bashmasks RTL
      StartAddress := $50;
      BashMasksRTL.SetSize(16, 10 * 4);
      BashMasksRTL.Clear(0);
      Y := 0;
      for iFrame := 1 to 4 do
      begin
        Planar.LoadFromStream(Mem, TempBitmap, StartAddress, 16, 10, 1, Pal);
        TempBitmap.DrawTo(BashMasksRTL, 0, Y);
        Inc(Y, 10);
        Inc(StartAddress, 160 div 8);
      end;


      // temporary solution 2 minemasks
      StartAddress := $00A0;
      MineMasks.SetSize(16, 13 * 2);
      MineMasks.Clear(0);
      Y := 0;
      for iFrame := 1 to 2 do
      begin
        Planar.LoadFromStream(Mem, TempBitmap, StartAddress, 16, 13, 1, Pal);
        TempBitmap.DrawTo(MineMasks, 0, Y);
        Inc(Y, 13);
        Inc(StartAddress, (16 * 13) div 8); // 1bpp
      end;

      // temporary solution 2 minemasks rtl
      StartAddress := $00D4;
      MineMasksRTL.SetSize(16, 13 * 2);
      MineMasksRTL.Clear(0);
      Y := 0;
      for iFrame := 1 to 2 do
      begin
        Planar.LoadFromStream(Mem, TempBitmap, StartAddress, 16, 13, 1, Pal);
        TempBitmap.DrawTo(MineMasksRTL, 0, Y);
        Inc(Y, 13);
        Inc(StartAddress, (16 * 13) div 8); // 1bpp
      end;


      // temporary solution 1 explosionmask
      StartAddress := $0108;
      Planar.LoadFromStream(Mem, ExplosionMask, StartAddress, 16, 22, 1, Pal);

      //ShowImageForm(Minemasks, 4);

  finally
    Arc.Free;
    Mem.Free;
    Planar.Free;
    TempBitmap.Free;
  end;
  st.free;
end;


procedure TBaseDosAnimationSet.SetAnimationPalette(const Value: TDosGroundVGAPaletteArray8);
begin
  fAnimationPalette := Value;
end;

procedure TBaseDosAnimationSet.InternalClearData;
begin
  fAnimationCache.Clear;
end;

constructor TBaseDosAnimationSet.Create(aOwnerStyle: TBaseLemmingStyle);
begin
  Assert(aOwnerStyle is TBaseDosStyle);
  inherited Create(aOwnerStyle);
  fAnimationPalette := DosInLevelPalette;
end;

{ TBaseDosLevelLoader }

function TBaseDosLevelLoader.EasyGetSectionName(aSection: Integer): string;
var
  L: TStringList;
begin
  L := TStringList.Create;
  try
    GetSections(L);
    Result := L[aSection];
  finally
    L.Free;
  end;
end;

function TBaseDosLevelLoader.FindFirstLevel(var Rec: TDosGamePlayInfoRec): Boolean;
var
  L: TStringList;
begin
  Result := True;
  L := TStringList.Create;
  try
    GetSections(L);
    with Rec do
    begin
      dValid         := True;
      dPack          := 0;
      dSection       := 0;
      dLevel         := 0;
      dSectionName   := L[0]; //#EL watch out for the record-string-mem-leak
    end;
  finally
    L.Free;
  end;
end;

function TBaseDosLevelLoader.FindLevel(var Rec: TDosGamePlayInfoRec): Boolean;
begin
  with Rec do
  begin
    Result := (dPack = 0) and (dSection >= 0) and (dSection <= 3) and
    (dLevel >= 0) and (dLevel <= 29);
    dValid := Result;
    if not Result then
      Exit;
    dSectionName := EasyGetSectionName(dSection);
  end;
end;

function TBaseDosLevelLoader.FindNextLevel(var Rec: TDosGamePlayInfoRec): Boolean;
var
  L: TStringList;
begin
  Result := (Rec.dLevel < 30) or (Rec.dSection < 3);

  Rec.dValid := False;
  if not Result then
    Exit;

  L := TStringList.Create;
  try
    GetSections(L);
    with Rec do
    begin
      dValid         := True;
      dPack          := 0;
      Inc(dLevel);
      if dLevel = 29 then
      begin
        dLevel := 0;
        Inc(dSection);
      end;
      dSectionName   := L[dSection]; //#EL watch out for the record-string-mem-leak
    end;
  finally
    L.Free;
  end;
end;

procedure TBaseDosLevelLoader.GetEntry(aSection, aLevel: Integer; var aFileName: string;
  var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer);
{-------------------------------------------------------------------------------
  This method must return information on where to get a level from
-------------------------------------------------------------------------------}
begin
  raise Exception.Create(ClassName + '.GetEntry is abstract');
end;

function TBaseDosLevelLoader.GetLevelCount(aSection: Integer): Integer;
begin
  raise Exception.Create(ClassName + '.GetLevelCount is abstract');
end;

function TBaseDosLevelLoader.GetSectionCount: Integer;
// we could overwrite this function with a faster one but maybe we are
// to lazy :)
var
  Dummy: TStringList;
begin
  Result := 0;
  Dummy := TStringList.Create;
  try
    GetSections(Dummy);
    Result := Dummy.Count;
  finally
    Dummy.Free;
  end;
end;

procedure TBaseDosLevelLoader.GetSections(aSectionNames: TStrings);
begin
  raise Exception.Create(ClassName + '.GetSections is abstract');
end;

procedure TBaseDosLevelLoader.InternalLoadLevel(aInfo: TLevelInfo; aLevel: TLevel);
var
  LVL: TLVLFileRec;
  NLVL: TNeoLVLRec;
  OddTable: TDosOddTable;
  Ox: Integer;
  Sty: TBaseDosStyle;
  Mem: TMemoryStream;
  Arc: TDosDatArchive;
  i: byte;
begin
  Assert(Owner is TBaseDosStyle, 'basedoslevelloader owner error');

  Sty := TBaseDosStyle(Owner);
  Arc := TDosDatArchive.Create;
  Mem := TMemoryStream.Create;
  try
    Arc.LoadFromFile(aInfo.DosLevelPackFileName);
    Arc.ExtractSection(aInfo.DosLevelPackIndex, Mem);
    Mem.Seek(0, soFromBeginning);
    Mem.Read(i, 1);
    case i of
      1: Mem.ReadBuffer(NLVL, SizeOf(NLVL));
      else Mem.ReadBuffer(LVL, SizeOf(LVL));
      end;
  finally
    Arc.Free;
    Mem.Free;
  end;

  if aInfo.DosIsOddTableClone then
  begin
    Ox := aInfo.DosOddTableIndex;
    OddTable := TDosOddTable.Create;
    try
      OddTable.LoadFromFile(aInfo.DosOddTableFileName);
      with OddTable do
      begin
        Move(Recs[Ox], LVL, SizeOf(TDosOddTableRec) - 32);
        Move(Recs[Ox].LevelName, LVL.LevelName, 32);
      end;
    finally
      OddTable.Free;
    end;
  end;

  aLevel.BeginUpdate;
  try
    aLevel.Style := nil;
    aLevel.Style := Sty;
    case i of
      1: aLevel.LoadFromLVLRec(NLVL);
      else aLevel.LoadFromLVLRec(LVL);
      end;
  finally
    aLevel.EndUpdate;
  end;
end;

procedure TBaseDosLevelLoader.InternalLoadSingleLevel(aPack, aSection, aLevelIndex: Integer; aLevel: TLevel);
{-------------------------------------------------------------------------------
  Method for loading one level, without the preparing caching system.
-------------------------------------------------------------------------------}
var
  LocalSectionNames: TStringList;
  iSection, iLevel: Integer;
  Fn: string;
  IsOdd: Boolean;
  OddIndex: Integer;
  FileIndex: Integer;
  Sty: TBaseDosStyle;

  OddTable: TDosOddTable;
  Arc: TDosDatArchive;
  LocalLevelInfo: TLevelInfo;

    function GetOddTitle(Inf: TLevelInfo): string;
    var
      Ox: Integer;
    begin
      Result := StringOfChar(' ', 32);
      Ox := Inf.DosOddTableIndex;
      Move(OddTable.Recs[ox].LevelName, Result[1], 32);
    end;

begin
  Assert(Owner is TBaseDosStyle);

  // first try if it is cached in the nested collections
  if aPack <= LevelPacks.Count - 1 then
    if aSection <= LevelPacks[aPack].PackSections.Count - 1 then
      if aLevelIndex <= LevelPacks[aPack][aSection].LevelInfos.Count - 1 then
      begin
        InternalLoadLevel(LevelPacks[aPack].PackSections[aSection].LevelInfos[aLevelIndex], aLevel);
        Exit;
      end;

  // otherwise gather info and load it then
  OddTable := nil;
  LocalSectionNames := TStringList.Create;
  Sty := TBaseDosStyle(Owner);
  LocalLevelInfo := TLevelInfo.Create(nil);

  TRY

  GetSections(LocalSectionNames);

  if Sty.OddTableFile <> '' then
  begin
    OddTable := TDosOddTable.Create;
    OddTable.LoadFromFile(Sty.IncludeCommonPath(Sty.OddTableFile));
  end;

  IsOdd := False;
  OddIndex := -1;
  FileIndex := -1;
  Fn := '';
  GetEntry(aSection, aLevelIndex, Fn, FileIndex, IsOdd, OddIndex);
  Fn := Sty.IncludeCommonPath(Fn);

  LocalLevelInfo.DosLevelPackFileName := Fn;
  LocalLevelInfo.DosLevelPackIndex := FileIndex;
  LocalLevelInfo.DosIsOddTableClone := IsOdd;
  LocalLevelInfo.DosOddTableIndex := OddIndex;
  if IsOdd then
    LocalLevelInfo.DosOddTableFileName := Sty.IncludeCommonPath(Sty.OddTableFile);

  {
  Arc := TDosDatArchive.Create;
  try
  Arc.LoadFromFile(Fn);
  if not IsOdd then
    LocalLevelInfo.TrimmedTitle := Trim(Arc.ExtractLevelTitle(FileIndex))
  else begin
    LocalLevelInfo.TrimmedTitle := Trim(GetOddTitle(LocalLevelInfo))
  end;
  finally
  Arc.Free;
  end;
  }

  InternalLoadLevel(LocalLevelInfo, aLevel);
  FINALLY

  if OddTable <> nil then
    OddTable.Free;
  LocalSectionNames.Free;
  LocalLevelInfo.Free;

  END;

end;

procedure TBaseDosLevelLoader.InternalPrepare;
{-------------------------------------------------------------------------------
  Load all level info
-------------------------------------------------------------------------------}
var
  LocalSectionNames: TStringList;
  iSection, iLevel: Integer;
  Pack: TLevelPack;
  Section: TPackSection;
  LevelInfo: TLevelInfo;
  Fn1, Fn: string;
  IsOdd: Boolean;
  OddIndex: Integer;
  FileIndex: Integer;
  Sty: TBaseDosStyle;

  OddTable: TDosOddTable;
  Arc: TDosDatArchive;

    function GetOddTitle(Inf: TLevelInfo): string;
    var
      Ox: Integer;
    begin
      Result := StringOfChar(' ', 32);
      Ox := Inf.DosOddTableIndex;
      move(OddTable.recs[ox].LevelName, result[1], 32);
    end;

begin
  OddTable := nil;
  if not (Owner is TBaseDosStyle) then
    raise Exception.Create('base dos level loader error');

  LocalSectionNames := TStringList.Create;
  Sty := TBaseDosStyle(Owner);
  Arc := TDosDatArchive.Create;

  TRY

  GetSections(LocalSectionNames);

  if Sty.OddTableFile <> '' then
  begin
    OddTable := TDosOddTable.Create;
    OddTable.LoadFromFile(Sty.IncludeCommonPath(Sty.OddTableFile));
  end;

  // default pack
  Pack := fLevelPacks.Add;
  Pack.LevelPackName := 'Default';
  // sections
  for iSection := 0 to LocalSectionNames.Count - 1 do
  begin
    Section := Pack.PackSections.Add;
    Section.SectionName := LocalSectionNames[iSection];
    //deb([section.sectionname + '_' + i2s(iSection)]);
    // levels
    for iLevel := 0 to GetLevelCount(iSection) - 1 do
    begin
      IsOdd := False;
      OddIndex := -1;
      FileIndex := -1;
      Fn := '';
      GetEntry(iSection{ + 1}, iLevel{ + 1}, Fn, FileIndex, IsOdd, OddIndex);
      Fn1 := Fn;
      Fn := Sty.IncludeCommonPath(Fn);
      LevelInfo := Section.LevelInfos.Add;
//      LevelInfo.iOwnerFile := '';
      LevelInfo.DosLevelPackFileName := Fn;
      LevelInfo.DosLevelPackIndex := FileIndex;
      LevelInfo.DosIsOddTableClone := IsOdd;
      LevelInfo.DosOddTableIndex := OddIndex;
      if IsOdd then
        LevelInfo.DosOddTableFileName := Sty.IncludeCommonPath(Sty.OddTableFile);
      {
      with LevelInfo do
        deb([section.sectionname + '_' + i2s(iLevel) + '=' + Fn1 + ',' + i2s(FileIndex) + ',' + i2s(OddIndex)]);
      }
      Arc.LoadFromFile(Fn);
//      DEB)(
      if not IsOdd then
        LevelInfo.TrimmedTitle := Trim(Arc.ExtractLevelTitle(FileIndex))
      else begin
        LevelInfo.TrimmedTitle := Trim(GetOddTitle(LevelInfo))
      end;
//      'level ' + i2s(iLevel + 1); // lemtools
//      LevelInfo.iOwnerIndex
      //raise exception.create('ERROR NOT FINISHED'); //kernel_resource
    end;
  end;

  FINALLY

  Arc.Free;
  if OddTable <> nil then
    OddTable.Free;
  LocalSectionNames.Free;

  END;

end;

{ TDosOrigLevelLoader }

procedure TDosOrigLevelLoader.GetEntry(aSection, aLevel: Integer;
  var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean;
  var aOddIndex: Integer);
{-------------------------------------------------------------------------------
  I think the numbers in the table are subtracted by 1 before being used in the manner Mike described.
  For example, for Fun 1 the number is 147.
  Divide by 2 (dropping lowest bit first) and you get 73.  "Just Dig" is the 2nd
  level in set level009.dat.  Using 0-based counting, 73 would indeed be the correct number.
  On the other hand, for Fun 8 the number is 14.  Divide by 2 and you get 7.
  Yet "Turn Around Young Lemmings!" is the 7th
  level in set level000.dat.  The correct number should be 6 if 0-based counting is used.
  This inconsistency goes away if all the numbers are subtracted by 1 first.  For example, the 147 becomes 146.  Divide by 2
  and you still get 73.  But for Fun 9, subtract by 1 and you get 13.  Divide by 2 and now you get 6, the correct number when
  counting from 0.  Alternatively, you can add 1 to all numbers for 1-based counting, so that the 147 becomes 148 and 73
  becomes 74.  But either way, the parity (odd/even) changes when you add/subtract 1, and after that, Mike's description
  regarding the bottom bit works correctly.
-------------------------------------------------------------------------------}
var
  B: Byte;
  Fx: Integer; //lemstyles
begin
  B := SectionTable[aSection + 1, aLevel + 1];
  Dec(B);
  IsOddTable := {not} odd(B);// and Bit0 <> 0;
  B := B div 2;
  Fx := (B) div 8;
  aFileName := 'LEVEL' + LeadZeroStr(Fx, 3) + '.DAT';
  aFileIndex := B mod 8;
  aOddIndex := -1;
  if IsOddTable then
    aOddIndex := B{ - 1};
end;

function TDosOrigLevelLoader.GetLevelCount(aSection: Integer): Integer;
begin
  Result := 30;
end;

procedure TDosOrigLevelLoader.GetSections(aSectionNames: TStrings);
begin
  aSectionNames.CommaText := 'Fun,Tricky,Taxing,Mayhem';

end;

(*procedure TDosOrigLevelLoader.InternalPrepare;
{-------------------------------------------------------------------------------
  Load all level info
-------------------------------------------------------------------------------}
const
  LocalSectionNames: array[0..3] of string = ('Fun', 'Tricky', 'Taxing', 'Mayhem');
var
  iSection, iLevel: Integer;
  Pack: TLevelPack;
  Section: TPackSection;
  LevelInfo: TLevelInfo;
  Fn1, Fn: string;
  IsOdd: Boolean;
  OddIndex: Integer;
  FileIndex: Integer;
  Sty: TDosOrigStyle;

  OddTable: TDosOddTable;
  Arc: TDosDatArchive;

    function GetOddTitle(Inf: TLevelInfo): string;
    var
      Ox: Integer;
    begin
      Result := StringOfChar(' ', 32);
      Ox := Inf.DosOddTableIndex;
      move(OddTable.recs[ox].LevelName, result[1], 32);
    end;

begin
  if not (Owner is TDosOrigStyle) then
    raise Exception.Create('dosorig loader error');
  Sty := TDosOrigStyle(Owner);
  Arc := TDosDatArchive.Create;
  OddTable := TDosOddTable.Create;
  OddTable.LoadFromFile(Sty.IncludeCommonPath(Sty.OddTableFile));
  // default pack
  Pack := fLevelPacks.Add;
  Pack.LevelPackName := 'Default';
  // sections
  for iSection := 0 to 3 do
  begin
    Section := Pack.PackSections.Add;
    Section.SectionName := LocalSectionNames[iSection];
    //deb([section.sectionname + '_' + i2s(iSection)]);
    // levels
    for iLevel := 0 to 29 do
    begin
      GetEntry(iSection + 1, iLevel + 1, Fn, FileIndex, IsOdd, OddIndex);
      Fn1 := Fn;
      Fn := Sty.IncludeCommonPath(Fn);
      LevelInfo := Section.LevelInfos.Add;
//      LevelInfo.iOwnerFile := '';
      LevelInfo.DosLevelPackFileName := Fn;
      LevelInfo.DosLevelPackIndex := FileIndex;
      LevelInfo.DosIsOddTableClone := IsOdd;
      LevelInfo.DosOddTableIndex := OddIndex;
      if IsOdd then
        LevelInfo.DosOddTableFileName := Sty.IncludeCommonPath(Sty.OddTableFile);
      {
      with LevelInfo do
        deb([section.sectionname + '_' + i2s(iLevel) + '=' + Fn1 + ',' + i2s(FileIndex) + ',' + i2s(OddIndex)]);
      }
      Arc.LoadFromFile(Fn);
      if not IsOdd then
        LevelInfo.TrimmedTitle := Trim(Arc.ExtractLevelTitle(FileIndex))
      else begin
        LevelInfo.TrimmedTitle := Trim(GetOddTitle(LevelInfo))
      end;
//      'level ' + i2s(iLevel + 1); // lemtools
//      LevelInfo.iOwnerIndex
      //raise exception.create('ERROR NOT FINISHED'); //kernel_resource
    end;
  end;

  Arc.Free;

end; *)


{ TDosOhNoLevelLoader }

procedure TDosOhNoLevelLoader.GetEntry(aSection, aLevel: Integer; var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean; var aOddIndex: Integer);
var
  H, Sec, Lev: Integer;
begin
  H := OhnoTable[aSection + 1, aLevel + 1];
  Sec := H div 10;
  Lev := H mod 10;
  aFilename := 'Dlvel' + LeadZeroStr(Sec, 3) + '.dat';
  aFileIndex := Lev;
end;

function TDosOhNoLevelLoader.GetLevelCount(aSection: Integer): Integer;
begin
  Result := 20;
end;

procedure TDosOhNoLevelLoader.GetSections(aSectionNames: TStrings);
begin
  aSectionNames.CommaText := 'Tame,Crazy,Wild,Wicked,Havoc';
end;


{ TDosChristmas91LevelLoader }

procedure TDosChristmas91LevelLoader.GetEntry(aSection, aLevel: Integer;
  var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean;
  var aOddIndex: Integer);
begin
  aFileName := 'Level000.dat';
  aFileIndex := aLevel{ - 1};
end;

function TDosChristmas91LevelLoader.GetLevelCount(aSection: Integer): Integer;
begin
  Result := 4;
end;

procedure TDosChristmas91LevelLoader.GetSections(aSectionNames: TStrings);
begin
  aSectionNames.Add('1991 levels');
end;

procedure TDosChristmas92LevelLoader.GetEntry(aSection, aLevel: Integer;
  var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean;
  var aOddIndex: Integer);
begin
  aFileName := 'Level000.dat';
  aFileIndex := aLevel{ - 1};
end;

function TDosChristmas92LevelLoader.GetLevelCount(aSection: Integer): Integer;
begin
  Result := 4;
end;

procedure TDosChristmas92LevelLoader.GetSections(aSectionNames: TStrings);
begin
  aSectionNames.Add('1992 levels');
end;

{ TDosHoliday93LevelLoader }

procedure TDosHoliday93LevelLoader.GetEntry(aSection, aLevel: Integer;
  var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean;
  var aOddIndex: Integer);
begin

  Inc(aSection);
  Inc(aLevel);

  case aSection of
    1:
      begin
        case aLevel of
          1..8  :
            begin
              aFileName := 'LEVEL000.DAT';
              aFileIndex := aLevel - 1;
            end;
          9..16 :
            begin
              aFileName := 'LEVEL001.DAT';
              aFileIndex := aLevel - 9;
            end;
        end;
      end;
    2:
      begin
        case aLevel of
          1..8  :
            begin
              aFileName := 'LEVEL002.DAT';
              aFileIndex := aLevel - 1;
            end;
          9..16 :
            begin
              aFileName := 'LEVEL003.DAT';
              aFileIndex := aLevel - 9;
            end;
        end;
      end;
  end;


end;

function TDosHoliday93LevelLoader.GetLevelCount(aSection: Integer): Integer;
begin
  Result := 16; //8; 8=mistake!
end;

procedure TDosHoliday93LevelLoader.GetSections(aSectionNames: TStrings);
begin
  aSectionNames.CommaText := 'Flurry,Blizzard';
end;

{ TDosHoliday94LevelLoader }

procedure TDosHoliday94LevelLoader.GetEntry(aSection, aLevel: Integer;
  var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean;
  var aOddIndex: Integer);
var
  H: Integer;
begin
  H := aSection * 2;
  if aLevel > 7 then
    Inc(H);

  aFileName := 'LEVEL' + LeadZeroStr(H, 3) + '.DAT';

  if aLevel <= 7 then
    aFileIndex := aLevel
  else
    aFileIndex := aLevel - 8;
end;

function TDosHoliday94LevelLoader.GetLevelCount(aSection: Integer): Integer;
begin
  Result := 16;
end;

procedure TDosHoliday94LevelLoader.GetSections(aSectionNames: TStrings);
begin
  aSectionNames.CommaText := 'Frost,Hail,Flurry,Blizzard';
end;

(*
{ TBaseDosLevelValidator }

class function TBaseDosLevelValidator.ValidBasherCount(Value: Integer): Boolean;
begin
end;

class function TBaseDosLevelValidator.ValidBlockerCount(Value: Integer): Boolean;
begin

end;

class function TBaseDosLevelValidator.ValidBomberCount(Value: Integer): Boolean;
begin

end;

class function TBaseDosLevelValidator.ValidBuilderCount(Value: Integer): Boolean;
begin

end;

class function TBaseDosLevelValidator.ValidClimberCount(Value: Integer): Boolean;
begin

end;

class function TBaseDosLevelValidator.ValidDiggerCount(Value: Integer): Boolean;
begin

end;

class function TBaseDosLevelValidator.ValidFloaterCount(Value: Integer): Boolean;
begin

end;

class function TBaseDosLevelValidator.ValidLemmingsCount(Value: Integer): Boolean;
begin

end;

class function TBaseDosLevelValidator.ValidMinerCount(Value: Integer): Boolean;
begin

end;

class function TBaseDosLevelValidator.ValidReleaseRate(Value: Integer): Boolean;
begin
  Result := (Value >= 0) and (Value <= 250);
end;

class function TBaseDosLevelValidator.ValidRescueCount(Value: Integer): Boolean;
begin

end;

class function TBaseDosLevelValidator.ValidTimeLimit(Value: Integer): Boolean;
begin

end;
*)


{ TDosChristmas92LevelLoader }

{ TMainDatExtractor }

constructor TMainDatExtractor.Create(const aMainFile: string);
var
  i: Integer;
begin
  fPlanar := TDosPlanarBitmap.Create;
  fMem    := TMemoryStream.Create;
  fArc    := TDosDatArchive.Create;
  fMainFile := aMainFile;
  for i := 0 to 6 do
    fMems[i] := TMemoryStream.Create;
//  Arc.LoadFromFile(fMainFile);
end;

destructor TMainDatExtractor.Destroy;
var
  i: Integer;
begin
  fPlanar.Free;
  fArc.Free;
  fMem.Free;
  for i := 0 to 6 do
    fMems[i].Free;
  inherited;
end;

procedure TMainDatExtractor.DoExtract(aSection: Integer);
begin
  if Arc.SectionList.Count = 0 then
    Arc.LoadFromFile(fMainFile);
  if fMems[aSection].Size = 0 then
    Arc.ExtractSection(aSection, fMems[aSection]);
end;

procedure TMainDatExtractor.ExtractBrownBackground(Bmp: TBitmap32);
begin
  DoExtract(3);
  Planar.DirectRGB := True;
  Planar.LoadFromStream(fMems[3], Bmp, 0, 320, 104, 2, DosMainMenuPalette);
end;

procedure TMainDatExtractor.ExtractSkillPanel(Bmp: TBitmap32; const aPal: TDosGroundVGAPaletteArray8);
begin
  DoExtract(6);
  Planar.DirectRGB := False;
  Planar.LoadFromStream(fMems[6], Bmp, 0, 320, 40, 4, aPal, aPal);
end;

procedure TMainDatExtractor.ExtractSkillPanelDigits(Bmp: TBitmap32);
begin
//  DoExtract(6);
end;

procedure TMainDatExtractor.SetMainFile(const Value: string);
var
  i: Integer;
begin
  if CompareText(fMainFile, Value) = 0 then
    Exit;
  fMainFile := Value;
  for i := Low(fMems) to High(fMems) do
    fMems[i].Clear;
end;

{ TCustLemmLevelLoader }

procedure TCustLemmLevelLoader.GetEntry(aSection, aLevel: Integer;
  var aFileName: string; var aFileIndex: Integer; var IsOddTable: Boolean;
  var aOddIndex: Integer);
begin
  aFileName := 'levelpak.dat';
  aFileIndex := aLevel;
  IsOddTable := False;
  aOddIndex := -1;
end;

function TCustLemmLevelLoader.GetLevelCount(aSection: Integer): Integer;
begin
  Result := 10;
end;

procedure TCustLemmLevelLoader.GetSections(aSectionNames: TStrings);
begin
  aSectionNames.Text := 'Default';
end;

end.

                     (*
  TDosOrigFileParam = record
    dFileName: string;
    dFileIndex: Integer;
    dIsClone: Boolean;
  end;             *)


//lemtypes
096
046
090
091
005
094
061
006
012
087
104
105
106
106
107
108
109
109
108
107
------
74.
53.
32.
27.
24.
23.
52.
17.
11.
03.
002
000
045
085
013
082
083
056
084
095






////////////////
////////////////
////////////////
(*
procedure TDosOrigStyle.ExtractLevelsInOrder;
var
  s, i: integer;
  fn:string;
  ix,ox:integer;
  isodd:boolean;
  cnt: integer;

    function GetIt(const f: string; ix: integer): string;
    var
      i,j: Integer;
      //F: string;
      Arc: TDosDatArchive;
      mem :TMemoryStream;
      lvl: TLVLFileRec;
      s:string;
    begin
//      for i := 0 to 9 do
      begin
//        F := CommonPath + 'LEVEL' + LeadZeroStr(i, 3) + '.DAT';
        Arc := TDosDatArchive.Create;
        Arc.LoadFromFile('d:\lemorig\' + f);
//        for j := 0 to arc.SectionList.count-1 do
        begin
          mem:=TMemoryStream.create;
          Arc.ExtractSection(ix, mem);
          mem.seek(0,sofrombeginning);
          mem.Read(lvl, 2048);
          s := Trim(lvl.levelname);  //ulog
          result := s;
//          deb([{f, ix, }s]);
          mem.free;
        end;
        Arc.Free;
      end;
    end;

var
  ot: tdosoddtable;

  tit:string;
begin
  ot:=TDosOddTable.create;
  ot.loadfromfile('d:\lemorig\oddtable.dat');
  for s:=1 to 4 do
  begin
    deb([DosOrigSectionNames[s]]);
    for i := 1 to 30 do
    begin
      GetDosOrigLevelEntry(s,i,fn, ix, isodd, ox);
      tit := getit(fn, ix);
      deb(['nr.' + i2s(i), tit]);
      if isodd then
      begin
        tit:=trim(ot.recs[ox].LevelName);
        deb(['-->', tit])
      end
//      else begin
  //    end;
    end;
 end;
  ot.free;
end;

procedure TDosOrigStyle.ExtractLevelTitles;
var
  i,j: Integer;
  F: string;
  Arc: TDosDatArchive;
  mem :TMemoryStream;
  lvl: TLVLFileRec;
  s:string;
begin
  for i := 0 to 9 do
  begin
    F := CommonPath + 'LEVEL' + LeadZeroStr(i, 3) + '.DAT';
    Arc := TDosDatArchive.Create;
    Arc.LoadFromFile(f);
    deb(['------------levelpack', i]);
    for j := 0 to arc.SectionList.count-1 do
    begin
      mem:=TMemoryStream.create;
      Arc.ExtractSection(j, mem);
      mem.seek(0,sofrombeginning);
      mem.Read(lvl, 2048);
      s := TrimRight(lvl.levelname);  //ulog
      deb([j, s]);
      mem.free;
    end;
    Arc.Free;
  end;
end;

procedure TDosOrigStyle.ExtractMain;
var
  F: string;
  Arc: TDosDatArchive;
  Mem: TMemoryStream;
  DosBmp: TDosPlanarBitmap;
  Bmp: TBitmap32;
  C: Char;
  Path: string;
begin
  F := CommonPath + 'main.dat';
  // extract sections
  Arc := TDosDatArchive.Create;
  Mem := TMemoryStream.Create;
  Arc.LoadFromFile(F);
  Arc.ExtractSection(4, Mem);
  Mem.Seek(0, soFromBeginning);
  DosBmp := TDosPlanarBitmap.Create;
  DosBmp.DirectRGB := True;
  Bmp := TBitmap32.Create;
  Path := ApplicationPath + 'dosbmp\';
  ForceDirectories(Path); //ftest
  for C := '!' to '~' do
  begin
    DosBmp.LoadFromStream(Mem, Bmp,
      {Mem.Position + }$69B0 + Integer(Ord(C) - 33) * $60,
      16, 16, 3, DosMainMenuPalette);
    Bmp.SaveToFile(Path + 'c_' + LeadZeroStr(ord(C), 3) + '.bmp');
  end;

  Bmp.Free;
  DosBmp.Free;
  Arc.Free;
  Mem.Free;
  // extract bitmaps
end;

procedure TDosOrigStyle.GetDosOrigLevelEntry(aSection,
  aLevel: Integer; var aFileName: string; var aFileIndex: Integer;
  var IsOddTable: Boolean; var aOddIndex: Integer);
var
  B: Byte;
  Fx: Integer; //lemstyles
begin
(*
I think the numbers in the table are subtracted by 1 before being used in the manner Mike described.
For example, for Fun 1 the number is 147.
Divide by 2 (dropping lowest bit first) and you get 73.  "Just Dig" is the 2nd
level in set level009.dat.  Using 0-based counting, 73 would indeed be the correct number.
On the other hand, for Fun 8 the number is 14.  Divide by 2 and you get 7.
Yet "Turn Around Young Lemmings!" is the 7th
level in set level000.dat.  The correct number should be 6 if 0-based counting is used.
This inconsistency goes away if all the numbers are subtracted by 1 first.  For example, the 147 becomes 146.  Divide by 2
and you still get 73.  But for Fun 9, subtract by 1 and you get 13.  Divide by 2 and now you get 6, the correct number when
counting from 0.  Alternatively, you can add 1 to all numbers for 1-based counting, so that the 147 becomes 148 and 73
becomes 74.  But either way, the parity (odd/even) changes when you add/subtract 1, and after that, Mike's description
regarding the bottom bit works correctly.
*)
  B := SectionTable[aSection, aLevel];
  Dec(B);
  IsOddTable := {not} odd(B);// and Bit0 <> 0;
  B := B div 2;
  Fx := (B) div 8;
  aFileName := 'LEVEL' + LeadZeroStr(Fx, 3) + '.DAT';
  aFileIndex := B mod 8;
  aOddIndex := -1;
  if IsOddTable then
    aOddIndex := B{ - 1};

end;

function TDosOrigStyle.GetLevel(var LVL: TLVLFileRec; aSection, aLevel: Integer;
  out Info: TDosOrigFileParam): string;
var
  s, i: integer;
//  fn:string;
  {ix,}ox:integer;
//  isodd:boolean;
  cnt: integer;


var
  ot: tdosoddtable;
      Arc: TDosDatArchive;
      mem :TMemoryStream;

  tit:string;
begin
  info.dFileIndex:=-1;
  info.dFileName:='';
  info.dIsClone:=false;

  ot:=TDosOddTable.create;
  ot.loadfromfile(IncludeCommonPath(OddTableFile));
  GetDosOrigLevelEntry(aSection, aLevel, {fn}info.dfilename, info.dfileindex{ix}, info.disclone{isodd}, ox);

  Arc := TDosDatArchive.Create;
  Arc.LoadFromFile(IncludeCommonPath(info.dfilename));
//        for j := 0 to arc.SectionList.count-1 do
  begin
    mem:=TMemoryStream.create;
    Arc.ExtractSection(info.dfileindex{ix}, mem);
    mem.seek(0,sofrombeginning);
    mem.Read(lvl, 2048);
//    info.disclone:=isodd;
    if info.disclone then
    begin
      move(ot.recs[ox], lvl, sizeof(TDosOddTableRec) - 32);
      move(ot.recs[ox].LevelName, lvl.LevelName, 32);
    end;
//    if isodd then
  //    result := trim(ot.recs[ox].LevelName)
    //else
      result := Trim(lvl.levelname);  //ulog
//          deb([{f, ix, }s]);
    mem.free;
  end;
  Arc.Free;
  ot.free;

(*

  for s:=1 to 4 do
  begin
    deb([DosOrigSectionNames[s]]);
    for i := 1 to 30 do
    begin
      tit := getit(fn, ix);
      deb(['nr.' + i2s(i), tit]);
      if isodd then
      begin
        tit:=trim(ot.recs[ox].LevelName);
        deb(['-->', tit])
      end
//      else begin
  //    end;
    end;
 end;
  ot.free; *)
end;

procedure TDosOrigStyle.InternalPrepareLevelLoading;
const
  LocalSectionNames: array[0..3] of string = ('Fun', 'Tricky', 'Taxing', 'Mayhem');
var
  iSection, iLevel: Integer;
  Pack: TLevelPack;
  Section: TPackSection;
  LevelInfo: TLevelInfo;
  Fn1, Fn: string;
  IsOdd: Boolean;
  OddIndex: Integer;
  FileIndex: Integer;
  Arc: TDosDatArchive;

    function GetOddTitle(Inf: TLevelInfo): string;
    var
      Ot: TDosOddTable;
      Ox: Integer;
    begin
      Result := StringOfChar(' ', 32);
      begin
        Ox := Inf.DosOddTableIndex;
        Ot := TDosOddTable.Create;
        try
          Ot.LoadFromFile(IncludeCommonPath(OddTableFile));
          //Move(ot.recs[ox], lvl, sizeof(TDosOddTableRec) - 32);
          move(ot.recs[ox].LevelName, result[1], 32);
        finally
          Ot.Free;
        end;
      end;
    end;

begin
  Arc := TDosDatArchive.Create;
  // default pack
  Pack := LevelPacks.Add;
  Pack.LevelPackName := 'Default';
  // sections
  for iSection := 0 to 3 do
  begin
    Section := Pack.PackSections.Add;
    Section.SectionName := LocalSectionNames[iSection];
    //deb([section.sectionname + '_' + i2s(iSection)]);
    // levels
    for iLevel := 0 to 29 do
    begin
      GetDosOrigLevelEntry(iSection + 1, iLevel + 1, Fn, FileIndex, IsOdd, OddIndex);
      Fn1 := Fn;
      Fn := IncludeCommonPath(Fn);
      LevelInfo := Section.LevelInfos.Add;
//      LevelInfo.iOwnerFile := '';
      LevelInfo.DosLevelPackFileName := Fn;
      LevelInfo.DosLevelPackIndex := FileIndex;
      LevelInfo.DosIsOddTableClone := IsOdd;
      LevelInfo.DosOddTableIndex := OddIndex;
      with LevelInfo do
        deb([section.sectionname + '_' + i2s(iLevel) + '=' + Fn1 + ',' + i2s(FileIndex) + ',' + i2s(OddIndex)]);
      Arc.LoadFromFile(Fn);
      if not IsOdd then
        LevelInfo.TrimmedTitle := Trim(Arc.ExtractLevelTitle(FileIndex))
      else begin
        LevelInfo.TrimmedTitle := Trim(GetOddTitle(LevelInfo))
      end;
//      'level ' + i2s(iLevel + 1); // lemtools
//      LevelInfo.iOwnerIndex
      //raise exception.create('ERROR NOT FINISHED'); //kernel_resource
    end;
  end;

  Arc.Free;

end;


function TDosOrigStyle.InternalGetLevelCount(aLevelPack, aSection: Integer): Integer;
begin
  Result := 30;
end;

procedure TDosOrigStyle.InternalGetLevelInfo(aLevelPack, aSection, aIndex: Integer; out Info: TLevelInfoRec);
var
  LVL: TLVLFileRec;
  Inf: TDosOrigFileParam;
begin
  inherited; // clear info
  GetLevel(LVL, aSection + 1, aIndex + 1, Inf); // we work 1-base internally
  Info.iSection := aSection;
  Info.iLevel := aIndex;
  Info.iTitle := Trim(LVL.LevelName);
end;

function TDosOrigStyle.InternalGetSectionCount(aLevelPack: Integer): Integer;
begin
  Result := 4;
end;

function TDosOrigStyle.InternalGetSectionName(aLevelPack, aSection: Integer): string;
begin
  case aSection of
    0: Result := 'Fun';
    1: Result := 'Tricky';
    2: Result := 'Taxing';
    3: Result := 'Mayhem';
  end;
end;

procedure TDosOrigStyle.InternalLoadLevel(aLevelPack, aSection, aIndex: Integer; aLevel: TLevel);
var
  LVL: TLVLFileRec;
  Info: TDosOrigFileParam;
  Inf: TLevelInfo;
  Ot: TDosOddTable;
  Ox: Integer;
begin
  GetLevel(LVL, aSection + 1, aIndex + 1, Info);
  Inf := LevelPacks[aLevelPack].PackSections[aSection].LevelInfos[aIndex];

  if Inf.DosIsOddTableClone then
  begin
    Ox := Inf.DosOddTableIndex;
    Ot := TDosOddTable.Create;
    try
      Ot.LoadFromFile(IncludeCommonPath(OddTableFile));
      Move(ot.recs[ox], lvl, sizeof(TDosOddTableRec) - 32);
      move(ot.recs[ox].LevelName, lvl.LevelName, 32);
    finally
      Ot.Free;
    end;
  end;

  //aLevel.BeginUpdate;
  aLevel.Style := Self;
  aLevel.LoadFromLVLRec(LVL);



  (*
var
  P, S, L: Integer;
begin
  L := Info.Index;
  S := Info.OwnerSection.Index;
  P := Info.OwnerSection.OwnerPack.Index;

  if   lemcore
    *)
  (*
  if


  ot:=TDosOddTable.create;
  ot.loadfromfile(IncludeCommonPath(OddTableFile));
  GetDosOrigLevelEntry(aSection, aLevel, {fn}info.dfilename, info.dfileindex{ix}, info.disclone{isodd}, ox);

  Arc := TDosDatArchive.Create;
  Arc.LoadFromFile(IncludeCommonPath(info.dfilename));
//        for j := 0 to arc.SectionList.count-1 do
  begin
    mem:=TMemoryStream.create;
    Arc.ExtractSection(info.dfileindex{ix}, mem);
    mem.seek(0,sofrombeginning);
    mem.Read(lvl, 2048);
//    info.disclone:=isodd;
    if info.disclone then
    begin
      move(ot.recs[ox], lvl, sizeof(TDosOddTableRec) - 32);
      move(ot.recs[ox].LevelName, lvl.LevelName, 32);
    end;
//    if isodd then
  //    result := trim(ot.recs[ox].LevelName)
    //else
      result := Trim(lvl.levelname);  //ulog
//          deb([{f, ix, }s]);
    mem.free;
  end;
  Arc.Free;
  ot.free;
    *)

end;

procedure TDosOhNoStyle.ExtractLevelTitles;
var
  i,j: Integer;
  F: string;
  Arc: TDosDatArchive;
  mem :TMemoryStream;
  lvl: TLVLFileRec;
  s:string;
begin
  for i := 0 to 12 do
  begin
    F := CommonPath + 'DLVEL' + LeadZeroStr(i, 3) + '.DAT';
    Arc := TDosDatArchive.Create;
    Arc.LoadFromFile(f);
    deb([ExtractFileName(F) + '------------']);
    for j := 0 to arc.SectionList.count-1 do
    begin
      mem:=TMemoryStream.create;
      Arc.ExtractSection(j, mem);
      mem.seek(0,sofrombeginning);
      mem.Read(lvl, 2048);
      s := TrimRight(lvl.levelname);  //ulog
      deb([ExtractFilename(F), j, s]);
      mem.free;
    end;
    Arc.Free;
  end;
end;

function TDosOhNoStyle.GetLevel(var LVL: TLVLFileRec; aSection, aLevel: Integer): string;
var
  H, Sec, Lev: Integer;
  Fn: string;
  Arc: TDosDatArchive;
  Mem: TMemoryStream;
begin
  H := OhnoTable[aSection, aLevel];
  Sec := H div 10;
  Lev := H mod 10;
  Fn := 'Dlvel' + LeadZeroStr(Sec, 3) + '.dat';

  Arc := TDosDatArchive.Create;
  Arc.LoadFromFile(IncludeCommonPath(Fn));
//        for j := 0 to arc.SectionList.count-1 do
  begin
    mem:=TMemoryStream.create;
    Arc.ExtractSection(Lev, mem);
    mem.seek(0,sofrombeginning);
    mem.Read(lvl, 2048);
    result := Trim(lvl.levelname);  //ulog
    mem.free;
  end;
  Arc.Free;
end;

function TDosOhNoStyle.InternalGetSectionCount(aLevelPack: Integer): Integer;
begin
  Result := 5;
end;

function TDosOhNoStyle.InternalGetSectionName(aLevelPack, aSection: Integer): string;
begin
  case aSection of
    0: Result := 'Tame';
    1: Result := 'Crazy';
    2: Result := 'Wild';
    3: Result := 'Wicked';
    4: Result := 'Havoc';
    else raise Exception.Create('DosOhNo invalid section ' + i2s(aSection));
  end;
end;

function TDosOhNoStyle.InternalGetLevelCount(aLevelPack, aSection: Integer): Integer;
begin
  Result := 20;
end;

procedure TDosOhNoStyle.InternalGetLevelInfo(aLevelPack, aSection, aIndex: Integer; out Info: TLevelInfoRec);
var
  LVL: TLVLFileRec;
begin
  inherited; // clear info
  GetLevel(LVL, aSection + 1, aIndex + 1); // we work 1-base internally
  Info.iSection := aSection;
  Info.iLevel := aIndex;
  Info.iTitle := Trim(LVL.LevelName);
end;

procedure TDosOhNoStyle.InternalLoadLevel(aLevelPack, aSection, aIndex: Integer; aLevel: TLevel);
var
  LVL: TLVLFileRec;
begin
  Inc(aSection);
  Inc(aIndex);
  GetLevel(LVL, aSection, aIndex);
  aLevel.Style := Self;
  aLevel.LoadFromLVLRec(LVL);
end;

procedure TDosOhNoStyle.makelist;
var
  i,j: Integer;
  F: string;
  Arc: TDosDatArchive;
  mem :TMemoryStream;
  lvl: TLVLFileRec;
  s:string;

  check: tstringlist;
  ix:integer;

    function find(const s: string): integer;
    begin
      for result := 0 to check.count-1 do
        if fastposnocase(check[result], s, length(check[result]), length(s), 1) > 0 then
          exit;
      result := -1;
    end;

begin

  check := tstringlist.create;
  check.loadfromfile(apppath+'ohno.ini');

  for i := 0 to 12 do
  begin
    F := CommonPath + 'DLVEL' + LeadZeroStr(i, 3) + '.DAT';
    Arc := TDosDatArchive.Create;
    Arc.LoadFromFile(f);
    deb([ExtractFileName(F) + '------------']);
    for j := 0 to arc.SectionList.count-1 do
    begin
      mem:=TMemoryStream.create;
      Arc.ExtractSection(j, mem);
      mem.seek(0,sofrombeginning);
      mem.Read(lvl, 2048);
      s := TrimRight(lvl.levelname);  //ulog
      //deb(['extract', ExtractFilename(F), j, s]);
      ix:= find(trim(s));
      if ix > 0 then
        check[ix] := check[ix] + 'in file: ' + ExtractFilename(F) + ' at: ' + i2s(j);

      //if ix<0 then deb(['not found ' + trim(s)]) else deb(['found ' + trim(s)]);

      deb([leadzerostr(i, 3), leadzerostr(j, 3), i * 8 + i mod 8]);

      mem.free;
    end;
    Arc.Free;
  end;

  check.savetofile(apppath + 'ohno.txt');
  check.free;

end;

procedure TDosOhNoStyle.InternalPrepareLevelLoading;
const
  LocalSectionNames: array[0..4] of string = ('Tame', 'Crazy', 'Wild', 'Wicked', 'Havoc');
var
  iSection, iLevel: Integer;
  Pack: TLevelPack;
  Section: TPackSection;
  LevelInfo: TLevelInfo;
  Fn1, Fn: string;
  IsOdd: Boolean;
  OddIndex: Integer;
  FileIndex: Integer;
  Arc: TDosDatArchive;
begin
  Arc := TDosDatArchive.Create;
  // default pack
  Pack := LevelPacks.Add;
  Pack.LevelPackName := 'Default';
  // sections
  for iSection := 0 to 4 do
  begin
    Section := Pack.PackSections.Add;
    Section.SectionName := LocalSectionNames[iSection];
    // levels
    for iLevel := 0 to 19 do
    begin
      GetLevelFile(iSection, iLevel, Fn, FileIndex);
      Fn1 := Fn;
      Fn := IncludeCommonPath(Fn);
      LevelInfo := Section.LevelInfos.Add;
//      LevelInfo.iOwnerFile := '';
      LevelInfo.DosLevelPackFileName := Fn;
      LevelInfo.DosLevelPackIndex := FileIndex;
//      LevelInfo.DosIsOddTableClone := False;
//      LevelInfo.iDosOddTableIndex := -1;
      Arc.LoadFromFile(Fn);
      LevelInfo.TrimmedTitle := Trim(Arc.ExtractLevelTitle(FileIndex));
      with LevelInfo do
        deb([section.sectionname + '_' + i2s(iLevel) + '=' + Fn1 + ',' + i2s(FileIndex) + ',' + i2s(OddIndex)]);
//      'level ' + i2s(iLevel + 1); // lemtools
//      LevelInfo.iOwnerIndex
      //raise exception.create('ERROR NOT FINISHED'); //kernel_resource
    end;
  end;

  Arc.Free;
end;

procedure TDosOhNoStyle.GetLevelFile(aSection, aLevel: Integer;
  var aFileName: string; var aIndex: Integer);
var
  H, Sec, Lev: Integer;
begin
  H := OhnoTable[aSection + 1, aLevel + 1];
  Sec := H div 10;
  Lev := H mod 10;
  aFilename := 'Dlvel' + LeadZeroStr(Sec, 3) + '.dat';
  aIndex := Lev;
end;



function TDosHoliday93Style.GetLevel(var LVL: TLVLFileRec; aSection, aLevel: Integer): string;
var
tot,  s, i: integer;
  fn:string;
  ix:integer;
  cnt: integer;


var
      Arc: TDosDatArchive;
      mem :TMemoryStream;

  tit:string;
begin

  case aSection of
    1:
      begin
        case aLevel of
          1..8  :
            begin
              Fn := 'LEVEL000.DAT';
              Ix := aLevel - 1;
            end;
          9..16 :
            begin
              Fn := 'LEVEL001.DAT';
              Ix := aLevel - 9;
            end;
        end;
      end;
    2:
      begin
        case aLevel of
          1..8  :
            begin
              Fn := 'LEVEL002.DAT';
              Ix := aLevel - 1;
            end;
          9..16 :
            begin
              Fn := 'LEVEL003.DAT';
              Ix := aLevel - 9;
            end;
        end;
      end;
  end;

  Arc := TDosDatArchive.Create;
  Arc.LoadFromFile(IncludeCommonPath(fn));
//        for j := 0 to arc.SectionList.count-1 do
  begin
    mem:=TMemoryStream.create;
    Arc.ExtractSection(ix, mem);
    mem.seek(0,sofrombeginning);
    mem.Read(lvl, 2048);
    result := Trim(lvl.levelname);  //ulog
    mem.free;
  end;
  Arc.Free;
end;

procedure TDosXMas93Style.GetLevelFile(aSection, aLevel: Integer;
  var aFileName: string; var aIndex: Integer);
begin

  case aSection of
    1:
      begin
        case aLevel of
          1..8  :
            begin
              aFileName := 'LEVEL000.DAT';
              aIndex := aLevel - 1;
            end;
          9..16 :
            begin
              aFileName := 'LEVEL001.DAT';
              aIndex := aLevel - 9;
            end;
        end;
      end;
    2:
      begin
        case aLevel of
          1..8  :
            begin
              aFileName := 'LEVEL002.DAT';
              aIndex := aLevel - 1;
            end;
          9..16 :
            begin
              aFileName := 'LEVEL003.DAT';
              aIndex := aLevel - 9;
            end;
        end;
      end;
  end;

end;

procedure TDosXMas93Style.InternalLoadLevel(aLevelPack, aSection, aIndex: Integer; aLevel: TLevel);
var
  LVL: TLVLFileRec;
begin
  GetLevel(LVL, aSection + 1, aIndex + 1); // internal one based
  aLevel.Style := Self;
  aLevel.LoadFromLVLRec(LVL);
end;

procedure TDosXMas93Style.InternalPrepareLevelLoading;
const
  LocalSectionNames: array[0..1] of string = ('Flurry', 'Blizzard');
var
  iSection, iLevel: Integer;
  Pack: TLevelPack;
  Section: TPackSection;
  LevelInfo: TLevelInfo;
  Fn: string;
  IsOdd: Boolean;
  OddIndex: Integer;
  FileIndex: Integer;
  Arc: TDosDatArchive;

begin

  Arc := TDosDatArchive.Create;
  // default pack
  Pack := LevelPacks.Add;
  Pack.LevelPackName := 'Default';
  // sections
  for iSection := 0 to 1 do
  begin
    Section := Pack.PackSections.Add;
    Section.SectionName := LocalSectionNames[iSection];
    // levels
    for iLevel := 0 to 15 do
    begin
      GetLevelFile(iSection + 1, iLevel + 1, Fn, FileIndex);
      Fn := IncludeCommonPath(Fn);
      LevelInfo := Section.LevelInfos.Add;
      LevelInfo.DosLevelPackFileName := Fn;
      LevelInfo.DosLevelPackIndex := FileIndex;
      Arc.LoadFromFile(Fn);
      LevelInfo.TrimmedTitle := Trim(Arc.ExtractLevelTitle(FileIndex));
//      'level ' + i2s(iLevel + 1); // lemtools
//      LevelInfo.iOwnerIndex
      //raise exception.create('ERROR NOT FINISHED'); //kernel_resource
    end;
  end;

  Arc.Free;
end;


function TDosXMas91Style.GetLevel(var LVL: TLVLFileRec; aSection, aLevel: Integer): string;
var
tot,  s, i: integer;
  fn:string;
  ix:integer;
  cnt: integer;


var
  Arc: TDosDatArchive;
  mem :TMemoryStream;

  tit:string;
begin
  GetLevelFile(aSection + 1, aLevel + 1, Fn, Ix);
  Arc := TDosDatArchive.Create;
  Arc.LoadFromFile(IncludeCommonPath(fn));
  begin
    mem:=TMemoryStream.create;
    Arc.ExtractSection(ix, mem);
    mem.seek(0,sofrombeginning);
    mem.Read(lvl, 2048);
    result := Trim(lvl.levelname);  //ulog
    mem.free;
  end;
  Arc.Free;
end;

procedure TDosXMas91Style.GetLevelFile(aSection, aLevel: Integer; var aFileName: string; var aIndex: Integer);
begin
  aFileName := 'Level000.dat';
  aIndex := aLevel - 1;
end;

procedure TDosXMas91Style.InternalLoadLevel(aLevelPack, aSection, aIndex: Integer; aLevel: TLevel);
var
  LVL: TLVLFileRec;
begin
  GetLevel(LVL, aSection + 1, aIndex + 1); // internal one based
//  aLevel.BeginUpdate;
  aLevel.Style := Self;
  aLevel.LoadFromLVLRec(LVL);
end;

procedure TDosXMas91Style.InternalPrepareLevelLoading;
const
  LocalSectionNames: array[0..0] of string = ('1991 levels');
var
  iSection, iLevel: Integer;
  Pack: TLevelPack;
  Section: TPackSection;
  LevelInfo: TLevelInfo;
  Fn: string;
  IsOdd: Boolean;
  OddIndex: Integer;
  FileIndex: Integer;
  Arc: TDosDatArchive;

begin

  Arc := TDosDatArchive.Create;
  // default pack
  Pack := LevelPacks.Add;
  Pack.LevelPackName := 'Default';
  // sections
  for iSection := 0 to 0 do
  begin
    Section := Pack.PackSections.Add;
    Section.SectionName := LocalSectionNames[iSection];
    // levels
    for iLevel := 0 to 3 do
    begin
      GetLevelFile(iSection + 1, iLevel + 1, Fn, FileIndex);
      Fn := IncludeCommonPath(Fn);
      LevelInfo := Section.LevelInfos.Add;
      LevelInfo.DosLevelPackFileName := Fn;
      LevelInfo.DosLevelPackIndex := FileIndex;
      Arc.LoadFromFile(Fn);
      LevelInfo.TrimmedTitle := Trim(Arc.ExtractLevelTitle(FileIndex));
//      'level ' + i2s(iLevel + 1); // lemtools
//      LevelInfo.iOwnerIndex
      //raise exception.create('ERROR NOT FINISHED'); //kernel_resource
    end;
  end;

  Arc.Free;
end;


(*
  DoAdd($0000, 'walking'              ,   8, 16, 10, 2,      9,  9,  4); // 9 = probably ok
  DoAdd($0140, 'jumping'              ,   1, 16, 10, 2,      9,  9,  4,  1);
  DoAdd($0168, 'walking (rtl)'        ,   8, 16, 10, 2,      8,  9,  4); // 8 = probably ok
  DoAdd($02A8, 'jumping (rtl)'        ,   1, 16, 10, 2,      8,  9,  4);
  DoAdd($02D0, 'digging'              ,  16, 16, 14, 3,      0,  0,  0);
  DoAdd($0810, 'climbing'             ,   8, 16, 12, 2,      7, 11,  4); // 7=ok
  DoAdd($0990, 'climbing (rtl)'       ,   8, 16, 12, 2,      7, 11,  4); // 7=ok
  DoAdd($0B10, 'drowning'             ,  16, 16, 10, 2,      0,  0,  0);
  DoAdd($0D90, 'post-climb'           ,   8, 16, 12, 2,      7, 11,  4,  1);
  DoAdd($0F10, 'post-climb (rtl)'     ,   8, 16, 12, 2,      0,  0,  0);
  DoAdd($1090, 'brick-laying'         ,  16, 16, 13, 3,      9,  12,  0);
  DoAdd($1570, 'brick-laying (rtl)'   ,  16, 16, 13, 3,      8,  12,  0);
  DoAdd($1A50, 'bashing (r)'          ,  32, 16, 10, 3,      0,  0,  0);
  DoAdd($21D0, 'bashing (l)'          ,  32, 16, 10, 3,      0,  0,  0);
  DoAdd($2950, 'mining (r)'           ,  24, 16, 13, 3,      0,  0,  0);
  DoAdd($30A0, 'mining (l)'           ,  24, 16, 13, 3,      0,  0,  0);
  DoAdd($37F0, 'falling'              ,   4, 16, 10, 2,      8,  9,  4); // 8=ok
  DoAdd($3890, 'falling (rtl)'        ,   4, 16, 10, 2,      8,  9,  4); // 8=ok
  DoAdd($3930, 'pre-umbrella (r)'     ,   4, 16, 16, 3,      8, 14,  4);
  DoAdd($3AB0, 'umbrella (r)'         ,   4, 16, 16, 3,      8, 14,  4);
  DoAdd($3C30, 'pre-umbrella (l)'     ,   4, 16, 16, 3,      7, 14,  4);
  DoAdd($3DB0, 'umbrella (l)'         ,   4, 16, 16, 3,      7, 14,  4);
  DoAdd($3F30, 'splatting'            ,  16, 16, 10, 2,      8,  9,  4,  1);
  DoAdd($41B0, 'exiting'              ,   8, 16, 13, 2,      0,  0,  0);
  DoAdd($4350, 'fried'                ,  14, 16, 14, 4,      0,  0,  0);
  DoAdd($4970, 'blocking'             ,  16, 16, 10, 2,      9,  9,  4);
  DoAdd($4BF0, 'shrugging'            ,   8, 16, 10, 2,      9,  9,  0);
  DoAdd($4D30, 'shrugging (l)'        ,   8, 16, 10, 2,      0,  0,  0);
  DoAdd($4E70, 'oh-no-ing'            ,  16, 16, 10, 2,      0,  0,  0);
  DoAdd($50F0, 'explosion'            ,   1, 32, 32, 3,      0,  0,  0);
*)

