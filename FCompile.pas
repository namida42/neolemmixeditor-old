{$include lem_directives.inc}

unit FCompile;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,
  LemCore, ComCtrls,
  UEvents, LemProgress;

type
  TFormCompile = class(TForm)
    Memo: TMemo;
    ProgressBar1: TProgressBar;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
//    PrevState: TArchiveCompilerStateRec;
    fLink: TMessageLink;
    fCancelled: Boolean;
    procedure LMProgress(var Msg: TLMProgress); message LM_PROGRESS;
    procedure Link_ManagerChange(Sender: TObject);
  public
    procedure ArchiveCompiler_Progress(Sender: TObject; const aState: TArchiveCompilerStateRec);
    property Link: TMessageLink read fLink;
  end;

//function FormCompile: TFormCompile;

implementation

uses DateUtils;

{$R *.dfm}


function FormCompile: TFormCompile;
var
  i: Integer;
begin
  with Screen do
  for i := 0 to CustomFormCount - 1 do
    if CustomForms[i] is TFormCompile then
    begin
      Result := TFormCompile(CustomForms[i]);
      Exit;
    end;

  Result := TFormCompile.Create(Application);

end;

{ TFormCompile }

procedure TFormCompile.ArchiveCompiler_Progress(Sender: TObject;
  const aState: TArchiveCompilerStateRec);
begin
(*  With astate, ProgressBar1 do
  begin
     if max <> cmaximum then
       max:=cmaximum;
     if Position <> cposition then
       position:=cposition;
     if prevstate.cState<>cstate then
       memo.Lines.add(cstate);
     if prevstate.cDetail<>cdetail then
       memo.Lines.add(cdetail);
      memo.CaretPos := Point(0, memo.lines.count-1)
  end;
  prevstate:=astate;
     update;
 *)
end;


procedure TFormCompile.FormCreate(Sender: TObject);
begin
  fLink := TMessageLink.Create(nil, Self);
  fLink.OnManagerChange := Link_ManagerChange;
end;

procedure TFormCompile.FormDestroy(Sender: TObject);
begin
  fLink.Free;
end;

procedure TFormCompile.LMProgress(var Msg: TLMProgress);
var
  P: TCustomProgressManager;
begin

  P := Msg.Progress;
  if fCancelled then
    p.UserCancel := true;
  With ProgressBar1 do
  begin
     if p.PrevMaximum <> p.maximum then
       max:=p.maximum;
     if p.prevPosition <> p.position then
       position:=p.position;
     if p.prevstate<>p.state then
       memo.Lines.add(p.state);
     if p.PrevDetail<>p.detail then
       memo.Lines.add(p.detail);
      memo.CaretPos := Point(0, memo.lines.count-1)
  end;
  update;
 //
end;

procedure TFormCompile.Button2Click(Sender: TObject);
begin
//  ShowMessage('Abort not implemented')
//  fCancelled := True;
end;

procedure TFormCompile.Button1Click(Sender: TObject);
begin
  close;
end;

procedure TFormCompile.Link_ManagerChange(Sender: TObject);
begin
//  showmessage('masterchange');
  memo.lines.clear;
  with ProgressBar1 do
  begin
    max:=0;
    position:=0;
  end;
end;

procedure TFormCompile.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  Action := caFree;
end;

end.
//lemfiles
