object MapEditorForm: TMapEditorForm
  Left = 889
  Top = 188
  Width = 798
  Height = 589
  Caption = 'Map Editor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnContextPopup = FormContextPopup
  OnCreate = Form_Create
  OnDestroy = Form_Destroy
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnKeyUp = FormKeyUp
  OnShow = Form_Show
  PixelsPerInch = 96
  TextHeight = 13
  object Img: TImgView32
    Left = 0
    Top = 29
    Width = 782
    Height = 476
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCustom
    Centered = False
    Color = clGray
    Constraints.MinHeight = 160
    ParentColor = False
    PopupMenu = MapPopup
    Scale = 2.000000000000000000
    ScaleMode = smScale
    ScrollBars.ShowHandleGrip = True
    ScrollBars.Style = rbsDefault
    ScrollBars.Size = 17
    SizeGrip = sgNone
    OverSize = 16
    TabOrder = 0
    TabStop = True
    OnDragDrop = Img_DragDrop
    OnDragOver = Img_DragOver
    OnEndDrag = Img_EndDrag
    OnKeyDown = Img_KeyDown
    OnKeyPress = Img_KeyPress
    OnMouseDown = Img_MouseDown
    OnMouseMove = Img_MouseMove
    OnMouseUp = Img_MouseUp
    OnMouseWheelDown = Img_MouseWheelDown
    OnMouseWheelUp = Img_MouseWheelUp
    OnStartDrag = Img_StartDrag
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 511
    Width = 782
    Height = 19
    Panels = <
      item
        Width = 80
      end
      item
        Width = 64
      end
      item
        Width = 64
      end
      item
        Width = 64
      end
      item
        Width = 132
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 782
    Height = 29
    ButtonHeight = 21
    ButtonWidth = 24
    Caption = 'ToolBar1'
    TabOrder = 2
    object ToolButton1: TToolButton
      Left = 0
      Top = 2
      Caption = 'ToolButton1'
      ImageIndex = 0
    end
    object ToolButton2: TToolButton
      Left = 24
      Top = 2
      Caption = 'ToolButton2'
      ImageIndex = 1
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionList_Update
    Left = 32
    Top = 56
  end
  object MainMenu: TMainMenu
    Left = 32
    Top = 96
    object MbFile: TMenuItem
      Caption = '&File'
    end
  end
  object MapPopup: TPopupMenu
    AutoPopup = False
    Left = 32
    Top = 176
  end
  object ImageList: TImageList
    Left = 104
    Top = 53
  end
end
