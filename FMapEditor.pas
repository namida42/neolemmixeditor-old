{$include lem_directives.inc}

unit FMapEditor;

{-------------------------------------------------------------------------------
  One of the possible Lemmix mainforms: the level editor. The other mainform is
  planned to be a lemming-playing form.
  o There are several develop forms. they are *NOT* allowed in exeversions!
  o The only designtime menuitem is the "file" item, just to see
    something designtime. The rest is created runtime.
-------------------------------------------------------------------------------}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, TypInfo,
  ClipBrd, FileCtrl, IniFiles, Dialogs, Math,
  Types, Menus, ActnList, ComCtrls, ExtCtrls, StdCtrls,
  // kernel + kernel 3dparty
  UMasterForms, UMisc, UEvents, UFiles, UTools, UWinTools, USplit,
  GR32, GR32_Image, GR32_Layers, GifImage,
  // develop
  {$ifdef develop}
{lemreplay, lemdosarc,lemdoscmp,  lemdevelop,
  LemConvert, FTest, FShowSPRFiles, FShowDOSGroundFile,}
  {$endif}
  // lemmings units
  LemDosFiles, LemStyles, LemStrings, LemTypes, LemFiles, LemMisc, LemCore, LemEdit,
  FEditItem, FEditStatics, FEditSkills, FLevelMatrix, FAbout, FBasicLevelEditor,
  {FEditSysDat,}
  FPickTerrain, FAlignSelection, FNewLevel, FPasteSpecial, FShiftLevel,
  FDosDatDecompression, FDosDecompress, //FViewDosLevelPack,
  FGSDownload,
  FWizLemminiCompiler, FAskStyle, FVgaSpecCreator, FValidationReport, FMassConvert,
  FVgaSpecViewer, FCompileStyle,
  LemNeoEncryption,
  // lemmings forms
  LemDosStyles, LemLemminiStyles, LemGraphics,
  FEditDosLevelPack,
  FOptions,
  ToolWin, ImgList, USplitPanels, StrUtils{, ShellApi};
  {fdraw, }

type
  TMapEditorForm = class;

  TClickAction = (
    caNone,
    caSelect
  );

  (*
  {-------------------------------------------------------------------------------
    Class to keep track of different modes of loading.
    The form keeps an instance of this class so that the form is aware of this
    edit-mode.
    So when loading a level in the editor
  -------------------------------------------------------------------------------}
  TLevelEditController = class
  private
  protected
  public
  published
  end;
  *)

  TMapEditorForm = class(TBasicLevelEditorForm)
    Img: TImgView32;
    StatusBar: TStatusBar;
    ActionList: TActionList;
    MainMenu: TMainMenu;
    MbFile: TMenuItem;
    MapPopup: TPopupMenu;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ImageList: TImageList;
    procedure Form_Create(Sender: TObject);
    procedure Form_Destroy(Sender: TObject);
    procedure Form_Show(Sender: TObject);
    procedure Form_CloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Img_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Img_KeyPress(Sender: TObject; var Key: Char);
    procedure Img_MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure Img_MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
    procedure Img_MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure Img_MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure Img_MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
    procedure Img_StartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure Img_DragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure Img_DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure Img_EndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure FormContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
    procedure ActionList_Update(Action: TBasicAction; var Handled: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    fRenderer                  : TLevelRenderer;
    fLocked                    : Boolean;
    fHitList                   : TList;
    fScaleIndex                : Integer;
    fDragger                   : TDragObject;
    fSelectionLayer            : TPositionedLayer; // the gr32_image layer to paint selectionrectangles
    fScreenStartLayer          : TPositionedLayer; // the gr32_image layer to paint the screenstart of the level
    fDrawingSelectionRect      : Boolean;
    fSelectionRect             : TRect;
    fSelectionRectVisible      : Boolean;
//    fCurrentClickTerrain       : Integer;
    DragSelection              : Boolean;
    DragIP                     : TPoint; // TPoint startdrag als Image coordinaat
    DragBP                     : TPoint; // TPoint startdrag als Bitmap coordinaat
//    fDragBitmap                : TBitmap32;
    LastHitItem                : TBaseLevelItem;
    fImageIsDragging           : Boolean;
    fMouseOnSelection          : Boolean;
    fCommandStr                : string;
    fCommandChangeTicks        : Cardinal;
//    fCopyPoint                 : TPoint;
    fInternalGettingDragBitmap : Boolean;
    fSelectorColors            : array[litObject..litSteel] of TColor32;
    fStaticsForm               : TFormEditStatics;
    fSkillsForm                : TFormEditSkills;
    //fSysdatForm                : TEditSysDatForm;
    fListForm                  : TLevelListEditorForm;
    fEditItemForm              : TEditItemForm;
  { menubar }
    MbEdit                     : TMenuItem; // menubar
    MbSelection                : TMenuItem;
    MbView                     : TMenuItem;
//    MbStyles                   : TMenuItem;
    MbTools                    : TMenuItem;
    MbHelp                     : TMenuItem;
    {$ifdef develop}
    MhRecentFiles              : TMenuItem; // menuholder recentfiles
    MbDevelop                  : TMenuItem;
    {$endif}
  { actions }
  { file ... }
    ActNewLevel                : TAction;
    ActOpenFile                : TAction;
    ActReopenFile              : TAction;
    //ActCloseLevel              : TAction;
    ActSave                    : TAction;
    ActSaveAs                  : TAction;
    ActEncryptFile             : TAction;
    ActOptions                 : TAction;
    ActQuit                    : TAction;
  { edit ... }
    ActUndo                    : TAction;
    ActRedo                    : TAction;
    ActInsertObject            : TAction;
    ActInsertObjectLocal       : TAction; // local menu
    ActInsertTerrain           : TAction;
    ActInsertTerrainLocal      : TAction; // local menu
    ActInsertSteel             : TAction;
    ActInsertSteelLocal        : TAction; // local menu
    //ActSetScreenPosition       : TAction;
    ActSetScreenPositionLocal  : TAction;
    ActCopy                    : TAction;
    ActCut                     : TAction;
    ActPaste                   : TAction;
    ActPasteSpecial            : TAction;
    ActShiftLevel              : TAction;
    ActImportLayout            : TAction;
  { selection ... }
    ActAlignSelection          : TAction;
    ActToFront                 : TAction;
    ActToBack                  : TAction;
  { view ... }
    ActShowInspector           : TAction;
    ActShowStatics             : TAction;
    ActShowSkills              : TAction;
    ActShowList                : TAction;
    ActShowImage               : TAction;
    ActToggleTerrain           : TAction;
    ActToggleObjects           : TAction;
    ActToggleBackground        : TAction;
    ActToggleSteel             : TAction;
    ActToggleScreenStart       : TAction;
    ActToggleTriggerAreas      : TAction;
    ActZoomIn                  : TAction;
    ActZoomOut                 : TAction;
  { styles ... }
    //ActStyleCompiler           : TAction;
    //ActAddStyle                : TAction;
  { tools ... }
    ActValidateLevel           : TAction;
    ActDosDatDecompress        : TAction;
    ActDownloadStyles          : TAction;
    ActShowSysdat              : TAction;
    ActPlay                    : TAction;
    ActMassConvert             : TAction;
  { help ... }
    ActAbout                   : TAction;
  { level messages }
    procedure LMLevelChanging(var Msg: TLMLevelChanging); message LM_LEVELCHANGING;
    procedure LMLevelChanged(var Msg: TLMLevelChanged); message LM_LEVELCHANGED;
    procedure LMStaticsChanged(var Msg: TLMStaticsChanged); message LM_STATICSCHANGED;
    procedure LMObjectListChanged(var Msg: TLMObjectListChanged); message LM_OBJECTLISTCHANGED;
    procedure LMTerrainListChanged(var Msg: TLMTerrainListChanged); message LM_TERRAINLISTCHANGED;
    procedure LMSteelListChanged(var Msg: TLMSteelListChanged); message LM_STEELLISTCHANGED;
    procedure LMSelectionChanged(var Msg: TLMSelectionChanged); message LM_SELECTIONCHANGED;
  { internal methods image editing }
    procedure SelectAllUnderRect(const aRect: TRect);
    function GetSelectionDragBitmap(Bmp: TBitmap; var HotX, HotY: Integer): Boolean;
    function LevelActive: Boolean;
    function ZoomIn(MouseX: Integer = MinInt; MouseY: Integer = MinInt): Boolean;
    function ZoomOut(MouseX: Integer = MinInt; MouseY: Integer = MinInt): Boolean;
    function DoZoom(NewZoom: Integer; MouseX: Integer = MinInt; MouseY: Integer = MinInt): Boolean;
//    procedure UpdateScreenCaption;
    procedure UpdatePanelResolution;
    procedure UpdatePanelScale;
    procedure UpdateSelectionLayer;
    function CalcCenteredInsertionPoint: TPoint;
    procedure CreateActionsAndMenus;
    function MapScaleIndex(const aScale: Single): Integer;
    procedure HandleCommandStr;
    procedure Rerender;
    function CheckCanDrag: Boolean;
  { internal methods }
    procedure InternalNewLevel(S: TBaseLemmingStyle; G: TBaseGraphicSet);
    procedure InternalPlayLevel(aLevel: TLevel);
    procedure InternalInsertObject(P: TPoint);
    procedure InternalInsertTerrain(P: TPoint);
    procedure InternalInsertSteel(P: TPoint);
    procedure InternalOpenFile(const aFileName: string; aType: Integer);
    procedure InternalImportLayout(const aFileName: String; aType: Integer);
  { private eventhandlers }
    procedure SelectionLayer_Paint(Sender: TObject; Buffer: TBitmap32);
    procedure ScreenStartLayer_Paint(Sender: TObject; Buffer: TBitmap32);
    procedure Form_DropFiles(Sender: TObject; const Files: TStrings);
  { app events }
  { action eventhandlers }
  { file ... }
    procedure ActNewLevel_Execute(Sender: TObject);
    procedure ActOpenFile_Execute(Sender: TObject);
    procedure ActOpenRecentFile_Execute(Sender: TObject);
    //procedure ActCloseLevel_Execute(Sender: TObject);
    procedure ActQuit_Execute(Sender: TObject);
    procedure ActSaveAs_Execute(Sender: TObject);
    procedure ActSave_Execute(Sender: TObject);
    procedure ActEncryptFile_Execute(Sender: TObject);
    procedure ActOptions_Execute(Sender: TObject);
  { edit ... }
    procedure ActUndo_Execute(Sender: TObject);
    procedure ActRedo_Execute(Sender: TObject);
    procedure ActInsertTerrain_Execute(Sender: TObject);
    procedure ActInsertObject_Execute(Sender: TObject);
    procedure ActInsertSteel_Execute(Sender: TObject);
    procedure ActSetScreenPosition_Execute(Sender: TObject);
    procedure ActCopy_Execute(Sender: TObject);
    procedure ActCut_Execute(Sender: TObject);
    procedure ActPaste_Execute(Sender: TObject);
    procedure ActPasteSpecial_Execute(Sender: TObject);
    procedure ActShiftLevel_Execute(Sender: TObject);
    procedure ActImportLayout_Execute(Sender: TObject);
  { selection ... }
    procedure ActAlignSelection_Execute(Sender: TObject);
    procedure ActToFront_Execute(Sender: TObject);
    procedure ActToBack_Execute(Sender: TObject);
  { view ... }
    procedure ActShowInspector_Execute(Sender: TObject);
    procedure ActShowStatics_Execute(Sender: TObject);
    procedure ActShowSkills_Execute(Sender: TObject);
    procedure ActShowList_Execute(Sender: TObject);
    procedure ActShowImage_Execute(Sender: TObject);
    procedure ActToggleTerrain_Execute(Sender: TObject);
    procedure ActToggleObjects_Execute(Sender: TObject);
    procedure ActToggleSteel_Execute(Sender: TObject);
    procedure ActToggleBackground_Execute(Sender: TObject);
    procedure ActToggleTriggerAreas_Execute(Sender: TObject);
    procedure ActToggleScreenStart_Execute(Sender: TObject);
    procedure ActZoomIn_Execute(Sender: TObject);
    procedure ActZoomOut_Execute(Sender: TObject);
  { styles ... }
    //procedure ActStyleCompiler_Execute(Sender: TObject);
  { tools ... }
    procedure ActValidateLevel_Execute(Sender: TObject);
    procedure ActDosDatDecompress_Execute(Sender: TObject);
    procedure ActDownloadStyles_Execute(Sender: TObject);
    procedure ActPlay_Execute(Sender: TObject);
    procedure ActMassConvert_Execute(Sender: TObject);
  { help ... }
    procedure ActAbout_Execute(Sender: TObject);

  {$ifdef develop}
    procedure ActTest_Execute(Sender: TObject);
    procedure ActShowSpriteFiles_Execute(Sender: TObject);
    procedure ActShowDOSGroundFile_Execute(Sender: TObject);
    procedure ActPackExtGraphics_Execute(Sender: TObject);
    procedure ActWizardLemmini_Execute(Sender: TObject);
  {$endif}

    procedure SetCommandStr(const Value: string);
    property CommandStr: string read fCommandStr write SetCommandStr;

  protected
    fCC: string;
    procedure DoAfterSetEditor(Value: TLevelEditor); override;

  public


//    procedure SetEditInfo(Who: TObject);
    procedure UpdateScreenCaption;

  end;

var
  GlobalEditorForm: TMapEditorForm;

implementation

{$R *.dfm}

uses
  LemApp, LemDialogs;

const
  panel_resolution = 0;
  panel_scale = 1;
  panel_imgcoord = 2;
  panel_levelcoord = 3;
  panel_selection = 4;
  panel_commandstr = 5;

//  OFD_ALL = 1;
  OFD_RAWLEVEL = 1;
  OFD_LEMMINI = 2;
  OFD_DOSLEVELPACK = 3;
  OFD_VGASPEC = 4;

  {OFD_DOSLEVELXXX = 6;
  OFD_DOSGROUNDXXDAT = 7;
  OFD_VGAGRXDAT = 8;
  OFD_UNKNOWNDAT = 9; }

  OpenDlgFilters : array[1..4] of string = (
//    'All files (*.*)|*.*|', // ofd_all
    'Lemmix level file (*.lvl)|*.lvl|', // ofd_rawlevel
    'Lemmini level file (*.ini)|*.ini|', // ofd_lemmini
    'Lemmix DAT level pack (*.dat)|*.dat|', // ofd_doslevelpack
    'VGASPEC file (vgaspec*.dat)|vgaspec*.dat|'
  );
//    'DOS Ground info file (ground??.dat)|ground??.dat|',
  //  'DOS VGA info file (vgagr?.dat)|vgagr?.dat|',
    //'DOS Generic DAT file (*.dat)|*.dat|'
//  );

  SaveAsFilters : array[1..3] of string = (
    'Lemmini level file (*.ini)|*.ini|',
    'Lemmix level file (*.lvl)|*.lvl|',
    'Image file (*.png)|*.png|'
  );


  // dos original levelpack
  // dos original extracted level

  // dos ohno levelpack
  // dos ohno extracted level

  //

  // winlemmings low resolution extracted level
  // winlemmings high resolution extracted level

  // lemmini level

type
  TFileType = (
    ftNone,
    ftLevel,
    ftLevelPack
  );



const
  AVAILABLESCALECOUNT = 11;

  AvailableScales: array[0..AVAILABLESCALECOUNT - 1] of Single = (
    0.125,
    0.25,
    0.5,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8
  );

const
//  ScaleStrings: array[0..10] of string =

  AvailableScaleNames : array[0..AVAILABLESCALECOUNT - 1] of string = (
    '1:8', '1:4', '1:2', '1:1', '2:1', '3:1', '4:1', '5:1', '6:1', '7:1', '8:1'
  );


type
  TEditorDragObject = class(TDragControlObject)
  private
    fDragImageList: TDragImageList;
    fEditorForm: TMapEditorForm;
//    fDragX, fDragY: Integer;
  protected
    function GetDragImages: TDragImageList; override;
  public
    constructor Create(aControl: TControl); override;
    destructor Destroy; override;
  published
  end;

  TRecentFileAction = class(TAction)
  private
    fFileType: string; // Level, LevelPack
    fFileInfo: string; // style classname if it's a level
    fFileName: string;
    fFileFormat: string;
  protected
  public
    property FileName: string read fFileName;
    property FileType: string read fFileType;
    property FileInfo: string read fFileInfo;
    property FileFormat: string read fFileFormat;
//    property FileInfo: string read fFileInfo write fFileInfo;
  published
  end;

  TFileTypeInfo = class
  private
    fDefaultFileMask: string;
  protected
  public
    property DefaultFileMask: string read fDefaultFileMask;
  published
  end;

  TFileMgr = class
  private
    fFileTypeList: TStringList;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    property FileTypeList: TStringList read fFileTypeList;
//    property utools
  published
  end;


{ TEditorDragObject }

constructor TEditorDragObject.Create(aControl: TControl);
begin
  inherited;
end;

destructor TEditorDragObject.Destroy;
begin
  //windlg('drag.destroy');
  fDragImageList.Free;
  inherited;
end;

function TEditorDragObject.GetDragImages: TDragImageList;
var
  Bmp: TBitmap;
  Ix: Integer;
//  Buf, Scaler: TBitmap32;
  hx,hy:integer;
begin
//  Result := nil; exit;

  if fDragImageList = nil then
    fDragImageList := TDragImageList.Create(nil);

  Result := fDragImageList;
  Result.Clear;
  begin

//    ix:=0;
    with fDragImageList do
    if Count = 0 then
    begin
      Bmp := TBitmap.Create;
      //Scaler := TBitmap32.Create;

      fEditorForm.GetSelectionDragBitmap(bmp, hx, hy);
      {
      bmp.width := 50;
      bmp.height := 50;
      bmp.canvas.Brush.color := clred;
      bmp.canvas.fillrect(rect(0, 0, 50, 50));
      }

      {Buf := PictureMgr.TerrainBitmaps[fGraphicSet, fTerrain.TerrainId];
      Scaler.Width := Buf.Width * 2;
      Scaler.Height := Buf.Height * 2;
      Scaler.Draw(Rect(0, 0, buf.Width * 2, buf.height * 2), rect(0,0,buf.width, buf.Height), buf);
      Bmp.Assign(Scaler);
      Scaler.Free;}



      Width := Bmp.Width;
      Height := Bmp.Height;
//      Deb([width]);
      Ix := Result.AddMasked(Bmp, clBlack);
      //Deb([ix]);
      Bmp.Free;
//Deb(['ok']);
//      Width := TestImg.Picture.Bitmap.Width;
  //    Height := TestImg.Picture.Bitmap.Height;
    //  Add(TestImg.Picture.Bitmap, nil);

      Result.SetDragImage(Ix, hx, hy);//fEditorForm.DragP.X, fEditorForm.DragP.Y);
    end;

//    Deb([ix]);


  end;

end;

{ TFileMgr }

constructor TFileMgr.Create;
//var
  //i: Integer;
begin
  inherited;
  fFileTypeList := TStringList.Create;
  fFileTypeList.Add('level,lvl');
  fFileTypeList.Add('level,lemmini');
  fFileTypeList.Add('levelpack,dos');

end;

destructor TFileMgr.Destroy;
begin
  fFileTypeList.Free;
  inherited;
end;

{ TMapEditorForm }

procedure TMapEditorForm.Form_Create(Sender: TObject);
{-------------------------------------------------------------------------------
  So what do we need:
  o Objects
    o A Link that sends change notifications from the leveleditor.
    o A Bitmap renderer to display a lemming level.
    o A HitList for mouse selection.
    o A Selection layer for displaying selection on top of the image.
    o A ScreenStart layer for displaying the initial screen
    o A Dragobject for dragging selection around the image.
    o Inspector forms
  o Dragging:
    o Let the form and all its subcontrols display dragimages.
    o Accept dragged files for opening a level.
  o Display:
    o Colors for selection
  o Actions, mainmenu, local menus
-------------------------------------------------------------------------------}
begin
  fRenderer := TLevelRenderer.Create(Self);
  fRenderer.RenderOptions := fRenderer.RenderOptions -
                             [roCollisionGridBuilding{, roInitialScreenPosRect}] + [roSteelNoOverwrite];
//                             + [roShowTriggerAreas];
  fRenderer.LevelBitmap := Img.Bitmap;
  //fRenderer.Resolution := GlobalResolution;
  //fRenderer.OnRenderItem := Renderer_RenderItem;
  fHitList := TList.Create;
  fScaleIndex := MapScaleIndex(App.ScreenSettings.EditorScale);
  Img.Scale := AvailableScales[fScaleIndex];
  Img.Height := ClientHeight - StatusBar.Height - Toolbar1.Height; //iif(App.EditorSettings.MapHeight > 0, App.EditorSettings.MapHeight, Img.Height);
  Img.Options := [pboWantArrowKeys, pboAutoFocus];
  fSelectionLayer := TPositionedLayer.Create(Img.Layers);
  fSelectionLayer.MouseEvents := False;
  fSelectionLayer.Scaled := True;
  fSelectionLayer.OnPaint := SelectionLayer_Paint;
  fScreenStartLayer := TPositionedLayer.Create(Img.Layers);
  fScreenStartLayer.MouseEvents := False;
  fScreenStartLayer.Scaled := True;
  fScreenStartLayer.OnPaint := ScreenStartLayer_Paint;
  fScreenStartLayer.Visible := False;
  fSelectorColors[litObject] := clLime32;
  fSelectorColors[litTerrain] := clYellow32;
  fSelectorColors[litSteel] := clRed32;
  fStaticsForm := TFormEditStatics.Create(Self);
  fSkillsForm := TFormEditSkills.Create(Self);
  //fSysdatForm := TEditSysdatForm.Create(Self);
  fStaticsForm.SkillsForm := fSkillsForm;
  fListForm := TLevelListEditorForm.Create(Self);
  fEditItemForm := TEditItemForm.Create(Self);
  fDragger := TEditorDragObject.Create(Img);
  TEditorDragObject(fDragger).fEditorForm := Self;
  SetDisplayDragImage(Self);
  AcceptDraggedFiles := True;
  OnDropFiles := Form_DropFiles;
  OnCloseQuery := Form_CloseQuery;
  CreateActionsAndMenus;
  fRenderer.RenderLevel(nil);
end;

procedure TMapEditorForm.Form_CloseQuery(Sender: TObject; var CanClose: Boolean);
var
  Response: Integer;
begin
  if Editor.IsChanged and (Editor.Style <> nil) then
    Response := MessageDlg('Level has been changed. Save level?', mtCustom, [mbYes, mbNo, mbCancel], 0)
  else
    Response := mrNo;

  case Response of
    mrYes: begin
             ActSave_Execute(self);
             CanClose := not Editor.IsChanged;
           end;
    mrNo: CanClose := true;
    else CanClose := false;
  end;
end;

procedure TMapEditorForm.CreateActionsAndMenus;
{-------------------------------------------------------------------------------
  We create the menu + actions at runtime.
-------------------------------------------------------------------------------}

    function NewMenubarItem(const aCaption: string): TMenuItem;
    // local function to add menubar item to the mainmenu
    begin
      Result := TMenuItem.Create(Self);
      Result.Caption := aCaption;
      MainMenu.Items.Add(Result);
    end;

    function NewHolderItem(aParent: TMenuItem; const aCaption: string): TMenuItem;
    // local function to add a menuitem (for holding submenus) to the mainmenu
    begin
      Result := TMenuItem.Create(Self);
      Result.Caption := aCaption;
      aParent.Add(Result);
    end;

    procedure NewSep(aParent: TMenuItem);
    // local function to add a seperator to the main menu
    var
      M: TMenuItem;
    begin
      if aParent <> nil then
      begin
        M := TMenuItem.Create(Self);
        M.Caption := '-';
        aParent.Add(M);
      end;
    end;

    function NewAction(aParent: TMenuItem;
                       const aCaption: string;
                       aExecuteHandler: TNotifyEvent;
                       aShortCut: TShortCut = 0): TAction;

    var
      M: TMenuItem;
    begin
      Result := TAction.Create(Self);
      Result.Caption := aCaption;
      Result.OnExecute := aExecuteHandler;
      Result.ShortCut  := aShortCut;
      Result.ActionList := ActionList;
      if aParent <> nil then
      begin
        M := TMenuItem.Create(Self);
        M.Action := Result;
        aParent.Add(M);
      end;
    end;


    function NewMapPopupAction(aParent: TMenuItem;
                               const aCaption: string;
                               aExecuteHandler: TNotifyEvent): TAction;

    var
      M: TMenuItem;
    begin
      Result := TAction.Create(Self);
      Result.Caption := aCaption;
      Result.OnExecute := aExecuteHandler;
      Result.ActionList := ActionList;
      M := TMenuItem.Create(Self);
      M.Action := Result;
      if aParent <> nil then
        aParent.Add(M)
      else
        MapPopup.Items.Add(M);
    end;


    (*
    procedure AddRecentFiles;
//    const
  //    PrefixChars: array[0..9 +
    var
      Act: TRecentFileAction;
      M: TMenuItem;
      i: Integer;
      C: Char;
      S: string;
    begin
      Assert(MhRecentFiles <> nil, 'recentfiles menu item error');
      with App.RecentFilesSettings do
      begin
        for i := 0 to FileStrings.Count - 1 do
        begin
//          Deb([FileNames[i]]);
          Act := TRecentFileAction.Create(Self);
          S := FileStrings[i];
          Act.fFileName := SplitString(S, 0, ',');
          Act.fFileType := SplitString(S, 1, ',');
          Act.fFileInfo := SplitString(S, 2, ',');
          Act.fFileFormat := SplitString(S, 3, ',');
          if i < 10 then
            C := i2s(i)[1]
          else if i < 36 then
            C := Chr(Ord('A') + i - 10);
          Act.Caption := C + ' ' + Act.FileName;
//          Act.fFileType := FileTypes[i];
//          Act.fFileInfo := FileInfo[i];
          Act.OnExecute := ActOpenRecentFile_Execute;
          M := TMenuItem.Create(Self);
          M.Action := Act;

          MhRecentFiles.Add(M);
          MhRecentFiles.Enabled := True;
        end;
      end;
    end;
    *)


var
  A: TAction;
  MhNew: TMenuItem;
//  MhZoomHolder: TMenuItem; // holder for all zooms
  MhViewLevel: TMenuItem;
//  M: TMenuItem;
  i: Integer;
begin

  MbEdit      := NewMenuBarItem('&Edit');
  MbSelection := NewMenuBarItem('&Selection');
  MbView      := NewMenuBarItem('&View');
  MbTools     := NewMenuBarItem('&Tools');
  MbHelp      := NewMenuBarItem('&Help');

  {
  MbFile.GroupIndex := 1;
  MbSelection.GroupIndex := 2;
  MbView.GroupIndex := 3;
  MbTools.GroupIndex := 4;
  MbHelp.GroupIndex := 255; }

//  MainMenu.AutoMerge := True;


  {$ifdef develop}
  MbDevelop := NewMenuBarItem('&Develop');
  {$endif}

  // file actions
  ActNewLevel := NewAction(MbFile, '&New...', ActNewLevel_Execute, ShortCut(Ord('N'), [ssCtrl]));
  //NewSep(MhNew);
  //ActNewDosExtGraphic := NewAction(MhNew, '&DOS Extended Graphic File...', ActNewDosExtGraphic_Execute);

  ActOpenFile := NewAction(MbFile, '&Open...', ActOpenFile_Execute, ShortCut(Ord('O'), [ssCtrl]));


  {
  MhRecentFiles := NewHolderItem(mbFile, '&Reopen');
  MhRecentFiles.Enabled := False;
  AddRecentFiles;
  }


  ActSave := NewAction(MbFile, '&Save', ActSave_Execute, Shortcut(Ord('S'), [ssCtrl]));
  ActSaveAs := NewAction(MbFile, 'Save &As...', ActSaveAs_Execute, Shortcut(Ord('S'), [ssCtrl, ssShift]));
  //ActCloseLevel := NewAction(MbFile, '&Close level...', ActCloseLevel_Execute, ShortCut(Ord('W'), [ssCtrl]));
//  ActSaveInDosPack := NewAction(MbFile, 'Save in DOS levelpack', ActSaveInDosPack_Execute);
  ActOptions := NewAction(MbFile, 'Settings...', ActOptions_Execute);
  NewSep(MbFile);
  //ActEncryptFile := NewAction(MbFile, 'Encrypt file...', ActEncryptFile_Execute);
  NewSep(MbFile);
  ActQuit := NewAction(MbFile, 'E&xit', ActQuit_Execute, ShortCut(VK_F4, [ssAlt]));


  // edit actions
  ActUndo := NewAction(MbEdit, 'Undo', ActUndo_Execute, ShortCut(Ord('Z'), [ssCtrl]));
  ActRedo := NewAction(MbEdit, 'Redo', ActRedo_Execute, ShortCut(Ord('Y'), [ssCtrl]));
  NewSep(MbEdit);
  
  ActInsertTerrain := NewAction(MbEdit, 'Insert &Terrain', ActInsertTerrain_Execute, ShortCut(Ord('T'), [ssAlt]));
  ActInsertTerrainLocal := NewMapPopupAction(nil, 'Insert &Terrain', ActInsertTerrain_Execute);

  ActInsertObject := NewAction(MbEdit, 'Insert &Object', ActInsertObject_Execute, ShortCut(Ord('O'), [ssAlt]));
  ActInsertObjectLocal := NewMapPopupAction(nil, 'Insert &Object', ActInsertObject_Execute);

  ActInsertSteel := NewAction(MbEdit, 'Insert &Steel', ActInsertSteel_Execute, ShortCut(Ord('S'), [ssAlt]));
  ActInsertSteelLocal := NewMapPopupAction(nil, 'Insert &Steel', ActInsertSteel_Execute);
  //ActSetScreenPosition := NewAction(MbEdit, 'Set Screen &Position', ActSetScreenPosition_Execute);
  ActSetScreenPositionLocal := NewMapPopupAction(nil, 'Set Screen &Position', ActSetScreenPosition_Execute);
  NewSep(MbEdit);

  ActCut := NewAction(MbEdit, '&Cut', ActCut_Execute, ShortCut(Ord('X'), [ssCtrl]));
  ActCopy := NewAction(MbEdit, '&Copy', ActCopy_Execute, ShortCut(Ord('C'), [ssCtrl]));
  ActPaste := NewAction(MbEdit, '&Paste', ActPaste_Execute, ShortCut(Ord('V'), [ssCtrl]));
  ActPasteSpecial := NewAction(MbEdit, 'Paste spe&cial', ActPasteSpecial_Execute, ShortCut(Ord('V'), [ssCtrl, ssShift]));
  NewSep(MbEdit);

  ActShiftLevel := NewAction(MbEdit, '&Shift level', ActShiftLevel_Execute);
  ActImportLayout := NewAction(MbEdit, 'Import layout', ActImportLayout_Execute);

  // selection actions
  ActAlignSelection := NewAction(MbSelection, '&Align...', ActAlignSelection_Execute);
  NewSep(mbSelection);
  ActToFront := NewAction(mbSelection, 'To &Front', ActToFront_Execute);
  ActToBack := NewAction(mbSelection, 'To &Back', ActToBack_Execute);

  // view actions
  ActZoomIn := NewAction(MbView, 'Zoom in', ActZoomIn_Execute);
  ToolButton1.Action := ActZoomIn;
  ActZoomOut := NewAction(MbView, 'Zoom out', ActZoomOut_Execute);
  ToolButton2.Action := ActZoomOut;
//  A :=
  NewSep(MbView);
  {
  mhZoomHolder := NewHolderItem(mbView, 'Zoom');
  for i := 0 to AVAILABLESCALECOUNT - 1 do
  begin
    A := NewAction(mhZoomHolder, Chr(Ord('a') + i) + ' ' + AvailableScaleNames[i], nil)
  end; }
//  asdf



//  ActShowImage := NewAction(MbView, 'Map editor', ActShowImage_Execute, ShortCut(VK_F12, []));
  ActShowInspector := NewAction(MbView, 'Inspector window', ActShowInspector_Execute, ShortCut(VK_F9, []));
  ActShowStatics := NewAction(MbView, 'Level properties window', ActShowStatics_Execute, ShortCut(VK_F10, []));
  ActShowSkills := NewAction(MbView, 'Level skillset window', ActShowSkills_Execute, ShortCut(VK_F11, []));
  ActShowList := NewAction(MbView, 'Level List window', ActShowList_Execute, ShortCut(VK_F8, []));
  NewSep(MbView);
  ActToggleTerrain := NewAction(MbView, 'Terrain rendering', ActToggleTerrain_Execute, ShortCut(VK_F5, []));
  ActToggleObjects := NewAction(MbView, 'Object rendering', ActToggleObjects_Execute, ShortCut(VK_F6, []));
  ActToggleSteel := NewAction(MbView, 'Steel rendering', ActToggleSteel_Execute, ShortCut(VK_F12, []));
  ActToggleBackground := NewAction(MbView, 'Background rendering', ActToggleBackground_Execute, ShortCut(VK_F7, []));
  ActToggleScreenStart := NewAction(MbView, 'Screen start', ActToggleScreenStart_Execute, ShortCut(VK_F3, []));
  ActToggleTriggerAreas := NewAction(MbView, 'Trigger areas', ActToggleTriggerAreas_Execute, ShortCut(VK_F4, []));
  NewSep(MbView);
  {MhViewLevel := NewHolderItem(MbView, 'Levels viewer');
  for i := 0 to StyleMgr.StyleList.Count - 1 do
  begin
    A := NewAction(mhViewLevel, StyleMgr[i].StyleDescription, ActViewLevels_Execute);
    A.Tag := i + 100;
  end;}


  // tools actions
  ActValidateLevel := NewAction(MbTools, 'Validate Level', ActValidateLevel_Execute);
  ActPlay := NewAction(MbTools, 'Play this level', ActPlay_Execute, ShortCut(VK_F2, []));
  NewSep(mbTools);
  //ActStyleCompiler := NewAction(MbTools, 'Compiler...', ActStyleCompiler_Execute);
  //NewSep(mbTools);


  //ActDosDatDecompress := NewAction(MbTools, 'DAT File Decompression...', ActDosDatDecompress_Execute); // just commented out, we might want to restore this later
  ActMassConvert := NewAction(MbTools, 'Mass Format Convertor', ActMassConvert_Execute);
  ActDownloadStyles := NewAction(MbTools, 'Download Graphic Sets', ActDownloadStyles_Execute);
//  {$ifndef develop}
//  ActPlay.Enabled := False;
//  {$endif} lemstrings

  // help actions
  ActAbout := NewAction(MbHelp, 'About...', ActAbout_Execute);





  // test actions
  {$ifdef develop}
  NewAction(MbDevelop, 'Play', ActPlay_Execute, ShortCut(VK_F2, []));
  NewAction(MbDevelop, 'Test...', ActTest_Execute, ShortCut(VK_F3, []));
  NewAction(MbDevelop, 'Show WinLemmings SPR Files', ActShowSpriteFiles_Execute);
  NewAction(MbDevelop, 'Show DOS Ground File Form', ActShowDOSGroundFile_Execute);
  NewSep(MbDevelop);
  NewAction(MbDevelop, 'Pack extended graphics', ActPackExtGraphics_Execute);
  NewSep(MbDevelop);
  //NewAction(MbDevelop, 'Lemmini Compiler Wizard', ActWizardLemmini_Execute);
  {$endif}


  // init
  ActToggleTerrain.Checked := True;
  ActToggleObjects.Checked := True;
  ActToggleBackground.Checked := True;

end;

procedure TMapEditorForm.Form_Destroy(Sender: TObject);
begin
  if Editor <> nil then
    App.EditorSettings.LastFile := Editor.FileName;

//  fEditorMapLink.Free;
  App.ScreenSettings.EditorScale := Img.Scale;
  App.EditorSettings.MapHeight := Img.Height;
  fHitList.Free;
  fDragger.Free;
end;

procedure TMapEditorForm.Form_Show(Sender: TObject);
var
  Item: TFormSettingItem;
begin
//  ShowMessage('1');
  Item := TFormSettingItem.Create(nil);
  try
    App.ReadForm(Self, Item);
    if Item.Height > 0 then BEGIN
    Self.WindowState := Item.WinState;
    //aForm.Visible := Item.Visible;
    if Self.WindowState = wsNormal then
    begin
    Self.Left := Item.Left;
    Self.Top := Item.Top;
    Self.Width := Item.Width;
    Self.Height := Item.Height;
    end;
//    if Item.StayOnTop then
  //     SaForm.FormStyle := fsStayOnTop;
  END;
  finally
    Item.Free;
  end;
//  ShowMessage('2');

(*

  *)

  InternalNewLevel(StyleMgr.Styles[0], StyleMgr.Styles[0].GraphicSets[0]);
  if (Editor.Graph.GraphicSetInternalName = 'placeholder') and
     (StyleMgr.Styles[0].GraphicSetCount > 1) then
    Editor.Graph := StyleMgr.Styles[0].GraphicSets[1];
  Editor.IsChanged := false;

{  if not fInspectorForm.Visible then
  begin
    fInspectorForm.Left := 300;
    fInspectorForm.Top := 320;
    fInspectorForm.Show;
  end; }
  exit;
  if not fStaticsForm.Visible then
  begin
  //  fStaticsForm.Left := 400;
//    fStaticsForm.Top := 330;
    fStaticsForm.Show;
  end;
  if not fSkillsForm.Visible then
    fSkillsForm.Show;

  if not fListForm.Visible then
  begin
  //  fListForm.Left := 500;
//    fListForm.Top := 320;
    fListForm.Show;
  end;
  if not fEditItemForm.Visible then
  begin
  //  fEditItemForm.Left := 550;
//    fEditItemForm.Top := 330;
    fEditItemForm.Show;
  end;
end;

procedure TMapEditorForm.UpdateSelectionLayer;
//var
  //Sel: TTerrain;
 // R: TRect;
begin
  (*
  Sel := LevelEditor.SelectedTerrain;
  if Sel <> nil then
  begin
    R.Left := Sel.TerrainX;
    R.Top := Sel.TerrainY;
    R.Right := R.Left + Sel.ItemWidth;
    R.Bottom := R.Top + Sel.ItemHeight;
//    R := MakeRect(fSelectionLayer.GetAdjustedLocation);
//    R := Rect(0, 0, 10, 10);
//    Deb([rectstr(r)]);

    fSelectionLayer.Location := FloatRect(R);
//    Deb([trunc(r.left)]);
  end; *)
//  fSelectionLayer.Visible := {Level}Editor.SelectorCollection.Count > 0; //Sel <> nil;
  fSelectionLayer.Changed;
//  else fSelection
end;

function TMapEditorForm.MapScaleIndex(const aScale: Single): Integer;
{-------------------------------------------------------------------------------
  Internal function returns the index of AvailableScales, closest to aScale.
  0.0 is mapped to a scale of 1.0
-------------------------------------------------------------------------------}
var
  i: Integer;
begin
  Result := 3;
  if aScale = 0.0 then
    Exit;
  for i := Low(AvailableScales) to High(AvailableScales) do
    if aScale = AvailableScales[i] then
    begin
      Result := i;
      Exit;
    end;
end;

procedure TMapEditorForm.Img_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
{-------------------------------------------------------------------------------
  A lot of edit functions are available with the keyboard:
  o Left, Right, Up, Down: Moving
  o Delete: Deleting
  o Insert: Inserting
  o A/O/T, Selecting
-------------------------------------------------------------------------------}
var
//  H, X, Y: Integer;
  P: TPoint;
  //B: TBaseLevelItem;
  //Dist: Integer;
  //Steel: TSteel;

    (*
    function GetDist: Integer;
    const
      Distances: array[1..3] of Integer = (1, 32, 8);
    begin
      Result := 3;

      if Shift = [] then
        Result := 3
      else if Shift = [ssCtrl] then
        Result := 1
      else if Shift = [ssCtrl, ssShift] then
        Result := 2;

      Result := Distances[Result];
    end; *)

    function GetDist: Integer;
    begin
      Result := App.EditorSettings.DefaultArrowKeysDistance;

//      if Shift = [] then
  //      Result := 3
//      else
      if Shift = [ssCtrl] then
        Result := App.EditorSettings.DefaultArrowKeysCtrlDistance;
//      else if Shift = [ssCtrl, ssShift] then
  //      Result := 2;

    //  Result := Distances[Result];
      if (Result < 0) or (Result > 128) then
        Result := 1; 
    end;

begin
  if not LevelActive then Exit;
  case Key of
//    Ord('C')

    VK_TAB:
      begin
        if ssCtrl in Shift  then
        begin
          editor.SelectNextItem(not (ssShift in Shift));
        end;
      end;
    VK_RETURN:
      begin
        HandleCommandStr;
      end;
    VK_LEFT:
      begin
       Editor.MoveSelection(-GetDist, 0);
       Img.Update;
      end;
    VK_RIGHT:
      begin
        Editor.MoveSelection(GetDist, 0);
        Img.Update;
      end;
    VK_UP:
      begin
        Editor.MoveSelection(0, -GetDist);
        Img.Update;
      end;
    VK_DOWN:
      begin
        Editor.MoveSelection(0, GetDist);
        Img.Update;
      end;
    VK_DELETE:
      begin
        Editor.DeleteSelection;
      end;
    VK_INSERT:
      begin
        ActInsertTerrain.Execute;
      end;

    VK_F10:
      if Shift = [ssShift] then
      begin
        P := Point(Img.Left + Img.Width div 2, Img.Top + Img.Height div 2);
        P := Img.ClientToScreen(P);
        MapPopup.Popup(P.X, P.Y);
      end;

  end;
end;

procedure TMapEditorForm.Img_KeyPress(Sender: TObject; var Key: Char);
//var
  //T: TTerrain;
  //p: tpoint;
//  S: string
begin
(*  if not (Key in ['0'..'9']) then
    if Key <> #13 then
    if Key <> #8 then
      CommandStr := ''; *)

  case Key of
    'i':
      begin
(*        if editor.selectorcount <> 1 then exit;
        p.x := editor.Selectors[0].RefItem.xposition;
        p.y := editor.Selectors[0].RefItem.yposition;
//        p:=img.BitmapToControl(p);
        img.ScrollToCenter(p.x, p.y); *)
      end;

    #8:
      begin
        CommandStr := CutRight(CommandStr, 1);
      end;
    '0'..'9':
      begin
        if GetTickCount - fCommandChangeTicks > 1000 then
          CommandStr := '';
        if Length(fCommandStr) = 1 then
          CommandStr := CommandStr + Key
        else
          CommandStr := Key;

      end;
    '=', '+', '.', '>' : ZoomIn;//(MinInt, MinInt);
    '-', ',', '<'      : ZoomOut;
    'a':
      begin
        Editor.SelectAll;
      end;

    'o':
      begin
        Editor.SelectAll([litObject]);
      end;
    'O':
      begin
        Editor.FilterSelection([litObject])
      end;
    't':
      begin
        Editor.SelectAll([litTerrain]);
      end;
    'T':
      begin
        Editor.FilterSelection([litTerrain])
      end;
    's':
      begin
        Editor.SelectAll([litSteel]);
      end;
    'S':
      begin
        Editor.FilterSelection([litSteel])
      end;

    'C', 'c':
      begin
        Editor.SelectorCollection.Clear;
      end;

    'x', 'X':
      begin
        FocusControl(Img);
      end;

  end;
end;

procedure TMapEditorForm.Img_MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
{-------------------------------------------------------------------------------
  Here is a lot of the mouselogic when editing a level.
  o Left button (plain) selects an object under the mousecursor.
    If the object is not selected it will be now.
    If the object is already selected drag/drop begins.
    If it's a doubleclick and we only hit air, then the selection is cleared.
  o Left button + Ctrl begins drawing a selection rectangle. Further handling in
    MouseMove and MouseUp. When we're in this mode the mouse is clipped.
  o Left button + Shift extends the selection with the hitted object.
    Then we follow the settings:
    if DirectSelect is False: If the hitted object is already selected
    it is removed from the selection.
    if DirectSelect is True: Dragging is controlled in mousemove
-------------------------------------------------------------------------------}
var
  P: TPoint;
  Hit: Integer;
  Lev: TLevelEditor;
  R: TRect;
  Filt: TLevelItemTypes;
begin

   fMouseOnSelection := False;
   DragSelection := False;
//  Img.SetFocus;
  if not LevelActive then Exit;
  if layer<>nil then exit;
  Lev := Editor;

{  if not fDrawingSelectionRect and (Button = mbRight) then
  begin
    P := Img.ClientToScreen(Point(X, Y));
    MapPopup.Popup(P.X, P.Y);
    Exit;
  end; }

  // selection rectangle
  if (Button = mbLeft) and (Shift * [ssCtrl, ssShift, ssAlt] = [ssCtrl]) then
  begin
    fDrawingSelectionRect := True;
    R := Img.BoundsRect;
    R.TopLeft := ClientToScreen(R.TopLeft);
    R.BottomRight := ClientToScreen(R.BottomRight);
    ClipCursor(@R);
    with fSelectionRect do
    begin
      TopLeft := Point(X, Y);
      BottomRight := TopLeft;
    end;
  end

  else if (Button = mbLeft) or (Button = mbRight) then
  begin
    P := Img.ControlToBitmap(Point(X, Y));
    fHitList.Clear; // belangrijk
    Filt := [litObject, litTerrain, litSteel] * fRenderer.RenderTypes; // chique
    if Filt = [] then
      Exit;

    Hit := fRenderer.HitTest(Lev, p.x, p.y, fHitList, Filt, 1);

    if Hit >= 0 then
      LastHitItem := fHitList.List^[0]
    else
      LastHitItem := nil;

    if Button = mbRight then
    begin
      P := Img.ClientToScreen(Point(X, Y));
      MapPopup.Popup(P.X, P.Y);
      Exit;
    end;

    // nothing hitted
    if Hit < 0 then
    begin
      // clear selection if left button
      //begin
        if App.EditorSettings.ClearSelectionOnAirClick then
          Lev.SelectorCollection.Clear
        else if ssDouble in Shift then
          Lev.SelectorCollection.Clear;
      //end
    end
    // there is a hit
    else if Hit >= 0 then
    begin
      //if Button = mbRight then
      //  Exit;
      // no shift keys
      if Shift * [ssCtrl, ssShift, ssAlt] = [] then
      begin
        // drag already made selection
        if Lev.SelectorCollection.FindBaseItem(TBaseLevelItem(fHitList[0])) >= 0 then
        begin
          if not CheckCanDrag then Exit;
          fMouseOnSelection := True;
          if not App.EditorSettings.DirectDragOnSelect then
          begin
            DragIP := Point(X, Y);
            DragBP := Img.ControlToBitmap(DragIP);
//            Img.Update;
            Img.BeginDrag(False);
            Exit;
          end;
        end
        // make new selection
        else begin
          if TObject(fHitList[0]) is TTerrain then
            Lev.SelectedTerrain := fHitList[0]
          else if TObject(fHitList[0]) is TInteractiveObject then
            Lev.SelectedObject := fHitList[0]
          else if TObject(fHitList[0]) is TSteel then
            Lev.SelectedSteel := fHitList[0];
          fMouseOnSelection := True;
        end;
      end
      // shift key pressed
      else if Shift * [ssCtrl, ssShift, ssAlt] = [ssShift] then
      begin
        Lev.ExtendSelection(fHitList, True);
      end;
    end; // hit >= 0

  end;
end;

procedure TMapEditorForm.Img_MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
type
  TMouseMoveAction = (mmNone, mmDrawRect, mmDrag);
var
//  DoRect, DoDrag: Boolean; UMISC
  MM: TMouseMoveAction;
  P: TPoint;
begin
  StatusBar.Panels[panel_imgcoord].Text := i2s(X) + ':' + i2s(Y);
  P := Img.ControlToBitmap(Point(X, Y));
  StatusBar.Panels[panel_levelcoord].Text := i2s(P.X) + ':' + i2s(P.Y);
  MM := mmNone;
  if fDrawingSelectionRect then
//  and (ssCtrl in Shift) then
    MM := mmDrawRect
  else if not fImageIsDragging
  and fMouseOnSelection
  and (ssLeft in Shift)
  and  App.EditorSettings.DirectDragOnSelect
  and CheckCanDrag then //disable dragging when zoomed in
    MM := mmDrag;

  case MM of
    mmNone:
      begin
        Exit;
      end;
    mmDrawRect:
      with fSelectionRect, Img.Canvas do
      begin
        Brush.Style := bsClear;
        Pen.Color := clWhite;
        Pen.Style := psDot;
        Pen.Mode := pmXOR;
        if fSelectionRectVisible then
          Rectangle(fSelectionRect);
        BottomRight := Point(X, Y);
        Rectangle(fSelectionRect);
        fSelectionRectVisible := True;
      end;
    mmDrag:
      begin
        DragIP := Point(X, Y);
        DragBP := Img.ControlToBitmap(DragIP);
//        with dragbp do
  //        deb(['startdrag', x,y]);
        //DraggedItem := fHitList.List^[0];
        Img.Update;
        Img.BeginDrag(False);
      end;
  end;
end;

procedure TMapEditorForm.Img_MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer);
var
  DoSelect: Boolean;
  R: TRect;
//  P: TPoint;
begin
//  Deb(['mu']);
  DoSelect := False;
  ClipCursor(nil);
  fMouseOnSelection := False;
  (*

  if DragSelection then
  begin
    DragSelection := False;
//    Deb(['dropselection']);
//    Deb([dragx, x]);
    // delete if not in map
    P := Img.ControlToBitmap(Point(X,Y));
    LevelEditor.MoveSelection(P.X - DragP.X, P.Y - DragP.Y);
    Exit;
  end; *)

  if fDrawingSelectionRect then
  //  if ssCtrl in Shift then
      with fSelectionRect do
      begin
        with Img.Canvas do
        begin
          Brush.Style := bsClear;
          Pen.Color := clWhite;
          Pen.Style := psDot;
          Pen.Mode := pmXOR;
          if fSelectionRectVisible then
            Rectangle(fSelectionRect);
          DoSelect := True;
        end;
      end;
  fDrawingSelectionRect := False;
  fSelectionRectVisible := False;
  if DoSelect then
  begin
//    R := fSelectionRect;
    R.TopLeft := Img.ControlToBitmap(fSelectionRect.TopLeft);
    R.BottomRight := Img.ControlToBitmap(fSelectionRect.BottomRight);
    if R.Bottom < R.Top then SwapInts(r.bottom, r.top);
    if R.Right < R.Left then SwapInts(r.right, r.left);
    SelectAllUnderRect(R);
  end;
end;


procedure TMapEditorForm.Img_MouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
//deb([mousepos.y]);
  ZoomOut(MousePos.X, MousePos.Y);
  Handled := True;
end;

procedure TMapEditorForm.Img_MouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ZoomIn(MousePos.X, MousePos.Y);
  Handled := True;
end;

function TMapEditorForm.LevelActive: Boolean;
begin
  Result := (Editor <> nil)
end;


procedure TMapEditorForm.SelectionLayer_Paint(Sender: TObject; Buffer: TBitmap32);
var
  R: TRect;
//  FR: TFloatRect;
//  Sel: TTerrain;
  i: Integer;
  S: TSelector;
begin
  if fLocked then Exit;
  if fInternalGettingDragBitmap then Exit;
  if Editor = nil then
    Exit;
//  if LevelEditor.IsUpdating then
  //  Exit;

  with Editor.SelectorCollection do
    for i := 0 to HackedList.Count - 1 do
    begin
      S := Items[i];
      R := S.RefItem.Bounds;
      R := MakeRect(fSelectionLayer.GetAdjustedRect(FloatRect(R)));
      Buffer.FrameRectS(R.Left, R.Top, R.Right, R.Bottom, fSelectorColors[S.RefItem.ItemType]);
{      if i = 0 then
      begin
        InflateRect(R, -1, -1);
        Buffer.FrameRectS(R.Left, R.Top, R.Right, R.Bottom, fSelectorColors[S.RefItem.ItemType]);
      end; }
    end;

end;

procedure TMapEditorForm.ScreenStartLayer_Paint(Sender: TObject; Buffer: TBitmap32);
var
  R: TRect;
//  FR: TFloatRect;
//  Sel: TTerrain;
//  i: Integer;
//  S: TSelector;
  Def: TDefaultsRec;
begin
  if fLocked then Exit;
  if Editor = nil then
    Exit;
  if Editor.Style = nil then exit;
  if Editor.Graph = nil then exit;
//  if LevelEditor.IsUpdating then
  //  Exit;

  with Editor, Statics do
  begin
//    deb(['reevaluate screenstartlayer_paint']);
//    raise Exception.Create('reevaluate screenstartlayer_paint');

    Style.GetStyleParams(Def);

    R := Rect(0, 0, 82, 82);
    RectMove(R, ScreenPosition-40, ScreenYPosition-40);

    R := MakeRect(fScreenStartLayer.GetAdjustedRect(FloatRect(R)));
    Buffer.FrameRectS(R.Left, R.Top, R.Right, R.Bottom, clWhite32);
    Buffer.LineS(R.Left, R.Top, R.Right, R.Bottom, clWhite32);
    Buffer.LineS(R.Left, R.Bottom, R.Right, R.Top, clWhite32);

  end;
end;

function TMapEditorForm.DoZoom(NewZoom: Integer; MouseX: Integer = MinInt; MouseY: Integer = MinInt): Boolean;
var
  P, Q: TPoint;
  HorzDisplayCenter, VertDisplayCenter: Integer;
begin
  Result := (NewZoom < High(AvailableScales)) and (NewZoom > 0);
  if not Result then Exit;

  with Img do
  begin
    // The visible range is the visible area of Img divided by the scale factor
    HorzDisplayCenter := (Img.GetViewportRect.Left + Img.GetViewportRect.Right) div 2;
    VertDisplayCenter := (Img.GetViewportRect.Top + Img.GetViewportRect.Bottom) div 2;
    Q := Img.ControlToBitmap(Point(trunc(HorzDisplayCenter), trunc(VertDisplayCenter)));

    fScaleIndex := NewZoom;

    Scale := AvailableScales[fScaleIndex];

    // Zoom settings:
    // 0 = center of selection, or center of current view
    // 1 = center of selection, or mouse position
    // 2 = center of current view
    // 3 = mouse position
    if Editor <> nil then
    begin
      if (Editor.SelectorCount > 0) and (App.EditorSettings.ZoomSetting < 2) then
        P := Editor.SelectorCollection.Center
      else if App.EditorSettings.ZoomSetting mod 2 = 1 then
        P := Img.ControlToBitmap(Point(MouseX, MouseY))
      else
        P := Q;
      Img.ScrollToCenter(P.X, P.Y);
    end;

  end;
  UpdatePanelScale;
end; 

function TMapEditorForm.ZoomIn(MouseX: Integer = MinInt; MouseY: Integer = MinInt): Boolean;
begin
  Result := DoZoom(fScaleIndex + 1, MouseX, MouseY);
end;

function TMapEditorForm.ZoomOut(MouseX: Integer = MinInt; MouseY: Integer = MinInt): Boolean;
{var
  Q, P: TPoint;
  Px, Py: Integer;}
begin
  Result := DoZoom(fScaleIndex - 1);
  {Result := fScaleIndex > 0;
  if Result then
  begin
    Dec(fScaleIndex);

    if (MouseX > MinInt) and (MouseY > MinInt) then
      Q := Img.ControlToBitmap(Point(MouseX, MouseY));

    Img.Scale := AvailableScales[fScaleIndex];

    if Editor <> nil then
    begin
      if Editor.SelectorCount > 0 then
        P := Editor.SelectorCollection.Center
      else
        P := Q;
      Img.ScrollToCenter(P.X, P.Y);
    end;

  end;
  UpdatePanelScale;}
end;

procedure TMapEditorForm.SelectAllUnderRect(const aRect: TRect);
var
  i: Integer;
  T: TTerrain;
  O: TInteractiveObject;
  S: TSteel;
  R, Dummy: TRect;
begin
  with Editor do
  begin
    SelectorCollection.BeginUpdate;
    SelectorCollection.Clear;

    if litObject in fRenderer.RenderTypes then
    for i := 0 to InteractiveObjectCollection.Count - 1 do
    if InteractiveObjectCollection[i].ItemActive then
    begin
      O := InteractiveObjectCollection[i];
      R.Left := O.ObjectX;
      R.Top := O.ObjectY;
      R.Right := R.Left + O.ItemWidth;
      R.Bottom := R.Top + O.ItemHeight;
{      if PtInRect(aRect, R.TopLeft)
      or PtInRect(aRect, R.BottomRight)
      or PtInRect(R, aRect.TopLeft)
      or PtInRect(R, aRect.BottomRight) then }
      if IntersectRect(Dummy, R, aRect) then
      begin
        with SelectorCollection.Add do
          RefItem := O;
//        Deb([t.terrainid, rectstr(r)]);
      end;
    end;

    if litTerrain in fRenderer.RenderTypes then
    for i := 0 to TerrainCollection.Count - 1 do
    if TerrainCollection[i].ItemActive then
    begin
      T := TerrainCollection[i];
      R.Left := T.TerrainX;
      R.Top := T.TerrainY;
      R.Right := R.Left + T.ItemWidth;
      R.Bottom := R.Top + T.ItemHeight;
{      if PtInRect(aRect, R.TopLeft)
      or PtInRect(aRect, R.BottomRight)
      or PtInRect(R, aRect.TopLeft)
      or PtInRect(R, aRect.BottomRight) then}
      if IntersectRect(Dummy, R, aRect) then
      begin
        with SelectorCollection.Add do
          RefItem := T;
//        Deb([t.terrainid, rectstr(r)]);
      end;
    end;


    if litSteel in fRenderer.RenderTypes then
    for i := 0 to SteelCollection.Count - 1 do
    if SteelCollection[i].ItemActive then
    begin
      S := SteelCollection[i];
      R.Left := S.SteelX;
      R.Top := S.SteelY;
      R.Right := R.Left + S.ItemWidth;
      R.Bottom := R.Top + S.ItemHeight;
{      if PtInRect(aRect, R.TopLeft)
      or PtInRect(aRect, R.BottomRight)
      or PtInRect(R, aRect.TopLeft)
      or PtInRect(R, aRect.BottomRight) then }
      if IntersectRect(Dummy, R, aRect) then
      begin
        with SelectorCollection.Add do
          RefItem := S;
//        Deb([t.terrainid, rectstr(r)]);
      end;
    end;

    SelectorCollection.EndUpdate;


  end;
end;

function TMapEditorForm.GetSelectionDragBitmap(Bmp: TBitmap; var HotX, HotY: Integer): Boolean;
{-------------------------------------------------------------------------------
  This routine is and may only be called when dragging.
-------------------------------------------------------------------------------}
var
//  i: Integer;
  Lev: TLevelEditor;
  D, R: TRect;
 // First: Boolean;
  Pic: TBitmap32;
//  Sca: Integer;

begin
  Lev := Editor;
  Result := Assigned(Lev) and (Lev.SelectorCollection.Count > 0);
  R := EmptyRect;
//  First := True;

  R := LastHitItem.Bounds;

//  Dec(R.Right);
//  Dec(R.Bottom);//oops

//  Deb([rectwidth(R)]);

//  HotX := DragIP

  HotX := Round((DragBP.X - R.Left) * Img.Scale);
  HotY := Round((DragBP.Y - R.Top) * Img.Scale);

  //inc(hotx);
//  inc(hoty);
//  Deb([hotx,hoty]);

  D := ZeroTopLeftRect(R);
  D.Right := Round(D.Right * Img.Scale);
  D.Bottom := Round(D.Bottom * Img.Scale);

  Bmp.Width := RectWidth(D)-1;
  Bmp.Height := RectHeight(D)-1;


//  Deb([bmp.width]);

  case LastHitItem.ItemType of
    litTerrain:
      begin
        // PictureMgr.TerrainBitmaps[Editor.Statics.GraphicSet, LastHitItem.Identifier];
        Pic := TBitmap32.Create;
        Pic.Assign(LastHitItem.Graph.TerrainBitmaps[LastHitItem.Identifier]);
        if (tdfRotate in TTerrain(LastHitItem).TerrainDrawingFlags) then
        begin
          //D.Bottom := Round(D.Right * Img.Scale);
          //D.Right := Round(D.Bottom * Img.Scale);
          Pic.Rotate90;
        end;
        if (tdfInvert in TTerrain(LastHitItem).TerrainDrawingFlags) then Pic.FlipVert;
        if (tdfFlip in TTerrain(LastHitItem).TerrainDrawingFlags) then Pic.FlipHorz;
        Pic.DrawTo(Bmp.Canvas.Handle, ZeroTopLeftRect(D),ZeroTopLeftRect(R));
        Pic.Free;
      end;
    litObject:
      begin
        //Pic := PictureMgr.ObjectBitmaps[Editor.Statics.GraphicSet, LastHitItem.Identifier];
        Pic := TBitmap32.Create;
        Pic.Assign(LastHitItem.Graph.ObjectBitmaps[LastHitItem.Identifier]);
        if (odfRotate in TInteractiveObject(LastHitItem).ObjectDrawingFlags) then Pic.Rotate90;        
        if (odfInvert in TInteractiveObject(LastHitItem).ObjectDrawingFlags) then Pic.FlipVert;
        if (odfFlip in TInteractiveObject(LastHitItem).ObjectDrawingFlags) then Pic.FlipHorz;
        Pic.DrawTo(Bmp.Canvas.Handle, ZeroTopLeftRect(D),ZeroTopLeftRect(R));
        Pic.Free;
      end;
    else
      Img.Bitmap.DrawTo(Bmp.Canvas.Handle, D, R);
  end;

//    Pic := PictureMgr.
end;

procedure TMapEditorForm.LMSelectionChanged(var Msg: TLMSelectionChanged);
begin
  StatusBar.Panels[panel_selection].Text := i2s(Editor.SelectorCollection.Count) + ' item(s) selected';
  UpdateSelectionLayer;
end;

procedure TMapEditorForm.LMLevelChanging(var Msg: TLMLevelChanging);
begin
  fLocked := True;
end;

procedure TMapEditorForm.LMLevelChanged(var Msg: TLMLevelChanged);
var
  Def: TDefaultsRec;
begin
  fLocked := False;
  ReRender;
  UpdateSelectionLayer;

  UpdateScreenCaption;
  UpdatePanelScale;
  UpdatePanelResolution;

  if (LCF_LOADFROMFILE and Msg.LevelChangeFlags <> 0)
  or (LCF_LOADFROMSTREAM and Msg.LevelChangeFlags <> 0)then
  begin
//    Deb(['aha']);

    if Editor.Style = nil then Exit;
    editor.style.GetStyleParams(def);
    Img.ScrollToCenter(Editor.Statics.ScreenPosition + Def.LevelHeight, 0);
//  Deb(['total update', editor.filename]);

//  deb(['we need to reevaluate scroll'])
  end;
end;

procedure TMapEditorForm.LMStaticsChanged(var Msg: TLMStaticsChanged);
begin
  if Msg.ChangeInfo = 0 then
    Img.Invalidate //???
  else
    ReRender;
end;

procedure TMapEditorForm.LMObjectListChanged(var Msg: TLMObjectListChanged);
begin
  ReRender;
end;

procedure TMapEditorForm.LMTerrainListChanged(var Msg: TLMTerrainListChanged);
begin
  ReRender;
end;

procedure TMapEditorForm.LMSteelListChanged(var Msg: TLMSteelListChanged);
begin
  ReRender;
end;

procedure TMapEditorForm.Img_StartDrag(Sender: TObject; var DragObject: TDragObject);
begin
  fImageIsDragging := True;
  Img.Update;
  DragObject := fDragger;
end;

procedure TMapEditorForm.Img_DragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := True;
end;

procedure TMapEditorForm.Img_DragDrop(Sender, Source: TObject; X, Y: Integer);
var
  P, S: TPoint;
  i: Integer;

begin
  DragSelection := False;

  S := DragBP;
  P := Img.ControlToBitmap(Point(X, Y));

  if (GetKeyState(VK_CONTROL) and $80) <> 0 then
  begin
    Editor.CloneSelection(P.X - S.X, P.Y - S.Y)
  end else
    Editor.MoveSelection(P.X - S.X, P.Y - S.Y);
end;

procedure TMapEditorForm.Img_EndDrag(Sender, Target: TObject; X, Y: Integer);
begin
  fImageIsDragging := False;
end;

procedure TMapEditorForm.Form_DropFiles(Sender: TObject; const Files: TStrings);
//var
  //Lev: TLevelEditor;
begin
  ShowMessage('Dragging files here not finished yet. Use File|Open to open files');
  exit;
end;

procedure TMapEditorForm.UpdateScreenCaption;
var
  Lev: TLevelEditor;
//  F: string;
//  T: string;
begin
  Lev := Editor;
  if Lev = nil then
    Caption := 'NeoLemmix Editor'
  else
    Caption := 'NeoLemmix Editor [' +
    MinimizeName(Lev.FileName, Self.Canvas, 256) + '] ' + Lev.LevelName;
//  else if Lev.LoaderObject is TFormEditDosLevelPack then
  //  Caption := 'Lemmix [' +
    //MinimizeName(FormEditDosLevelPack.FileName, Self.Canvas, 256) + '] ' +
//    i2s(FormEditDosLevelPack.tree.selectedindex) +
//    Lev.LevelName;
end;

procedure TMapEditorForm.UpdatePanelResolution;
begin
  if Editor = nil then
    Exit;
  StatusBar.Panels[panel_resolution].Text := i2s(Img.Bitmap.Width) + ' * ' + i2s(Img.Bitmap.Height);
end;

procedure TMapEditorForm.UpdatePanelScale;
begin
  StatusBar.Panels[panel_scale].Text := availableScaleNames[fScaleIndex];
end;

procedure TMapEditorForm.ActEncryptFile_Execute(Sender: TObject);
var
  NeoEncrypt: TNeoEncryption;
  OpenDlg: TOpenDialog;
  SaveDlg: TSaveDialog;
  Strm: TMemoryStream;
  i: Integer;
begin
  OpenDlg := TOpenDialog.Create(Self);
  SaveDlg := TSaveDialog.Create(Self);
  NeoEncrypt := TNeoEncryption.Create;
  Strm := TMemoryStream.Create;
  try
    OpenDlg.Title := 'Source file';
    OpenDlg.Filter := 'Encryptable files|*.dat;*.lvl';
    OpenDlg.Options := [ofFileMustExist];
    if not OpenDlg.Execute then Exit;
    SaveDlg.Title := 'Destination file';
    SaveDlg.FileName := ChangeFileExt(OpenDlg.FileName, '') + '_encrypted' + ExtractFileExt(OpenDlg.FileName);
    if not SaveDlg.Execute then Exit;
    if FileExists(SaveDlg.FileName) then
    begin
      if SaveDlg.FileName = OpenDlg.FileName then
        i := MessageDlg('WARNING: If you encrypt this file and save it over' + CrLf +
                        'the source file, it will not be possible to unencrypt' + CrLf +
                        'it later. It is recommended you save the encrypted file' + CrLf +
                        'with a different filename. Continue?',
                        mtCustom, [mbYes, mbNo], 0)
        else
        i := MessageDlg('File already exists. Overwrite?', mtCustom, [mbYes, mbNo], 0);
      if i <> mrYes then Exit;
    end;
    Strm.LoadFromFile(OpenDlg.FileName);
    NeoEncrypt.SaveFile(Strm, SaveDlg.FileName);
  finally
    OpenDlg.Free;
    SaveDlg.Free;
    NeoEncrypt.Free;
    Strm.Free;
  end;
end;

procedure TMapEditorForm.ActNewLevel_Execute(Sender: TObject);
var
  OK: Boolean;
//  S: Integer;
//  R: TResolution;
  S: TBaseLemmingStyle;
  G: TBaseGraphicSet;
  Response: Integer;
begin
  if Editor = nil then
    Exit;

  if Editor.IsChanged and (Editor.Style <> nil) then
    Response := MessageDlg('Level has been changed. Save level?', mtCustom, [mbYes, mbNo, mbCancel], 0)
  else
    Response := mrNo;

  case Response of
    mrYes: begin
             ActSave_Execute(self);
             if Editor.IsChanged then Exit;
           end;
    mrCancel: Exit;
  end;

  OK := ExecuteNewLevelForm(S, G);
  if not OK then
    Exit;

  InternalNewLevel(S, G);
end;

procedure TMapEditorForm.InternalNewLevel(S: TBaseLemmingStyle; G: TBaseGraphicSet);
begin
  Editor.BeginUpdate;
  try
    Editor.CloseLevel;
//    Editor.GraphicSet := G;
//    Editor.
    Editor.Style := S;
    Editor.Graph := G;
    Editor.Statics.GraphicSet := G.GraphicSetId; //lemcore
//    Editor.Resolution := R;
//    PictureMgr.Resolution := R;
//    fRenderer.Resolution := R;
    if Editor.Style is TLemminiStyle then
    begin
      Editor.Statics.ScreenPosition := Editor.Statics.ScreenPosition * 2;
      Editor.Statics.ScreenYPosition := Editor.Statics.ScreenYPosition * 2;
      Editor.Statics.VgaspecX := Editor.Statics.VgaspecX * 2;
      Editor.Statics.VgaspecY := Editor.Statics.VgaspecY * 2;
      Editor.InteractiveObjectCollection.TargetItemCount := -1;
      Editor.TerrainCollection.TargetItemCount := -1;
      Editor.SteelCollection.TargetItemCount := -1;
      if TLemminiStyle(Editor.Style).SuperLemmini then
      begin
        Editor.Statics.Width := 640;
        Editor.Statics.Height := 320;
        Editor.Statics.ScreenPosition := 0;
        Editor.Statics.HasTimeLimit := false;
      end else begin
        Editor.Statics.Width := Editor.Style.GetParams.LevelWidth;
        Editor.Statics.Height := Editor.Style.GetParams.LevelHeight;
      end;
    end else begin
      if TCustLemmStyle(Editor.Style).NeoLemmix then
      begin
        Editor.InteractiveObjectCollection.TargetItemCount := -1;
        Editor.TerrainCollection.TargetItemCount := -1;
        Editor.SteelCollection.TargetItemCount := -1;
        Editor.Statics.Width := 320;
        Editor.Statics.Height := 160;
        Editor.Statics.ScreenPosition := 0;
        Editor.Statics.HasTimeLimit := false;
      end else begin
        Editor.InteractiveObjectCollection.TargetItemCount := 32;
        Editor.TerrainCollection.TargetItemCount := 400;
        Editor.SteelCollection.TargetItemCount := 32;
        Editor.Statics.Width := Editor.Style.GetParams.LevelWidth;
        Editor.Statics.Height := Editor.Style.GetParams.LevelHeight;
      end;
    end;
    Editor.History.Clear;
    Editor.HistoryPosition := -1;
  finally
    Editor.EndUpdate;
  end;
end;

procedure TMapEditorForm.ActOpenFile_Execute(Sender: TObject);
{-------------------------------------------------------------------------------
  Action Execute eventhandler to open a level-file.
  History is saved.
-------------------------------------------------------------------------------}
var
  Path: string;
  Dlg: TOpenDialog;
  i: Integer;
  F: string;
  Ix: Integer;
  Exec: Boolean;
//  S: string;
  Response: Integer;
begin
  if Editor = nil then
    Exit;

  if Editor.IsChanged and (Editor.Style <> nil) then
    Response := MessageDlg('Level has been changed. Save level?', mtCustom, [mbYes, mbNo, mbCancel], 0)
  else
    Response := mrNo;

  case Response of
    mrYes: begin
             ActSave_Execute(self);
             if Editor.IsChanged then Exit;
           end;
    mrCancel: Exit;
  end;


  Path := App.HistorySettings.EditorLastOpenDir;
  if Path = '' then
    Path := ApplicationPath;

  Ix := 0;
  Dlg := TOpenDialog.Create(nil);
  try
    Dlg.InitialDir := Path;

    for i := Low(OpenDlgFilters) to High(OpenDlgFilters) do
      Dlg.Filter := Dlg.Filter + OpenDlgFilters[i];
    Dlg.FilterIndex := App.HistorySettings.EditorLastOpenFilterIndex;
    Exec := Dlg.Execute;
    if Exec then
    begin
      App.HistorySettings.EditorLastOpenDir := ExtractFilePath(Dlg.FileName);
      App.HistorySettings.EditorLastOpenFilterIndex := Dlg.FilterIndex;
      F := Dlg.FileName;
      ix := Dlg.FilterIndex;

//      Editor.LoadFromFile(Dlg.FileName, FileNameToFormat(Dlg.FileName));

//    R := Rect(0, 0, Defaults[Resolution].ViewWidth, Defaults[Resolution].ViewHeight); //lemstartup
//    RectMove(R, ScreenPosition, 0);
//    R := MakeRect(fScreenStartLayer.GetAdjustedRect(FloatRect(R)));
//    Buffer.FrameRectS(R.Left, R.Top, R.Right, R.Bottom, clWhite32);
//
//      Img.ScrollToCenter(Editor.Statics.ScreenPosition + Defaults[Editor.Resolution].ViewWidth div 2, 0);
    end;
  finally
    Dlg.Free;
  end;

  //
  if Exec then
    InternalOpenFile(F, Ix);

end;

procedure TMapEditorForm.ActOpenRecentFile_Execute(Sender: TObject);
{-------------------------------------------------------------------------------
  This eventhandler is triggered by a TRecentFileAction. So we know the file
  and what file.
-------------------------------------------------------------------------------}
var
  Act: TRecentFileAction absolute Sender;
  Sty: TBaseLemmingStyle;
  Typ: TLemmingFileFormat;
  { TODO : checks }
  Response: Integer;
begin
  if Editor = nil then
    Exit;

  if Editor.IsChanged and (Editor.Style <> nil) then
    Response := MessageDlg('Level has been changed. Save level?', mtCustom, [mbYes, mbNo, mbCancel], 0)
  else
    Response := mrNo;

  case Response of
    mrYes: begin
             ActSave_Execute(self);
             if Editor.IsChanged then Exit;
           end;
    mrCancel: Exit;
  end;

  Assert(Sender is TRecentFileAction, 'sender error');
  with Act do
  begin

    if comparetext(act.filetype,'level')=0 then
    begin
      Sty := StyleMgr.StyleByName(act.FileInfo);
      Typ := TLemmingFileFormat(GetEnumValue(typeinfo(TLemmingFileFormat), act.FileFormat));
      Editor.LoadFromFile(FileName, Sty, Typ);
    end;

  //    Editor.LoadFromFile(FileName, STYLE_LEMMINI, lffLemmini);
//    else if comparetext(act.filetype,'dosorig')=0 then
  //    InternalOpenFile(FileName, OFD_DOSLEMMINI)

  end;

end;


(*procedure TMapEditorForm.ActCloseLevel_Execute(Sender: TObject);
var
  Response: Integer;
begin
  Assert(Editor <> nil, 'editor = nil');
  if Editor.IsChanged and (Editor.Style <> nil) then
    Response := MessageDlg('Level has been changed. Save level?', mtCustom, [mbYes, mbNo, mbCancel], 0)
  else
    Response := mrNo;

  case Response of
    mrYes: begin
             ActSave_Execute(self);
             if not Editor.IsChanged then Editor.CloseLevel;
           end;
    mrNo: Editor.CloseLevel;
  end;
end;*)

procedure TMapEditorForm.ActQuit_Execute(Sender: TObject);
begin
  Close;
end;

procedure TMapEditorForm.ActSave_Execute(Sender: TObject);
var
  TheFormat: TLemmingFileFormat;
begin
  if Editor.Style = nil then Exit;

  if Editor.FileName = '' then
  begin
    ActSaveAs_Execute(Sender);
    Exit;
  end;

  if Editor.Style is TLemminiStyle then
    TheFormat := lffLemmini
  else
    TheFormat := lffLVL;

  Editor.SaveToFile(Editor.FileName, TheFormat);
end;

procedure TMapEditorForm.ActSaveAs_Execute(Sender: TObject);
{-------------------------------------------------------------------------------
  Action Execute eventhandler to save a level-file.
  History is saved.
-------------------------------------------------------------------------------}
var
  Path, F: string;
  Dlg: TSaveDialog;
  i, bs: Integer;
  TheFormat: TLemmingFileFormat;
  SaveImageOnly: Boolean;
begin
  if Editor = nil then
    Exit;
  if Editor.Style = nil then Exit;
  Path := App.HistorySettings.EditorLastSaveDir;
  if Path = '' then
    Path := ApplicationPath;
  Dlg := TSaveDialog.Create(nil);
  try
    //Dlg.OnTypeChange := SaveDlg_TypeChange;


    Dlg.InitialDir := Path;

    if Editor.Style is TLemminiStyle then
      Dlg.Filter := Dlg.Filter + SaveAsFilters[1]
      else
      Dlg.Filter := Dlg.Filter + SaveAsFilters[2];

    for i := Low(SaveAsFilters)+2 to High(SaveAsFilters) do
      Dlg.Filter := Dlg.Filter + SaveAsFilters[i];

    case Editor.CurrentFormat of
      lffLVL:
        Dlg.FilterIndex := 1;
      lffLemmini:
        Dlg.FilterIndex := 1;
    else
      Dlg.FilterIndex := App.HistorySettings.EditorLastSaveFilterIndex;
    end;

    if Editor.FileName <> '' then
      Dlg.FileName := Editor.FileName;
    if Dlg.Execute then
    begin
      F := Dlg.FileName;
      case Dlg.FilterIndex of
        1: begin
             if Editor.Style is TLemminiStyle then
               TheFormat := lffLemmini
               else
               TheFormat := lffLVL;
           end;
        2: SaveImageOnly := true;
      else
        raise Exception.Create('SaveAs: file format error');
      end;
      F := Editor.GetAdjustedFileName(F, TheFormat);
      App.HistorySettings.EditorLastSaveDir := ExtractFilePath(F);
      App.HistorySettings.EditorLastSaveFilterIndex := Dlg.FilterIndex;
      if FileExists(F) then
        bs := MessageDlg('File already exists. Overwrite it?',mtCustom, [mbYes, mbNo], 0)
        else
        bs := mrYes;
      if bs = mrYes then
      begin
        if SaveImageOnly then
          fRenderer.RenderToFile(Editor, F)
          else
          Editor.SaveToFile(F, TheFormat);
      end
    end;
  finally
    Dlg.Free;
  end;

  //TCustomLevelValidator.ValidateLevel(Editor);

end;



procedure TMapEditorForm.ActOptions_Execute(Sender: TObject);
begin
  ExecuteFormOptions;
end;


procedure TMapEditorForm.ActAlignSelection_Execute(Sender: TObject);
var
  Horz: TItemHorzAlign;
  Vert: TItemVertAlign;
begin
  if Editor = nil then
    Exit;
  if ExecuteAlignSelectionForm(Horz, Vert) then
    Editor.AlignSelection(Horz, Vert);
end;

procedure TMapEditorForm.ActToFront_Execute(Sender: TObject);
begin
  Editor.SelectionToFront;
end;

procedure TMapEditorForm.ActToBack_Execute(Sender: TObject);
begin
  Editor.SelectionToBack;
end;

function TMapEditorForm.CalcCenteredInsertionPoint: TPoint;
{-------------------------------------------------------------------------------
  Calculate the "for the eye" best point to insert a new object into the map.
  It returns a bitmap coordinate (not an image coordinate!)
  If the center of the image is not a bitmap point then the scale is most likely
  very small and we take the center of the bitmap as result.
-------------------------------------------------------------------------------}
begin
  Result := Point(Img.Width div 2, Img.Height div 2);
  Result := Img.ControlToBitmap(Result);
  if not PtInRect(Img.Bitmap.BoundsRect, Result) then
    Result := Point(Img.Bitmap.Width div 2, Img.Bitmap.Height div 2);
end;

procedure TMapEditorForm.ActUndo_Execute(Sender: TObject);
var
  HorzDisplayCenter, VertDisplayCenter: Integer;
  Q: TPoint;
begin
  HorzDisplayCenter := (Img.GetViewportRect.Left + Img.GetViewportRect.Right) div 2;
  VertDisplayCenter := (Img.GetViewportRect.Top + Img.GetViewportRect.Bottom) div 2;
  Q := Img.ControlToBitmap(Point(HorzDisplayCenter, VertDisplayCenter));

  Editor.Undo;

  Img.ScrollToCenter(Q.X, Q.Y);
end;

procedure TMapEditorForm.ActRedo_Execute(Sender: TObject);
var
  HorzDisplayCenter, VertDisplayCenter: Integer;
  Q: TPoint;
begin
  HorzDisplayCenter := (Img.GetViewportRect.Left + Img.GetViewportRect.Right) div 2;
  VertDisplayCenter := (Img.GetViewportRect.Top + Img.GetViewportRect.Bottom) div 2;
  Q := Img.ControlToBitmap(Point(HorzDisplayCenter, VertDisplayCenter));

  Editor.Redo;

  Img.ScrollToCenter(Q.X, Q.Y);
end;

procedure TMapEditorForm.ActInsertObject_Execute(Sender: TObject);
{-------------------------------------------------------------------------------
  Action event handler.
  There are two actions that are attached to this eventhandler:
  1) ActInsertObject from the main menu
  2) ActInsertObject from the local menu
  In the case of the local menu we take the popup point as location
-------------------------------------------------------------------------------}
var
  P: TPoint;
//  O: TInteractiveObject;
begin
  if Sender = ActInsertObjectLocal then
  begin
    // convert global screen coordinate
    P := MapPopup.PopupPoint; // this is a global screen coordinate
    P := Img.ScreenToClient(P);
    P := Img.ControlToBitmap(P);
  end
  else
    P := CalcCenteredInsertionPoint;

  InternalInsertObject(P);
end;

procedure TMapEditorForm.ActInsertTerrain_Execute(Sender: TObject);
{-------------------------------------------------------------------------------
  See also: ActInsertObject_Execute
-------------------------------------------------------------------------------}
var
  P: TPoint;
//  O: TInteractiveObject;
begin
  if Sender = ActInsertTerrainLocal then
  begin
    // convert global screen coordinate
    P := MapPopup.PopupPoint; // this is a global screen coordinate
    P := Img.ScreenToClient(P);
    P := Img.ControlToBitmap(P);
  end
  else
    P := CalcCenteredInsertionPoint;

  InternalInsertTerrain(P);
end;

procedure TMapEditorForm.ActInsertSteel_Execute(Sender: TObject);
{-------------------------------------------------------------------------------
  See also: ActInsertObject_Execute
-------------------------------------------------------------------------------}
var
  P: TPoint;
begin
  if Sender = ActInsertSteelLocal then
  begin
    // convert global screen coordinate
    P := MapPopup.PopupPoint; // this is a global screen coordinate
    P := Img.ScreenToClient(P);
    P := Img.ControlToBitmap(P);
  end
  else
    P := CalcCenteredInsertionPoint;

  InternalInsertSteel(P);
end;


procedure TMapEditorForm.ActSetScreenPosition_Execute(Sender: TObject);
{-------------------------------------------------------------------------------
  See also: ActInsertObject_Execute
-------------------------------------------------------------------------------}
var
  P: TPoint;
  Rec: TDefaultsRec;
  Sx, Sy: Integer;
begin
  P := MapPopup.PopupPoint; // this is a global screen coordinate
  P := Img.ScreenToClient(P);
  P := Img.ControlToBitmap(P);

  Editor.Statics.ScreenPosition := P.X;
  Editor.Statics.ScreenYPosition := P.Y;
end;

procedure TMapEditorForm.ActCopy_Execute(Sender: TObject);
begin
  if Editor = nil then
    Exit;
  Editor.CopyToClipBoard;
end;

procedure TMapEditorForm.ActCut_Execute(Sender: TObject);
begin
  if Editor = nil then
    Exit;
  Editor.CopyToClipBoard;
  Editor.DeleteSelection;
end;

procedure TMapEditorForm.ActPaste_Execute(Sender: TObject);
{-------------------------------------------------------------------------------
  Action eventhandler. If
-------------------------------------------------------------------------------}
begin
  if Editor = nil then
    Exit;
(*  with Editor do
    if SelectorCount > 0 then
      Editor.PasteFromClipBoard(4, 4)
    else                          *)
      Editor.PasteFromClipBoard;//(fSelectionRect.Left, fSelectionRect.Top);
end;

procedure TMapEditorForm.ActPasteSpecial_Execute(Sender: TObject);
var
  H, V, C: Integer;
begin
  if Editor = nil then
    Exit;
  if ExecutePasteSpecialForm(H, V, C) then
  begin
    Editor.PasteFromClipBoard(H, V, C);
  end;
end;

procedure TMapEditorForm.ActShiftLevel_Execute(Sender: TObject);
var H, V: Integer;
begin
  if Editor = nil then
    Exit;
  if ExecuteShiftLevelForm(H, V) then
  begin
    Editor.ShiftLevel(H, V);
  end;
end;

procedure TMapEditorForm.ActImportLayout_Execute(Sender: TObject);
var
  TempEditor: TLevelEditor;
  OpenDlg: TOpenDialog;
  OpenFormat: TLemmingFileFormat;
begin
  if Editor = nil then Exit;
  TempEditor := TLevelEditor.Create(self);
  OpenDlg := TOpenDialog.Create(self);
  try
    if Editor.Style is TLemminiStyle then
    begin
      OpenDlg.Filter := 'Lemmini / SuperLemmini levels|*.ini';
      OpenFormat := lffLemmini;
    end else begin
      OpenDlg.Filter := 'Lemmix / NeoLemmix Levels|*.lvl';
      OpenFormat := lffLVL;
    end;
    OpenDlg.Title := 'Select level file(s)...';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    if not OpenDlg.Execute then Exit; // this does still execute the "finally" section :)
    TempEditor.LoadFromFile(OpenDlg.FileName, Editor.Style, OpenFormat);
    Editor.CopyLayoutFromLevel(TempEditor);
  finally
    OpenDlg.Free;
    TempEditor.Free;
  end;
end;

procedure TMapEditorForm.InternalImportLayout;
begin

end;

procedure TMapEditorForm.ActShowInspector_Execute(Sender: TObject);
begin
  fEditItemForm.Show;
end;

procedure TMapEditorForm.ActShowList_Execute(Sender: TObject);
begin
  fListForm.Show;
end;

procedure TMapEditorForm.ActShowStatics_Execute(Sender: TObject);
begin
  fStaticsForm.Show;
end;

procedure TMapEditorForm.ActShowSkills_Execute(Sender: TObject);
begin
  fSkillsForm.Show;
end;



procedure TMapEditorForm.ActShowImage_Execute(Sender: TObject);
begin
  if Img.CanFocus then
    Img.SetFocus;
end;

procedure TMapEditorForm.HandleCommandStr;
var
  B: TBaseLevelItem;
  I: Integer;
begin
  if Length(fCommandStr) < 1 then
    Exit;
  if Editor = nil then
    Exit;
  if Editor.SelectorCount <> 1 then
    Exit;

   I := StrToIntDef(fCommandStr, -1);
   if I = -1 then
     Exit;
   B := Editor.SelectorCollection[0].RefItem;
   case B.ItemType of
     litObject:
       begin
         if TInteractiveObject(B).ValidObjectID(s2i(fCommandStr)) then
           TInteractiveObject(B).ObjectID := s2i(fCommandStr);
       end;
     litTerrain:
       begin
         if TTerrain(B).ValidTerrainID(s2i(fCommandStr)) then
           TTerrain(B).TerrainID := s2i(fCommandStr);
       end;
   end;

//   CommandStr := '';

end;

procedure TMapEditorForm.SetCommandStr(const Value: string);
begin
  fCommandStr := Value;
  StatusBar.Panels[panel_commandstr].Text := fCommandStr;
  fCommandChangeTicks := GetTickCount;
end;


procedure TMapEditorForm.DoAfterSetEditor(Value: TLevelEditor);
begin
//  fInspectorForm.Editor := Value;
  fStaticsForm.Editor := Value;
  fSkillsForm.Editor := Value;
  //fSysdatForm.Editor := Value;
  fListForm.Editor := Value;
  fEditItemForm.Editor := Value;
  Value.UseInternalClipboard := App.EditorSettings.AltPasteMode;
end;

procedure TMapEditorForm.FormContextPopup(Sender: TObject; MousePos: TPoint; var Handled: Boolean);
begin
//  inherited;
//  Deb(['context', sender.classname]);
  handled := true;
end;

procedure TMapEditorForm.ActToggleTerrain_Execute(Sender: TObject);
begin
  with fRenderer do
  begin
    if litTerrain in RenderTypes
    then RenderTypes := RenderTypes - [litTerrain]
    else RenderTypes := RenderTypes + [litTerrain];
    ActToggleTerrain.Checked := litTerrain in RenderTypes;
    ReRender;
  //  fRenderer.RenderLevel(Editor);
  end;
end;

procedure TMapEditorForm.ActToggleObjects_Execute(Sender: TObject);
begin
  with fRenderer do
  begin
    if litObject in RenderTypes
    then RenderTypes := RenderTypes - [litObject]
    else RenderTypes := RenderTypes + [litObject];
    ActToggleObjects.Checked := litObject in RenderTypes;
    ReRender;
   //  fRenderer.RenderLevel(Editor);
  end;
end;

procedure TMapEditorForm.ActToggleSteel_Execute(Sender: TObject);
begin
  with fRenderer do
  begin
    if litSteel in RenderTypes
    then RenderTypes := RenderTypes - [litSteel]
    else RenderTypes := RenderTypes + [litSteel];
    ActToggleSteel.Checked := litSteel in RenderTypes;
    ReRender;
  //  fRenderer.RenderLevel(Editor);
  end;
end;

procedure TMapEditorForm.ActToggleBackground_Execute(Sender: TObject);
begin
  with fRenderer do
  begin
    if litBackground in RenderTypes
    then RenderTypes := RenderTypes - [litBackground]
    else RenderTypes := RenderTypes + [litBackground];
    ActToggleBackground.Checked := litBackground in RenderTypes;
    ReRender;
  //  fRenderer.RenderLevel(Editor);
  end;
end;

procedure TMapEditorForm.ActToggleTriggerAreas_Execute(Sender: TObject);
begin
  with fRenderer do
  begin
    if roShowTriggerAreas in RenderOptions
    then RenderOptions := RenderOptions - [roShowTriggerAreas]
    else RenderOptions := RenderOptions  + [roShowTriggerAreas];
    ActToggleTriggerAreas.Checked := roShowTriggerAreas in RenderOptions;
    ReRender;
//    fRenderer.RenderLevel(Editor);
  end;
end;

procedure TMapEditorForm.ActToggleScreenStart_Execute(Sender: TObject);
begin
  fScreenStartLayer.Visible := not fScreenStartLayer.Visible;
  ActToggleScreenStart.Checked := fScreenStartLayer.Visible;
end;

{procedure TMapEditorForm.ActViewLevels_Execute(Sender: TObject);
var
  A: TAction absolute Sender;
begin
  Assert(Sender is TAction, 'Sender error');
  if A.Tag >= 100 then
    if A.Tag <= 300 then
      FormViewLevels(StyleMgr[a.Tag - 100]).Show;
end;}


procedure TMapEditorForm.ActZoomIn_Execute(Sender: TObject);
begin
  ZoomIn;
end;

procedure TMapEditorForm.ActZoomOut_Execute(Sender: TObject);
begin
  ZoomOut;
end;


{procedure TMapEditorForm.ActStyleCompiler_Execute(Sender: TObject);
begin
  with TFormCompileStyle.create(nil) do
  begin
    ShowModal;
    Free;
  end;
end;}


procedure TMapEditorForm.ActValidateLevel_Execute(Sender: TObject);
//var
  //L: TStringList;
  //OK: Boolean;
begin
  if Editor = nil then
    Exit;
  {L := TStringList.Create;
  try
    OK := TCustomLevelValidator.ValidateLevel(Editor, L);
    if OK then
      L.Insert(0, 'Level OK')
    else
      L.Insert(0, 'Errors found');}
    ExecuteFormValidationReport(Editor);
  {finally
    L.Free;
  end;}
end;

procedure TMapEditorForm.ActDosDatDecompress_Execute(Sender: TObject);
begin
  ExecuteFormDosDecompress;
end;

procedure TMapEditorForm.ActPlay_Execute(Sender: TObject);
begin
  InternalPlayLevel(Editor);
end;

procedure TMapEditorForm.ActDownloadStyles_Execute(Sender: TObject);
var
  F: TFDownloadForm;
begin
  F := TFDownloadForm.Create(self);
  try
    F.ShowModal;
  finally
    F.Free;
  end;
end;

procedure TMapEditorForm.ActMassConvert_Execute(Sender: TObject);
begin
  LaunchConversion;
end;

procedure TMapEditorForm.ActionList_Update(Action: TBasicAction; var Handled: Boolean);
var
  Edit, {LevelOpen, }Select, MultiSelect, Clip: Boolean;
  n: Integer;
begin
  Edit         := Assigned(Editor) and Assigned(Editor.Style) and Assigned(Editor.Graph);
  Select       := Edit and (Editor.SelectorCollection.HackedList.Count > 0);
  MultiSelect  := Select and (Editor.SelectorCollection.HackedList.Count > 1);
  Clip         := ClipBoard.HasFormat(CF_TEXT);

  ActNewLevel.Enabled := Assigned(Editor);
//  ActOpenFile.Enabled             := Edit;
  //ActCloseLevel.Enabled := Edit;
  ActSave.Enabled := Edit;
  ActSaveAs.Enabled := Edit;
//  ActSaveInDosPack.Enabled := Edit and (Editor.Loader is TFormEditDosLevelPack) and
  //                           FormEditDosLevelPackActive; // for safety

  ActInsertObject.Enabled := Edit;
  ActInsertObjectLocal.Enabled := ActInsertObject.Enabled;

  ActInsertTerrain.Enabled := Edit;
  ActInsertTerrainLocal.Enabled := ActInsertTerrain.Enabled;

  ActInsertSteel.Enabled := Edit;
  ActInsertSteelLocal.Enabled := ActInsertSteel.Enabled;

  ActCopy.Enabled := Edit and Select;
  ActCut.Enabled := Edit and Select;
  ActPaste.Enabled := Edit and Clip;
  ActPasteSpecial.Enabled := Edit and Clip;

  ActAlignSelection.Enabled := Edit and MultiSelect;

  if Action.Tag >= 100 then
    if Action.Tag <= 300 then
    begin
      n := Action.Tag - 100;
      TAction(Action).Enabled := Assigned(StyleMgr[n].LevelLoader);
    end;

  Handled := True;
end;



procedure TMapEditorForm.ActAbout_Execute(Sender: TObject);
begin
  ShowAboutForm;
  //MessageDlg(SAboutStr, mtInformation, [mbOK], 0);
end;

{$ifdef develop}

procedure TMapEditorForm.ActTest_Execute(Sender: TObject);
begin
{  with TTestForm.Create(nil) do
  begin
    ShowModal;
    Free;
  end;
}
end;

procedure TMapEditorForm.ActShowSpriteFiles_Execute(Sender: TObject);
begin
  {
  with TFormShowSPR.Create(nil) do
  begin
    ShowModal;
    Free;
  end;
  }
end;

procedure TMapEditorForm.ActShowDOSGroundFile_Execute(Sender: TObject);
begin

  with TDOSGroundFileForm.Create(nil) do
  begin
    ShowModal;
    Free;
  end;
  
end;

procedure TMapEditorForm.ActPackExtGraphics_Execute(Sender: TObject);
begin
//  PackExtendedGraphics;
end;

procedure TMapEditorForm.ActWizardLemmini_Execute(Sender: TObject);
begin
  with TWizardLemminiCompiler.Create(nil) do
  begin
    ShowModal;
    Free;
  end;
end;

{$endif}

procedure TMapEditorForm.InternalPlayLevel(aLevel: TLevel);
var
  Mem: TMemoryStream;
  RunString, ParmString: String;
//  S: string;


begin
  if not LevelActive then
    Exit;

  if not ((Editor.Style is TBaseDosStyle) or (Editor.Style is TLemminiStyle)) then
  begin
    MessageDlg('Error: Playtest mode only supported for Lemmix-based or Lemmini-based styles.', mtError, [mbOk], 0);
    Exit;
  end;

  //RunString := TBaseDosStyle(Editor.Style).TestEXE;
  RunString := Editor.Style.TestEXE;

  if RunString = '' then
  begin
    MessageDlg('Error: No test player specified in Styles INI.', mtError, [mbOk], 0);
    Exit;
  end;

  if RunString[2] <> ':' then RunString := ExtractFilePath(ParamStr(0)) + RunString;

  if not FileExists(RunString) then
  begin
    MessageDlg('Error: Test player EXE does not exist.', mtError, [mbOk], 0);
    Exit;
  end;

  if Editor.Style is TLemminiStyle then
  begin
    ParmString := '-jar "' + RunString + '" ';
    RunString := App.EditorSettings.JavaPath;
  end;

  // try Lemmini
  {if aLevel.Style is TLemminiStyle then
  begin
    Mem := TMemoryStream.Create;
    try
      Clipboard.AsText := Editor.SaveToLemminiString;
    finally
      Mem.Free;
    end;
    Exit;
  end;}

  // try internal dos player
  {if not (aLevel.Style is TBaseDosStyle) then
  begin
    ShowMessage('Currentlevel is not a DOS level');
    Exit;
  end;}

  if Editor.Style is TBaseDosStyle then
  begin
    Editor.SaveToFile(ExtractFilePath(Application.ExeName) + 'temptestlevel', lffLVL, false);
    ParmString := 'testmode147 temptestlevel ';
    ParmString := ParmString + Editor.Style.CommonPath + ' 0'; // backwards compatibility, V1.47n does not like lack of any test mode indicator
  end else begin
    Editor.SaveToFile(ExtractFilePath(Application.ExeName) + 'temptestlevel.ini', lffLemmini, false);
    ParmString := ParmString + '-l "' + ExtractFilePath(Application.ExeName) + 'temptestlevel.ini"';
  end;

  DoShellExecute(RunString, ParmString);
  //MessageDlg(RunString, mtCustom, [mbOk], 0);

  {with TFormGame.Create(nil) do
  begin
    aLevel.LockLevel;
    LoadAndPlay(aLevel);
    Free;
    aLevel.UnLockLevel;
  end;}
end;

procedure TMapEditorForm.InternalInsertObject(P: TPoint);
var
  O: TInteractiveObject;
  NewID: Integer;
  Graph: TBaseGraphicSet;
begin
  if Editor = nil then
    Exit;
  NewID := 0;
  Graph := Editor.Graph;
  if not App.EditorSettings.NoPromptOnNewPiece then
  if not PickListObject(Graph, NewID) then Exit;
  if Graph.MetaObjectCollection[NewID].TriggerEffect = 32 then
  begin
    ShowMessage('You selected a background image object. These cannot be placed as regular objects, use the "Select Background" button' + #13 + 'in the Level Properties menu instead.');
    Exit;
  end;
  Editor.BeginUpdate;
  try
    O := Editor.InteractiveObjectCollection.Add;
    if O <> nil then
    begin
      O.ObjectX := P.X;
      O.ObjectY := P.Y;
      O.Graph := Graph;
      O.ObjectID := NewID;
      O.ObjectSValue := 0;
      O.ObjectLValue := 0;
      if App.EditorSettings.NoOverwriteDefault and not (O.Graph.MetaObjectCollection[NewID].TriggerEffect in [7, 8, 9, 10, 13, 16]) then
        O.ObjectDrawingFlags := [odfNoOverwrite]
      else
        O.ObjectDrawingFlags := [];
      Editor.SelectedObject := O;
      O.ItemActive := true;
    end;
  finally
    Editor.EndUpdate;
  end;
end;

procedure TMapEditorForm.InternalInsertTerrain(P: TPoint);
var
  T: TTerrain;
  NewID: Integer;
  Graph: TBaseGraphicSet;
begin
  if Editor = nil then
    Exit;
  NewID := 0;
  Graph := Editor.Graph;
  if not App.EditorSettings.NoPromptOnNewPiece then
    if not PickListTerrain(Graph, NewID) then Exit;
  Editor.BeginUpdate;
  try
    T := Editor.TerrainCollection.Add;
    if T <> nil then
    begin
      T.TerrainX := P.X;
      T.TerrainY := P.Y;
      T.Graph := Graph;
      T.TerrainID := NewID;
      T.TerrainDrawingFlags := [];
      Editor.SelectedTerrain := T;
      T.ItemActive := true;
      if App.EditorSettings.OneWayDefault and (T.Graph.MetaTerrainCollection[NewID].Unknown = 0) and ((Editor.Statics.AutoSteelOptions and 128) <> 0) then
        T.TerrainDrawingFlags := T.TerrainDrawingFlags + [tdfNoOneWay];
    end;
  finally
    Editor.EndUpdate;
  end;
end;

procedure TMapEditorForm.InternalInsertSteel(P: TPoint);
var
  S: TSteel;
begin
  if Editor = nil then
    Exit;
  Editor.BeginUpdate;
  try
    S := Editor.SteelCollection.Add;
    if S <> nil then
    begin
      S.SteelX := P.X;
      S.SteelY := P.Y;
      S.SteelWidth := 40;
      S.SteelHeight := 40;
      Editor.SelectedSteel := S;
      S.ItemActive := true;
    end;
  finally
    Editor.EndUpdate;
  end;
end;


procedure TMapEditorForm.InternalOpenFile(const aFileName: string; aType: Integer);
{-------------------------------------------------------------------------------
  Key routine to open a file. lemstrings
  Use the OFD_XXX consts as aType parameter
-------------------------------------------------------------------------------}
var
  aStyle: TBaseLemmingStyle;
begin
  aStyle := StyleMgr.Styles[0]; // important
  case aType of
    OFD_DOSLEVELPACK:
      begin
        FormEditDosLevelPack.FileName := aFileName;
        FormEditDosLevelPack.Show;
      end;
    OFD_RAWLEVEL:
      begin
          Editor.LoadFromFile(aFileName, aStyle, lffLVL);
          Editor.Loader := Self;
          Editor.History.Clear;
          Editor.HistoryPosition := -1;
          Editor.AddToHistory;
      end;
    OFD_LEMMINI:
      begin
        Editor.LoadFromFile(aFileName, aStyle, lffLemmini);
        Editor.Loader := Self;
        Editor.History.Clear;
        Editor.HistoryPosition := -1;
        Editor.AddToHistory;
      end;
    OFD_VGASPEC:
      begin
        ExecuteFormVgaSpecViewer(aFileName);
      end;
  end;


end;

procedure TMapEditorForm.Rerender;
var
  IsResizing: Boolean;
  P: TPoint;
begin
  if (Editor.Statics.Width <> Img.Bitmap.Width) or (Editor.Statics.Height <> Img.Bitmap.Height) then
  begin
    IsResizing := true;
    P := Img.BitmapToControl(Point(0, 0));
    P.X := P.X - trunc(16 * AvailableScales[fScaleIndex]);
    P.Y := P.Y - trunc(16 * AvailableScales[fScaleIndex]);
  end else
    IsResizing := false;
  fRenderer.RenderLevel(Editor);
  if IsResizing then
    Img.Scroll(0-P.X, 0-P.Y);
end;

function TMapEditorForm.CheckCanDrag: Boolean;
var
  Def: TDefaultsRec;
begin
  Result := True;
  exit;
  Result := False;
  if Editor = nil then exit;
  if editor.graph =nil then exit;
  editor.Style.GetStyleParams(def);
  if def.Resolution = 1.0 then
    Result := fScaleIndex <= 5
  else result := fScaleIndex <= 4;
end;

procedure TMapEditorForm.Button2Click(Sender: TObject);
begin
end;

{ txlist }

procedure TMapEditorForm.Button4Click(Sender: TObject);
begin
//  formviewlevels(StyleMgr[style_dosohno]).show;
  DosOrigStyle.AnimationSet.ReadData;

  ShowImageForm(dosorigstyle.AnimationSet.AnimationBitmaps[0]);
//  DosHoliday93Style.AnimationSet.ReadData;
end;

procedure TMapEditorForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  Img_KeyPress(Sender, Key);
  Key := #0;
end;

procedure TMapEditorForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Img_KeyDown(Sender, Key, Shift);
  Key := 0;
end;

procedure TMapEditorForm.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Key := 0;
end;

end.
