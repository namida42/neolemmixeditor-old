unit LemStartupPlayer;

interface

procedure StartupPlayer;

implementation

uses
  Forms, FPlayer;

procedure StartupPlayer;
begin
  Application.ShowMainForm := False;
  Application.CreateForm(TFormPlayer, FormPlayer);
  Application.ShowMainForm := True;
end;

end.

