{$include lem_directives.inc}
{-------------------------------------------------------------------------------
  Unit which contains the types for the original DOS files. Guided by the
  document(s) of ccexplore and mindless.
-------------------------------------------------------------------------------}


unit LemDosFiles;

//lemtypes
interface

uses
  Classes, SysUtils,
  UMisc;

{-------------------------------------------------------------------------------
  MAIN.DAT section 0 (21.104 bytes)


   offset (hex)  description        # of frames  width x height  bpp
   ------------  -----------------  -----------  --------------  ---
   [0x0000]     walking (r)              8          16x10        2
   [0x0140]     jumping (r)              1          16x10        2
   [0x0168]     walking (l)              8          16x10        2
   [0x02A8]     jumping (l)              1          16x10        2
   [0x02D0]     digging                 16          16x14        3
   [0x0810]     climbing (r)             8          16x12        2
   [0x0990]     climbing (l)             8          16x12        2
   [0x0B10]     drowning                16          16x10        2
   [0x0D90]     post-climb (r)           8          16x12        2
   [0x0F10]     post-climb (l)           8          16x12        2
   [0x1090]     brick-laying (r)        16          16x13        3
   [0x1570]     brick-laying (l)        16          16x13        3
   [0x1A50]     bashing (r)             32          16x10        3
   [0x21D0]     bashing (l)             32          16x10        3
   [0x2950]     mining (r)              24          16x13        3
   [0x30A0]     mining (l)              24          16x13        3
   [0x37F0]     falling (r)              4          16x10        2
   [0x3890]     falling (l)              4          16x10        2
   [0x3930]     pre-umbrella (r)         4          16x16        3
   [0x3AB0]     umbrella (r)             4          16x16        3
   [0x3C30]     pre-umbrella (l)         4          16x16        3
   [0x3DB0]     umbrella (l)             4          16x16        3
   [0x3F30]     splatting               16          16x10        2
   [0x41B0]     exiting                  8          16x13        2
   [0x4350]     fried                   14          16x14        4
   [0x4970]     blocking                16          16x10        2
   [0x4BF0]     shrugging (r)            8          16x10        2
   [0x4D30]     shrugging (l)            8          16x10        2
   [0x4E70]     oh-no-ing               16          16x10        2
   [0x50F0]     explosion                1          32x32        3
------------------------------------------------------------------------------}

{type
  TAnimationInfo = record
    FrameCount : Byte;
    Width      : Byte;
    Height     : Byte;
    BPP        : Byte;
  end;      }

//const


//const


type
  TBytes4 = array[0..3] of Byte;

  // frame (16x10 pixels, 2 bits per pixel = 160/4 = 40 bytes)
  TDOSAnimationFrame_16x10x2 = packed record
    case Integer of
      0: (
           Bytes           : array[0..39] of Byte;
         );
      1: (
           LinesA          : array[0..9] of TBytes4;
         );
      2: (
           LinesC          : array[0..9] of Cardinal;
         );
  end;

  // animation
  TDOSWalking_Animation = array[0..7] of TDOSAnimationFrame_16x10x2;
  TDOSJumping_Animation = array[0..0] of TDOSAnimationFrame_16x10x2;

  // total cased record to access information in different ways
  TDOSMainDat0 = packed record
    case Integer of
      0: (
           WalkingRBytes       : array[$0000..$0139] of Byte;
           JumpingRBytes       : array[$0000..$0027] of Byte;
         );
      1: (
           WalkingRAnimation   : TDOSWalking_Animation;
           JumpingRAnimation   : TDOSJumping_Animation;
         );
  {    2: (
           WalkingRFrame00: TDOSWalkingR_Frame;
         ); }

//      2
  end;



{-------------------------------------------------------------------------------
  GROUNDXX.DAT files (1056 bytes) (16 objects, 64 terrain, color palette)
  o See documentation for details
  o Little Endian words, so we can just load them from disk halleluyah!

-------------------------------------------------------------------------------}

type
  PDOSGroundObject = ^TDOSGroundObject;
  TDOSGroundObject = packed record
    oAnimation_flags               : Word; // 2
    oStart_animation_frame_index   : Byte; // 3
    oEnd_animation_frame_index     : Byte; // 4
    oWidth                         : Byte; // 5
    oHeight                        : Byte; // 6
    oAnimation_frame_data_size     : Word; // 8   size in bytes of each animation frame
    oMask_offset_from_image        : Word; // 10
    oUnknown1                      : Word; // 12
    oUnknown2                      : Word; // 14
    oTrigger_left                  : Byte; // 16
    oTrigger_pointx                : Byte;
    oTrigger_top                   : Byte; // 18
    oTrigger_pointy                : Byte;
    oTrigger_width                 : Byte; // 19
    oTrigger_height                : Byte; // 20
    oTrigger_effect_id             : Byte; // 21
    oAnimation_frames_base_loc     : Word; // 23
    oPreview_image_location        : Word; // 25
    oUnknown3                      : Word; // 27
    oTrap_sound_effect_id          : Byte; // 28
  end;
  TDOSGroundObjectArray = packed array[0..15] of TDOSGroundObject;

  PDOSGroundTerrain = ^TDOSGroundTerrain;
  TDOSGroundTerrain = packed record
    tWidth        : Byte;
    tHeight       : Byte;
    tImage_loc    : Word;
    tMask_loc     : Word;
    tUnknown1     : Word
  end;
  TDOSGroundTerrainArray = packed array[0..63] of TDOSGroundTerrain;

  TDOSGroundEGAPaletteArray8 = packed array[0..7] of Byte;

  TDosVgaColorRec = packed record
    R, G, B: Byte;
  end;

  TDosGroundVGAPaletteArray8 = packed array[0..7] of TDOSVGAColorRec;

  TDosGroundRec = packed record
    case Integer of
      1: (
            ObjectInfoArray     : TDOSGroundObjectArray;
            TerrainInfoArray    : TDOSGroundTerrainArray;
            EGA_PaletteCustom   : TDOSGroundEGAPaletteArray8;
            EGA_PaletteStandard : TDOSGroundEGAPaletteArray8;
            EGA_PalettePreview  : TDOSGroundEGAPaletteArray8;
            VGA_PaletteCustom   : TDOSGroundVGAPaletteArray8; // rgb * 4!!!!
            VGA_PaletteStandard : TDOSGroundVGAPaletteArray8; // rgb * 4!!!!
            VGA_PalettePreview  : TDOSGroundVGAPaletteArray8; // rgb * 4!!!!
         );
  end;

  TDosGroundVGAPaletteArray16 = packed array[0..15] of TDOSVGAColorRec;

  TDosVgaSpecPaletteHeader = packed record
    VgaPal: TDOSGroundVGAPaletteArray8; // 24
    EgaPal: TDOSGroundEGAPaletteArray8; // 8
    UnknownPal: array[0..7] of Byte; // maybe even less colors
  end;

  TDosOddTableRec = packed record
    _ReleaseRateNotUsed        : Byte; // big/little endian's
    ReleaseRate                : Byte;
    _LemmingsCountNotUsed      : Byte;
    LemmingsCount              : Byte;
    _RescueCountNotUsed        : Byte;
    RescueCount                : Byte;
    _TimeLimitNotUsed          : Byte;
    TimeLimit                  : Byte;
    _ClimberNotUsed            : Byte;
    ClimberCount               : Byte;
    _FloaterNotUsed            : Byte;
    FloaterCount               : Byte;
    _BomberNotUsed             : Byte;
    BomberCount                : Byte;
    _BlockerNotUsed            : Byte;
    BlockerCount               : Byte;
    _BuilderNotUsed            : Byte;
    BuilderCount               : Byte;
    _BasherNotUsed             : Byte;
    BasherCount                : Byte;
    _MinerNotUsed              : Byte;
    MinerCount                 : Byte;
    _DiggerNotUsed             : Byte;
    DiggerCount                : Byte;
    LevelName: array[0..31] of Char;
  end;

  TDosOddTable = class
  private
  protected
  public
    Recs: array of TDosOddTableRec;
    procedure LoadFromFile(const aFilename: string);
  published
  end;

type
  TDosGround = class
  private
  protected
  public
    GroundRec: TDOSGroundRec;
    GroundRec2: TDosGroundRec;
    procedure LoadFromFile(const aFilename: string);
  published
  end;


const
  { TODO : do not mix original rgb and converted rgb }
  DosMainMenuPalette: TDOSGroundVGAPaletteArray16 = (
    (R: 000; G: 000; B: 000), // black
    (R: 128; G: 064; B: 032), // browns
    (R: 096; G: 048; B: 032),
    (R: 048; G: 000; B: 016),
    (R: 032; G: 008; B: 124), // purples
    (R: 064; G: 044; B: 144),
    (R: 104; G: 088; B: 164),
    (R: 152; G: 140; B: 188),
    (R: 000; G: 080; B: 000), // greens
    (R: 000; G: 096; B: 016),
    (R: 000; G: 112; B: 032),
    (R: 000; G: 128; B: 064),
    (R: 208; G: 208; B: 208), // white
    (R: 176; G: 176; B: 000), // yellow
    (R: 064; G: 080; B: 176), // blue
    (R: 224; G: 128; B: 144)  // pink
  );
  DosRed: TDOSVGAColorRec = (R: 240; G: 032; B: 032);


  DosInLevelPalette: TDOSGroundVGAPaletteArray8 = (
    (R: 000; G: 000; B: 000), // black
    (R: 016; G: 016; B: 056), // blue
    (R: 000; G: 044; B: 000), // green
    (R: 060; G: 052; B: 052), // white
    (R: 044; G: 044; B: 000), // yellow
    (R: 060; G: 008; B: 008), // red
    (R: 032; G: 032; B: 032), // grey
    (R: 000; G: 000; B: 000) // not used: probably this color is replaced with the standard palette entry in ground??.dat
  );

(*  DosInLevelPaletteChristmas: TDOSGroundVGAPaletteArray8 = (
    (R: 000; G: 000; B: 000), // black
    (R: 060; G: 008; B: 008), // red
    (R: 000; G: 044; B: 000), // green
    (R: 060; G: 052; B: 052), // white
    (R: 044; G: 044; B: 000), // yellow
    (R: 016; G: 016; B: 056), // blue
    (R: 032; G: 032; B: 032), // grey
    (R: 000; G: 000; B: 000) // not used: probably this color is replaced with the standard palette entry in ground??.dat
  );*)

//  clDosWhite32 =

(*
[colorlist values][0][0][44][0][$FF00B000]
[colorlist values][1][8][24][8][$FF206020]
[colorlist values][2][16][16][20][$FF404050]
[colorlist values][3][16][16][56][$FF4040E0]
[colorlist values][4][24][0][4][$FF600010]
[colorlist values][5][24][24][28][$FF606070]
[colorlist values][6][28][36][0][$FF709000]
[colorlist values][7][32][32][32][$FF808080]
[colorlist values][8][36][8][4][$FF902010]
[colorlist values][9][48][20][4][$FFC05010]
[colorlist values][10][52][32][8][$FFD08020]
[colorlist values][11][56][32][8][$FFE08020]
[colorlist values][12][60][8][8][$FFF02020]
[colorlist values][13][60][52][52][$FFF0D0D0]
[colorlist values][14][60][60][0][$FFF0F000]
[colorlist values][15][0][0][0][$0]
*)


  DosInLevelPaletteConverted: TDOSGroundVGAPaletteArray8 = (
    (R: 000; G: 000; B: 000), // black
    (R: 064; G: 064; B: 224), // blue
    (R: 000; G: 176; B: 000), // green
    (R: 240; G: 208; B: 208), // white
    (R: 176; G: 176; B: 000), // yellow
    (R: 240; G: 032; B: 032), // red
    (R: 128; G: 128; B: 128), // grey
    (R: 000; G: 000; B: 000) // not used
  );


implementation

{ TDosOddTable }

procedure TDosOddTable.LoadFromFile(const aFilename: string);
var
  F: TFileStream;
  i: Integer;
  s: string;
begin
  F := TFileStream.Create(aFileName, fmOpenRead);
  try
    SetLength(Recs, F.Size div SizeOf(TDosOddTableRec));
//    if F.Size <> SizeOf(GroundRec) then
  //    raise exception.create(ClassName + ' load error (size)');
    for i := 0 to Length(Recs) - 1 do
    begin
      F.Read(Recs[i], Sizeof(TDosOddTableRec));
      s := trim(recs[i].LevelName);
      {
      with recs[i] do
      deb([padr(s, 32),
      lemmingscount, rescuecount,
    TimeLimit,
    ClimberCount,
    FloaterCount,
    BomberCount,
    BlockerCount,
    BuilderCount,
    BasherCount  ,
    MinerCount    ,
    DiggerCount    ]);}
    end;

  finally
    F.Free;
  end;
end;


{ TDosGround }

procedure TDosGround.LoadFromFile(const aFilename: string);
var
  F: TFileStream;
  i: integer;
  b: ^byte;
begin
  F := TFileStream.Create(aFileName, fmOpenRead);
  try
    if (F.Size <> SizeOf(GroundRec))
    and (F.Size <> SizeOf(GroundRec) * 2) then
      raise exception.create(ClassName + ' load error (size)');
    F.Read(GroundRec, SizeOf(GroundRec));
    if F.Size = SizeOf(GroundRec) * 2 then
      F.Read(GroundRec2, SizeOf(GroundRec))
      else begin
      b := @GroundRec2;
      for i := 0 to SizeOf(GroundRec2) - 1 do
      begin
        b^ := 0;
        Inc(b);
      end;
    end;
  finally
    F.Free;
  end;
end;

end.



