unit LemGraphics;

interface

uses
  lemtypes,  ulog, Windows, Graphics, GR32, GIFImage, UTools, sysutils;

procedure GetDistinctColors(Bmp: TBitmap32; aList: TIntegerList; ClearList: Boolean = False);

function GetBitmapPalette(Bmp: TBitmap32; var Pal: TPalette32; aMaxColors: Integer = 255): Integer;
procedure Palette32ToGifColorMap(const Pal: TPalette32; GifMap: TColorMap);
procedure BitmapToGif(Bmp: TBitmap32; Gif: TGifImage);




implementation

procedure GetDistinctColors(Bmp: TBitmap32; aList: TIntegerList; ClearList: Boolean = False);
var
  x, y, i: Integer;
  C: PColor32;
begin
  aList.Sorted := True;
  with Bmp do
  begin
    C := PixelPtr[0,0];
    for y := 0 to Height - 1 do
      for x := 0 to Width - 1 do
      begin
        if aList.IndexOf(Integer(C^)) < 0 then
          aList.Add(Integer(C^));
        Inc(C);
      end;
  end;
end;

function GetBitmapPalette(Bmp: TBitmap32; var Pal: TPalette32; aMaxColors: Integer = 255): Integer;
{-------------------------------------------------------------------------------
  Create 32bits color palette from bitmap
-------------------------------------------------------------------------------}
var
  C: PColor32;
  x, y: Integer;
  List: TIntegerList;
  PalIndex: Integer;

    procedure AddColor;
    begin
      if List.IndexOf(Integer(C^)) < 0 then
      begin
        Pal[PalIndex] := C^;
        Inc(PalIndex);
        if PalIndex > aMaxColors then
          raise Exception.Create('GetBitmapPalette too many colors');
        List.Add(Integer(C^));
      end;
    end;

begin
  if aMaxColors > 255 then
    aMaxColors := 255;
  Result := 1;
  PalIndex := 1;
  List := TIntegerList.Create;
  List.Sorted := True;
  try
    FillChar(Pal, SizeOf(Pal), 0);
    with Bmp do
    begin
      C := PixelPtr[0,0];
      for y := 0 to Height - 1 do
        for x := 0 to Width - 1 do
        begin
          if C^ <> 0 then
            AddColor;
          Inc(C);
        end;
    end;
    Result := PalIndex;
  finally
    List.Free;
  end;
end;

procedure Palette32ToGifColorMap(const Pal: TPalette32; GifMap: TColorMap);
var
  i: Integer;
begin
  FillChar(GifMap, SizeOf(GifMap), 0);
  for i := 0 to 255 do
    with GifMap[i] do
    begin
      Red := RedComponent(Pal[i]);
      Green := GreenComponent(Pal[i]);
      Blue := BlueComponent(Pal[i]);
//      deb([i, red,green,blue])
    end;
{  for i := 0 to 255 do
    with GifMap[i] do
    begin
      deb([i, red,green,blue])
    end; }
end;

procedure BitmapToGif(Bmp: TBitmap32; Gif: TGifImage);
var
  Pal: TPalette32;
  y,x: integer;
  bytes:pbytes;
  PalCount: Integer;
  GifMap: TColorMap;
  iPal: TColor;
  ColorCount: Integer;
  IBmp: TBitmap;
  sub: TGIFSubImage;
begin
  PalCount := GetBitmapPalette(Bmp, Pal);
  Palette32ToGifColorMap(Pal, GifMap);


  IBmp := TBitmap.Create;
  IBmp.Assign(Bmp);
//  Gif.Header.BackgroundColorIndex := 0;

  Gif.GlobalColorMap.ImportColorMap(GifMap, 256);

  sub:=TGIFSubImage.Create(Gif);
  sub.Width := bmp.width;
  sub.height := bmp.height;
//  gif.pixe
//  sub.height := 20;
  gif.Images.Add(TGIFSubImage.Create(Gif));

  for y := 0 to sub.height-1 do
  begin
    bytes:=sub.scanline[y];
    for x := 0 to sub.width-1 do
      bytes^[x] := 1;
  end;

//  sub.Scanline[0]
  //sub.Assign();



//  Gif.Assign(IBmp);
  //Gif.OptimizeColorMap;
//  gif.Images[0].ColorMap.Optimize;
//  gif.BackgroundColor := clBlack;
  //gif.width := 20;
  //gif.height := 20;

//  Gif.GlobalColorMap.Assign(gif.Images[0].ColorMap);
//  Gif.Images[0].GraphicControlExtension.TransparentColor := clblack;

  //  Ibmp.transparent:=true;
  //ibmp.TransparentColor:=clblack;
//  gif.Header.ColorMap.Assign(gif.Images[0].ColorMap);
//Gif.OptimizeColorMap;
//  gif.BackgroundColor := clBlack;
//  Gif.Optimize([], rmNone{Gif.ColorReduction}, Gif.DitherMode, Gif.ReductionBits);

//  Gif.Transparent := True;
//  Gif.BackgroundColor := clBlack;

//  deb([gif.BackgroundColorIndex]);
//  Gif.GlobalColorMap.
//  gif.Images[0].b
 // deb(['subimages', gif.Images.count]);
  //deb(['subimagecolorcount', gif.Images[0].ColorMap.Count]);
//  gif.col
//  deb([gif.BackgroundColorIndex, Gif.BackgroundColor]);
  (*
  ColorCount := GetBitmapPalette(Bmp, Pal);
  with Gif, Header do
  begin
  //  ColorMap.c
    //GIFVersions
    ColorMap.Clear;
    for iPal := 0 to ColorCount - 1 do
      ColorMap.AddUnique(WinColor(Pal[iPal]));


    //Width := Bmp.Width;
//    Height := Bmp.Height;
  //  Images.Add(TGIFSubImage.Create(Gif));
  end;
  *)
  Ibmp.Free;
end;

end.

