object FormCompileStyle: TFormCompileStyle
  Left = 93
  Top = 213
  Width = 352
  Height = 212
  Caption = 'Create compiled archive(s) of graphic set(s)'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Form_Create
  PixelsPerInch = 96
  TextHeight = 13
  Position = poMainFormCenter
  object MainPanel: TSplitPanel
    Left = 0
    Top = 0
    Width = 344
    Height = 185
    Align = alClient
    Caption = 'MainPanel'
    Constraints.MinHeight = 180
    Constraints.MinWidth = 300
    TabOrder = 0
    BorderWidth = 4
    BevelKind = bkNone
    BevelOuter = bvNone
    SplitMode = smVertical
    SplitPosition = spNone
    object TSplitPanel
      Left = 0
      Top = 0
      Width = 336
      Height = 141
      Constraints.MinHeight = 4
      Constraints.MinWidth = 4
      TabOrder = 0
      BevelKind = bkNone
      BevelOuter = bvNone
      SplitMode = smVertical
      SplitPosition = spNone
      object VTree: TVirtualStringTree
        Left = 0
        Top = 0
        Width = 336
        Height = 141
        Align = alClient
        CheckImageKind = ckLightTick
        Header.AutoSizeIndex = 0
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        Header.MainColumn = -1
        Header.Options = [hoColumnResize, hoDrag]
        TabOrder = 0
        OnFocusChanged = VTree_FocusChanged
        OnFreeNode = VTree_FreeNode
        OnGetText = VTree_GetText
        OnGetImageIndex = VTree_GetImageIndex
        Columns = <>
      end
    end
    object TSplitPanel
      Left = 0
      Top = 145
      Width = 336
      Height = 32
      Constraints.MaxHeight = 32
      Constraints.MinHeight = 32
      Constraints.MinWidth = 4
      TabOrder = 2
      BevelKind = bkNone
      BevelOuter = bvNone
      SplitMode = smVertical
      SplitPosition = spLast
      DesignSize = (
        336
        32)
      object Button1: TButton
        Left = 0
        Top = 6
        Width = 201
        Height = 25
        Caption = 'Compile'
        Enabled = False
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 261
        Top = 6
        Width = 75
        Height = 25
        Anchors = [akRight]
        Caption = 'Close'
        TabOrder = 1
        OnClick = Button2Click
      end
    end
  end
end
