{$include lem_directives.inc}


unit FVgaSpecViewer;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtDlgs, StdCtrls, GR32_Image, LemDosBmp,
  ComCtrls;

type
  TFormVgaSpecViewer = class(TForm)
    Img: TImgView32;
    BtnClose: TButton;
    procedure BtnCloseClick(Sender: TObject);
    procedure Form_Create(Sender: TObject);
  private
    fFileName: string;
    procedure SetFileName(const Value: string);
  public
    property FileName: string read fFileName write SetFileName;
  end;

procedure ExecuteFormVgaSpecViewer(const aFileName: string);

implementation

{$R *.dfm}

procedure ExecuteFormVgaSpecViewer(const aFileName: string);
var
  F: TFormVgaSpecViewer;
begin
  F := TFormVgaSpecViewer.Create(nil);
  try
    F.FileName := aFileName;
    F.ShowModal;
  finally
    F.Free;
  end;
end;

procedure TFormVgaSpecViewer.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFormVgaSpecViewer.SetFileName(const Value: string);
var
  Vga : TVgaSpecBitmap;
begin
  if CompareText(fFileName, Value) = 0 then
    Exit;

  Vga := TVgaSpecBitmap.Create;
  try
//    Vga.OnCompressProgress := DoCompressProgress;
    Vga.LoadFromFile(Value, Img.Bitmap);
  finally
    Vga.Free;
  end;

  fFileName := Value; // set it after successfull load
end;

procedure TFormVgaSpecViewer.Form_Create(Sender: TObject);
begin
  Img.ScaleMode := smOptimalScaled;  
end;

end.
//lemstrings
