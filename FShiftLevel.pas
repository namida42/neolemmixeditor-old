{$include lem_directives.inc}

unit FShiftLevel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UMasterControls, Buttons, UMisc;

type
  TShiftLevelForm = class(TForm)
    EditHorz: TEditEx;
    EditVert: TEditEx;
    //EditCopies: TEditEx;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    MasterLabel1: TMasterLabel;
    MasterLabel2: TMasterLabel;
    //MasterLabel3: TMasterLabel;
  private
  public
  end;

function ExecuteShiftLevelForm(var Horz, Vert{, Count}: Integer): Boolean;

implementation

{$R *.dfm}

function ExecuteShiftLevelForm(var Horz, Vert{, Count}: Integer): Boolean;
var
  F: TShiftLevelForm;
begin
  F := TShiftLevelForm.Create(nil);
  try
    F.Position := poMainFormCenter;
    Result := F.ShowModal = mrOK;
    if Result then
    begin
      Horz := s2i(F.EditHorz.Text);
      Vert := s2i(F.EditVert.Text);
      //Count := s2i(F.EditCopies.Text);
      //Restrict(Count, 0, 64);
    end;
  finally
    F.Free;
  end;
end;

end.

