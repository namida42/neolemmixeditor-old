{$include lem_directives.inc}

unit FPickTerrain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Contnrs,
  Dialogs, UMx, GR32, Math, umisc,
  LemStrings,
  LemCore, StdCtrls, Buttons;

const
  MAX_PICTURE_WIDTH = 512;
  MAX_PICTURE_HEIGHT = 256;

type
  TTerrainMatrixData = class(TCellData)
  private
    Index: Integer;
    Bmp: TBitmap32; // ref
    Frame: TBitmap32; // instance
  protected
    procedure Clear; override;
  public
    constructor Create;
    destructor Destroy; override;
  end;

  TTOMode = (
    tNone,
    tTerrains,
    tObjects
  );

  TPickTerrainForm = class(TForm)
    TerrainMatrix: TSuperMatrix;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    cbStyle: TComboBox;
    Label1: TLabel;
    ScrollBar1: TScrollBar;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TerrainMatrix_GetCellData(Sender: TObject; aCol, aRow: Integer; out Data: TCellData);
    procedure TerrainMatrix_PaintCell(Sender: TObject; Params: TCellParams; Data: TCellData);
    procedure TerrainMatrixCellDblClick(Sender: TObject; aCol,
      aRow: Integer);
    procedure cbStyleChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure ScrollBar1Change(Sender: TObject);
    procedure TerrainMatrixSelectCell(Sender: TObject; aCol,
      aRow: Integer);
  private
    fGraphicSet: TBaseGraphicSet;//Integer;
    fCellData: TTerrainMatrixData;
    fScale: Integer;
    fMode: TTOMode;
    fOK: Boolean;
    fLocalImages: TObjectList; // we don't really need to know that they're TBitmap32s, as this is just for freeing them later
//    fObjBmp: TBitmap32;
    procedure AdjustMatrix;
    procedure SetGraphicSet(const Value: TBaseGraphicSet);
    procedure SetMode(const Value: TTOMode);
    function GetCurrentID: Integer;
    procedure SetCurrentID(const Value: Integer);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property GraphicSet: TBaseGraphicSet read fGraphicSet write SetGraphicSet;
//    property Graph: TBaseGraphicSet read fGraph write SetGraph;
    property Mode: TTOMode read fMode write SetMode;
    property CurrentID: Integer read GetCurrentID write SetCurrentID;
  end;

function PickListTerrain(var aGraphicSet: TBaseGraphicSet; var aTerrainID: Integer; AllowGSChange: Boolean = true): Boolean;
function PicklistObject(var aGraphicSet: TBaseGraphicSet; var aObjectID: Integer; AllowGSChange: Boolean = true): Boolean;

implementation

{$R *.dfm}

const
  picture_column = 1;

function DoPicklist(var aGraphicSet: TBaseGraphicSet; aMode: TTOMode; var aID: Integer; AllowGSChange: Boolean): Boolean;
var
  F: TPickTerrainForm;
begin
  Result := False;
//  aTerrainID := -1;
  F := TPickTerrainForm.Create(nil);
  with F do
  begin
    Position := poMainFormCenter;
    GraphicSet := aGraphicSet;
    Mode := aMode;
    CurrentID := aID;
    cbStyle.Enabled := AllowGSChange;
    if ShowModal = mrOK then
    begin
      Result := True;
      aGraphicSet := GraphicSet;
      aID := CurrentID;
    end;
    Free;
  end;
end;

function PicklistTerrain(var aGraphicSet: TBaseGraphicSet; var aTerrainID: Integer; AllowGSChange: Boolean = true): Boolean;
begin
  Result := DoPicklist(aGraphicSet, tTerrains, aTerrainID, AllowGSChange);
end;

function PicklistObject(var aGraphicSet: TBaseGraphicSet; var aObjectID: Integer; AllowGSChange: Boolean = true): Boolean;
begin
  Result := DoPicklist(aGraphicSet, tObjects, aObjectID, AllowGSChange);
end;

{ TTerrainMatrixData }

procedure TTerrainMatrixData.Clear;
begin
  inherited Clear;
  Index := 0;
  Bmp := nil;
//  Frame
end;


constructor TTerrainMatrixData.Create;
begin
  inherited;
  Frame := TBitmap32.Create;
end;

destructor TTerrainMatrixData.Destroy;
begin
  Frame.Free;
  inherited;
end;

{ TPickTerrainForm }

constructor TPickTerrainForm.Create(aOwner: TComponent);
begin
  inherited;
  fLocalImages := TObjectList.Create(true);
end;

destructor TPickTerrainForm.Destroy;
begin
  fLocalImages.Free;
  inherited;
end;

procedure TPickTerrainForm.AdjustMatrix;
var
  W, H, i: Integer;
  Bmp, SrcBmp: TBitmap32;
begin
  if fGraphicSet = nil then
    Exit;
  if fMode = tNone then
    Exit;

  case fMode of
    tTerrains : Caption := 'Pick Terrain';
    tObjects  : Caption := 'Pick Object';
  end;


  with TerrainMatrix do
  begin
    Bmp := nil;
    PaintLock;
    try
      case fMode of
        tNone: RowCount := 1;
        tTerrains: RowCount := GraphicSet.MetaTerrainCollection.Count;// PictureMgr.TerrainBitmapCount[fGraphicSet];
        tObjects: RowCount := GraphicSet.MetaObjectCollection.Count;// PictureMgr.ObjectBitmapCount[fGraphicSet];
      end;


      for i := 0 to RowCount - 1 do
      begin
        //Bmp := PictureMgr.TerrainBitmaps[fGraphicSet, i];
        case fMode of
          tTerrains: Bmp := GraphicSet.TerrainBitmaps[i];
          tObjects: Bmp := GraphicSet.ObjectBitmaps[i];
        end;

        if Bmp <> nil then
        begin
//          if Bmp.Width * fScale > ColWidths[2]  * fScale then ColWidths[2] := Bmp.Width  * fScale;
          W := (Bmp.Width * 2) * fScale - fScale + 4 {extra};
          if W > MAX_PICTURE_WIDTH then W := MAX_PICTURE_WIDTH;
          //ColWidths[picture_column] := Max(ColWidths[picture_column], Bmp.Width * fScale - fScale + 4 {extra});
          if i = 0 then
            ColWidths[picture_column] := W
          else
            ColWidths[picture_column] := Max(ColWidths[picture_column], W);

          if fMode = tObjects then
            H := GraphicSet.MetaObjectCollection[i].Height * 2
          else
            H := (Bmp.Height * 2);
          H := H * fScale - fScale + 4;
          if H > MAX_PICTURE_HEIGHT then H := MAX_PICTURE_HEIGHT;
          RowHeights[i] := Max(H(*H * fScale - fScale + 4{extra}*), 19);

        end
      end;

      Width := ColWidths[0] + ColWidths[1] + ColWidths[2] + 6;
    finally
      PaintUnlock;
    end;
  end;

  OnCanResize := nil;
  ClientWidth := TerrainMatrix.Width + ScrollBar1.Width;
  OnCanResize := FormCanResize;

  if TerrainMatrix.RowCount = 0 then
    ScrollBar1.Max := 0
  else
    ScrollBar1.Max := TerrainMatrix.RowCount - 1;
end;

procedure TPickTerrainForm.SetGraphicSet(const Value: TBaseGraphicSet);
var
  i: Integer;
begin
  cbStyle.OnChange := nil;
  cbStyle.ItemIndex := -1; // fallback
  for i := 0 to cbStyle.Items.Count-1 do
    if cbStyle.Items.Objects[i] = Value then
    begin
      cbStyle.ItemIndex := i;
      Break;
    end;

  if cbStyle.ItemIndex = -1 then
  begin
    cbStyle.ItemIndex := 0;
    fGraphicSet := TBaseGraphicSet(cbStyle.Items.Objects[0]);
  end else
    fGraphicSet := Value;
    
  fGraphicSet.EnsureMetaData;
  fGraphicSet.EnsureData;
  cbStyle.OnChange := cbStyleChange;
  AdjustMatrix;
end;

procedure TPickTerrainForm.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  fGraphicSet := nil;//-1;
  fScale := 1;
  fCellData := TTerrainMatrixData.Create;
//  fObjBmp := TBitmap32.Create;
  cbStyle.Items.Clear;
  for i := 0 to StyleMgr[0].GraphicSetCount-1 do
  begin
    if StyleMgr[0].GraphicSets[i].IsSpecial then Continue;
    if StyleMgr[0].GraphicSets[i].GraphicSetInternalName = 'placeholder' then Continue;
    cbStyle.AddItem(StyleMgr[0].GraphicSets[i].GraphicSetName, StyleMgr[0].GraphicSets[i]);
  end;
end;

procedure TPickTerrainForm.FormDestroy(Sender: TObject);
begin
  fCellData.Free;
//  fObjBitmap.Free;
end;

procedure TPickTerrainForm.TerrainMatrix_GetCellData(Sender: TObject; aCol, aRow: Integer; out Data: TCellData);
var
  UseFrame: Integer;
  SrcBmp: TBitmap32;
  OldDrawMode: TDrawMode;

  function GetDisplayFrame(aObject: TMetaObject): Integer;
  begin
    Result := aObject.PreviewFrameIndex;
    case aObject.TriggerEffect of
      // handle cases where this needs to be overridden
      14: Result := 1;     // pickup skill - show an actual skill icon
      15, 17: Result := 1; // locked exits and unlock buttons - show as locked
      23: Result := 0; // entrance - show as open
      31: Result := 1; // single use trap - show in unactivated form
    end;

    if UseFrame >= aObject.EndAnimationFrameIndex then
      UseFrame := aObject.EndAnimationFrameIndex - 1;
  end;
begin
  Data := fCellData;
  //fCellData.Index := aRow;
//  fCellData.Bmp := nil;
  if fCellData.Bmp <> nil then
  begin
    fCellData.Bmp.Free;
    fCellData.Bmp := nil;
  end;
  fCellData.Clear;

  if aCol = 0 then
    fCellData.Text := i2s(aRow);

  if aCol = picture_column then
    if fGraphicSet <> nil then //>= 0 then
    begin
      case fMode of
        tTerrains: SrcBmp := GraphicSet.TerrainBitmaps[aRow];// PictureMgr.TerrainBitmaps[fGraphicSet, aRow];
        tObjects:
          begin
//            fCellData.Bmp := GraphicSet.ObjectBitmaps[aRow];// PictureMgr.ObjectBitmaps[fGraphicSet, aRow];
            UseFrame := GetDisplayFrame(GraphicSet.MetaObjectCollection[aRow]);
            fCellData.Frame.Clear(0);
            GraphicSet.GetObjectFrame(aRow, UseFrame, fCellData.Frame);
            SrcBmp := fCellData.Frame;
          end;
      end;

      //fCellData.Bmp
      if fCellData.Bmp = nil then fCellData.Bmp := TBitmap32.Create;
      fCellData.Bmp.SetSize(SrcBmp.Width * 2, SrcBmp.Height * 2);
      fCellData.Bmp.Clear($FF000000 or fGraphicSet.BackgroundColor);
      OldDrawMode := SrcBmp.DrawMode;
      SrcBmp.DrawMode := dmBlend;
      SrcBmp.DrawTo(fCellData.Bmp, fCellData.Bmp.BoundsRect, SrcBmp.BoundsRect);
      SrcBmp.DrawMode := OldDrawMode;
    end;

  if aCol = 2 then
    if fGraphicSet <> nil then
    begin
      case fMode of
        tTerrains: begin
                     if GraphicSet.MetaTerrainCollection[aRow].Unknown = 1 then fCellData.Text := 'Steel';                     
                   end;
        tObjects:  begin
                     case GraphicSet.MetaObjectCollection[aRow].TriggerEffect of
                       0: fCellData.Text := 'No Effect';
                       1: fCellData.Text := 'Exit';
                       2: fCellData.Text := 'One-Way-Left Field';
                       3: fCellData.Text := 'One-Way-Right Field';
                       4: fCellData.Text := 'Triggered Trap';
                       5: fCellData.Text := 'Water';
                       6: fCellData.Text := 'Fire';
                       7: fCellData.Text := 'One-Way-Left Arrows';
                       8: fCellData.Text := 'One-Way-Right Arrows';
                       // 9, 10 shouldn't ever be used. Let's not encourage it.
                       11: fCellData.Text := 'Teleporter';
                       12: fCellData.Text := 'Receiver';
                       13: fCellData.Text := 'Pre-Placed Lemming';
                       14: fCellData.Text := 'Pickup Skill';
                       15: fCellData.Text := 'Locked Exit';
                       16: fCellData.Text := 'Sketch';
                       17: fCellData.Text := 'Unlock Button';
                       18: fCellData.Text := 'Radiation';
                       19: fCellData.Text := 'One-Way-Down Arrows';
                       20: fCellData.Text := 'Updraft';
                       21: fCellData.Text := 'Splitter';
                       22: fCellData.Text := 'Slowfreeze';
                       23: fCellData.Text := 'Entrance';
                       24: fCellData.Text := 'Triggered Decoration';
                       // Hint is also not currently supported so don't list it.
                       26: fCellData.Text := 'Anti-Splat Pad';
                       27: fCellData.Text := 'Splat Pad';
                       // 28 and 29 have been removed
                       30: fCellData.Text := 'Moving Background';
                       31: fCellData.Text := 'One-Use Trap';
                       32: fCellData.Text := 'Image Background';
                       else fCellData.Text := '(Unknown or no-longer-supported type)';
                     end;
                   end;
      end;
    end;
end;

procedure TPickTerrainForm.TerrainMatrix_PaintCell(Sender: TObject; Params: TCellParams; Data: TCellData);
{-------------------------------------------------------------------------------
  Paint terrain bitmap
-------------------------------------------------------------------------------}
var
  Bmp: TBitmap32;
  R, D: TRect;
  OldDrawMode: TDrawMode;
begin
  if fGraphicSet = nil then //< 0 then
    Exit;
  with Params do
  begin
    if (pCol = picture_column) and (pStage = pcsContents) then
    begin
      Bmp := fCellData.Bmp;
      if Bmp = nil then
        Exit;
      with Bmp do
        R := Rect(0, 0, Width - 1, Height - 1);
      D := R;
      D.Right := (D.Right * fScale) + 1;
      D.Bottom := (D.Bottom * fScale) + 1;

      OldDrawMode := Bmp.DrawMode;
      Bmp.Drawmode := dmTransparent;
      try
        Bmp.DrawTo(pCellCanvas.Handle, D, R);
      finally
        Bmp.DrawMode := OldDrawMode;
      end;
    end;
  end;
end;


procedure TPickTerrainForm.SetMode(const Value: TTOMode);
begin
  if fMode = Value then Exit;
  fMode := Value;

  AdjustMatrix;
end;

function TPickTerrainForm.GetCurrentID: Integer;
begin
  Result := TerrainMatrix.Row;
end;

procedure TPickTerrainForm.TerrainMatrixCellDblClick(Sender: TObject; aCol, aRow: Integer);
begin
  ModalResult := mrOK;
end;

procedure TPickTerrainForm.SetCurrentID(const Value: Integer);
begin
  if Between(Value, 0, TerrainMatrix.RowCount - 1) then
  begin
    TerrainMatrix.Row := Value;
    ScrollBar1.Position := Value;
  end;
end;

procedure TPickTerrainForm.cbStyleChange(Sender: TObject);
begin
  GraphicSet := TBaseGraphicSet(cbStyle.Items.Objects[cbStyle.ItemIndex]);
  case fMode of
    tTerrains: BitBtn1.Enabled := GraphicSet.MetaTerrainCollection.Count > 0;
    tObjects: BitBtn1.Enabled := GraphicSet.MetaObjectCollection.Count > 0;
    else BitBtn1.Enabled := false;
  end;
end;

procedure TPickTerrainForm.FormResize(Sender: TObject);
begin
  // Set button position
  // 183 - 126 Cancel
  // 214 - 183 Ok
  BitBtn1.Left := (Width div 2) + 31;
  BitBtn2.Left := (Width div 2) - 57;
end;

procedure TPickTerrainForm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  NewWidth := Width; // don't allow horizontal resize, vertical only
end;

procedure TPickTerrainForm.ScrollBar1Change(Sender: TObject);
begin
  TerrainMatrix.Row := ScrollBar1.Position;
  if Visible then // exception gets raised otherwise
    TerrainMatrix.SetFocus;
end;

procedure TPickTerrainForm.TerrainMatrixSelectCell(Sender: TObject; aCol,
  aRow: Integer);
begin
  ScrollBar1.Position := TerrainMatrix.Row;
end;

end.
//
