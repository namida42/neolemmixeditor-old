unit FRepConv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UMasterForms, LemReplay;

type
  TRepConvForm = class(TCustomMasterForm)
    procedure FormCreate(Sender: TObject);
  private
    procedure Convert(const F: string);
  public
    procedure DropIt(Sender: TObject; const DroppedFiles: TStrings);

  end;

var
  RepConvForm: TRepConvForm;

implementation

{$R *.dfm}

(*

19, rrStartIncrease, 1
19, rrStop, 85
19, rrStartIncrease, 85
19, rrStop, 86
55, raSkillAssignment, baClimbing, 0
58, rrStartDecrease, 86
58, rrStop, 1
66, raSkillAssignment, baClimbing, 1
77, raSkillAssignment, baExplosion, 0
85, raSkillAssignment, baFloating, 0
96, raSkillAssignment, baFloating, 1
118, raSkillAssignment, baFloating, 2
184, raSkillAssignment, baBuilding, 1
202, raSkillAssignment, baBuilding, 1
219, raSkillAssignment, baBashing, 1
226, raSkillAssignment, baBuilding, 1
243, raSkillAssignment, baBashing, 1
249, raSkillAssignment, baBuilding, 1
412, rrStartIncrease, 1
412, rrStop, 99
518, raSkillAssignment, baBlocking, 1

*)

{ TRepConvForm }

procedure TRepConvForm.Convert(const F: string);
var
  L: TStringList;
  i: integer;
  s: string;
begin
  L:= TStringList.create;
  try
    l.loadfromfile(f);
    for i := 0 to l.count-1 do
    begin
      s:=l[i];
    end;
  finally
    l.free;
  end;

end;

procedure TRepConvForm.DropIt(Sender: TObject; const DroppedFiles: TStrings);
var
  i: Integer;
begin
  for i := 0 to DroppedFiles.Count - 1 do
  begin
    Convert(DroppedFiles[i]);
  end;
end;

procedure TRepConvForm.FormCreate(Sender: TObject);
begin
  AcceptDraggedFiles := True;
  OnDropFiles := DropIt;
end;

end.

