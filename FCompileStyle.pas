{$include lem_directives.inc}

unit FCompileStyle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LemCore, FCompile, ComCtrls, StdCtrls, VirtualTrees,
  kernel_resource, USplit, USplitPanels;

type
  TMyData = class
  private
    NodeType    : (ntNone, ntStyle, ntGraph);
    fGraph      : TBaseGraphicSet;
    fStyle      : TBaseLemmingStyle;
    fIsCompiled : Integer;
  public
//    property
    property Style: TBaseLemmingStyle read fStyle;
    property Graph: TBaseGraphicSet read fGraph;
    property IsCompiled: Integer read fIsCompiled;
  end;

type
  TFormCompileStyle = class(TForm)
    MainPanel: TSplitPanel;
    VTree: TVirtualStringTree;
    Button1: TButton;
    Button2: TButton;
    procedure Form_Create(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure VTree_GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure VTree_FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure VTree_GetImageIndex(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
      var Ghosted: Boolean; var ImageIndex: Integer);
    procedure VTree_FocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
  private
    cf: TFormCompile;

    SelNode      : PVirtualNode;
    SelectedStyle: TBaseLemmingStyle;
    SelectedGraph: TBaseGraphicSet;
//    CurrentProgressForm: TFormCompile;
     function GetMyNodeData(aNode: PVirtualNode): TMyData;
     function UpdateNodeCompileInfo(aNode: PVirtualNode): Integer;
   public
    procedure DoCompile(Sty: TBaseLemmingStyle; aGraph: TBaseGraphicSet = nil);
  end;
//ftest
implementation

{$R *.dfm}

type
  PDataRec = ^TDataRec;
  TDataRec = record
    Obj: TObject;
  end;

{ TFormCompileStyle }

procedure TFormCompileStyle.DoCompile(Sty: TBaseLemmingStyle; aGraph: TBaseGraphicSet = nil);
var
  gra:TBaseGraphicSet;//uzip lemstrings
  //cf: TFormCompile;
  compiler: TArchiveCompiler;
  var
    i: Integer;
//var
  //D: TMyData;
begin

  compiler := TArchiveCompiler.Create;
  try

  if cf=nil then
    cf:=tFormCompile.create(self);
  cf.link.Manager := compiler.mgr;
  cf.show;

  begin
    //OnProgress:=cf.ArchiveCompiler_Progress;
//    Sty := stylemgr.StyleList[STYLE_LEMMINI];
//    Gra := Sty.GraphicSetList[3];
    if aGraph = nil then
    for i := 0 to Sty.GraphicSetList.Count - 1 do
    begin
      Gra := Sty.GraphicSetList[i];
      Compiler.CompileArchive(sty, gra);
      UpdateNodeCompileInfo(SelNode);
      VTree.Invalidate;
    end
    else begin
      Compiler.CompileArchive(sty, agraph);
      UpdateNodeCompileInfo(SelNode.Parent);
      VTree.Invalidate;
      //Invalidate;

    end;
//    cf.link
  end;

  finally
    compiler.free;
  end;



end;

procedure TFormCompileStyle.Form_Create(Sender: TObject);
{-------------------------------------------------------------------------------
  Fill the Treeview with all the descriptions of styles and their graphicsets.
  The objects are added to the nodes.
-------------------------------------------------------------------------------}
var
  i, j: Integer;
  S: TBaseLemmingStyle;
  G: TBaseGraphicSet;
//  It: TTreeNode;
  CompileCount: Integer;

  SData, GData: TMyData;
  SNode, GNode: PVirtualNode;
begin

  VTree.Images := MxImages10;
  VTree.NodeDataSize := SizeOf(TDataRec);

//  VTree.TreeOptions.MiscOptions := VTree.TreeOptions.MiscOptions + [toCheckSupport];

  with StyleMgr do
    for i := 0 to StyleList.Count - 1 do
    begin
      S := StyleList[i];
      SData := TMyData.Create;
      SData.fStyle := S;
      SData.NodeType := ntStyle;

      CompileCount := 0;

      SNode := VTree.AddChild(nil, SData);
      for j := 0 to S.GraphicSetList.Count - 1 do
      begin
        G := S.GraphicSetList[j];
        GData := TMyData.Create;
        GData.fGraph := G;
        GData.NodeType := ntGraph;

        GNode := VTree.AddChild(SNode, GData);
        VTree.CheckType[GNode] := ctCheckBox;

        if G.GraphicSetArchive <> '' then
          if FileExists(G.FullArchiveName) then
          begin
            GData.fIsCompiled := 1;
            Inc(CompileCount);
          end;
//        VTree.CheckState[GNode] := csCheckedNormal;


        //Tree.Items.AddChildObject(It, G.GraphicSetName, G);
      end;

      if CompileCount = S.GraphicSetList.Count then
        SData.fIsCompiled := 1;

    end;

end;


procedure TFormCompileStyle.Button1Click(Sender: TObject);
//var
  //s:tbaselemmingstyle;
//  g:tbasegraphicset;

begin
  if MessageDlg('This can cost a few minutes and cannot be cancelled. ' + Chr(13) +
                'Are you sure you want to compile?',
    mtConfirmation, [mbYes, mbNo], 0) <> mrYes then exit;

//  GetTreeViewResult(s,g);
  case button1.Tag of
    1: docompile(selectedstyle);
    2:
      begin
        docompile(selectedstyle,SelectedGraph);
        UpdateNodeCompileInfo(SelNode);
      end;
  end;

//            GData.fIsCompiled := 1;

end;

procedure TFormCompileStyle.Button2Click(Sender: TObject);
begin
  Close;
end;

function TFormCompileStyle.GetMyNodeData(aNode: PVirtualNode): TMyData;
var
  D: PDataRec;
begin
  Result := nil;
  D := VTree.GetNodeData(aNode);
  if Assigned(D) and (D^.Obj is TMyData) then
    Result := TMyData(D^.Obj);
end;

procedure TFormCompileStyle.VTree_GetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
var
  D: TMyData;
begin
  D := GetMyNodeData(Node);
  if D = nil then
    Exit;
  case D.NodeType of
    ntStyle: CellText := D.Style.StyleDescription; // + ' (' + D.Style.StyleName + ')'
    ntGraph: CellText := D.Graph.GraphicSetName;
  else
    CellText := '';
  end;

end;

procedure TFormCompileStyle.VTree_FreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
   GetMyNodeData(Node).Free;
end;

procedure TFormCompileStyle.VTree_GetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: Integer);
var
  D: TMyData;
begin
  D := GetMyNodeData(Node);
  if D = nil then
    Exit;
//  if d.style=nil then exit;
//  if not d.isgraph then exit;
  if D.IsCompiled = 1 then
    ImageIndex := Ord(TMX_led_flat_green10)
  else //ghosted := true;
    ImageIndex := Ord(TMX_led_flat_off10)

end;

procedure TFormCompileStyle.VTree_FocusChanged(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
var
  D: TMyData;
begin
  D := GetMyNodeData(Node); //virtualtrees
  if D = nil then
    Exit;
{  if D.Graph <> nil then
    CellText := D.Graph.GraphicSetName
  else if D.Style <> nil then
    CellText := D.Style.StyleName
  else
    CellText := ''; }
  selnode:=node;

  if node = nil then
  begin
    button1.caption := 'compile';
    button1.tag := 0;
    button1.enabled := false;
    SelectedStyle:=nil;
    SelectedGraph:=nil;
  end
  else if D.Graph <> nil then
  begin
    button1.Caption := 'compile graphic set';
    button1.tag := 2;
    button1.enabled := true;
    SelectedGraph:=D.Graph;//TBaseGraphicSet(TObject(Tree.Selected.Data));
    SelectedStyle := SelectedGraph.OwnerStyle;
  end
  else if D.Style <> nil then
  begin
    button1.Caption := 'compile all graphicsets of this style';
    button1.tag := 1;
    button1.enabled := true;
    SelectedStyle := D.Style;
  end



end;

function TFormCompileStyle.UpdateNodeCompileInfo(aNode: PVirtualNode): Integer;
var
  D: TMyData;
//  i: Integer;
  N: PVirtualNode;
  All: Boolean;
begin
  Result := 0;

  D := GetMyNodeData(aNode);
  if D = nil then
    Exit;

  case D.NodeType of
    ntStyle:
      begin
        All := True;
        N := VTree.GetFirstChild(aNode);
        while N <> nil do
        begin
          All := All and (UpdateNodeCompileInfo(N) > 0);
          N := VTree.GetNextSibling(N);
        end;
        if All then
          D.fIsCompiled := 1
        else
          D.fIsCompiled := 0;
      end;
    ntGraph:
      begin
        if (D.Graph.GraphicSetArchive <> '')
        and (FileExists(D.Graph.FullArchiveName)) then
          D.fIsCompiled := 1
        else
          D.fIsCompiled := 0;

        Result := D.fIsCompiled;
      end;
  end;
end;

end.

(*
    function GetTreeViewResult(var aStyle: TBaseLemmingStyle; var aGraph: TBaseGraphicSet): Boolean;

function TFormCompileStyle.GetTreeViewResult(var aStyle: TBaseLemmingStyle;
  var aGraph: TBaseGraphicSet): Boolean;
{-------------------------------------------------------------------------------
  Get the style and graphicset from the selected treenode
-------------------------------------------------------------------------------}
var
  O: TObject;
  S: TBaseLemmingStyle;
  G: TBaseGraphicSet;
  It: TTreeNode;
begin

  G := nil;
  S := nil;

  Result := False;
  It := Tree.Selected;
  if It = nil then
    Exit;

  O := TObject(It.Data);
  if O is TBaseGraphicSet then
    G := TBaseGraphicSet(O)
  else
    Exit;

  It := It.Parent;
  if It = nil then
    Exit;

  O := TObject(It.Data);
  if O is TBaseLemmingStyle then
    S := TBaseLemmingStyle(O)
  else
    Exit;

  aStyle := S;
  aGraph := G;
  Result := True;
    //  aGraph := Tree.S

end;

procedure TFormCompileStyle.TreeChange(Sender: TObject; Node: TTreeNode);
begin
  if tree.selected = nil then
  begin
    button1.caption := 'compile';
    button1.tag := 0;
    button1.enabled := false;
    SelectedStyle:=nil;
    SelectedGraph:=nil;
  end
  else if (Tree.Selected <> nil) and (TObject(Tree.Selected.Data) is TBaseGraphicSet) then
  begin
    button1.Caption := 'compile graphic set';
    button1.tag := 2;
    button1.enabled := true;
    SelectedGraph:=TBaseGraphicSet(TObject(Tree.Selected.Data));
    SelectedStyle := SelectedGraph.OwnerStyle;
  end
  else
  if (Tree.Selected <> nil) and (TObject(Tree.Selected.Data) is TBaseLemmingStyle) then
  begin
    button1.Caption := 'compile all graphicsets of this style';
    button1.tag := 1;
    button1.enabled := true;
    SelectedStyle := TBaseLemmingStyle(TObject(Tree.Selected.Data));
  end
end;

