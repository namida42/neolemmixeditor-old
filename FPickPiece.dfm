object Form1: TForm1
  Left = 239
  Top = 173
  Width = 545
  Height = 392
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  Position = poMainFormCenter
  object SuperMatrix1: TSuperMatrix
    Left = 8
    Top = 8
    Width = 524
    Height = 353
    BorderStyle = bsSingle
    ParentColor = False
    ScrollBars = ssNone
    Options.AutoSizeOptions = [aoAutoWidth]
    Options.UseOptions = []
    Options.PaintOptions = [poHorzLines, poVertLines, poHeaderHorzLines, poHeaderVertLines]
    Options.SizingOptions = [soColSizing, soRowSizing, soCellSizing]
    Options.NavigateOptions = []
    Options.EditOptions = []
    Options.SearchOptions = []
    Options.MiscOptions = []
    CellOffsetX = 0
    CellOffsetY = 0
    ColCount = 8
    CustomPainting = False
    CustomPaintStages = []
    DefaultRowHeight = 64
    RowCount = 8
    SelectMode = smCellSelect
    VisibleColsRequested = 0
    VisibleRowsRequested = 0
  end
end
