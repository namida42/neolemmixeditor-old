Welcome to the NeoLemmix Editor, a lemmings level editor for NeoLemmix. Please note that
as of V1.47n, NeoLemmix Editor does not support editing levels for any engine other than
NeoLemmix (you can still import Lemmix, Lemmini and SuperLemmini levels, but not export).

REMINDER: Please ALWAYS keep a backup and regular saves of any level you are working on!

Included player version: V10.13.16

V10.13.13
---------
> Fixed bug: Editor misbehaves when a level is loaded that contains an unrecognized piece. It is now replaced with a placeholder piece.
> Added protections against (and detection in the validation report) placing background image objects in the level.
> Several graphic sets have been updated.
> VGASPEC support has been fully stripped.
> Window order editing has been removed, as NeoLemmix no longer supports it. (Use multiple overlapping entrances instead. Existing levels will be auto-converted to this system when opened.)
> Fencers can now be added to a skillset.
> Screen start position is now based on center of screen, not topleft edge.

V10.12.12-C
-----------
> Fixed the memory leak when scrolling through the piece selection window.

V10.12.12-B
-----------
> Fixed the bug where an error message prevented changing the background
> Fixed the bug where if a flipped or inverted object was copy-pasted, the new object would display in both forms

V10.12.12
---------
> Many improvements to the piece selection menu, including it can now be vertically resized
> Fixed the bug where object rotation is lost when copy/pasting

V10.12.11
---------
> Dropped leading zeros from version number
> Included many more graphic sets by default

V10.010.010
-----------
> Switched to new versioning scheme
> Added "Move to Front" and "Move to Back" options (on the menu bar under "Selection")
> Removed many no-longer-relevant options from the menu bar
> Allows specifying an "alternate" name for a graphic set, which will be recognized when loading but not used when saving
> If graphic set cannot be found when loading level, user will be asked to pick one rather than loading failing
> Fixed the bug where Insert Terrain / Object cannot be selected if the primary graphic set has no pieces of the respective type

V1.48-B
-------
> Graphic set can now be selected directly in the piece selection window
> Removed the "Classic Delete" option; it is now treated as always on
> Editor behaves correctly in case of a missing settings INI file
> Added an "Update All Styles" button (in Tools > Download Graphic Sets)

V1.48
-----
> Supports multiple backgrounds in a tileset
> Fixed a bug where graphic sets with no objects are always detected as VGASPECs
> Fixed a bug that caused mixed-graphic-set levels to sometimes fail to open
> Removed several obsolete options
> Removed the now-broken test mode settings
> Some improvements to which frame of an object shows in the piece selection dialog
> Fixed a bug where if a new piece is selected while mixing graphic sets, the piece ID will change
  but the graphic set will not
          
V1.47n
------
> Supports new V1.47n features, removes no-longer-relevant features
> Removed support for editing Lemmix, Lemmini and SuperLemmini
> Fixed some bugs with window order editing
> Fixed the bug where "only on terrain" objects only display on one-way-capable terrain
> Added the ability to download graphic sets directly into the editor

NOTE: Editor V1.47n's playtest feature will not work properly with player versions older than V1.47n.
          
V1.43n-C
--------
> Removed the panels at the bottom of the main window
> Various improvements to window ordering
> Fixes the following bugs:
  - Input in text boxes is discarded unless another text box is selected before saving
  - Screen jumps to screen start position when undoing / redoing

					
V1.43n-B
--------
> Added Undo and Redo functionality (via "Edit" on the menu bar, or Ctrl+Z / Ctrl+Y respectively)
> Added an option for whether or not to set No Overwrite by default on most objects.
> Removed the hotkey "H" to toggle the highlighting of selected pieces.
> Added a horizontal scroll bar in the piece selection window.
> Fixed the following bugs:
  - Right-click popup menu only works when there is no piece underneath the cursor
	- Locked pieces cannot be unlocked
	- Locked pieces can be deleted
	- Some hotkeys stop working after resizing the split panels at the bottom
	- Some S / L value setting panels may be visible when they shouldn't when sub-windows are docked
	- One-Way arrows are rendered on steel piecel (semi-fixed; it still doesn't take into account
	  manually placed steel areas)
	
NOTE: Support for DAT level packs is being phased out. It is recommended that you use an external
      tool (such as the DAT Packer) rather than the editor itself to handle these. Anything that
			breaks in regard to DAT level packs will not be fixed.

V1.43n
------
> Level List window has been remade from scratch. One advantage is that scrolling no longer interferes
  with selection.
> Added a "Lock Piece" selection. Locked pieces cannot be modified at all; they can still be copied.
  Lock data is preserved when saving / loading a level in either NeoLemmix or SuperLemmini format.
> Fixed the bug where one-way arrows get drawn onto objects as well as suitable terrain.

V1.42n-C
--------
> Adds a scroll bar in the Level List window.
> Fixed the following bugs:
  - The scroll position resets to the top-left corner when the level is resized
  - The level is not marked as having been changed after using the Fix Issues options in Level Validation

V1.42n-B
--------
> Restores the "Flip" checkbox for objects.

V1.42n
------
> Added an "Import Layout" option (in the Edit menu); this imports the layout of another level without
  overwriting the stats, skillset and gimmicks of the current level
> Pieces can be cloned by click-dragging, then holding Ctrl while dropping them
> The piece properties window has been tidied up
> Extra options have been added to the piece properties window to simplify settings that are
  handled via the S and L values
> Added an option of whether to set "One-Way" flag by default on new terrain pieces.
> Added options to zoom based on current center of display rather than mouse position, and
  whether or not to override this with piece position if pieces are selected
> Updated the easy music selection to conform to how music is now implemented in NeoLemmix.
> When selecting pieces, the object type will be displayed; steel terrain will also be flagged as such
> It is no longer possible to have the editor running without any level open
> Fixed the following bugs:
  - Some graphic sets - particularly those converted from Lemmini - display trigger areas in the
    wrong position
  - Popup dialogs (such as that for selecting pieces) appear in unusual places when the editor is
    used on a PC with a multi-display setup

V1.40n
------
> Removed the SYSTEM.DAT editor and VGASPEC generator as these have been obsolete for a long time.
> For NeoLemmix and SuperLemmini, the default level size has been changed to a single screen (ie:
  320x160 for NeoLemmix, or 640x320 for SuperLemmini).
> For NeoLemmix and SuperLemmini, the default time limit is now unlimited, rather than 5 minutes.
> Fixed the bug where it was possible to make a level smaller than one screen in size.

V1.39n-C
--------
> Fixes issues with the "Fix All" button in level validation that could result in errors or level
  corruption.

V1.39n-B
--------
> When adding a terrain piece or object, the editor immediately asks which piece to add. Those
  who prefer the old behaviour can revert to it via File > Settings.
> Added a "Save" option (rather than only "Save As")
> Changed the hotkeys for piece addition to Alt+[key] rather than Ctrl+Shift+[key] (Ctrl+[key]
  has conflicts, and Shift+[key] or just [key] can have problems when editing level titles etc)
> NeoLemmix levels now have Zombies and Ghosts enabled by default (since if the level doesn't
  contain any zombie / ghost lemmings, it makes no difference)
> Fixed the bug where most things in the Level Properties window would not trigger the "level
  has changed, do you want to save?" prompt.
> Fixed the bug where some vanilla Lemmix levels (particularly those in the Dirt style) would
  appear to not load properly if loaded directly into NeoLemmix style.
> Fixed the bug where ordering in the style.ini file was not applied to VGASPEC graphics.
> Fixed the bug where OK was initially clickable in the select style dialog, even if nothing
  was selected yet.

V1.39n-A
--------
> Music file name can now be edited in NeoLemmix levels.
> Fixed the bug where a level would be considered modified after entering playtest mode.

V1.38n-B
--------
> Adds a confirmation when quitting / closing a level, if the level has not been saved. Note: Chances
  are this doesn't work 100% of the time, so firstly - don't *rely* on it, and secondly - let me know
  if you encounter any changes that don't trigger this!
> Safety checks against old-format graphic sets in NeoLemmix set folders have been removed; by this
  point you should've gotten rid of them anyway! (Note: Old format VGASPECs remain compatible; it's
  only regular graphic sets that MUST be in up to date format.) The reason for this is that the editor
  boots up MUCH quicker when it's not performing these checks.
> Percentage is no longer displayed when editing NeoLemmix levels
> Fixed the bug where, when multiple terrain pieces are selected, the Rotate checkbox may be grayed
  when it should be simply checked

V1.38n-A
--------
> Adds support for rotating terrain pieces

V1.36n-B
--------
> Adds a scrollbar to the terrain / object selection menu

V1.36n-A
--------
> Fixes more bugs with the Validate Level feature

V1.35n-E
--------
> Fixes a bug where some older levels may not load, depending on the exact combinations of previous
  formats they've been saved in, graphic sets they use and editor versions they've been edited in.
> The "Ghosts" and "Ghost on Death" gimmicks are now always selectable in the Gimmicks window without
  having to reveal the hidden gimmicks.

V1.35n-D
--------
> Fixes the bugs with the Validate Level option
> Fixes the bugs with setting the Bait & Switch levels
> Fixes the bug where Oddtable rank / level settings didn't become enabled when Oddtable was
  checked / disabled when Oddtable was unchecked

V1.35n-C
--------
> Fixes a bug where most VGASPECs were unusable in NeoLemmix levels
> The name of the Disarmer skill no longer displays as its old name "Mechanic"

V1.35n-B
--------
> Will no longer load older-format NeoLemmix graphic sets; this is due to repeated cases of issues
  caused by the use of them. (They're still supported for traditional Lemmix levels obviously.)
> Has an updated version of the Martian graphic set with a fix to the exit's trigger area.

V1.35n-A
--------
> Completely redesigned the gimmicks menu.
> Supports some new additions in player V1.34n-B onwards, such as clock gimmick and bait-and-switch
  settings being seperated from the main settings.
> Preserves (and sets) a level ID, used as of player V1.35n to identify whether a replay is for the
  correct level even in test mode, single levels, or if the level is repositioned.
> Added a menu to more easily select music tracks. This is only applicable to NeoCustLemmix; it cannot
  currently be customized for Flexi-based or other packs.
> Fixed the bug where new-format level titles wouldn't display properly in the DAT level pack manager.
> Includes the remaining Lemmings Plus IV graphic sets (Space and Wasteland).

Please note: NeoLemmix levels created with this version (including when using playtest mode) require a
             recent version of NeoLemmix.
             V1.33n or higher is required to play them at all.
             V1.34n-B or higher is required for secret levels with specified redirects, or levels using
                the Clock Gimmick or Bait-and-Switch.
             V1.35n or higher is required for levels using Invert Fall Fatality, or any gimmick listed
                after Bait-and-Switch in the list.




---EDITOR HELP---

-=Setup for Lemmini / SuperLemmini=-
By default the NeoLemmix Editor contains all the metadata needed for Lemmini and SuperLemmini
editing, but does not come with an actual copy of the styles. This is due to the licence of
these apps.

To add them, you must copy the contents of the respective game's "styles" folder into the
editor's "styles/lemmini" and "styles/superlemmini" folders respectively.

-=Adding extra graphic sets=-
To add a graphic set, simply copy its data files (or folder, in the case of (Super)Lemmini)
into the respective style folder. It's that simple!

Optionally, you may want to modify the styles.ini file to add info about the graphic set.
This is not required, but can make things tidier. Take a look at the existing style.ini
files to work out how it's done.

-=Playtest mode=-
Keeping a completely built-in playtest mode would have been far too much hassle, so instead,
NeoLemmix Editor can launch an external engine to test the levels. The EXE to use can be
specified in the relevant style's section in the INI. NeoLemmix Editor supports integration
with the following engines for testing:

  * CustLemmix       : Place CustLemmix.exe in the same folder as the editor, and specify
                       only the filename (CustLemmix.exe); this also extends to CustLemmixOrig
                       and CustLemmixOhno. NeoLemmix Editor will automatically direct
                       CustLemmix to the correct style files; you do not need to provide a
                       second copy of them for ones that CustLemmix doesn't have built in.

  * NeoCustLemmix    : Integration works the same way as standard CustLemmix.

  * SuperLemmini     : You don't need to put SuperLemmini in the same folder as NeoLemmix
                       Editor; but you must specify the full path of the SuperLemmini.jar
                       file even if it is in the same folder as the editor. You also must
                       have a copy of the style in SuperLemmini's folders as well as in
                       NeoLemmix Editor's; unlike with (Neo)CustLemmix, the editor cannot
                       tell it where to find the files. You also need to specify the path
                       to your java EXE, this one is in the configuration INI.

There is no integration with standard Lemmini, though you can use SuperLemmini for testing
standard Lemmini levels if you want (though it may not be perfectly accurate, so you should
always test your levels in standard Lemmini as well before releasing them).


-=Loading levels=-
Please note that attempting to load one style level directly into another style often will
not work properly - this is especially true of trying to load SuperLemmini levels into any
other format, or trying to load non-extended-format NeoLemmix levels into a Lemmini format.
You should instead load it into its correct style, then change the style in the Level Properties
editor. In addition to this, you should also immediately save the level then reload it.


-=New Levels=-
Make sure you always create a new level via the File menu (or by opening a blank level), and not
by simply choosing a style in Level Properties window while no level is loaded. This latter method
might work fine, but it is untested.