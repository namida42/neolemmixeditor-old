{-------------------------------------------------------------------------------
  This unit contains a common ancestor form for level editing.
  It contains a link to the editor and a direct access pointer to the
  editor to avoid multiple function calls of TLevelEditorLink.
-------------------------------------------------------------------------------}

{$include lem_directives.inc}

unit FBasicLevelEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UMasterForms,
  UMisc, LemApp,
  LemCore,
  LemEdit;

type
  TBasicLevelEditorForm = class(TCustomMasterForm, ISetting)
  private
    fSettingLoaded: Boolean;
    procedure SetEditor(const Value: TLevelEditor);
    procedure CMDeactivate(var Message: TCMDeactivate); message CM_DEACTIVATE;
  protected
    fEditorLink: TLevelEditorLink;
    fEditor: TLevelEditor; // direct access to editor
    procedure DoCreate; override;
    procedure DoDestroy; override;
    procedure DoAfterSetEditor(Value: TLevelEditor); virtual;
    function GetSection: string;
    procedure DoShow; override;
  public
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    property Editor: TLevelEditor read fEditor write SetEditor;
    property SettingLoaded: Boolean read fSettingLoaded;
  end;

implementation

{$R *.dfm}

{ TBasicLevelEditorForm }

procedure TBasicLevelEditorForm.AfterConstruction;
begin
  inherited AfterConstruction;
//  Exit;
  if not fSettingLoaded then
  begin
    fSettingLoaded := true;
    try
//      showmessage('1');
exit;
      if App <> nil then
        App.ReadForm(Self); // problemen met mainform???
  //    showmessage('2');
    except
      ShowMessage('error loading settings ' + classname);
    end;
  end;
end;

procedure TBasicLevelEditorForm.BeforeDestruction;
begin
  if App <> nil then
    App.SaveForm(Self);
  inherited BeforeDestruction;
end;

procedure TBasicLevelEditorForm.CMDeactivate(var Message: TCMDeactivate);
begin
  inherited;
  //Height := 0;
//  Log(['deact']);
end;

procedure TBasicLevelEditorForm.DoAfterSetEditor(Value: TLevelEditor);
begin
  //
end;

procedure TBasicLevelEditorForm.DoCreate;
begin
  fEditorLink := TLevelEditorLink.Create(nil, Self);
  inherited DoCreate;
end;

procedure TBasicLevelEditorForm.DoDestroy;
begin
  FreeAndNil(fEditorLink);
  inherited DoDestroy;
end;

procedure TBasicLevelEditorForm.DoShow;
begin
//  Log([classname]);
  (*
  if not fSettingLoaded then
  begin
    fSettingLoaded := true;
    try
      if App <> nil then
        App.ReadForm(Self);
    except
      ShowMessage('error loading settings ' + classname);
    end;
  end; *)
  inherited;
end;

function TBasicLevelEditorForm.GetSection: string;
begin
  Result := CutLeft(ClassName, 1);
//  windlg(result);
end;

procedure TBasicLevelEditorForm.SetEditor(const Value: TLevelEditor);
begin
  fEditor := Value;
  fEditorLink.LevelEditor := Value;
  DoAfterSetEditor(Value);
end;

end.

