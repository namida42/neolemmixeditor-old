object FormCompile: TFormCompile
  Left = 259
  Top = 224
  Width = 391
  Height = 372
  Caption = 'Compiling...'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  Position = poMainFormCenter
  object Memo: TMemo
    Left = 0
    Top = 0
    Width = 367
    Height = 281
    Align = alTop
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object ProgressBar1: TProgressBar
    Left = 0
    Top = 320
    Width = 367
    Height = 9
    Align = alBottom
    TabOrder = 1
  end
  object Button1: TButton
    Left = 288
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Close'
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Abort'
    ModalResult = 3
    TabOrder = 3
    Visible = False
    OnClick = Button2Click
  end
end
