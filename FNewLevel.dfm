object NewLevelForm: TNewLevelForm
  Left = 1303
  Top = 309
  Width = 294
  Height = 324
  Caption = 'New level'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblChooseGS: TLabel
    Left = 16
    Top = 8
    Width = 94
    Height = 13
    Caption = 'Select a graphic set'
  end
  object lbStyles: TListBox
    Left = 8
    Top = 24
    Width = 257
    Height = 217
    ItemHeight = 13
    TabOrder = 0
    OnClick = lbStylesClick
    OnDblClick = lbStylesDblClick
  end
  object btnCancel: TButton
    Left = 184
    Top = 248
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object btnOK: TButton
    Left = 104
    Top = 248
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
  end
end
