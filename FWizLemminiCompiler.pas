{$include lem_directives.inc}

unit FWizLemminiCompiler;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
UFiles,  Dialogs, Buttons, StdCtrls, UMasterControls, CheckLst;

type
  TWizardLemminiCompiler = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    EditEx1: TEditEx;
    BitBtn1: TBitBtn;
    CheckListBox1: TCheckListBox;
    Button4: TButton;
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TWizardLemminiCompiler.Button4Click(Sender: TObject);
var
  D: string;

begin
  with CheckListBox1 do
  begin
    Items.clear;
    D := IncludeTrailingBackslash(EditEx1.Text);
    CreateFileList(Items, D + '*.*', faDirectory, False);
    //  for i
    //items.add
  end;
end;

end.

