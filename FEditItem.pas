{$include lem_directives.inc}

unit FEditItem;

interface

// Proper with is 364
// Proper position of Extra Panel is 200, 152

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  math, umisc,  Dialogs, LemEdit, FBasicLevelEditor, StdCtrls, UMasterControls,
fpickterrain,  LemCore, ComCtrls, ExtCtrls, GR32_Image, Buttons, LemDosStyles,
LemLemminiStyles;

const
  // extra panel is moved during runtime, and I have this form wider in
  // design time to access the hidden panels
  RUNTIME_WIDTH = 364;
  EXTRA_PANEL_LEFT = 200;
  EXTRA_PANEL_TOP = 176;

type
  TTempData = record
    SelCount: Integer;
    SelTypes: TLevelItemTypes; // welke items zitten er in de selectie
    HasObjects: Boolean;
    HasTerrain: Boolean;
    HasSteel: Boolean;

    FirstBaseItem: TBaseLevelItem; // eerste

    // flags
    FirstTerrainErase: Boolean;
    FirstTerrainInvert: Boolean;
    FirstTerrainNoOverwrite: Boolean;
    FirstTerrainInvisible: Boolean;
    FirstTerrainFake: Boolean;
    FirstTerrainFlip: Boolean;
    FirstTerrainNoOneWay: Boolean;
    FirstTerrainRotate: Boolean;

    FirstObjectOnlyTerrain: Boolean;
    FirstObjectInvert: Boolean;
    FirstObjectNoOverwrite: Boolean;
    FirstObjectFaceLeft: Boolean;
    FirstObjectFake: Boolean;
    FirstObjectInvisible: Boolean;
    FirstObjectFlip: Boolean;
    FirstObjectRotate: Boolean;

    SameTerrainErase: Boolean;
    SameTerrainInvert: Boolean;
    SameTerrainNoOverwrite: Boolean;
    SameTerrainInvisible: Boolean;
    SameTerrainFake: Boolean;
    SameTerrainFlip: Boolean;
    SameTerrainNoOneWay: Boolean;
    SameTerrainRotate: Boolean;

    SameObjectOnlyTerrain: Boolean;
    SameObjectInvert: Boolean;
    SameObjectNoOverwrite: Boolean;
    SameObjectFaceLeft: Boolean;
    SameObjectFake: Boolean;
    SameObjectInvisible: Boolean;
    SameObjectFlip: Boolean;
    SameObjectRotate: Boolean;

    FirstLocked: Boolean;
    SameLocked: Boolean;

    // positions
    FirstX: Integer;
    FirstY: Integer;
    FirstW: Integer;
    FirstH: Integer;

    SameX: Boolean;
    SameY: Boolean;
    SameW: Boolean;
    SameH: Boolean;

    // id's
    FirstTerrainID: Integer;
    FirstObjectID: Integer;
    FirstObjectSV: Integer;
    FirstObjectLV: Integer;
    FirstSteelOption: Integer;

    SameTerrainID: Boolean;
    SameObjectID: Boolean;
    SameObjectSV: Boolean;
    SameObjectLV: Boolean;
    SameSteelOption: Boolean;

    // index
    TheIndex: Integer;

    // helpers
    //GrSet: Integer;
    TerGrSet: TBaseGraphicSet;
    ObjGrSet: TBaseGraphicSet;

  end;

type
  TEditItemForm = class(TBasicLevelEditorForm)
    EditX: TEditEx;
    EditY: TEditEx;
    EditW: TEditEx;
    EditH: TEditEx;
    EditTID: TEditEx;
    EditOID: TEditEx;
    EditObjSV: TEditEx;
    EditObjLV: TEditEx;
    EditIx: TEditEx;

    StatusBar: TStatusBar;
    LabelX: TLabel;
    LabelY: TLabel;
    LabelW: TLabel;
    LabelH: TLabel;
    LabelTerrain: TLabel;
    LabelIndex: TLabel;
    LabelObject: TLabel;
    LabelSV: TLabel;
    LabelLV: TLabel;
    CbTerrainErase: TCheckBox;
    CbTerrainUpside: TCheckBox;
    CbTerrainNoOverwrite: TCheckBox;
    CbTerrainRotate: TCheckBox;
    CbTerrainFlip: TCheckBox;
    CbTerrainNoOneWay: TCheckBox;
    CBObjectOnlyOnTerrain: TCheckBox;
    CbObjectUpside: TCheckBox;
    CbObjectFlip: TCheckBox;
    CbObjectNoOverwrite: TCheckBox;
    CbObjectFaceLeft: TCheckBox;
    CbObjectFake: TCheckBox;
    CbObjectInvisible: TCheckBox;
    SteelOption: TComboBox;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    ImgTerrain: TImage32;
    ImgObject: TImage32;
    BtnPickTerrain: TSpeedButton;
    BtnPickObject: TSpeedButton;
    Label1: TLabel;
    Bevel4: TBevel;
    panSLNone: TPanel;
    Label2: TLabel;
    panSLTeleport: TPanel;
    btnTeleportLink: TButton;
    panSLLemming: TPanel;
    Label5: TLabel;
    cbPAClimber: TCheckBox;
    cbPASwimmer: TCheckBox;
    cbPAFloater: TCheckBox;
    cbPAGlider: TCheckBox;
    cbPADisarmer: TCheckBox;
    cbPABlocker: TCheckBox;
    cbPAZombie: TCheckBox;
    Label6: TLabel;
    Label7: TLabel;
    panSLPickup: TPanel;
    Label8: TLabel;
    rbPUWalker: TRadioButton;
    rbPUClimber: TRadioButton;
    rbPUSwimmer: TRadioButton;
    rbPUFloater: TRadioButton;
    rbPUGlider: TRadioButton;
    rbPUDisarmer: TRadioButton;
    rbPUBomber: TRadioButton;
    rbPUStoner: TRadioButton;
    rbPUBlocker: TRadioButton;
    rbPUPlatformer: TRadioButton;
    rbPUBuilder: TRadioButton;
    rbPUStacker: TRadioButton;
    rbPUBasher: TRadioButton;
    rbPUMiner: TRadioButton;
    rbPUDigger: TRadioButton;
    rbPUCloner: TRadioButton;
    panSLSecret: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    ebSecretRank: TEditEx;
    ebSecretLevel: TEditEx;
    panSLBackground: TPanel;
    Image321: TImage32;
    rbDir0: TRadioButton;
    rbDir1: TRadioButton;
    rbDir2: TRadioButton;
    rbDir3: TRadioButton;
    rbDir4: TRadioButton;
    rbDir5: TRadioButton;
    rbDir6: TRadioButton;
    rbDir7: TRadioButton;
    rbDir8: TRadioButton;
    rbDir9: TRadioButton;
    rbDir10: TRadioButton;
    rbDir11: TRadioButton;
    rbDir12: TRadioButton;
    rbDir13: TRadioButton;
    rbDir14: TRadioButton;
    rbDir15: TRadioButton;
    Label12: TLabel;
    Label13: TLabel;
    ebMoveSpeed: TEditEx;
    cbLocked: TCheckBox;
    cbObjectRotate: TCheckBox;
    rbPUFencer: TRadioButton;
    procedure Form_Create(Sender: TObject);
    procedure Form_Destroy(Sender: TObject);
    procedure Form_Deactivate(Sender: TObject);
    procedure Edit_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CbTerrain_Click(Sender: TObject);
    procedure CbObject_Click(Sender: TObject);
    //procedure CbSteel_Click(Sender: TObject);
    procedure SteelOption_Click(Sender: TObject);
    procedure StatusBar_DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
    procedure Edit_Exit(Sender: TObject);
    procedure BtnPickTerrain_Click(Sender: TObject);
    procedure BtnPickObject_Click(Sender: TObject);
    //procedure Edit_QuickSelect(Sender: TObject);
    procedure btnTeleportLinkClick(Sender: TObject);
    procedure PreAssignedSkillClick(Sender: TObject);
    procedure PickupSkillTypeClick(Sender: TObject);
    procedure ebSecretRankChange(Sender: TObject);
    procedure rbDirClick(Sender: TObject);
    procedure ebMoveSpeedChange(Sender: TObject);
  private
    fLabels: TList;
    fEdits: TList;
    fExiting: Boolean;
    fActivePanel: TPanel;
//    Sending: Boolean;
    Clicking: Boolean;
    Upd: Boolean;
    CurrentInfo: TTempData;
    procedure LMLevelChanged(var Msg: TLMLevelChanged); message LM_LEVELCHANGED;
    procedure LMObjectListChanged(var Msg: TLMObjectListChanged); message LM_OBJECTLISTCHANGED;
    procedure LMTerrainListChanged(var Msg: TLMTerrainListChanged); message LM_TERRAINLISTCHANGED;
    procedure LMSteelListChanged(var Msg: TLMSteelListChanged); message LM_STEELLISTCHANGED;
    procedure LMSelectionChanged(var Msg: TLMSelectionChanged); message LM_SELECTIONCHANGED;

    procedure UpdateIDS(aType: TLevelItemType; aGraph: TBaseGraphicSet; V: Integer);
    procedure UpdateEditVar(Sender: TEditEx; bypass: Boolean = false);
    procedure UpdateCheckBoxVar(Sender: TCheckBox);

    procedure DoPickList(aType: TLevelItemType);

    procedure UpdateInterface;
    procedure DisableInterface;
    procedure ClearContents;
    function GetInfo(var Inf: TTempData): Boolean;

    procedure SetEnabledOptions;
    procedure SetActiveSLBox;

    procedure SetSValues(aValue: Integer);
    procedure SetLValues(aValue: Integer);
  protected
    procedure DoAfterSetEditor(Value: TLevelEditor); override;
  public
  end;

implementation

{$R *.dfm}

uses
  LemApp;

const
  tag_x                 = 1;
  tag_y                 = 2;
  tag_w                 = 3;
  tag_h                 = 4;
  tag_tid               = 5;
  tag_oid               = 6;
  tag_ter_erase         = 7;
  tag_ter_invert        = 8;
  tag_ter_nooverwrite   = 9;
  tag_obj_onlyterrain   = 10;
  tag_obj_invert        = 11;
  tag_obj_nooverwrite   = 12;
  tag_obj_faceleft      = 13;
  tag_obj_fake          = 14;
  tag_obj_sv            = 15;
  tag_obj_lv            = 16;
  //tag_stl_neg           = 17;
  tag_obj_invisible     = 18;
  tag_ix                = 20;
  tag_ter_invisible     = 21;
  tag_ter_fake          = 22;
  tag_ter_flip          = 23;
  tag_obj_flip          = 24;
  tag_obj_invert_trigger= 25;
  tag_ter_nooneway      = 26;
  tag_ter_rotate        = 27;
  tag_gen_lock          = 28;
  tag_obj_rotate        = 29;

  panel_colorblocks     = 0;
  panel_seltext         = 1;


{ TEditItemForm }

procedure TEditItemForm.SetActiveSLBox;
var
  TargetPanel: TPanel;
  O, SrcO: TInteractiveObject;
  MO: TMetaObject;
  i: Integer;
  DoSetS: Boolean;
  DoSetL: Boolean;
  ObjCount: Integer;

  procedure SetNewPanel(aPanel: TPanel);
  begin
    if (TargetPanel = nil) or (TargetPanel = aPanel) then
      TargetPanel := aPanel
    else
      TargetPanel := panSLNone;
  end;

  procedure SetCheckWithoutEvent(aBox: TCheckbox; aState: Boolean);
  begin
    aBox.OnClick := nil;
    aBox.Checked := aState;
    aBox.OnClick := PreAssignedSkillClick;
  end;

begin
  TargetPanel := nil;
  SrcO := nil;
  ObjCount := 0;

  DoSetS := true;
  DoSetL := true;

  for i := 0 to Editor.SelectorCollection.Count-1 do
  begin
    if Editor.SelectorCollection[i].RefItem.ItemType <> litObject then Continue;
    O := TInteractiveObject(Editor.SelectorCollection[i].RefItem);
    MO := O.Graph.MetaObjectCollection[O.ObjectID];
    if SrcO = nil then SrcO := O;
    Inc(ObjCount);
    case MO.TriggerEffect of
      11, 12, 28: SetNewPanel(panSLTeleport);
      13, 23: SetNewPanel(panSLLemming);
      14: SetNewPaneL(panSLPickup);
      16: SetNewPanel(panSLSecret);
      30: SetNewPanel(panSLBackground);
      else SetNewPanel(panSLNone);
    end;
    DoSetS := (O.ObjectSValue = SrcO.ObjectSValue) and DoSetS;
    DoSetL := (O.ObjectLValue = SrcO.ObjectLValue) and DoSetL;
  end;

  if TargetPanel = nil then TargetPanel := panSLNone;

  if SrcO <> nil then
  begin
    if (TargetPanel = panSLTeleport) then
      btnTeleportLink.Enabled := (ObjCount > 1);

    if (TargetPanel = panSLLemming) then
      if DoSetL then
      begin
        i := SrcO.ObjectLValue;
        SetCheckWithoutEvent(cbPAClimber, (i and 1) <> 0);
        SetCheckWithoutEvent(cbPASwimmer, (i and 2) <> 0);
        SetCheckWithoutEvent(cbPAFloater, (i and 4) <> 0);
        SetCheckWithoutEvent(cbPAGlider, (i and 8) <> 0);
        SetCheckWithoutEvent(cbPADisarmer, (i and 16) <> 0);
        SetCheckWithoutEvent(cbPABlocker, (i and 32) <> 0);
        SetCheckWithoutEvent(cbPAZombie, (i and 64) <> 0);
      end else begin
        SetCheckWithoutEvent(cbPAClimber, false);
        SetCheckWithoutEvent(cbPASwimmer, false);
        SetCheckWithoutEvent(cbPAFloater, false);
        SetCheckWithoutEvent(cbPAGlider, false);
        SetCheckWithoutEvent(cbPADisarmer, false);
        SetCheckWithoutEvent(cbPABlocker, false);
        SetCheckWithoutEvent(cbPAZombie, false);
      end;

    if (TargetPanel = panSLPickup) then
      if DoSetS then
        case SrcO.ObjectSValue + (SrcO.ObjectLValue * 16) of
          0: rbPUClimber.Checked := true;
          1: rbPUFloater.Checked := true;
          2: rbPUBomber.Checked := true;
          3: rbPUBlocker.Checked := true;
          4: rbPUBuilder.Checked := true;
          5: rbPUBasher.Checked := true;
          6: rbPUMiner.Checked := true;
          7: rbPUDigger.Checked := true;
          8: rbPUWalker.Checked := true;
          9: rbPUSwimmer.Checked := true;
          10: rbPUGlider.Checked := true;
          11: rbPUDisarmer.Checked := true;
          12: rbPUStoner.Checked := true;
          13: rbPUPlatformer.Checked := true;
          14: rbPUStacker.Checked := true;
          15: rbPUCloner.Checked := true;
          16: rbPUFencer.Checked := true;
        end;

    if (TargetPanel = panSLSecret) then
      if DoSetS and DoSetL then
      begin
        ebSecretRank.Text := IntToStr(SrcO.ObjectSValue);
        ebSecretLevel.Text := IntToStr(SrcO.ObjectLValue);
      end;

    if (TargetPanel = panSLBackground) then
    begin
      if DoSetS then
      begin
        case SrcO.ObjectSValue of
          0: rbDir0.Checked := true;
          1: rbDir1.Checked := true;
          2: rbDir2.Checked := true;
          3: rbDir3.Checked := true;
          4: rbDir4.Checked := true;
          5: rbDir5.Checked := true;
          6: rbDir6.Checked := true;
          7: rbDir7.Checked := true;
          8: rbDir8.Checked := true;
          9: rbDir9.Checked := true;
          10: rbDir10.Checked := true;
          11: rbDir11.Checked := true;
          12: rbDir12.Checked := true;
          13: rbDir13.Checked := true;
          14: rbDir14.Checked := true;
          15: rbDir15.Checked := true;
        end;
      end else begin
        rbDir0.Checked := false;
        rbDir1.Checked := false;
        rbDir2.Checked := false;
        rbDir3.Checked := false;
        rbDir4.Checked := false;
        rbDir5.Checked := false;
        rbDir6.Checked := false;
        rbDir7.Checked := false;
        rbDir8.Checked := false;
        rbDir9.Checked := false;
        rbDir10.Checked := false;
        rbDir11.Checked := false;
        rbDir12.Checked := false;
        rbDir13.Checked := false;
        rbDir14.Checked := false;
        rbDir15.Checked := false;
      end;

      if DoSetL then
        ebMoveSpeed.Text := IntToStr(SrcO.ObjectLValue)
      else
        ebMoveSpeed.Text := '';
    end;
  end;

  if TargetPanel = fActivePanel then Exit;

  fActivePanel.Left := Width + 20; // move it off the visible area
  fActivePanel.Visible := false;
  fActivePanel.Enabled := false;

  fActivePanel := TargetPanel;

  fActivePanel.Left := EXTRA_PANEL_LEFT;
  fActivePanel.Top := EXTRA_PANEL_TOP;
  fActivePanel.Visible := true;
  fActivePanel.Enabled := true;
end;

procedure TEditItemForm.SetEnabledOptions;
begin
    if SteelOption.Items.Count = 2 then
    begin
      SteelOption.AddItem('OWW Left', nil);
      SteelOption.AddItem('OWW Right', nil);
      SteelOption.AddItem('OWW Down', nil);
    end;
end;

procedure TEditItemForm.LMLevelChanged(var Msg: TLMLevelChanged);
begin
  UpdateInterface;
end;

procedure TEditItemForm.LMObjectListChanged(var Msg: TLMObjectListChanged);
begin
  UpdateInterface;
end;

procedure TEditItemForm.LMSelectionChanged(var Msg: TLMSelectionChanged);
begin
  UpdateInterface;
end;

procedure TEditItemForm.LMSteelListChanged(var Msg: TLMSteelListChanged);
begin
  UpdateInterface;
end;

procedure TEditItemForm.LMTerrainListChanged(var Msg: TLMTerrainListChanged);
begin
  UpdateInterface;
end;

procedure TEditItemForm.Form_Create(Sender: TObject);
const
  IntChars = ['0'..'9', '-'];
  HexChars = ['0'..'9', 'A'..'F', 'a'..'f'];
begin

   {
   with Constraints do
   begin
     MinWidth := Width;
     MaxWidth := Width;
     MinHeight := Height;
     MaxHeight := Height;
   end; }

  Width := RUNTIME_WIDTH;

  fEdits := TList.Create;
  fEdits.Add(EditX);
  fEdits.Add(EditY);
  fEdits.Add(EditW);
  fEdits.Add(EditH);

  EditTID.Tag := tag_tid;
  EditOID.Tag := tag_oid;

  CbTerrainErase.Tag := tag_ter_erase;
  CbTerrainUpside.Tag := tag_ter_invert;
  CbTerrainNoOverwrite.Tag := tag_ter_nooverwrite;

  CbTerrainRotate.Tag := tag_ter_rotate;

  CbTerrainFlip.Tag := tag_ter_flip;
  CbTerrainNoOneWay.Tag := tag_ter_nooneway;

  CBObjectOnlyOnTerrain.Tag := tag_obj_onlyterrain;
  CbObjectUpside.Tag := tag_obj_invert;
  CbObjectFlip.Tag := tag_obj_flip;
  cbObjectRotate.Tag := tag_obj_rotate;
  CbObjectNoOverwrite.Tag := tag_obj_nooverwrite;
  CbObjectFaceLeft.Tag := tag_obj_faceleft;
  CbObjectFake.Tag := tag_obj_fake;
  CbObjectInvisible.Tag := tag_obj_invisible;

  CbLocked.Tag := tag_gen_lock;

  //CbSteelNegative.Tag := tag_stl_neg;


  EditX.Tag := tag_x;
  EditY.Tag := tag_y;
  EditW.Tag := tag_w;
  EditH.Tag := tag_h;

  EditIx.Tag := tag_ix;

  EditObjSV.Tag := tag_obj_sv;
  EditObjLV.Tag := tag_obj_lv;

  EditX.ValidChars := IntChars;
  EditY.ValidChars := IntChars;
  EditW.ValidChars := IntChars;
  EditH.ValidChars := IntChars;
  EditTID.ValidChars := IntChars;
  EditOID.ValidChars := IntChars;
  EditIx.ValidChars := IntChars;
  EditObjSV.ValidChars := IntChars;
  EditObjLV.ValidChars := IntChars;

  ebSecretRank.ValidChars := IntChars;
  ebSecretLevel.ValidChars := IntChars;
  ebMoveSpeed.ValidChars := IntChars;

  SteelOption.AddItem('Steel', nil);
  SteelOption.AddItem('Negative Steel', nil);
  SteelOption.AddItem('OWW Left', nil);
  SteelOption.AddItem('OWW Right', nil);
  SteelOption.AddItem('OWW Down', nil);

  fActivePanel := panSLNone;
end;

procedure TEditItemForm.Form_Destroy(Sender: TObject);
begin
  fEdits.Free;
end;

procedure TEditItemForm.ClearContents;
begin
  EditTID.Text := '';
  EditOID.Text := '';
  EditObjSV.Text := '';
  EditObjLV.Text := '';
  EditIx.Text := '';
  EditX.Text := '';
  EditY.Text := '';
  EditW.Text := '';
  EditH.Text := '';

{  TxtMinX.Caption := '';
  TxtMinY.Caption := '';
  TxtMinW.Caption := '';
  TxtMinH.Caption := '';

  TxtMaxX.Caption := '';
  TxtMaxY.Caption := '';
  TxtMaxW.Caption := '';
  TxtMaxH.Caption := '';}

end;

//procedure

procedure TEditItemForm.DisableInterface;
begin
  //
end;

procedure TEditItemForm.Edit_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  E: TEditEx absolute Sender;
begin
  Assert(Sender is TEditEx, 'sender error');
  case Key of
    VK_RETURN : UpdateEditVar(E);
    VK_F4:
      case E.Tag of
        tag_tid: DoPickList(litTerrain);
        tag_oid: DoPickList(litObject);
      end;
  end;
end;

{procedure TEditItemForm.WriteContentsToSelection(Tag: Integer; Value: Integer)
begin

end;}

procedure TEditItemForm.UpdateIDS(aType: TLevelItemType; aGraph: TBaseGraphicSet; V: Integer);
var
  i: Integer;
  B: TBaseLevelItem;
  T: Integer;
begin
  if Editor = nil then
    Exit;

  if clicking or upd then exit;
  //clicking:=true;
  try

  with Editor.SelectorCollection.HackedList do
  begin
    if Count = 0 then
      Exit;
    Editor.BeginUpdate;
    try

      for i := 0 to Count - 1 do
      begin
        B := TSelector(List^[i]).RefItem;
        if B.ItemLocked then Continue;
        case aType of
          litTerrain: if b.itemtype = litterrain then
                      begin
                        if aGraph <> nil then b.Graph := aGraph;
                        if tterrain(b).ValidTerrainID(v) then
                          b.Identifier := v;
                      end;
          litObject: if b.itemtype = litObject then
                     begin
                       if aGraph <> nil then b.Graph := aGraph;
                       if TInteractiveObject(b).ValidObjectID(v) then
                         b.Identifier := v;
                     end;
        end;
      end;

  finally
    Editor.EndUpdate;
  end;

  end;

  finally
    //clicking := false;
  end;


end;

procedure TEditItemForm.UpdateEditVar(Sender: TEditEx; bypass: Boolean = false);
var
  i: Integer;
  B: TBaseLevelItem;
  O: TInteractiveObject;
  V, T: Integer;
begin
  if Editor = nil then
    Exit;

  if not (Sender.Modified or bypass) then
    Exit;

  if clicking or upd then exit;
  clicking:=true;
  try

  with Editor.SelectorCollection.HackedList do
  begin
    if Count = 0 then
      Exit;
    T := Sender.Tag;
    V := StrToIntDef(Sender.Text, MinInt);
    if V = MinInt then raise exception.create('invalid input');


//    Log(['updateeditvarr']);
    Editor.BeginUpdate;
    try

{      if T = tag_x then
      begin

        V := V - CurrentInfo.FirstBaseItem.XPosition;
        Editor.MoveSelection(V, 0);
        Sender.Modified := False;
        Exit;
      end;}

      for i := 0 to Count - 1 do
      begin
        B := TSelector(List^[i]).RefItem;
        if B.ItemLocked then Continue;
        case T of
          tag_tid:
            if b.itemtype = litterrain then
              if tterrain(b).ValidTerrainID(v) then
              begin
                b.Identifier := v;
              end;
          tag_oid:
            if b.itemtype = litObject then
              if TInteractiveObject(b).ValidObjectID(v) then
              begin
                b.Identifier := v;
              end;
          tag_x:
            begin

              b.XPosition := V;
            end;
          tag_y: b.YPosition := V;
          tag_w:
            if b.itemtype=litSteel then
              b.ItemWidth := V;
          tag_h:
            if b.itemtype=litSteel then
              b.ItemHeight := V;
          tag_ix:
            //if
            if count=1 then
            b.index := V;
          tag_obj_sv:
            if b.itemtype=litObject then
              b.SetSValue(V);
          tag_obj_lv:
            if b.itemtype=litObject then
              b.SetLValue(V);
        end;
      end;

    Sender.Modified := False;
  finally
    Editor.EndUpdate;
  end;

  end;

  finally
    clicking := false;
  end;


end;

procedure TEditItemForm.UpdateCheckBoxVar(Sender: TCheckBox);
{-------------------------------------------------------------------------------
  This is the main "checkbox change" method, called by the OnClick eventhandler.
  It's rude and stupid but it works.
-------------------------------------------------------------------------------}
var
  i: Integer;
  B: TBaseLevelItem;
  C: Boolean;
  T: Integer;
  Ter: TTerrain;
  Obj: TInteractiveObject;
  Stl: TSteel;
begin
  if Editor = nil then
    Exit;

  with Editor.SelectorCollection.HackedList do
  begin
    if Count = 0 then
      Exit;
    T := Sender.Tag;
    C := Sender.Checked;

    Editor.BeginUpdate;
    try

      for i := 0 to Count - 1 do
      begin
        B := TSelector(List^[i]).RefItem;
        if (B.ItemLocked) and (T <> tag_gen_lock) then Continue;

        case T of
          // terrain
          tag_ter_erase:
            if b.itemtype = litterrain then
            begin
              Ter := TTerrain(B);
              if C
              then Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags + [tdfErase]
              else Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags - [tdfErase]
            end;

          tag_ter_invert:
            if b.itemtype = litterrain then
            begin
              Ter := TTerrain(B);
              if C
              then Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags + [tdfInvert]
              else Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags - [tdfInvert]
            end;

          tag_ter_nooverwrite:
            if b.itemtype = litterrain then
            begin
              Ter := TTerrain(B);
              if C
              then Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags + [tdfNoOverwrite]
              else Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags - [tdfNoOverwrite]
            end;

          tag_ter_invisible:
            if b.itemtype = litterrain then
            begin
              Ter := TTerrain(B);
              if C
              then Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags + [tdfInvisible]
              else Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags - [tdfInvisible]
            end;

          tag_ter_fake:
            if b.itemtype = litterrain then
            begin
              Ter := TTerrain(B);
              if C
              then Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags + [tdfFake]
              else Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags - [tdfFake]
            end;

          tag_ter_flip:
            if b.itemtype = litterrain then
            begin
              Ter := TTerrain(B);
              if C
              then Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags + [tdfFlip]
              else Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags - [tdfFlip]
            end;

          tag_ter_nooneway:
            if b.itemtype = litterrain then
            begin
              Ter := TTerrain(B);
              if C
              then Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags + [tdfNoOneWay]
              else Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags - [tdfNoOneWay]
            end;

          tag_ter_rotate:
            if b.itemtype = litterrain then
            begin
              Ter := TTerrain(B);
              if C
              then Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags + [tdfRotate]
              else Ter.TerrainDrawingFlags := Ter.TerrainDrawingFlags - [tdfRotate]
            end;

          // object
          tag_obj_onlyterrain:
            if b.itemtype = litObject then
            begin
              Obj := TInteractiveObject(B);
              if C
              then Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags + [odfOnlyShowOnTerrain]
              else Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags - [odfOnlyShowOnTerrain]
            end;

          tag_obj_invert:
            if b.itemtype = litObject then
            begin
              Obj := TInteractiveObject(B);
              if C
              then Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags + [odfInvert]
              else Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags - [odfInvert];
            end;

          tag_obj_rotate:
            if b.itemtype = litObject then
            begin
              Obj := TInteractiveObject(B);
              if C
              then Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags + [odfRotate]
              else Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags - [odfRotate]
            end;

          tag_obj_flip:
            if b.itemtype = litObject then
            begin
              Obj := TInteractiveObject(B);
              if C
              then Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags + [odfFlip]
              else Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags - [odfFlip]
            end;

          tag_obj_nooverwrite:
            if b.itemtype = litObject then
            begin
              Obj := TInteractiveObject(B);
              if C
              then Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags + [odfNoOverwrite]
              else Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags - [odfNoOverwrite]
            end;

          tag_obj_faceleft:
            if b.itemtype = litObject then
            begin
              Obj := TInteractiveObject(B);
              if C
              then Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags + [odfFaceLeft]
              else Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags - [odfFaceLeft]
            end;

          tag_obj_fake:
            if b.itemtype = litObject then
            begin
              Obj := TInteractiveObject(B);
              if C
              then Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags + [odfFake]
              else Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags - [odfFake]
            end;

          tag_obj_invisible:
            if b.itemtype = litObject then
            begin
              Obj := TInteractiveObject(B);
              if C
              then Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags + [odfInvisible]
              else Obj.ObjectDrawingFlags := Obj.ObjectDrawingFlags - [odfInvisible]
            end;

          tag_gen_lock:
            B.ItemLocked := C;

          //steel
          {tag_stl_neg:
            if b.ItemType = litSteel then
            begin
              Stl := TSteel(B);
              if C
              then Stl.SteelType := 1
              else Stl.SteelType := 0;
            end;}

        end;
      end;

    finally
      Editor.EndUpdate;
    end;

  end;


end;


procedure TEditItemForm.CbTerrain_Click(Sender: TObject);
var
  C: TCheckBox absolute Sender;
begin
  if Clicking or Upd then Exit; // kut!
  Assert(Sender is TCheckBox, 'sender error');
  Clicking := True;
  try
    UpdateCheckBoxVar(C);
  finally
    Clicking := False;
  end;
end;

procedure TEditItemForm.CbObject_Click(Sender: TObject);
var
  C: TCheckBox absolute Sender;
begin
  if Clicking or Upd then Exit; // kut!
  Assert(Sender is TCheckBox, 'sender error');
  Clicking := True;
  try
    UpdateCheckBoxVar(C);
  finally
    Clicking := False;
  end;
end;

{procedure TEditItemForm.CbSteel_Click(Sender: TObject);
var
  C: TCheckBox absolute Sender;
begin
  if Clicking or Upd then Exit; // kut!
  Assert(Sender is TCheckBox, 'sender error');
  Clicking := True;
  try
    UpdateCheckBoxVar(C);
  finally
    Clicking := False;
  end;
end;}

procedure TEditItemForm.SteelOption_Click(Sender: TObject);
var
  S, i: Integer;
  B: TBaseLevelItem;
begin
  S := TComboBox(Sender).ItemIndex;
  Editor.BeginUpdate;
  try
    with Editor.SelectorCollection.HackedList do
    begin
      if Count = 0 then
        Exit;
      for i := 0 to Count-1 do
      begin
        B := TSelector(List^[i]).RefItem;
        if B.ItemLocked then Continue;
        if not (B.ItemType = litsteel) then Continue;
        TSteel(B).SteelType := S;
      end;
    end;
  finally
    Editor.EndUpdate;
  end;
end;

procedure TEditItemForm.DoAfterSetEditor(Value: TLevelEditor);
begin
  UpdateInterface;
end;

procedure TEditItemForm.StatusBar_DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel; const Rect: TRect);
var
  D: TRect;
  H: INteger;
begin
  if Panel.Index = panel_colorblocks then
  begin
    with StatusBar.Canvas do
    begin
      Pen.Color := clGray;
      D := Rect;
      InflateRect(D, -2, -2);
      D.Right := D.Left + (D.Bottom - D.Top);
      H := D.Bottom - D.Top;

      if CurrentInfo.HasObjects then
        Brush.Color := ObjColorH
      else
        Brush.Color := clBtnFace;
      Rectangle(D);

      if CurrentInfo.HasTerrain then
        Brush.Color := TerColorH
      else
        Brush.Color := clBtnFace;
      RectMove(D, H + 2, 0);
      Rectangle(D);

      if CurrentInfo.HasSteel then
        Brush.Color := StlColorH
      else
        Brush.Color := clBtnFace;
      RectMove(D, H + 2, 0);
      Rectangle(D);
    end;
  end;
end;

function TEditItemForm.GetInfo(var Inf: TTempData): Boolean;
var
  i: Integer;
  B: TBaseLevelItem;
  Flag: Boolean;
  IsFirstTerrain, IsFirstObject, IsFirstSteel: Boolean;
begin

  Result := Editor.SelectorCollection.Count > 0;
  FillChar(Inf, SizeOf(Inf), 0);
  Inf.FirstTerrainID := -1;
  Inf.FirstObjectID := -1;
  Inf.FirstObjectSV := 0;
  Inf.FirstObjectLV := 0;
  Inf.FirstSteelOption := 0;
  with Editor, SelectorCollection.HackedList do
  begin
    if Count = 0 then
      Exit;

    for i := 0 to Count - 1 do
    begin
      B := TSelector(List^[i]).RefItem;
      if i = 0 then
        Inf.FirstBaseItem := B;
      IsFirstTerrain := (B.ItemType = litTerrain) and not (litTerrain in Inf.SelTypes);
      IsFirstObject := (B.ItemType = litObject) and not (litObject in Inf.SelTypes);
      IsFirstSteel := (B.ItemType = litSteel) and not (litSteel in Inf.SelTypes);
      Include(Inf.SelTypes, B.ItemType);
      // very first item
      if i = 0 then
      begin
        Inf.TheIndex := B.Index;
        Inf.SelCount := Count;
        Inf.FirstX := B.XPosition;
        Inf.FirstY := B.YPosition;
        Inf.FirstW := B.ItemWidth;
        Inf.FirstH := B.ItemHeight;
        Inf.FirstLocked := B.ItemLocked;
        Inf.SameX := True;
        Inf.SameY := True;
        Inf.SameW := True;
        Inf.SameH := True;
        Inf.SameLocked := true;
      end
      // following items
      else begin
        Inf.SameX := Inf.SameX and (B.Xposition = Inf.FirstX);
        Inf.SameY := Inf.SameY and (B.Yposition = Inf.FirstY);
        Inf.SameW := Inf.SameW and (B.ItemWidth = Inf.FirstW);
        Inf.SameH := Inf.SameH and (B.ItemHeight = Inf.FirstH);
        Inf.SameLocked := Inf.SameLocked and (B.ItemLocked = Inf.FirstLocked);
      end;

      case B.ItemType of
        litTerrain:
          with TTerrain(B) do
          begin
            // first terrain
            if IsFirstTerrain then
            begin
              Inf.FirstTerrainErase := tdfErase in TerrainDrawingFlags;
              Inf.FirstTerrainInvert := tdfInvert in TerrainDrawingFlags;
              Inf.FirstTerrainNoOverwrite := tdfNoOverwrite in TerrainDrawingFlags;
              Inf.FirstTerrainInvisible := tdfInvisible in TerrainDrawingFlags;
              Inf.FirstTerrainFake := tdfFake in TerrainDrawingFlags;
              Inf.FirstTerrainFlip := tdfFlip in TerrainDrawingFlags;
              Inf.FirstTerrainNoOneWay := tdfNoOneWay in TerrainDrawingFlags;
              Inf.FirstTerrainRotate := tdfRotate in TerrainDrawingFlags;
              Inf.SameTerrainErase := True;
              Inf.SameTerrainInvert := True;
              Inf.SameTerrainNoOverwrite := True;
              Inf.SameTerrainInvisible := True;
              Inf.SameTerrainFake := True;
              Inf.SameTerrainFlip := True;
              Inf.SameTerrainNoOneWay := True;
              Inf.SameTerrainRotate := True;

              Inf.FirstTerrainID := TerrainID;
              Inf.SameTerrainID := True;

              Inf.TerGrSet := Graph;
            end
            // following terrain
            else begin
              Inf.SameTerrainErase := Inf.SameTerrainErase and (Inf.FirstTerrainErase = (tdfErase in TerrainDrawingFlags) );
              Inf.SameTerrainInvert := Inf.SameTerrainInvert and ( Inf.FirstTerrainInvert = (tdfInvert in TerrainDrawingFlags));
              Inf.SameTerrainNoOverwrite := Inf.SameTerrainNoOverwrite and (Inf.FirstTerrainNoOverwrite = (tdfNoOverwrite in TerrainDrawingFlags));
              Inf.SameTerrainInvisible := Inf.SameTerrainInvisible and (Inf.FirstTerrainInvisible = (tdfInvisible in TerrainDrawingFlags));
              Inf.SameTerrainFake := Inf.SameTerrainFake and (Inf.FirstTerrainFake = (tdfFake in TerrainDrawingFlags));
              Inf.SameTerrainFlip := Inf.SameTerrainFlip and (Inf.FirstTerrainFlip = (tdfFlip in TerrainDrawingFlags));
              Inf.SameTerrainNoOneWay := Inf.SameTerrainNoOneWay and (Inf.FirstTerrainNoOneWay = (tdfNoOneWay in TerrainDrawingFlags));
              Inf.SameTerrainRotate := Inf.SameTerrainRotate and (Inf.FirstTerrainRotate = (tdfRotate in TerrainDrawingFlags));
              Inf.SameTerrainID := Inf.SameTerrainID and (Inf.FirstTerrainID = TerrainID);
            end;
          end;
        litObject:
          with TInteractiveObject(B) do
          begin
            // first object
            if IsFirstObject then
            begin
              Inf.FirstObjectOnlyTerrain := odfOnlyShowOnTerrain in ObjectDrawingFlags;
              Inf.FirstObjectInvert := odfInvert in ObjectDrawingFlags;
              Inf.FirstObjectFlip   := odfFlip in ObjectDrawingFlags;
              Inf.FirstObjectRotate := odfRotate in ObjectDrawingFlags;
              Inf.FirstObjectNoOverwrite := odfNoOverwrite in ObjectDrawingFlags;
              Inf.FirstObjectFaceLeft := odfFaceLeft in ObjectDrawingFlags;
              Inf.FirstObjectFake := odfFake in ObjectDrawingFlags;
              Inf.FirstObjectInvisible := odfInvisible in ObjectDrawingFlags;
              Inf.SameObjectOnlyTerrain := True;
              Inf.SameObjectInvert := True;
              Inf.SameObjectFlip := True;
              Inf.SameObjectRotate := True;
              Inf.SameObjectNoOverwrite := True;
              Inf.SameObjectFaceLeft := True;
              Inf.SameObjectFake := True;
              Inf.SameObjectInvisible := True;
              Inf.FirstObjectID := ObjectID;
              Inf.FirstObjectSV := ObjectSValue;
              Inf.FirstObjectLV := ObjectLValue;
              Inf.SameObjectID := True;
              Inf.SameObjectSV := True;
              Inf.SameObjectLV := True;

              Inf.ObjGrSet := Graph;
            end
            // following object
            else begin
              Inf.SameObjectOnlyTerrain := Inf.SameObjectOnlyTerrain and (Inf.FirstObjectOnlyTerrain = (odfOnlyShowOnTerrain in ObjectDrawingFlags));
              Inf.SameObjectInvert := Inf.SameObjectInvert and (Inf.FirstObjectInvert = (odfInvert in ObjectDrawingFlags));
              Inf.SameObjectFlip := Inf.SameObjectFlip and (Inf.FirstObjectFlip = (odfFlip in ObjectDrawingFlags));
              Inf.SameObjectRotate := Inf.SameObjectRotate and (Inf.FirstObjectRotate = (odfRotate in ObjectDrawingFlags));
              Inf.SameObjectNoOverwrite := Inf.SameObjectNoOverwrite and (Inf.FirstObjectNoOverwrite = (odfNoOverwrite in ObjectDrawingFlags));
              Inf.SameObjectFaceLeft := Inf.SameObjectFaceLeft and (Inf.FirstObjectFaceLeft = (odfFaceLeft in ObjectDrawingFlags));
              Inf.SameObjectFake := Inf.SameObjectFake and (Inf.FirstObjectFake = (odfFake in ObjectDrawingFlags));
              Inf.SameObjectInvisible := Inf.SameObjectInvisible and (Inf.FirstObjectInvisible = (odfInvisible in ObjectDrawingFlags));
              Inf.SameObjectID := Inf.SameObjectID and (Inf.FirstObjectID = ObjectID);
              Inf.SameObjectSV := Inf.SameObjectSV and (Inf.FirstObjectSV = ObjectSValue);
              Inf.SameObjectLV := Inf.SameObjectLV and (Inf.FirstObjectLV = ObjectLValue);
            end;
          end;
        litSteel:
          with TSteel(B) do
          begin
            if IsFirstSteel then
            begin
              Inf.FirstSteelOption := SteelType;
              Inf.SameSteelOption := true;
            end else begin
              Inf.SameSteelOption := Inf.SameSteelOption and (Inf.FirstSteelOption = SteelType);
            end;
          end;

      end; // case

    end; // for i

    Inf.HasObjects := litObject in Inf.SelTypes;
    Inf.HasTerrain := litTerrain in Inf.SelTypes;
    Inf.HasSteel := litSteel in Inf.SelTypes;


  end; // with
end;

procedure TEditItemForm.UpdateInterface;
var
  Inf: TTempData;
  HasTerrain, HasObjects, HasSteel : Boolean;

    procedure SetCheckBox(aCheckBox: TCheckBox; First, All: Boolean);
    begin
      if not All
      then aCheckBox.State := cbGrayed
      else aCheckBox.Checked := First;
    end;

begin
  if Clicking or Upd then
    Exit;

  if Editor = nil then
  begin
    DisableInterface;
    Exit;
  end;

  SetEnabledOptions;

  Upd := True;
  try
    GetInfo(Inf);
    CurrentInfo := Inf;


    if Inf.SelCount > 1 then
    begin
      StatusBar.Panels[1].Text := i2s(Inf.SelCount) + ' items selected';
    end
    else if Inf.SelCount = 1 then begin
      StatusBar.Panels[1].Text := '1 item selected';
    end
    else if Inf.SelCount = 0 then
      StatusBar.Panels[1].Text := 'No items selected';

    statusbar.invalidate;
    //

    //general
    EditX.Enabled := Inf.SelCount > 0;
    EditY.Enabled := Inf.SelCount > 0;
    EditW.Enabled := Inf.SelCount > 0;
    EditH.Enabled := Inf.SelCount > 0;
    EditIx.Enabled := Inf.SelCount = 1;
    LabelX.Enabled := Inf.SelCount > 0;
    LabelY.Enabled := Inf.SelCount > 0;
    LabelW.Enabled := Inf.SelCount > 0;
    LabelH.Enabled := Inf.SelCount > 0; //controls
    if Inf.SelCount = 0 then begin
      EditX.Text := '';
      EditY.Text := '';
      EditW.Text := '';
      EditH.Text := '';
      SetCheckbox(cbLocked, false, true);
      cbLocked.Enabled := false;
    end
    else begin
      EditX.Text := i2s(Inf.FirstX);
      EditY.Text := i2s(Inf.FirstY);
      EditW.Text := i2s(Inf.FirstW);
      EditH.Text := i2s(Inf.FirstH);
      SetCheckbox(cbLocked, Inf.FirstLocked, Inf.SameLocked);
      cbLocked.Enabled := true;
    end;

    if (Inf.SelCount = 1)
    then EditIx.Text := i2s(Inf.TheIndex)
    else EditIx.Text := '';
    LabelIndex.Enabled := Inf.SelCount = 1;

    HasTerrain := litTerrain in Inf.SelTypes;
    HasObjects := litObject in Inf.SelTypes;
    HasSteel   := litSteel in Inf.SelTypes;
    with Inf do
    begin
      // terrain
       EditTID.Enabled := HasTerrain;
       LabelTerrain.Enabled := HasTerrain;
       CbTerrainErase.Enabled := HasTerrain;
       CbTerrainUpside.Enabled := HasTerrain;
       CbTerrainFlip.Enabled := HasTerrain;
       CbTerrainNoOverwrite.Enabled := HasTerrain;
       CbTerrainRotate.Enabled := HasTerrain;
       CbTerrainNoOneWay.Enabled := HasTerrain;
       BtnPickTerrain.Enabled := HasTerrain;
      if HasTerrain then
      begin
        EditTID.Text := i2s(FirstTerrainID);
        if Inf.FirstTerrainID >= 0 then
          //ImgTerrain.Bitmap.Assign(PictureMgr.TerrainBitmaps[Inf.GrSet, Inf.FirstTerrainID]);
          ImgTerrain.Bitmap.Assign(TerGrSet.TerrainBitmaps[Inf.FirstTerrainID]);
        SetCheckBox(CbTerrainErase, Inf.FirstTerrainErase, Inf.SameTerrainErase);
        SetCheckBox(CbTerrainUpside, Inf.FirstTerrainInvert, Inf.SameTerrainInvert);
        SetCheckBox(CbTerrainFlip, Inf.FirstTerrainFlip, Inf.SameTerrainFlip);
        SetCheckBox(CbTerrainNoOverwrite, Inf.FirstTerrainNoOverwrite, Inf.SameTerrainNoOverwrite);
        SetCheckBox(CbTerrainNoOneWay, Inf.FirstTerrainNoOneWay, Inf.SameTerrainNoOneWay);
        SetCheckBox(CbTerrainRotate, Inf.FirstTerrainRotate, Inf.SameTerrainRotate);
      end
      else begin
        EditTID.Text := '';
        ImgTerrain.Bitmap := nil;
        CbTerrainErase.Checked := False;
        CbTerrainUpside.Checked := False;
        CbTerrainFlip.Checked := False;
        CbTerrainNoOverwrite.Checked := False;
        CbTerrainRotate.Checked := False;
        CbTerrainNoOneWay.Checked := False;
      end;
      // objects
       EditOID.Enabled := HasObjects;
       EditObjSV.Enabled := HasObjects;
       EditObjLV.Enabled := HasObjects;
       LabelSV.Enabled := EditObjSV.Enabled;
       LabelLV.Enabled := EditObjLV.Enabled;
       LabelObject.Enabled := HasObjects;
       CBObjectOnlyOnTerrain.Enabled := HasObjects;
       CbObjectUpside.Enabled := HasObjects;
       CbObjectFlip.Enabled := HasObjects;
       cbObjectRotate.Enabled := HasObjects;
       CbObjectNoOverwrite.Enabled := HasObjects;
       CbObjectFaceLeft.Enabled := HasObjects;
       CbObjectFake.Enabled := HasObjects;
       CbObjectInvisible.Enabled := HasObjects;
       BtnPickObject.Enabled := HasObjects;
      if HasObjects then
      begin
        EditOID.Text := i2s(FirstObjectID);
        EditObjSV.Text := i2s(FirstObjectSV);
        EditObjLV.Text := i2s(FirstObjectLV);
        if Inf.FirstObjectID >= 0 then
          //ImgObject.Bitmap.Assign(PictureMgr.ObjectBitmaps[Inf.GrSet, Inf.FirstObjectID]);
          //ImgObject.Bitmap.Assign(GrSet.ObjectBitmaps[Inf.FirstObjectID]);
//          ImgObject.Bitmap.Assign(GrSet.ObjectBitmaps[Inf.FirstObjectID]);
          ObjGrSet.GetObjectFrame(Inf.FirstObjectID, 0, ImgObject.Bitmap);
        SetCheckBox(CBObjectOnlyOnTerrain, Inf.FirstObjectOnlyTerrain, Inf.SameObjectOnlyTerrain);
        SetCheckBox(CbObjectUpside, Inf.FirstObjectInvert, Inf.SameObjectInvert);
        SetCheckBox(CbObjectFlip, Inf.FirstObjectFlip, Inf.SameObjectFlip);
        SetCheckBox(CbObjectRotate, Inf.FirstObjectRotate, Inf.SameObjectRotate);
        SetCheckBox(CbObjectNoOverwrite, Inf.FirstObjectNoOverwrite, Inf.SameObjectNoOverwrite);
        SetCheckBox(CbObjectFaceLeft, Inf.FirstObjectFaceLeft, Inf.SameObjectFaceLeft);
        SetCheckBox(CbObjectFake, Inf.FirstObjectFake, Inf.SameObjectFake);
        SetCheckBox(CbObjectInvisible, Inf.FirstObjectInvisible, Inf.SameObjectInvisible);
      end
      else begin
        EditOID.Text := '';
        EditObjSV.Text := '';
        EditObjLV.Text := '';
        ImgObject.Bitmap := nil;
        CBObjectOnlyOnTerrain.Checked := False;
        CbObjectUpside.Checked := False;
        CbObjectFlip.Checked := False;
        cbObjectRotate.Checked := false;
        CbObjectNoOverwrite.Checked := False;
        CbObjectFaceLeft.Checked := False;
        CbObjectFake.Checked := False;
        CbObjectInvisible.Checked := False;
      end;

      //Steel
      EditW.Enabled := HasSteel;
      EditH.Enabled := HasSteel;
      SteelOption.Enabled := HasSteel;
      if HasSteel then
      begin
        if Inf.SameSteelOption then
          SteelOption.ItemIndex := Inf.FirstSteelOption
          else
          SteelOption.Text := '(Mixed)';
      end else begin
        SteelOption.ItemIndex := 0;
      end;
    end;

    SetActiveSLBox;
  finally
    Upd := False;
  end;
end;

procedure TEditItemForm.Form_Deactivate(Sender: TObject);
var
  E: TEditEx;
begin
  if ActiveControl is TEditEx then
  begin
    E := TEditEx(ActiveControl);
    if Assigned(E.OnExit) then
      E.OnExit(E);
  end;
//
end;

procedure TEditItemForm.Edit_Exit(Sender: TObject);
var
  E: TEditEx absolute Sender;
begin
  Assert(Sender is TEditEx, 'sender error (no TEditEx)');
  if E.Modified then
    try
      UpdateEditVar(E);
    except
      E.SetFocus;
      raise;
    end;
end;

procedure TEditItemForm.DoPickList(aType: TLevelItemType);
var
  i: Integer;
  UseSet: TBaseGraphicSet;
begin
  if Editor = nil then
    Exit;
  if not (aType in CurrentInfo.SelTypes) then
    Exit;

  case aType of
    litTerrain:
      begin
        UseSet := CurrentInfo.TerGrSet;
        i := CurrentInfo.FirstTerrainID;
        if PickListTerrain(UseSet, i) then
          UpdateIDS(aType, UseSet, i);
      end;

    litObject:
      begin
        UseSet := CurrentInfo.ObjGrSet;
        i := CurrentInfo.FirstObjectID;
        if PicklistObject(UseSet, i) then
        begin
          if UseSet.MetaObjectCollection[i].TriggerEffect = 32 then
          begin
            ShowMessage('You selected a background image object. These cannot be placed as regular objects, use the "Select Background" button' + #13 + 'in the Level Properties menu instead.');
            Exit;
          end;
          UpdateIDS(aType, UseSet, i);
        end;
      end;
  end;
//    fTerrain.TerrainID := i;
end;

procedure TEditItemForm.BtnPickTerrain_Click(Sender: TObject);
begin
  DoPickList(litTerrain);
end;

procedure TEditItemForm.BtnPickObject_Click(Sender: TObject);
begin
  DoPicklist(litObject);
end;

procedure TEditItemForm.SetSValues(aValue: Integer);
var
  i: Integer;
  O: TInteractiveObject;
begin
  Editor.BeginUpdate;
  try
    for i := 0 to Editor.SelectorCollection.Count-1 do
    begin
      if Editor.SelectorCollection[i].RefItem.ItemLocked then Continue;
      if Editor.SelectorCollection[i].RefItem.ItemType <> litObject then Continue;
      O := TInteractiveObject(Editor.SelectorCollection[i].RefItem);
      O.ObjectSValue := aValue;
    end;
  finally
    Editor.EndUpdate;
  end;
end;

procedure TEditItemForm.SetLValues(aValue: Integer);
var
  i: Integer;
  O: TInteractiveObject;
begin
  Editor.BeginUpdate;
  try
    for i := 0 to Editor.SelectorCollection.Count-1 do
    begin
      if Editor.SelectorCollection[i].RefItem.ItemLocked then Continue;
      if Editor.SelectorCollection[i].RefItem.ItemType <> litObject then Continue;
      O := TInteractiveObject(Editor.SelectorCollection[i].RefItem);
      O.ObjectLValue := aValue;
    end;
  finally
    Editor.EndUpdate;
  end;
end;

procedure TEditItemForm.btnTeleportLinkClick(Sender: TObject);
var
  i: Integer;
  AcceptableValues: array[0..15] of Boolean;
  UseValue: Integer;
  O: TInteractiveObject;
  MO: TMetaObject;
begin
  // We need to find a value NOT used by any other teleporters, receivers or one-way teleporters.
  // It's easiest to just ignore the current value of the selected ones too.
  for i := 0 to 15 do
    AcceptableValues[i] := true;

  for i := 0 to Editor.InteractiveObjectCollection.Count-1 do
  begin
    O := Editor.InteractiveObjectCollection[i];
    MO := O.Graph.MetaObjectCollection[O.ObjectID];
    if (MO.TriggerEffect in [11, 12, 28]) then AcceptableValues[O.ObjectSValue] := false;
  end;

  UseValue := 0;
  for i := 0 to 15 do
    if AcceptableValues[i] then
    begin
      UseValue := i;
      Break;
    end else if i = 15 then
      ShowMessage('Linking failed. You may have too many teleporters in your level for auto-linking.');

  SetSValues(UseValue);
end;

procedure TEditItemForm.PreAssignedSkillClick(Sender: TObject);
var
  NewLValue: Integer;

  procedure ClearBox(aTarget: TCheckbox);
  begin
    // This procedure ensures PreAssignedSkillClick won't be triggered again for the unchecking.
    aTarget.OnClick := nil;
    aTarget.Checked := false;
    aTarget.OnClick := PreAssignedSkillClick;
  end;

  procedure ApplyBox(aBox: TCheckbox; aValue: Integer);
  begin
    if aBox.Checked then NewLValue := NewLValue + aValue;
  end;
begin
  // First, filter out imcompatible combinations.
  if TCheckbox(Sender).Checked then
  begin
    if Sender = cbPAFloater then ClearBox(cbPAGlider);
    if Sender = cbPAGlider then ClearBox(cbPAFloater);
  end;

  NewLValue := 0;
  ApplyBox(cbPAClimber, 1);
  ApplyBox(cbPASwimmer, 2);
  ApplyBox(cbPAFloater, 4);
  ApplyBox(cbPAGlider, 8);
  ApplyBox(cbPADisarmer, 16);
  ApplyBox(cbPABlocker, 32);
  ApplyBox(cbPAZombie, 64);

  SetLValues(NewLValue);
end;

procedure TEditItemForm.PickupSkillTypeClick(Sender: TObject);
begin
  SetSValues(TRadioButton(Sender).Tag mod 16);
  SetLValues(TRadioButton(Sender).Tag div 16);
end;

procedure TEditItemForm.ebSecretRankChange(Sender: TObject);
var
  R, L: Integer;
begin
  if (ebSecretRank.Text = '') or (ebSecretLevel.Text = '') then Exit;
  // This triggers some OnChange somewhere, which resets the L value.
  // So we need to store it in variables.
  R := StrToIntDef(ebSecretRank.Text, 1);
  L := StrToIntDef(ebSecretLevel.Text, 1);
  SetSValues(R);
  SetLValues(L);
end;

procedure TEditItemForm.rbDirClick(Sender: TObject);
begin
  if not TRadioButton(Sender).Checked then Exit;
  SetSValues(TRadioButton(Sender).Tag);
end;

procedure TEditItemForm.ebMoveSpeedChange(Sender: TObject);
begin
  if ebMoveSpeed.Text = '' then Exit;
  SetLValues(StrToIntDef(ebMoveSpeed.Text, 0));
end;

end.

