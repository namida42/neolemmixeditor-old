unit LemEditHistory;

// For Undo feature.

interface

uses
  Classes, SysUtils, Contnrs, LemTypes, LemCore, LemLemminiStyles, LemDosStyles;

type
  TFormatMemoryStream = class(TMemoryStream)
    private
      fStyle: TBaseLemmingStyle;
      function GetFormat: TLemmingFileFormat;
    public
      property LevelFormat: TLemmingFileFormat read GetFormat;
      property Style: TBaseLemmingStyle read fStyle write fStyle;
  end;

  TFormatMemoryStreamList = class(TObjectList)
    private
      function GetItem(Index: Integer): TFormatMemoryStream;
    public
      function Add: TFormatMemoryStream;
      property Items[Index: Integer]: TFormatMemoryStream read GetItem; default;
      property List;
  end;

implementation

function TFormatMemoryStreamList.Add: TFormatMemoryStream;
begin
  // Creates a new TFormatMemoryStream, adds it, and returns it.
  Result := TFormatMemoryStream.Create;
  inherited Add(Result);
end;

function TFormatMemoryStreamList.GetItem(Index: Integer): TFormatMemoryStream;
begin
  // Gets a TFormatMemoryStream from the list.
  Result := inherited Get(Index);
end;

function TFormatMemoryStream.GetFormat: TLemmingFileFormat;
begin
  if Style is TLemminiStyle then
    Result := lffLemmini
  else
    Result := lffLvl;
end;

end.