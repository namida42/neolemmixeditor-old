unit UDate;

interface


(*

Sysutils

The following are a set of variables used to define the format for numeric or date/time strings:

var CurrencyString: string;
var CurrencyFormat: Byte;
var NegCurrFormat: Byte;
var ThousandSeparator: Char;
var DecimalSeparator: Char;
var CurrencyDecimals: Byte;
var DateSeparator: Char;
var ShortDateFormat: string;
var LongDateFormat: string;
var TimeSeparator: Char;
var TimeAMString: string;
var TimePMString: string;
var ShortTimeFormat: string;

var LongTimeFormat: string;
var ShortMonthNames: array[1..12] of string;
var LongMonthNames: array[1..12] of string;
var ShortDayNames: array[1..7] of string;
var LongDayNames: array[1..7] of string;

var SysLocale: TSysLocale;
var EraNames: array[1..7] of string;
var EraYearOffsets: array[1..7] of Integer;
var TwoDigitYearCenturyWindow: Word = 50;

var ListSeparator: Char;

FormatDateTime

*)

uses
  Controls, SysUtils;

type
  TDateDisplayMode = record
    Seperator: Char;
  end;

function DisplayDate(const aDate: TDate; Long: boolean): string;

implementation

uses
  UMisc;

const
  Dagnamen: array [1..7] of String15 =
    ('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
  MaandNamen: array[1..12] of String =
    ('januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december');

//    LongMonth

function DisplayDate(const aDate: TDate; Long: boolean): string;
var
  Y, M, D: word;
begin
  begin
    Result := '';
    if aDate = 0 then
      Exit;
    case Long of
      True: Result := FormatDateTime('dd mmmmmm yyyy', aDate);
      False: Result := FormatDateTime('d-m-yyyy', aDate);
    end;
//    DecodeDate(aDate, Y, M, D);
  //  Result := i2s(D) + ' ' + MaandNamen[M]{Copy(MaandNamen[M], 1, 3)} + ' ' + i2s(Y);
  end;
end;

var
  i: integer;

initialization

  for i := 1 to 12 do
    LongMonthNames[i] := MaandNamen[i];

    DecimalSeparator := '.';
//var CurrencyDecimals: Byte;
    DateSeparator := '-';
//var ShortDateFormat: string;
//var LongDateFormat: string;
//var TimeSeparator: Char;

end.

