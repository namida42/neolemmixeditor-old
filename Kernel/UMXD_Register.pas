unit UMXD_Register;

interface

procedure Register;

implementation

uses
  Classes, DesignIntf, DesignEditors, Dialogs,
  UMXD_DesignEditors,
  UMx, UMxStrings;


procedure Register;
begin
  RegisterComponents('Master', [TSuperMatrix,
                               TStringMatrix,
                               TListBoxMatrix,
                               TCalendarMatrix
                               ]);

  //RegisterPropertyEditor(TypeInfo(TMatrixOptions), TBaseMatrix, 'Options', TMatrixOptionsPropertyEditor);;
end;


end.

