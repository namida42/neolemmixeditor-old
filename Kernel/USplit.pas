
unit USplit;

{.$define dolog}

interface

(*
 Splitters todo:

recalculate afhankelijk van sender werken
*** eigen border tekenen (hoe?) (--> bevelkind! dus niet eigen border)
*** bufferbitmap maken
*** pulldown maken bij constraints
**  scale slim maken (hoe perfect? subsubs ook?)
autoconstraints inbouwen zodat we niet steeds opnieuw hoeven te rekenen
*** aantasten echte constraints weghalen als autosize, slimmer dus
*** saFirst en saLast afmaken
*** Visible inbouwen
xxx "uit beeld slepen" inbouwen
knipperen bij parentautosize wegwerken
tijdens ��n splitterresize-sessie kunnen we wat vlaggen zetten (min-reached, max-reached)
bepaalde properties mogen niet veranderen in bepaalde situaties (tijdens resize bijvoorbeeld)
*** InternalCheckConstraints afmaken en gebruiken in CheckSize (ietsje anders gedaan)
*** autosize is weer verbeterd, horizontaal moet nog
nivo's inbouwen
misschien rootpanel + childpanels maken
*** designtime delete met subcontrols IDE crasht. csDestroying heeft geholpen
priopanel resize inbouwen of prio integer
beginupdate / paintlock / disablealign goed maken
*** cancel splitterdrag (is wat slordig, moet mooier)
hoekpunten slepen
*** correct procedure in recalculate iets soepeler en slimmer maken (wel flicker maar dat doen borland panels ook)
kijken of we requestalign slimmer kunnen maken. als de bounds totaal niet veranderd zijn hoeft er niets
splitters op panel self tekenen
align voor children onmogelijk maken
*)

uses
  Windows, Classes, Messages, Controls, Graphics, Forms, SysUtils, TypInfo, ExtCtrls,
  Dialogs, Math, Themes,
  UMisc, UTools;

type
  // split direction
  TSplitMode = (
    smVertical,             // Panel wordt vertikaal gesplitst
    smHorizontal            // Panel wordt horizontaal gesplitst
  );

  // panel positie (nog niet helemaal ok)
  TSplitPosition = (
    spNone,
    spFirst,
    spLast
  );

  // hoe worden de panels verdeeld
  TDivideMode = (
    dmNone,
    dmEqual
//    dmScale
  );

  TSplitPanelOption = (
    poAutoSizeParent,              // tricky: autosize de parent
    poDragPush,                    // panels slepen kan met "duwen" en "trekken" van onder en bovengelegen panels
    poDesignTemplateWarning,       // designtime waarschuwing bij verandering template
    poDisablePaintingWhenCovered,  // disable painting als er een clientcontrol op staat
    poPaintBuffered                // paint eerst naar een bitmap
  );
  TSplitPanelOptions = set of TSplitPanelOption;

  TQuickBorderStyle = (
    qbUnknown,
    qbNone,
    qbBorder,
    qbLowered,
    qbRaised
  );

  // herberekening vlag (intern)
  TCalcInfo = (
    riNone,                 // Geen info
    riPanelAdded,           // Er is een panel toegevoegd
    riDistanceChanged,      // Afstand tussen de panels is veranderd
    riSplitModeChanged,     // De splitmode is veranderd
    riDivideModeChanged,    // Dividemode is veranderd
    riAlignControls,        // AlignControls actief
    riSplitterMoved,        // Splitter moved
    riTemplateChanged       // Unknown
  );

  // voor reset children (intern)
  TChildCommand = (
    ccDistance,
    ccColor,
    ccLiveSizing,
    ccQuickBorder
  );

  // state (intern)
  TPanelStates = (
    psUpdatingLists,
    psCalculating,
    psUpdatingBorder,
    psNoPaint,
    psSplitterDragging,
    psParentUpdating,
    psNeedsScale
  );
  TPanelState = set of TPanelStates;

  // mode van panel voor makkelijke access (intern) (nog niet gebruikt)
  TPanelMode = (
    pmRoot,
    pmMiddle,
    pmLeave
  );

  // draginfo voor panelsplitter (intern)
  TDragInfo = record
    iDragging : Boolean;
    iStartX   : Integer; // start drag x
    iStartY   : Integer; // start drag y
    iCurrentX : Integer; // active muis x
    iCurrentY : Integer; // active muis y
    iDrawX    : Integer; // laatste xor teken x
    iDrawY    : Integer; // laatste xor teken y
  end;


const
  DEF_CONSTRAINTSIZE = 2;
  DEF_OPTIONS = [
    poDragPush,
    poDesignTemplateWarning,
    poDisablePaintingWhenCovered,
    poPaintBuffered];



type
  TSplitPanel = class;
  TPanelSplitter = class;
  TSplitPanelList = class(TList)
  private
    function GetItem(Index: Integer): TSplitPanel;
  protected
    procedure SortBySize(SmallFirst: Boolean);
  public
    function Add(Item: TSplitPanel): Integer;
    function Insert(Index: Integer; Item: TSplitPanel): Integer;
    property Items[Index: Integer]: TSplitPanel read GetItem; default;
  end;

  TPanelSplitterList = class(TList)
  private
    function GetItem(Index: Integer): TPanelSplitter;
  protected
  public
    function Add(Item: TPanelSplitter): Integer;
    function Insert(Index: Integer; Item: TPanelSplitter): Integer;
    property Items[Index: Integer]: TPanelSplitter read GetItem; default;
  end;

  TInternalSplitterDrawMode = (
    dmShow,
    dmMove,
    dmHide
  );

  TInternalUsedSizeFlag = (
    sfPanels,
    sfSplitters,
    sfHidden
  );
  TInternalUsedSizeFlags = set of TInternalUsedSizeFlag;

  TPanelSplitter = class (TGraphicControl)
  private
    fParentPanel    : TSplitPanel;
    fSecondPanel    : TSplitPanel;
    fFirstPanel     : TSplitPanel;
    fHighlightColor : TColor;
    fSplitMode      : TSplitMode;
    fDragInfo       : TDragInfo;
    fDisabled       : Boolean;
    fStep           : Integer;
    fLiveResize     : Boolean;
    fOldKeyDownEvent: TKeyEvent;
    fControl: TWinControl;
  { lijntjes }
    fLineDC         : HDC; // handle om lijn op te tekenen
    fBrush          : TBrush; // brush voor lijn
    fPrevBrush      : HBrush; // used brush
    fLineVisible    : Boolean;
    procedure CMMouseEnter(var Msg:TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Msg:TMessage); message CM_MOUSELEAVE;
    function GetSplitMode: TSplitMode;
    procedure SetSplitMode(const Value: TSplitMode);
    function GetIndex: Integer;
    function FindPanels: Boolean;
    procedure AllocLineDC;
    procedure ReleaseLineDC;
    procedure DrawLine(aMode: TInternalSplitterDrawMode);
    procedure HandleKey(Sender: TObject; var Key: Word; Shift: TShiftState);
  protected
    procedure DoResizePanel(X, Y: Integer);
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure SetParent(aControl: TWinControl); override;
    property SplitMode: TSplitMode read fSplitMode write SetSplitMode;
    property Index: Integer read GetIndex;
    property IsDragging: Boolean read fDragInfo.iDragging;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property FirstPanel: TSplitPanel read fFirstPanel;
    property SecondPanel: TSplitPanel read fSecondPanel;
    property Disabled: Boolean read fDisabled write fDisabled;

  published
    property Color;
    property HighlightColor: TColor read fHighlightColor write fHighlightColor;
    property LiveResize: Boolean read fLiveResize write fLiveResize;
  end;

  TSplitterParams = class(TPersistent) // nog niet in gebruik
  private
    fHighlightColor: TColor;
    fOnChange: TNotifyEvent;
    fHighlightWidth: Integer;
    procedure SetHighlightColor(const Value: TColor);
    procedure SetHighlightWidth(const Value: Integer);
    procedure Changed;
  protected
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  public
  published
    property HighlightColor: TColor read fHighlightColor write SetHighlightColor;
    property HighlightWidth: Integer read fHighlightWidth write SetHighlightWidth;
  end;

  // interne events
  TGetEvent = function: Integer of object;
  TSetEvent = procedure(Value: Integer) of object;
  TGetConstraintSizeEvent = function: TConstraintSize of object;
  TSetConstraintSizeEvent = procedure(Value: TConstraintSize) of object;

  TSplitPanel = class(TCustomControl)
  private
    fAfterCalculate       : TNotifyEvent;
    fAlignment            : TAlignment;            // alleen valide voor root-panel
    fBorderStyle          : TBorderStyle;
    fCalculatedSize       : Integer;
    fDistance             : Integer;
    fDivideMode           : TDivideMode;           // hoe verdelen we de panels
    fLiveSizing           : Boolean;               // live slepen met splitters
    fOldConstraintsEvent  : TNotifyEvent;          // save onchange event van constraints
    fOptions              : TSplitPanelOptions;
    fPaintLock            : Integer;
    fPanelCount           : Integer;               // aantal panels (nu nog Get...)
    fPanels               : TSplitPanelList;       // lijst van panels
    fParentPanel          : TSplitPanel;
    fRealBevelInner       : TBevelCut;
    fRealBevelKind        : TBevelKind;
    fRealBevelOuter       : TBevelCut;
    fRealBevelWidth       : TBevelWidth;
    fRealBorderStyle      : TBorderStyle;
    fRealConstraints      : TSizeConstraints;
    fResizeList           : TSplitPanelList;       // volgorde resize (1.none 2.bottom 3.top)
    fResizeStep           : Integer;               // nog niet goed
    fResizeStepStart      : Integer;               // voor berekenen resize
    fScaleOnResize        : Boolean;               // behoud schaal
    fShowCaption          : Boolean;
    fSplitMode            : TSplitMode;
    fSplitPosition        : TSplitPosition;        // boven, onder, none
    fSplitterCount        : Integer;               // aantal splitters (nu nog Get...)
    fSplitters            : TPanelSplitterList;    // lijst van splitters om de subpanels te resizen
    fState                : TPanelState;
    fTemplateName         : string;
    fUpdateCount          : Integer;
    fUpdateRect           : TRect;
    fVisiblePanelCount    : Integer;               // nu nog Get...
    fVisiblePanels        : TSplitPanelList;       // lijst van huidige zichtbare panels
    fVisibleSplitterCount : Integer;               // nu nog Get...
    fVisibleSplitters     : TPanelSplitterList;    // lijst van huidige zichtbare splitters
  { interne richtingonafhankelijke events }
    EvGetSize             : TGetEvent;
    EvSetSize             : TSetEvent;
    EvGetPosition         : TGetEvent;
    EvSetPosition         : TSetEvent;
    EvGetMinSize          : TGetConstraintSizeEvent;
    EvSetMinSize          : TSetConstraintSizeEvent;
    EvGetMaxSize          : TGetConstraintSizeEvent;
    EvSetMaxSize          : TSetConstraintSizeEvent;
    EvGetRealMinSize      : TGetConstraintSizeEvent;
    EvGetRealMaxSize      : TGetConstraintSizeEvent;
  { tijdelijke herberekening }
    fOldPosition          : Integer;               // alleen gebruikt bij herberekening
    fOldSize              : Integer;               // idem
    fNewPosition          : Integer;               // idem
    fNewSize              : Integer;               // idem
    fOldBounds            : TRect;                 // alleen gebruikt in SetBounds en AlignControls
    fBoundsBeforeDrag     : TRect;                 // dragsave
    fCurrentScale         : Double;                // verhouding in parentpanel
  { interne method events }
    function InternalGetWidth: Integer;
    procedure InternalSetWidth(Value: Integer);
    function InternalGetLeft: Integer;
    procedure InternalSetLeft(Value: Integer);
    function InternalGetHeight: Integer;
    procedure InternalSetHeight(Value: Integer);
    function InternalGetTop: Integer;
    procedure InternalSetTop(Value: Integer);
    function InternalGetMinHeight: TConstraintSize;
    procedure InternalSetMinHeight(Value: TConstraintSize);
    function InternalGetMaxHeight: TConstraintSize;
    procedure InternalSetMaxHeight(Value: TConstraintSize);
    function InternalGetMinWidth: TConstraintSize;
    procedure InternalSetMinWidth(Value: TConstraintSize);
    function InternalGetMaxWidth: TConstraintSize;
    procedure InternalSetMaxWidth(Value: TConstraintSize);
    function InternalGetRealMinWidth: TConstraintSize;
    function InternalGetRealMaxWidth: TConstraintSize;
    function InternalGetRealMinHeight: TConstraintSize;
    function InternalGetRealMaxHeight: TConstraintSize;
  { interne methods }
    procedure BeginUpdate;
    procedure CheckParentAutoSize;
    procedure ClearBorder;
    procedure EndUpdate(Direct: Boolean = False);
    function GetUsedSize(Flags: TInternalUsedSizeFlags = [sfPanels, sfSplitters]): Integer;
    function InternalCheckAutoSize(var NewWidth, NewHeight: Integer): Boolean;
    procedure InternalCheckConstraints(var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
    function IsUpdating: Boolean;
    procedure NewConstraintsOnChange(Sender: TObject);
    procedure PaintLock;
    procedure PaintUnlock;
    procedure ParentUpdating(aUpdate: Boolean);
    procedure ResetBorder;
    procedure ResetMethods(aParentSplitMode: TSplitMode);
    procedure SplitterDrag(Sender: TPanelSplitter; Delta: Integer);
    procedure SplitterDragBegin(Sender: TPanelSplitter);
    procedure SplitterDragEnd(Sender: TPanelSplitter);
    procedure SplitterDragCancel(Sender: TPanelSplitter);
  { main update routines }
    procedure UpdateLists; // main
    procedure UpdatePanelOrder; { a }
    procedure UpdateSplitterList; { b }
    procedure UpdateVisiblePanelList; { c }
    procedure UpdateVisibleSplitterList; { d }
    procedure UpdateSplitterRefs; { e }
    procedure UpdateRealConstraints; { f }
    procedure UpdateResizeList; { g }
  { andere update routines }
    procedure RecalcScales(DoClear: Boolean = False);
  { property access }
    procedure DoChildren(aCommand: TChildCommand);
    function GetBevelInner: TBevelCut;
    function GetBevelKind: TBevelKind;
    function GetBevelOuter: TBevelCut;
    function GetBevelWidth: TBevelWidth;
    function GetBorderStyle: TBorderStyle;
    function GetFixedSize: Integer;
    function GetIndex: Integer;
    function GetLevel: Integer;
    function GetMaxSize: TConstraintSize;
    function GetMinSize: TConstraintSize;
    function GetMode: TPanelMode;
    function GetNextPanel: TSplitPanel;
    function GetNextVisiblePanel: TSplitPanel;
    function GetPanel(Index: Integer): TSplitPanel;
    function GetPanelCount: Integer;
    function GetPosition: Integer;
    function GetPrevPanel: TSplitpanel;
    function GetPrevVisiblePanel: TSplitPanel;
    function GetRealMaxSize: TConstraintSize;
    function GetRealMinSize: TConstraintSize;
    function GetQuickBorder: TQuickBorderStyle;
    function GetRoot: TSplitPanel;
    function GetSize: Integer;
    function GetSplitterCount: Integer;
    function GetVisibleIndex: Integer;
    function GetVisiblePanel(Index: Integer): TSplitPanel;
    function GetVisiblePanelCount: Integer;
    function GetVisibleSplitterCount: Integer;
    procedure SetAlignment(const Value: TAlignment);
    procedure SetBevelInner(const Value: TBevelCut);
    procedure SetBevelKind(const Value: TBevelKind);
    procedure SetBevelOuter(const Value: TBevelCut);
    procedure SetBevelWidth(const Value: TBevelWidth);
    procedure SetBorderStyle(const Value: TBorderStyle);
    procedure SetDistance(const Value: Integer);
    procedure SetDivideMode(const Value: TDivideMode);
    procedure SetFixedSize(const Value: Integer);
    procedure SetMaxSize(Value: TConstraintSize);
    procedure SetMinSize(Value: TConstraintSize);
    procedure SetOptions(const Value: TSplitPanelOptions);
    procedure SetPanelCount(Value: Integer);
    procedure SetPosition(const Value: Integer);
    procedure SetQuickBorder(const Value: TQuickBorderStyle);
    procedure SetResizeStep(Value: Integer);
    procedure SetScaleOnResize(const Value: Boolean);
    procedure SetShowCaption(const Value: Boolean);
    procedure SetSize(const Value: Integer);
    procedure SetSplitMode(const Value: TSplitMode);
    procedure SetSplitPosition(const Value: TSplitPosition);
    procedure SetTemplateName(const Value: string);
  { vcl + win messages }
    procedure CMDesignHitTest(var Message: TCMDesignHitTest); message CM_DESIGNHITTEST;
    procedure CMBorderChanged(var Message: TMessage); message CM_BORDERCHANGED;
    procedure CMControlChange(var Message: TCMControlChange); message CM_CONTROLCHANGE;
    procedure CMCtl3DChanged(var Message: TMessage); message CM_CTL3DCHANGED;
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
    procedure WMEraseBkgnd(var Msg: TWmEraseBkgnd); message WM_ERASEBKGND;
    procedure WMPaint(var Msg: TWMPaint); message WM_PAINT;
    procedure WMNCPaint(var Message: TMessage{RealWMNCPaint}); message WM_NCPAINT;
    procedure WMWindowPosChanging(var Msg: TWMWindowPosMsg); message WM_WINDOWPOSCHANGING;
    procedure WMWindowPosChanged(var Message: TWMWindowPosChanged); message WM_WINDOWPOSCHANGED;
    procedure SetLiveSizing(const Value: Boolean);
  protected
  { protected overrides }
    procedure AlignControls(AControl: TControl; var Rect: TRect); override;
    function CanAutoSize(var NewWidth, NewHeight: Integer): Boolean; override;
    procedure ConstrainedResize(var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer); override;
    procedure CreateParams(var Params: TCreateParams); override;
    function GetClientRect: TRect; override;
    procedure Loaded; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure PaintPanel(aCanvas: TCanvas; const aWindow: TRect);
    procedure RequestAlign; override;
    procedure SetAutoSize(Value: Boolean); override;
    procedure SetBounds(aLeft, aTop, aWidth, aHeight: Integer); override;
    procedure SetParent(aControl: TWinControl); override;
    procedure VisibleChanging; override;
  { protected nieuw }
    procedure AddPanel(P: TSplitPanel);
    procedure AddSplitter(S: TPanelSplitter);
    procedure ForEachPanel(LocalProc: Pointer; BackWards: Boolean = False);
    procedure ForEachVisiblePanel(LocalProc: Pointer; BackWards: Boolean = False);
    procedure RemovePanel(P: TSplitPanel);
    procedure RemoveSplitter(S: TPanelSplitter);
    property CalculatedSize: Integer read fCalculatedSize;
//    property RealConstraints: TSizeConstraints read fRealConstraints;
    property RealMinSize: TConstraintSize read GetRealMinSize;
    property RealMaxSize: TConstraintSize read GetRealMaxSize;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function CanPaint: Boolean; // private maken
    function CheckSize(aSize: Integer): Integer; // private maken
//    procedure
    function HasControls: Boolean;
    property Index: Integer read GetIndex;
    property VisibleIndex: Integer read GetVisibleIndex; // private maken
    property Level: Integer read GetLevel;
    property Mode: TPanelMode read GetMode;
    function NewPanel: TSplitPanel;
    property NextPanel: TSplitPanel read GetNextPanel; // private maken
    property NextVisiblePanel: TSplitPanel read GetNextVisiblePanel; // private maken
    property Panels[Index: Integer]: TSplitPanel read GetPanel; default;
    property VisiblePanels[Index: Integer]: TSplitPanel read GetVisiblePanel; // private maken
    property Position: Integer read GetPosition write SetPosition;
    property PrevPanel: TSplitpanel read GetPrevPanel; // private maken
    property PrevVisiblePanel: TSplitPanel read GetPrevVisiblePanel; // private maken
    procedure Recalculate(aControl: TControl; Info: TCalcInfo; Delta: Integer = 0); // private maken
    procedure ResetToDefault; // private maken
    property ResizeStepStart: Integer read fResizeStepStart; // private maken
    property Root: TSplitPanel read GetRoot;
    property SplitterCount: Integer read GetSplitterCount; // private maken
    property VisiblePanelCount: Integer read GetVisiblePanelCount; // private maken
    property VisibleSplitterCount: Integer read GetVisibleSplitterCount; // private maken
    property ParentPanel: TSplitPanel read fParentPanel;
    procedure Flip;
  published
  { inherited properties }
    property Align;
    property AutoSize;
    property BorderWidth;
    property Caption;
    property Color;
    property Constraints;
    property Ctl3d;
    property DockSite;
    property Font;
    property Height;
    property UseDockManager;
    property Visible;
    property Width;
  { aangepaste properties }
    property BorderStyle: TBorderStyle read GetBorderStyle write SetBorderStyle;
    property BevelKind: TBevelKind read GetBevelKind write SetBevelKind default bkTile;
    property BevelInner: TBevelCut read GetBevelInner write SetBevelInner default bvNone;
    property BevelOuter: TBevelCut read GetBevelOuter write SetBevelOuter default bvLowered;
    property BevelWidth: TBevelWidth read GetBevelWidth write SetBevelWidth default 1;
  { nieuwe properties }
    property Alignment: TAlignment read fAlignment write SetAlignment default taCenter;
    property ShowCaption: Boolean read fShowCaption write SetShowCaption;
    property Size: Integer read GetSize write SetSize stored False;
    property SplitPosition: TSplitPosition read fSplitPosition write SetSplitPosition;
    property MaxSize: TConstraintSize read GetMaxSize write SetMaxSize stored False;
    property MinSize: TConstraintSize read GetMinSize write SetMinSize stored False;
    property FixedSize: Integer read GetFixedSize write SetFixedSize stored False;
    property Distance: Integer read fDistance write SetDistance default 4;
    property LiveSizing: Boolean read fLiveSizing write SetLiveSizing default True;
    property SplitMode: TSplitMode read fSplitMode write SetSplitMode;
    property PanelCount: Integer read GetPanelCount write SetPanelCount stored False;
    property RealConstraints: TSizeConstraints read fRealConstraints stored False;
    property DivideMode: TDivideMode read fDivideMode write SetDivideMode;
    property ScaleOnResize: Boolean read fScaleOnResize write SetScaleOnResize;
    property AfterCalculate: TNotifyEvent read fAfterCalculate write fAfterCalculate;
    property Options: TSplitPanelOptions read fOptions write SetOptions default DEF_OPTIONS;
    property ResizeStep: Integer read fResizeStep write SetResizeStep default 0;
    property TemplateName: string read fTemplateName write SetTemplateName;// stored False;
    property QuickBorder: TQuickBorderStyle read GetQuickBorder write SetQuickBorder stored False;
  end;

  TRootPanel = class(TSplitPanel)
  private
  protected
  public
  published
  end;

  TTemplateMgr = class(TClassFactory)
  private
  protected
  public
    procedure ActivateTemplate(aSplitPanel: TSplitPanel; const aName: string);
  published
  end;

  TCustomSplitPanelTemplateClass = class of TCustomSplitPanelTemplate;
  TCustomSplitPanelTemplate = class
  private
  protected
    class procedure SetDefaults(P: TSplitPanel); virtual;
  public
  published
  end;

  TSplitPanelTemplate01 = class(TCustomSplitPanelTemplate)
  protected
    class procedure SetDefaults(P: TSplitPanel); override;
  public
  end;

  TSplitPanelTemplate02 = class(TCustomSplitPanelTemplate)
  protected
    class procedure SetDefaults(P: TSplitPanel); override;
  public
  end;

function TemplateMgr: TTemplateMgr;
                  
implementation


{$ifdef dolog}
uses
  Ulog, Types;

procedure Log(const AValues: array of const; Dlg: boolean = False);
begin
  ulog.log(avalues);
end;
{$endif}

var
  _TemplateMgr: TTemplateMgr;
  UserCount: Integer = 0;
  PanelBitmap: TBitmap;

//const
  //RESIZE_TIMER:

function OppMode(aMode: TSplitMode): TSplitMode;
begin
  if aMode = smVertical then
    Result := smHorizontal
  else
    Result := smVertical;  
end;


procedure Frame3D(Canvas: TCanvas; var Rect: TRect; TopColor, BottomColor: TColor;
  Width: Integer);

  procedure DoRect;
  var
    TopRight, BottomLeft: TPoint;
  begin
    with Canvas, Rect do
    begin
      TopRight.X := Right;
      TopRight.Y := Top;
      BottomLeft.X := Left;
      BottomLeft.Y := Bottom;
      Pen.Color := TopColor;
      PolyLine([BottomLeft, TopLeft, TopRight]);
      Pen.Color := BottomColor;
      Dec(BottomLeft.X);
      PolyLine([TopRight, BottomRight, BottomLeft]);
    end;
  end;

begin
  Canvas.Pen.Width := 1;
  Dec(Rect.Bottom); Dec(Rect.Right);
  while Width > 0 do
  begin
    Dec(Width);
    DoRect;
    InflateRect(Rect, -1, -1);
  end;
  Inc(Rect.Bottom); Inc(Rect.Right);
end;


procedure UsesBitmaps;
var
  M: Cardinal;
begin
  if UserCount = 0 then
    PanelBitmap := TBitmap.Create;
  Inc(UserCount);
end;

procedure ReleaseBitmaps;
begin
  Dec(UserCount);
  if UserCount = 0 then
    PanelBitmap.Free;
end;

procedure BitmapResize(aBitmap: TBitmap; aWidth, aHeight: Integer);
begin
  with aBitmap do
  begin
    Width := Max(Width, aWidth);
    Height := Max(Height, aHeight);
  end;
end;

procedure CopyCanvas(Target: TCanvas; const TargetRect: TRect; Source: TCanvas; const SourceRect: TRect);
{ simpel kopieren }
begin
  with TargetRect do
    BitBlt(Target.Handle,
           Left, Top, Right - Left, Bottom - Top,
           Source.Handle,
           SourceRect.Left, SourceRect.Top,
           cmSrcCopy);
end;

type
  TCalcAction = (
    caNone,
    caDefault,        // default verdeling panels
    caScalePanels,    // schalen
    caEqualDivide,    // gelijke verdeling panels
    caSplitter,       // splitter resize
    caAutoSize
  );


function TemplateMgr: TTemplateMgr;
begin
  if _TemplateMgr = nil then
    _TemplateMgr := TTemplateMgr.Create(TCustomSplitPanelTemplate);
  Result := _TemplateMgr;
end;

function RandomColor: TColor;
begin
  Result := RGB(Random(255), Random(255), Random(255));
end;

function RandomCol(Ix: Integer): TColor;
begin
  case Ix of
    0: Result := clRed;
    1: Result := clBlue;
    2: Result := clYellow;
    3: Result := clGreen;
    4: Result := clFuchsia;
  else
    Result := clLime;
  end;
end;


function ComparePanelSizesLargeFirst(Item1, Item2: Pointer): Integer;
var
  A: TSplitPanel absolute Item1;
  B: TSplitPanel absolute Item2;
begin
  Result := A.Size - B.Size;
end;

function ComparePanelSizesSmallFirst(Item1, Item2: Pointer): Integer;
var
  A: TSplitPanel absolute Item1;
  B: TSplitPanel absolute Item2;
begin
  Result := B.Size - A.Size;
end;

{ TSplitPanelList }

function TSplitPanelList.Add(Item: TSplitPanel): Integer;
begin
  Assert(IndexOf(Item) = -1, 'SplitPanelList.Add error');
//  Result := -1;
  //if IndexOf(Item) >= 0 then
    //Exit;
  Result := inherited Add(Item);
end;

function TSplitPanelList.GetItem(Index: Integer): TSplitPanel;
begin
  Result := inherited Get(Index);
end;

function TSplitPanelList.Insert(Index: Integer; Item: TSplitPanel): Integer;
begin
  Result := -1;
  if IndexOf(Item) >= 0 then
    Exit;
  inherited Insert(Index, Item);
  Result := Index;
end;

procedure TSplitPanelList.SortBySize(SmallFirst: Boolean);
begin
  if SmallFirst then
    Sort(ComparePanelSizesSmallFirst)
  else
    Sort(ComparePanelSizesLargeFirst)
end;

{ TPanelSplitterList }

function TPanelSplitterList.Add(Item: TPanelSplitter): Integer;
begin
  Assert(IndexOf(Item) = -1, 'PanelSplitterList.Add error');
  //Result := -1;
//  if IndexOf(Item) >= 0 then
  //  Exit;
  Result := inherited Add(Item);
end;

function TPanelSplitterList.GetItem(Index: Integer): TPanelSplitter;
begin
  Result := inherited Get(Index);
end;

function TPanelSplitterList.Insert(Index: Integer; Item: TPanelSplitter): Integer;
begin
  inherited Insert(Index, Item);
  Result := Index;
end;

{ TPanelSplitter }

procedure TPanelSplitter.CMMouseEnter(var Msg: TMessage);
begin
  if not fDisabled then
    if fParentPanel <> nil then
      if not (csDesigning in fParentPanel.ComponentState) then
        if fParentPanel.DivideMode <> dmEqual then
        begin
          Color := fHighlightColor;
        end;
  inherited;
end;

procedure TPanelSplitter.CMMouseLeave(var Msg: TMessage);
begin
  if fParentPanel <> nil then
    Color := fParentPanel.Color;
  inherited;
end;

constructor TPanelSplitter.Create(aOwner: TComponent);
begin
  inherited;
  ControlStyle := ControlStyle + [csCaptureMouse];
  fHighlightColor := clYellow;
  fStep := 1;
  fLiveResize := True;
end;


function TPanelSplitter.FindPanels: Boolean;
begin
  Result := Assigned(fFirstPanel) and Assigned(fSecondPanel);
end;

function TPanelSplitter.GetIndex: Integer;
begin
  Result := -1;
  if fParentPanel = nil then
    Exit;
  Result := fParentPanel.fSplitters.IndexOf(Self);
end;

function TPanelSplitter.GetSplitMode: TSplitMode;
begin
  Result := smVertical;
  if Parent is TSplitPanel then
    Result := TSplitPanel(Parent).SplitMode;
end;

procedure TPanelSplitter.DoResizePanel(X, Y: Integer);
begin
  with fDragInfo do
  begin
    case fSplitMode of
      smVertical:
        begin
          if Abs(Y - iStartY) >= fStep then
            fParentPanel.SplitterDrag(Self, Y - iStartY);
        end;
      smHorizontal:
        begin
          if Abs(X - iStartX) >= fStep then
            fParentPanel.SplitterDrag(Self, X - iStartX);
        end;
    end;
  end;
end;

type
  THackedWinControl = class(TWinControl);

procedure TPanelSplitter.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  F: TCustomForm;
begin
  inherited;
  if not FindPanels then
    Exit;
  if fDisabled then Exit;
  with fDragInfo do
  begin

    iCurrentX := X;
    iCurrentY := Y;
    iStartX := X;
    iStartY := Y;
    iDragging := True;

    F := GetParentForm(Self);
    if F <> nil then
    begin
      fControl := F.ActiveControl;
      if fControl <> nil then
      begin
        fOldKeyDownEvent := THackedWinControl(fControl).OnKeyDown;
        THackedWinControl(fControl).OnKeyDown := HandleKey;
      end;
    end;
    fParentPanel.SplitterDragBegin(Self);

    if not fLiveResize then
    begin
      AllocLineDC;
      Drawline(dmShow);
    end;
  end;
end;

procedure TPanelSplitter.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if fDisabled or not FindPanels then
    Exit;

  with fDragInfo do
  begin
    iCurrentX := X;
    iCurrentY := Y;
    if iDragging and (Shift = [ssLeft]) then
    begin
      if not fLiveResize then
      begin
        DrawLine(dmMove);
      end
      else if fLiveResize then
      begin
        DoResizePanel(X, Y);
      end;
    end;
  end;
end;

procedure TPanelSplitter.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  with fDragInfo do
  begin
    iCurrentX := X;
    iCurrentY := Y;
    if not iDragging then Exit;
    fDragInfo.iDragging := False;
    if not fLiveResize then
    begin
      DrawLine(dmHide);
      ReleaseLineDC;
      DoResizePanel(X, Y);
        //Color := fParentPanel.Color;themes   gui_fcard extctrls  umx  extctrls
    end;
    fParentPanel.SplitterDragEnd(Self);

  end;
  inherited;
end;

procedure TPanelSplitter.Paint;
var
  R: TRect;
begin
  with Canvas do
  begin
    if fParentPanel = nil then Exit;
//    if fParentPanel.IsUpdating then Exit;

    R := ClientRect;
    Brush.Color := fParentPanel.Color;
    FillRect(R);
    case fParentPanel.SplitMode of
      smVertical   : RectGrow(R, 0, -1);
      smHorizontal : RectGrow(R, -1, 0);
    end;
    Brush.Color := Self.Color;
    FillRect(R);
  end;
end;

procedure TPanelSplitter.SetParent(aControl: TWinControl);
{-------------------------------------------------------------------------------
  notify parentpanel
-------------------------------------------------------------------------------}
var
  OldParent: TWinControl;
begin
  OldParent := Parent;

  inherited;

  if OldParent = Parent then
    Exit;

  if aControl is TSplitPanel then
  begin
    fParentPanel := TSplitPanel(aControl);
    fParentPanel.AddSplitter(Self);
  end
  else begin
    if fParentPanel <> nil then
      fParentPanel.RemoveSplitter(Self);
    fParentPanel := nil;
  end;
end;

procedure TPanelSplitter.SetSplitMode(const Value: TSplitMode);
begin
  fSplitMode := Value;
  if not fDisabled then
    case fSplitMode of
      smVertical   : Cursor := crSizeNS;
      smHorizontal : Cursor := crSizeWE
    end
  else
    Cursor := crDefault;
end;

procedure TPanelSplitter.AllocLineDC;
{-------------------------------------------------------------------------------
  maak dc voor xor-lijntje (teken op dc van parent)
-------------------------------------------------------------------------------}
begin
  fLineDC := GetDCEx(Parent.Handle, 0, DCX_CACHE or DCX_CLIPSIBLINGS or DCX_LOCKWINDOWUPDATE);
  if FBrush = nil then
  begin
    fBrush := TBrush.Create;
    fBrush.Bitmap := AllocPatternBitmap(clBlack, clYellow{White});
  end;
  fPrevBrush := SelectObject(fLineDC, fBrush.Handle);
end;

procedure TPanelSplitter.ReleaseLineDC;
{-------------------------------------------------------------------------------
  geef dc voor xor-lijntje vrij
-------------------------------------------------------------------------------}
begin
  if fPrevBrush <> 0 then
    SelectObject(fLineDC, fPrevBrush);
  ReleaseDC(Parent.Handle, fLineDC);
  if fBrush <> nil then
  begin
    fBrush.Free;
    fBrush := nil;
  end;
end;

procedure TPanelSplitter.DrawLine(aMode: TInternalSplitterDrawMode);
{-------------------------------------------------------------------------------
  teken xor-lijn tijdens slepen
-------------------------------------------------------------------------------}
var
  P: TPoint;
begin
  with fDragInfo do
  begin
    P := Point(iCurrentX + Left - iStartX + 1, iCurrentY + Top - iStartY + 1);
    case fSplitMode of
      smVertical:
        begin
          case aMode of
            dmShow:
              begin
                PatBlt(fLineDC, Left, P.Y, Width, 2, PATINVERT);
              end;
            dmMove:
              begin
                PatBlt(fLineDC, Left, iDrawY, Width, 2, PATINVERT);
                PatBlt(fLineDC, Left, P.Y, Width, 2, PATINVERT);
              end;
            dmHide:
              begin
                PatBlt(fLineDC, Left, iDrawY, Width, 2, PATINVERT);
              end;
          end;
        end;
      smHorizontal:
        begin
          case aMode of
            dmShow:
              begin
                PatBlt(fLineDC, P.X, Top, 2, Height, PATINVERT);
              end;
            dmMove:
              begin
                PatBlt(fLineDC, iDrawX, Top, 2, Height, PATINVERT);
                PatBlt(fLineDC, P.X, Top, 2, Height, PATINVERT);
              end;
            dmHide:
              begin
                PatBlt(fLineDC, P.X, Top, 2, Height, PATINVERT);
              end;
          end;    
        end;
     end;
    iDrawX := P.X;
    iDrawY := P.Y;
  end;
  fLineVisible := not fLineVisible;
end;

destructor TPanelSplitter.Destroy;
begin
  fBrush.Free;
  inherited;
end;

procedure TPanelSplitter.HandleKey(Sender: TObject; var Key: Word; Shift: TShiftState);
{-------------------------------------------------------------------------------
  tijdelijke "geleende" key event van activecontrol (linke soep)
-------------------------------------------------------------------------------}
begin
  if key = vk_escape then
  begin
    if fControl <> nil then
      THackedWinControl(fControl).OnKeyDown := fOldKeyDownEvent;
    fDragInfo.iDragging := False;
    fParentPanel.SplitterDragCancel(Self);
  end;
end;

{ TSplitterParams }

procedure TSplitterParams.Changed;
begin
  if Assigned(fOnChange) then fOnChange(Self);
end;

procedure TSplitterParams.SetHighlightColor(const Value: TColor);
begin
  fHighlightColor := Value;
end;

procedure TSplitterParams.SetHighlightWidth(const Value: Integer);
begin
  fHighlightWidth := Value;
end;

{ TSplitPanel }

constructor TSplitPanel.Create(aOwner: TComponent);

  function GetName: string;
  var
    i, Ix: Integer;
  begin
    Result := '';
    ix := 1;
    if aOwner <> nil then
      for i := 0 to aOwner.ComponentCount - 1 do
      begin
        if aOwner.Components[i] is TSplitPanel then
        begin
          Inc(Ix);
        end;
      end;
    Result := 'SplitPanel' + i2s(Ix);
  end;

begin
  UsesBitmaps;
  inherited Create(aOwner);
//  Name := GetName;
  fRealConstraints := TSizeConstraints.Create(nil);
  fOldConstraintsEvent := Constraints.OnChange;
  Constraints.OnChange := NewConstraintsOnChange;
  ControlStyle := [csAcceptsControls, csCaptureMouse, csClickEvents,
    csSetCaption, csOpaque, csDoubleClicks, csReplicatable];
  if ThemeServices.ThemesEnabled then
    ControlStyle := ControlStyle + [csParentBackground] - [csOpaque];
  DoubleBuffered := True;
  ResetMethods(smVertical); // voor de zekerheid
  fPanels := TSplitPanelList.Create;
  fVisiblePanels := TSplitPanelList.Create;
  fResizeList := TSplitPanelList.Create;
  fSplitters := TPanelSplitterList.Create;
  fVisibleSplitters := TPanelSplitterList.Create;
  fLiveSizing := True;

  Align := alNone;
  fOptions := DEF_OPTIONS;

  BevelKind := bktile;
  BevelInner := bvNone;
  BevelOuter := bvLowered;
  BevelWidth := 1;

  fRealBevelKind := bkTile;
  fRealBevelInner := bvNone;
  fRealBevelOuter := bvLowered;
  fRealBevelWidth := 1;

  fBorderStyle := bsNone;
  fRealBorderStyle := bsNone;

  fDistance := 4;
  fResizeStep := 0;
  with Constraints do
  begin
    MinWidth := DEF_CONSTRAINTSIZE;
    MinHeight := DEF_CONSTRAINTSIZE;
  end;
  Width := 200;
  Height := 200;
end;

procedure TSplitPanel.ResetToDefault;
begin
  if PanelCount > 0 then
    PanelCount := 0;

  Align := alNone;

  AutoSize := False;
  Options := DEF_OPTIONS;

  BevelKind := bktile;
  BevelInner := bvNone;
  BevelOuter := bvLowered;
  BevelWidth := 1;

  fRealBevelKind := bkTile;
  fRealBevelInner := bvNone;
  fRealBevelOuter := bvLowered;
  fRealBevelWidth := 1;
  fBorderStyle := bsNone;
  fRealBorderStyle := bsNone;
  PERFORM(CM_BORDERCHANGED, 0, 0);

  Distance := 4;
  ResizeStep := 0;

  with Constraints do
  begin
    MinWidth := 10;
    MinHeight := 10;
  end;
  Width := 200;
  Height := 200;
end;

destructor TSplitPanel.Destroy;
var
  i: Integer;
begin
  try
    fPanels.Free;
    fResizeList.Free;
    fVisiblePanels.Free;
    fSplitters.Free;
    fVisibleSplitters.Free;
    if fParentPanel <> nil then
      fParentPanel.RemovePanel(Self);
    fRealConstraints.Free;
    inherited;
  finally
    ReleaseBitmaps;
  end;
end;

procedure TSplitPanel.AddPanel(P: TSplitPanel);
var
  Ix: Integer;
begin
  if csDestroying in ComponentState then
    Exit;
  BeginUpdate;
  try
    fPanels.Add(P);
    fPanelCount := fPanels.Count;
    UpdateLists;
    if not (csReading in ComponentState) then
    if not (csLoading in ComponentState) then
    begin
      P.BevelKind := fRealBevelKind;
      P.BorderStyle := fRealBorderStyle;
    end;
    if PanelCount = 1 then
      ClearBorder;
  finally
    EndUpdate;
    if not IsUpdating then
      Recalculate(nil, riPanelAdded);
  end;
end;

procedure TSplitPanel.RemovePanel(P: TSplitPanel);
begin
  if csDestroying in ComponentState then
    Exit;
  BeginUpdate;
  try
    fPanels.Remove(P);
    fPanelCount := fPanels.Count;
    UpdateLists;
//    RefreshSplitters;
    if PanelCount = 0 then
      ResetBorder;
  finally
    EndUpdate;
    if not IsUpdating then
      Recalculate(nil, riNone{P, riPanelAdded});
    //Invalidate;
  end;
end;

procedure TSplitPanel.AddSplitter(S: TPanelSplitter);
begin
  if csDestroying in ComponentState then
    Exit;
  fSplitters.Add(S);
  fSplitterCount := fSplitters.Count;
end;

procedure TSplitPanel.RemoveSplitter(S: TPanelSplitter);
begin
  if csDestroying in ComponentState then
    Exit;
  fSplitters.Remove(S);
  fSplitterCount := fSplitters.Count;
end;

function TSplitPanel.NewPanel: TSplitPanel;
var
  S: TPanelSplitter;
  Root: TSplitPanel;
  TheOwner: TComponent;
begin
  Result := nil;
  BeginUpdate;
  try
    TheOwner := nil;
    Root := GetRoot;
    if Root <> nil then
      TheOwner := Root.Owner
    else
      TheOwner := Self;
    Result := TSplitPanel.Create(TheOwner);
    Result.Parent := Self;
  finally
    EndUpdate;
  if not IsUpdating then
    Recalculate(Result, riPanelAdded);
  end;
end;

procedure TSplitPanel.BeginUpdate;
begin
  Inc(fUpdateCount);
end;

procedure TSplitPanel.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);

  with Params do
  begin
    Style := Style or WS_CLIPCHILDREN or WS_CLIPSIBLINGS;

    // single border
    if fBorderStyle = bsSingle then
    begin
      if NewStyleControls and Ctl3D then
      begin
        Style := Style and not WS_BORDER;
        ExStyle := ExStyle or WS_EX_CLIENTEDGE;
      end
      else
        Style := Style or WS_BORDER;
    end;

//    WindowClass.style := WindowClass.style or CS_OWNDC;
//    WindowClass.style := WindowClass.style and not (CS_HREDRAW or CS_VREDRAW); // een wonderbaarlijke genezing!!!
  end;
end;

procedure TSplitPanel.EndUpdate(Direct: Boolean = False);
var
  C, R: TRect;
begin
  if fUpdateCount > 0 then
  begin
    Dec(fUpdateCount);
    if fUpdateCount = 0 then
    begin
     if not HandleAllocated then
       Exit;
//     Invalidate;
//      C := ClientRect;
  //    GetUpdateRect(Handle, R, False);
//      Windows.InvalidateRect(Handle, @R, False);

//      perform(wm_ncpaint, 0, 0);windows
//      Invalidate;
      //if Direct then Update;
    end;
  end;
end;

function TSplitPanel.GetPanel(Index: Integer): TSplitPanel;
begin
  if fPanels = nil then
    Result := nil
  else
    Result := fPanels[Index];
end;

function TSplitPanel.GetPanelCount: Integer;
begin
  if fPanels = nil then
    Result := 0
  else
    Result := fPanels.Count;
end;

function TSplitPanel.GetSplitterCount: Integer;
begin
  if fSplitters =nil then
    Result := 0
  else
    Result := fSplitters.Count;
end;

function TSplitPanel.GetClientRect: TRect;
begin
  if Parent <> nil then
    Result := inherited GetClientRect
  else
    Result := BoundsRect;
end;

procedure TSplitPanel.PaintPanel(aCanvas: TCanvas; const aWindow: TRect);
var
  R: TRect;
  S: string;
  C: TCanvas;
begin

  if poPaintBuffered in fOptions then
  begin

    C := PanelBitmap.Canvas;
    C.Lock;
    try
      BitmapResize(PanelBitmap, ClientWidth, ClientHeight);
      R := ClientRect;
      SetBkMode(C.Handle, OPAQUE);
      C.Brush.Color := Self.Color;
      C.Brush.Style := bsSolid;
      C.FillRect(R);

      if fShowCaption then
      begin
        S := Caption;
        if S <> '' then
        begin
          C.Font := Self.Font;
          SetBkMode(C.Handle, TRANSPARENT);
          DrawTextEx(C.Handle, PChar(S), Length(S), R, {DT_VCENTER + DT_SINGLELINE + DT_CENTER}0, nil);
        end;
      end;

      CopyCanvas(aCanvas, aWindow, C, aWindow);
    finally
      C.Unlock;
    end;

  end
  else begin
    C := aCanvas;
    R := ClientRect;
    SetBkMode(C.Handle, OPAQUE);
    C.Brush.Color := Self.Color;
    C.Brush.Style := bsSolid;
    C.FillRect(R);

    if fShowCaption then
    begin
      S := Caption;
      if S <> '' then
      begin
        C.Font := Self.Font;
        SetBkMode(C.Handle, TRANSPARENT);
        DrawTextEx(C.Handle, PChar(S), Length(S), R, {DT_VCENTER + DT_SINGLELINE + DT_CENTER}0, nil);
      end;
    end;
  end;

end;

procedure TSplitPanel.Paint;
var
  Window: TRect;
begin
  if psNoPaint in fState then
    Exit;
  if not IsRectEmpty(fUpdateRect) then
  begin
    Window := fUpdateRect;
    PaintPanel(Canvas, Window);
  end;
end;


function TSplitPanel.GetBorderStyle: TBorderStyle;
begin
  Result := fRealBorderStyle;
end;

procedure TSplitPanel.SetBorderStyle(const Value: TBorderStyle);
begin
  if fRealBorderStyle = Value then
    Exit;
//  fSaveBorderStyle := Value;
  fRealBorderStyle := Value;
  if PanelCount = 0 then
  begin
    fBorderStyle := Value;
    PERFORM(CM_BORDERCHANGED, 0, 0);
  end;
end;

procedure TSplitPanel.SetDistance(const Value: Integer);
begin
  if fDistance = Value then
    Exit;
  fDistance := Value;
  BeginUpdate;
//  PaintLock;
  try

    DoChildren(ccDistance);
    //Realign
  finally
    EndUpdate;
    if not IsUpdating then
      Recalculate(nil, riDistanceChanged);
  end;
end;

function TSplitPanel.IsUpdating: Boolean;
begin
  Result := fUpdateCount > 0;
//  if ParentPanel <> nil then
  //  Result := Result or ParentPanel.IsUpdating;
end;

procedure TSplitPanel.WMEraseBkgnd(var Msg: TWmEraseBkgnd);
begin
//  inherited;
  Msg.Result := 1; // geen erase background-onzin
end;


procedure TSplitPanel.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  Exit;
  if PanelCount = 0 then
  if fParentPanel <> nil then
    case Button of
      mbLeft:
        if fParentPanel.SplitMode = smVertical then
          Height := Height + 10
        else
          Width := Width + 10;
      mbRight:
        if fParentPanel.SplitMode = smVertical then
          Height := Height - 10
        else
          Width := Width - 10;
   end;
end;

procedure TSplitPanel.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited;
end;

procedure TSplitPanel.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
end;

procedure TSplitPanel.SetSplitMode(const Value: TSplitMode);
var
  i: Integer;
begin
  if fSplitMode = Value then Exit;
  fSplitMode := Value;
//  ResetMethods(fSplitMode); 
  for i := 0 to PanelCount - 1 do
    Panels[i].ResetMethods(fSplitMode);
  Recalculate(nil, riSplitModeChanged);
end;

procedure TSplitPanel.Flip;
begin
  if SplitMode = smVertical then
    SplitMode := smHorizontal
  else
    SplitMode := smVertical;
end;

procedure TSplitPanel.AlignControls(aControl: TControl; var Rect: TRect);
begin
  if IsUpdating then Exit;

  // check of we wel iets hoeven te doen todo: bij visiblechanged moet het wel
  // dus even uitgevlagd
{  if (aControl is TSplitPanel)
  and EqualRects(TSplitPanel(aControl).fOldBounds, TSplitPanel(aControl).BoundsRect) then
    Exit; }

  // check of de paintvlag uitkan

  Exclude(fState, psNoPaint);
  if poDisablePaintingWhenCovered in fOptions then
    if ControlCount = 1 then
      if Controls[0].Align = alClient then
      begin
        Include(fState, psNoPaint);
      end;



  if PanelCount = 0 then
    inherited AlignControls(aControl, Rect)
  else
    Recalculate(aControl, riAlignControls);

//  if ParentPanel <> nil then //##AI moet het nu zo??? nee!
  //  ParentPanel.Recalculate(aControl, riAlignControls);

end;

function TSplitPanel.GetIndex: Integer;
begin
  if fParentPanel = nil then
    Result := -1
  else
    Result := fParentPanel.fPanels.IndexOf(Self);
end;

function TSplitPanel.GetVisibleIndex: Integer;
begin
  if fParentPanel = nil then
    Result := -1
  else
    Result := fParentPanel.fVisiblePanels.IndexOf(Self);
end;

procedure TSplitPanel.SplitterDrag(Sender: TPanelSplitter; Delta: Integer);
begin
  Recalculate(Sender, riSplitterMoved, Delta);
end;

procedure TSplitPanel.Recalculate(aControl: TControl; Info: TCalcInfo; Delta: Integer = 0);
{-------------------------------------------------------------------------------
  De belangrijkste routine: het opnieuw uitrekenen van sizes en posities van
  panels n.a.v. sizing gebeurtenissen.
-------------------------------------------------------------------------------}

var
  Sender: TSplitPanel;
  VisCnt, Cnt: Integer;
  MySize, ClientSize, AvailablePanelSize, UsedPanelSize: Integer;
  TotalPanelSizesOnly: Integer;
  NeededAction: TCalcAction;

    procedure InitLocals;
    {-------------------------------------------------------------------------------
      Bepaal locale vars
      Bepaal NeededAction
    -------------------------------------------------------------------------------}
    var
      i: Integer;
      P: TSplitPanel;
      Loading: Boolean;
    begin
      Loading := (csLoading in ComponentState) or (csReading in ComponentState);
      NeededAction := caDefault;
      AvailablePanelSize := 0;
      UsedPanelSize := 0;
      ClientSize := 0;
      MySize := 0;
      TotalPanelSizesOnly := 0;


      // init positions
      for i := 0 to Cnt - 1 do
      begin
        P := Panels[i];
        P.ParentUpdating(True);
        P.fOldPosition := P.Position;
        P.fNewPosition := P.Position;
        P.fOldSize     := P.Size;
        P.fNewSize     := P.Size;
        if P.Visible then
          Inc(TotalPanelSizesOnly, P.fOldSize);
      end;

      VisCnt := VisiblePanelCount;
      UsedPanelSize := GetUsedSize([sfPanels]);

      case SplitMode of
        smVertical:
          begin
            ClientSize := ClientHeight;
            MySize := ClientWidth;
            AvailablePanelSize := ClientSize - (VisCnt - 1) * fDistance;
          end;
        smHorizontal:
          begin
            ClientSize := ClientWidth;
            MySize := ClientHeight;
            AvailablePanelSize := ClientSize - (VisCnt - 1) * fDistance;
          end;
      end;

      if (fDivideMode = dmEqual) then
        NeededAction := caEqualDivide
      else if not Loading and (Info = riPanelAdded) then
        NeededAction := caEqualDivide
      else if not Loading and (Info = riTemplateChanged) then
        NeededAction := caEqualDivide
      else if Info = riSplitModeChanged then
        NeededAction := caEqualDivide
      else if Info = riSplitterMoved then
        NeededAction := caSplitter
      else if not Loading and fScaleOnResize and (Sender = nil) then
        NeededAction := caScalePanels;
    end;

    procedure ExitLocals;
    var
      i: Integer;
      P: TSplitPanel;
    begin
      for i := 0 to Cnt - 1 do
      begin
        P := Panels[i];
        P.ParentUpdating(False);
      end;
    end;

    procedure ApplyChanges;
    {-------------------------------------------------------------------------------
      Zet de berekende bounds. De uiteindelijk bounds mogen niet afwijken van de
      berekende.
    -------------------------------------------------------------------------------}
    var
      i, N: Integer;
      P: TSplitPanel;
    begin
      N := 0;
      case SplitMode of
        smVertical:
          for i := 0 to VisCnt - 1 do
          begin
            P := VisiblePanels[i];
            P.SetBounds(0, N, MySize, P.fNewSize);
            Inc(N, P.Height + fDistance);
          end;
        smHorizontal:
          for i := 0 to VisCnt - 1 do
          begin
            P := VisiblePanels[i];
            P.SetBounds(N, 0, P.fNewSize, MySize);
            Inc(N, P.Width + fDistance);
          end;
      end;
    end;

    function Correct(ToDo: Integer; Scaled: Boolean = False): Integer;
    {-------------------------------------------------------------------------------
      Corrigeren van berekening.
      Result = aantal resterende pixels (zou altijd 0 moeten zijn).
      Wanneer Scaled dan wordt er eerst gesorteerd op grootte: bij groter worden
      worden de kleine panels het eerst gedaan en andersom.
      Als niet scaled wordt eerst het laatste panel aangepast en zo terug.
    -------------------------------------------------------------------------------}
    var
      i, Old: Integer;
      P: TSplitPanel;
      List: TSplitPanelList;
    begin
      Result := ToDo;
      if ToDo = 0 then
        Exit;
      List := TSplitPanelList.Create;
      List.Assign(fVisiblePanels);
      try
        // panels moeten groter worden
        if ToDo > 0 then
        begin
          if Scaled then
            List.SortBySize(False); // klein achteraan
          for i := List.Count - 1 downto 0 do
          begin
            P := List[i];
            Old := P.fNewSize;
            P.fNewSize := P.CheckSize(P.fNewSize + ToDo);
            Dec(ToDo, P.fNewSize - Old);
            if ToDo <= 0 then
              Break;
          end;
        end
        // panels moeten kleiner worden
        else if ToDo < 0 then begin
          if Scaled then
            List.SortBySize(True); // klein vooraan
          ToDo := -ToDo;
          for i := List.Count - 1 downto 0 do
          begin
            P := List[i];
            Old := P.fNewSize;
            P.fNewSize := P.CheckSize(Old - ToDo);
            Dec(ToDo, Old - P.fNewSize);
            if ToDo <= 0 then
              Break;
          end;
        end;
        Result := ToDo;
      finally
        List.Free;
      end;
    end;

    procedure HandleEqualDividePanels;
    {-------------------------------------------------------------------------------
       Bereken de sizes van alle panels in geval van gelijke verdeling.
       a) Verdeel het gemiddelde
       b) Corrigeer de overgebleven pixels, wanneer er dan nog pixels overblijven
          hebben we pech gehad :)
    -------------------------------------------------------------------------------}
    var
      i: Integer;
      Max: Integer;
      Average: Integer;
      Old, Used, ToDo: Integer;
      P: TSplitPanel;
    begin
      // verdeel het gemiddelde
      Average := AvailablePanelSize div VisCnt;
      Used := 0;
      for i := 0 to VisCnt - 1 do
      begin
        P := VisiblePanels[i];
        P.fNewSize := P.CheckSize(Average);
        Inc(Used, P.fNewSize);
      end;
      // corrigeer de afwijkingen
      Correct(AvailablePanelSize - Used);
    end;

    procedure HandleAdjustPanels;
    {-------------------------------------------------------------------------------
      Bereken de sizes van alle panels in geval van self-resize. Bottom - Top tries
    -------------------------------------------------------------------------------}
    var
      i: Integer;
      Average: Integer;
      Old, ToDo: Integer;
      P: TSplitPanel;
      L: TSplitPanelList;
      //todo: na een setbounds goed uitrekenen wat we moeten verschuiven!!!
    begin
      // bereken correctie
      ToDo := AvailablePanelSize - UsedPanelSize;
      if sender <> nil then
//      if name='f3' then Log(['adjust', todo, sender.index, sender.height]);
      if ToDo = 0 then
        Exit;

      // corrigeer
      if ToDo > 0 then
      begin
        for i := VisCnt - 1 downto 0 do
        begin
          P := VisiblePanels[i];
//          if P <> Sender then
          begin
            Old := P.fNewSize;
            P.fNewSize := P.CheckSize(Old + ToDo);
            Dec(ToDo, P.fNewSize - Old);
          end;
          if ToDo <= 0 then
            Exit;
        end;
      end
      else if ToDo < 0 then begin
        ToDo := -ToDo;
        for i := VisCnt - 1 downto 0 do
        begin
          P := VisiblePanels[i];
//          if P <> Sender then
          begin
            Old := P.fNewSize;
            P.fNewSize := P.CheckSize(Old - ToDo);
            Dec(ToDo, Old - P.fNewSize);
          end;
          if ToDo <= 0 then
            Exit;
        end;
      end;
    end;

    procedure HandleAdjustPanels2;
    {-------------------------------------------------------------------------------
      Bereken de sizes van alle panels in geval van self-resize. Bottom - Top tries
      Maakt gebruik van ResizeList
    -------------------------------------------------------------------------------}
    var
      i: Integer;
      Old, ToDo: Integer;
      P: TSplitPanel;
      L: TSplitPanelList;
    begin
      // bereken correctie
      ToDo := AvailablePanelSize - UsedPanelSize;
      if ToDo = 0 then
        Exit;

      L := fResizeList;
      // corrigeer
      if ToDo > 0 then
      begin
        for i := 0 to L.Count - 1 do
        begin
          P := L[i];
          Old := P.fNewSize;
          P.fNewSize := P.CheckSize(Old + ToDo);
          Dec(ToDo, P.fNewSize - Old);
          if ToDo <= 0 then
            Exit;
        end;
      end
      else if ToDo < 0 then begin
        ToDo := -ToDo;
        for i := 0 to L.Count - 1 do
        begin
          P := L[i];
          Old := P.fNewSize;
          P.fNewSize := P.CheckSize(Old - ToDo);
          Dec(ToDo, Old - P.fNewSize);
          if ToDo <= 0 then
            Exit;
        end;
      end;
    end;

    procedure HandleScalePanels;
    {-------------------------------------------------------------------------------
      Herverdeel op schaal als self.bounds veranderd zijn
    -------------------------------------------------------------------------------}
    var
      i: Integer;
      P: TSplitPanel;
      H: Double;
      Old, ToDo, Used: Integer;
      SmallFirst: Boolean;
    begin
      if UsedPanelSize = 0 then
        Exit;
      Used := 0;
      for i := 0 to VisCnt - 1 do
      begin
        P := VisiblePanels[i];
        H := ClientSize * P.fCurrentScale;
        P.fNewSize := P.CheckSize(Round(H));
        Inc(Used, P.fNewSize);
      end;
      Correct(AvailablePanelSize - Used, True);
    end;

    procedure HandleAutoSize;
    {-------------------------------------------------------------------------------
      Groeien
    -------------------------------------------------------------------------------}
    begin
      case SplitMode of
        smVertical   : Height := GetUsedSize;
        smHorizontal : Width := GetUsedSize;
      end;
    end;

    procedure HandleSplitterMoved;
    {-------------------------------------------------------------------------------
      Afhandelen user resize splitter. Er zijn 2 situaties:
      1) Resize naar beneden (Delta > 0):
         a) Bereken hoeveel krimp-mogelijkheid er bij de volgende panelen is en
            pas Delta aan als Delta te groot is.
         b) Pas FirstPanel aan. Wanneer deze niet van grootte veranderd proberen
            we Het Panel erboven. Op die manier kunnen we "trekken".
         c) Pas het SecondPanel aan. Wanneer deze niet van
            grootte veranderd (door constraints of autosize) proberen we het paneel
            daaronder etc. Op die manier kunnen we "duwen".
            worden.
      2) Resize omhoog (Delta < 0):
         Idem maar dan de andere kant op

      N.B: Wanneer een panel autosize is, is de Shrink-mogelijkheid *niet*
           gelijk aan Size - MinSize. Deze worden dus niet meegenomen in de berekening
    -------------------------------------------------------------------------------}
    var
      First, Second, Current: TSplitPanel;
      FirstToDo, SecondToDo: Integer;
      Push: Boolean;

        // uitrekenen van maximum krimp-mogelijkheid onderliggende panels
        // of 1 panel als push uit staat
        function CalcMaxShrinkDown: Integer;
        var
          i: Integer;
          S: Integer;
          P: TSplitPanel;
        begin
          Result := 0;
          for i := First.VisibleIndex + 1 to VisCnt - 1 do 
          begin
            P := VisiblePanels[i]; 
            if P.fCalculatedSize = 0 then
              Inc(Result, Max(0, P.fOldSize - P.RealMinSize));
            if not Push then
              Break;
          end;
        end;

        // uitrekenen van maximum krimp-mogelijkheid bovenliggende panels
        // of 1 panel als push uit staat
        function CalcMaxShrinkUp: Integer;
        var
          i: Integer;
          S: Integer;
          P: TSplitPanel;
        begin
          Result := 0;
          for i := First.VisibleIndex downto 0 do 
          begin
            P := VisiblePanels[i]; 
            if P.fCalculatedSize = 0 then
              Inc(Result, Max(0, P.fOldSize - P.RealMinSize));
            if not Push then
              Break;
          end;
        end;

    begin
      First := TPanelSplitter(aControl).FirstPanel;
      Second := TPanelSplitter(aControl).SecondPanel;
      Push := poDragPush in fOptions;
      // check autosize
      if AutoSize then
      begin
        First.fNewSize := First.CheckSize(First.fOldSize + Delta);
        Exit;
      end;
      // situatie 1
      if Delta > 0 then
      begin
        // resize eerste panel, rekening houdend met constraints
        Delta := Min(Delta, CalcMaxShrinkDown);
        if Delta <= 0 then
          Exit;
        // boven (links)
        Current := First;
        FirstToDo := Delta;
        SecondToDo := 0;
        repeat
          if (Current = nil) or (FirstToDo <= 0) then
            Break;
          Current.fNewSize := Current.CheckSize(Current.fOldSize + FirstToDo);
          Dec(FirstToDo, Current.fNewSize - Current.fOldSize);
          Inc(SecondToDo, Current.fNewSize - Current.fOldSize);
          Current := Current.PrevVisiblePanel; 
          if not Push then
            Break;
        until False;
        // onder (rechts)
        Current := Second;
        repeat
          if (Current = nil) or (SecondToDo <= 0) then
            Break;
          Inc(Current.fNewPosition, SecondToDo);
          Current.fNewSize := Current.CheckSize(Current.fOldSize - SecondToDo);
          Dec(SecondToDo, Current.fOldSize - Current.fNewSize);
          Current := Current.NextVisiblePanel; 
          if not Push then
            Break;
        until False;
      end
      // situatie 2
      else if Delta < 0 then
      begin
        Delta := -Delta;
        Delta := Min(Delta, CalcMaxShrinkUp);
        if Delta <= 0 then
          Exit;
        // onder (rechts)
        Current := Second;
        FirstToDo := Delta;
        SecondToDo := 0;
        repeat
          if (Current = nil) or (FirstToDo <= 0) then
            Break;
          Current.fNewSize := Current.CheckSize(Current.fOldSize + FirstToDo);
          Dec(FirstToDo, Current.fNewSize - Current.fOldSize);
          Inc(SecondToDo, Current.fNewSize - Current.fOldSize);
          Current := Current.NextVisiblePanel; 
        until False;
        // boven (links)
        Current := First;
        repeat
          if (Current = nil) or (SecondToDo <= 0) then
            Break;
          Current.fNewSize := Current.CheckSize(Current.fOldSize - SecondToDo);
          Dec(SecondToDo, Current.fOldSize - Current.fNewSize);
          Current := Current.PrevVisiblePanel;
        until False;
      end;
    end;

    function CheckDisableSplitter(S: TPanelSplitter): Boolean;
    begin
       Result := True;
      if (csDesigning in ComponentState) then
        Exit;
      if DivideMode = dmEqual then
        Exit;
      // dit is niet helemaal goed maar redelijk
      if (S.FirstPanel.FixedSize > 0) or (S.FirstPanel.CalculatedSize > 0) then
        if  S.FirstPanel.VisibleIndex = 0 then
          Exit;
      if not (poAutoSizeParent in fOptions) then
        if (S.SecondPanel.FixedSize > 0) or (S.SecondPanel.CalculatedSize > 0) then
          if  S.SecondPanel.VisibleIndex = VisCnt - 1 then
            Exit;
      Result := False;
    end;

    procedure DoSplitters;
    {-------------------------------------------------------------------------------
      Opnieuw positioneren van de splitters
    -------------------------------------------------------------------------------}
    var
      A, B: TSplitPanel;
      S: TPanelSplitter;
      X, Y, W, H, i: Integer;

    begin
      Assert(VisiblePanelCount - 1 = VisibleSplitterCount, 'DoSplitters error');
      for i := 1 to VisCnt - 1 do
      begin
        S := fVisibleSplitters[i - 1];
        A := VisiblePanels[i - 1];
        B := VisiblePanels[i];
        case SplitMode of
          smVertical:
            begin
              X := 0;
              Y := A.Top + A.Height;
              H := fDistance;
              W := ClientWidth;
              S.fDisabled := CheckDisableSplitter(S);
              S.SplitMode := SplitMode;
              S.fLiveResize := Self.LiveSizing;
              S.SetBounds(X, Y, W, H);
            end;
          smHorizontal:
            begin
              Y := 0;
              X := A.Left + A.Width;
              W := fDistance;
              H := ClientHeight;
              S.fDisabled := CheckDisableSplitter(S);
              S.SplitMode := SplitMode;
              S.fLiveResize := Self.LiveSizing;
              S.SetBounds(X, Y, W, H);
            end;
        end;
      end;
    end;

begin
  if (csDestroying in ComponentState)
  or (psCalculating in fState)
  or (csReading in ComponentState)
  or IsUpdating then
    Exit;

  Cnt := PanelCount;
  if Cnt = 0 then
    Exit;
  if aControl is TSplitPanel then
    Sender := TSplitPanel(aControl)
  else
    Sender := nil;

  Include(fState, psCalculating);
  BeginUpdate;
  PaintLock;
  InitLocals;
  try
    case NeededAction of
      caNone         : Exit;
      caDefault      : if Sender <> nil then HandleAdjustPanels else HandleAdjustPanels2;
      caScalePanels  : HandleScalePanels;
      caEqualDivide  : HandleEqualDividePanels;
      caSplitter     : HandleSplitterMoved;
      caAutoSize     : HandleAutoSize;
    end;
    ApplyChanges;
    DoSplitters;
  finally
    Exclude(fState, psCalculating);
    ExitLocals;
    PaintUnLock;
    EndUpdate;
  end;
  if Assigned(fAfterCalculate) then fAfterCalculate(Self);
end;

procedure TSplitPanel.WMPaint(var Msg: TWMPaint);
begin
  GetUpdateRect(Handle, fUpdateRect, False);
  inherited;
end;

procedure TSplitPanel.WMNCPaint(var Message: TMessage{RealWMNCPaint});
begin
  inherited;
end;

function TSplitPanel.GetLevel: Integer;
var
  P: TSplitPanel;
begin
  Result := 0;
  P := Self;
  while P.ParentPanel <> nil do
  begin
    Inc(Result);
    P := P.ParentPanel;
  end;
end;

procedure TSplitPanel.SetPanelCount(Value: Integer);
{-------------------------------------------------------------------------------
  insert of remove panels.
-------------------------------------------------------------------------------}
var
  Old, i: Integer;
begin
  if Value < 0 then
    Value := 0;
  if Value > 32 then
    Value := 32;
  Old := PanelCount;
  if Value = Old then
    Exit;
  BeginUpdate;
  try
    if Value > Old then
      for i := Old + 1 to Value do
        NewPanel
    else
      while PanelCount <> Value do
        Panels[PanelCount - 1].Free;

  finally
    EndUpdate;
    if not IsUpdating then
      Recalculate(nil, riPanelAdded);
  end;
end;

procedure TSplitPanel.SetParent(aControl: TWinControl);
{-------------------------------------------------------------------------------
  notify parentpanel.
  interne method-events worden double-safe gereset. kan nog geoptimaliseerd
-------------------------------------------------------------------------------}
var
  OldParent: TWinControl;
begin
  if aControl is TSplitPanel then
    ResetMethods(TSplitPanel(aControl).SplitMode);

  OldParent := Parent;

  inherited SetParent(aControl);

  if Parent = OldParent then
    Exit;

  if aControl is TSplitPanel then
  begin
    fParentPanel := TSplitPanel(aControl);
    ResetMethods(fParentPanel.SplitMode);
    fParentPanel.AddPanel(Self);
  end
  else begin
    if fParentPanel <> nil then
      fParentPanel.RemovePanel(Self);
    fParentPanel := nil;
  end;

  CheckParentAutoSize;

end;

function TSplitPanel.GetRoot: TSplitPanel;
var
  P: TSplitPanel;
begin
  Result := Self;
  P := Self;
  while P.ParentPanel <> nil do
  begin
    P := P.ParentPanel;
    Result := P;
  end;
end;


procedure TSplitPanel.CMDesignHitTest(var Message: TCMDesignHitTest);
begin
  inherited;
//    Message.Result := 0;
end;

procedure TSplitPanel.Loaded;
begin
  inherited Loaded;
  UpdateLists;
  RecalcScales;
end;

procedure TSplitPanel.DoChildren(aCommand: TChildCommand);
var
  i: Integer;
begin
  BeginUpdate;
  try
    for i := 0 to PanelCount - 1 do
    begin
      case aCommand of
        ccDistance    : Panels[i].Distance := Self.Distance;
        ccColor       : Panels[i].ParentColor := True;
        ccLiveSizing  : Panels[i].LiveSizing := Self.LiveSizing;
        ccQuickBorder : Panels[i].QuickBorder := Self.QuickBorder;
      end;
    end;
  finally
    EndUpdate;
  end;
end;

function TSplitPanel.GetNextPanel: TSplitPanel;
var
  Ix: Integer;
begin
  Result := nil;
  if ParentPanel = nil then
    Exit;
  Ix := Index;
  if Ix < 0 then
    Exit;
  if Ix < ParentPanel.PanelCount - 1 then
    Result := ParentPanel.Panels[Ix + 1];
end;

function TSplitPanel.GetPrevPanel: TSplitpanel;
var
  Ix: Integer;
begin
  Result := nil;
  if ParentPanel = nil then
    Exit;
  Ix := Index;
  if Ix > 0 then
    Result := ParentPanel.Panels[Ix - 1];
end;

function TSplitPanel.GetNextVisiblePanel: TSplitPanel;
{-------------------------------------------------------------------------------
  alleen uitgaan van VisiblePanels!
-------------------------------------------------------------------------------}
var
  Ix: Integer;
begin
  Result := nil;
  if ParentPanel = nil then
    Exit;
  Ix := VisibleIndex;
  if Ix < 0 then
    Exit;
  if Ix < ParentPanel.VisiblePanelCount - 1 then
    Result := ParentPanel.VisiblePanels[Ix + 1];
end;

function TSplitPanel.GetPrevVisiblePanel: TSplitPanel;
{-------------------------------------------------------------------------------
  alleen uitgaan van VisiblePanels!
-------------------------------------------------------------------------------}
var
  Ix: Integer;
begin
  Result := nil;
  if ParentPanel = nil then
    Exit;
  Ix := VisibleIndex;
  if Ix > 0 then
    Result := ParentPanel.VisiblePanels[Ix - 1];
end;

procedure TSplitPanel.SetDivideMode(const Value: TDivideMode);
begin
  fDivideMode := Value;
  Recalculate(nil, riDivideModeChanged);
end;

procedure TSplitPanel.ConstrainedResize(var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
{-------------------------------------------------------------------------------
  Bereken het totaal van min en maxsizes van de subpanels
-------------------------------------------------------------------------------}
var
  i: Integer;
  Min, Max: Integer;
  HasMin, HasMax: Boolean;
  P: TSplitPanel;
begin
  inherited ConstrainedResize(MinWidth, MinHeight, MaxWidth, MaxHeight);
  InternalCheckConstraints(MinWidth, MinHeight, MaxWidth, MaxHeight);
end;

function TSplitPanel.InternalGetLeft: Integer;
begin
  Result := Left;
end;

procedure TSplitPanel.InternalSetLeft(Value: Integer);
begin
  Left := Value;
end;

function TSplitPanel.InternalGetTop: Integer;
begin
  Result := Top;
end;

procedure TSplitPanel.InternalSetTop(Value: Integer);
begin
  Top := Value;
end;

function TSplitPanel.InternalGetWidth: Integer;
begin
  Result := Width;
end;

procedure TSplitPanel.InternalSetWidth(Value: Integer);
begin
  Width := Value;
end;

function TSplitPanel.InternalGetHeight: Integer;
begin
  Result := Height;
end;

procedure TSplitPanel.InternalSetHeight(Value: Integer);
begin
  Height := Value;
end;

function TSplitPanel.InternalGetMinWidth: TConstraintSize;
begin
  Result := Constraints.MinWidth;
end;

procedure TSplitPanel.InternalSetMinWidth(Value: TConstraintSize);
begin
  Constraints.MinWidth := Value;
end;

function TSplitPanel.InternalGetMinHeight: TConstraintSize;
begin
  Result := Constraints.MinHeight;
end;

procedure TSplitPanel.InternalSetMinHeight(Value: TConstraintSize);
begin
  Constraints.MinHeight := Value;
end;

function TSplitPanel.InternalGetMaxWidth: TConstraintSize;
begin
  Result := Constraints.MaxWidth;
end;

procedure TSplitPanel.InternalSetMaxWidth(Value: TConstraintSize);
begin
  Constraints.MaxWidth := Value;
end;

function TSplitPanel.InternalGetMaxHeight: TConstraintSize;
begin
  Result := Constraints.MaxHeight;
end;

procedure TSplitPanel.InternalSetMaxHeight(Value: TConstraintSize);
begin
  Constraints.MaxHeight := Value;
end;

function TSplitPanel.InternalGetRealMaxHeight: TConstraintSize;
begin
  Result := RealConstraints.MaxHeight;
end;

function TSplitPanel.InternalGetRealMaxWidth: TConstraintSize;
begin
  Result := RealConstraints.MaxWidth;
end;

function TSplitPanel.InternalGetRealMinHeight: TConstraintSize;
begin
  Result := RealConstraints.MinHeight;
end;

function TSplitPanel.InternalGetRealMinWidth: TConstraintSize;
begin
  Result := RealConstraints.MinWidth;
end;

procedure TSplitPanel.ResetMethods(aParentSplitMode: TSplitMode);
var
  H: Integer;
begin
  case aParentSplitMode of
    smVertical:
      begin
        EvGetSize          := InternalGetHeight;
        EvSetSize          := InternalSetHeight;
        EvGetPosition      := InternalGetTop;
        EvSetPosition      := InternalSetTop;
        EvGetMinSize       := InternalGetMinHeight;
        EvSetMinSize       := InternalSetMinHeight;
        EvGetMaxSize       := InternalGetMaxHeight;
        EvSetMaxSize       := InternalSetMaxHeight;
        EvGetRealMinSize   := InternalGetRealMinHeight;
        EvGetRealMaxSize   := InternalGetRealMaxHeight;
      end;
    smHorizontal:
      begin
        EvGetSize          := InternalGetWidth;
        EvSetSize          := InternalSetWidth;
        EvGetPosition      := InternalGetLeft;
        EvSetPosition      := InternalSetLeft;
        EvGetMinSize       := InternalGetMinWidth;
        EvSetMinSize       := InternalSetMinWidth;
        EvGetMaxSize       := InternalGetMaxWidth;
        EvSetMaxSize       := InternalSetMaxWidth;
        EvGetRealMinSize   := InternalGetRealMinWidth;
        EvGetRealMaxSize   := InternalGetRealMaxWidth;
      end;
  end;

  case aParentSplitMode of
    smVertical:
      begin
        DisableAlign;
        if Constraints.MinWidth > 0 then
        begin
          Constraints.MinHeight := Constraints.MinWidth;
          Constraints.MinWidth := 0;
        end;

        if Constraints.MaxWidth > 0 then
        begin
          Constraints.MaxHeight := Constraints.MaxWidth;
          Constraints.MaxWidth := 0;
        end;
        EnableAlign;
      end;
    smHorizontal:
      begin
        DisableAlign;
        if Constraints.MinHeight > 0 then
        begin
          Constraints.MinWidth := Constraints.MinHeight;
          Constraints.MinHeight := 0;
        end;

        if Constraints.MaxHeight > 0 then
        begin
          Constraints.MaxWidth := Constraints.MaxHeight;
          Constraints.MaxHeight := 0;
        end;
        EnableAlign;
      end;
  end;

end;

function TSplitPanel.GetSize: Integer;
begin
  Assert(Assigned(EvGetSize), 'GetSize error');
  Result := EvGetSize;
end;

procedure TSplitPanel.SetSize(const Value: Integer);
begin
  Assert(Assigned(EvSetSize), 'SetSize error');
  EvSetSize(Value);
end;

function TSplitPanel.GetPosition: Integer;
begin
  Assert(Assigned(EvGetPosition), 'GetPosition error');
  Result := EvGetPosition;
end;

procedure TSplitPanel.SetPosition(const Value: Integer);
begin
  Assert(Assigned(EvSetPosition), 'SetPosition error');
  EvSetPosition(Value);
end;

procedure TSplitPanel.CMBorderChanged(var Message: TMessage);
begin
  inherited;
  RecreateWnd;
  Invalidate;
end;

procedure TSplitPanel.CMCtl3DChanged(var Message: TMessage);
begin
  inherited;
  RecreateWnd;
end;

procedure TSplitPanel.CMTextChanged(var Message: TMessage);
begin
  Invalidate;
end;

procedure TSplitPanel.CMControlChange(var Message: TCMControlChange);
{-------------------------------------------------------------------------------
  Message komt aan wanneer er we parent worden van een control of wanneer een
  childcontrol verwijderd wordt.
  Hier kunnen we nog een aantal slimmigheidjes doen...
-------------------------------------------------------------------------------}
begin
  inherited;
end;

procedure TSplitPanel.PaintLock;
var
  i: Integer;
begin
  Inc(fPaintLock);
//  for i := 0 to PanelCount - 1 do
  //  Panels[i].PaintLock;
end;

procedure TSplitPanel.PaintUnlock;
var
  i: Integer;
begin
  if fPaintLock > 0 then
  begin
    Dec(fPaintLock);
  end;
end;

function TSplitPanel.CanPaint: Boolean;
begin
  Result := fPaintLock = 0;
  if ParentPanel <> nil then
    Result := Result and ParentPanel.CanPaint;
end;

function TSplitPanel.InternalCheckAutoSize(var NewWidth, NewHeight: Integer): Boolean;
{-------------------------------------------------------------------------------
  Bereken Autosize als AutoSize aan staat
  a) Het panel heeft childpanels: de grootte de grootte van alle panels bij
     elkaar + de benodigde ruimte voor de splitters.
  b) Het panel heeft geen childpanels: de posities en groottes van
     de subcontrols bij elkaar opgeteld.
-------------------------------------------------------------------------------}
var
  i: Integer;
  P, S: Integer;
  C: TControl;
  NonClientPixels: Integer;
  HasCalcResult: Boolean;
begin
  Result := False;
  if not AutoSize then
    Exit;

  {-------------------------------------------------------------------------------
    (a)
  -------------------------------------------------------------------------------}
  if VisiblePanelCount > 0 then
  begin
    Result := VisiblePanelCount > 0;
    case splitmode of
      smVertical   : NewHeight := GetUsedSize;
      smHorizontal : NewWidth := GetUsedSize;
    end;
  end
  {-------------------------------------------------------------------------------
    (b)
  -------------------------------------------------------------------------------}
  else begin
    P := 0;
    S := 0;
    NonClientPixels := Width - ClientWidth;
    HasCalcResult := False;
    if ParentPanel = nil then
      Exit;

    case ParentPanel.SplitMode of
      smVertical:
        begin
          if ControlCount = 0 then
            Exit;
          // loop door non-height-client controls
          for i := 0 to ControlCount - 1 do
          begin
            C := Controls[i];
            if not (C.Align in [alLeft, alRight, alClient]) then
            begin
              S := Max(S, C.Top + C.Height);
              HasCalcResult := True;
            end;
          end;

          // is er iets uitgekomen?
          if HasCalcResult then
          begin
            NewHeight := S + NonClientPixels;
            fCalculatedSize := NewHeight; // wordt gebruikt bij shrink berekening resize
            Result := True;
          end;

        end;
      smHorizontal:
        begin
          // todo...
        end;
    end;
  end;
end;

procedure TSplitPanel.InternalCheckConstraints(var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
  MinWidth := RealConstraints.MinWidth;
  MinHeight := RealConstraints.MinHeight;
  MaxWidth := RealConstraints.MaxWidth;
  MaxHeight := RealConstraints.MaxHeight;
end;

function TSplitPanel.CheckSize(aSize: Integer): Integer;
var
  Min, Max: Integer;
  M: Integer;
  Auto: Boolean;
begin
  Min := RealMinSize;
  Max := RealMaxSize;
  Result := aSize;

  // bereken step, rekening houdend met startgrootte
  if fResizeStep > 0 then
  begin
    M := fResizeStepStart mod fResizeStep;
    if aSize + fResizeStep > M then
    begin
      while (aSize + fResizeStep) mod fResizeStep <> M do
        Dec(aSize);
    end
  end;

  result := asize;

  Auto := InternalCheckAutoSize(aSize, aSize);


//  if fCalculatedSize > 0 then
  //  aSize := fCalculatedSize;

  Result := aSize;

//  ConstrainedResize(Min, min, max, max);
//  InternalCheckConstraints(Min, Min, Max, Max);

  if (Max > 0) and (aSize > Max) then
    Result := Max
  else if (Min > 0) and (aSize < Min) then
    Result := Min;

end;

function TSplitPanel.GetMaxSize: TConstraintSize;
begin
  Assert(Assigned(EvGetMaxSize), 'GetMaxSize error');
  Result := EvGetMaxSize;
end;

function TSplitPanel.GetMinSize: TConstraintSize;
begin
  Assert(Assigned(EvGetMinSize), 'GetMinSize error');
  Result := EvGetMinSize;
end;

procedure TSplitPanel.SetMaxSize(Value: TConstraintSize);
begin
  Assert(Assigned(EvSetMaxSize), 'SetMaxSize error');
  EvSetMaxSize(Value)
end;

procedure TSplitPanel.SetMinSize(Value: TConstraintSize);
begin
  Assert(Assigned(EvSetMinSize), 'SetMinSize error');
  EvSetMinSize(Value)
end;

function TSplitPanel.CanAutoSize(var NewWidth, NewHeight: Integer): Boolean;
begin
  Result := InternalCheckAutoSize(NewWidth, NewHeight);
end;

function TSplitPanel.GetUsedSize(Flags: TInternalUsedSizeFlags = [sfPanels, sfSplitters]): Integer;
var
  i: Integer;
  P: TSplitPanel;
begin
 if csDesigning in ComponentState then
   Include(Flags, sfHidden);

  Result := 0;
  for i := 0 to PanelCount - 1 do
  begin
    P := Panels[i];
{    if i > 0 then
      if sfSplitters in Flags then
        Inc(Result, fDistance); }
    if sfPanels in Flags then
    begin
      if sfHidden in Flags then
        Inc(Result, P.Size)
      else if P.Visible then
        Inc(Result, P.Size)
    end;
  end;

  if sfSplitters in Flags then
  begin
    if sfHidden in Flags then
      Inc(Result, (PanelCount - 1) * fDistance)
    else
      Inc(Result, (VisiblePanelCount - 1) * fDistance);
  end;

end;


function TSplitPanel.GetFixedSize: Integer;
begin
  if (MinSize <> MaxSize) then //and (MinSize > DEF_CONSTRAINTSIZE) then
    Result := 0
  else
    Result := MaxSize;
end;

procedure TSplitPanel.SetFixedSize(const Value: Integer);
begin
  if csReading in ComponentState then
    Exit;
  if csLoading in ComponentState then
    Exit;
  if FixedSize = Value then Exit;
  if Value <= DEF_CONSTRAINTSIZE then
  begin
    MinSize := 0;
    MaxSize := 0;
  end
  else begin
    MinSize := Value;
    MaxSize := MinSize;
  end;
end;

procedure TSplitPanel.SetSplitPosition(const Value: TSplitPosition);
begin
  if fSplitPosition = Value then
    Exit;
  fSplitPosition := Value;
  if ParentPanel <> nil then
    ParentPanel.UpdateLists;//PanelPositionChanged(Self);
end;

procedure TSplitPanel.SetOptions(const Value: TSplitPanelOptions);
begin
  if fOptions = Value then
    Exit;
  fOptions := Value;
  CheckParentAutoSize;
end;


procedure TSplitPanel.CheckParentAutoSize;
var
  OldAlign: TAlign;
  F: TWinControl;
begin
  if poAutoSizeParent in fOptions then
    if ParentPanel = nil then // werkt alleen voor root
    begin
      F := TWinControl(Parent);
      if F.ControlCount = 1 then
      begin

//        if (F.ClientWidth <> Self.Width + Self.Left * 2) or
  //      (F.ClientHeight <>Self.Height + Self.Top * 2) then
        BEGIN

        OldAlign := Self.Align;
        F.DisableAlign;
        if F is TForm then
          TForm(F).AutoSize := False
        else if F is TPanel then
          TPanel(F).AutoSize := False
        else if F is TFrame then
          TFrame(F).AutoSize := False;

//        if IsPublishedProp(F, 'AutoSize') then
  //        if PropIsType(F, 'AutoSize', tkEnumeration) then


        Self.Align := alNone;
        //F.AutoSize := False;
        F.ClientWidth := Self.Width + Self.Left * 2;
        F.ClientHeight := Self.Height + Self.Top * 2;
        Self.Align := OldAlign;

        F.EnableAlign;
        END;
      end;
    end;
end;

procedure TSplitPanel.RequestAlign;
{-------------------------------------------------------------------------------
  Wordt aangeroepen in de inherited SetBounds. Gebruikt voor Scaling.
  Als er een SetBounds is geweest die *niet* door een parent-recalculate is
  gebeurt, wordt de schaal opnieuw berekend.
  Dit moet gebeurten voordat het parentpanel gaat alignen.
-------------------------------------------------------------------------------}
begin
  if (psParentUpdating in fState) then
  begin
//    Log(['no need']);
//    ControlState := ControlState - [csAlignmentNeeded];
    exit;
  end; 
(*  if EqualRects(fOldBounds, BoundsRect) then
  begin
    exit;
    //Log(['asdf']);
  end; *)
//exit;
  begin
//  if not (csReading in ComponentState) then
  if Assigned(ParentPanel) then
    if not (psParentUpdating in fState) or (psSplitterDragging in ParentPanel.fState) then
      if not EqualRects(fOldBounds, BoundsRect) then
         ParentPanel.RecalcScales;

  inherited RequestAlign;
  end;
end;

procedure TSplitPanel.SetBounds(aLeft, aTop, aWidth, aHeight: Integer);
{-------------------------------------------------------------------------------

-------------------------------------------------------------------------------}
begin
  if (ALeft <> Left) or (ATop <> Top) or
    (AWidth <> Width) or (AHeight <> Height) then
  begin

  fOldBounds := BoundsRect;
  inherited SetBounds(aLeft, aTop, aWidth, aHeight);
//  if name='BottomPanel' then Log([height]);
  //if color=clred then Log(['setbounds']);
  CheckParentAutoSize;
  end;
end;

function TSplitPanel.HasControls: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to PanelCount - 1 do
    if Panels[i].HasControls then
    begin
      Result := True;
      Exit;
    end;

  for i := 0 to ControlCount - 1 do
    if not (Controls[i] is TSplitPanel) then
      if not (Controls[i] is TPanelSplitter) then
      begin
        Result := True;
        Exit;
      end;
end;

procedure TSplitPanel.SetTemplateName(const Value: string);

    function Confirm: Boolean;
    begin
      Result := True;
      if not (csDesigning in ComponentState) then
        Exit;
      if not (poDesignTemplateWarning in fOptions) then
        Exit;
      if not HasControls{(PanelCount = 0) and (ControlCount = 0)} then
        Exit;
      Result := MessageDlg('Alle controls op dit SplitPanel worden verwijderd.' + Chr(13) + 'Doorgaan?',
        mtWarning, [mbYes, mbNo], 0) = mrYes;
    end;

begin
  if CompareText(fTemplateName, Value) = 0 then
    Exit;
  if not (csLoading in ComponentState) then
  if not (csReading in ComponentState) then
  begin
    if Root <> Self then
      raise Exception.Create('Templates alleen voor RootPanel bedoeld');
    if not Confirm then
      Exit;

    if Value <> '' then
    begin
      BeginUpdate;
      try
        //ResetToDefault;
        TemplateMgr.ActivateTemplate(Self, Value);
      finally
        EndUpdate;
        if not IsUpdating then
          Recalculate(nil, riTemplateChanged);
      end;
    end;
  end;
  fTemplateName := Value;
end;

procedure TSplitPanel.SetResizeStep(Value: Integer);
begin
  if Value < 0 then
    Value := 0;
  if Value > 1024 then
    Value := 1024;
  if fResizeStep = Value then
    Exit;
  fResizeStep := Value;
  if fResizeStep > 0 then
    fResizeStepStart := Height
  else
    fResizeStepStart := 0;  
  //Size := CheckSize(Size);
end;


function TSplitPanel.GetMode: TPanelMode;
begin
  if ParentPanel = nil then
    Result := pmRoot
  else if PanelCount = 0 then
    Result := pmLeave
  else
    Result := pmMiddle;
end;


procedure TSplitPanel.SetShowCaption(const Value: Boolean);
begin
  if fShowCaption = Value then Exit;
  fShowCaption := Value;
  Invalidate;
end;

procedure TSplitPanel.SetAlignment(const Value: TAlignment);
begin
  if fAlignment = Value then Exit;
  fAlignment := Value;
  Invalidate;
end;

function TSplitPanel.GetQuickBorder: TQuickBorderStyle;
begin
  Result := qbUnknown;
  if (fRealBevelKind = bkNone) and (fRealBevelInner = bvNone) and (fRealBevelOuter = bvNone) and (fRealBorderStyle = bsNone) then
    Result := qbNone
  else if (fRealBevelKind = bkNone) and (fRealBevelInner = bvNone) and (fRealBevelOuter = bvNone) and (fRealBorderStyle = bsSingle) then
    Result := qbBorder
  else if (fRealBevelKind = bkTile) and (fRealBevelInner = bvNone) and (fRealBevelOuter = bvLowered) and (fRealBorderStyle = bsNone) then
    Result := qbLowered
  else if (fRealBevelKind = bkTile) and (fRealBevelInner = bvRaised) and (fRealBevelOuter = bvNone) and (fRealBorderStyle = bsNone) then
    Result := qbRaised;

end;

procedure TSplitPanel.SetQuickBorder(const Value: TQuickBorderStyle);
begin
  if Value <> QuickBorder then
  case Value of
    qbNone:
      begin
        BevelKind := bkNone;
        BorderStyle := bsNone;
        BevelInner := bvNone;
        BevelOuter := bvNone;
      end;
    qbBorder:
      begin
        BevelKind := bkNone;
        BevelInner := bvNone;
        BevelOuter := bvNone;
        BorderStyle := bsSingle;
      end;
    qbLowered:
      begin
        BorderStyle := bsNone;
        BevelKind := bkTile;
        BevelInner := bvNone;
        BevelOuter := bvLowered;
      end;
    qbRaised:
      begin
        BorderStyle := bsNone;
        BevelKind := bkTile;
        BevelInner := bvRaised;
        BevelOuter := bvNone;
      end;
  end;
  DoChildren(ccQuickBorder)
end;

procedure TSplitPanel.ClearBorder;
begin
  Include(fState, psUpdatingBorder);
  try
    fBorderStyle := bsNone;
    inherited BevelKind := bkNone;
    inherited BevelInner := bvNone;
    inherited BevelOuter := bvNone;
    inherited BevelWidth := 1;
  finally
    Exclude(fState, psUpdatingBorder);
    Perform(CM_BORDERCHANGED, 0, 0);
  end;
end;

procedure TSplitPanel.ResetBorder;
begin
  Include(fState, psUpdatingBorder);
  try
    fBorderStyle := fRealBorderStyle;
    inherited BevelKind   := fRealBevelKind;
    inherited BevelInner  := fRealBevelInner;
    inherited BevelOuter  := fRealBevelOuter;
    inherited BevelWidth  := fRealBevelWidth;
  finally
    Exclude(fState, psUpdatingBorder);
    Perform(CM_BORDERCHANGED, 0, 0);
  end;
end;

function TSplitPanel.GetBevelInner: TBevelCut;
begin
  Result := fRealBevelInner;
end;

function TSplitPanel.GetBevelKind: TBevelKind;
begin
  Result := fRealBevelKind;
end;

function TSplitPanel.GetBevelOuter: TBevelCut;
begin
  Result := fRealBevelOuter;
end;

function TSplitPanel.GetBevelWidth: TBevelWidth;
begin
  Result := fRealBevelWidth;
end;

procedure TSplitPanel.SetBevelInner(const Value: TBevelCut);
begin
  if PanelCount = 0 then
    inherited BevelInner := Value;
  fRealBevelInner := Value;
end;

procedure TSplitPanel.SetBevelKind(const Value: TBevelKind);
begin
  if PanelCount = 0 then
    inherited BevelKind := Value;
  fRealBevelKind := Value;
end;

procedure TSplitPanel.SetBevelOuter(const Value: TBevelCut);
begin
  if PanelCount = 0 then
    inherited BevelOuter := Value;
  fRealBevelOuter := Value;
end;

procedure TSplitPanel.SetBevelWidth(const Value: TBevelWidth);
begin
  if PanelCount = 0 then
    inherited BevelWidth := Value;
  fRealBevelWidth := Value;
end;


procedure TSplitPanel.SetAutoSize(Value: Boolean);
begin
  if Value <> AutoSize then
  begin
    case Value of
      False:
        begin
          fCalculatedSize := 0;
          //FixedSize := 0;
        end;
      True:
        begin
          //ControlState := ControlState + [csAlignmentNeeded];
        end;
    end;
    inherited SetAutoSize(Value);
  end;
end;


procedure TSplitPanel.SetLiveSizing(const Value: Boolean);
begin
  fLiveSizing := Value;
  DoChildren(ccLiveSizing);
end;


procedure TSplitPanel.CMVisibleChanged(var Message: TMessage);
begin
{  if csReading in COmponentState then
    if ParentPanel <> nil then
      windlg(''); }
  if not (csReading in ComponentState) then
    if ParentPanel <> nil then
      ParentPanel.UpdateLists;
  //windlg(['vis changed', visible]);
  inherited;
//  RequestAlign; wordt al gedaan in wincontrol
end;


function TSplitPanel.GetVisiblePanelCount: Integer;
begin
  Result := fVisiblePanels.Count;
end;

function TSplitPanel.GetVisiblePanel(Index: Integer): TSplitPanel;
begin
  if fVisiblePanels = nil then
    Result := nil
  else
    Result := fVisiblePanels[Index];
end;

procedure TSplitPanel.VisibleChanging;
begin
  if IsUpdating then
    raise Exception.Create('Illegal Panel show of hide');
end;

procedure TSplitPanel.UpdatePanelOrder;
{-------------------------------------------------------------------------------
  sorteer panellist afh. van position (first, none, last)
-------------------------------------------------------------------------------}
var
  List: TSplitPanelList;

    procedure DoAdd(aPos: TSplitPosition);
    var
      i: Integer;
      P: TSplitPanel;
    begin
      for i := 0 to PanelCount - 1 do
      begin
        P := Panels[i];
        if P.SplitPosition = aPos then
          List.Add(P);
      end;
    end;

    procedure Sync;
    var
      i: Integer;
      P: TSplitPanel;
    begin
      Assert(List.Count = PanelCount, 'UpdatePanelOrder Sync error');
      for i := 0 to List.Count - 1 do
      begin
        fPanels.List^[i] := List[i];
      end;
    end;

begin
  List := TSplitPanelList.Create;
  try
    DoAdd(spFirst);
    DoAdd(spNone);
    DoAdd(spLast);
    Sync;
  finally
    List.Free;
  end;

end;

procedure TSplitPanel.UpdateSplitterList;
{-------------------------------------------------------------------------------
  Zorg dat de splitters (en het aantal splitters) synchroon lopen
-------------------------------------------------------------------------------}
var
  i, SCount, PCount: Integer;
  NewCount: Integer;
  S: TPanelSplitter;
begin
  Assert(psUpdatingLists in fState, 'UpdateSplitterList illegal');
  PCount := fPanels.Count;
  SCount := fSplitters.Count;
  if PCount <= 1 then
    NewCount := 0
  else
    NewCount := PCount - 1;
  if NewCount = SCount then
    Exit;

  // creeer
  if NewCount > SCount then
  begin
    for i := SCount + 1 to NewCount do
    begin
      S := TPanelSplitter.Create(Self);
      S.Parent := Self;
    end
  end
  // verwijder
  else begin
    while fSplitters.Count <> NewCount do
    begin
      fSplitters[fSplitters.Count - 1].Free;
    end;
  end;
end;

procedure TSplitPanel.UpdateVisiblePanelList;
{-------------------------------------------------------------------------------
  zet alle visible panels in list
-------------------------------------------------------------------------------}
var
  i: Integer;
  P: TSplitPanel;
begin
  Assert(psUpdatingLists in fState, 'SyncVisiblePanels illegal');
  fVisiblePanels.Clear;
  for i := 0 to PanelCount - 1 do
  begin
    P := Panels[i];
    if P.Visible or (csDesigning in ComponentState) then
      fVisiblePanels.Add(P);
  end;
  fVisiblePanelCount := fVisiblePanels.Count;
end;

procedure TSplitPanel.UpdateVisibleSplitterList;
var
  i: Integer;
  P: TSplitPanel;
begin
  Assert(psUpdatingLists in fState, 'SyncVisibleSplitters illegal');
  fVisibleSplitters.Clear;
  for i := 1 to PanelCount - 1 do
  begin
    P := Panels[i];
    if P.Visible or (csDesigning in ComponentState) then
      fVisibleSplitters.Add(fSplitters[i - 1]);
  end;
  fVisibleSplitterCount := fVisibleSplitters.Count - 1;
end;

procedure TSplitPanel.UpdateSplitterRefs;
{-------------------------------------------------------------------------------
  Bereken de splitterpointers
-------------------------------------------------------------------------------}
var
  S: TPanelSplitter;
  i: Integer;
  A, B: TSplitPanel;
begin
  if PanelCount < 2 then
    Exit;
  Assert(SplitterCount = PanelCount - 1, 'RefreshSplitters error 1');
  // eerst voor de zekerheid alle panels
  for i := 1 to PanelCount - 1 do
  begin
    A := Panels[i - 1];
    B := Panels[i];
    S := fSplitters[i - 1];
    S.fFirstPanel := A;
    S.fSecondPanel := B;
    S.SplitMode := Self.SplitMode;
    if fVisibleSplitters.IndexOf(S) < 0 then
    begin
      S.Visible := False;
    end
    else
      S.Visible := True;
  end;

  // dan reset als er hidden panels zijn
  if VisiblePanelCount < 2 then
    Exit;
  if VisibleSplitterCount = SplitterCount then
    Exit;
  Assert(VisibleSplitterCount = VisiblePanelCount - 1, 'RefreshSplitters error 2');
  for i := 1 to VisiblePanelCount - 1 do
  begin
    A := VisiblePanels[i - 1];
    B := VisiblePanels[i];
    S := fVisibleSplitters[i - 1];
    S.fFirstPanel := A;
    S.fSecondPanel := B;
    S.SplitMode := Self.SplitMode;
  end;
end;

procedure TSplitPanel.UpdateRealConstraints;
{-------------------------------------------------------------------------------
  Aanpassen RealConstraints: de som van alle childpanels.realconstraints
-------------------------------------------------------------------------------}
var
  i, Min, Max: Integer;
  HasMax: Boolean;
  P: TSplitPanel;
begin
  RealConstraints.Assign(Constraints);

  if PanelCount = 0 then
    Exit;

  Min := (VisibleSplitterCount) * fDistance;
  Max := 0;

//  HasMax := True;   umx

  case SplitMode of
    smVertical:
      begin
        for i := 0 to VisiblePanelCount - 1 do
        begin
          P := VisiblePanels[i];
          Inc(Min, P.RealConstraints.MinHeight);
{ nog niet goed           Inc(Max, P.RealConstraints.MaxHeight);
          if P.RealConstraints.MaxHeight = 0 then
            HasMax := False; }
        end;
        if Min > Constraints.MinHeight then
          RealConstraints.MinHeight := Min;
{        if HasMax then
          RealConstraints.MaxHeight := Max; }

      end;
    smHorizontal:
      begin
        for i := 0 to VisiblePanelCount - 1 do
          Inc(Min, VisiblePanels[i].RealConstraints.MinWidth);
        if Min > Constraints.MinWidth then
          RealConstraints.MinWidth := Min;
      end;
  end;
end;

procedure TSplitPanel.UpdateResizeList;
var
  aPosition: TSplitPosition;

    procedure DoPanel(P: TSplitPanel);
    begin
      if aPosition = P.SplitPosition then
        fResizeList.Add(P);
    end;

begin
  fResizeList.Clear;
  aPosition := spNone;
  ForEachVisiblePanel(@DoPanel, True);
  aPosition := spLast;
  ForEachVisiblePanel(@DoPanel, True);
  aPosition := spFirst;
  ForEachVisiblePanel(@DoPanel, True);
end;


procedure TSplitPanel.UpdateLists;
{-------------------------------------------------------------------------------
  a) bijwerken van order van de panellist ahv splitposition
  b) creeeren en weggooien van splitters ahv panellist
  c) bijwerken visiblepanels
  d) bijwerken visiblesplitters
  e) bijwerken splitter pointers naar panels
  f) real constraints bijwerken
  g) bijwerken van resizelist (goede volgorde van resizing )
-------------------------------------------------------------------------------}
var
  i: Integer;
begin
  Include(fState, psUpdatingLists);
  BeginUpdate;
  try
    UpdatePanelOrder;
    UpdateSplitterList;
    UpdateVisiblePanelList;
    UpdateVisibleSplitterList;
    UpdateSplitterRefs;
    UpdateRealConstraints;
    UpdateResizeList;
  finally
    Exclude(fState, psUpdatingLists);
    EndUpdate;
    if not IsUpdating then
      Recalculate(nil, riNone);
  end;

end;


function TSplitPanel.GetVisibleSplitterCount: Integer;
begin
  Result := fVisibleSplitters.Count;
end;


procedure TSplitPanel.NewConstraintsOnChange(Sender: TObject);
{-------------------------------------------------------------------------------
  Overridden Constraints.OnChange.
  a) Constraints worden keihard op 2 gezet als kleiner dan 2 (N.B. recursie wordt
     voorkomen door de OnChange eventhandler tijdelijk uit te zetten)
  b) De RealConstraints worden bijgewerkt.
  c) roep de originele vcl eventhandler aan
-------------------------------------------------------------------------------}
var
  Ev: TNotifyEvent;
begin
  Ev := Constraints.OnChange;
  Constraints.OnChange := nil;
  if Constraints.MinHeight < DEF_CONSTRAINTSIZE then
    Constraints.MinHeight := DEF_CONSTRAINTSIZE;
  if Constraints.MinWidth < DEF_CONSTRAINTSIZE then
    Constraints.MinWidth := DEF_CONSTRAINTSIZE;
  Constraints.OnChange := Ev;

  UpdateRealConstraints;
  if ParentPanel <> nil then
    ParentPanel.UpdateRealConstraints;
  fOldConstraintsEvent(Sender);
end;

function TSplitPanel.GetRealMaxSize: TConstraintSize;
begin
  Assert(Assigned(EvGetRealMaxSize), 'GetRealMaxSize error');
  Result := EvGetRealMaxSize;
end;

function TSplitPanel.GetRealMinSize: TConstraintSize;
begin
  Assert(Assigned(EvGetRealMinSize), 'GetRealMinSize error');
  Result := EvGetRealMinSize;
end;

procedure TSplitPanel.SplitterDragBegin(Sender: TPanelSplitter);

    procedure DoSave(P: TSplitPanel);
    var
      i: Integer;
    begin
      P.fBoundsBeforeDrag := P.BoundsRect;
      for i := 0 to P.VisiblePanelCount - 1 do
        DoSave(P.VisiblePanels[i]);
    end;

begin
  Include(fState, psSplitterDragging);
  DoSave(Self);
end;

procedure TSplitPanel.SplitterDragCancel(Sender: TPanelSplitter);

    procedure DoRestore(P: TSplitPanel);
    var
      i: Integer;
    begin
      for i := 0 to P.VisiblePanelCount - 1 do
        DoRestore(P.VisiblePanels[i]);
      P.BoundsRect := P.fBoundsBeforeDrag;
    end;
var
  i: Integer;
begin
  if psSplitterDragging in fState then
  begin
    for i := 0 to splittercount-1 do
      fsplitters[i].fdraginfo.idragging := false;
    DoRestore(Self);
//    windlg('');
    Exclude(fState, psSplitterDragging);
  end;
end;

procedure TSplitPanel.SplitterDragEnd(Sender: TPanelSplitter);
begin
  Exclude(fState, psSplitterDragging);
end;


procedure TSplitPanel.SetScaleOnResize(const Value: Boolean);
var
  i: Integer;
  P: TSplitPanel;
  Used: Integer;
  Check: Double;
begin
//  if csReading in ComponentState then
  //  Exit;
  if fScaleOnResize = Value then
    Exit;

  fScaleOnResize := Value;
  case Value of
    False:
      begin
        RecalcScales(True);
      end;
    True:
      begin
        RecalcScales;
      end;
  end;
end;

procedure TSplitPanel.ParentUpdating(aUpdate: Boolean);

    procedure DoPanel(P: TSplitPanel); far;
    begin
      P.ParentUpdating(aUpdate);
    end;

begin
  case aUpdate of
    False :  Exclude(fState, psParentUpdating);
    True  :  Include(fState, psParentUpdating);
  end;
  ForEachPanel(@DoPanel);
end;


procedure TSplitPanel.ForEachPanel(LocalProc: Pointer; BackWards: Boolean = False);
{-------------------------------------------------------------------------------
  LocalProc = Locale (!) procedure met de volgende declaratie:
  procedure DoPanel(P: TSplitPanel); far;
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  i: Integer;
  P: TSplitPanel;
begin
  { Set up stack frame for local }
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  if not BackWards then
    for i := 0 to PanelCount - 1 do
    begin
      P := Panels[i];
      asm
        PUSH CallerBP                     { bewaar stack, deze wordt na de call automatisch gepopt }
        MOV EAX, P                        { param 1 in EAX }
        CALL LocalProc
        POP ECX
      end;
    end

  else
    for i := PanelCount - 1 downto 0 do
    begin
      P := Panels[i];
      asm
        PUSH CallerBP                     { bewaar stack, deze wordt na de call automatisch gepopt }
        MOV EAX, P                        { param 1 in EAX }
        CALL LocalProc
        POP ECX
      end;
    end
end;

procedure TSplitPanel.ForEachVisiblePanel(LocalProc: Pointer; BackWards: Boolean = False);
{-------------------------------------------------------------------------------
  LocalProc = Locale (!) procedure met de volgende declaratie:
  procedure DoPanel(P: TSplitPanel); far;
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  i: Integer;
  P: TSplitPanel;
begin
  { Set up stack frame for local }
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  if not BackWards then
    for i := 0 to VisiblePanelCount - 1 do
    begin
      P := VisiblePanels[i];
      asm
        PUSH CallerBP                     { bewaar stack, deze wordt na de call automatisch gepopt }
        MOV EAX, P                        { param 1 in EAX }
        CALL LocalProc
        POP ECX
      end;
    end

  else
    for i := VisiblePanelCount - 1 downto 0 do
    begin
      P := VisiblePanels[i];
      asm
        PUSH CallerBP                     { bewaar stack, deze wordt na de call automatisch gepopt }
        MOV EAX, P                        { param 1 in EAX }
        CALL LocalProc
        POP ECX
      end;
    end;

end;

procedure TSplitPanel.RecalcScales(DoClear: Boolean = False);
var
  i, Used: Integer;
  P: TSplitPanel;
begin
  if not fScaleOnResize then
    Exit;
{  if csReading in ComponentState then
    Exit;
  if csLoading in ComponentState then
    Exit; }

  Used := 0;
  case SplitMode of
    smVertical: Used := ClientHeight;
    smHorizontal: Used := ClientWidth;
  end;

//  Log([self.size]);

  for i := 0 to VisiblePanelCount - 1 do
  begin
    P := VisiblePanels[i];
    P.fCurrentScale := 0;
    if not DoClear or (Used > 0) then
    begin
      P.fCurrentScale := P.Size/Used;
//      if p.level=1 then
  //    Log([p.index, p.fcurrentscale]);
    end;
  end;
end;

procedure TSplitPanel.WMWindowPosChanging(var Msg: TWMWindowPosMsg);
begin
  inherited;
end;

procedure TSplitPanel.WMWindowPosChanged(var Message: TWMWindowPosChanged);
begin
  inherited;
  //Invalidate;
end;

{ TTemplateMgr }

procedure TTemplateMgr.ActivateTemplate(aSplitPanel: TSplitPanel; const aName: string);
var
  MyClass: TCustomSplitPanelTemplateClass;
begin
  MyClass := TCustomSplitPanelTemplateClass(GetClass(aName));
  aSplitPanel.ResetToDefault;
  MyClass.SetDefaults(aSplitPanel);
//  TCustomSplitPanelTemplate(GetClass(aName)).SetDefaults(aSplitPanel);
end;

{ TCustomSplitPanelTemplate }

class procedure TCustomSplitPanelTemplate.SetDefaults(P: TSplitPanel);
begin
  // doe niets
end;

{ TSplitPanelTemplate01 }

class procedure TSplitPanelTemplate01.SetDefaults(P: TSplitPanel);
begin
  with P do
  begin
    Align := alClient;
    SplitMode := smVertical;
    PanelCount := 3;
    Panels[0].FixedSize := 20;
    Panels[2].FixedSize := 20; //umisc
  end;
end;

{ TSplitPanelTemplate02 }

class procedure TSplitPanelTemplate02.SetDefaults(P: TSplitPanel);
begin
  with P do
  begin
    Align := alClient;
    SplitMode := smVertical;
    PanelCount := 3;
    Panels[0].FixedSize := 20;
    Panels[1].SplitMode := smHorizontal;
    Panels[1].PanelCount := 2;
    Panels[2].FixedSize := 20;
  end;
end;


initialization
  //RegisterClass(TPanelSplitter); // streaming
  //TemplateMgr.RegisterClass(TSplitPanelTemplate01, 'Eric'); // test
  //TemplateMgr.RegisterClass(TSplitPanelTemplate02, 'Gerrit'); // test
finalization
  FreeAndNil(_TemplateMgr);
end.

