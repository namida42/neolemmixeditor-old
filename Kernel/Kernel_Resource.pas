unit Kernel_Resource;

interface

uses
  Controls, Graphics;

{$R Kernel_Resource.Res}

var
  MxImages10: TImageList; // matrix system images 10x10
  XpImages16: TImageList; // xp bitmaps 16x16
  XpImages24: TImageList; // xp bitmaps 24x24

type
  { TMxGlyph mapped naar TXPBitmap16 of TXPBitmap24 }
  TMXGlyph = (
    TMX_arrowleft_green,
    TMX_arrowleft_green_d,
    TMX_arrowleft_green_h,
    TMX_arrowright_green,
    TMX_arrowright_green_d,
    TMX_arrowright_green_h,
    TMX_copy_clipboard,
    TMX_copy_clipboard_d,
    TMX_copy_clipboard_h,
    TMX_copy_to_folder,
    TMX_copy_to_folder_d,
    TMX_copy_to_folder_h,
    TMX_cut_clipboard,
    TMX_cut_clipboard_d,
    TMX_cut_clipboard_h,
    TMX_delete_x,
    TMX_delete_x_d,
    TMX_delete_x_h,
    TMX_favorites,
    TMX_favorites_d,
    TMX_favorites_h,
    TMX_folders,
    TMX_folders_d,
    TMX_folders_h,
    TMX_folder_closed,
    TMX_folder_closed_d,
    TMX_folder_closed_h,
    TMX_folder_open,
    TMX_folder_open_d,
    TMX_folder_open_h,
    TMX_folder_options,
    TMX_folder_options_d,
    TMX_folder_options_h,
    TMX_history,
    TMX_history_d,
    TMX_history_h,
    TMX_home_d,
    TMX_home_brown,
    TMX_home_brown_h,
    TMX_home_green,
    TMX_home_green_h,
    TMX_home_purple,
    TMX_home_purple_h,
    TMX_home_yellow,
    TMX_home_yellow_h,
    TMX_mail,
    TMX_mail_d,
    TMX_mail_h,
    TMX_move_to_folder,
    TMX_move_to_folder_d,
    TMX_move_to_folder_h,
    TMX_new_document,
    TMX_new_document_d,
    TMX_new_document_h,
    TMX_open_document,
    TMX_open_document_d,
    TMX_open_document_h,
    TMX_paste_clipboard,
    TMX_paste_clipboard_d,
    TMX_paste_clipboard_h,
    TMX_print,
    TMX_print_d,
    TMX_print_h,
    TMX_print_preview,
    TMX_print_preview_d,
    TMX_print_preview_h,
    TMX_properties_doc,
    TMX_properties_doc_d,
    TMX_properties_doc_h,
    TMX_redo,
    TMX_redo_d,
    TMX_redo_h,
    TMX_redo_square,
    TMX_redo_square_d,
    TMX_redo_square_h,
    TMX_refresh_doc,
    TMX_refresh_doc_d,
    TMX_refresh_doc_h,
    TMX_save,
    TMX_save_d,
    TMX_save_h,
    TMX_save_green,
    TMX_save_green_h,
    TMX_search,
    TMX_search_d,
    TMX_search_h,
    TMX_stop,
    TMX_stop_d,
    TMX_stop_h,
    TMX_undo,
    TMX_undo_d,
    TMX_undo_h,
    TMX_undo_square,
    TMX_undo_square_d,
    TMX_undo_square_h,
    TMX_up_folder,
    TMX_up_folder_d,
    TMX_up_folder_h,
    TMX_views,
    TMX_views_d,
    TMX_views_h,
    TMX_none,
    TMX_usespace
  );

  TXPBitmap16 = (
    TXP_arrowleft_green16,
    TXP_arrowleft_green16_d,
    TXP_arrowleft_green16_h,
    TXP_arrowright_green16,
    TXP_arrowright_green16_d,
    TXP_arrowright_green16_h,
    TXP_copy_clipboard16,
    TXP_copy_clipboard16_d,
    TXP_copy_clipboard16_h,
    TXP_copy_to_folder16,
    TXP_copy_to_folder16_d,
    TXP_copy_to_folder16_h,
    TXP_cut_clipboard16,
    TXP_cut_clipboard16_d,
    TXP_cut_clipboard16_h,
    TXP_delete_x16,
    TXP_delete_x16_d,
    TXP_delete_x16_h,
    TXP_favorites16,
    TXP_favorites16_d,
    TXP_favorites16_h,
    TXP_folders16,
    TXP_folders16_d,
    TXP_folders16_h,
    TXP_folder_closed16,
    TXP_folder_closed16_d,
    TXP_folder_closed16_h,
    TXP_folder_open16,
    TXP_folder_open16_d,
    TXP_folder_open16_h,
    TXP_folder_options16,
    TXP_folder_options16_d,
    TXP_folder_options16_h,
    TXP_history16,
    TXP_history16_d,
    TXP_history16_h,
    TXP_home16_d,
    TXP_home_brown16,
    TXP_home_brown16_h,
    TXP_home_green16,
    TXP_home_green16_h,
    TXP_home_purple16,
    TXP_home_purple16_h,
    TXP_home_yellow16,
    TXP_home_yellow16_h,
    TXP_mail16,
    TXP_mail16_d,
    TXP_mail16_h,
    TXP_move_to_folder16,
    TXP_move_to_folder16_d,
    TXP_move_to_folder16_h,
    TXP_new_document16,
    TXP_new_document16_d,
    TXP_new_document16_h,
    TXP_open_document16,
    TXP_open_document16_d,
    TXP_open_document16_h,
    TXP_paste_clipboard16,
    TXP_paste_clipboard16_d,
    TXP_paste_clipboard16_h,
    TXP_print16,
    TXP_print16_d,
    TXP_print16_h,
    TXP_print_preview16,
    TXP_print_preview16_d,
    TXP_print_preview16_h,
    TXP_properties_doc16,
    TXP_properties_doc16_d,
    TXP_properties_doc16_h,
    TXP_redo16,
    TXP_redo16_d,
    TXP_redo16_h,
    TXP_redo_square16,
    TXP_redo_square16_d,
    TXP_redo_square16_h,
    TXP_refresh_doc16,
    TXP_refresh_doc16_d,
    TXP_refresh_doc16_h,
    TXP_save16,
    TXP_save16_d,
    TXP_save16_h,
    TXP_save_green16,
    TXP_save_green16_h,
    TXP_search16,
    TXP_search16_d,
    TXP_search16_h,
    TXP_stop16,
    TXP_stop16_d,
    TXP_stop16_h,
    TXP_undo16,
    TXP_undo16_d,
    TXP_undo16_h,
    TXP_undo_square16,
    TXP_undo_square16_d,
    TXP_undo_square16_h,
    TXP_up_folder16,
    TXP_up_folder16_d,
    TXP_up_folder16_h,
    TXP_views16,
    TXP_views16_d,
    TXP_views16_h
  );

  TXPBitmap24 = (
    TXP_arrowleft_green24,
    TXP_arrowleft_green24_d,
    TXP_arrowleft_green24_h,
    TXP_arrowright_green24,
    TXP_arrowright_green24_d,
    TXP_arrowright_green24_h,
    TXP_copy_clipboard24,
    TXP_copy_clipboard24_d,
    TXP_copy_clipboard24_h,
    TXP_copy_to_folder24,
    TXP_copy_to_folder24_d,
    TXP_copy_to_folder24_h,
    TXP_cut_clipboard24,
    TXP_cut_clipboard24_d,
    TXP_cut_clipboard24_h,
    TXP_delete_x24,
    TXP_delete_x24_d, 
    TXP_delete_x24_h,
    TXP_favorites24, 
    TXP_favorites24_d,
    TXP_favorites24_h, 
    TXP_folders24, 
    TXP_folders24_d,
    TXP_folders24_h,
    TXP_folder_closed24, 
    TXP_folder_closed24_d,
    TXP_folder_closed24_h, 
    TXP_folder_open24,
    TXP_folder_open24_d,
    TXP_folder_open24_h,
    TXP_folder_options24, 
    TXP_folder_options24b, 
    TXP_folder_options24b_d,
    TXP_folder_options24b_h,
    TXP_folder_options24_d,
    TXP_folder_options24_h,
    TXP_history24, 
    TXP_history24b, 
    TXP_history24b_d, 
    TXP_history24b_h, 
    TXP_history24_d,
    TXP_history24_h,
    TXP_home24_d,
    TXP_home_brown24,
    TXP_home_brown24_h, 
    TXP_home_green24,
    TXP_home_green24_h,
    TXP_home_purple24, 
    TXP_home_purple24_h,
    TXP_home_yellow24, 
    TXP_home_yellow24_h,
    TXP_mail24,
    TXP_mail24_d, 
    TXP_mail24_h,
    TXP_move_to_folder24,
    TXP_move_to_folder24_d,
    TXP_move_to_folder24_h,
    TXP_new_document24,
    TXP_new_document24_d,
    TXP_new_document24_h, 
    TXP_open_document24,
    TXP_open_document24_d, 
    TXP_open_document24_h, 
    TXP_paste_clipboard24,
    TXP_paste_clipboard24_d,
    TXP_paste_clipboard24_h,
    TXP_print24, 
    TXP_print24_d, 
    TXP_print24_h, 
    TXP_print_preview24, 
    TXP_print_preview24_d, 
    TXP_print_preview24_h,
    TXP_properties_doc24, 
    TXP_properties_doc24b,
    TXP_properties_doc24b_d,
    TXP_properties_doc24b_h, 
    TXP_properties_doc24_d,
    TXP_properties_doc24_h,
    TXP_redo24, 
    TXP_redo24_d, 
    TXP_redo24_h,
    TXP_redo_square24,
    TXP_redo_square24_d, 
    TXP_redo_square24_h,
    TXP_refresh_doc24,
    TXP_refresh_doc24_d,
    TXP_refresh_doc24_h,
    TXP_save24,
    TXP_save24_d,
    TXP_save24_h, 
    TXP_save_green24, 
    TXP_save_green24_h,
    TXP_search24, 
    TXP_search24_d, 
    TXP_search24_h, 
    TXP_stop24, 
    TXP_stop24_d,
    TXP_stop24_h, 
    TXP_undo24, 
    TXP_undo24_d, 
    TXP_undo24_h,
    TXP_undo_square24,
    TXP_undo_square24_d,
    TXP_undo_square24_h,
    TXP_up_folder24,
    TXP_up_folder24_d,
    TXP_up_folder24_h,
    TXP_views24,
    TXP_views24_d,
    TXP_views24_h
  );

  TMXBitmap10 = (
    TMX_led_aan10,
    TMX_led_blauw_aan10,
    TMX_led_blauw_uit10,
    TMX_led_geel_aan10,
    TMX_led_geel_uit10,
    TMX_led_groen_aan10,
    TMX_led_groen_uit10,
    TMX_led_rood_aan10,
    TMX_led_rood_uit10,
    TMX_led_uit10,
    TMX_sort_asc10,
    TMX_sort_desc10,
    TMX_blank10,
    TMX_led_flat_off10,
    TMX_led_flat_green10
  );

//function mx

implementation

uses
  SysUtils, TypInfo, UMisc;

procedure InitImages;
var
  mx10: TMxBitmap10;
  xp16: TXpBitmap16;
  xp24: TXpBitmap24;
  ResName : string;
  BMP: TBitmap;
begin
  BMP := TBitmap.Create;

  try


  with MxImages10 do
  begin
    Height := 10;
    Width := 10;
    for mx10 := Low(TMxBitmap10) to High(TMxBitmap10) do
    begin
      Resname := GetEnumName(TypeInfo(TMxBitmap10), Integer(mx10));
      { converteer typenaam naar resourcenaam }
      ResName := CutLeft(ResName, 4);
      BMP.LoadFromResourceName(HINSTANCE, ResName);
      if AddMasked(BMP, BMP.TransparentColor) < 0 then
        AppError('Kernel_Resouurce.InitImages ' + Resname + ' kan niet toegevoegd worden aan MxImages10');
    end;
  end;

  with XpImages16 do
  begin
    Height := 16;
    Width := 16;
    for xp16 := Low(TXpBitmap16) to High(TXpBitmap16) do
    begin
      Resname := GetEnumName(TypeInfo(TXpBitmap16), Integer(xp16));
      { converteer typenaam naar resourcenaam }
      ResName := CutLeft(ResName, 4);
      BMP.LoadFromResourceName(HINSTANCE, ResName);
      if AddMasked(BMP, BMP.TransparentColor) < 0 then
        AppError('Kernel_Resouurce.InitImages ' + Resname + ' kan niet toegevoegd worden aan XpImages16');
    end;
  end;


  with XpImages24 do
  begin
    Height := 24;
    Width := 24;
    for xp24 := Low(TXpBitmap24) to High(TXpBitmap24) do
    begin
      Resname := GetEnumName(TypeInfo(TXpBitmap24), Integer(xp24));
      { converteer typenaam naar resourcenaam }
      ResName := CutLeft(ResName, 4);
      BMP.LoadFromResourceName(HINSTANCE, ResName);
      if AddMasked(BMP, BMP.TransparentColor) < 0 then
        AppError('InitializeGlobals ImageList Bitmap ' + Resname + ' kan niet geladen worden');
    end;
  end;

  finally
    BMP.Free;
  end;

end;

initialization
  MxImages10 := TImageList.Create(nil);
  XpImages16 := TImageList.Create(nil);
  XpImages24 := TImageList.Create(nil);
  InitImages;
finalization
  FreeAndNil(MxImages10);
  FreeAndNil(XpImages16);
  FreeAndNil(XpImages24);
end.

