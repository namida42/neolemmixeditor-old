object FormEditMatrixOptions: TFormEditMatrixOptions
  Left = 149
  Top = 240
  Width = 709
  Height = 567
  Caption = 'FormEditMatrixOptions'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object CheckListBox1: TCheckListBox
    Left = 8
    Top = 8
    Width = 217
    Height = 441
    ItemHeight = 13
    Items.Strings = (
      'poHorzLines'
      'poVertLines'
      'poCalculatedRowHeights'
      'poCalculatedColWidths     '
      'poTransparentVertLines'
      'poTransparentHorzLines'
      'poHidePartialRows'
      'poHidePartialCols'
      'poHidePartialVertLines'
      'poHidePartialHorzLines'
      'poHideLastVertLine'
      'poHideLastHorzLine'
      'poTransparentBackGround'
      'poDrawFocusRect'
      'poHoveringCells'
      'poHeaderHighlightStrings'
      'poIncrementalSearchHighlight'
      'poMixStyles')
    TabOrder = 0
  end
end
