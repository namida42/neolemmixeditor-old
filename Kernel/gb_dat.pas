unit gb_dat;

interface
uses gb_str,umisc,sysutils;

procedure dat2num(const dat:string;var j,m,d:integer);
function num2dat(j,m,d:integer):string;
procedure tijd2num(var tijd:string;var u,m,s:integer);
function jaar(dat:string):integer;
function maand(dat:string):integer;
function dag(dat:string):integer;
function schrikkeljaar(dat:string):boolean;
function dagen_in_maand(jaar,mnd:integer):integer;
function dat2dt(dat1:string):integer;
function maandteller(dat1:string):integer;
function daysbtwn(dat1,dat2:string):integer;
function monthsbtwn(dat1,dat2:string):integer;
function yearsbtwn(dat1,dat2:string):integer;
function eerste_van_maand(dat:string):string;
function dow2(dat:string):integer; {day of week}
function weekdag_van_maand(dag,nr,jaar,mnd:integer):string;
function dag_strl(dagnr:integer;t:char):string;
function dag_strk(dagnr:integer;t:char):string;
function mnd_strk(mndnr:integer;t:char):string;
function mnd_strl(mndnr:integer;t:char):string;
function dat_strl(dat:string;t:char):string;
function dat_strm(dat:string;t:char):string;
function dat_strk(dat:string;t:char):string;
function dat_l(const dat:string):string;
function dat_m(const dat:string):string;
function dat_k(const dat:string):string;
function AddDays(dat1:string;a:integer):string;
function AddMonths(dat1:string;a:integer):string;
function weeknr(dat:string):integer;
function checkdate(s:string): boolean;
function correctdate(dat:string):string;
function rnddatum:string;
function sysdatum:string;

implementation
//uses logger;
function rnddatum:string;
var s:string;
begin
  s:=dtos(date);
  result:=AddDays(s,-random(300));
end;
{****************************************************************************}
function sysdatum:string;
var s:string;
begin
  s:=dtos(date);
//  vlog('sysdat='+s);
  result:=s;
end;

procedure dat2num(const dat:string;var j,m,d:integer);
begin
  d:=s2i(rstr(dat,2));
  m:=s2i(mstr(dat,5,2));
  j:=s2i(lstr(dat,4));
//  vlog('jaar='+dat+' '+i2s(j));
end;
{****************************************************************************}
function num2dat(j,m,d:integer): string;
begin
  result:=LeadZeroStr(j,4)+LeadZeroStr(m,2)+LeadZeroStr(d,2);
end;
{****************************************************************************}
procedure tijd2num(var tijd:string;var u,m,s:integer);
begin
  u:=sti(lstr(tijd,2));
  m:=sti(mstr(tijd,4,2));
  s:=sti(rstr(tijd,2));
end;
{****************************************************************************}
function jaar(dat:string):integer;
begin
  jaar:=sti(lstr(dat,4));
end;
{*****************************************************************************}
function maand(dat:string):integer;
begin
  maand:=sti(copy(dat,5,2));
end;
{*****************************************************************************}
function dag(dat:string):integer;
begin
  dag:=sti(copy(dat,7,2));
end;
{****************************************************************************}
function schrikkeljaar(dat:string):boolean;
begin
  if sti(lstr(dat,4)) mod 4=0 then
  schrikkeljaar:=true else schrikkeljaar:=false;
end;
{****************************************************************************}
function dagen_in_maand(jaar,mnd:integer):integer;
var m:array[1..12] of integer;
begin
  m[1]:=31;if schrikkeljaar(gln(jaar,4)) then m[2]:=29 else m[2]:=28;
  m[3]:=31;m[4]:=30;m[5]:=31;m[6]:=30;m[7]:=31;
  m[8]:=31;m[9]:=30;m[10]:=31;m[11]:=30;m[12]:=31;
  if ((mnd>=1) and (mnd<=12)) then
  dagen_in_maand:=m[mnd] else dagen_in_maand:=0;
end;
{****************************************************************************}
function dat2dt(dat1:string):integer;
{datum to dagteller vanaf 1/1/80 t/m 31/12/2099}
var jr,md,dg,dt,t{,h}:integer;
begin
  dat2num(dat1,dg,md,jr);
  if ((jr>=1980) and (jr<=2099)) then
  begin
    dt:=0;
    if jr>1980 then;
    for t:=1980 to jr-1 do
    begin
     if schrikkeljaar(gln(t,4)+'0101'+gln(t,2)) then dt:=dt+365
                                                else dt:=dt+366;
     end;
    if md>1 then
    for t:=1 to md-1 do dt:=dt+dagen_in_maand(jr,t);
    dt:=dt+dg;
    dat2dt:=dt;
  end else dat2dt:=0;
end;
{*****************************************************************************}
function maandteller(dat1:string):integer;
var jr,dg,md,mt,{h,}t:integer;
begin
  dat2num(dat1,dg,md,jr);
  if ((jr>=1980) and (jr<=2099)) then
  begin
    mt:=0;
    if jr>1980 then
    for t:=1980 to jr-1 do mt:=mt+12;
    mt:=mt+md;
    maandteller:=mt;
  end else maandteller:=0;
end;
{*****************************************************************************}
function daysbtwn(dat1,dat2:string):integer;
begin
  daysbtwn:=dat2dt(dat2)-dat2dt(dat1);
end;
{*****************************************************************************}
function monthsbtwn(dat1,dat2:string):integer;
var h:integer;
begin
  h:=maandteller(dat2)-maandteller(dat1);
  if dag(dat2)<dag(dat1) then dec(h);
  result:=h;
end;
{*****************************************************************************}
function yearsbtwn(dat1,dat2:string):integer;
var h:integer;
begin
  h:=s2i(lstr(dat1,4))-s2i(lstr(dat2,4));
  if h<>0 then
  begin
  if maand(dat2)>maand(dat1) then dec(h)
  else if maand(dat2)=maand(dat1) then
            if dag(dat2)>dag(dat1) then dec(h);
  end;
  result:=h;
end;
{****************************************************************************}
function eerste_van_maand(dat:string):string;
begin
  eerste_van_maand:=lstr(dat,6)+'01';
end;
{****************************************************************************}
function dag_strl(dagnr:integer;t:char):string;
begin
  if upcase(t)='N' then
  case dagnr of
  1: dag_strl:='zondag';          2: dag_strl:='maandag';
  3: dag_strl:='dinsdag';         4: dag_strl:='woensdag';
  5: dag_strl:='donderdag';       6: dag_strl:='vrijdag';
  7: dag_strl:='zaterdag';        else dag_strl:='onbekend';
  end else
  case dagnr of
  1: dag_strl:='Sunday';          2: dag_strl:='Monday';
  3: dag_strl:='Tuesday';         4: dag_strl:='Wednesday';
  5: dag_strl:='Thursday';        6: dag_strl:='Friday';
  7: dag_strl:='Saturdag';        else dag_strl:='Unknown';
  end;
end;
{****************************************************************************}
function dag_strk(dagnr:integer;t:char):string;
begin
//  vlog(i2s(dagnr));
  if upcase(t)='N' then
  case dagnr of
  1: dag_strk:='zo';       2: dag_strk:='ma';
  3: dag_strk:='di';       4: dag_strk:='wo';
  5: dag_strk:='do';       6: dag_strk:='vr';
  7: dag_strk:='za';       else dag_strk:='dd';
  end else
  case dagnr of
  1: dag_strk:='Su';        2: dag_strk:='Mo';
  3: dag_strk:='Tu';        4: dag_strk:='We';
  5: dag_strk:='Th';        6: dag_strk:='Fr';
  7: dag_strk:='Sa';        else dag_strk:='dd';
  end;
end;
{****************************************************************************}
function mnd_strk(mndnr:integer;t:char):string;
begin
  if upcase(t)='N' then
  case mndnr of
  1: mnd_strk:='jan';       2: mnd_strk:='feb';
  3: mnd_strk:='mrt';       4: mnd_strk:='apr';
  5: mnd_strk:='mei';       6: mnd_strk:='jun';
  7: mnd_strk:='jul';       8: mnd_strk:='aug';
  9: mnd_strk:='sep';      10: mnd_strk:='okt';
 11: mnd_strk:='nov';      12: mnd_strk:='dec';
  else mnd_strk:='mmm';
  end else
  case mndnr of
  1: mnd_strk:='Jan';       2: mnd_strk:='Feb';
  3: mnd_strk:='Mar';       4: mnd_strk:='Apr';
  5: mnd_strk:='May';       6: mnd_strk:='Jun';
  7: mnd_strk:='Jul';       8: mnd_strk:='Aug';
  9: mnd_strk:='Sep';      10: mnd_strk:='Oct';
 11: mnd_strk:='Nov';      12: mnd_strk:='Dec';
  else mnd_strk:='mmm';
  end;
end;
{****************************************************************************}
function mnd_strl(mndnr:integer;t:char):string;
begin
  if upcase(t)='N' then
  case mndnr of
  1: mnd_strl:='januari';       2: mnd_strl:='februari';
  3: mnd_strl:='maart';         4: mnd_strl:='april';
  5: mnd_strl:='mei';           6: mnd_strl:='juni';
  7: mnd_strl:='juli';          8: mnd_strl:='augustus';
  9: mnd_strl:='septeber';      10: mnd_strl:='oktober';
 11: mnd_strl:='november';      12: mnd_strl:='december';
  else mnd_strl:='mmmmmmmm';
  end else
  case mndnr of
  1: mnd_strl:='January';       2: mnd_strl:='February';
  3: mnd_strl:='March';         4: mnd_strl:='April';
  5: mnd_strl:='May';           6: mnd_strl:='June';
  7: mnd_strl:='July';          8: mnd_strl:='August';
  9: mnd_strl:='September';     10: mnd_strl:='October';
 11: mnd_strl:='November';      12: mnd_strl:='December';
  else mnd_strl:='mmmmmmmm';
  end;
end;
{****************************************************************************}
function dow2(dat:string):integer; {day of week}
const refday=3; {1/1/80 is dinsdag}
var i,dag,mnd,jr:integer;dagnr,dpj:integer;aantdag:integer;
    adtmd:integer;
begin
  dat2num(dat,dag,mnd,jr);
{  writeln(jr,' ',mnd,' ',dag,' ');}
  aantdag:=0;dagnr:=8;
  if ((jr>=1900) and (jr<=2099)) then
  if jr>=1980 then
  begin
    for i:=1980 to jr-1 do
    begin
      if schrikkeljaar(gln(i,4)) then dpj:=366 else dpj:=365;
      aantdag:=aantdag+dpj;
    end;
    for i:=1 to mnd-1 do aantdag:=aantdag+dagen_in_maand(jr,i);
    aantdag:=aantdag+dag-1;
{    writeln('na peildag: ',aantdag,' ',aantdag mod 7);}
    dagnr:=refday+(aantdag mod 7);
    if dagnr>7 then dagnr:=dagnr-7;
  end else
  begin
    {datum voor peildag 1/1/1980}
    for i:=jr+1 to 1980-1 do
    begin
      if schrikkeljaar(gln(i,4)) then dpj:=366 else dpj:=365;
      aantdag:=aantdag+dpj;
    end;
    {bereken aantal dagen tot aan meetdag}
    adtmd:=0;
    for i:=1 to mnd-1 do adtmd:=adtmd+dagen_in_maand(jr,i);
    {bereken aantal dagen vanaf meetdag tot einde jaar}
    if schrikkeljaar(gln(jr,4)) then dpj:=366 else dpj:=365;
    aantdag:=aantdag+dpj-adtmd;
{    writeln('voor peildag: ',aantdag);}
    dagnr:=refday-(aantdag mod 7);
    if dagnr<1 then dagnr:=dagnr+7;
  end;
  dow2:=dagnr;
end;
{****************************************************************************}
function weekdag_van_maand(dag,nr,jaar,mnd:integer):string;
var wdt,d{{m,j}:integer;sd,dat:string[8];
begin
  {bepaalt de datum van de NR=[1e, 2e, 3e en 4e] DAG (zo=1,ma=2)
   van de maand, in het gegeven jaar en maand. NR=5 geeft de laatste
   weekdag van de maand}

  sd:=gln(jaar,4)+gln(mnd,2);
  wdt:=0;d:=0;
  if nr<=4 then
  begin
  repeat
    inc(d);dat:=sd+gln(d,2);
    if dow2(dat)=dag then inc(wdt);
{    writeln(sd,' ',d,' ',dow(sd),' ',wdt,'         ');}
  until nr=wdt;end
  else
  begin
    wdt:=0;nr:=1;d:=dagen_in_maand(jaar,mnd)+1;
    repeat
     dec(d);dat:=sd+gln(d,2);
     if dow2(dat)=dag then inc(wdt);
{     writeln(sd,' ',d,' ',dow(sd),' ',wdt,'         ');}
    until nr=wdt;
  end;
  weekdag_van_maand:=dat;
end;
{****************************************************************************}
function dat_strl(dat:string;t:char):string;
var d,m,j:integer;
begin
  dat2num(dat,d,m,j);
  dat_strl:=dag_strl(dow2(dat),t)+', '+trim(its(d,2))+' '+mnd_strl(m,t)+' '+gln(j,4);
end;
{****************************************************************************}
function dat_strm(dat:string;t:char):string;
var d,m,j:integer;
begin
  if dat<>'' then
  begin
  dat2num(dat,d,m,j);
  result:=dag_strk(dow2(dat),t)+', '+trim(its(d,2))+' '+mnd_strk(m,t)+' '+gln(j,4);
  end else result:='';
end;
{****************************************************************************}
function dat_strk(dat:string;t:char):string;
var d,m,j:integer;
begin
  result:='';
  if dat='' then exit;
  dat2num(dat,d,m,j);
//  vlog('dat='+dat);
//  vlog(i2s(d));
//  vlog(i2s(m));
//  vlog(i2s(j));
  result:=dag_strk(dow2(dat),t)+', '+its(d,2)+' '+mnd_strk(m,t)+' '+gln(j,4);
//  vlog('sysdatum='+result);
end;

function dat_l(const dat:string):string;
begin
  result:=dat_strl(dat,'N');
end;
function dat_m(const dat:string):string;
begin
//  vlog('dat_m:'+dat);
  result:=dat_strm(dat,'N');
//  vlog('dat_m:'+result);
end;
function dat_k(const dat:string):string;
begin
  result:=dat_strk(dat,'N');
end;
{*****************************************************************************}
function adddays(dat1:string;a:integer):string;
var m:array[1..12] of integer;h,dat:string;
    {i,}jr,md,dg{,dt,sd, r}:integer;
begin
  h:=dat1;
  m[1]:=31;if schrikkeljaar(dat1) then m[2]:=29 else m[2]:=28;m[3]:=31;m[4]:=30;
  m[5]:=31;m[6]:=30;m[7]:=31;m[8]:=31;m[9]:=30;m[10]:=31;m[11]:=30;m[12]:=31;
  md:=sti(mstr(dat1,5,2));
  dg:=sti(rstr(dat1,2));
  jr:=sti(lstr(dat1,4));
  if a>0 then
  if a in [1..28] then
  begin
  if dg+a<=m[md] then dg:=dg+a
                 else if md<12 then begin dg:=dg+a-m[md];inc(md);end
                               else begin dg:=dg+a-m[md];md:=1;inc(jr);end;
  end;
  if a<0 then
  begin a:=abs(a);
  if a in [1..28] then
  begin
  if dg-a>0 then dg:=dg-a
            else if md>1 then begin dec(md);dg:=dg-a+m[md];end
                         else begin dg:=dg+m[12]-a;md:=12;dec(jr);end;
  end;
  end;

  dat:=gln(jr,4)+gln(md,2)+gln(dg,2);
  adddays:=dat;
{  if check_dat(dat) then addays:=dat
                    else addays:=h;}
end;
{*****************************************************************************}
function addmonths(dat1:string;a:integer):string;
var m:array[1..12] of integer;d,dat:string;
    {i,}jr,md,dg{,dt,sd}:integer;
begin
  dat:=dat1;
  m[1]:=31;if schrikkeljaar(dat1) then m[2]:=29 else m[2]:=28;m[3]:=31;m[4]:=30;
  m[5]:=31;m[6]:=30;m[7]:=31;m[8]:=31;m[9]:=30;m[10]:=31;m[11]:=30;m[12]:=31;
  md:=sti(mstr(dat1,5,2));
  dg:=sti(rstr(dat1,2));
  jr:=sti(lstr(dat1,4));
  if a>0 then
  begin
    if md+a<=12 then md:=md+a
                else begin md:=md+a-12;inc(jr);end;
    if dg>m[md] then dg:=m[md];
  end
  else
  begin
    if md+a>=1 then md:=md+a
               else begin md:=md+12+a;dec(jr);end;
    if dg>m[md] then dg:=m[md];
  end;

  d:=gln(jr,4)+gln(md,2)+gln(dg,2);
  if not schrikkeljaar(d) then
   if dag(d)=29 then
    if maand(d)=2 then d:=adddays(d,-1);

  addmonths:=d;
{  if check_dat(d) then admonths:=d
                  else admonths:=dat;}
end;
{****************************************************************************}
function weeknr(dat:string):integer;
var dag1,ldag,ma_1e:string;wk_ldag,cor,btwn,wk:integer;
begin
  {bepaal eerste maandag van het jaar (2=ma)}
  dag1:=lstr(dat,4)+'0101';cor:=0;
  ldag:=lstr(dat,4)+'1231';
  if dow2(dag1) in [5,6,7,1] then
  begin
     btwn:=daysbtwn(dag1,dat);
     wk:=(btwn div 7)+1;
     writeln('1/1 = '+dat_strl(dag1,'N'));
     writeln('cor=',cor,' btwn=',btwn,' week= ',wk);
  end else
  begin
     ma_1e:=weekdag_van_maand(2,1,jaar(dat),1);
     btwn:=daysbtwn(ma_1e,dat);
     cor:=daysbtwn(ma_1e,lstr(dat,4)+'0101');
     wk:=((btwn+abs(cor)) div 7)+1;
     wk_ldag:=((daysbtwn(ma_1e,ldag)+abs(cor)) div 7)+1;
     if ((wk_ldag=53) and (dow2(ldag)<>1)) then wk:=wk_ldag;
     writeln('1e ma = '+dat_strl(ma_1e,'N'));
     writeln('cor=',cor,' btwn=',btwn,' week= ',wk,' wk Ldag=',wk_ldag);
  end;
  writeln(dat_strl(dat,'N'));
  weeknr:=wk;
end;
{****************************************************************************}
function checkdate(s:string): boolean;
var d,m,j:integer;
begin
  result:=true; //### eric warning delphi
 dat2num(s,d,m,j);
//  if d>0 then
//  if d<}
end;

function correctdate(dat:string):string;
var dat2,today:string;ok:boolean;
    jr,dag,mnd:integer;
begin
  ok:=false;
  dat2:=repstr(dat,'.','-');dat2:=repstr(dat2,'.','-');
  today:=dtos(date);
  if keepstr(dat,'0123456789')=dat then
  begin
    if length(dat)=4 then begin result:=lstr(today,4)+lstr(dat,2)+rstr(dat,2);ok:=true;end;
    if length(dat)=6 then begin if s2i(rstr(dat,2))>s2i(mstr(today,3,2))+40
                                then result:='19'+rstr(dat,2)+lstr(dat,2)+rstr(dat,2)
                                else result:='20'+rstr(dat,2)+lstr(dat,2)+rstr(dat,2);
                                ok:=true;end;
//    if length(dat)=8 then
//    if lstr(dat,2)<>'19' then  begin result:=rstr(dat,4)+mstr(dat,3,2)+lstr(dat,2);ok:=true;end;
  end else
  if countchar(dat,'-')=1 then
  begin
    dag:=s2i(splitstr(dat,'-',1));
    mnd:=s2i(splitstr(dat,'-',2));
    result:=lstr(today,4)+gln(mnd,2)+gln(dag,2);
    ok:=true;
  end else
  if countchar(dat,'-')=2 then
  begin
    dag:=s2i(splitstr(dat,'-',1));
    mnd:=s2i(splitstr(dat,'-',2));
    jr:=s2i(splitstr(dat,'-',3));
    if jr<100 then if jr>40 then jr:=1900+jr else jr:=2000+jr;
    result:=gln(jr,4)+gln(mnd,2)+gln(dag,2);
    ok:=true;
  end;
  if not ok then result:=dat;
end;

begin
end.
