unit MxInspector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls,
  ULog, UMisc, Mx;

type
  TMatrixInspectorForm = class(TForm, IMatrixInspector)
    PageControl1: TPageControl;
    PageTab: TTabSheet;
    TabSheet2: TTabSheet;
    Bevel2: TBevel;
    MasterLabel2: TStaticText;
    MasterLabel3: TStaticText;
    MasterLabel4: TStaticText;
    MasterLabel5: TStaticText;
    MasterLabel6: TStaticText;
    MasterLabel8: TStaticText;
    MasterLabel9: TStaticText;
    MasterLabel10: TStaticText;
    MasterLabel11: TStaticText;
    MasterLabel12: TStaticText;
    MasterLabel13: TStaticText;
    MasterLabel1: TStaticText;
    LastCol: TStaticText;
    LastRow: TStaticText;
    VisibleCols: TStaticText;
    VisibleRows: TStaticText;
    FullVisibleCols: TStaticText;
    FullVisibleRows: TStaticText;
    GridRight: TStaticText;
    GridBottom: TStaticText;
    LastFullVisibleCol: TStaticText;
    LastFullVisibleRow: TStaticText;
    Col: TStaticText;
    Row: TStaticText;
    StaticText1: TStaticText;
    FirstCol: TStaticText;
    StaticText3: TStaticText;
    FirstRow: TStaticText;
    TxtVisibleRows: TStaticText;
    StaticText2: TStaticText;
    StaticText4: TStaticText;
    TxtVisibleCols: TStaticText;
    StaticText5: TStaticText;
    OffsetCol: TStaticText;
    StaticText7: TStaticText;
    OffsetROw: TStaticText;
    StaticText6: TStaticText;
    TxtHiddenCols: TStaticText;
    StaticText9: TStaticText;
    TxtHiddenRows: TStaticText;
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    fMatrix: TCustomMatrix;
  protected
    procedure ConnectMatrix(aMatrix: TCustomMatrix);
    procedure DisconnectMatrix(aMatrix: TCustomMatrix);
    procedure PageInfoChanged(const aPageInfo: TPageInfo);
    procedure ColRowChanged(aCol, aRow: Integer);
  public
  end;

var
  MatrixInspectorForm: TMatrixInspectorForm;

implementation

{$R *.DFM}

{ TMatrixInspectorForm }

procedure TMatrixInspectorForm.ConnectMatrix(aMatrix: TCustomMatrix);
begin
  fMatrix := aMatrix;
//  Log(['Matrix Connected']);
end;

procedure TMatrixInspectorForm.DisconnectMatrix(aMatrix: TCustomMatrix);
begin
  fMatrix := nil;
//  Log(['Matrix Disconnected']);
end;

procedure TMatrixInspectorForm.PageInfoChanged(const aPageInfo: TPageInfo);
var
  i: integer;
  S: string;
begin
  with aPageInfo.Vert do
  begin
    S := '';
    for i := 0 to VisibleCount - 1 do
    begin
      if S <> '' then S := S + ', ';
      S := S + i2s(Indices[i]);
    end;
    TxtVisibleRows.Caption := S;
    FirstRow.Caption := i2s(FirstVisible);
    OffsetRow.Caption := i2s(Offset);
    LastRow.Caption := i2s(LastVisible);
    LastFullVisibleRow.Caption := i2s(LastFullVisible);
    VisibleRows.Caption := i2s(VisibleCount);
    FullVisibleRows.Caption := i2s(FullVisibleCount);
    GridBottom.Caption := i2s(GridExtent);
  end;

  with aPageInfo.Horz do
  begin
    S := '';
    for i := 0 to VisibleCount - 1 do
    begin
      if S <> '' then S := S + ', ';
      S := S + i2s(Indices[i]);
    end;
    TxtVisibleCols.Caption := S;
    TxtHiddenCols.Caption := i2s(HiddenCount);
    FirstCol.Caption := i2s(FirstVisible);
    OffsetCol.Caption := i2s(Offset);
    LastCol.Caption := i2s(LastVisible);
    LastFullVisibleCol.Caption := i2s(LastFullVisible);
    VisibleCols.Caption := i2s(VisibleCount);
    FullVisibleCols.Caption := i2s(FullVisibleCount);
    GridRight.Caption := i2s(GridExtent);
  end;
end;

procedure TMatrixInspectorForm.FormDestroy(Sender: TObject);
begin
  if fMatrix <> nil then
    if TMatrix(fMatrix).Inspector = IMatrixInspector(Self) then
    begin
      TMatrix(fMatrix).Inspector := nil;
//      Log(['freenotifymatrix']);
    end;
end;

procedure TMatrixInspectorForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TMatrixInspectorForm.ColRowChanged(aCol, aRow: Integer);
begin
  Col.Caption := i2s(aCol);
  Row.Caption := i2s(aRow);
end;

end.

