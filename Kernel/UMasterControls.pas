unit UMasterControls;

interface

uses
  Windows, Messages, Forms, SysUtils, Classes, Graphics, Controls, StdCtrls,
  Buttons, ExtCtrls, ComCtrls, CommCtrl,
  UMisc, ULog;

const
  MM_HINTFOCUS = WM_USER + 1000;

var
 xxxx:integer; 

type
  TMMHintFocus = packed record
    Msg: Cardinal;
    Control: TControl;
    Unused: Longint;
    Result: Longint;
  end;

type
  TCustomMasterLabel = class(TCustomLabel)
  private
    FFontColor      : TColor; { originele fontkleur }
    FColorHighLight : TColor;
    procedure SetColorHighLight(const Value: TColor);
    procedure CMFocusChanged(var Message: TCMFocusChanged); message CM_FOCUSCHANGED;
  protected
    procedure Click; override;
        { focus focuscontrol }
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
        { verander highlight bij verander focus van focuscontrol }
//    procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Align;
    property Alignment;
    property Anchors;
    property AutoSize default False;
    property BiDiMode;
    property Caption;
    property Color;
    property ColorHighLight: TColor read FColorHighLight write SetColorHighLight default clBlue;
    property Constraints;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property FocusControl;
    property Font;
    property ParentBiDiMode;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowAccelChar;
    property ShowHint;
    property Transparent;
    property Layout default tlCenter;
    property Visible;
    property WordWrap;
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;

  TMasterLabel = class(TCustomMasterLabel)
  published
    property Align;
    property Alignment;
    property Anchors;
    property AutoSize;
    property BiDiMode;
    property Caption;
    property Color;
    property ColorHighLight;
    property Constraints;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property FocusControl;
    property Font;
    property ParentBiDiMode;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowAccelChar;
    property ShowHint;
    property Transparent;
    property Layout default tlCenter;
    property Visible;
    property WordWrap;
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnMouseDown;
//    property OnMouseEnter;
//    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;

  TAutoCompleteEvent = procedure(Sender: TObject; const InString: string; var OutString: string) of object;

  TEditEx = class(TEdit)
  private
    fComponentSource: TComponent;
    fComponentProperty: string;
    fItems: TStrings;
    fOnAutoComplete: TAutoCompleteEvent;
    fCaseSensitive: Boolean;
    fIsPickList: Boolean;
    fValidChars: TCharSet;
//    procedure UpdateComponentSource;
    procedure SetItems(const Value: TStrings);
    procedure SetValidChars(const Value: TCharSet);
    // todo: na exit ook update
  protected
    procedure CreateWnd; override;
    function AutoCompleteStr(const S: string): string;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
  public
    constructor Create(aOwner: TComponent); override;
    procedure AfterConstruction; override;
    destructor Destroy; override;
    property ValidChars: TCharSet read fValidChars write SetValidChars;
  published
    property ComponentSource: TComponent read fComponentSource write fComponentSource;
    property ComponentProperty: string read fComponentProperty write fComponentProperty;
    property Items: TStrings read fItems write SetItems;
    property OnAutoComplete: TAutoCompleteEvent read fOnAutoComplete write fOnAutoComplete;
    property CaseSensitive: Boolean read fCaseSensitive write fCaseSensitive;
    property IsPickList: Boolean read fIsPickList write fIsPickList;
  end;


  TCheckBoxEx = class(TCheckBox)
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  end;

  TTreeViewEx = class(TTreeView)
  private
//    procedure CNNotify(var Message: TWMNotify); message CN_NOTIFY;
  published
    property OnMouseWheelDown;
    property OnMouseWheelUp;
//    property OnEditCancel
  end;

  TBlinkType = (btNone, btSlow, btFast);

  TBarTextItem = class(TCollectionItem)
  private
    fText: string;
    fBlinkColor: TColor;
    fColor: TColor;
    fBlink: boolean;
    procedure SetBlinkColor(const Value: TColor);
    procedure SetColor(const Value: TColor);
    procedure SetText(const Value: string);
    procedure SetBlink(const Value: boolean);
  protected
  public
    constructor Create(Collection: TCollection); override;
  published
    property Text: string read fText write SetText;
    property Color: TColor read fColor write SetColor default clLime;
    property BlinkColor: TColor read fBlinkColor write SetBlinkColor default clBlack;
    property Blink: boolean read fBlink write SetBlink;
  end;

  TTicketBar = class;

  TBarTextCollection = class(TCollection)
  private
    fTicketBar: TTicketBar;
    fOnUpdate: TNotifyEvent;
  protected
    function GetOwner: TPersistent; override;
    procedure Update(AItem: TCollectionItem); override;
    property OnUpdate: TNotifyEvent read fOnUpdate write fOnUpdate;
  public
    constructor Create;
    function Add: TBarTextItem;
//    property Items: TBarTextItem read GetItem;
  published
  end;

  TTicketBar = class(TCustomControl)
  private
    fBorderStyle: TBorderStyle;
    fTimer: TTimer;
    fBarText: TBarTextCollection;
    fShift: integer;
    fPixelShift: integer;
    fInterval: integer;
    fBlinkSpeed: integer;
    fSeperator: TBarTextItem;
    fDrawBitmap: TBitmap;
    fBlinkBitmap: TBitmap;
    fTempBitmap: TBitmap;
    fTextRect: TRect;
    fTextWidth: integer;
    fTextHeight: integer;
    fBlinkStart: integer;
    fBlinkTime: integer;
    fUseBlink: boolean;
    //fSettingText: Boolean;
    procedure BitmapResize(aBitmap: TBitmap; aWidth, aHeight: integer);
    procedure SetBarText(const Value: TBarTextCollection);
    procedure BarTextChanged(Sender: TObject);
    procedure DoOnTimer(Sender: TObject);
    procedure SetInterval(const Value: integer);
    procedure WMEraseBkgnd(var Message: TWmEraseBkgnd); message WM_ERASEBKGND;
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
    procedure DoAssignText(const S: string);
    procedure SetBorderStyle(const Value: TBorderStyle);
//    function GetText: string;
//    procedure SetText(const Value: string);
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DefaultHandler(var Message); override;
    procedure Loaded; override;
    procedure Start;
    procedure Stop;
    procedure Resume;
    property DrawBitmap: TBitmap read fDrawBitmap;
    procedure test;
  published
    property Align;
    property Anchors;
    property BorderStyle: TBorderStyle read fBorderStyle write SetBorderStyle;
    property Color default clBlack;
    property Font;
    property Width default 120;
    property Height default 24;
    property Visible;
    property BarText: TBarTextCollection read fBarText write SetBarText;
    property Text;
    property Interval: integer read fInterval write SetInterval default 100;
    property Shift: integer read fShift write fShift default 1;
    property Seperator: TBarTextItem read fSeperator write fSeperator;
    property BlinkSpeed: integer read fBlinkSpeed write fBlinkSpeed;
  end;

{type
    TTestPropertyCategory = class(TPropertyCategory)
    public
      class function Name: string; override;
      class function Description: string; override;
    end;}


//procedure Register;

implementation

uses
  Math, TypInfo, UWinTools;

//uwintools
{class function TTestPropertyCategory.Description: string;
begin
  Result := 'My Category';
end;

class function TTestPropertyCategory.Name: string;
begin
  Result := 'TestProp';
end;}



{procedure Register;
begin
  RegisterComponents('Master', [TMasterLabel, TEditEx, TComboBoxEx, TCheckBoxEx, TTicketBar]);
end;}

{ TCustomMasterLabel }

constructor TCustomMasterLabel.Create(AOwner: TComponent);
begin
  inherited;
  FColorHighLight := clBlue;
  Layout := tlCenter;
  AutoSize := False;
end;

procedure TCustomMasterLabel.Click;
begin
  inherited Click;
  if not (csDesigning in ComponentState) and Assigned(FocusControl) then
    if FocusControl.CanFocus then
      FocusControl.SetFocus;
end;

procedure TCustomMasterLabel.CMFocusChanged(var Message: TCMFocusChanged);
begin
  inherited;
  with Message do
    if Sender = FocusControl then
      Font.Color := FColorHighLight
    else
      Font.Color := FFontColor;
end;

procedure TCustomMasterLabel.SetColorHighLight(const Value: TColor);
begin
  FColorHighLight := Value;
end;


procedure TCustomMasterLabel.CMEnabledChanged(var Message: TMessage);
begin
  inherited;
//  Log(['enablechange']);
end;

{ TEditEx }

type
  THackedWinControl = class(TWinControl);


constructor TEditEx.Create(aOwner: TComponent);
begin
  inherited;
  fItems := TStringList.Create;
  fValidChars := [#0..#255];
//  ParentCtl3D := False;
end;

procedure TEditEx.AfterConstruction;
begin
  inherited;
//  Ctl3D := False;//True;//fFlat := True; dbctrls
end;

procedure TEditEx.CreateWnd;
begin
  inherited;
//  Ctl3d := not fFlat;
end;

{function TEditEx.GetFlat: Boolean;
begin
  Result := fFlat;
end;}

destructor TEditEx.Destroy;
begin
  fItems.Free;
  inherited;
end;

function TEditEx.AutoCompleteStr(const S: string): string;
var
  Len, i: Integer;
begin
  Result := '';
  if Assigned(fOnAutoComplete) then
  begin
    fOnAutoComplete(Self, S, Result);
  end;

  if Result <> '' then
    Exit;

//  log(['gb editex',fItems.count,fCaseSensitive]);
  Len := Length(S);
  with fItems do
    for i := 0 to Count - 1 do
    begin
      case fCaseSensitive of
        False:
          begin
//            log(['gb editex',s,strings[i],CompareText(S, Copy(Strings[i], 1, Len))]);
            if CompareText(S, Copy(Strings[i], 1, Len)) = 0 then
            begin
              Result := Strings[i];
              Exit;
            end;
          end;
        True:
          begin
            if S = Copy(Strings[i], 1, Len) then
            begin
              Result := Strings[i];
              Exit;
            end;
          end;
      end;//case    
    end;
//  i := fItems.IndexOf(S)
end;

procedure TEditEx.KeyDown(var Key: Word; Shift: TShiftState);

var
  C: TWinControl;
begin
  inherited; // = event onkeydown

  case Key of
    VK_RETURN:
      begin
        //UpdateComponentSource;
        //Log(['enter']);
      end;
    VK_DOWN, VK_UP:
      if (Shift = []) and (Owner is TWinControl) then
      begin
         C := THackedWinControl(Owner).FindNextControl(Self, Key = VK_DOWN, True, False);
         if C is TCustomEdit then C.SetFocus;
      end;
  end;

  //TWinControl(Owner).Perform(CM_DialogKey, VK_TAB, 0);

end;

{procedure TEditEx.SetFlat(const Value: Boolean);
begin
  fFlat := Value;
  Ctl3D := not Value;
end;}

procedure TEditEx.KeyPress(var Key: Char);
var
  Temp, oldtext,newtext:string;
  start, SelLen: Integer;
label
  ExitPoint;
begin
  if not (Key in ValidChars) then
  begin
    Key := #0;
    Exit;
  end;
  
  if Key < Chr(32) then
    goto ExitPoint;

  Start := SelStart;
  OldText := Text;
  NewText := OldText;

  if SelLength > 0 then
    Delete(NewText, Start + 1, SelLength);
  Insert(Key, NewText, Start + 1);
  Temp := AutoCompleteStr(Copy(NewText, 1, Start + 1));
  if Temp <> '' then
  begin
    Key := #0;
    Text := Temp;
    SelLen := Length(Text) - Start - 1;
    SelectBackwards(Self, Length(Text), SelLen);
    Exit;
  end
  else begin
//    if fIsPickList then
    begin
//      Text := OldText;
      //SelL
//      Exit;
    end;
  end;

  ExitPoint:
  inherited KeyPress(Key);
end;

procedure TEditEx.SetItems(const Value: TStrings);
begin
  fItems.Assign(Value);
end;
(*
procedure TEditEx.UpdateComponentSource;
begin
  if ComponentSource <> nil then
    if ComponentProperty <> '' then
    begin
      SetPropValue(ComponentSource, ComponentProperty, Text);
      //with ComponentSource do
    end;
end; *)

procedure TEditEx.SetValidChars(const Value: TCharSet);
begin
  fValidChars := Value;
  Include(fValidChars, #8);
  Include(fValidChars, #13);
end;

{ TCheckBoxEx }

procedure TCheckBoxEx.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
//  Param stdctrls
  //if NewStyleControls and Ctl3D and (FBorderStyle = bsSingle) then
//  Params.Style := Params.Style {or BS_MULTILINE } or bs_flat;// or BS_TOP;
  with params do
  begin
//    Style := Style and not WS_BORDER;
    //ExStyle := ExStyle or WS_EX_CLIENTEDGE;
  end;
end;

{ TBarTextItem }

constructor TBarTextItem.Create(Collection: TCollection);
begin
  inherited;
  fColor := clLime;
  fBlinkColor := clBlack;
end;

procedure TBarTextItem.SetBlink(const Value: boolean);
begin
  fBlink := Value;
  Changed(False);
end;

procedure TBarTextItem.SetBlinkColor(const Value: TColor);
begin
  fBlinkColor := Value;
  Changed(False);
end;

procedure TBarTextItem.SetColor(const Value: TColor);
begin
  fColor := Value;
  Changed(False);
end;

procedure TBarTextItem.SetText(const Value: string);
begin
  fText := Value;
  Changed(False);
end;

{ TBarTextCollection }

function TBarTextCollection.Add: TBarTextItem;
begin
  Result := TBarTextItem(inherited Add);
end;

constructor TBarTextCollection.Create;//(ItemClass: TCollectionItemClass);
begin
  inherited Create(TBarTextItem);
end;

function TBarTextCollection.GetOwner: TPersistent;
begin
  Result := fTicketBar;
end;

procedure TBarTextCollection.Update(AItem: TCollectionItem);
begin
  if (fTicketBar <> nil) and not (csLoading in fTicketBar.ComponentState) and Assigned(fOnUpdate) then
    fOnUpdate(AItem);
end;

{ Thread }

(*type
  TTicketBarThread = class(TThread)
  private
    fTicketBar: TTicketBar;
  protected
    procedure Execute; override;
  public
    constructor Create(CreateSuspended: boolean; aTicketBar: TTicketBar);
  end; *)

{ TTicketBar }

procedure TTicketBar.BitmapResize(aBitmap: TBitmap; aWidth, aHeight: integer);
begin
  with aBitmap do
    begin
      Width := aWidth;//Max(Width, aWidth);
      Height := aHeight;//Max(Height, aHeight);
    end;
end;

procedure TTicketBar.BarTextChanged(Sender: TObject);
var
  LeftPos, i: integer;
  S: string;
  R, DR: TRect;
  BT: TBarTextItem;
  Sep: string;
begin
//Log(['bartextchanged']);

  S := '';

  Sep := fSeperator.Text;

  // bepaal totaal text
  for i := 0 to fBarText.Count - 1 do
  begin
    S := S + TBarTextItem(BarText.Items[i]).Text;
  end;

  // DRAWBITMAP
  with fDrawBitmap.Canvas do
  begin
    Lock;

    // bepaal grootte drawbitmap
    Font := Self.Font;
    fTextWidth := TextWidth(S);
    fTextHeight := TextHeight(S);
    fTextRect := Rect(0, 0, fTextWidth, fTextHeight);
    fDrawBitmap.Width := fTextWidth;
    fDrawBitmap.Height := fTextHeight;
    Brush.Style := bsSolid;
    Brush.Color := Self.Color;//clBlack;
    FillRect(fTextRect);

    LeftPos := 0;

    for i := 0 to fBarText.Count - 1 do
    begin
      BT := TBarTextItem(BarText.Items[i]);
      S := BT.Text;
      R := Rect(0, 0, 0, 0);
      // bepaal rechthoek stukje tekst
      DrawText(Handle, PChar(S), -1, R, DT_CALCRECT + DT_VCENTER);
      DR := R;
      RectMove(DR, LeftPos, 0);
      // teken stukje tekst naar bitmap
      Font.Color := BT.Color;
      DrawText(Handle, PChar(S), -1, DR, DT_VCENTER);
      Inc(LeftPos, R.Right);
    end;

    UnLock;
  end;

  // BLINKBITMAP
  with fBlinkBitmap.Canvas do
  begin
    Lock;

    // bepaal grootte blinkbitmap
    Font := Self.Font;
    fBlinkBitmap.Width := fTextWidth;
    fBlinkBitmap.Height := fTextHeight;
    Brush.Style := bsSolid;
    Brush.Color := Self.Color;//clBlack;
    FillRect(fTextRect);

    LeftPos := 0;

    for i := 0 to fBarText.Count - 1 do
    begin
      BT := TBarTextItem(BarText.Items[i]);
      S := BT.Text;
      R := Rect(0, 0, 0, 0);
      // bepaal rechthoek stukje tekst
      DrawText(Handle, PChar(S), -1, R, DT_CALCRECT + DT_VCENTER);
      DR := R;
      RectMove(DR, LeftPos, 0);
      // teken stukje tekst naar bitmap
      if BT.Blink then
        Font.Color := BT.BlinkColor
      else
        Font.Color := BT.Color;
      DrawText(Handle, PChar(S), -1, DR, DT_VCENTER);
      Inc(LeftPos, R.Right);
    end;

    UnLock;
  end;

//  Perform(WM_GETTEXT, 0, 0); controls

  Invalidate;
end;

procedure TTicketBar.WMEraseBkgnd(var Message: TWmEraseBkgnd);
begin
  Message.Result := 1;
end;

procedure TTicketBar.WMSize(var Msg: TWMSize);
begin
  BitmapResize(fTempBitmap, Msg.Width, Msg.Height);
  inherited;
end;

procedure TTicketBar.Paint;
var
  CR, Dest, Source: TRect;
  CopyW: integer;
  BMP: TBitmap;
begin

  with Canvas do
  begin
    if fUseBlink then
      BMP := fBlinkBitmap
    else
      BMP := fDrawBitmap;
{    Brush.Style := bsSolid;
    Brush.Color := Self.Color;
    FillRect(ClientRect);}
{    PaintText;       }


    // teken bar in tempbitmap over gehele breedte
    CR := Rect(0, 0, Self.Width, Self.Height);
//    BitmapResize(fTempBitmap, Width, Height);
    with fTempBitmap.Canvas do
    begin
      Brush.Color := Self.Color;//clBlack;
      FillRect(CR);
    end;

    // kopieer laatste stuk tekst in tempbitmap
    Dest := Rect(0, 0, fTextWidth, fTextHeight{fDrawBitmap.Width, fDrawBitmap.Height});
    Source := Dest;
    RectMove(Source, fPixelShift, 0);
    fTempBitmap.Canvas.CopyRect(Dest, BMP.Canvas, Source);

    // kopieer roterend eerste stuk erachteraan
    CopyW := fTextWidth - fPixelShift;
    if (fTextWidth > Self.Width) and (CopyW < Self.Width) then
    begin
      Dest := Rect(0, 0, fTextWidth, fTextHeight);
      Source := Dest;
      RectMove(Dest, CopyW + 1, 0);
      fTempBitmap.Canvas.CopyRect(Dest, BMP.Canvas, Source);
    end;

    // kopieer tempbitmap naar canvas
    CopyRect(CR, fTempBitmap.Canvas, CR);
    // brush.style:=bsclear; pen.color:=clyellow; rectangle(clientrect);
  end;

end;

constructor TTicketBar.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Color := clBlack;

  fTempBitmap := TBitmap.Create;
  fDrawBitmap := TBitmap.Create;
  fBlinkBitmap := TBitmap.Create;

  Width := 120;
  Height := 24;


  fTimer := TTimer.Create(Self);
  if not (csDesigning in ComponentState) then
    fTimer.Enabled := True
  else
    fTimer.Enabled := False;

  fBarText := TBarTextCollection.Create;
  fBarText.fTicketBar := Self;
  fBarText.OnUpdate := BarTextChanged;

  fSeperator := TBarTextItem.Create(nil);
  fSeperator.Text := ' *** ';
  fSeperator.Color := clYellow;

  Interval := 100;
  fShift := 1;

  fTimer.OnTimer := DoOnTimer;

end;

destructor TTicketBar.Destroy;
begin
  fTimer.Enabled := False;
  fSeperator.Free;
  fBarText.Free;
  fDrawBitmap.Free;
  fTempBitmap.Free;
  fBlinkBitmap.Free;
  inherited;
end;

procedure TTicketBar.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
  begin
//    Style := Style or WS_TABSTOP;
//    if FScrollBars in [ssVertical, ssBoth] then Style := Style or WS_VSCROLL;
//    if FScrollBars in [ssHorizontal, ssBoth] then Style := Style or WS_HSCROLL;
//    WindowClass.style := CS_DBLCLKS;



  {  if FBorderStyle = bsSingle then
      if NewStyleControls and Ctl3D then
      begin
        Style := Style and not WS_BORDER;
        ExStyle := ExStyle or WS_EX_CLIENTEDGE;
      end
      else }
//        Style := Style or WS_BORDER;


  end;
end;

procedure TTicketBar.DoOnTimer(Sender: TObject);
var
  Ticks: integer;
begin
//Log(['timer']);
  Inc(fPixelShift, fShift);
  if fPixelShift > fTextWidth then
    fPixelShift := 0;

  Ticks := GetTickCount;

  if fBlinkTime > 300 then
  begin
    fBlinkStart := Ticks;
    fBlinkTime := 0;
    fUseBlink := not fUseBlink;
  end
  else begin
    fBlinkTime := Ticks - fBlinkStart;
  end;

//  Log(['timer']);
  InvalidateRect(Handle, @fTextRect, False);
  Update;
end;

procedure TTicketBar.Resume;
begin

end;

procedure TTicketBar.SetBarText(const Value: TBarTextCollection);
begin
  fBarText.Assign(Value);
end;

procedure TTicketBar.SetInterval(const Value: integer);
begin
  if fInterval = Value then Exit;
  fInterVal := Value;
  fTimer.Interval := Value;
end;

procedure TTicketBar.Start;
begin
end;

procedure TTicketBar.Stop;
begin
end;

procedure TTicketBar.test;
var
  i: integer;
begin
  for i := 1 to 10000 do
  begin
//    delay(0);
    doontimer(nil);
  end;
end;

procedure TTicketBar.Loaded;
begin
  inherited;
  BarTextChanged(nil);
end;


procedure TTicketBar.DefaultHandler(var Message);
begin
  inherited;
  { zie ook TControl.DefaultHandler }

  with TMessage(Message) do
    case Msg of
      WM_SETTEXT:
        begin
          DoAssignText(Text);
          //Log([text]);
        end;
    end;

end;

procedure TTicketBar.DoAssignText(const S: string);
var
  OldEnabled: Boolean;
  i: integer;
  List: TStringList;
begin
  OldEnabled := fTimer.Enabled;
  fTimer.Enabled := False;
  List := TStringList.Create;

  try
    SplitString_To_StringList(S, List, ';');
    fBarText.BeginUpdate;
    try
      fBarText.Clear;
      for i := 0 to List.Count - 1 do
      begin
        with fBarText.Add do
        begin
          Text := List[i];
        end;
      end;
    finally
      fBarText.EndUpdate;
    end;
  finally
    List.Free;
    fTimer.Enabled := OldEnabled;
  end;
end;

procedure TTicketBar.SetBorderStyle(const Value: TBorderStyle);
begin
  if fBorderStyle = Value then Exit;
  fBorderStyle := Value;
  RecreateWnd;
end;

{ TTicketBarThread }

(*constructor TTicketBarThread.Create(CreateSuspended: boolean;
  aTicketBar: TTicketBar);
begin
//  inherited Create(True);
  fTicketBar := aTicketBar;
end;

procedure TTicketBarThread.Execute;
begin

  inherited;
end; *)

(*
var
  C: TWinControl;
begin
  if ActiveControl is TCustomEdit then
    case Key of
      VK_DOWN:
        begin
          C := FindNextControl(ActiveControl, True, True, False);
          if C is TCustomEdit then
          begin
            C.SetFocus;
            Exit;
          end;
//          SelectNext(ActiveControl, True, True); Exit;
        end;
      VK_UP:
        begin
          C := FindNextControl(ActiveControl, False, True, False);
          if C is TCustomEdit then
          begin
            C.SetFocus;
            Exit;
          end;
        //  SelectNext(ActiveControl, True, True); Exit;
        end;
    end;
  inherited;
  //Perform(CM_DialogKey, VK_TAB, 0);
  *)

{ TTreeViewEx }

{procedure TTreeViewEx.CNNotify(var Message: TWMNotify);
begin
  inherited;
  with Message do
    case NMHdr^.code of
      TVM_ENDEDITLABELNOW:
        log(['END EDIT TREEVIEWEX']);
    end;
end;}

end.

