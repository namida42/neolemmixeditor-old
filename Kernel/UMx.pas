unit UMx;
(*

  de verwijzing "#windows rect shit" duidt op rectangle-paint of invalidate ellende

*)       //umxstrings controls umxstrings
//todo paintheaderlines in paintlines stoppen: gedaan
//todo traverseinfo overal met bits inbouwen
//todo Columns.Free direct aanroepen vanuit app leidt nu tot access violation
// mxrows destroy aanpassen
// updaterows gelijk maken aan updatecolumns
interface

{.$define use_gr32}

{.$define matrix_testmode}
{.$define check_bugs}

uses
  Windows, Messages, Classes, Types, Controls, StdCtrls, Graphics, Math, SysUtils,
  TypInfo, Forms, Variants, Contnrs, Dialogs, ImgList,
  {$ifdef use_gr32}
  GR32,
  {$endif}
  UMisc, UMxMisc, UTools;

{$ifdef use_gr32}
type
  TMxBitmap = class(TBitmap32);
{$else}
type
  TMxBitmap = class(TBitmap);
{$endif}

type
  PRect = ^TRect;

{-------------------------------------------------------------------------------
  Edit Matrix Messages
-------------------------------------------------------------------------------}
const
  EMM_CANCELEDIT = WM_USER + 1;
  EMM_ENDEDIT    = WM_USER + 2;
  EMM_EXITEDIT   = WM_USER + 3;

  TRAVERSE_FIRST = Bit0;
  TRAVERSE_LAST  = Bit1;
  TRAVERSE_BREAK = Bit2;

type
//  TTraverseInfo = Byte;
  TMultiLineRange = False..True;
  TColorRange = 0..15; // aantal mogelijke state kleuren

{-------------------------------------------------------------------------------
  untyped consts
-------------------------------------------------------------------------------}
const
  { kleuren }
  MI_TEXTCOLOR                 = 0;  // normaal
  MI_BACKCOLOR                 = 1;  // normaal
  MI_TEXTFOCUSED               = 2;  // huidige cell, matrix focused
  MI_BACKFOCUSED               = 3;  // huidige cell, matrix focused
  MI_TEXTCURRENT               = 4;  // huidige cell, matrix niet focused
  MI_BACKCURRENT               = 5;  // huidige cell, matrix niet focused
  MI_TEXTSELECTED              = 6;  // cell behoort bij selectie
  MI_BACKSELECTED              = 7;  // cell behoort bij selectie
  MI_TEXTHYPERLINK             = 8;  // mousover
  MI_TEXTMATCH                 = 9;  // tekstkleur voor matched string
  MI_BACKMATCH                 = 10; // bk kleur voor matched string
  MI_DIFFBACKCOLOR             = 11; // normaal achtergrond kleur voor om de zoveel rijen
  MI_TEXTMATCH2                = 12; // tekstkleur 2 voor matched string
  MI_BACKMATCH2                = 13; // bk kleur 2 voor matched string
  MI_HOVERBACK                 = 14;
  MI_RESERVED15                = 15;


  {
    MATRIX FOCUSED
      .focused
      .colrowhighlight
      .hyper
      .diff
      .hover
      .match
    MATRIX NON-FOCUSED
      .focused
      .colrowhighlight
      .hyper
      .diff
      .hover
      .match
  }

  { defaults }
  DEF_WIDTH                    = 180; // default breedte matrix
  DEF_HEIGHT                   = 120; // default hoogte matrix
  DEF_COLCOUNT                 = 5;   //
  DEF_ROWCOUNT                 = 7;
  DEF_DEFAULTROWHEIGHT         = 19;
  DEF_DEFAULTCOLWIDTH          = 64;
  DEF_DEFAULTHORZLINEWIDTH     = 1;
  DEF_DEFAULTVERTLINEWIDTH     = 1;
  DEF_LINECOLOR                = clSilver;

  DEF_HEADERLINECOLOR          = clBlack;

  { ranges }
  MIN_COLWIDTH                 = 4;
  MAX_COLWIDTH                 = 2048;
  MIN_ROWHEIGHT                = 4;
  MAX_ROWHEIGHT                = 2048;

  MAX_HEIGHT                   = 32767;
  MAX_WIDTH                    = 32767;
//  MIN_COLCOUNT                 = 1;
  //MIN_ROWCOUNT                 =

  HIDDEN_HEIGHT                = -32767; // symbolisch negatief getal voor hidden kolommen/rijen
  INVALID_CACHE_SIZE           = -32766; // invalid cached height

  { visibility }
  NOT_VISIBLE                  = 0;
  PARTIAL_VISIBLE              = 1;
  FULL_VISIBLE                 = 2;


  AXISINFOFLAG_VERTICAL        = Bit0; // TAxisInfo.Flags: bit staat aan als vertikaal

type
  TPos = record A, B: Integer; end;
  TCachedIntegers = array of Integer;
  TCachedPoints = array of TPos;

  TAxis = (axNone, axHorizontal, axVertical);

  TRealWMNCPaint = packed record
    Msg: Cardinal;
    Rgn: HRGN;
    lParam: Integer;
    Result: Integer;
  end;

  TPageAxisInfo = record
    Flags            : DWORD;             // richting (kolom of rij)
    Indices          : TCachedIntegers;   // indexen van zichtbare kolommen of rijen
    Sizes            : TCachedPoints;     // start- en eindpunten van cell
    Lines            : TCachedIntegers;   // lijndiktes
    Offset           : Integer;           // topleft
    VisibleCount     : Integer;
    HiddenCount      : Integer;
    TotalCount       : Integer;
    FullVisibleCount : Integer;
    FirstVisible     : Integer;
    LastVisible      : Integer;
    LastFullVisible  : Integer;
    GridSize         : Integer;           // de hoogte of breedte van het cellen-gedeelte inclusief lijn
    Valid            : Boolean;
  end;

  TPageInfo = record
    Horz  : TPageAxisInfo;
    Vert  : TPageAxisInfo;
  end;

  TAxisRange = record
    First: Integer;
    Last: Integer;
  end;

  TPaintInfo = record
    TargetCanvas   : TCanvas;
    PageInfo       : TPageInfo;
    Target         : TRect;
    Bitmap         : TMxBitmap;
  end;

  TSizeCache = record
    Valid : Boolean;
    First : Integer;
    Last  : Integer;
    Sizes : TCachedIntegers;
    Lines : TCachedIntegers;
  end;

  { options voor  }
  TMXUseOption = (
    uoColumns,          // Gebruik Matrix.Columns
    uoRows,             // Gebruik Matrix.Rows
    uoTopHeaders,       // Gebruik Matrix.TopHeaders
    uoLeftHeaders,      // Gebruik Matrix.LeftHeaders
    uoRightHeaders,     // niet geimplementeerd
    uoBottomHeaders     // niet geimplementeerd
  );
  TMXUseOptions = set of TMXUseOption;

  { options voor painting }
  TMXPaintOption = (
    poSkipBackGround,               // Teken geen achtergrond omdat cellen en lijnen dit al doen
    poHorzLines,                    // Teken horizontale lijnen
    poVertLines,                    // Teken vertikale lijnen
    poCalculatedRowHeights,         // Hoogtes zijn virtueel
    poCalculatedColWidths,          // Breedtes zijn virtueel
    poTransparentVertLines,         // Lijndiktes worden alleen gebruikt om afstand tussen de cellen te maken
    poTransparentHorzLines,         // Lijndiktes worden alleen gebruikt om afstand tussen de cellen te maken
    poHidePartialRows,              // Rijen die gedeeltelijk zichtbaar zijn (onderin) worden niet weergegeven
    poHidePartialCols,              // Kolommen die gedeeltelijk zichtbaar zijn (rechts) worden niet weergegeven
    poHidePartialVertLines,         // Teken de vertikale lijnen van gedeeltelijke rijen (onderin) niet
    poHidePartialHorzLines,         // Teken de horizontale lijnen van gedeeltelijke kolommen (rechts) niet
    poHideLastVertLine,             // Teken de laatste vertikale lijn niet
    poHideLastHorzLine,             // Teken de laatste horizontale lijn niet
    poTransparentCells,             // Geen cell-achtergrond, alleen contents
    poSelector,                     // Teken een selector
    poOnlyHighlightContents,        // Highlight alleen het contents rect
    poDrawFocusRect,                // Teken cell focusrect
    poHoveringCells,                // Cell mouseover
    poHeaderHorzLines,              // Teken de horizontale lijnen van de topheaders
    poHeaderVertLines,              // Teken de vertikale lijnen van de leftheaders
    poHeaderHighlightStrings,       // Searchstring highlight in headers
    poIncrementalSearchHighlight,   // Incrementalsearch string highlight
    poMixStyles                     // Mix kleuren als kolom en rij stijl beiden aanwezig
  );
  TMXPaintOptions = set of TMXPaintOption;

  TMXSizingOption = (
    soColSizing,       // Kolommen mogen geresized worden
    soRowSizing,       // Rijen mogen geresized worden
    soCellSizing,      // Cellen mogen geresized worden
    soColMoving,       // Kolommen mogen verplaatst worden
    soRowMoving,       // Rijen mogen verplaatst worden
    soLiveSizing,      // Tijdens muis resize wordt de resize direct op het scherm uitgevoerd
    soHeaderSizingOnly // Resizen alleen mogelijk dmv headers
  );
  TMXSizingOptions = set of TMXSizingOption;

  TMXNavigateOption = (
    noNextLine,
    noPrevLine,
    noNextRow,
    noPrevRow
  );
  TMXNavigateOptions = set of TMXNavigateOption;

  { options voor autosizing }
  TMXAutoSizeOption = (
    aoAutoWidth,
    aoAutoHeight,
    aoAutoAdjustLastColumn,
    aoAutoAdjustLastRow,
    aoAddSpaceForRequestedCols, // als VisualColsRequested > ColCount voeg gridspace toe
    aoAddSpaceForRequestedRows // als VisualColsRequested > ColCount voeg gridspace toe
  );
  TMXAutoSizeOptions = set of TMXAutoSizeOption;

  TMXEditOption = (
    eoCanEdit,          // edit mogelijk
    eoAlwaysShow,       // editor altijd zichtbaar (als focus)
    eoStartOnChar,      // start edit op keypress editchars
    eoAutoAccept        // end edit auto confirm
  );
  TMXEditOptions = set of TMXEditOption;

  TMXSearchOption = (
    soIncrementalSearch
  );
  TMXSearchOptions = set of TMXSearchOption;

  TMXCreateOnDemandOption = (
    cdColumns,         // TBaseMatrix.Columns is runtime load on demand
    cdRows,            // TBaseMatrix.Rows is runtime load on demand
    cdCellStyle,       // TMxColumn.GetStyle en TMxRow.GetStyle zijn runtime load on demand
    cdHeaders
  );
  TMXCreateOnDemandOptions = set of TMXCreateOnDemandOption;

  TMXMiscOption = (
    moKeepColumnsOnDestroy,
    moKeepRowsOnDestroy
  );
  TMXMiscOptions = set of TMXMiscOption;

  TSelectMode = (
    smCellSelect,
    smRowSelect,
    smColSelect,
    smColRowSelect
  );

  TVertAlignment = (
    vaTop,
    vaBottom,
    vaCenter
  );

  TMargin = record
    tLeft, tTop, tRight, tBottom: ShortInt;
  end;

  TCellStyleOption = (
    csoButton,
    csoMultiLine
  );
  TCellStyleOptions = set of TCellStyleOption;

  TCellDrawStates = (
    dsMatrixFocused,   // matrix is gefocused
    dsFocused,         // actieve cell
    dsHover,           // cell is onder de muis,
    dsHyperLink,       // cell is hyperlink
    dsHighlightRow,    // rowselect
    dsHighlightCol,    // colselect
    dsHighlightString,
    dsSelection,
    dsGlyph
  );
  TCellDrawState = set of TCellDrawStates;

  THeaderDrawStates = (
    hsMatrixFocused,   // matrix is gefocused
    hsHighlight,       // rowselect
    hsHover,           // cell is onder de muis,
    hsHighlightString,
    hsSelection
  );
  THeaderDrawState = set of THeaderDrawStates;

  TGlyphPosition = (
    gpLeft,
    gpRight,
    gpTop,
    gpBottom,
    gpCenter
  );

const
  DEF_PAINTOPTIONS             = [poHorzLines, poVertLines, poHeaderHorzLines, poHeaderVertLines];
  DEF_AUTOSIZEOPTIONS          = [];
  DEF_HORZALIGN                = taLeftJustify;
  DEF_VERTALIGN                = vaCenter;
  DEF_USEOPTIONS               = [];
//  DEF_SCROLLOPTIONS        = [soMouseWheelClamp, soScrollBarClamp];
  DEF_SIZINGOPTIONS            = [soColSizing, soRowSizing, soCellSizing{ soLiveSizing}];
//  DEF_EDITOPTIONS          = [eoCanEdit, eoCanDeleteRow, eoCanAppendRow, eoStartOnChar, eoAutoAccept];
  DEF_CREATEONDEMANDOPTIONS    = [cdColumns, cdRows, cdCellStyle, cdHeaders];
  DEF_FLAT                   = True;
  DEF_CELLBORDERWIDTH  = 0;
//  DEF_HYPERLINK        : array[THeaderRange] of Boolean = (False, False);
  DEF_MULTILINE        = False;//True;
  DEF_WORDWRAP         = False;
  DEF_GLYPHPOSITION    = gpLeft;
//  DEF_BTNLOOK          : array[THeaderRange] of Boolean = (False, True);

  DEF_HEADERFLAT = False;

type
  EMatrixError = class(Exception);

  TBaseMatrix = class; { forward }
  TMatrixHeader = class; { forward }
  TMatrixHeaders = class; { forward }

  TMatrixObject = class(TPersistent)
  private
    fMatrix: TBaseMatrix;
    fOnChange: TNotifyEvent;
  protected
    procedure Changed; virtual;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  public
    constructor Create(aMatrix: TBaseMatrix);
    property Matrix: TBaseMatrix read fMatrix;
  published
  end;

  TOwnerFlag = (
    ofNone,
    ofMatrix,
    ofColumn,
    ofRow,
    ofHeader
  );

  TStateColorArray = array[TColorRange] of TColor;
  TAssignedColorProps = set of TColorRange; //Byte; //gebruikt om te bekijken welke kleuren veranderd zijn
  TStateColors = class(TPersistentWithOwner)
  private
    fColorArray          : TStateColorArray;
    fAssignedColorProps  : TAssignedColorProps;
    fOnChange            : TNotifyEvent;
    fOwnerFlag           : TOwnerFlag;
    function GetColorElement(const Index: Integer): TColor;
    procedure SetColorElement(const Index: Integer; const Value: TColor);
    function GetDefaultColorElement(const Index: Integer): TColor;
    function IsColorStored(const Index: Integer): Boolean;
    procedure Changed;
  protected
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  public
    constructor Create(aOwner: TPersistent; aOwnerFlag: TOwnerFlag);
    property DefaultColorElement[const Index: Integer]: TColor read GetDefaultColorElement;
    procedure Assign(Source: TPersistent); override;
  published
    property TextColor: TColor index MI_TEXTCOLOR read GetColorElement write SetColorElement stored IsColorStored;
    property BackColor: TColor index MI_BACKCOLOR read GetColorElement  write SetColorElement stored IsColorStored;
    property TextFocused: TColor index MI_TEXTFOCUSED read GetColorElement write SetColorElement stored IsColorStored;
    property BackFocused: TColor index MI_BACKFOCUSED read GetColorElement write SetColorElement stored IsColorStored;
    property TextCurrent: TColor index MI_TEXTCURRENT read GetColorElement write SetColorElement stored IsColorStored;
    property BackCurrent: TColor index MI_BACKCURRENT read GetColorElement write SetColorElement stored IsColorStored;
    property TextSelected: TColor index MI_TEXTSELECTED read GetColorElement write SetColorElement stored IsColorStored;
    property BackSelected: TColor index MI_BACKSELECTED read GetColorElement write SetColorElement stored IsColorStored;
    property TextHyperLink: TColor index MI_TEXTHYPERLINK read GetColorElement write SetColorElement stored IsColorStored;
    property TextMatch: TColor index MI_TEXTMATCH read GetColorElement write SetColorElement stored IsColorStored;
    property BackMatch: TColor index MI_BACKMATCH read GetColorElement write SetColorElement stored IsColorStored;
    property TextMatch2: TColor index MI_TEXTMATCH2 read GetColorElement write SetColorElement stored IsColorStored;
    property BackMatch2: TColor index MI_BACKMATCH2 read GetColorElement write SetColorElement stored IsColorStored;
    property DiffBack: TColor index MI_DIFFBACKCOLOR read GetColorElement write SetColorElement stored IsColorStored;
    property HoverBack: TColor index MI_HOVERBACK read GetColorElement write SetColorElement stored IsColorStored;
  end;

  {-------------------------------------------------------------------------------
    cellstyle:
    tassignedcellprops wordt gebruikt om column/row/cell-properties "transparant"
    te laten werken tov de matrix
  -------------------------------------------------------------------------------}
  TAssignedCellProp = (
    acp_Font,
    acp_Edge,
    acp_Clip,
    acp_Flat,
    acp_HorzAlign,
    acp_HyperLink,
    acp_VertAlign,
    acp_MultiLine,
    acp_WordWrap,
    acp_BtnLook,
    acp_BorderWidth,
    acp_GlyphPosition
    );
  TAssignedCellProps = set of TAssignedCellProp;

  TDrawMargin = class(TPersistent)
  private
    fBottom: Integer;
    fLeft: Integer;
    fTop: Integer;
    fRight: Integer;
    procedure SetBottom(const Value: Integer);
    procedure SetLeft(const Value: Integer);
    procedure SetRight(const Value: Integer);
    procedure SetTop(const Value: Integer);
  protected
  public
  published
    property Left: Integer read fLeft write SetLeft;
    property Top: Integer read fTop write SetTop;
    property Right: Integer read fRight write SetRight;
    property Bottom: Integer read fBottom write SetBottom;
  end;

  TCellCache = record
  { cached statecolors }
    Colors            : TStateColorArray;
  { cached font }
    Font              : TFont;
  { cached cellstyle }
    BorderWidth       : Integer;
    Flat              : Boolean;
    HorzAlign         : TAlignment;
    HyperLink         : Boolean;
    VertAlign         : TVertAlignment;
    MultiLine         : Boolean;
    Margin            : TMargin;
    WordWrap          : Boolean;
    GlyphPosition     : TGlyphPosition;
  end;

  TAxisFlag = (
    afAutoGenerated,     // col of row is autogenerated (readonly)
    afHidden,            // col of row is hidden
    afHiddenLine,        // col of row heeft hidden line
    afTagged             // col of row is selected or part of selection
  );
  TAxisFlags = set of TAxisFlag;

  TCellStyle = class(TMatrixObject)
  private
    fOwnerFlag         : TOwnerFlag;
    fBorderWidth       : Integer;
    fFont              : TFont;
    fHorzAlign         : TAlignment;
    fVertAlign         : TVertAlignment;
    fMargin            : TMargin;
    fCached            : Boolean;
    fOnChange          : TNotifyEvent;
    fColors            : TStateColors;
    fGlyphPosition     : TGlyphPosition;
    fMultiLine         : Boolean;
    fWordWrap          : Boolean;
    fFlat              : Boolean;
  { overig }
    fCache             : TCellCache; // record voor snelle toegang properties
    fAssignedCellProps : TAssignedCellProps; // gebruikt om te kijken welke cellstyle properties expliciet gezet zijn
    fUpdateFlag        : Integer;
  { intern }
    procedure ColorChanged(Sender: TObject);
    procedure FontChanged(Sender: TObject);
  { getters }
    function GetBorderWidth: Integer;
    function GetFlat: Boolean;
    function GetFont: TFont;
    function GetHorzAlign: TAlignment;
    function GetVertAlign: TVertAlignment;
    function GetMultiLine: Boolean;
    function GetWordWrap: Boolean;
    function GetGlyphPosition: TGlyphPosition;
  { setters }
    procedure SetBorderWidth(const Value: Integer);
    procedure SetColors(const Value: TStateColors);
    procedure SetFlat(const Value: Boolean);
    procedure SetFont(const Value: TFont);
    procedure SetHorzAlign(const Value: TAlignment);
    procedure SetVertAlign(const Value: TVertAlignment);
    procedure SetMultiLine(const Value: Boolean);
    procedure SetWordWrap(const Value: Boolean);
    procedure SetGlyphPosition(const Value: TGlyphPosition);
  { default getters }
    function GetDefaultBorderWidth: Integer;
    function GetDefaultFlat: Boolean;
    function GetDefaultFont: TFont;
    function GetDefaultHorzAlign: TAlignment;
    function GetDefaultVertAlign: TVertAlignment;
    function GetDefaultMultiLine: Boolean;
    function GetDefaultWordWrap: Boolean;
    function GetDefaultGlyphPosition: TGlyphPosition;
  { interne storage functies }
    function IsBorderWidthStored: Boolean;
    function IsFlatStored: Boolean;
    function IsFontStored: Boolean;
    function IsHorzAlignStored: Boolean;
    function IsVertAlignStored: Boolean;
    function IsMultiLineStored: Boolean;
    function IsWordWrapStored: Boolean;
    function IsGlyphPositionStored: Boolean;
  protected
    procedure Changed; override;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
    procedure SetCached(DoCache: Boolean);
  public
    constructor Create(aMatrix: TBaseMatrix; aOwnerFlag: TOwnerFlag);
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure BeginUpdate;
    procedure EndUpdate;
  { default properties }
    property DefaultBorderWidth: Integer read GetDefaultBorderWidth;
    property DefaultFlat: Boolean read GetDefaultFlat;
    property DefaultFont: TFont read GetDefaultFont;
    property DefaultHorzAlign: TAlignment read GetDefaultHorzAlign;
    property DefaultVertAlign: TVertAlignment read GetDefaultVertAlign;
    property DefaultMultiLine: Boolean read GetDefaultMultiLine;
    property DefaultWordWrap: Boolean read GetDefaultWordWrap;
    property DefaultGlyphPosition: TGlyphPosition read GetDefaultGlyphPosition;
  { cached properties }
    property Cached: Boolean read fCached write SetCached;
    property CachedBorderWidth: Integer read fCache.BorderWidth;
    property CachedColors: TStateColorArray read fCache.Colors;
    property CachedFlat: Boolean read fCache.Flat;
    property CachedFont: TFont read fCache.Font;
    property CachedHorzAlign: TAlignment read fCache.HorzAlign;
    property CachedHyperLink: Boolean read fCache.HyperLink;
    property CachedVertAlign: TVertAlignment read fCache.VertAlign;
    property CachedMultiLine: Boolean read fCache.MultiLine;
    property CachedMargin: TMargin read fCache.Margin;
    property CachedWordWrap: Boolean read fCache.WordWrap;
    property CachedGlyphPosition: TGlyphPosition read fCache.GlyphPosition;
  published
  { published }
    property BorderWidth: Integer read GetBorderWidth write SetBorderWidth stored IsBorderWidthStored;
    property Colors: TStateColors read fColors write SetColors;
    property Flat: Boolean read GetFlat write SetFlat stored IsFlatStored;
    property Font: TFont read GetFont write SetFont stored IsFontStored;
    property HorzAlign: TAlignment read GetHorzAlign write SetHorzAlign stored IsHorzAlignStored;
    property VertAlign: TVertAlignment read GetVertAlign write SetVertAlign stored IsVertAlignStored;
    property MultiLine: Boolean read GetMultiLine write SetMultiLine stored IsMultiLineStored;
    property WordWrap: Boolean read GetWordWrap write SetWordWrap stored IsWordWrapStored;
    property GlyphPosition: TGlyphPosition read GetGlyphPosition write SetGlyphPosition stored IsGlyphPositionStored;
    property Margin: TMargin read fMargin write fMargin;
  end;

  TOptionsChangeRec = record
    cAutoSizeOptions       : TMXAutoSizeOptions;
    cUseOptions            : TMXUseOptions;
    cPaintOptions          : TMXPaintOptions;
    cSizingOptions         : TMXSizingOptions;
    cNavigateOptions       : TMXNavigateOptions;
    cEditOptions           : TMXEditOptions;
    cCreateOnDemandOptions : TMXCreateOnDemandOptions;
    cSearchOptions         : TMXSearchOptions;
  end;

  { todo protected options maken? of checkroutine ergens of enum available }
  TMatrixOptions = class(TMatrixObject)
  private
  protected
    fUpdateCount           : Integer;
    fOptionsDeleted        : TOptionsChangeRec; // houdt deleted bij
    fOptionsInserted       : TOptionsChangeRec; // houdt inserted bij
    fOptionsChanged        : TOptionsChangeRec; // houdt changes bij
    fAutoSizeOptions       : TMXAutoSizeOptions;
    fUseOptions            : TMXUseOptions;
    fPaintOptions          : TMXPaintOptions;
    fSizingOptions         : TMXSizingOptions;
    fNavigateOptions       : TMXNavigateOptions;
    fEditOptions           : TMXEditOptions;
    fCreateOnDemandOptions : TMXCreateOnDemandOptions;
    fSearchOptions         : TMXSearchOptions;
    fMiscOptions: TMXMiscOptions;
  { intern }
    procedure ClearRecs;
    property DeletedAutoSizeOptions: TMXAutoSizeOptions read fOptionsDeleted.cAutoSizeOptions write fOptionsDeleted.cAutoSizeOptions;
    property DeletedUseOptions: TMXUseOptions read fOptionsDeleted.cUseOptions write fOptionsDeleted.cUseOptions;
    property DeletedPaintOptions: TMXPaintOptions read fOptionsDeleted.cPaintOptions write fOptionsDeleted.cPaintOptions;
    property DeletedSizingOptions: TMXSizingOptions read fOptionsDeleted.cSizingOptions write fOptionsDeleted.cSizingOptions;
    property DeletedNavigateOptions: TMXNavigateOptions read fOptionsDeleted.cNavigateOptions write fOptionsDeleted.cNavigateOptions;
    property DeletedEditOptions: TMXEditOptions read fOptionsDeleted.cEditOptions write fOptionsDeleted.cEditOptions;
    property DeletedCreateOnDemandOptions: TMXCreateOnDemandOptions read fOptionsDeleted.cCreateOnDemandOptions write fOptionsDeleted.cCreateOnDemandOptions;
    property DeletedSearchOptions: TMXSearchOptions read fOptionsDeleted.cSearchOptions write fOptionsDeleted.cSearchOptions;
    property InsertedAutoSizeOptions: TMXAutoSizeOptions read fOptionsInserted.cAutoSizeOptions write fOptionsInserted.cAutoSizeOptions;
    property InsertedUseOptions: TMXUseOptions read fOptionsInserted.cUseOptions write fOptionsInserted.cUseOptions;
    property InsertedPaintOptions: TMXPaintOptions read fOptionsInserted.cPaintOptions write fOptionsInserted.cPaintOptions;
    property InsertedSizingOptions: TMXSizingOptions read fOptionsInserted.cSizingOptions write fOptionsInserted.cSizingOptions;
    property InsertedNavigateOptions: TMXNavigateOptions read fOptionsInserted.cNavigateOptions write fOptionsInserted.cNavigateOptions;
    property InsertedEditOptions: TMXEditOptions read fOptionsInserted.cEditOptions write fOptionsInserted.cEditOptions;
    property InsertedCreateOnDemandOptions: TMXCreateOnDemandOptions read fOptionsInserted.cCreateOnDemandOptions write fOptionsInserted.cCreateOnDemandOptions;
    property InsertedSearchOptions: TMXSearchOptions read fOptionsInserted.cSearchOptions write fOptionsInserted.cSearchOptions;
    property ChangedAutoSizeOptions: TMXAutoSizeOptions read fOptionsChanged.cAutoSizeOptions write fOptionsChanged.cAutoSizeOptions;
    property ChangedUseOptions: TMXUseOptions read fOptionsChanged.cUseOptions write fOptionsChanged.cUseOptions;
    property ChangedPaintOptions: TMXPaintOptions read fOptionsChanged.cPaintOptions write fOptionsChanged.cPaintOptions;
    property ChangedSizingOptions: TMXSizingOptions read fOptionsChanged.cSizingOptions write fOptionsChanged.cSizingOptions;
    property ChangedNavigateOptions: TMXNavigateOptions read fOptionsChanged.cNavigateOptions write fOptionsChanged.cNavigateOptions;
    property ChangedEditOptions: TMXEditOptions read fOptionsChanged.cEditOptions write fOptionsChanged.cEditOptions;
    property ChangedCreateOnDemandOptions: TMXCreateOnDemandOptions read fOptionsChanged.cCreateOnDemandOptions write fOptionsChanged.cCreateOnDemandOptions;
    property ChangedSearchOptions: TMXSearchOptions read fOptionsChanged.cSearchOptions write fOptionsChanged.cSearchOptions;
  { property access }
    procedure SetAutoSizeOptions(const Value: TMXAutoSizeOptions);
    procedure SetUseOptions(const Value: TMXUseOptions);
    procedure SetPaintOptions(const Value: TMXPaintOptions);
    procedure SetSizingOptions(const Value: TMXSizingOptions);
    procedure SetNavigateOptions(const Value: TMXNavigateOptions);
    procedure SetEditOptions(const Value: TMXEditOptions);
    procedure SetMiscOptions(const Value: TMXMiscOptions);
  { easy property boolean eccess }
    function GetAutoWidth: Boolean;
    procedure SetAutoWidth(const Value: Boolean);
    function GetAutoHeight: Boolean;
    procedure SetAutoHeight(const Value: Boolean);
    function GetShowLines: Boolean;
    procedure SetShowLines(const Value: Boolean);
    function GetShowHeaderLines: Boolean;
    procedure SetShowHeaderLines(const Value: Boolean);
    function GetUseColumns: Boolean;
    procedure SetUseColumns(const Value: Boolean);
    function GetUseRows: Boolean;
    procedure SetUseRows(const Value: Boolean);
    function GetUseTopHeaders: Boolean;
    procedure SetUseTopHeaders(const Value: Boolean);
    function GetUseLeftHeaders: Boolean;
    procedure SetUseLeftHeaders(const Value: Boolean);
    function GetUseBottomHeaders: Boolean;
    procedure SetUseBottomHeaders(const Value: Boolean);
    function GetUseRightHeaders: Boolean;
    procedure SetUseRightHeaders(const Value: Boolean);
    function GetTransparentVertlines: Boolean;
    procedure SetTransparentVertlines(const Value: Boolean);
    function GetTransparentHorzlines: Boolean;
    procedure SetTransparentHorzlines(const Value: Boolean);
    function GetCalculatedRowHeights: Boolean;
    procedure SetCalculatedRowHeights(const Value: Boolean);
    function GetTransparentLines: Boolean;
    procedure SetTransparentLines(const Value: Boolean);
    function GetAutoAdjustLastColumn: Boolean;
    procedure SetAutoAdjustLastColumn(const Value: Boolean);
    function GetAutoAdjustLastRow: Boolean;
    procedure SetAutoAdjustLastRow(const Value: Boolean);
    function GetHideLastVertLine: Boolean;
    procedure SetHideLastVertLine(const Value: Boolean);
    function GetCanEdit: Boolean;
    procedure SetCanEdit(const Value: Boolean);
    function GetKeepColumnsOnDestroy: Boolean;
  protected
    procedure Changed; override;
  public
    constructor Create(aMatrix: TBaseMatrix);
    procedure Assign(Source: TPersistent); override;
    procedure BeginUpdate;
    procedure EndUpdate;
    function Updating: Boolean;
  { easy boolean access autosizeoptions }
    property AutoWidth: Boolean read GetAutoWidth write SetAutoWidth;
    property AutoHeight: Boolean read GetAutoHeight write SetAutoHeight;
    property AutoAdjustLastColumn: Boolean read GetAutoAdjustLastColumn write SetAutoAdjustLastColumn;
    property AutoAdjustLastRow: Boolean read GetAutoAdjustLastRow write SetAutoAdjustLastRow;
  { easy boolean access useoptions }
    property UseColumns: Boolean read GetUseColumns write SetUseColumns;
    property UseRows: Boolean read GetUseRows write SetUseRows;
    property UseTopHeaders: Boolean read GetUseTopHeaders write SetUseTopHeaders;
    property UseLeftHeaders: Boolean read GetUseLeftHeaders write SetUseLeftHeaders;
    property UseBottomHeaders: Boolean read GetUseBottomHeaders write SetUseBottomHeaders;
    property UseRightHeaders: Boolean read GetUseRightHeaders write SetUseRightHeaders;
  { easy boolean access paintoptions }
    property CalculatedRowHeights: Boolean read GetCalculatedRowHeights write SetCalculatedRowHeights;
    property TransparentVertlines: Boolean read GetTransparentVertlines write SetTransparentVertlines;
    property TransparentHorzlines: Boolean read GetTransparentHorzlines write SetTransparentHorzlines;
    property TransparentLines: Boolean read GetTransparentLines write SetTransparentLines;
    property HideLastVertLine: Boolean read GetHideLastVertLine write SetHideLastVertLine;
    property ShowLines: Boolean read GetShowLines write SetShowLines;
    property ShowHeaderLines: Boolean read GetShowHeaderLines write SetShowHeaderLines;
  { easy boolean access miscoptions }
    property KeepColumnsOnDestroy: Boolean read GetKeepColumnsOnDestroy;
  { easy boolean access navigateoptions }
  { easy boolean access editoptions }
    property CanEdit: Boolean read GetCanEdit write SetCanEdit;
  { easy boolean access CreateOnDemandOptions }
  published
    property AutoSizeOptions: TMXAutoSizeOptions read fAutoSizeOptions write SetAutoSizeOptions stored True;
    property UseOptions: TMXUseOptions read fUseOptions write SetUseOptions stored True;
    property PaintOptions: TMXPaintOptions read fPaintOptions write SetPaintOptions stored True;
    property SizingOptions: TMXSizingOptions read fSizingOptions write SetSizingOptions stored True;
    property NavigateOptions: TMXNavigateOptions read fNavigateOptions write SetNavigateOptions stored True;
    property EditOptions: TMXEditOptions read fEditOptions write SetEditOptions stored True;
    property CreateOnDemandOptions: TMXCreateOnDemandOptions read fCreateOnDemandOptions write fCreateOnDemandOptions default DEF_CreateOnDemandOptions;
    property SearchOptions: TMXSearchOptions read fSearchOptions write fSearchOptions;
    property MiscOptions: TMXMiscOptions read fMiscOptions write SetMiscOptions;
  end;

  TAxisItemVisualChangedEvent = procedure(Sender: TObject; SomeSizeChanged: Boolean) of object;

  {#todo: het verschijnsel collection verlaten? sneller en kleiner! }
  { abstracte baseclass voor columns of rows } {12 bytes (???) }
  TMxAxisItemClass = class of TMxAxisItem;
  TMxAxisItem = packed class(TCollectionItem)
  private
    function GetVisible: Boolean;
    procedure SetVisible(const Value: Boolean);
    function GetObject: TObject;
    procedure SetObject(const Value: TObject);
  protected
    fTag        : Integer;  // 32 bits      = 16
    fStyle      : TCellStyle; // 32 bits    = 20
    fSize       : SmallInt; // 16 bits      = 22
    fIndent     : Byte;
    fLineWidth  : ShortInt; // 8 bits {19}
    fFlags      : TAxisFlags; // 8 bits flags zie AF_XXXX constanten {20}
    procedure SetSize(const Value: SmallInt);
    procedure SetFlags(const Value: TAxisFlags);
    function GetStyleAssigned: Boolean;
    procedure SetStyleAssigned(const Value: Boolean);
    function GetStyle: TCellStyle;
    function GetMatrix: TBaseMatrix;
    procedure SetStyle(const Value: TCellStyle);
    procedure SetLineWidth(const Value: ShortInt);
    procedure StyleChanged(Sender: TObject);
  { streaming }
    function IsStoredStyle: Boolean;
  protected
    property Matrix: TBaseMatrix read GetMatrix;
    class function GetAxisOwnerFlag: TOwnerFlag; virtual;
    procedure VisualChanged(SomeSizeChanged: Boolean); virtual; abstract;
    procedure IndexChanged(OldIndex, NewIndex: Integer); virtual;
    procedure SetIndex(Value: Integer); override;
//    procedure Assign(Source: TPersistent); override;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    property Flags: TAxisFlags read fFlags write SetFlags; // do not publish!
  { protected properties }
//    property Index: Integer read fIndex write SetIndex;
    property Size: SmallInt read fSize write SetSize;
    property LineWidth: ShortInt read fLineWidth write SetLineWidth;
    property Tag: Integer read fTag write fTag;
    property Obj: TObject read GetObject write SetObject; // wrapper om tag
    property StyleAssigned: Boolean read GetStyleAssigned write SetStyleAssigned;
    property Style: TCellStyle read GetStyle write SetStyle stored IsStoredStyle;
    property Visible: Boolean read GetVisible write SetVisible;
  published
  end;

  TMxAxisCollection = class(TOwnedCollection)
  private
    fMatrix: TBaseMatrix;
    function GetCount: Integer;
    procedure SetCount(const Value: Integer);
    function InternalAdd: TMxAxisItem;
    function InternalInsert(Index: Integer): TMxAxisItem;
  protected
    class function ClearAutoGeneratedOnAdd: Boolean; virtual;
    procedure ItemIndexChanged(Item: TMxAxisItem; OldIndex, NewIndex: Integer); virtual;
  public
    constructor Create(aOwner: TPersistent; aColumnClass: TMxAxisItemClass);
    function AutoGenerated: Boolean;
    property Count: Integer read GetCount write SetCount;
  end;

  TMxColumnClass = class of TMxColumn;
  TMxColumn = class(TMxAxisItem)
  private
    fTitle: string;
    procedure SetTitle(const Value: string);
  protected
    class function GetAxisOwnerFlag: TOwnerFlag; override;
    procedure VisualChanged(SomeSizeChanged: Boolean); override;
  public
    constructor Create(Collection: TCollection); override;
    procedure Assign(Source: TPersistent); override;
  published
    property Width: SmallInt read fSize write SetSize;
    property LineWidth;
    property Tag;
    property StyleAssigned;
    property Style;
    property Title: string read fTitle write SetTitle;
  end;

  TMxColumns = class(TMxAxisCollection)
  private
    function GetItem(Index: Integer): TMxColumn;
    procedure SetItem(Index: Integer; const Value: TMxColumn);
  protected
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
    procedure Update(Item: TCollectionItem); override;
    procedure ItemIndexChanged(Item: TMxAxisItem; OldIndex, NewIndex: Integer); override;
    //procedure ForceDestroy;
  public
    constructor Create(aOwner: TPersistent; aColumnClass: TMxColumnClass);
    destructor Destroy; override;
    function Add: TMxColumn;
    function Insert(Index: Integer): TMxColumn;
    property Items[Index: Integer]: TMxColumn read GetItem write SetItem; default;
  published
  end;

  TMxRowClass = class of TMxRow;
  TMxRow = class(TMxAxisItem)
  private
  protected
    class function GetAxisOwnerFlag: TOwnerFlag; override;
    procedure VisualChanged(SomeSizeChanged: Boolean); override;
  public
    constructor Create(Collection: TCollection); override;
  published
    property Height: SmallInt read fSize write SetSize;
    property LineWidth;
    property Tag;
    property StyleAssigned;
    property Style;
    property Indent: Byte read fIndent write findent;
  end;

  TMxRows = class(TMxAxisCollection)
  private
    function GetItem(Index: Integer): TMxRow;
    procedure SetItem(Index: Integer; const Value: TMxRow);
  protected
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(aOwner: TPersistent; aRowClass: TMxRowClass);
    function Add: TMxRow;
    function Insert(Index: Integer): TMxRow;
    property Items[Index: Integer]: TMxRow read GetItem write SetItem; default;
  published
  end;

  TMxCell = class
  private
  protected
  public
  published
  end;

  TMxCells = class
  private
    //fColIndices: TIntegerList; umisc
    //fRowIndices: TIntegerList;
  protected
  public
  published
  end;

  TScrollReason = (
    srNone,
    srData,
    srScrollBar,
    srKeyBoard,
    srClick,
    srMouseWheel
  );

  TScrollCommand = (
    scNone,
    scAbsolute,
    scRight,         // 1 naar rechts
    scLeft,          // 1 naar links
    scDown,          // 1 naar beneden
    scUp,            // 1 naar boven
    scPgDn,          // pagina naar beneden
    scPgUp,          // pagina naar boven
    scHorzStart,     // eerste kolom
    scHorzEnd,       // laatste kolom
    scVertStart,     // eerste rij
    scVertEnd,       // laatste rij
    scBegin,         // eerste cell
    scEnd            // laatste cell
  );

  TVerticalFocusPosition = (
    vfpNone,
    vfpAuto,
    vfpTop,
    vfpCenter,
    vfpBottom
  );

  THorizontalFocusPosition = (
    hfpNone,
    hfpAuto,
    hfpLeft,
    hfpCenter,
    hfpRight
  );

  TNavigationInfo = record
    nNavigating   : Boolean;
    nReason       : TScrollReason;
    nCommand      : TScrollCommand;
    nOldCol       : Integer;
    nOldRow       : Integer;
    nCellExited   : Boolean;
//    nVertPosition : TVerticalFocusPosition;
//    nHorzPosition : THorizontalFocusPosition;
  end;

  TNavigationResults = (
    nrColChanged,
    nrRowChanged
  );
  TNavigationResult = set of TNavigationResults;

  TClickInfos = (
    ciDownCell,  // er is op een cell geklikt, TMouseInfo.miDownCell is gevuld (MouseDown)
    ciMoveCell
  );
  TClickInfo = set of TClickInfos;

  TMouseDragState = (
    mdNone,
    //mdColSizingPossible,  // bepaald door MouseMove als muis op lijn
    mdColSizing,          // colsizing is in actie
    //mdRowSizingPossible,  // bepaald door MouseMove als muis op lijn
    mdRowSizing,          // rowsizing is in actie
    //mdCellSizingPossible, // bepaald door MouseMove als muis op kruising
    mdCellSizing         // cellsizing is in actie
    //mdRectSelectingStart, // bepaald door MouseDown
    //mdRectSelecting       // rectselecting in actie
  );

  TLineHit = (
    lhHorz,  // horizontale lijn geraakt door muis
    lhVert   // verticale lijn geraakt door muis
  );
  TLineHits = set of TLineHit;

  THeaderAreas = record
    hTop: TRect;
    hLeft: TRect;
    hRight: TRect;
    hBottom: TRect;

    hTopLeft: TRect;
    hTopRight: TRect;
    hBottomLeft: TRect;
    hBottomRight: TRect;
  end;

  TMatrixArea = (
    maNone,
    maGrid,
    maTopLeft,
    maTop,
    maTopRight,
    maLeft,
    maRight,
    maBottomLeft,
    maBottom,
    maBottomRight
  );

  TValidMatrixArea = maGrid..maBottomRight;
  TAreaRects = array[TValidMatrixArea] of PRect;

  TMouseStateInfo = record
    mHitResult   : Boolean;             // hit?
    mMouseCoord  : TPoint;              // muiscoordinaat
    mHitCol      : Integer;             // welke kolom
    mHitRow      : Integer;             // welke rij
    mHitRect     : TRect;               // logische cellrect
    mArea        : TMatrixArea;         // welk gebied
    mHeaders     : TMatrixHeaders;      // op welke headercollection
    mHeaderIndex : Integer;             // op welke header
    mLineHits    : TLineHits;           // hor/ver resizelinehit
  end;

  TMouseInfoEx = record
    InMatrix         : Boolean;
    ShiftState       : TShiftState;
    DownInfo         : TMouseStateInfo;
    MoveInfo         : TMouseStateInfo;
    UpInfo           : TMouseStateInfo;
    ProcInfo         : (ctNone, ctDown, ctMove, ctUp);
    ResizeState      : TMouseDragState;
    ResizeRect       : TRect;
    StartingWidth    : Integer;
    StartingHeight   : Integer;
  end;


  TInternalState = (
    isCalculatingPageInfo,    // We zijn een visuele pagina aan het berekenen
    isCalculatingHorzInfo,    // We zijn horizontaal aan het berekenen
    isCalculatingVertInfo,    // We zijn vertikaal aan het berekenen
    isTraversingCells,        // Er wordt door de cellen heengelopen
    isTraversingCols,         // Er wordt door de kolommen heengelopen
    isTraversingRows,         // Er wordt door de rijen heengelopen
    isTraversingVisibleCells, // Er wordt door de zichtbare cellen heengelopen
    isTraversingVisibleCols,  // Er wordt door de zichtbare kolommen heengelopen
    isTraversingVisibleRows,  // Er wordt door de zichtbare rijen heengelopen
    isResizeTimerActive,      // Muis resize cursor timer is actief
    isClearSearchTimerActive, // Clear incremental search timer is actief
    isPaintingMatrix,         // PaintMatrix is actief
    isEditBeginning,          // Bezig met de methode DoEditBegin
    isEditEnding,             // Bezig met de methode DoEditEnd
    isEditCanceling,          // Bezig met de methode DoEditCancel
    isKillingFocus,           // Bezig met wmkillfocus
    isAutoBounds,             // Bezig met updaten autowidth/height
    isEditing,                // Edit bezig
    isVisibleHeaders,         // Er zijn zichtbare headers
    isVisibleTopHeaders,      // Er zijn zichtbare TopHeaders
    isVisibleLeftHeaders,     // Er zijn zichtbare TopHeaders
    isVisibleRightHeaders,    // Er zijn zichtbare TopHeaders
    isVisibleBottomHeaders,   // Er zijn zichtbare TopHeaders
    isUpdatingColumns,        // Matrix.UpdateColumns
    isCreatingColumns,
    isDestroyingColumns
  );
  TInternalStates = set of TInternalState;

  TMatrixDataMode = (
    dmFixed,
    dmVirtual
  );

  TPaintCellStage = (
    pcsPrepare,
    pcsImageIndex,
    pcsBeforePaint,
    pcsBackGround,
    pcsContents,
    pcsAfterPaint

//    pcsHeaderPrepare,
  //  pcs
  );
  TPaintCellStages = set of TPaintCellStage;

  TCellParams = class
  private
    cpCalculating  : Boolean;
  { altijd valide, ook wanneer calculating }
    cpCol          : Integer;           // kolom
    cpRow          : Integer;           // rij
    cpCellBitmap   : TMxBitmap;
    cpCellCanvas   : TCanvas;           // cell-canvas (read only)
    cpActiveStyle  : TCellStyle;        // ref naar actieve gekozen cellstyle (read only)
    cpMatrixStyle  : TCellStyle;        // ref naar matrixstyle
    cpColStyle     : TCellStyle;        // ref naar colstyle
    cpRowStyle     : TCellStyle;        // ref naar rowstyle
    cpTextFlags    : DWORD;             // windows flags text - gezet ahv cellstyle
  { alleen valide als calculating }
    cpCalcSize     : Integer;           // = colwidth als autohoogte, = rowheight als autobreedte
  { alleen valide als er echt getekend wordt }
    cpStage        : TPaintCellStage;   // tekenfase
    cpCellBounds   : TRect;             // globale cellpositie in matrix (read only)
    cpCanvasRect   : TRect;             // zero offset rect voor pCellCanvas (read only)
    cpContentsRect : TRect;             // deel van cpCanvasRect (margin) (read only)
    cpImageRect    : TRect;             // deel van cpCanvasRect (read only)
    cpTextParams   : TDrawTextParams;   // tekst parameters (read only)
    cpDrawState    : TCellDrawState;    // info over cell draw state (read only)
    cpBackColor    : TColor;            // huidige achtergrondkleur (ahv state)
    cpTextColor    : TColor;            // huidge tekstkleur (ahv state)
    cpImageIndex   : Integer;           // imageindex (event)
    cpImageList    : TCustomImageList;  // imagelist (prepared)
    cpHandled      : Boolean;           // event afgehandeld
    procedure SetImageIndex(const Value: Integer);
    procedure CalcImageRect(ImageWidth, ImageHeight: Integer);
    procedure SetImageList(const Value: TCustomImageList);
  public
    property pCol: Integer read cpCol;
    property pRow: Integer read cpRow;
    property pStage: TPaintCellStage read cpStage;
    property pCellBitmap: TMxBitmap read cpCellBitmap;
    property pCellCanvas: TCanvas read cpCellCanvas;
    property pCellBounds: TRect read cpCellBounds;
    property pCanvasRect: TRect read cpCanvasRect;
    property pDrawState: TCellDrawState read cpDrawState;
    property pCellStyle: TCellStyle read cpActiveStyle;
    property pContentsRect: TRect read cpContentsRect write cpContentsRect;
    property pTextFlags: DWORD read cpTextFlags write cpTextFlags;
    property pBackColor: TColor read cpBackColor write cpBackColor;
    property pTextColor: TColor read cpTextColor write cpTextColor;
    property pImageIndex: Integer read cpImageIndex write SetImageIndex;
    property pImageList: TCustomImageList read cpImageList write SetImageList;
    property Handled: Boolean read cpHandled write cpHandled;
  end;

  THeaderParams = class
  private
    hpCol          : Integer;           // kolom
    hpRow          : Integer;           // rij
    hpHeader       : TMatrixHeader;     // ref naar header
    hpHeaderCanvas : TCanvas;           // header-canvas
    hpCellStyle    : TCellStyle;        // ref naar cellstyle
    hpTextFlags    : DWORD;             // windows flags text - gezet ahv cellstyle
    hpStage        : TPaintCellStage;   // tekenfase
    hpHeaderBounds : TRect;             // globale cellpositie in matrix (read only)
    hpCanvasRect   : TRect;             // zero offset rect voor pCellCanvas (read only)
    hpContentsRect : TRect;             // deel van pCanvasRect (read only)
    hpImageRect    : TRect;             // deel van pCanvasRect (read only)
    hpTextParams   : TDrawTextParams;   // tekst parameters (read only)
    hpDrawState    : TCellDrawState;    // info over cell draw state (read only)
    hpBackColor    : TColor;            // huidige achtergrondkleur (ahv state)
    hpTextColor    : TColor;            // huidige tekstkleur (ahv state)
    hpImageIndex   : Integer;           // image index
    hpImageList    : TCustomImageList;  // imagelist
    hpHandled      : Boolean;           // event afgehandeld
    procedure CalcImageRect(ImageWidth, ImageHeight: Integer);
  public
    property pCol: Integer read hpCol;
    property pRow: Integer read hpRow;
    property pStage: TPaintCellStage read hpStage;
    property pHeader: TMatrixHeader read hpHeader;
//    property pHeaderBitmap: TMxBitmap read hpHeaderBitmap;
    property pHeaderCanvas: TCanvas read hpHeaderCanvas;
    property pHeaderBounds: TRect read hpHeaderBounds;
    property pCanvasRect: TRect read hpCanvasRect;
    property pDrawState: TCellDrawState read hpDrawState;
    property pCellStyle: TCellStyle read hpCellStyle;
    property pContentsRect: TRect read hpContentsRect write hpContentsRect;
    property pTextFlags: DWORD read hpTextFlags write hpTextFlags;
    property pBackColor: TColor read hpBackColor write hpBackColor;
    property pTextColor: TColor read hpTextColor write hpTextColor;
    property pImageIndex: Integer read hpImageIndex write hpImageIndex;
    property pImageList: TCustomImageList read hpImageList write hpImageList;
    property Handled: Boolean read hpHandled write hpHandled;
  end;

  TGridCache = class(TMatrixObject)
  private
    fValidHeights: Boolean;
    fHeights: TIntegerList;
    fWidths:  TIntegerList;
    procedure InternalUpdate;
  public
    constructor Create(aMatrix: TBaseMatrix);
    destructor Destroy; override;
    procedure Update(const PgInfo: TPageInfo);
    procedure CacheHeight(aRow, aHeight: Integer);
    procedure InvalidateHeight(aRow: Integer);
    procedure InvalidateHeights;
    function ProbeHeight(aRow: Integer): Integer;
    property ValidHeights: Boolean read fValidHeights;
  end;


  TMatrixSelector = class(TCustomControl)
  private
  protected
  public
    constructor Create(aOwner: TComponent); override;
    procedure Paint; override;
  published
  end;

  TCellDataClass = class of TCellData;
  TCellData = class(TPersistent)
  private
    fText: string;
  public
    property Text: string read fText write fText;
    procedure Clear; virtual;
  end;

  IMatrixEditor = interface
    //[SID_MatrixEditLink]
    function PrepareEdit(aMatrix: TBaseMatrix; aCol, aRow: Integer): Boolean;
    function BeginEdit: Boolean;
    function EndEdit: Boolean;
    function CancelEdit: Boolean;
    function GetBounds: TRect;
    procedure SetBounds(R: TRect);
    //procedure ProcessMessage(var Message: TMessage);
  end;

  TCellDataGraphic = class(TCellData)
  private
    fGraphic: TGraphic;
  public
    property Graphic: TGraphic read fGraphic write fGraphic;
  end;

  {-------------------------------------------------------------------------------
    Cell Painters
  -------------------------------------------------------------------------------}
  TCellPainterClass = class of TCellPainter;
  TCellPainter = class
  protected
    class procedure DoPaint(Params: TCellParams; Data: TCellData); virtual;
  end;

  TStringPainter = class(TCellPainter)
  protected
    class procedure DoPaint(Params: TCellParams; Data: TCellData); override;
  end;

  TImagePainter = class(TCellPainter)
  protected
    class procedure DoPaint(Params: TCellParams; Data: TCellData); override;
  end;

  {-------------------------------------------------------------------------------
    Headers
  -------------------------------------------------------------------------------}
  THeaderType = (
    htLeft,
    htTop,
    htRight,
    htBottom
  );
  THeaderTypes = set of THeaderType;

  TMxHeaderOption = (
    hoButtonLook,
    hoResize
  );


  TMatrixHeaderClass = class of TMatrixHeader;
  TMatrixHeader = class(TCollectionItem)
  private
    fHeaderType : THeaderType;
    fSize       : Integer;
    fSpanned    : Boolean;
    fLineWidth  : Integer;
    fDownIndex  : Integer;
    fIndex      : Integer;
    fStyle      : TCellStyle;
    fHidden     : Boolean;  // nog niet geimplementeerd
    fTag        : Integer;
    procedure SetSize(const Value: Integer);
    procedure SetSpanned(const Value: Boolean);
    procedure SetLineWidth(const Value: Integer);
    function GetMatrix: TBaseMatrix;
    procedure SetStyle(const Value: TCellStyle);
    procedure StyleChanged(Sender: TObject);
    procedure SetHidden(const Value: Boolean);
  protected
    procedure SetIndex(Value: Integer); override;
    property Matrix: TBaseMatrix read GetMatrix;
  public
    constructor Create(aCollection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    property HeaderType: THeaderType read fHeaderType;
  published
    property Size: Integer read fSize write SetSize;
    property LineWidth: Integer read fLineWidth write SetLineWidth default 1;
    property Spanned: Boolean read fSpanned write SetSpanned;
    property Style: TCellStyle read fStyle write SetStyle;
    property Hidden: Boolean read fHidden write SetHidden;
    property Tag: Integer read fTag write fTag;
  end;

  TMatrixHeaders = class(TOwnedCollection)
  private
    fMatrix     : TBaseMatrix;
    fHeaderType : THeaderType;
    fSize       : Integer; // totale size van alle headeritems
    function GetItem(Index: Integer): TMatrixHeader;
  protected
    procedure Notify(Item: TCollectionItem; Action: TCollectionNotification); override;
    procedure Update(Item: TCollectionItem); override;
    procedure UpdateSize;
  public
    constructor Create(aOwner: TPersistent; aHeaderClass: TMatrixHeaderClass; aHeaderType: THeadertype);
    function Add: TMatrixHeader;
    function Insert(aIndex: Integer): TMatrixHeader;
    property Size: Integer read fSize;
    property HeaderType: THeaderType read fHeaderType;
    property Items[Index: Integer]: TMatrixHeader read GetItem; default;
  published
  end;

  TMxTopHeader = class(TMatrixHeader);
  TMxLeftHeader = class(TMatrixHeader);
  TMxTemplate = class;

  {-------------------------------------------------------------------------------
    Matrix events
  -------------------------------------------------------------------------------}
  TCellEvent = procedure(Sender: TObject; aCol, aRow: Integer) of object;
  THoverCellEvent = procedure(Sender: TObject; aCol, aRow: Integer; Leave: Boolean) of object;
  TMeasureRowEvent = procedure(Sender: TObject; aRow: Integer; var aHeight: Integer) of object;
  TMeasureColEvent = procedure(Sender: TObject; aCol: Integer; var aWidth: Integer) of object;
  TGetCellHintEvent = procedure(Sender: TObject; aCol, aRow: Integer; var aHint: string) of object;
  TPaintBackGroundEvent = procedure(Sender: TObject; const PaintInfo: TPaintInfo) of object;
  TPaintCellEvent = procedure(Sender: TObject; Params: TCellParams; Data: TCellData) of object;
  TGetCellPainterClassEvent = procedure(Sender: TObject; Params: TCellParams; Data: TCellData; out aPainterClass: TCellPainterClass) of object;
  TCellClickEvent = procedure(Sender: TObject; aCol, aRow: Integer) of object;
  TCellDataCreateEvent = procedure(Sender: TObject; out Data: TCellData) of object;
  TGetCellDataEvent = procedure(Sender: TObject; aCol, aRow: Integer; out Data: TCellData) of object;
  TGetActiveCellStyleEvent = procedure(Sender: TObject; aCol, aRow: Integer; aMatrixStyle, aColStyle, aRowStyle: TCellStyle; var ActiveStyle: TCellStyle) of object;
  TCreateEditorEvent = procedure(Sender: TObject; aCol, aRow: Integer; var aEditor: IMatrixEditor) of object;
  // header events
  TPaintHeaderEvent = procedure(Sender: TObject; Params: THeaderParams) of object;
  TGetHeaderTextEvent = procedure(Sender: TObject; aHeader: TMatrixHeader; aIndex: Integer; var aText: string) of object;
  TTopHeaderClickEvent = procedure(Sender: TObject; aHeader: TMatrixHeader; aCol: Integer) of object;
  TBottomHeaderClickEvent = procedure(Sender: TObject; aHeader: TMatrixHeader; aCol: Integer) of object;
  TLeftHeaderClickEvent = procedure(Sender: TObject; aHeader: TMatrixHeader; aRow: Integer) of object;
  TRightHeaderClickEvent = procedure(Sender: TObject; aHeader: TMatrixHeader; aRow: Integer) of object;


  TBaseMatrix = class(TCustomControl)
  private
    procedure SetHotImages(const Value: TCustomImageList);
    procedure SetCellStyle(const Value: TCellStyle);
    procedure SetDefaultHeaderLineColor(const Value: TColor);
    procedure SetVisibleRowsRequested(const Value: Integer);
    procedure SetVisibleColsRequested(const Value: Integer);
  protected
    fCanCopyTemplate           : Boolean;
    fBlockCommand              : string;
    fBorderStyle               : TBorderStyle;
    fCellData                  : TCellData;
    fCellOffsetX               : Integer; // x-pos in pixels van eerste cell
    fCellOffsetY               : Integer; // y-pos in pixels van eerste cell
    fCellParams                : TCellParams;
    fCellStyle                 : TCellStyle;
    fClientHeight              : Integer; // cached ClientHeight
    fClientWidth               : Integer; // cached ClientWidth
    fCol                       : Integer;
    fColCount                  : Integer;
    fColumns                   : TMxColumns;
    fCustomPainting            : Boolean;
    fCustomPaintStages         : TPaintCellStages;
    fDataMode                  : TMatrixDataMode;
    fDefaultColWidth           : Integer;
    fDefaultHorzLineWidth      : Integer;
    fDefaultLineColor          : TColor;
    fDefaultHeaderLineColor    : TColor;
    fDefaultRowHeight          : Integer;
    fDefaultVertLineWidth      : Integer;
    fEditCol                   : Integer;
    fEditor                    : IMatrixEditor;
    fEditRow                   : Integer;
    fGridCache                 : TGridCache;
    fGridRect                  : TRect; // cellen gebied
    fHeaderAreas               : THeaderAreas; // header gebieden
    fHeaderParams              : THeaderParams;
    fImages                    : TCustomImageList;
    fHotImages                 : TCustomImageList;
    fAreaRects                 : TAreaRects; // pointers naar gebieden
    fHighlightString           : string;
    fInternalStates            : TInternalStates;
    fLeftHeaders               : TMatrixHeaders; // headers links
    fRightHeaders              : TMatrixHeaders; // headers rechts
    fMatrixFocused             : Boolean;
    fMatrixOffsetX             : Integer;
    fMatrixOffsetY             : Integer;
    fMaxColWidth               : Integer;
    fMaxRowHeight              : Integer;
    fMinColWidth               : Integer;
    fMinRowHeight              : Integer;
    fMouseInfo                 : TMouseInfoEx;
    //fMultiCastEventHandler     : TMxMultiCastEventHandler;
    fNavigationInfo            : TNavigationInfo;
    fNavigationRecursion       : Integer;
    fOnCellClick               : TCellClickEvent;
    fOnCellDblClick            : TCellClickEvent;
    fOnCreateEditor            : TCreateEditorEvent;
    fOnGetActiveStyle          : TGetActiveCellStyleEvent;
    fOnGetCellData             : TGetCellDataEvent;
    fOnGetCellHint             : TGetCellHintEvent;
    fOnGetCellPainter          : TGetCellPainterClassEvent;
    fOnGetHeaderText           : TGetHeaderTextEvent;
    fOnHoverCell               : THoverCellEvent;
    fOnMeasureRow              : TMeasureRowEvent;
    fOnMouseLeave              : TNotifyEvent;
    fOnMouseEnter              : TNotifyEvent;
    fOnPaintBackGround         : TPaintBackGroundEvent;
    fOnPaintCell               : TPaintCellEvent;
    fOnPaintHeader             : TPaintHeaderEvent;
    fOnSelectCell              : TCellEvent;
    fOnDeselectCell            : TCellEvent;
    fOnLeftHeaderClick         : TLeftHeaderClickEvent;
    fOnTopHeaderClick          : TTopHeaderClickEvent;
    fOptions                   : TMatrixOptions;
    fPageCache                 : TPageInfo; // paint cache voor actieve pagina
    fPaintLock                 : Integer;
    fLineTreshold              : Integer;
    fRow                       : Integer;
    fRowCache                  : TSizeCache;
    fRowCount                  : Integer;
    fRows                      : TMxRows;
    fScrollBars                : TScrollStyle;
    fSearchString              : string;
    fSelection                 : TRect;
    fSelectMode                : TSelectMode;
    fSizingLinesRect           : TRect;
    fTopHeaders                : TMatrixHeaders; // topheaders
    fTopLeft                   : TPoint;
    fUpdateRect                : TRect; // cached windows updaterect
    fUseCache                  : Boolean;
    fVisibleRowsRequested      : Integer;
    fVisibleColsRequested      : Integer;
    fZoomRatio                 : Extended;
  { interne methods }
    procedure CacheClientSize;
    function CalcAutoClientWidth: Integer;
    function CalcAutoClientHeight: Integer;
    procedure CalcInfoHorizontal(var aInfo: TPageAxisInfo; aLeft: Integer = -1);
    procedure CalcInfoVertical(var aInfo: TPageAxisInfo; aTop: Integer = -1);
    procedure CalcPageInfo(var aPageInfo: TPageInfo; aLeft: integer = -1; aTop: Integer = -1; ForceRecalc: Boolean = False);
    function CalcPgDn(const CurrentInfo: TPageInfo; var NewInfo: TPageInfo): Boolean;
    function CellRectToScreenRect(aCol, aRow: integer; var aRect: TRect): Boolean;
    function CellRectToVirtualScreenRect(aCol, aRow: integer; var aRect: TRect): Boolean;
    procedure CellStyleChanged(Sender: TObject);
    procedure CheckCol(aCol: Integer);
    procedure CheckCoord(aCol, aRow: Integer); overload;
    procedure CheckCoord(aCoord: TPoint); overload;
    procedure CheckRow(aRow: Integer);
    procedure ColumnAdded(Item: TMxColumn);
    procedure ColumnDeleting(Item: TMxColumn);
    procedure ColumnIndexChanged(Item: TMxColumn; OldIndex, NewIndex: Integer);
    procedure ColumnVisualChanged(aColumn: TMxColumn; SomeSizeChanged: Boolean);
    function ColVisibility(aCol: Integer; const HorzInfo: TPageAxisInfo): Integer;
    function DoChangeCol(Delta: Integer): Boolean; //scroll
    function DoChangeRow(Delta: Integer): Boolean; //scroll
    function DoGetActiveStyle(aCol, aRow: Integer; var aMatrixStyle, aColStyle, aRowStyle: TCellStyle): TCellStyle; virtual;
    function DoGotoFirstRow: Boolean; //scroll
    function DoGotoLastRow: Boolean; //scroll
    function DoGotoFirstCol: Boolean; //scroll
    function DoGotoLastCol: Boolean; //scroll
    procedure DoCellExit(aCol, aRow: Integer); virtual;
    procedure DoCellEnter(aCol, aRow: Integer); virtual;
    procedure DoHoverCell(aCol, aRow: Integer; Leave: Boolean); virtual;
    function GetBkColor(const CellStyle: TCellStyle; DrawState: TCellDrawState): TColor;
    function GetTxtColor(const CellStyle: TCellStyle; DrawState: TCellDrawState): TColor;
    function GetCellStyleEx(aCol, aRow: Integer; var aMatrixStyle, aColStyle, aRowStyle: TCellStyle): TCellStyle;
    function GetEffectiveHeaderLineWidth(aHeader: TMatrixHeader): Integer;
    function GetEffectiveHorzLineWidth(aRow: Integer): Integer;
    function GetEffectiveRowHeight(aRow: Integer; Calcing: Boolean = False): Integer;
    function GetEffectiveVertLineWidth(aCol: Integer): Integer;
    function FindCachedWidth(aCol: Integer): Integer;
    function FindCachedHeight(aRow: Integer): Integer;
    procedure HandleMouseResize;
    procedure HandleScrollBarCommand(aScrollBar, aScrollCode, aPos: Cardinal); virtual;
    procedure HeadersChanged(Sender: TObject);
    procedure InvalidateHoveringCell;
    procedure InternalSetCol(aCol: Integer; ChangeResult: Boolean);
    procedure InternalSetRow(aRow: Integer; ChangeResult: Boolean);
    procedure InvalidateMouseInfo;
    procedure InvalidatePageInfo;
    procedure InvalidateRowCache;
    procedure InvalidateSelection;
    function IsActiveControl: Boolean;
    procedure MatrixOptionsChanged(Sender: TObject);
    function NewPageHorz(aNewCol: Integer; aPosition: THorizontalFocusPosition): Integer;
    function NewPageVert(aNewRow: Integer; aPosition: TVerticalFocusPosition): Integer;
    function NewPageVertDownwards(aNewRow: Integer; aPosition: TVerticalFocusPosition): Integer;
    function PageInfoPtr(aLeft: integer = -1; aTop: Integer = -1; ForceRecalc: Boolean = False): TPageInfo;
    procedure PaintSizingLines(MouseX, MouseY: Integer; DoHide: Boolean = False);
    function PointToHeaderIndex(X, Y: Integer; aType: THeaderType; aArea: TValidMatrixArea): Integer;
    function RestrictCol(var aCol: Integer): Boolean;
    function RestrictLeft(var aLeft: Integer): Boolean;
    function RestrictRow(var aRow: Integer): Boolean;
    function RestrictTop(var aTop: Integer): Boolean;
    procedure RowAdded(Item: TMxRow);
    procedure RowDeleting(Item: TMxRow);
    function RowVisibility(aRow: Integer; const VertInfo: TPageAxisInfo): Integer;
    procedure RowVisualChanged(aRow: TMxRow; SomeSizeChanged: Boolean);
    procedure StateChange(Enter, Leave: TInternalStates);
    procedure StateEnter(aState: TInternalState);
    procedure StateLeave(aState: TInternalState);
    procedure TimerStart(ID: Integer);
    procedure TimerStop(ID: Integer);
    procedure TraverseCells(LocalCellProc: pointer; const aRange: TRect);
    procedure TraverseCols(LocalColProc: pointer; const aRange: TAxisRange);
    procedure TraverseRows(LocalRowProc: Pointer; const aRange: TAxisRange);
    function TraverseRowHeights(LocalRowProc: Pointer; const aRange: TAxisRange): Integer;
    procedure TraverseVisibleCells(LocalCellProc: pointer; const PageInfo: TPageInfo; const aRange: TRect);
    procedure TraverseVisibleCols(LocalColProc: pointer; const HorzInfo: TPageAxisInfo; const aRange: TAxisRange);
    procedure TraverseVisibleRows(LocalRowProc: Pointer; const VertInfo: TPageAxisInfo; const aRange: TAxisRange; const aWindow: TRect);
    procedure TraverseVisibleLeftHeaders(LocalProc: Pointer);
    procedure TraverseVisibleTopHeaders(LocalProc: Pointer);
    procedure UpdateAutoBounds(Force: Boolean = False);
    procedure UpdateBlockCommand(C: Char= #0);
    procedure UpdateColumns;
    procedure UpdateGridRect;
    procedure UpdateHeaders;
    procedure UpdateHeaderSizes;
    procedure UpdateRows;
    procedure UpdateScrollPos;
    procedure UpdateSelection(aCol, aRow: Integer);
    function ValidCol(aCol: Integer): Boolean;
    function ValidCoord(aCol, aRow: Integer): Boolean; overload;
    function ValidCoord(aCoord: TPoint): Boolean; overload;
    function ValidPageInfo: Boolean;
    function ValidRow(aRow: Integer): Boolean;
  { streaming }
    function IsStoredColumns: Boolean;
    function IsStoredRows: Boolean;
  { property access }
    function GetCellText(aCol, aRow: Integer): string;
    function GetColRow: TPoint;
    function GetColWidth(aCol: Integer): Integer;
    function GetColumns: TMxColumns;
    function GetLeftHeaders: TMatrixHeaders;
    function GetRightHeaders: TMatrixHeaders;
    function GetRowHeight(aRow: Integer): Integer;
    function GetRows: TMxRows;
    function GetTopHeaders: TMatrixHeaders;
    function GetVisibleRows: Integer;
    procedure SetBorderStyle(const Value: TBorderStyle);
    procedure SetCellOffsetX(const Value: Integer);
    procedure SetCellOffsetY(const Value: Integer);
    procedure SetCellText(aCol, aRow: Integer; const Value: string);
    procedure SetCol(const Value: Integer);
    procedure SetColWidth(aCol: Integer; Value: Integer);
    procedure SetColCount(Value: Integer);
    procedure SetColRow(const Value: TPoint);
    procedure SetColumns(const Value: TMxColumns);
    procedure SetCustomPainting(const Value: Boolean);
    procedure SetDefaultColWidth(Value: Integer);
    procedure SetDefaultHorzLineWidth(const Value: Integer);
    procedure SetDefaultLineColor(const Value: TColor);
    procedure SetDefaultRowHeight(Value: Integer);
    procedure SetDefaultVertLineWidth(const Value: Integer);
    procedure SetImages(const Value: TCustomImageList);
    procedure SetLeftHeaders(const Value: TMatrixHeaders);
    //procedure SetMultiCastEventHandler(const Value: TMxMultiCastEventHandler);
    procedure SetOnGetActiveStyle(const Value: TGetActiveCellStyleEvent);
    procedure SetOnMeasureRow(const Value: TMeasureRowEvent);
    procedure SetOptions(const Value: TMatrixOptions);
    procedure SetParent(AParent: TWinControl); override;
    procedure SetRightHeaders(const Value: TMatrixHeaders);
    procedure SetRow(const Value: Integer);
    procedure SetRowCount(const Value: Integer);
    procedure SetRowHeight(aRow: Integer; Value: Integer);
    procedure SetRows(const Value: TMxRows);
    procedure SetScrollBars(const Value: TScrollStyle);
    procedure SetSelectMode(const Value: TSelectMode);
    procedure SetTopHeaders(const Value: TMatrixHeaders);
    procedure SetVisibleRows(const Value: Integer);
  { vcl + win messages }
    procedure CMBorderChanged(var Msg: TMessage); message CM_BORDERCHANGED;
    procedure CMCtl3DChanged(var Message: TMessage); message CM_CTL3DCHANGED;
    procedure CMExit(var Message: TMessage); message CM_EXIT;
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure CMRecreateWnd(var Message: TMessage); message CM_RECREATEWND;
    procedure WMEraseBkgnd(var Msg: TWmEraseBkgnd); message WM_ERASEBKGND;
    procedure WMGetDlgCode(var Msg: TWMGetDlgCode); message WM_GETDLGCODE;
    procedure WMHHcroll(var Msg: TWMHScroll); message WM_HSCROLL;
    procedure WMNCPaint(var Message: TRealWMNCPaint); message WM_NCPAINT;
    procedure WMPaint(var Msg: TWMPaint); message WM_PAINT;
    procedure WMKillFocus(var Msg: TWMKillFocus); message WM_KILLFOCUS;
    procedure WMSetCursor(var Msg: TWMSetCursor); message WM_SETCURSOR;
    procedure WMSetFocus(var Msg: TWMSetFocus); message WM_SETFOCUS;
    procedure WMTimer(var Msg: TWMTimer); message WM_TIMER;
    procedure WMVScroll(var Msg: TWMVScroll); message WM_VSCROLL;
  { matrix messages }
    procedure EMMCancelEdit(var Msg: TMessage); message EMM_CANCELEDIT;
    procedure EMMEndEdit(var Msg: TMessage); message EMM_ENDEDIT;
    procedure EMMExitEdit(var Msg: TMessage); message EMM_EXITEDIT;
  protected
    fVirtualData: Boolean;
  { protected overrides }
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    function DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    function DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    function GetClientRect: TRect; override;
    procedure KeyDown(var Key: word; Shift: TShiftState); override;
    procedure KeyUp(var Key: word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Notification(aComponent: TComponent; Operation: TOperation); override;
    procedure Paint; override;
    procedure Resize; override;
  { protected methods }
    procedure CalculateCellHeight(Params: TCellParams; Data: TCellData; var aHeight: Integer); virtual;
    function CalculateRowHeight(aRow: Integer): Integer; virtual;
    function CanEditCell(aCol, aRow: Integer): Boolean;
    function CanPaint: Boolean;
    function ChangeCol(Delta: Integer; var aNewCol: Integer): Boolean; virtual; //scroll
    function ChangeRow(Delta: Integer; var aNewRow: Integer): Boolean; virtual; //scroll
    procedure CustomPaintBackGround(const PaintInfo: TPaintInfo); virtual;
    procedure CustomPaintCell(Params: TCellParams; const aPaintInfo: TPaintInfo; Data: TCellData); virtual;
    procedure CustomAfterPaint(const PaintInfo: TPaintInfo); virtual;
    procedure DoAfterColumnIndexChanged(aItem: TMxColumn; aOldIndex, aNewIndex: Integer); virtual;
    procedure DoAfterDeletingColumn(aItem: TMxColumn); virtual;
    function DoCreateColumns: TMxColumns; dynamic;
    function DoCreateEditor(aCol, aRow: Integer): IMatrixEditor; virtual;
    function DoCreateRows: TMxRows; dynamic;
    function DoCreateCellData: TCellData; dynamic;
    procedure DoEditBegin;
    procedure DoEditCancel(Refocus: Boolean = True);
    procedure DoEditEnd(Refocus: Boolean = True);
    procedure DoCellClick(aCol, aRow: Integer); virtual;
    procedure DoCellDblClick(aCol, aRow: Integer);
    procedure DoCellHint(aCol, aRow: Integer); virtual;
    procedure DoGetCellData(aCol, aRow: Integer; out Data: TCellData); virtual;
    procedure DoSetCellData(aCol, aRow: Integer; Data: TCellData); virtual;
    function DoGetCellPainter(Params: TCellParams; Data: TCellData): TCellPainterClass; virtual;
    function DoGetHeaderText(aHeader: TMatrixHeader; aIndex: Integer): string; virtual;
    function DoGetCellHint(aCol, aRow: Integer): string; virtual;
    procedure DoHeaderClick(aHeader: TMatrixHeader; aCol, aRow: Integer); virtual;
    function GotoFirstRow(var aNewRow: Integer): Boolean; virtual; //scroll
    function GotoLastRow(var aNewRow: Integer): Boolean; virtual; //scroll
    function GotoFirstCol(var aNewCol: Integer): Boolean; virtual; //scroll
    function GotoLastCol(var aNewCol: Integer): Boolean; virtual; //scroll
    procedure DoNavigate(aReason: TScrollReason; aCommand: TScrollCommand; aHorzPosition: THorizontalFocusPosition = hfpAuto; aVertPosition: TVerticalFocusPosition = vfpAuto; XParam: Integer = -1; YParam: Integer = -1); virtual;
    procedure DoSetBottomRight(aRight, aBottom: Integer);
    function HitTest(X, Y: Integer; out aCell: TPoint; out aCellRect: TRect): Boolean;
    function HitTestEx(X, Y: Integer; out aHitCol: Integer; out aHitRow: Integer; out aCellRect: TRect; out aHeaders: TMatrixHeaders; out aHeaderIndex: Integer; out aArea: TMatrixArea; out aLineHits: TLineHits): Boolean;
    function HitTestResize(X, Y: Integer; out aCell: TPoint; out aCellRect: TRect; out aLineHits: TLineHits): Boolean;
    procedure IncrementalSearch;
    procedure IncrementalSearchUpdate(aKey: Char = #0; one:boolean=false);
    procedure MoveCurrent(aCol, aRow: Integer; aHorz: THorizontalFocusPosition = hfpAuto; aVert: TVerticalFocusPosition = vfpAuto);
    procedure MoveTopLeft(aLeft, aTop: Integer; aFocusedCol: Integer = -1; aFocusedRow: Integer = -1);
    function Navigation(aReason: TScrollReason; aCommand: TScrollCommand; XParam: Integer = -1; YParam: Integer = -1): TNavigationResult; virtual;
    function NavigationFixed(aReason: TScrollReason; aCommand: TScrollCommand; XParam: Integer = -1; YParam: Integer = -1): TNavigationResult; virtual;
    procedure PaintBackGround(const PaintInfo: TPaintInfo); virtual;
    procedure PaintCell(Params: TCellParams; const aPaintInfo: TPaintInfo; Data: TCellData);
    procedure PaintCells(const PaintInfo: TPaintInfo);
    procedure PaintHeader(Params: THeaderParams; const PaintInfo: TPaintInfo);
    procedure PaintHeaders(const PaintInfo: TPaintInfo);
    procedure PaintLines(const PaintInfo: TPaintInfo);
    procedure PaintLinesEx(const PaintInfo: TPaintInfo);
    procedure PaintMatrix(aCanvas: TCanvas; aWindow: TRect);
    procedure PreparePaintCell(Params: TCellParams; Data: TCellData); virtual;
    procedure PreparePaintHeader(Params: THeaderParams);
    procedure Reset;
  { protected properties vooraan }
    property Options: TMatrixOptions read fOptions write SetOptions stored True; // publish vooraan!
  { protected properties }
    property BorderStyle: TBorderStyle read fBorderStyle write SetBorderStyle;
    property CellOffsetX: Integer read fCellOffsetX write SetCellOffsetX;
    property CellOffsetY: Integer read fCellOffsetY write SetCellOffsetY;
    property CellStyle: TCellStyle read fCellStyle write SetCellStyle;
    property CellText[aCol, aRow: Integer]: string read GetCellText write SetCellText;
    property Col: Integer read fCol  write SetCol; // do not publish
    property ColCount: Integer read fColCount write SetColCount default DEF_COLCOUNT;
    property ColRow: TPoint read GetColRow write SetColRow; // do not publish
    property ColWidths[aCol: Integer]: Integer read GetColWidth write SetColWidth;
    property Columns: TMxColumns read GetColumns write SetColumns stored IsStoredColumns;
    property CustomPainting: Boolean read fCustomPainting write SetCustomPainting;
    property CustomPaintStages: TPaintCellStages read fCustomPaintStages write fCustomPaintStages;
    property DataMode: TMatrixDataMode read fDataMode write fDataMode;
    property DefaultColWidth: Integer read fDefaultColWidth write SetDefaultColWidth default DEF_DEFAULTCOLWIDTH;
    property DefaultHorzLineWidth: Integer read fDefaultHorzLineWidth write SetDefaultHorzLineWidth default DEF_DEFAULTHORZLINEWIDTH;
    property DefaultHeaderLineColor: TColor read fDefaultHeaderLineColor write SetDefaultHeaderLineColor default DEF_HEADERLINECOLOR;
    property DefaultLineColor: TColor read fDefaultLineColor write SetDefaultLineColor default DEF_LINECOLOR;
    property DefaultRowHeight: Integer read fDefaultRowHeight write SetDefaultRowHeight default DEF_DEFAULTROWHEIGHT;
    property DefaultVertLineWidth: Integer read fDefaultVertLineWidth write SetDefaultVertLineWidth default DEF_DEFAULTVERTLINEWIDTH;
    property EditCol: Integer read fEditCol;
    property EditRow: Integer read fEditRow;
    property HighlightString: string read fHighlightString write fHighlightString;
    property HotImages: TCustomImageList read fHotImages write SetHotImages;
    property Images: TCustomImageList read fImages write SetImages;
    property Row: Integer read fRow  write SetRow; // do not publish
    property RowCount: Integer read fRowCount write SetRowCount default DEF_ROWCOUNT;
    property RowHeights[aRow: Integer]: Integer read GetRowHeight write SetRowHeight;  // do not publish
    property Rows: TMxRows read GetRows write SetRows stored IsStoredRows;
    property ScrollBars: TScrollStyle read fScrollBars write SetScrollBars;
    property SelectMode: TSelectMode read fSelectMode write SetSelectMode;
    property TopHeaders: TMatrixHeaders read GetTopHeaders write SetTopHeaders;
    property LeftHeaders: TMatrixHeaders read GetLeftHeaders write SetLeftHeaders;
    property RightHeaders: TMatrixHeaders read GetRightHeaders write SetRightHeaders;
    property TopLeft: TPoint read fTopLeft; // do not publish
    property UseCache: Boolean read fUseCache write fUseCache;
    property VisibleRows: Integer read GetVisibleRows write SetVisibleRows;
    property VisibleColsRequested: Integer read fVisibleColsRequested write SetVisibleColsRequested;
    property VisibleRowsRequested: Integer read fVisibleRowsRequested write SetVisibleRowsRequested;
  { protected events }
    property OnGetActiveStyle: TGetActiveCellStyleEvent read fOnGetActiveStyle write SetOnGetActiveStyle;
    property OnCellClick: TCellClickEvent read fOnCellClick write fOnCellClick;
    property OnCellDblClick: TCellClickEvent read fOnCellDblClick write fOnCellDblClick;
    property OnCreateEditor: TCreateEditorEvent read fOnCreateEditor write fOnCreateEditor;
    property OnGetCellData: TGetCellDataEvent read fOnGetCellData write fOnGetCellData;
    property OnGetCellHint: TGetCellHintEvent read fOnGetCellHint write fOnGetCellHint;
    property OnGetCellPainter: TGetCellPainterClassEvent read fOnGetCellPainter write fOnGetCellPainter;
    property OnGetHeaderText: TGetHeaderTextEvent read fOnGetHeaderText write fOnGetHeaderText;
    property OnHoverCell: THoverCellEvent read fOnHoverCell write fOnHoverCell;
    property OnLeftHeaderClick: TLeftHeaderClickEvent read fOnLeftHeaderClick write fOnLeftHeaderClick;
    property OnTopHeaderClick: TTopHeaderClickEvent read fOnTopHeaderClick write fOnTopHeaderClick;
    property OnMeasureRow: TMeasureRowEvent read fOnMeasureRow write SetOnMeasureRow;
    property OnPaintBackGround: TPaintBackGroundEvent read fOnPaintBackGround write fOnPaintBackGround;
    property OnPaintCell: TPaintCellEvent read fOnPaintCell write fOnPaintCell;
    property OnPaintHeader: TPaintHeaderEvent read fOnPaintHeader write fOnPaintHeader;
    property OnDeselectCell: TCellEvent read fOnDeselectCell write fOnDeselectCell;
    property OnSelectCell: TCellEvent read fOnSelectCell write fOnSelectCell;
  public
    procedure aaa;
    procedure Zoom(aRatio: Extended);
    procedure Assign(Source: TPersistent); override;

    procedure SaveAsTemplate(const aFileName: string);
    procedure LoadFromTemplate(const aFileName: string);
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  { public overrides }
    procedure Invalidate; override;
    procedure SetBounds(aLeft, aTop, aWidth, aHeight: Integer); override;
    procedure SetFocus; override;
  { public methods }
    function CanAutoGenerateObjects: Boolean;
    function GetCellRect(aCol, aRow: integer): TRect;
    function GetColRect(aCol: Integer): TRect;
    function GetRowRect(aRow: Integer): TRect;
    procedure InvalidateCell(aCol, aRow: Integer; DirectUpdate: Boolean = False);
    procedure InvalidateHeader(aHeaders: TMatrixHeaders; aCol, aRow, aHeaderIndex: Integer;  const aRect: TRect; Down: Boolean);
    procedure InvalidateTopHeaders;
    procedure InvalidateCol(aCol: Integer; DirectUpdate: Boolean = False);
    procedure InvalidateRow(aRow: Integer; DirectUpdate: Boolean = False);
    procedure PaintLock;
    function PaintLocked: Boolean;
    procedure PaintUnlock;
    procedure ColumnToBitmap(aCol: Integer; aBitmap: TMxBitmap);
    function GetCellData(aCol, aRow: Integer): TCellData;

  { public procs tbv editors }
    procedure EditCancel;
    procedure EditEnd;
    procedure EditExit;
    procedure GetData(aCol, aRow: Integer; out Data: TCellData);
    procedure SetData(aCol, aRow: Integer; Data: TCellData);
    procedure DoPostMessage(var Message: TMessage);
    procedure SendKeyDown(var Key: Word; Shift: TShiftState);
    function IsScrollKey(Key: Word; Shift: TShiftState): Boolean; virtual;
    function InState(aState: TInternalState): Boolean;
    function InStates(aStates: TInternalStates): Boolean;
    function AllStates(aStates: TInternalStates): Boolean;
    procedure ForceAuto;
  published
    procedure SetTemplate(const aTemplate: TMxTemplate);
  end;

  TSuperMatrix = class(TBaseMatrix)
  private
  protected
  public
    property Col; // do not publish
    property ColRow; // do not publish
    property ColWidths; // do not publish
    property EditCol; // do not publish
    property EditRow; // do not publish
    property HighlightString;
    property Row; // do not publish
    property RowHeights; // do not publish
    property TabStop;
    property VisibleRows; // do not publish
//    property Columns;
  //  property Rows;
  published
  { inherited properties }
    property Align;
    property Anchors;
    property BevelInner;
    property BevelOuter;
    property BevelKind;
    property BevelWidth;
    property BorderWidth;
    property BorderStyle;
    property Color;
    property Ctl3D;
    property ParentColor;
    property PopupMenu;
    property ScrollBars;
  { matrix properties }
    property Options; // publish vooraan!
    property CellStyle;
    property CellOffsetX;
    property CellOffsetY;
    property ColCount;
    property Columns;
    property CustomPainting;
    property CustomPaintStages;
    property DefaultColWidth;
    property DefaultHorzLineWidth;
    property DefaultLineColor;
    property DefaultRowHeight;
    property DefaultVertLineWidth;
    property HotImages;
    property Images;
    property LeftHeaders;
    property RightHeaders;
    property RowCount;
    property Rows;
    property SelectMode;
    property TopHeaders;
    property VisibleColsRequested;
    property VisibleRowsRequested;
  { inherited events }
    property OnExit;
    property OnEnter;
    property OnKeyDown;
    property OnKeyPress;
    property OnMouseMove;
  { matrix events }
    property OnCellClick;
    property OnCellDblClick;
    property OnCreateEditor;
    property OnGetActiveStyle;
    property OnGetCellData;
    property OnGetCellHint;
    property OnGetCellPainter;
    property OnGetHeaderText;
    property OnHoverCell;
    property OnMeasureRow;
    property OnPaintBackGround;
    property OnPaintCell;
    property OnPaintHeader;
    property OnDeselectCell;
    property OnSelectCell;
    property OnLeftHeaderClick;
    property OnTopHeaderClick;
  end;

  TMxTemplate = class(TComponent)
  private
    fMatrix: TBaseMatrix;
    procedure Check;
    procedure SetMatrix(const Value: TBaseMatrix);
  protected
    property Matrix: TBaseMatrix read fMatrix;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SaveToStream(S: TStream);
    procedure SaveToFile(const aFileName: string);
    procedure LoadFromStream(S: TStream);
    procedure LoadFromFile(const aFileName: string);
  published
  end;

procedure CopyCanvas(Target: TCanvas; const TargetRect: TRect; Source: TCanvas; const SourceRect: TRect);

implementation

uses
  UMxEditors;


procedure FillR(aCanvas: TCanvas; R: TRect);
{-------------------------------------------------------------------------------
  Een normale fillrect die de laatste coordinaten meeneemt
-------------------------------------------------------------------------------}
begin
  Inc(R.Right);
  Inc(R.Bottom);
  aCanvas.FillRect(R);
end;

procedure CopyCanvas(Target: TCanvas; const TargetRect: TRect; Source: TCanvas; const SourceRect: TRect);
{ simpel kopieren }
begin
  with TargetRect do
    BitBlt(Target.Handle,
           Left, Top, Right - Left, Bottom - Top,
           Source.Handle,
           SourceRect.Left, SourceRect.Top,
           cmSrcCopy);
end;

procedure Error(const Msg: string);
begin
  raise EMatrixError.Create(Msg);
end;

procedure DrawHeader(const aCanvas: TCanvas; const aRect: TRect; Down: Boolean = False; Fill: Boolean = False; Flat: Boolean = False);
var
  R: TRect;
begin
  with aCanvas do
  begin
    R := aRect;
    if Fill then
      FillRect(R);
    if Flat then
      Exit;  
    { teken buttonlook header }
    begin
      SetBkMode(Handle, TRANSPARENT);
      if not Down then
      begin
        DrawEdge(Handle, R, BDR_RAISEDINNER, BF_TOPLEFT);
        DrawEdge(Handle, R, BDR_RAISEDINNER, BF_BOTTOMRIGHT);
      end
      else begin
        DrawEdge(Handle, R, BDR_SUNKENINNER, BF_TOPLEFT);
        DrawEdge(Handle, R, BDR_SUNKENINNER, BF_BOTTOMRIGHT);
      end;
    end;
  end
end;

function MakeAxisRange(A, B: Integer): TAxisRange;
begin
  Result.First := A;
  Result.Last := B;
end;

//procedure Design

var
  Initialized: Boolean;
  UserCount: Integer;
  MatrixBitmap: TMxBitmap; // gebruikt door PaintMatrix
  CellBitmap: TMxBitmap; // gebruikt door PaintCell
  HeaderBitmap: TMXBitmap; // gebruikt door PaintHeader;
  GlobalCellStyle: TCellStyle; // globale cellstyle voor defaults
  GlobalColors: TStateColors; // global colors voor defaults

  WinFlags: array[TMultiLineRange, TAlignment, TVertAlignment] of DWORD; // snelle conversie voor Windows.DrawTextEx

const
  AllCells: TRect = (Left: MinInt; Top: MinInt; Right: MaxInt; Bottom: MaxInt);
  AllAxis: TAxisRange = (First: MinInt; Last: MaxInt);
  AllWindow: TRect = (Left: MinInt; Top: MinInt; Right: MaxInt; Bottom: MaxInt);
  ErrorRect: TRect = (Left: -1; Top: -1; Right: -1; Bottom: -1);
  EmptyRect: TRect = (Left: 0; Top: 0; Right: 0; Bottom: 0);
  ErrorPoint: TPoint = (X: -1; Y: -1);

  {Areas
    maNone,
    maGrid,
    maTopLeft,
    maTop,
    maTopRight,
    maLeft,
    maRight,
    maBottomLeft,
    maBottom,
    maBottomRight}


  { default kleuren }
  DEF_TEXTCOLOR     = clWindowText;
  DEF_BACKCOLOR     = clWindow;
  DEF_TEXTFOCUSED   = clHighLightText;
  DEF_BACKFOCUSED   = $00C66300; //clHighLight; // $00C66300 mooi blauw
  DEF_TEXTCURRENT   = clWindowText;
  DEF_BACKCURRENT   = $00FFF0F0;//$00FFDCB9; // lichtblauw
  DEF_TEXTSELECTED  = clWindowText;
  DEF_BACKSELECTED  = clAqua;
  DEF_TEXTHYPERLINK = clBlue;
  DEF_TEXTMATCH     = clHighlightText;//Red;
  DEF_BACKMATCH     = clHighlight;
  DEF_DIFFBACKCOLOR = clWindow;//$00F0F0F0 lichtgrijs
  DEF_TEXTMATCH2    = clHighLight;//clNavy;
  DEF_BACKMATCH2    = clnone;//Red;
  DEF_HOVERBACK     = clYellow;


  DEF_COLORS: TStateColorArray = (
    DEF_TEXTCOLOR,             // 0
    DEF_BACKCOLOR,             // 1
    DEF_TEXTFOCUSED,           // 2
    DEF_BACKFOCUSED,           // 3
    DEF_TEXTCURRENT,           // 4
    DEF_BACKCURRENT,           // 5
    DEF_TEXTSELECTED,          // 6
    DEF_BACKSELECTED,          // 7
    DEF_TEXTHYPERLINK,         // 8
    DEF_TEXTMATCH,             // 9
    DEF_BACKMATCH,             // 10
    DEF_DIFFBACKCOLOR,         // 11
    DEF_TEXTMATCH2,            // 12
    DEF_BACKMATCH2,            // 13
    DEF_HOVERBACK,             // 14
    clNone                     // 15
  );

  EmptyTextParams : TDrawTextParams = (
    cbSize: SizeOf(TDrawTextParams);
    iTabLength: 0;
    iLeftMargin: 0;
    iRightMargin: 0;
    uiLengthDrawn: 0
  );

  { timer constanten voor windows timers }
  CURSOR_TIMER = 1; // timer voor het zetten van resize-cursor (voorkomt knipperende muiscursor)
  CLEARSEARCH_TIMER = 2; // timer voor legen incremental searchstring
  SCROLL_TIMER = 3; // timer voor scroll events

  CURSOR_TIMER_INTERVAL = 256;
  CLEARSEARCH_TIMER_INTERVAL = 600;


procedure InitWinFlags;
var
  Def: Integer;
begin
  Def := DT_NOPREFIX; {or DT_NOPREFIX or DT_WORDBREAK or DT_WORD_ELLIPSIS or DT_NOCLIP}

  { singleline }
  WinFlags[False, taLeftJustify, vaTop]     := Def or DT_LEFT or DT_TOP or DT_SINGLELINE;
  WinFlags[False, taLeftJustify, vaCenter]  := Def or DT_LEFT or DT_VCENTER or DT_SINGLELINE;
  WinFlags[False, taLeftJustify, vaBottom]  := Def or DT_LEFT or DT_BOTTOM or DT_SINGLELINE;

  WinFlags[False, taCenter, vaTop]          := Def or DT_CENTER or DT_TOP or DT_SINGLELINE;
  WinFlags[False, taCenter, vaCenter]       := Def or DT_CENTER or DT_VCENTER or DT_SINGLELINE;
  WinFlags[False, taCenter, vaBottom]       := Def or DT_CENTER or DT_BOTTOM or DT_SINGLELINE;

  WinFlags[False, taRightJustify, vaTop]    := Def or DT_RIGHT or DT_TOP or DT_SINGLELINE;
  WinFlags[False, taRightJustify, vaCenter] := Def or DT_RIGHT or DT_VCENTER or DT_SINGLELINE;
  WinFlags[False, taRightJustify, vaBottom] := Def or DT_RIGHT or DT_BOTTOM or DT_SINGLELINE;

  { multiline }
  WinFlags[True, taLeftJustify, vaTop]      := Def or DT_EDITCONTROL;
  WinFlags[True, taLeftJustify, vaCenter]   := Def or DT_EDITCONTROL;
  WinFlags[True, taLeftJustify, vaBottom]   := Def or DT_EDITCONTROL;

  WinFlags[True, taCenter, vaTop]           := Def or DT_EDITCONTROL;
  WinFlags[True, taCenter, vaCenter]        := Def or DT_EDITCONTROL;
  WinFlags[True, taCenter, vaBottom]        := Def or DT_EDITCONTROL;

  WinFlags[True, taRightJustify, vaTop]     := Def or DT_EDITCONTROL;
  WinFlags[True, taRightJustify, vaCenter]  := Def or DT_EDITCONTROL;
  WinFlags[True, taRightJustify, vaBottom]  := Def or DT_EDITCONTROL;
end;

function CalcWinFlags(MultiLine: Boolean; HorzAlign: TAlignment; VertAlign: TVertAlignment; WordWrap: Boolean): DWORD;
begin
  Result := WinFlags[MultiLine, HorzAlign, VertAlign];
  if WordWrap then
    Result := Result or DT_WORDBREAK;
end;

procedure FinalizeGlobals;
begin
  if not Initialized then Exit;
  // heel belangrijk, als dit in een module zit. waarom PRECIES weet ik nog niet
  Initialized := False;
end;

procedure InitializeGlobals;
begin
  if Initialized then Exit;
  Initialized := True;
  InitWinFlags;
end;

procedure UsesBitmaps;
var
  M: Cardinal;
begin
  if UserCount = 0 then
  begin
    MatrixBitmap := TMxBitmap.Create;
    CellBitmap := TMxBitmap.Create;
    HeaderBitmap := TMxBitmap.Create;

//    windlg([ord(matrixbitmap.pixelformat)]);
//    MatrixBitmap.PixelFormat := pf32Bit;
//    CellBitmap.PixelFormat := pf32Bit;
//    HeaderBitmap.PixelFormat := pf32Bit;

    GlobalCellStyle := TCellStyle.Create(nil, ofNone);
  end;
  Inc(UserCount);
end;

procedure ReleaseBitmaps;
begin
//  messageboxdlg('release bitmaps') u
  Dec(UserCount);
  if UserCount = 0 then
  begin
    MatrixBitmap.Free;
    CellBitmap.Free;
    HeaderBitmap.Free;
    GlobalCellStyle.Free;
  end;
end;

procedure BitmapResize(aBitmap: TMxBitmap; aWidth, aHeight: Integer);
begin
  with aBitmap do
  begin
    Width := Max(Width, aWidth);
    Height := Max(Height, aHeight);
  end;
end;

function RectsDoIntersect(const R1, R2: TRect): Boolean;
begin
  if (R1.Left > R2.Right) or (R1.Top > R2.Bottom) or
     (R1.Bottom < R2.Top) or (R1.Right < R2.Left)
  then
    Result := False
  else
    Result := True;
end;

function DoIntersectRect(var R: TRect; const R1, R2: TRect): Boolean;
begin
  if (R1.Left > R2.Right) or (R1.Top > R2.Bottom) or
     (R1.Bottom < R2.Top) or (R1.Right < R2.Left)
  then begin
    Result := False;
    R := ErrorRect;
  end
  else begin
    Result := True;
//    R.Left :=
  end;
end;

function RectFitsIn(const R1, R2: TRect): Boolean;
begin
  Result := (R1.Left >= R2.Left) and (R1.Top >= R2.Top) and
     (R1.Bottom <= R2.Bottom) and (R1.Right <= R2.Right)
end;

function PointInRect(X, Y: Integer; const R: TRect): Boolean;
begin
  Result := (X >= R.Left) and (X <= R.Right) and (Y >= R.Top) and (Y <= R.Bottom);
end;

function TBaseMatrix.GetBkColor(const CellStyle: TCellStyle; DrawState: TCellDrawState): TColor;
begin
  with CellStyle do
  begin
    { 1: matrix gefocused }
    if dsMatrixFocused in DrawState then
    begin
      { 1a: highlight }
      if [dsHighlightRow, dsHighlightCol] * DrawState <> [] then
      begin
        Result := CachedColors[MI_BACKCURRENT];
        Exit;
      end;
      { 1b: niet huidige cell }
      if not (dsFocused in DrawState) then
      begin
        Result := CachedColors[MI_BACKCOLOR];
        if dsHover in DrawState then
        begin
          Result := CachedColors[MI_HOVERBACK];
          Exit;
        end;
        if dsSelection in DrawState then
        begin
          Result := CachedColors[MI_BACKSELECTED];
        end;
        Exit;
      end;
      { 1c: huidige cell}
      if dsFocused in DrawState then
      begin
        Result := CachedColors[MI_BACKFOCUSED];
        Exit;
      end;
      Result := clWhite; // voor de zekerheid
    end
    { 2: matrix niet gefocused }
    else begin
      { designing }
      if csDesigning in ComponentState then
      begin
        Result := CachedColors[MI_BACKCOLOR];
        Exit;
      end;
      { 1a: highlight }
      if [dsHighlightRow, dsHighlightCol] * DrawState <> [] then
      begin
        Result := CachedColors[MI_BACKCURRENT];
        Exit;
      end;
      { 1b: niet huidige cell }
      if not (dsFocused in DrawState) then
      begin
        Result := CachedColors[MI_BACKCOLOR];
        Exit;
      end;
      { 1c: huidige cell}
      if dsFocused in DrawState then
      begin
        Result := CachedColors[MI_BACKCURRENT];
        Exit;
      end;
      Result := clWhite; // voor de zekerheid
    end;
  end;

end;

function TBaseMatrix.GetTxtColor(const CellStyle: TCellStyle; DrawState: TCellDrawState): TColor;
begin

  with CellStyle do
  begin
    { 1: matrix gefocused }
    if dsMatrixFocused in DrawState then
    begin
      { 1a: highlight }
      if [dsHighlightRow, dsHighlightCol] * DrawState <> [] then
      begin
        Result := CachedColors[MI_TEXTCURRENT];
        Exit;
      end;
      { 1b: niet huidige cell }
      if not (dsFocused in DrawState) then
      begin
        if dsHyperLink in DrawState then
          Result := CachedColors[MI_TEXTHYPERLINK]
        else
          Result := CachedColors[MI_TEXTCOLOR];
        Exit;
      end;
      { 1c: huidige cell}
      if dsFocused in DrawState then
      begin
        // hyperlink niet of nieuwe kleur maken
        Result := CachedColors[MI_TEXTFOCUSED];
        Exit;
      end;
      Result := clBlack; // voor de zekerheid
    end
    { 2: matrix niet gefocused }
    else begin
      { 1a: highlight }
      if [dsHighlightRow, dsHighlightCol] * DrawState <> [] then
      begin
        Result := CachedColors[MI_TEXTCURRENT];
        Exit;
      end;
      { 1b: niet huidige cell }
      if not (dsFocused in DrawState) then
      begin
        if dsHyperLink in DrawState then
          Result := CachedColors[MI_TEXTHYPERLINK]
        else
          Result := CachedColors[MI_TEXTCOLOR];
        Exit;
      end;
      { 1c: huidige cell}
      if dsFocused in DrawState then
      begin
        Result := CachedColors[MI_TEXTCURRENT];
        Exit;
      end;
      Result := clBlack; // voor de zekerheid
    end;
  end;

end;

type
  TRGBRec = packed record
    case Byte of
      0:
        (Value: TColor);
      1:
        (r: byte;
         g: byte;
         b: byte;
         u: byte);
  end;

function MixColors(const Color1, Color2: TColor): TColor;
begin
  Result := RGB(
    (integer(TRGBRec(Color1).r) + TRGBRec(Color2).r ) div 2,
    (integer(TRGBRec(Color1).g) + TRGBRec(Color2).g ) div 2,
    (integer(TRGBRec(Color1).b) + TRGBRec(Color2).b ) div 2
  );
end;

const
  DELTA_INTEGERS = 32;

procedure SetInteger(var aInts: TCachedIntegers; aIndex: integer; aValue: integer);
var
  L: integer;
begin
  L := Length(aInts);
  if L <= aIndex then
    SetLength(aInts, aIndex + DELTA_INTEGERS);
  aInts[aIndex] := aValue;
end;

function ExistInteger(const aInts: TCachedIntegers; aValue, aLast: integer): boolean;
var
  i: integer;
begin
  Assert(aLast < Length(aInts));
  for i := 0 to aLast do
    if aInts[i] = aValue then
    begin
      Result := True;
      Exit;
    end;
  Result := False;
end;

function FindInteger(const aInts: TCachedIntegers; aValue, aLast: integer): Integer;
var
  i: integer;
begin
  Assert(aLast < Length(aInts));
  for i := 0 to aLast do
    if aInts[i] = aValue then
    begin
      Result := i;
      Exit;
    end;
  Result := -1;
end;

procedure SetPoint(var aPoints: TCachedPoints; aIndex, aFirst, aLast: Integer);
var
  L: integer;
begin
  L := Length(aPoints);
  if L <= aIndex then
    SetLength(aPoints, aIndex + DELTA_INTEGERS);
  with aPoints[aIndex] do
  begin
    A := aFirst;
    B := aLast;
  end;
end;

{ TMatrixObject }

procedure TMatrixObject.Changed;
begin
  if Assigned(fOnChange) then
    fOnChange(Self);
end;

constructor TMatrixObject.Create(aMatrix: TBaseMatrix);
begin
  inherited Create;
  fMatrix := aMatrix;
end;

{ TStateColors }

procedure TStateColors.Assign(Source: TPersistent);
var
  St: TStateColors absolute Source;
  i: Integer;
begin
  { header en ownerflag moeten behouden blijven }
  if Source is TStateColors then
  begin
    fAssignedColorProps := St.fAssignedColorProps;//TStateColors(Source).fAssignedColorProps;
//    if St.fcolorarray[MI_BACKCOLOR] = clred then
  //    windlg('rood');
    for i := Low(TColorRange) to High(TColorRange) do
      fColorArray[i] := St.fColorArray[i];
    //BackColor := St.BackColor;
  end
  else inherited Assign(Source);
  //Changed;
end;

procedure TStateColors.Changed;
begin
  if Assigned(fOnChange) then fOnChange(Self);
end;

constructor TStateColors.Create(aOwner: TPersistent; aOwnerFlag: TOwnerFlag);
begin
  inherited Create(aOwner);
  if not (aOwner is TCellStyle) then raise Exception.Create('StateColors mogen alleen door TCellStyle gemaakt worden');
//  fIsHeader := aHeader;
  fOwnerFlag := aOwnerFlag;
  fColorArray := DEF_COLORS;
end;

function TStateColors.GetColorElement(const Index: Integer): TColor;
begin
  if Index in fAssignedColorProps then
    Result := fColorArray[Index]
  else
    Result := DefaultColorElement[Index];
end;

function TStateColors.GetDefaultColorElement(const Index: Integer): TColor;
{-------------------------------------------------------------------------------
  Haalt default kleur op.
  Als StateColors bij Column of Row hoort dan is de default kleur de kleur
  van de Matrix.
-------------------------------------------------------------------------------}
var
  M: TBaseMatrix;
begin
  Result := DEF_COLORS[Index];

  case fOwnerFlag of
    ofColumn, ofRow:
      begin
        M := TCellStyle(Owner).Matrix;
        if M <> nil then
          Result := M.CellStyle.Colors.GetColorElement(Index);
      end;
  end;

end;

function TStateColors.IsColorStored(const Index: Integer): Boolean;
begin
  Result := (Index in fAssignedColorProps) and (fColorArray[Index] <> DefaultColorElement[Index]);
end;

procedure TStateColors.SetColorElement(const Index: Integer; const Value: TColor);
begin
  if (Index in fAssignedColorProps) and (fColorArray[Index] = Value) then Exit;
  fColorArray[Index] := Value;
  Include(fAssignedColorProps, Index);
  Changed;
end;

{ TDrawMargin }

procedure TDrawMargin.SetBottom(const Value: Integer);
begin
  if fBottom = Value then Exit;
  fBottom := Value;
end;

procedure TDrawMargin.SetLeft(const Value: Integer);
begin
  if fLeft = Value then Exit;
  fLeft := Value;
end;

procedure TDrawMargin.SetRight(const Value: Integer);
begin
  if fRight = Value then Exit;
  fRight := Value;
end;

procedure TDrawMargin.SetTop(const Value: Integer);
begin
  if fTop = Value then Exit;
  fTop := Value;
end;

{ TCellStyle }

procedure TCellStyle.Assign(Source: TPersistent);
var
  CS: TCellStyle absolute Source;
begin
  if Source is TCellStyle then
  begin
    fHorzAlign := CS.fHorzAlign;
    fVertAlign := CS.fVertAlign;
    fMultiLine := CS.fMultiLine;
    fFlat := CS.Flat;
    fColors.Assign(CS.fColors);
    fFont.Assign(CS.fFont);
    fAssignedCellProps := CS.fAssignedCellProps;
  end
  else inherited Assign(Source);
  //SetCached(False);
  Changed;
end;

procedure TCellStyle.BeginUpdate;
begin
  Inc(fUpdateFlag);
end;

procedure TCellStyle.Changed;
begin
  fCached := False;
  if fUpdateFlag = 0 then
  begin
{    if fOwnerFlag = ofMatrix then
      if TCustomMatrix(Owner).UsingColumns then sad }
    if Assigned(fOnChange) then
      fOnChange(Self);
  end;
end;

procedure TCellStyle.ColorChanged(Sender: TObject);
begin
  Changed;
end;

constructor TCellStyle.Create(aMatrix: TBaseMatrix; aOwnerFlag: TOwnerFlag);
begin
  inherited Create(aMatrix);
  fFont := TFont.Create;
  fFont.OnChange := FontChanged;
  fColors := TStateColors.Create(Self, aOwnerFlag);
  fColors.OnChange := ColorChanged;
  fCache.Font  := TFont.Create;
  fBorderWidth := DEF_CELLBORDERWIDTH;
  fMultiLine   := DEF_MULTILINE;//false
  fFlat        := DEF_FLAT;
  fVertAlign   := DEF_VERTALIGN;
  fMargin.tLeft := 2;
  fMargin.tTop  := 2;
  fMargin.tRight := 2;
  fMargin.tBottom := 2;

  if aOwnerFlag = ofHeader then
  begin
    fColors.BackColor := clbtnface;
    fFlat := DEF_HEADERFLAT;
  end;
end;

destructor TCellStyle.Destroy;
begin
  fColors.Free;
  fFont.Free;
  fCache.Font.Free;
  inherited;
end;

procedure TCellStyle.EndUpdate;
begin
  if fUpdateFlag > 0 then
  begin
    Dec(fUpdateFlag);
    if fUpdateFlag = 0 then
      Changed;
  end;
end;

procedure TCellStyle.FontChanged(Sender: TObject);
begin
  Include(fAssignedCellProps, acp_Font);
  //if Font.Color <> Colors.TextColor then...
  Changed;
end;

function TCellStyle.GetBorderWidth: Integer;
begin
  if fCached then Result := fCache.BorderWidth
  else if acp_BorderWidth in fAssignedCellProps then Result := fBorderWidth
  else Result := DefaultBorderWidth;
end;

function TCellStyle.GetDefaultBorderWidth: Integer;
begin
  Result := DEF_CELLBORDERWIDTH;
  if fMatrix <> nil then
    case fOwnerFlag of
      ofColumn, ofRow: Result := fMatrix.CellStyle.BorderWidth;
    end;
end;

function TCellStyle.GetDefaultFlat: Boolean;
begin
  Result := DEF_FLAT;
  if fMatrix <> nil then
    case fOwnerFlag of
      ofColumn, ofRow: Result := fMatrix.CellStyle.Flat;
      ofHeader: Result := DEF_HEADERFLAT;
    end;
end;

function TCellStyle.GetDefaultFont: TFont;
begin
  if fMatrix <> nil then
  begin
    Result := fMatrix.CellStyle.fFont;
  end
  else begin
    Result := fFont;
    // geen font
  end;
end;

function TCellStyle.GetDefaultHorzAlign: TAlignment;
begin
  Result := DEF_HORZALIGN;
  if fMatrix <> nil then
    case fOwnerFlag of
      ofColumn, ofRow: Result := fMatrix.CellStyle.HorzAlign
    end;
end;

function TCellStyle.GetDefaultMultiLine: Boolean;
begin
  Result := DEF_MULTILINE;
  if fMatrix <> nil then
  case fOwnerFlag of
    ofColumn, ofRow: Result := fMatrix.CellStyle.MultiLine;
  end;
end;

function TCellStyle.GetDefaultVertAlign: TVertAlignment;
begin
  Result := DEF_VERTALIGN;
  if fMatrix <> nil then
  case fOwnerFlag of
    ofColumn, ofRow: Result := fMatrix.CellStyle.VertAlign
  end;
end;

function TCellStyle.GetDefaultWordWrap: Boolean;
begin
  Result := DEF_WORDWRAP;
  if fMatrix <> nil then
  case fOwnerFlag of
    ofColumn, ofRow: Result := fMatrix.CellStyle.WordWrap;
  end;
end;

function TCellStyle.GetDefaultGlyphPosition: TGlyphPosition;
begin
  Result := DEF_GLYPHPOSITION;
  if fMatrix <> nil then
  case fOwnerFlag of
    ofColumn, ofRow: Result := fMatrix.CellStyle.GlyphPosition;
  end;
end;

function TCellStyle.GetFlat: Boolean;
begin
  if fCached then Result := fCache.Flat
  else if acp_Flat in fAssignedCellProps then Result := fFlat
  else Result := DefaultFlat;
end;

function TCellStyle.GetFont: TFont;
var
  Save: TNotifyEvent;
begin
//  if fCached then Result := fCache.Font
//  else
  if not (acp_Font in fAssignedCellProps) and (fFont.Handle <> DefaultFont.Handle) then
  begin
    Save := fFont.OnChange;
    fFont.OnChange := nil;
    fFont.Assign(DefaultFont);
    fFont.OnChange := Save;
  end;
  Result := fFont;
end;

function TCellStyle.GetHorzAlign: TAlignment;
begin
  if fCached then Result := fCache.HorzAlign
  else if acp_HorzAlign in fAssignedCellProps then Result := fHorzAlign
  else Result := DefaultHorzAlign;
end;

function TCellStyle.GetMultiLine: Boolean;
begin
  if fCached then Result := fCache.MultiLine else
  if acp_MultiLine in fAssignedCellProps then Result := fMultiline
  else Result := DefaultMultiLine;
end;

function TCellStyle.GetVertAlign: TVertAlignment;
begin
  if fCached then Result := fCache.VertAlign else
  if acp_VertAlign in fAssignedCellProps then Result := fVertALign
  else Result := DefaultVertAlign;
end;

function TCellStyle.GetWordWrap: Boolean;
begin
  if fCached then Result := fCache.WordWrap else
  if acp_WordWrap in fAssignedCellProps then Result := fWordWrap
  else Result := DefaultWordWrap;
end;

function TCellStyle.GetGlyphPosition: TGlyphPosition;
begin
  if fCached then Result := fCache.GlyphPosition else
  if acp_GlyphPosition in fAssignedCellProps then Result := fGlyphPosition
  else Result := DefaultGlyphPosition;
end;

function TCellStyle.IsBorderWidthStored: Boolean;
begin
  Result := (acp_BorderWidth in fAssignedCellProps) and (fBorderWidth <> DefaultBorderWidth);
end;

function TCellStyle.IsFlatStored: Boolean;
begin
  Result := (acp_Flat in fAssignedCellProps) and (fFlat <> DefaultFlat);
end;

function TCellStyle.IsFontStored: Boolean;
begin
  Result := acp_Font in fAssignedCellProps;
end;

function TCellStyle.IsHorzAlignStored: Boolean;
begin
  Result := (acp_HorzAlign in fAssignedCellProps) and (fHorzAlign <> DefaultHorzAlign);
end;

function TCellStyle.IsMultiLineStored: Boolean;
begin
  Result := (acp_MultiLine in fAssignedCellProps) and (fMultiLine <> DefaultMultiLine);
end;

function TCellStyle.IsVertAlignStored: Boolean;
begin
  Result := (acp_VertAlign in fAssignedCellProps) and (fVertAlign <> DefaultVertAlign);
end;

function TCellStyle.IsWordWrapStored: Boolean;
begin
  Result := (acp_WordWrap in fAssignedCellProps) and (fWordWrap <> DefaultWordWrap);
end;

function TCellStyle.IsGlyphPositionStored: Boolean;
begin
  Result := (acp_GlyphPosition in fAssignedCellProps) and (fGlyphPosition <> DefaultGlyphPosition);
end;

procedure TCellStyle.SetBorderWidth(const Value: Integer);
begin
  if (fBorderWidth = Value) and (acp_BorderWidth in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_BorderWidth);
  fBorderWidth := Value;
  Changed;
end;

procedure TCellStyle.SetCached(DoCache: Boolean);
var
  i: integer;
begin
  if fCached = DoCache then Exit;

  {-------------------------------------------------------------------------------
    zet fcached tijdelijk uit om te voorkomen dat de property-getters
    bij het assignen van fcache NIET waarden uit de cache teruggeven.
  -------------------------------------------------------------------------------}
  fCached := False;

  if DoCache then
  begin
//    Log(['cache']);
    for i := Low(TColorRange) to High(TColorRange) do
      fCache.Colors[i] := fColors.GetColorElement(i);
    fCache.BorderWidth := BorderWidth;
    fCache.Flat        := Flat;
    fCache.Font.Assign(Font);
    fCache.HorzAlign   := HorzAlign;
    fCache.VertAlign   := VertAlign;
    fCache.MultiLine   := MultiLine;
    fCache.WordWrap    := WordWrap;
    fCache.GlyphPosition := GlyphPosition;
    fCache.Margin        := Margin; 
  end;

  fCached := DoCache;
end;

procedure TCellStyle.SetColors(const Value: TStateColors);
begin
  fColors.Assign(Value);
end;

{procedure TCellStyle.SetEdge(const Value: TEdge);
begin
  fEdge.Assign(Value);
end;}

procedure TCellStyle.SetFlat(const Value: Boolean);
begin
  if (fFlat = Value) and (acp_Flat in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_Flat);
  fFlat := Value;
  Changed;
end;

procedure TCellStyle.SetFont(const Value: TFont);
begin
  fFont.Assign(Value);
end;

procedure TCellStyle.SetHorzAlign(const Value: TAlignment);
begin
  if (fHorzAlign = Value) and (acp_HorzAlign in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_HorzAlign);
  fHorzAlign := Value;
  Changed;
end;

procedure TCellStyle.SetMultiLine(const Value: Boolean);
begin
  if (fMultiLine = Value) and (acp_MultiLine in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_MultiLine);
  fMultiLine := Value;
  Changed;
end;

procedure TCellStyle.SetVertAlign(const Value: TVertAlignment);
begin
  if (fVertAlign = Value) and (acp_VertAlign in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_VertAlign);
  fVertAlign := Value;
  Changed;
end;

procedure TCellStyle.SetWordWrap(const Value: Boolean);
begin
  if (fWordWrap = Value) and (acp_WordWrap in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_WordWrap);
  fWordWrap := Value;
  Changed;
end;

procedure TCellStyle.SetGlyphPosition(const Value: TGlyphPosition);
begin
  if (fGlyphPosition = Value) and (acp_GlyphPosition in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_GlyphPosition);
  fGlyphPosition := Value;
  Changed;
end;

{ TMatrixOptions }

procedure TMatrixOptions.SetAutoSizeOptions(const Value: TMXAutoSizeOptions);
begin
  if fAutoSizeOptions = Value then Exit;
  DeletedAutoSizeOptions := DeletedAutoSizeOptions + fAutoSizeOptions - Value;
  InsertedAutoSizeOptions := InsertedAutoSizeOptions + Value - fAutoSizeOptions;
  ChangedAutoSizeOptions :=  DeletedAutoSizeOptions + InsertedAutoSizeOptions;
  fAutoSizeOptions := Value;
  Changed;
end;

procedure TMatrixOptions.SetEditOptions(const Value: TMXEditOptions);
begin
  if fEditOptions = Value then Exit;
  DeletedEditOptions := DeletedEditOptions + fEditOptions - Value;
  InsertedEditOptions := InsertedEditOptions + Value - fEditOptions;
  ChangedEditOptions :=  DeletedEditOptions + InsertedEditOptions;
  fEditOptions := Value;
  Changed;
end;

procedure TMatrixOptions.SetPaintOptions(const Value: TMXPaintOptions);
begin
  if fPaintOptions = Value then Exit;
  DeletedPaintOptions := DeletedPaintOptions + fPaintOptions - Value;
  InsertedPaintOptions := InsertedPaintOptions + Value - fPaintOptions;
  ChangedPaintOptions :=  DeletedPaintOptions + InsertedPaintOptions;
  fPaintOptions := Value;
  Changed;
end;

procedure TMatrixOptions.SetUseOptions(const Value: TMXUseOptions);
begin
  if fUseOptions = Value then Exit;
  DeletedUseOptions := DeletedUseOptions + fUseOptions - Value;
  InsertedUseOptions := InsertedUseOptions + Value - fUseOptions;
  ChangedUseOptions :=  DeletedUseOptions + InsertedUseOptions;
  fUseOptions := Value;
  Changed;
end;

procedure TMatrixOptions.SetNavigateOptions(const Value: TMXNavigateOptions);
begin
  if fNavigateOptions = Value then Exit;
  fNavigateOptions := Value;
end;

procedure TMatrixOptions.Assign(Source: TPersistent);
var
  Opt: TMatrixOptions absolute Source;
begin
  if Source is TMatrixOptions then
  begin
    // to do!!!
    fAutoSizeOptions       := Opt.fAutoSizeOptions;
    fUseOptions            := Opt.fUseOptions;
    fPaintOptions          := Opt.fPaintOptions;
    fSizingOptions         := Opt.fSizingOptions;
    fNavigateOptions       := Opt.fNavigateOptions;
    fEditOptions           := Opt.fEditOptions;
    fCreateOnDemandOptions := Opt.fCreateOnDemandOptions;
//    Changed;
(*    BeginUpdate;
    try
      AutoSizeOptions       := Opt.AutoSizeOptions;
      UseOptions            := Opt.UseOptions;
      PaintOptions          := Opt.PaintOptions;
      SizingOptions         := Opt.SizingOptions;
      NavigateOptions       := Opt.NavigateOptions;
      EditOptions           := Opt.EditOptions;
      CreateOnDemandOptions := Opt.CreateOnDemandOptions;
    finally
      EndUpdate;
    end; *)
  end
  else inherited;
end;

procedure TMatrixOptions.BeginUpdate;
begin
  if fUpdateCount = 0 then
    ClearRecs;
  Inc(fUpdateCount);
end;

procedure TMatrixOptions.Changed;
begin
  if fUpdateCount = 0 then
  try
    inherited Changed;
  finally
    ClearRecs;
  end;
end;

procedure TMatrixOptions.ClearRecs;
begin
  FillChar(fOptionsDeleted, SizeOf(fOptionsDeleted), 0);
  FillChar(fOptionsInserted, SizeOf(fOptionsInserted), 0);
  FillChar(fOptionsChanged, SizeOf(fOptionsChanged), 0);
end;

constructor TMatrixOptions.Create(aMatrix: TBaseMatrix);
begin
  inherited;
  fAutoSizeOptions       := DEF_AUTOSIZEOPTIONS;
  fPaintOptions          := DEF_PAINTOPTIONS;
  fSizingOptions         := DEF_SIZINGOPTIONS;
  fCreateOnDemandOptions := DEF_CREATEONDEMANDOPTIONS;
end;

procedure TMatrixOptions.EndUpdate;
begin
  if fUpdateCount > 0 then
  begin
    Dec(fUpdateCount);
    if fUpdateCount = 0 then
      Changed;
  end;
end;

function TMatrixOptions.GetAutoAdjustLastColumn: Boolean;
begin
  Result := aoAutoAdjustLastColumn in fAutoSizeOptions;
end;

function TMatrixOptions.GetAutoAdjustLastRow: Boolean;
begin
  Result := aoAutoAdjustLastRow in fAutoSizeOptions;
end;

function TMatrixOptions.GetAutoHeight: Boolean;
begin
  Result := aoAutoHeight in fAutoSizeOptions;
end;

function TMatrixOptions.GetAutoWidth: Boolean;
begin
  Result := aoAutoWidth in fAutoSizeOptions;
end;

function TMatrixOptions.GetCalculatedRowHeights: Boolean;
begin
  Result := poCalculatedRowHeights in fPaintOptions;
end;

function TMatrixOptions.GetCanEdit: Boolean;
begin
  Result := eoCanEdit in fEditOptions;
end;

function TMatrixOptions.GetHideLastVertLine: Boolean;
begin
  Result := poHideLastVertLine in fPaintOptions;
end;

function TMatrixOptions.GetTransparentHorzlines: Boolean;
begin
  Result := poTransparentHorzLines in fPaintOptions;
end;

function TMatrixOptions.GetTransparentLines: Boolean;
begin
  Result := TransparentVertLines and TransparentHorzLines;
end;

function TMatrixOptions.GetTransparentVertlines: Boolean;
begin
  Result := poTransparentVertLines in fPaintOptions;
end;

procedure TMatrixOptions.SetAutoAdjustLastColumn(const Value: Boolean);
begin
  case Value of
    False : AutoSizeOptions := AutoSizeOptions - [aoAutoAdjustLastColumn];
    True  : AutoSizeOptions := AutoSizeOptions + [aoAutoAdjustLastColumn];
  end;
end;

procedure TMatrixOptions.SetAutoAdjustLastRow(const Value: Boolean);
begin
  case Value of
    False : AutoSizeOptions := AutoSizeOptions - [aoAutoAdjustLastRow];
    True  : AutoSizeOptions := AutoSizeOptions + [aoAutoAdjustLastRow];
  end;
end;

procedure TMatrixOptions.SetSizingOptions(const Value: TMXSizingOptions);
begin
  if fSizingOptions = Value then Exit;
  fSizingOptions := Value;
end;


procedure TMatrixOptions.SetAutoHeight(const Value: Boolean);
begin
  AutoSizeOptions := AutoSizeOptions + [aoAutoHeight];
end;

procedure TMatrixOptions.SetAutoWidth(const Value: Boolean);
begin
  case Value of
    False : AutoSizeOptions := AutoSizeOptions - [aoAutoWidth];
    True  : AutoSizeOptions := AutoSizeOptions + [aoAutoWidth];
  end;
end;

procedure TMatrixOptions.SetCalculatedRowHeights(const Value: Boolean);
begin
  case Value of
    False : PaintOptions := PaintOptions - [poCalculatedRowHeights];
    True  : PaintOptions := PaintOptions + [poCalculatedRowHeights];
  end;
end;

procedure TMatrixOptions.SetCanEdit(const Value: Boolean);
begin
  case Value of
    False : EditOptions := EditOptions - [eoCanEdit];
    True  : EditOptions := EditOptions + [eoCanEdit];
  end;
end;

procedure TMatrixOptions.SetHideLastVertLine(const Value: Boolean);
begin
  case Value of
    False : PaintOptions := PaintOptions - [poHideLastVertLine];
    True  : PaintOptions := PaintOptions + [poHideLastVertLine];
  end;
end;

procedure TMatrixOptions.SetTransparentHorzlines(const Value: Boolean);
begin
  case Value of
    False : PaintOptions := PaintOptions - [poTransparentHorzLines];
    True  : PaintOptions := PaintOptions + [poTransparentHorzLines];
  end;
end;

procedure TMatrixOptions.SetTransparentLines(const Value: Boolean);
begin
  SetTransparentHorzLines(Value);
  SetTransparentVertLines(Value);
end;

procedure TMatrixOptions.SetTransparentVertlines(const Value: Boolean);
begin
  case Value of
    False : PaintOptions := PaintOptions - [poTransparentVertLines];
    True  : PaintOptions := PaintOptions + [poTransparentVertLines];
  end;
end;

function TMatrixOptions.GetUseColumns: Boolean;
begin
  Result := uoColumns in fUseOptions;
end;

procedure TMatrixOptions.SetUseColumns(const Value: Boolean);
begin
  case Value of
    False : UseOptions := UseOptions - [uoColumns];
    True  : UseOptions := UseOptions + [uoColumns];
  end;
end;

function TMatrixOptions.GetUseRows: Boolean;
begin
  Result := uoRows in fUseOptions;
end;

procedure TMatrixOptions.SetUseRows(const Value: Boolean);
begin
  case Value of
    False : UseOptions := UseOptions - [uoRows];
    True  : UseOptions := UseOptions + [uoRows];
  end;
end;

function TMatrixOptions.GetUseTopHeaders: Boolean;
begin
  Result := uoTopHeaders in fUseOptions;
end;

procedure TMatrixOptions.SetUseTopHeaders(const Value: Boolean);
begin
  case Value of
    False : UseOptions := UseOptions - [uoTopHeaders];
    True  : UseOptions := UseOptions + [uoTopHeaders];
  end;
end;

function TMatrixOptions.GetUseLeftHeaders: Boolean;
begin
  Result := uoLeftHeaders in fUseOptions;
end;

procedure TMatrixOptions.SetUseLeftHeaders(const Value: Boolean);
begin
  case Value of
    False : UseOptions := UseOptions - [uoLeftHeaders];
    True  : UseOptions := UseOptions + [uoLeftHeaders];
  end;
end;

function TMatrixOptions.GetUseBottomHeaders: Boolean;
begin
  Result := uoBottomHeaders in fUseOptions;
end;

function TMatrixOptions.GetUseRightHeaders: Boolean;
begin
  Result := uoRightHeaders in fUseOptions;
end;

procedure TMatrixOptions.SetUseBottomHeaders(const Value: Boolean);
begin
  case Value of
    False : UseOptions := UseOptions - [uoBottomHeaders];
    True  : UseOptions := UseOptions + [uoBottomHeaders];
  end;
end;

procedure TMatrixOptions.SetUseRightHeaders(const Value: Boolean);
begin
  case Value of
    False : UseOptions := UseOptions - [uoRightHeaders];
    True  : UseOptions := UseOptions + [uoRightHeaders];
  end;
end;

function TMatrixOptions.Updating: Boolean;
begin
  Result := fUpdateCount > 0;
end;

function TMatrixOptions.GetShowLines: Boolean;
begin
  Result := (poHorzLines in fPaintOptions) or (poVertLines in fPaintOptions);
end;

procedure TMatrixOptions.SetShowLines(const Value: Boolean);
begin
  case Value of
    False: PaintOptions := PaintOptions - [poHorzLines, poVertLines];
    True: PaintOptions := PaintOptions + [poHorzLines, poVertLines];
  end;
end;

function TMatrixOptions.GetShowHeaderLines: Boolean;
begin
  Result := (poHeaderHorzLines in fPaintOptions) or (poHeaderVertLines in fPaintOptions);
end;

procedure TMatrixOptions.SetShowHeaderLines(const Value: Boolean);
begin
  case Value of
    False: PaintOptions := PaintOptions - [poHeaderHorzLines, poHeaderVertLines];
    True: PaintOptions := PaintOptions + [poHeaderHorzLines, poHeaderVertLines];
  end;
end;

function TMatrixOptions.GetKeepColumnsOnDestroy: Boolean;
begin
  Result := moKeepColumnsOnDestroy in fMiscOptions;
end;

procedure TMatrixOptions.SetMiscOptions(const Value: TMXMiscOptions);
begin
  fMiscOptions := Value;
end;

{ TMxAxisItem }

procedure TMxAxisItem.Assign(Source: TPersistent);
var
  A: TMxAxisItem absolute Source;
begin
  if Source is TMxAxisItem then
  begin
    StyleAssigned := A.StyleAssigned;
    Tag        := A.Tag;
    Style      := A.Style;
    Size       := A.Size;
    fIndent    := A.fIndent;
    LineWidth  := A.LineWidth;
    Flags      := A.Flags;
  end else inherited Assign(Source);
end;

constructor TMxAxisItem.Create(Collection: TCollection);
begin
  inherited;
end;

destructor TMxAxisItem.Destroy;
begin
  fStyle.Free;
  inherited;
end;

class function TMxAxisItem.GetAxisOwnerFlag: TOwnerFlag;
begin
  Result := ofNone;
end;

function TMxAxisItem.GetMatrix: TBaseMatrix;
begin
  Result := TMxAxisCollection(Collection).fMatrix;
end;

function TMxAxisItem.GetObject: TObject;
begin
  Result := TObject(fTag);
end;

function TMxAxisItem.GetStyle: TCellStyle;
var
  M: TBaseMatrix;
begin
  Result := fStyle;
  if Result = nil then
  begin
    M := Matrix;
    if M = nil then
      Exit;
    // runtime only create on demand
    if (M.CanAutoGenerateObjects)
    and (cdCellStyle in M.Options.CreateOnDemandOptions) then
    begin
      StyleAssigned := True;
      Result := fStyle;
    end;
  end
end;

function TMxAxisItem.GetStyleAssigned: Boolean;
begin
  Result := Assigned(fStyle);
end;

function TMxAxisItem.GetVisible: Boolean;
begin
  Result := not (afHidden in fFlags);
end;

procedure TMxAxisItem.IndexChanged(OldIndex, NewIndex: Integer);
begin
  TMxAxisCollection(Collection).ItemIndexChanged(Self, OldIndex, NewIndex);
end;

function TMxAxisItem.IsStoredStyle: Boolean;
begin
  Result := Assigned(fStyle);
end;

procedure TMxAxisItem.SetFlags(const Value: TAxisFlags);
begin
  fFlags := Value;
end;

procedure TMxAxisItem.SetIndex(Value: Integer);
var
  OldIndex, NewIndex: Integer;
begin
  OldIndex := Index;
  inherited SetIndex(Value);
  NewIndex := Index;
  if Index <> OldIndex then
    IndexChanged(OldIndex, NewIndex);
end;

procedure TMxAxisItem.SetLineWidth(const Value: ShortInt);
begin
  if fLineWidth = Value then Exit;
  fLineWidth := Value;
  VisualChanged(True);
end;

procedure TMxAxisItem.SetObject(const Value: TObject);
begin
  fTag := Integer(Value);  
end;

procedure TMxAxisItem.SetSize(const Value: SmallInt);
begin
  if fSize = Value then Exit;
  fSize := Value;
  VisualChanged(True);
end;

procedure TMxAxisItem.SetStyle(const Value: TCellStyle);
begin
  if fStyle = nil then
    Exit;
  if Value = nil then
    Exit;  
  fStyle.Assign(Value);
end;

procedure TMxAxisItem.SetStyleAssigned(const Value: Boolean);
begin
  case Value of
    False: FreeAndNil(fStyle);
    True:
      begin
        if fStyle = nil then
        begin
          fStyle := TCellStyle.Create(Matrix, GetAxisOwnerFlag);
          fStyle.OnChange := StyleChanged;
          VisualChanged(False);
        end;
      end;
  end;
end;

procedure TMxAxisItem.SetVisible(const Value: Boolean);
begin
 if Value = GetVisible then Exit;
 case Value of
   False: Include(fFlags, afHidden);
   True: Exclude(fFlags, afHidden);
 end;
 if Matrix <> nil then
   Matrix.Invalidate;
end;

procedure TMxAxisItem.StyleChanged(Sender: TObject);
begin
  VisualChanged(False);
end;

{ TMxAxisCollection}

function TMxAxisCollection.AutoGenerated: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to Count - 1 do
    if not (afAutoGenerated in TMxAxisItem(Items[i]).fFlags) then
      Exit;
  Result := True;
end;

class function TMxAxisCollection.ClearAutoGeneratedOnAdd: Boolean;
begin
  Result := True;
end;

constructor TMxAxisCollection.Create(aOwner: TPersistent; aColumnClass: TMxAxisItemClass);
begin
  inherited Create(aOwner, aColumnClass);
  if aOwner is TBaseMatrix then
    fMatrix := TBaseMatrix(aOwner);
end;

function TMxAxisCollection.GetCount: Integer;
begin
  Result := inherited Count;
end;

function TMxAxisCollection.InternalAdd: TMxAxisItem;
begin
  Result := TMxAxisItem(inherited Add);
  Include(Result.fFlags, afAutoGenerated);// := AF_AUTOGENERATED;
end;

function TMxAxisCollection.InternalInsert(Index: Integer): TMxAxisItem;
begin
  Result := TMxAxisItem(inherited Insert(Index));
  Include(Result.fFlags, afAutoGenerated);// := AF_AUTOGENERATED;
end;

procedure TMxAxisCollection.ItemIndexChanged(Item: TMxAxisItem; OldIndex, NewIndex: Integer);
begin
  //
end;

procedure TMxAxisCollection.SetCount(const Value: Integer);
var
  i, Delta: Integer;
begin
  Delta := Value - Count;
  if Delta > 0  then
    for i := 1 to Delta do InternalAdd
  else if Delta < 0 then
    while Count > Value do
      Delete(Count - 1);
end;

{ TMxColumn }

procedure TMxColumn.Assign(Source: TPersistent);
var
  C: TMxColumn absolute Source;
begin
  if Source is TMxColumn then
  begin
    inherited Assign(Source); // TMxAxisItem.Assign
    Title := C.Title;
  end else inherited Assign(Source);
end;

constructor TMxColumn.Create(Collection: TCollection);
var
  M: TBaseMatrix;
begin
  inherited;
  M := Matrix;
  if M <> nil then
  begin
    fSize := M.DefaultColWidth;
    fLineWidth := M.DefaultVertLineWidth;
  end
  else begin
    fSize := DEF_DEFAULTCOLWIDTH;
    fLineWidth := DEF_DEFAULTVERTLINEWIDTH;
  end;
end;


class function TMxColumn.GetAxisOwnerFlag: TOwnerFlag;
begin
  Result := ofColumn;
end;

procedure TMxColumn.SetTitle(const Value: string);
begin
  if fTitle = Value then Exit;
  fTitle := Value;
  VisualChanged(True); //aiaiai
end;

procedure TMxColumn.VisualChanged(SomeSizeChanged: Boolean);
begin
  if Matrix <> nil then
    Matrix.ColumnVisualChanged(Self, SomeSizeChanged);
end;

{ TMxColumns }

function TMxColumns.Add: TMxColumn;
{-------------------------------------------------------------------------------
  als alles autogenerated dan verwijder deze
-------------------------------------------------------------------------------}
begin
  if ClearAutoGeneratedOnAdd then
    if AutoGenerated then
      Clear;
  Result := TMxColumn(inherited Add);
end;

constructor TMxColumns.Create(aOwner: TPersistent; aColumnClass: TMxColumnClass);
begin
  inherited Create(aOwner, aColumnClass);
end;

function TMxColumns.GetItem(Index: Integer): TMxColumn;
begin
  Result := TMxColumn(inherited GetItem(Index))
end;

procedure TMxColumns.ItemIndexChanged(Item: TMxAxisItem; OldIndex, NewIndex: Integer);
begin
  if fMatrix <> nil then
    fMatrix.ColumnIndexChanged(Item as TMxColumn, OldIndex, NewIndex);
end;

function TMxColumns.Insert(Index: Integer): TMxColumn;
begin
  Result := TMxColumn(inherited Insert(Index));
end;

procedure TMxColumns.Notify(Item: TCollectionItem; Action: TCollectionNotification);
begin
  if fMatrix <> nil then
    case Action of
      cnAdded    : fMatrix.ColumnAdded(Item as TMxColumn);
      cnDeleting, cnExtracting : fMatrix.ColumnDeleting(Item as TMxColumn);
    end;
end;

procedure TMxColumns.SetItem(Index: Integer; const Value: TMxColumn);
begin
  inherited SetItem(Index, Value)
end;

procedure TMxColumns.Update(Item: TCollectionItem);
begin
  if fMatrix <> nil then
    fMatrix.Invalidate;
end;

destructor TMxColumns.Destroy;
begin
  if Assigned(fMatrix)
  and not (csDestroying in fMatrix.ComponentState)
  and not fMatrix.Instate(isUpdatingColumns) then
    raise Exception.Create('columns destroy niet mogelijk, gebruik Matrix.UseOptions');
  inherited Destroy;
end;

{ TMxRow }

constructor TMxRow.Create(Collection: TCollection);
var
  M: TBaseMatrix;
begin
  inherited;
  M := Matrix;
  if M <> nil then
  begin
    fSize := M.DefaultRowHeight;
    fLineWidth := M.DefaultHorzLineWidth;
  end
  else begin
    fSize := DEF_DEFAULTROWHEIGHT;
    fLineWidth := DEF_DEFAULTHORZLINEWIDTH;
  end;
end;

class function TMxRow.GetAxisOwnerFlag: TOwnerFlag;
begin
  Result := ofRow;
end;

procedure TMxRow.VisualChanged(SomeSizeChanged: Boolean);
begin
  if Matrix <> nil then
    Matrix.RowVisualChanged(Self, SomeSizeChanged);
end;

{ TMxRows }

function TMxRows.Add: TMxRow;
{-------------------------------------------------------------------------------
  als alles autogenerated dan verwijder deze
-------------------------------------------------------------------------------}
begin
  if ClearAutoGeneratedOnAdd then
    if AutoGenerated then
      Clear;
  Result := TMxRow(inherited Add);
end;

constructor TMxRows.Create(aOwner: TPersistent; aRowClass: TMxRowClass);
begin
  inherited Create(aOwner, aRowClass);
end;


function TMxRows.GetItem(Index: Integer): TMxRow;
begin
  Result := TMxRow(inherited GetItem(Index))
end;

function TMxRows.Insert(Index: Integer): TMxRow;
begin
  Result := TMxRow(inherited Insert(Index));
end;

procedure TMxRows.Notify(Item: TCollectionItem; Action: TCollectionNotification);
begin
  if fMatrix <> nil then
    case Action of
      cnAdded    : fMatrix.RowAdded(TMxRow(Item));
      cnDeleting : fMatrix.RowDeleting(TMxRow(Item));
    end;
end;

procedure TMxRows.SetItem(Index: Integer; const Value: TMxRow);
begin
  inherited SetItem(Index, Value)
end;

procedure TMxRows.Update(Item: TCollectionItem);
begin
  inherited;
  if fMatrix <> nil then
    fMatrix.Invalidate;
end;

{ TCellParams }

procedure TCellParams.CalcImageRect(ImageWidth, ImageHeight: Integer);
{-------------------------------------------------------------------------------
  Bereken positie cpImageRect en pas cpContentsRect aan.
  NB: cpImageRect is al geinitialiseerd op leeg.
-------------------------------------------------------------------------------}
var
  N: Integer;
begin

  case cpActiveStyle.CachedGlyphPosition of
    gpLeft:
      begin
        cpImageRect.Left := cpContentsRect.Left;
        cpImageRect.Right := cpImageRect.Left + ImageWidth;
        cpImageRect.Bottom := ImageHeight;
        cpContentsRect.Left := cpImageRect.Right + 2;
        N := RectHeight(cpCanvasRect) - RectHeight(cpImageRect);
        Inc(cpImageRect.Top, N div 2);
        //Log([n, cpimagerect.top]);
      end;
    gpRight:
      begin
        cpImageRect.Left := cpCanvasRect.Right - ImageWidth;
        cpImageRect.Bottom := ImageHeight;
        cpContentsRect.Right := cpImageRect.Left - 2;
        N := RectHeight(cpCanvasRect) - RectHeight(cpImageRect);
        Inc(cpImageRect.Top, N div 2);
      end;
    gpTop:
      begin
        //cpCanvasRect
        cpImageRect.Top := 0;
        cpImageRect.Left := (RectWidth(cpCanvasRect) - ImageWidth) div 2;
        cpImageRect.Bottom := ImageHeight;
        cpContentsRect.Top := cpImageRect.Bottom + 2;
//        N := RectWidth(cpCanvasRect) - RectWidth(cpImageRect);
  //      Inc(cpImageRect.Left, N div 2);
      end;
    gpBottom:
      begin

      end;
    gpCenter:
      begin
        cpImageRect.Right := ImageWidth;
        cpImageRect.Bottom := ImageHeight;
        N := RectHeight(cpCanvasRect) - RectHeight(cpImageRect);
        Inc(cpImageRect.Top, N div 2);
        N := RectWidth(cpCanvasRect) - RectWidth(cpImageRect);
        Inc(cpImageRect.Left, N div 2);
        //log([n]);
        cpContentsRect := ErrorRect;
      end;
  end;
end;

procedure TCellParams.SetImageIndex(const Value: Integer);
begin
  if cpStage <> pcsImageIndex then
    raise Exception.Create('Setting image index not allowed');
  cpImageIndex := Value;
end;

procedure TCellParams.SetImageList(const Value: TCustomImageList);
begin
  if cpStage <> pcsImageIndex then
    raise Exception.Create('Setting imagelist not allowed');
  cpImageList := Value;
end;

{ THeaderParams }

procedure THeaderParams.CalcImageRect(ImageWidth, ImageHeight: Integer);
{-------------------------------------------------------------------------------
  Bereken positie cpImageRect en pas cpContentsRect aan.
  NB: cpImageRect is al geinitialiseerd op leeg.
-------------------------------------------------------------------------------}
var
  N: Integer;
begin

  case hpCellStyle.CachedGlyphPosition of
    gpLeft:
      begin
        hpImageRect.Left := hpContentsRect.Left;
        hpImageRect.Right := hpImageRect.Left + ImageWidth;
        hpImageRect.Bottom := ImageHeight;
        hpContentsRect.Left := hpImageRect.Right + 2;
        N := RectHeight(hpCanvasRect) - RectHeight(hpImageRect);
        Inc(hpImageRect.Top, N div 2);
        //Log([n, cpimagerect.top]);
      end;
    gpRight:
      begin
        hpImageRect.Left := hpCanvasRect.Right - ImageWidth;
        hpImageRect.Bottom := ImageHeight;
        hpContentsRect.Right := hpImageRect.Left - 2;
        N := RectHeight(hpCanvasRect) - RectHeight(hpImageRect);
        Inc(hpImageRect.Top, N div 2);
      end;
    gpTop:
      begin
        //cpCanvasRect
        hpImageRect.Top := 0;
        hpImageRect.Left := (RectWidth(hpCanvasRect) - ImageWidth) div 2;
        hpImageRect.Bottom := ImageHeight;
        hpContentsRect.Top := hpImageRect.Bottom + 2;
//        N := RectWidth(cpCanvasRect) - RectWidth(cpImageRect);
  //      Inc(cpImageRect.Left, N div 2);
      end;
    gpBottom:
      begin

      end;
    gpCenter:
      begin
        hpImageRect.Right := ImageWidth;
        hpImageRect.Bottom := ImageHeight;
        N := RectHeight(hpCanvasRect) - RectHeight(hpImageRect);
        Inc(hpImageRect.Top, N div 2);
        N := RectWidth(hpCanvasRect) - RectWidth(hpImageRect);
        Inc(hpImageRect.Left, N div 2);
        //log([n]);
        hpContentsRect := ErrorRect;
      end;
  end;
end;

{ TGridCache }

procedure TGridCache.CacheHeight(aRow, aHeight: Integer);
begin
  InternalUpdate;
  fHeights[aRow] := aHeight;
end;

constructor TGridCache.Create(aMatrix: TBaseMatrix);
begin
  inherited;
  fHeights := TIntegerList.Create('GridCacheHeights');
  fWidths := TIntegerList.Create('GridCacheWidths');
end;

destructor TGridCache.Destroy;
begin
  fWidths.Free;
  fHeights.Free;
  inherited;
end;

procedure TGridCache.InternalUpdate;
begin
  if fHeights.Capacity < fMatrix.fRowCount then
    fHeights.SetCapacityAndCount(fMatrix.fRowCount, -1);
  if fWidths.Capacity < fMatrix.fColCount then
    fWidths.SetCapacityAndCount(fMatrix.fColCount, -1);
end;

procedure TGridCache.InvalidateHeight(aRow: Integer);
begin
  InternalUpdate;
  fHeights[aRow] := INVALID_CACHE_SIZE;
end;

procedure TGridCache.InvalidateHeights;
begin
  fValidHeights := False;
end;

function TGridCache.ProbeHeight(aRow: Integer): Integer;
begin
  Result := INVALID_CACHE_SIZE;
  if fValidHeights then
    if fHeights.Count > aRow then
      Result := fHeights[aRow];
end;

procedure TGridCache.Update(const PgInfo: TPageInfo);
var
  x, y: Integer;
begin
  InternalUpdate;
  with PgInfo do
  begin
    with Horz do
    begin
      if Valid then
        for x := FirstVisible to LastVisible do
          fWidths[x] := Sizes[x].B - Sizes[x].A
      else
        for x := FirstVisible to LastVisible do
          fWidths[x] := INVALID_CACHE_SIZE;
    end;
    with Vert do
    begin
      if Valid then
        for y := FirstVisible to LastVisible do
          fHeights[y] := Sizes[y].B - Sizes[y].A
      else
        for y := FirstVisible to LastVisible do
          fHeights[y] := INVALID_CACHE_SIZE;
    end;
  end;
end;

{ TMatrixSelector }

constructor TMatrixSelector.Create(aOwner: TComponent);
begin
  inherited;
  Visible := False;
end;

procedure TMatrixSelector.Paint;
begin
  //inherited;
  with Canvas do
  begin
    Brush.Color := clRed;
    Pen.Color := clWhite;
    Pen.Width := 3;
//    Rectangle(BoundsRect);
    FillRect(Rect(0, 0, ClientWidth, ClientHeight));

  end;
end;

{ TCellData }

procedure TCellData.Clear;
begin
  fText := '';
end;


{ TCellPainter }

class procedure TCellPainter.DoPaint(Params: TCellParams; Data: TCellData);
begin
  // doe niets
end;

{ TStringPainter }

class procedure TStringPainter.DoPaint(Params: TCellParams; Data: TCellData);
begin
  with Params do
  begin
    if Data = nil then
      Exit;
    if Data.Text <> '' then
    begin
       pTextColor := clred;
       pCellCanvas.Font.Color := pTextColor;
       SetBkMode(pCellCanvas.Handle, TRANSPARENT);
       DrawTextEx(cpCellCanvas.Handle, PChar(Data.fText), length(Data.fText), cpContentsRect, cpTextFlags, @cpTextParams);
    end;
  end; // with params
end;

{ TImagePainter }

class procedure TImagePainter.DoPaint(Params: TCellParams; Data: TCellData);
var
  D: TCellDataGraphic absolute Data;
begin
  with Params do
  begin
    if Data = nil then
      Exit;
    if not (Data is TCellDataGraphic) then
      Exit;
    if D.Graphic <> nil then
      pCellCanvas.StretchDraw(cpCanvasRect, D.Graphic);
  end; // with params
end;

{ TMatrixHeader }

procedure TMatrixHeader.Assign(Source: TPersistent);
var
  H: TMatrixHeader absolute Source;
begin
  if Source is TMatrixHeader then
  begin
    fHeaderType := H.fHeaderType;
    Size       := H.Size;
    Spanned    := H.Spanned;
    LineWidth  := H.LineWidth;
    fDownIndex  := H.fDownIndex;
    Index      := H.Index;
    Style      := H.Style;
    Hidden     := H.Hidden;
    Tag        := H.Tag;
{    if H.HeaderType = htLeft then
    if H.Hidden then
      windlg([H.Index, H.Hidden]); }
    //Changed(True);
  end
  else inherited Assign(Source);
end;

constructor TMatrixHeader.Create(aCollection: TCollection);
begin
  inherited Create(aCollection);
  fStyle := TCellStyle.Create(Matrix, ofHeader);
  fStyle.fOwnerFlag := ofHeader; // belangrijk voor defaults!
  fStyle.OnChange := StyleChanged;
  fHeaderType := TMatrixHeaders(aCollection).fHeaderType;
  case fHeaderType of
    htTop, htBottom: fSize := DEF_DEFAULTROWHEIGHT;
    htLeft, htRight: fSize := DEF_DEFAULTCOLWIDTH;
  end;
  fLineWidth := 1;
  fDownIndex := -1;

end;


destructor TMatrixHeader.Destroy;
begin
  fStyle.Free;
  inherited;
end;

function TMatrixHeader.GetMatrix: TBaseMatrix;
begin
  Result := TMatrixHeaders(Collection).fMatrix;
end;

procedure TMatrixHeader.SetHidden(const Value: Boolean);
begin
  if fHidden = Value then Exit;
  fHidden := Value;
  Changed(False);
end;

procedure TMatrixHeader.SetIndex(Value: Integer);
begin
  inherited;
  fIndex := Value;
end;

procedure TMatrixHeader.SetLineWidth(const Value: Integer);
begin
  if fLineWidth = Value then Exit;
  fLineWidth := Value;
  Changed(False);
end;

procedure TMatrixHeader.SetSize(const Value: Integer);
begin
  if fSize = Value then Exit;
  fSize := Value;
  Changed(False);
end;

procedure TMatrixHeader.SetSpanned(const Value: Boolean);
begin
  if fSpanned = Value then Exit;
  fSpanned := Value;
  Changed(False);
end;

procedure TMatrixHeader.SetStyle(const Value: TCellStyle);
begin
  fStyle.Assign(Value);
end;

procedure TMatrixHeader.StyleChanged(Sender: TObject);
begin
  if Matrix <> nil then
    Matrix.Invalidate; //###todo
end;

{ TMatrixHeaders }

function TMatrixHeaders.Add: TMatrixHeader;
begin
  Result := TMatrixHeader(inherited Add);
  UpdateSize;
  //Update(nil);
end;

constructor TMatrixHeaders.Create(aOwner: TPersistent; aHeaderClass: TMatrixHeaderClass;
  aHeaderType: THeadertype);
begin
  inherited Create(aOwner, aHeaderClass);
  fMatrix := TBaseMatrix(aOwner);
  fHeaderType := aHeaderType;
end;

function TMatrixHeaders.GetItem(Index: Integer): TMatrixHeader;
begin
  Result := TMatrixHeader(inherited GetItem(Index));
end;

function TMatrixHeaders.Insert(aIndex: Integer): TMatrixHeader;
begin
  Result := TMatrixHeader(inherited Insert(aIndex));
  UpdateSize;
end;

procedure TMatrixHeaders.Notify(Item: TCollectionItem; Action: TCollectionNotification);
begin
//  inherited;
{  case Action of
    cnDeleting: Dec(fSize, TMatrixHeader(Item).Size);
    cnAdded   : Inc(fSize, TMatrixHeader(Item).Size);
  end;                 }
  UpdateSize;
  fMatrix.HeadersChanged(Self);
  //if not (csDestroying in fMatrix.ComponentState) then
    //showmessage(i2s(count));
end;

procedure TMatrixHeaders.Update(Item: TCollectionItem);
begin
  inherited;
  UpdateSize;
  fMatrix.HeadersChanged(Self);
end;

procedure TMatrixHeaders.UpdateSize;
var
  OldSize: Integer;
  i: Integer;
  Item: TMatrixHeader;
  Ln: Integer;
begin
  OldSize := fSize;
  fSize := 0;

  for i := 0 to Count - 1 do
  begin
    Item := Items[i];
    if not Item.Hidden then
    begin
      Ln := fMatrix.GetEffectiveHeaderLineWidth(Item);
      Inc(fSize, Item.fSize + Ln);
    end;
  end;

  if fSize <> OldSize then
    fMatrix.HeadersChanged(Self);
end;


{ TBaseMatrix }

procedure TBaseMatrix.Assign(Source: TPersistent);
var
  Mx: TBaseMatrix absolute Source;

    procedure CopyOptions;
    begin
  //    Options.BeginUpdate;
      try
        SetOptions(Mx.Options);
      finally
//        Options.EndUpdate;   //dbgrids
      end;
      MatrixOptionsChanged(nil);
    end;

begin
  if Source is TBaseMatrix then
  begin
//    PaintLock;
    try
      Color := Mx.Color;
      TabStop := Mx.TabStop;
      Height := Mx.Height;
      Width := Mx.Width;
      BevelInner := Mx.BevelInner;
      BevelOuter := Mx.BevelOuter;
      BevelKind := Mx.BevelKind;
      BevelWidth := Mx.BevelWidth;
      BorderWidth := Mx.BorderWidth;
      BorderStyle := Mx.BorderStyle;
      Ctl3D := Mx.Ctl3D;
      ParentColor := Mx.ParentColor;
      ScrollBars := ScrollBars;
      DefaultLineColor := Mx.DefaultLineColor;
      DefaultHeaderLineColor := Mx.DefaultHeaderLineColor;


      SetCellStyle(Mx.fCellStyle);
      SetRowCount(Mx.fRowCount);
      SetColCount(Mx.fColCount);
      SetDefaultColWidth(Mx.fDefaultColWidth);
      SetDefaultRowHeight(Mx.fDefaultRowHeight);
      SetVisibleRowsRequested(Mx.fVisibleRowsRequested);

      CopyOptions;

      if Mx.fColumns <> nil then
        SetColumns(Mx.Columns);
      //SetRows(Mx.Rows);
//      windlg([mx.columns[2].style.colors.backcolor = clred]);
  //    windlg([columns[2].style.colors.backcolor = clred]);
    //  columns[2].style.colors.backcolor := mx.columns[2].style.colors.backcolor;



      if Mx.fTopHeaders <> nil then
        SetTopHeaders(Mx.fTopHeaders);
      if Mx.fLeftHeaders <> nil then
        SetLeftHeaders(Mx.fLeftHeaders);
//      UpdateHeaders;
    //  windlg([mx.leftheaders[0].hidden]);
     // windlg([leftheaders[0].hidden]);
//      windlg([topheaders.count]);

(*
  published
  { inherited properties }
    property Align;
    property BevelInner;
    property BevelOuter;
    property BevelKind;
    property BevelWidth;
    property BorderWidth;
    property BorderStyle;
    property Color;
    property Ctl3D;
    property ParentColor;
    property PopupMenu;
    property ScrollBars;
  { matrix properties }
    property Options; // publish vooraan!
    property CellStyle;
    property CellOffsetX;
    property CellOffsetY;
    property ColCount;
    property Columns;
    property CustomPainting;
    property CustomPaintStages;
    property DefaultColWidth;
    property DefaultHorzLineWidth;
    property DefaultLineColor;
    property DefaultRowHeight;
    property DefaultVertLineWidth;
    property HotImages;
    property Images;
    property LeftHeaders;
    property RightHeaders;
    property RowCount;
    property Rows;
    property SelectMode;
    property TopHeaders;
    property VisibleColsRequested;
    property VisibleRowsRequested; *)


//      InvalidatePageInfo;
  //    UpdateHeaders;
      //MatrixOptionsChanged(nil);
    finally

  //    PaintUnlock;
    end
  end
  else inherited;
end;

procedure TBaseMatrix.CacheClientSize;
{-------------------------------------------------------------------------------
  Cache van clientwindow om calls naar ClientWidth en ClientHeight te voorkomen.
-------------------------------------------------------------------------------}
var
  W, H: Integer;
begin
  W := ClientHeight;
  H := ClientWidth;
  fClientHeight := ClientHeight;
  fClientWidth := ClientWidth;
  UpdateGridRect;
end;

function TBaseMatrix.CalcAutoClientHeight: Integer;
{-------------------------------------------------------------------------------
  Zie opmerkingingen CalcAutoWidth
-------------------------------------------------------------------------------}

    procedure DoRow(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far;
    begin
      Result := aBottom + aLineWidth + 1;
    end;

var
  Range: TAxisRange;
begin
  Result := 0;//fGridRect.Top; // fCellOffsetY; ###headers
  if fVisibleRowsRequested <= 0 then
    Range := AllAxis
  else begin
    Range.First := 0;
    Range.Last := fVisibleRowsRequested - 1;
    RestrictRow(Range.Last);
  end;
  TraverseRows(@DoRow, Range);
  Inc(Result, fCellOffsetY);
  if (aoAddSpaceForRequestedRows in fOptions.fAutoSizeOptions)
  and (fVisibleRowsRequested > fRowCount) then
    Inc(Result, (fVisibleRowsRequested - fRowCount) * (fDefaultRowHeight + fDefaultHorzLineWidth));
end;

function TBaseMatrix.CalcAutoClientWidth: Integer;
{-------------------------------------------------------------------------------
  Bereken automatische breedte
  - Wanneer de CellOffset (links) gezet is wordt deze rechts ook erbij genomen
    voor de symmetrie (?????)
-------------------------------------------------------------------------------}

    procedure DoCol(aCol, aLeft, aRight, aLineWidth: Integer; var Stop: Boolean); far;
    begin
      Result := aRight + aLineWidth + 1;
      //Inc(Result, aRight - aLeft + 1);
    end;

var
  Range: TAxisRange;
begin
  Result := 0;//fGridRect.Left;//fCellOffsetX; ///#headers
  if fVisibleColsRequested <= 0 then
    Range := AllAxis
  else begin
    Range.First := 0;
    Range.Last := fVisibleColsRequested - 1;
    RestrictCol(Range.Last);
  end;
  TraverseCols(@DoCol, Range);
  Inc(Result, CellOffsetX);

  if (aoAddSpaceForRequestedCols in fOptions.fAutoSizeOptions)
  and (fVisibleColsRequested > fColCount) then
    Inc(Result, (fVisibleColsRequested - fColCount) * (fDefaultColWidth + fDefaultVertLineWidth));
end;

procedure TBaseMatrix.CalcInfoHorizontal(var aInfo: TPageAxisInfo; aLeft: Integer = -1);
{-------------------------------------------------------------------------------
  Berekening van horizontale paintcache.
  Houdt rekening met:
  o CellOffsetX
  o AutoAdjustLastColumn
  o HidePartialColumns
-------------------------------------------------------------------------------}
var
  x, cx, MaxWidth, TotalWidth, CurrentWidth, CurrentLnWidth, LowPoint, HighPoint: Integer;

    //#####EL autoadjustcolumn
    procedure CheckAutoLast;
    begin
      with Options do
        if (aoAutoAdjustLastColumn in fAutoSizeOptions) and
        not (aoAutoWidth in fAutoSizeOptions)
        and (CurrentWidth < fClientWidth - TotalWidth - CurrentlnWidth) then
          CurrentWidth := fClientWidth - TotalWidth - CurrentlnWidth;
    end;

begin
  if aLeft < 0 then aLeft := fTopLeft.x;
  MaxWidth := fClientWidth;//fGridRect.Right - fGridRect.Left;
  TotalWidth := fGridRect.Left;// fCellOffsetX; //#headers
  with aInfo do
  begin
    Valid := False;

    { laat de compiler de arrays opschonen }
    Indices := nil;
    Sizes := nil;
    Lines := nil;
    FillChar(aInfo, SizeOf(aInfo), 0);
    OffSet := aLeft;
    for x := 0 to fColCount - 1 do
    begin
      cx := x;
      //if cx >= fFixedCols then cx := x + aLeft - fFixedCols;
      cx := x + aLeft; //#######
      begin
        if x = 0 then FirstVisible := cx;
        CurrentWidth := GetColWidth(cx);
        CurrentLnWidth := GetEffectiveVertLineWidth(cx);
        if cx = ColCount - 1 then
          CheckAutoLast;
        Inc(TotalWidth, CurrentWidth + CurrentLnWidth);
        LowPoint := TotalWidth - CurrentWidth - CurrentLnWidth;
        HighPoint := TotalWidth - CurrentLnWidth - 1;
        Inc(VisibleCount);
        SetInteger(Indices, VisibleCount - 1, cx);
        SetPoint(Sizes, VisibleCount - 1, Lowpoint, HighPoint);
        SetInteger(Lines, VisibleCount - 1, CurrentLnWidth);
      end;
//      else Inc(HiddenCount);
      if (TotalWidth >= MaxWidth{fClientWidth}) or (cx >= fColCount - 1) then // ###headers
      begin
        LastVisible := cx;
        Break;
      end;
    end;
    GridSize           := Min(TotalWidth, MaxWidth{fClientWidth}); //##headers
    TotalCount         := VisibleCount + HiddenCount;
    FullVisibleCount   := VisibleCount - Ord(TotalWidth > MaxWidth{fClientWidth}); //##headers
    LastFullVisible    := LastVisible - Ord(FullVisibleCount <> VisibleCount);

    Valid := True;
    { dit lijkt te werken om gedeeltelijke laatste cell te verbergen
    if LastFullVisible < LastVisible then
    begin
      Dec(TotalCount);
      Dec(LastVisible);
      Dec(VisibleCount);
    end;
    }

  end;
end;

procedure TBaseMatrix.CalcInfoVertical(var aInfo: TPageAxisInfo; aTop: Integer = -1);
var
  y, cy, TotalHeight, CurrentHeight, CurrentLnWidth, LowPoint, HighPoint: Integer;

    //#####EL autoadjustrow
    procedure CheckAutoLast;
    begin
      with Options do
        if (aoAutoAdjustLastRow in fAutoSizeOptions)
        and not (aoAutoHeight in fAutoSizeOptions)
        and (CurrentHeight < fClientHeight - TotalHeight - CurrentlnWidth) then
           CurrentHeight := fClientHeight - TotalHeight - CurrentlnWidth;
    end;


  {todo: hier optie in verwerken om halve cellen te verbergen }
begin
  StateEnter(isCalculatingVertInfo);
  try

    if aTop < 0 then aTop := fTopLeft.y;
    TotalHeight := fGridRect.Top;//fCellOffsetY; ###headers
  //  Log([fgridrect.top]);
    with aInfo do
    begin
      Valid := False;

      { laat Delphi de array opschonen }
      Indices := nil;
      Sizes := nil;
      Lines := nil;
      FillChar(aInfo, SizeOf(aInfo), 0);
      FirstVisible := -1;
      Flags := AXISINFOFLAG_VERTICAL;
      OffSet := aTop;
      for y := 0 to fRowCount - 1 do
      begin
        cy := y;
        cy := y + aTop;
        { houdt hoogte bij }
        CurrentHeight := GetEffectiveRowHeight(cy);
        if CurrentHeight = HIDDEN_HEIGHT then Continue;
        CurrentLnWidth := GetEffectiveHorzLineWidth(cy);
        if cy = RowCount - 1 then
          CheckAutoLast;
        Inc(TotalHeight, CurrentHeight + CurrentLnWidth);
        LowPoint := TotalHeight - CurrentHeight - CurrentLnWidth;
        HighPoint := TotalHeight - CurrentLnWidth - 1;
        { zet eerste zichtbare rij en update aantal zichtbare rijen }
        if TotalHeight >= 0 then
        begin
          if FirstVisible = -1 then
            FirstVisible := cy;
          Inc(VisibleCount);
        end;
        { voeg zichtbare rij toe aan array en cache rowpoints }
        if VisibleCount > 0 then
        begin
          SetInteger(Indices, VisibleCount - 1, cy);
          SetPoint(Sizes, VisibleCount - 1, LowPoint, HighPoint);
          SetInteger(Lines, VisibleCount - 1, CurrentLnWidth);
        end;
        { als we bijna out of range zijn stoppen we ermee }
        if (TotalHeight >= fClientHeight) or (cy >= fRowCount - 1) then
        begin
          LastVisible := cy;
          Break;
        end;
      end;

      GridSize           := Min(TotalHeight, fClientHeight);
      TotalCount         := VisibleCount + HiddenCount;
      FullVisibleCount   := VisibleCount - Ord(TotalHeight > fClientHeight);
      LastFullVisible    := LastVisible - Ord(FullVisibleCount <> VisibleCount);

      Valid := True;
      { dit lijkt te werken om gedeeltelijke laatste rij te verbergen }
  {    if LastFullVisible < LastVisible then
      if poHidePartialRows in Options.PaintOptions then
      begin
        Dec(TotalCount);
        Dec(LastVisible);
        Dec(VisibleCount);
      end; }

    end;
  finally
    StateLeave(isCalculatingVertInfo);
  end;
end;


procedure TBaseMatrix.CalcPageInfo(var aPageInfo: TPageInfo; aLeft: Integer = -1; aTop: Integer = -1; ForceRecalc: Boolean = False);
begin
  if aLeft < 0 then aLeft := fTopLeft.x;
  if aTop < 0 then aTop := fTopLeft.y;

  if not HandleAllocated then //##voorlopige noodoplossing
  begin
    fClientWidth := Width;
    fClientHeight := Height;
  end;


  if not ForceRecalc and ValidPageInfo and (aLeft = fTopLeft.x) and (aTop = fTopLeft.y) then
  begin
    aPageInfo := fPageCache;
    Exit;
  end;

//  Log(['calc']);

  StateEnter(isCalculatingPageInfo);
  try
    { eerst moeten we de kolombreedtes weten ivm eventuele auto-textheights.
      dit moet waarschijnlijk nog aangepast als de volgorde anders moet zijn. }
    CalcInfoHorizontal(aPageInfo.Horz, aLeft);
    fPageCache.Horz := aPageInfo.Horz;

    CalcInfoVertical(aPageInfo.Vert, aTop);
    fPageCache.Vert := aPageInfo.Vert;
  finally
    StateLeave(isCalculatingPageInfo);
  end;

end;

function TBaseMatrix.CalcPgDn(const CurrentInfo: TPageInfo; var NewInfo: TPageInfo): Boolean;
{-------------------------------------------------------------------------------
  Er zijn een aantal situaties mogelijk:
  1) normale pgdown
  2) pgdown komt aan het eind van de rows en zet de page zo, dat er een maximaal
     aantal rijen zichtbaar is
  3) er is maar 1 rij zichtbaar, forceer 1 naar beneden (als mogelijk)
-------------------------------------------------------------------------------}
var
  CurrLast, TestTop: Integer;
  SaveInfo: TPageInfo;
  RowsDone: Integer;
//  Ints: TCachedIntegers;
  H: Integer;

    procedure DoRow(aRow, aHeight, aLineWidth: Integer; var Stop: Boolean); far;
    begin
      Inc(H, aHeight + aLineWidth);
      if H >= RectHeight(fGridRect) then
      begin
        Stop := True;
        Exit;
      end;
      TestTop := aRow;
      //Log(['testtop',testtop, aHeight, H, RectHeight(fgridrect)]);
    end;

begin
  Result := False;

  with CurrentInfo.Vert do
  begin
    // check last
    if FullVisibleCount = 0
    then CurrLast := LastVisible
    else CurrLast := LastFullVisible;

  // we kunnen echt niet verder naar beneden
    if LastFullVisible >= fRowCount - 1 then
      Exit;
  end;

  // nu hebben we in ieder geval iets
  Result := True;

  CalcInfoVertical(NewInfo.Vert, CurrLast + 1);

  // gewone page, klaar
  if NewInfo.Vert.LastVisible < fRowCount - 1 then
    Exit;

  // loop naar boven tot niet meer past
  H := 0;
  TestTop := fRowCount - 1;
  TraverseRowHeights(@DoRow, MakeAxisRange(fRowCount - 1, CurrentInfo.Vert.FirstVisible));

  CalcPageInfo(NewInfo, fTopLeft.X, TestTop);
  fPageCache := CurrentInfo;//##aiaiai cache fout

end;

procedure TBaseMatrix.CalculateCellHeight(Params: TCellParams; Data: TCellData; var aHeight: Integer);
var
  R: TRect;
  Flags: DWORD;
  S: string;
begin
  if Data <> nil then
    S := Data.Text
  else
    S := '';
//  Result := params.cpRow * 10;
  R := EmptyRect;
  R.Right := Params.cpCalcSize;
  with Params do
    aHeight := DrawTextEx(cpCellCanvas.Handle, PChar(S), Length(S), R, cpTextFlags or DT_CALCRECT, nil);
  Inc(aHeight, 4); // ##margin
end;

function TBaseMatrix.CalculateRowHeight(aRow: Integer): Integer;
var
  X, H: Integer;
  Data: TCellData;
begin
  Result := 0;
  h := 0; //if not fPageCache.PageInfo.Horz.Valid then //raise Exception.Create('Kan hoogte niet berekenen');
  fCellParams.cpCalculating := True;
  try
    for x := 0 to ColCount - 1 do
    begin
      Data := nil;
      DoGetCellData(x, aRow, Data);
      { ###EL nog niet perfect als laatste kolom autofill EN wordwrap aan!!! }
      with fCellParams do
      begin
        cpCol := x;
        cpRow := aRow;
        cpCalcSize := FindCachedWidth(x);
        if cpcalcsize = -1 then
          cpCalcSize := ColWidths[x];
      end;
      //then raise exception.create('ai');
      PreparePaintCell(fCellParams, Data);
      CalculateCellHeight(fCellParams, Data, h);
      Result := Max(h, Result);
    end;
  finally
    fCellParams.cpCalculating := False;
  end;
end;

function TBaseMatrix.CanAutoGenerateObjects: Boolean;
begin
  Result := ComponentState * [csDesigning, csLoading, csDestroying] = [];  //##oei oei oei
end;

function TBaseMatrix.CanEditCell(aCol, aRow: Integer): Boolean;
begin
  Result := eoCanEdit in fOptions.fEditOptions;
end;

function TBaseMatrix.CanPaint: Boolean;
begin
  Result := HandleAllocated and not PaintLocked and Visible;
end;

function TBaseMatrix.CellRectToScreenRect(aCol, aRow: integer; var aRect: TRect): Boolean;
var
  PgInfo: TPageInfo;
  R: TRect;

    procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far;
    begin
      aRect := aCellRect;
    end;

begin
  aRect := ErrorRect;
  CalcPageInfo(PgInfo);
  TraverseVisibleCells(@DoCell, PgInfo, Rect(aCol, aRow, aCol, aRow));
  Result := aRect.Left >= 0;
end;

function TBaseMatrix.CellRectToVirtualScreenRect(aCol, aRow: integer; var aRect: TRect): Boolean;
{-------------------------------------------------------------------------------
  CellRectToVirtualScreenRect:
  geeft cellrect terug, ongeacht of deze op het scherm staat
-------------------------------------------------------------------------------}

  procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer;
    var Stop: Boolean); far;
  begin
    aRect := aCellRect;
  end;

begin
  aRect := ErrorRect;
  TraverseCells(@DoCell, Rect(aCol, aRow, aCol, aRow));
  Result := aRect.Left >= 0;
end;

procedure TBaseMatrix.CellStyleChanged(Sender: TObject);
begin
  Invalidate;
end;

function TBaseMatrix.ChangeCol(Delta: Integer; var aNewCol: Integer): Boolean;
begin
  aNewCol := fCol;
  Inc(aNewCol, Delta);
  RestrictCol(aNewCol);
  Result := fCol <> aNewCol;
end;

function TBaseMatrix.ChangeRow(Delta: Integer; var aNewRow: Integer): Boolean;
begin
  aNewRow := fRow;
  Inc(aNewRow, Delta);
  RestrictRow(aNewRow);
  Result := fRow <> aNewRow;
end;

procedure TBaseMatrix.CheckCol(aCol: Integer);
begin
  if not ValidCol(aCol) then raise Exception.CreateFmt('CheckCol %d', [aCol]);
end;

procedure TBaseMatrix.CheckCoord(aCol, aRow: Integer);
begin
  CheckCol(aCol);
  CheckRow(aRow);
end;

procedure TBaseMatrix.CheckCoord(aCoord: TPoint);
begin
  CheckCoord(aCoord.x, aCoord.y);
end;

procedure TBaseMatrix.CheckRow(aRow: Integer);
begin
  if not ValidRow(aRow) then raise Exception.CreateFmt('CheckRow %d', [aRow]);
end;

procedure TBaseMatrix.CMBorderChanged(var Msg: TMessage);
begin
  inherited;
  CacheClientSize;
  UpdateAutoBounds;
end;

procedure TBaseMatrix.CMCtl3DChanged(var Message: TMessage);
begin
  inherited;
  CacheClientSize;
  RecreateWnd;
  UpdateAutoBounds;
end;

procedure TBaseMatrix.CMExit(var Message: TMessage);
begin
  StateEnter(isKillingFocus);
  try
    inherited;
  finally
    StateLeave(isKillingFocus);
  end;
end;

procedure TBaseMatrix.CMMouseEnter(var Message: TMessage);
begin
  inherited;
  fMouseInfo.InMatrix := True;
  if Assigned(fOnMouseEnter) then
    fOnMouseEnter(Self);
end;

procedure TBaseMatrix.CMMouseLeave(var Message: TMessage);
var
  OldX, OldY: Integer;
begin
  inherited;
  fMouseInfo.InMatrix := False;
  if Assigned(fOnMouseLeave) then
    fOnMouseLeave(Self);

  with fMouseInfo.MoveInfo do
  begin
    OldX := mHitCol;
    OldY := mHitRow;
    mHitCol := -1;
    mHitRow := -1;
  end;
  InvalidateCell(OldX, OldY, True);
end;

procedure TBaseMatrix.ColumnAdded(Item: TMxColumn);
begin
  fColCount := fColumns.Count;
end;

procedure TBaseMatrix.ColumnDeleting(Item: TMxColumn);
var
  Keep: Boolean;
begin
//  if InState(isUpdatingColumns) then
  //  if Options.KeepColumnsOnDestroy then
    //  Exit;
  fColCount := fColumns.Count - 1; // item is nu nog niet verwijderd!
  DoAfterDeletingColumn(Item);
end;

procedure TBaseMatrix.ColumnIndexChanged(Item: TMxColumn; OldIndex, NewIndex: Integer);
begin
//  if not csDestroying in ComponentState then
  DoAfterColumnIndexChanged(Item, OldIndex, NewIndex);
end;

procedure TBaseMatrix.ColumnToBitmap(aCol: Integer; aBitmap: TMxBitmap);
var
  R: TRect;
begin
  if aBitmap = nil then
    Exit;
  R := GetColRect(aCol);
  RectMove(R, -R.Left, -R.Top);
  aBitmap.Width := R.Right - R.Left;
  aBitmap.Height := R.Bottom - R.Top;
//  fUpdateRect :
//  PaintTo(aBitmap.Canvas, -R.Left, -R.Top);
//Include()
  ControlState := ControlState + [csPaintCopy];
  try
    PaintMatrix(aBitmap.Canvas, R);
  finally
    ControlState := ControlState - [csPaintCopy];
  end;
end;

procedure TBaseMatrix.ColumnVisualChanged(aColumn: TMxColumn; SomeSizeChanged: Boolean);
begin
  if SomeSizeChanged then
  begin
    PaintLock;
    try
      UpdateAutoBounds;
    finally
      PaintUnlock;
    end
  end
  else InvalidateCol(aColumn.Index);
end;

function TBaseMatrix.ColVisibility(aCol: Integer; const HorzInfo: TPageAxisInfo): Integer;
{-------------------------------------------------------------------------------
  ColVisibility:
  kijkt ahv horzinfo hoe zichtbaar de kolom is
  0 : niet zichtbaar
  1 : gedeeltelijk zichtbaar
  2 : volledig zichtbaar
-------------------------------------------------------------------------------}
begin
  {$ifdef check_bugs}
  if HorzInfo.Flags and AXISINFOFLAG_VERTICAL <> 0 then
    raise Exception.Create('ColVisibility flag error');
  {$endif}
  Result := NOT_VISIBLE;
  with HorzInfo do
  begin
    { niet zichtbaar }
    if VisibleCount = 0 then
      Exit;
    if ExistInteger(Indices, aCol, VisibleCount - 1) then
      Result := PARTIAL_VISIBLE;
    if Result >= PARTIAL_VISIBLE then
    begin
      if aCol <= LastFullVisible then
        Result := FULL_VISIBLE;
    end;
  end;
end;

constructor TBaseMatrix.Create(aOwner: TComponent);
begin
  inherited;
//  Visible := False;
//  showmessage(inttostr(ord(visible)));
//  InitializeGlobals;
  UsesBitmaps;
  // voorkom hover bij opstart
  with fMouseInfo.MoveInfo do
  begin
    mHitRow := -1;
    mHitCol := -1;
  end;

  ControlStyle := ControlStyle + [csDoubleClicks];

  ParentColor                  := False;
  TabStop                      := True;
  Color                        := DEF_BACKCOLOR;

  fColCount                    := DEF_COLCOUNT;
  fDefaultColWidth             := DEF_DEFAULTCOLWIDTH;
  fDefaultHorzLineWidth        := DEF_DEFAULTHORZLINEWIDTH;
  fDefaultRowHeight            := DEF_DEFAULTROWHEIGHT;
  fDefaultVertLineWidth        := DEF_DEFAULTVERTLINEWIDTH;
  fRowCount                    := DEF_ROWCOUNT;
  fDefaultLineColor            := DEF_LINECOLOR;
  fDefaultHeaderLineColor      := DEF_HEADERLINECOLOR;
  fSelection                   := ErrorRect;
  fBorderStyle                 := bsSingle;
  fLineTreshold                := 2;

  fAreaRects[maGrid]           := @fGridRect;
  fAreaRects[maTopLeft]        := @fHeaderAreas.hTopLeft;
  fAreaRects[maTop]            := @fHeaderAreas.hTop;
//  fAreaRects[maTopRight]       := @fHeaderAreas.hTopLeft;
  fAreaRects[maLeft]           := @fHeaderAreas.hLeft;
//  fAreaRects[maRight]          := @fHeaderAreas.hTopLeft;
//  fAreaRects[maBottomLeft]     := @fHeaderAreas.hTopLeft;
//  fAreaRects[maBottom]         := @fHeaderAreas.hTopLeft;
//  fAreaRects[maBottomRight]    := @fHeaderAreas.hTopLeft;


  fUseCache := True;
  fGridCache := TGridCache.Create(Self);
  fCellData := TCellData.Create;
  fCellParams := TCellParams.Create;
  fHeaderParams := THeaderParams.Create;
  fOptions := TMatrixOptions.Create(Self);
  fOptions.OnChange := MatrixOptionsChanged;
  fCellStyle := TCellStyle.Create(Self, ofMatrix);
  fCellStyle.OnChange := CellStyleChanged;
  fZoomRatio := 1;

  { dit mag hier pas ivm interne objecten }
  Width := DEF_WIDTH;
  Height := DEF_HEIGHT;

  UpdateGridRect;
  MatrixOptionsChanged(Self);
end;

procedure TBaseMatrix.CreateParams(var Params: TCreateParams);
begin
  inherited;
  with Params do
  begin
    Style := Style or WS_CLIPCHILDREN or WS_CLIPSIBLINGS or WS_TABSTOP;// or WS_BORDER;


    if fBorderStyle = bsSingle then
      if NewStyleControls and Ctl3D then
      begin
        Style := Style and not WS_BORDER;
        ExStyle := ExStyle or WS_EX_CLIENTEDGE;
      end
      else
        Style := Style or WS_BORDER;
    // ExStyle := ExStyle or WS_EX_OVERLAPPEDWINDOW; 3D dik
    //ExStyle := ExStyle or WS_EX_STATICEDGE; 3D dun
    // WS_BORDER = single;
    // WS_THICKFRAME = resizable als form   grids

    if fScrollBars in [ssVertical, ssBoth] then Style := Style or WS_VSCROLL;
    if fScrollBars in [ssHorizontal, ssBoth] then Style := Style or WS_HSCROLL;
    WindowClass.style := CS_DBLCLKS;

//    WindowClass.style := WindowClass.style and not (CS_HREDRAW or CS_VREDRAW); // een wonderbaarlijke genezing!!!


    //Params.WindowClass.Style := Params.WindowClass.Style or CS_DROPSHADOW;
  end;
end;

procedure TBaseMatrix.CreateWnd;
begin
  inherited CreateWnd;
  CacheClientSize;
//  ColRow := Point(fCol, fRow);
  if not (csDesigning in ComponentState) then
    DoCellEnter(fCol, fRow); //###
end;

destructor TBaseMatrix.Destroy;
begin
//  StateEnter(isDestroying)
  {$ifdef check_bugs}
  //WinDlg([timercounts]);
  {$endif}
  try
    TimerStop(CURSOR_TIMER);
    TimerStop(CLEARSEARCH_TIMER);

    if fEditor <> nil then
      fEditor := nil;

    if fColumns <> nil then fColumns.Free;
    if fRows <> nil then fRows.Free;
    fOptions.Free;
    fCellStyle.Free;
    fCellParams.Free;
    fHeaderParams.Free;
    fCellData.Free;
    fTopHeaders.Free;
    fLeftHeaders.Free;
    fRightHeaders.Free;
    fGridCache.Free;

  //  fSelector.Free;
    inherited;
  finally
    ReleaseBitmaps; //dbgrids
  end;
end;

procedure TBaseMatrix.CustomPaintBackGround(const PaintInfo: TPaintInfo);
begin
  // doe niets
end;

procedure TBaseMatrix.CustomPaintCell(Params: TCellParams; const aPaintInfo: TPaintInfo; Data: TCellData);
begin
  // doe niets
end;

procedure TBaseMatrix.CustomAfterPaint(const PaintInfo: TPaintInfo);
begin
  // doe niets
end;

procedure TBaseMatrix.DoAfterColumnIndexChanged(aItem: TMxColumn; aOldIndex, aNewIndex: Integer);
begin
  // doe niets
end;

procedure TBaseMatrix.DoCellClick(aCol, aRow: Integer);
begin
  if Assigned(fOnCellClick) then
    fOnCellClick(Self, aCol, aRow);
end;

procedure TBaseMatrix.DoCellDblClick(aCol, aRow: Integer);
begin
  if Assigned(fOnCellDblClick) then
    fOnCellDblClick(Self, aCol, aRow)
end;

procedure TBaseMatrix.DoCellEnter(aCol, aRow: Integer);
begin
//  Log(['enter cell', acol, arow]);
//  R := GetCellRect(fCol, fRow);
//  fSelector.SetBounds(R.Left, R.Top, RectWidth(R), RectHeight(R));
//  fSelector.Show;
//  if fMatrixFocused then
  //  if eoAlwaysShow in fOptions.fEditOptions then
    //  DoEditBegin;
  if Assigned(fOnSelectCell) then fOnSelectCell(Self, aCol, aRow); //###do
//  if fMultiCastEventHandler <> nil then
  //  fMultiCastEventHandler.HandleOnSelectCell(aCol, aRow);
end;

procedure TBaseMatrix.DoHoverCell(aCol, aRow: Integer; Leave: Boolean);
begin
  if ValidCoord(aCol, aRow) then
    if Assigned(fOnHoverCell) then fOnHoverCell(Self, aCol, aRow, Leave);
end;

procedure TBaseMatrix.DoCellExit(aCol, aRow: Integer);
begin
  if Assigned(fOnDeselectCell) then
    fOnDeselectCell(Self, fCol, fRow);
  DoEditEnd;
  if not (csDesigning in ComponentState) then
    Application.CancelHint;
end;

procedure TBaseMatrix.DoCellHint(aCol, aRow: Integer);
begin
  if ShowHint then
  begin
    Hint := DoGetCellHint(aCol, aRow);
    if Hint <> '' then
      if not (csDesigning in ComponentState) then
        Application.CancelHint;//(mouse.cursorpos);
  end;
end;

function TBaseMatrix.DoChangeCol(Delta: Integer): Boolean;
var
  OldCol, NewCol: Integer;
begin

//  DoEditEnd;

  {$ifdef check_bugs}
  OldCol := fCol;
  {$endif}

  NewCol := fCol;
  Result := ChangeCol(Delta, NewCol);

  {$ifdef check_bugs}
  if fCol <> OldCol then
    raise Exception.Create('DoChangeRow error: fCol mag niet veranderen in ChangeCol');
  {$endif}

  //if not ValidCol(NewCol) then
    //Exit; //raise Exception.Create('DoChangeCol error');

  RestrictCol(NewCol);

  InternalSetCol(NewCol, Result);
//  fCol := NewCol; //##fcol changed 1
end;

function TBaseMatrix.DoChangeRow(Delta: Integer): Boolean;
var
  OldRow, NewRow: Integer;
begin

//  DoEditEnd;

  {$ifdef check_bugs}
  OldRow := fRow;
  {$endif}

  NewRow := fRow;
  Result := ChangeRow(Delta, NewRow);

  {$ifdef check_bugs}
  if fRow <> OldRow then
    raise Exception.Create('DoChangeRow error: fRow mag niet veranderen in ChangeRow');
  {$endif}

  RestrictRow(NewRow);

  InternalSetRow(NewRow, Result);
  //fRow := NewRow; //##frow changed 1
end;

procedure TBaseMatrix.DoAfterDeletingColumn(aItem: TMxColumn);
begin
  // doe niets
end;

function TBaseMatrix.DoCreateCellData: TCellData;
begin
  Result := TCellData.Create;
end;

function TBaseMatrix.DoCreateColumns: TMxColumns;
begin
  Result := TMxColumns.Create(Self, TMxColumn);
end;

function TBaseMatrix.DoCreateEditor(aCol, aRow: Integer): IMatrixEditor;
begin
  Result := nil;
  if Assigned(fOnCreateEditor) then
    fOnCreateEditor(Self, aCol, aRow, Result);
  if Result = nil then
    Result := TStringEditor.Create;
end;

function TBaseMatrix.DoCreateRows: TMxRows;
begin
  Result := TMxRows.Create(Self, TMxRow);
end;

procedure TBaseMatrix.DoEditBegin;
{-------------------------------------------------------------------------------
  1) Editor wordt opgestart: Editor krijgt focus (normaal gesproken)
  2) WMKillFocus gaat af
  3) Daar wordt DoEditEnd aangeroepen
-------------------------------------------------------------------------------}
var
  R: TRect;
begin
  if InState(isEditing) then Exit;
  if not ValidCoord(fCol, fRow) then Exit;
  if not fMatrixFocused then Exit;
  if not CanEditCell(fCol, fRow) then Exit;

  StateEnter(isEditBeginning);
  try
    fEditCol := fCol;
    fEditRow := fRow;
    fEditor := DoCreateEditor(fCol, fRow);
    if fEditor <> nil then
    begin
      if fEditor.PrepareEdit(Self, fCol, fRow) then
      begin
        R := GetCellRect(fCol, fRow);
        fEditor.SetBounds(R);
        if fEditor.BeginEdit then
        begin
          StateEnter(isEditing);
        end;
      end;
    end;
  finally
    StateLeave(isEditBeginning)
  end;
end;

procedure TBaseMatrix.DoEditCancel(Refocus: Boolean = True);
begin
  if not InState(isEditing) then
    Exit;
  StateEnter(isEditCanceling);
  try
    fEditor.CancelEdit;
    fEditor := nil;
    StateLeave(isEditing);
    if not InStates([isKillingFocus, isEditEnding]) then
      SetFocus;
  finally
    StateLeave(isEditCanceling);
  end;
end;

procedure TBaseMatrix.DoEditEnd(Refocus: Boolean = True);
begin
  if not InState(isEditing) then
    Exit;
  StateEnter(isEditEnding);
  try
    fEditor.EndEdit;
    fEditor := nil;
    StateLeave(isEditing);

    if not InStates([isKillingFocus]) then
      if Refocus then
        SetFocus;

    if poCalculatedRowHeights in Options.PaintOptions then //### nog een beetje botte oplossing
      Invalidate;

//    Log([getparentform(self).ActiveControl = nil]);
  finally
    StateLeave(isEditEnding);
  end;
end;

procedure TBaseMatrix.DoGetCellData(aCol, aRow: Integer; out Data: TCellData);
begin
  Data := fCellData;
  if Assigned(fOnGetCellData) then
  begin
    if ValidCoord(aCol, aRow) then
      fOnGetCellData(Self, aCol, aRow, Data);
  end;
end;

function TBaseMatrix.DoGetCellPainter(Params: TCellParams; Data: TCellData): TCellPainterClass;
begin
  Result := nil;
  if Assigned(fOnGetCellPainter) then
    fOnGetCellPainter(Self, Params, Data, Result);
  if Result = nil then
    Result := TStringPainter;
end;

function TBaseMatrix.DoGetHeaderText(aHeader: TMatrixHeader; aIndex: Integer): string;
begin
  Result := '';
  case aHeader.HeaderType of
    htTop:
      begin
        if fColumns <> nil then
          Result := fColumns[aIndex].Title;
      end;
    htLeft:
      begin
        //Result := '';
      end;
  end;
  if Assigned(fOnGetHeaderText) then
    fOnGetHeaderText(Self, aHeader, aIndex, Result);

end;

function TBaseMatrix.DoGetCellHint(aCol, aRow: Integer): string;
begin
  Result := '';
  {$ifdef matrix_testmode}
  Result := i2s(aCol)+ ':' +i2s(aRow);
  {$endif}
  if Assigned(fOnGetCellHint) then
    fOnGetCellHint(Self, aCol, aRow, Result);
end;

procedure TBaseMatrix.DoHeaderClick(aHeader: TMatrixHeader; aCol, aRow: Integer);
begin
  case aHeader.fHeaderType of
    htTop: if Assigned(fOnTopHeaderClick) then fOnTopHeaderClick(Self, aHeader, aCol);
    htBottom:;
    htLeft: if Assigned(fOnLeftHeaderClick) then fOnLeftHeaderClick(Self, aHeader, aRow);
    htRight:;
  end;
end;

function TBaseMatrix.DoGotoFirstCol: Boolean;
var
  OldCol, NewCol: Integer;
begin
  {$ifdef check_bugs}
  OldCol := fCol;
  {$endif}

  NewCol := fCol;
  Result := GotoFirstCol(NewCol);

  {$ifdef check_bugs}
  if fCol <> OldCol then
    raise Exception.Create('DoChangeCol error');
  {$endif}

  RestrictCol(NewCol);
  InternalSetCol(NewCol, Result);
//  fCol := NewCol; //##fcol changed 2
end;

function TBaseMatrix.DoGetActiveStyle(aCol, aRow: Integer; var aMatrixStyle, aColStyle, aRowStyle: TCellStyle): TCellStyle;
begin
  Result := nil;
  if Assigned(fOnGetActiveStyle) then
    fOnGetActiveStyle(Self, aCol, aRow, aMatrixStyle, aColStyle, aRowStyle, Result);
end;

function TBaseMatrix.DoGotoFirstRow: Boolean;
var
  OldRow, NewRow: Integer;
begin
//  DoEditEnd;

  {$ifdef check_bugs}
  OldRow := fRow;
  {$endif}

  NewRow := fRow;
  Result := GotoFirstRow(NewRow);

  {$ifdef check_bugs}
  if fRow <> OldRow then
    raise Exception.Create('DoChangeRow error');
  {$endif}

  RestrictRow(NewRow);
  InternalSetRow(NewRow, Result);
//  fRow := NewRow; //##frow changed 2
end;

function TBaseMatrix.DoGotoLastCol: Boolean;
var
  OldCol, NewCol: Integer;
begin
  {$ifdef check_bugs}
  OldCol := fCol;
  {$endif}

  NewCol := fCol;
  Result := GotoLastCol(NewCol);

  {$ifdef check_bugs}
  if fCol <> OldCol then
    raise Exception.Create('DoChangeCol error');
  {$endif}

  RestrictCol(NewCol);
  InternalSetCol(NewCol, Result);
  //fCol := NewCol; //##fcol changed 3
end;

function TBaseMatrix.DoGotoLastRow: Boolean;
var
  OldRow, NewRow: Integer;
begin

//  DoEditEnd;
  {$ifdef check_bugs}
  OldRow := fRow;
  {$endif}

  NewRow := fRow;
  Result := GotoLastRow(NewRow);

  {$ifdef check_bugs}
  if fRow <> OldRow then
    raise Exception.Create('DoChangeRow error');
  {$endif}

  RestrictRow(NewRow);
  InternalSetRow(NewRow, Result);
//  fRow := NewRow; //##frow changed 3
end;

function TBaseMatrix.DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
  Result := True;
  DoNavigate(srMouseWheel, scDown);
//  MoveTopLeft(fTopLeft.X, fTopLeft.Y + 1);
//  invalidate;
end;

function TBaseMatrix.DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
  Result := True;
  DoNavigate(srMouseWheel, scUp)
end;

procedure TBaseMatrix.DoNavigate(aReason: TScrollReason;
                                 aCommand: TScrollCommand;
                                 aHorzPosition: THorizontalFocusPosition = hfpAuto;
                                 aVertPosition: TVerticalFocusPosition = vfpAuto;
                                 XParam: Integer = -1;
                                 YParam: Integer = -1);
{-------------------------------------------------------------------------------
  De centrale navigatie routine
-------------------------------------------------------------------------------}
begin

  with fNavigationInfo do
  begin
    if nNavigating then
      Exit;
    nNavigating := True;
    nReason := aReason;
    nCommand := aCommand;
    nOldCol := fCol;
    nOldRow := fRow;
    nCellExited := False;
  end;

  try
    Navigation(aReason, aCommand, XParam, YParam);

    if fNavigationInfo.nCellExited then
    begin
      DoCellEnter(fCol, fRow);
      //Log(['cell exited']);
    end;

    if fScrollBars <> ssNone then
      UpdateScrollPos;

  finally
    with fNavigationInfo do
    begin
      nNavigating := False;
      nReason := srNone;
      nCommand := scNone;
    end;
  end;
end;

procedure TBaseMatrix.DoPostMessage(var Message: TMessage);
begin
  PostMessage(Handle, Message.Msg, message.WParam, message.LParam);
end;

procedure TBaseMatrix.DoSetBottomRight(aRight, aBottom: Integer);
begin

end;

procedure TBaseMatrix.DoSetCellData(aCol, aRow: Integer; Data: TCellData);
begin

(*
  if Assigned(fOnGetCellData) then
  begin
    if ValidCoord(aCol, aRow) then
      fOnGetCellData(Self, aCol, aRow, Data);
  end;
  *)
  //
end;

procedure TBaseMatrix.EditCancel;
{-------------------------------------------------------------------------------
  Een veilige manier om de edit te destroyen vanuit de editor zelf
-------------------------------------------------------------------------------}
begin
  if InState(isEditing) then
    PostMessage(Handle, EMM_CANCELEDIT, 0, 0);
end;

procedure TBaseMatrix.EditEnd;
{-------------------------------------------------------------------------------
  Een veilige manier om de edit te destroyen vanuit de editor zelf
-------------------------------------------------------------------------------}
begin
  if InState(isEditing) then
    PostMessage(Handle, EMM_ENDEDIT, 0, 0);
end;

procedure TBaseMatrix.EditExit;
begin
  if InState(isEditing) then
    PostMessage(Handle, EMM_EXITEDIT, 0, 0);
end;

procedure TBaseMatrix.EMMCancelEdit(var Msg: TMessage);
begin
  DoEditCancel;
end;

procedure TBaseMatrix.EMMEndEdit(var Msg: TMessage);
begin
  DoEditEnd;
end;

procedure TBaseMatrix.EMMExitEdit(var Msg: TMessage);
begin
  DoEditEnd(False);
end;

function TBaseMatrix.FindCachedHeight(aRow: Integer): Integer;
var
  i: Integer;
begin
  Result := -1;
  with fPageCache.Vert do
  begin
    if not Valid then
      raise Exception.Create('FindCachedHeight');
    i := FindInteger(Indices, aRow, VisibleCount - 1);
    if i >= 0 then
    begin
      Result := Sizes[i].B - Sizes[i].A + 1;
    end;
  end;
end;

function TBaseMatrix.FindCachedWidth(aCol: Integer): Integer;
var
  i: Integer;
begin
  Result := -1;
  with fPageCache.Horz do
  begin
    if not Valid then
      raise Exception.Create('FindCachedWidth');
    i := FindInteger(Indices, aCol, VisibleCount - 1);
    if i >= 0 then
    begin
      Result := Sizes[i].B - Sizes[i].A + 1;
    end;
  end;
end;

function TBaseMatrix.GetCellRect(aCol, aRow: integer): TRect;
begin
  CellRectToScreenRect(aCol, aRow, Result);
end;

(*function TBaseMatrix.GetCellStyle(aCol, aRow: Integer): TCellStyle;
begin
  if fColumns <> nil then
     Result := fColumns[aCol].fStyle;

  if Result = nil then
    if fRows <> nil then
      Result := fRows[aRow].fStyle;

  if Result = nil then
    Result := fCellStyle;

end;*)

function TBaseMatrix.GetCellStyleEx(aCol, aRow: Integer; var aMatrixStyle, aColStyle, aRowStyle: TCellStyle): TCellStyle;
var
  CustomResult: TCellStyle;
begin

  aMatrixStyle := fCellStyle;

  if fColumns <> nil
  then aColStyle := fColumns[aCol].fStyle
  else aColStyle := nil;

  if fRows <> nil
  then aRowStyle := fRows[aRow].fStyle
  else aRowStyle := nil;

  if aColStyle <> nil then
    Result := aColStyle
  else if aRowStyle <> nil then
    Result := aRowStyle
  else
    Result := fCellStyle;

  CustomResult := DoGetActiveStyle(aCol, aRow, aMatrixStyle, aColStyle, aRowStyle);
  if CustomResult <> nil then
    Result := CustomResult;
end;

function TBaseMatrix.GetCellText(aCol, aRow: Integer): string;
begin
end;

function TBaseMatrix.GetClientRect: TRect;
begin
  if Parent <> nil then
    Result := inherited GetClientRect
  else begin
    Result := BoundsRect;
  end;

{  Result.Left := 0;
  Result.Top := 0;
  Result.Right := Width - BorderWidth;
  Result.Bottom := Height - BorderWidth; }
//  Result := Width - BorderWidth
end;

function TBaseMatrix.GetColRect(aCol: Integer): TRect;
var
  PgInfo: TPageInfo;
  TopRect, BottomRect: TRect;
begin
  CheckCol(aCol);
  CalcPageInfo(PgInfo);
  if  CellRectToScreenRect(aCol, PgInfo.Vert.FirstVisible, TopRect)
  and CellRectToScreenRect(aCol, PgInfo.Vert.LastVisible, BottomRect) then
  begin
//    Include(fInternalPaintOptions, ipPaintCells);
//    Include(fInternalPaintOptions, ipPaintLines);
    Result.TopLeft := TopRect.TopLeft;
    Result.BottomRight := BottomRect.BottomRight;
  end;
end;

function TBaseMatrix.GetColRow: TPoint;
begin
  Result := Point(fCol, fRow);
end;

function TBaseMatrix.GetColumns: TMxColumns;
begin
  Result := fColumns;

  if Result = nil then
  begin

    //showmessage()
//    if not (uoColumns in fOptions.UseOptions) then
  //    showmessage('columns error');

    if CanAutoGenerateObjects
    and (cdRows in Options.CreateOnDemandOptions) then
    begin
      Options.UseOptions := Options.UseOptions + [uoColumns];
      Result := fColumns;
      {if Result = nil then
        raise Exception.Create('Design load error GetRows'); }
    end;
    //else raise Exception.Create('Runtime load error GetColumns');
  end;

  //EXIT;


(*  if (Result = nil)
  and (ComponentState * [csDesigning, csLoading] = []) then
  begin
    // load on demand
    Options.UseOptions := Options.UseOptions + [uoColumns];
    Result := fColumns;
  end;
*)

(*
  {-------------------------------------------------------------------------------
    Wanneer het streaming mechanisme columns aan het laden is, betekent dat
    dat ze opgeslagen zijn.
    Waarschijnlijk omdat de UseOptions nog niet gestreamed zijn
    zijn de columns nog niet gealloceerd en forceren we dat hier dus.
    TMatrixOptions.SetUseOptions zorgt ervoor dat de columns aangemaakt worden.
    Zie ook GetRows.
    Het wonderbaarlijke is dat deze truuk bij CellStyle niet lijkt te hoeven.
  -------------------------------------------------------------------------------}
  if Result = nil then
  begin
    // runtime
    if ComponentState * [csDesigning, csLoading] = [] then
    begin
      raise Exception.Create('GetColumn error');
    end;
    // designtime
    if csLoading in ComponentState then
    begin
      if not (uoColumns in Options.UseOptions) then
        raise Exception.Create('GetColumns Design Load error 1');
      {
        showmessage('loading nil columns vreemd gedrag')
      else
        showmessage('loading nil columns niks aan te doen?');
      }
      Options.UseOptions := Options.UseOptions + [uoColumns];
      Result := fColumns;
      if Result = nil then
        raise Exception.Create('GetColumns design load error 2');
    end;
  end;

  *)

end;

function TBaseMatrix.GetColWidth(aCol: Integer): Integer;
{-------------------------------------------------------------------------------
  GetColWidth:
  geeft waarde uit columns terug of defaultwidth
-------------------------------------------------------------------------------}
begin
  if uoColumns in Options.UseOptions then
    Result := fColumns[aCol].fSize
  else
    Result := fDefaultColWidth;
end;

procedure TBaseMatrix.GetData(aCol, aRow: Integer; out Data: TCellData);
begin
  DoGetCellData(aCol, aRow, Data);
end;

function TBaseMatrix.GetEffectiveHeaderLineWidth(aHeader: TMatrixHeader): Integer;
begin
  Result := aHeader.fLineWidth;
  case aHeader.fHeaderType of
    htLeft : if aHeader.Hidden or not (poHeaderVertLines in fOptions.fPaintOptions) then Result := 0;
    htTop  : if aHeader.Hidden or not (poHeaderHorzLines in fOptions.fPaintOptions) then Result := 0;
  end;
end;

function TBaseMatrix.GetEffectiveHorzLineWidth(aRow: Integer): Integer;
begin
  if aRow = fRowCount - 1 then
    if poHideLastHorzLine in Options.PaintOptions then
    begin
      Result := 0;
      Exit;
    end;

  if poHorzLines in Options.PaintOptions then
  begin
    Result := fDefaultHorzLineWidth;
    if uoRows in Options.UseOptions
    then Result := fRows[aRow].LineWidth
    else Result := fDefaultHorzLineWidth
  end
  else
    Result := 0;           //mxmisc
end;

function TBaseMatrix.GetEffectiveRowHeight(aRow: Integer; Calcing: Boolean = False): Integer;
{-------------------------------------------------------------------------------
  Geef rijhoogte terug. Er zijn een aantal gevallen mogelijk:
  1) gewoon de rijhoogte
  2) de rij is hidden: geef -1 terug
  3) De hoogte wordt door een event bepaald
  4) AutoHeight berekening.
     We zijn een pagina aan het berekenen en rijhoogten zijn dynamisch. In dit
     geval moeten we de kolommen doorlopen en de max-rijhoogte van zijn data
     teruggeven. Kortom: rijhoogtes worden vlak voor het tekenen berekend.
     NB: Wanneer hoogte berekend wordt moet de breedte bekend zijn.

  todo: mxrows nog verwerken
-------------------------------------------------------------------------------}
var
  h, x: Integer;
  aStyle: TCellStyle;
  Data: TCellData;
label
  _EXITPOINT;
begin
{  if fUseCache then
  begin
    Result := fGridCache.ProbeHeight(aRow);
    if Result <> INVALID_CACHE_SIZE then
      Exit;
  end; }

  Result := GetRowHeight(aRow);

  {if (uoRows in fOptions.fUseOptions)
  and (afHidden in fRows[aRow].fFlags) then
  begin
    Result := HIDDEN_HEIGHT;
    Exit;
  end;}

  if Assigned(fOnMeasureRow) then //###do
  begin
    fOnMeasureRow(Self, aRow, Result);
    goto _EXITPOINT;
  end;

  if not (csDesigning in ComponentState) then
  begin
    if (poCalculatedRowHeights in Options.PaintOptions) then
    begin
    if Calcing or ([isCalculatingPageInfo, isCalculatingVertInfo] * fInternalStates <> []) then
      Result := CalculateRowHeight(aRow);
    end;
  end;

  _EXITPOINT:
{    if fUseCache then
      fGridCache.CacheHeight(aRow, Result); }

end;

function TBaseMatrix.GetEffectiveVertLineWidth(aCol: Integer): Integer;
begin
  if aCol = fColCount - 1 then
  begin
    if poHideLastVertLine in Options.PaintOptions then
    begin
      Result := 0;
      Exit;
    end
{    else if (poHidePartialVertLines in Options.PaintOptions)
    and (aoAutoAdjustLastColumn in Options.AutoSizeOptions) then
    begin
      Result := 0;
      Exit;
    end; }
  end;

  if poVertLines in Options.PaintOptions then
  begin
    {if coColInfo in Options.UseOptions
    then Result := TColInfo(fColInfoList[aCol]).fLineSize
    else Result := fDefaultVertLineWidth}
    if uoColumns in Options.UseOptions
    then Result := fColumns[aCol].LineWidth
    else Result := fDefaultVertLineWidth
  end
  else
    Result := 0;
end;

function TBaseMatrix.GetLeftHeaders: TMatrixHeaders;
begin
  Result := fLeftHeaders;
  if Result = nil then
  begin
    if CanAutoGenerateObjects
    and (cdHeaders in Options.CreateOnDemandOptions) then
    begin
      Options.UseLeftHeaders := True;
      Result := fLeftHeaders;
    end;
  end;
end;

function TBaseMatrix.GetRightHeaders: TMatrixHeaders;
begin
  Result := fRightHeaders;
  if Result = nil then
  begin
    if CanAutoGenerateObjects and
    (cdHeaders in Options.CreateOnDemandOptions) then
    begin
      Options.UseRightHeaders := True;
      Result := fRightHeaders;
    end;
  end;
end;

function TBaseMatrix.GetRowHeight(aRow: Integer): Integer;
var
  h, i, x: Integer;
begin
  if uoRows in Options.UseOptions then
    Result := fRows[aRow].fSize
  else
    Result := fDefaultRowHeight;
end;

function TBaseMatrix.GetRowRect(aRow: Integer): TRect;
var
  PgInfo: TPageInfo;
  LeftRect, RightRect: TRect;
begin
  CheckRow(aRow);
  CalcPageInfo(PgInfo);
  if  CellRectToScreenRect(PgInfo.Horz.FirstVisible, aRow, LeftRect)
  and CellRectToScreenRect(PgInfo.Horz.LastVisible, aRow, RightRect) then
  begin
    Result.TopLeft := LeftRect.TopLeft;
    Result.BottomRight := RightRect.BottomRight;
  end;
end;

function TBaseMatrix.GetRows: TMxRows;
begin
  Result := fRows;

  if Result = nil then
  begin
    if CanAutoGenerateObjects
    and (cdRows in Options.CreateOnDemandOptions) then
    begin
      Options.UseOptions := Options.UseOptions + [uoRows];
      Result := fRows;
      {if Result = nil then
        raise Exception.Create('Design load error GetRows'); }
    end
//    else raise Exception.Create('Runtime load error GetRows');
  end;
(*
  { zie opm. GetColumns }
  if Result = nil then
    if csLoading in ComponentState then
    begin
      Options.UseOptions := Options.UseOptions + [uoRows];
      Result := fRows;
      if Result = nil then
        raise Exception.Create('Design load error GetRows');
    end;
    *)
end;

function TBaseMatrix.GetTopHeaders: TMatrixHeaders;
begin
  Result := fTopHeaders;
  if Result = nil then
  begin
    if CanAutoGenerateObjects and
    (cdHeaders in Options.CreateOnDemandOptions) then
    begin
      Options.UseTopHeaders := True;
      Result := fTopHeaders;
    end;
  end;
end;

function TBaseMatrix.GetVisibleRows: Integer;
var
  pgInfo: TPageInfo;
begin
  if not ValidPageInfo then
     CalcPageInfo(PgInfo);
  Result := fPageCache.Vert.VisibleCount
end;

function TBaseMatrix.GotoFirstCol(var aNewCol: Integer): Boolean;
begin
  if fCol <> 0 then
  begin
    aNewCol := 0;
    Result := True;
  end
  else Result := False;
end;

function TBaseMatrix.GotoFirstRow(var aNewRow: Integer): Boolean;
begin
  if fRow <> 0 then
  begin
    aNewRow := 0;
    Result := True;
  end
  else Result := False;
end;

function TBaseMatrix.GotoLastCol(var aNewCol: Integer): Boolean;
begin
  if fCol <> fColCount - 1 then
  begin
    aNewCol := fColCount - 1;
    Result := True;
  end
  else Result := False;
end;

function TBaseMatrix.GotoLastRow(var aNewRow: Integer): Boolean;
begin
  if fRow <> fRowCount - 1 then
  begin
    aNewRow := fRowCount - 1;
    Result := True;
  end
  else Result := False;
end;

procedure TBaseMatrix.HandleScrollBarCommand(aScrollBar, aScrollCode, aPos: Cardinal);

    {-------------------------------------------------------------------------------
      local om beperking van 16 bits message te omzeilen.
    -------------------------------------------------------------------------------}
    function GetRealScrollPosition(aScrollBar: Integer): Integer;
    var
      SI: TScrollInfo;
      Code: Integer;

    begin
      SI.cbSize := SizeOf(TScrollInfo);
      SI.fMask := SIF_TRACKPOS;
      Code := aScrollBar;
      GetScrollInfo(Handle, Code, SI);
      Result := SI.nTrackPos;
    end;

begin
  case aScrollBar of
    SB_VERT:
      begin
//        Log([GetRealScrollPosition(SB_VERT), frowcount-1]);
//        if ascrollcode = sb_bottom then Log(['bottom']);
        case aScrollCode of
          SB_LINEDOWN      : DoNavigate(srScrollBar, scDown);
          SB_LINEUP        : DoNavigate(srScrollBar, scUp);
          SB_THUMBTRACK    : DoNavigate(srScrollBar, scAbsolute, hfpAuto, vfpAuto, fCol, GetRealScrollPosition(SB_VERT));
//          SB_THUMBPOSITION : DoNavigate(srScrollBar, scAbsolute, hfpAuto, vfpAuto, fCol, aPos);
          SB_PAGEUP        : DoNavigate(srScrollBar, scPgUp);
          SB_PAGEDOWN      : DoNavigate(srScrollBar, scPgDn);
        end;
      end;
    SB_HORZ:
      begin
        case aScrollCode of
          SB_LINELEFT      : DoNavigate(srScrollBar, scLeft);
          SB_LINERIGHT     : DoNavigate(srScrollBar, scRight);
          SB_THUMBTRACK    : DoNavigate(srScrollBar, scAbsolute, hfpAuto, vfpAuto, GetRealScrollPosition(SB_VERT), fRow);
//          SB_THUMBPOSITION : DoNavigate(srScrollBar, scAbsolute, hfpAuto, vfpAuto, aPos, fRow);
//          SB_PAGELEFT
//          SB_PAGERIGHT = 3;
        end;
      end;
  end; // case aScollBar
         { SB_LINEDOWN = 1;
          SB_PAGEUP = 2;
          SB_PAGELEFT = 2;
          SB_PAGEDOWN = 3;
          SB_PAGERIGHT = 3;
          SB_THUMBPOSITION = 4;
          SB_THUMBTRACK = 5;
          SB_TOP = 6;
          SB_LEFT = 6;
          SB_BOTTOM = 7;
          SB_RIGHT = 7;
          SB_ENDSCROLL = 8;
          }
end;

procedure TBaseMatrix.HeadersChanged(Sender: TObject);
begin
  if csDestroying in ComponentState then
    Exit;

  if (fTopHeaders = nil) or (fTopHeaders.Count = 0) or (fTopHeaders.Size = 0)
  then StateLeave(isVisibleTopHeaders)
  else StateEnter(isVisibleTopHeaders);

  if (fLeftHeaders = nil) or (fLeftHeaders.Count = 0) or (fLeftHeaders.Size = 0)
  then StateLeave(isVisibleLeftHeaders)
  else StateEnter(isVisibleLeftHeaders);

  if (fRightHeaders = nil) or (fRightHeaders.Count = 0)
  then StateLeave(isVisibleRightHeaders)
  else StateEnter(isVisibleRightHeaders);

  if InStates([isVisibleTopHeaders, isVisibleLeftHeaders, isVisibleRightHeaders])
  then StateEnter(isVisibleHeaders)
  else StateLeave(isVisibleHeaders);

  UpdateGridRect;
  UpdateAutoBounds;
  Invalidate;
end;

function TBaseMatrix.HitTest(X, Y: Integer; out aCell: TPoint; out aCellRect: TRect): Boolean;

    procedure DoTest(aCol, aRow: Integer; const aRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far;
    var
      R: TRect;
    begin
      R := aRect;
      Inc(R.Right, VertLn);
      Inc(R.Bottom, HorzLn);
      Stop := PointInRect(X, Y, R);
      Result := Stop;
      if Result then
      begin
        aCell := Point(aCol, aRow);
        aCellRect := aRect;
      end;
    end;

var
  PgInfo: TPageInfo;
begin
  Result := False;
  CalcPageInfo(PgInfo);
  TraverseVisibleCells(@DoTest, PgInfo, AllCells);
end;

function TBaseMatrix.HitTestEx(X, Y: Integer; out aHitCol: Integer;
                                              out aHitRow: Integer;
                                              out aCellRect: TRect;
                                              out aHeaders: TMatrixHeaders;
                                              out aHeaderIndex: Integer;
                                              out aArea: TMatrixArea;
                                              out aLineHits: TLineHits): Boolean;
{-------------------------------------------------------------------------------
  Deze methode geeft informatie terug over een coordinaat van het grid.
-------------------------------------------------------------------------------}

    procedure DoTestGrid(aCol, aRow: Integer; const aRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far;
    var
      R: TRect;
    begin
      R := aRect;
      Inc(R.Right, VertLn);
      Inc(R.Bottom, HorzLn);
      Stop := PointInRect(X, Y, R);
      Result := Stop;

      { check lijn }
      if Stop then
      begin
        if Between(X, R.Right - fLineTreshold, R.Right + HorzLn + fLineTreshold) then Include(aLineHits, lhVert);
        if Between(Y, R.Bottom - fLineTreshold, R.Bottom + VertLn + fLineTreshold) then Include(aLineHits, lhHorz);
      end;

      if Result then
      begin
        aHitCol := aCol;//Cell := Point(aCol, aRow);
        aHitRow := aRow;
        aCellRect := aRect;
      end;
    end;

    procedure DoCol(aCol, aLeft, aRight, aLineWidth: Integer; var Info: DWORD{Stop: Boolean}); far;
    // zoek col
    var
      i: Integer;
      TestY: Integer;
      H: TMatrixHeader;
    begin
      //Stop := X <= aRight + aLineWidth;
      //Result := Stop;
      if X <= aRight + aLineWidth then Info := Info or TRAVERSE_BREAK;
      Result := Info and TRAVERSE_BREAK <> 0;

      if Result then
      begin

        // zoek header index
        TestY := 0;
        for i := 0 to aHeaders.Count - 1 do
        begin
          H := aHeaders[i];
          if Y <= TestY + H.fSize + H.fLineWidth then
          begin
            aHeaderIndex := i;
            aHitCol := aCol;
            //aCell := Point(aCol, aHeaderIndex);
            aCellRect := Rect(aLeft, TestY, aRight + aLineWidth, TestY + H.fSize + H.fLineWidth);
            //Log(['index = ' , i]);
            Exit;
          end;
          Inc(TestY, H.Size + H.LineWidth);
        end;
        Result := False;
      end;
    end;

    procedure DoRow(aRow, aTop, aBottom, aLineWidth: Integer; var Info: DWORD); far;
    // zoek col
    var
      i: Integer;
      TestX: Integer;
      H: TMatrixHeader;
    begin
      if Y <= aBottom + aLineWidth then Info := Info or TRAVERSE_BREAK;
      Result := Info and TRAVERSE_BREAK <> 0;//Stop;

      if Result then
      begin

        // zoek header index
        TestX := 0;
        for i := 0 to aHeaders.Count - 1 do
        begin
          H := aHeaders[i];
          if X <= TestX + H.fSize + H.fLineWidth then
          begin
            aHeaderIndex := i;
            aHitRow := aRow;//Cell := Point(aHeaderIndex, aRow);
            //aCellRect := Rect(aLeft, TestY, aRight + aLineWidth, TestY + H.fSize + H.fLineWidth);
            aCellRect := Rect(TestX, aTop, TestX + H.fSize + H.fLineWidth, aBottom + aLineWidth);
            //Log(['index = ' , i]);
            Exit;
          end;
          Inc(TestX, H.Size + H.LineWidth);
        end;
        Result := False;
      end;
    end;


    function FindArea: TMatrixArea;
    begin
      if not InState(isVisibleHeaders) then
      begin
        Result := maGrid;
        Exit;
      end;
      for Result := Low(TValidMatrixArea) to High(TValidMatrixArea) do
        if fAreaRects[Result] <> nil then
          if PointInRect(X, Y, fAreaRects[Result]^) then
          begin
            Exit;
          end;
      Result := maNone;
       //raise Exception.Create('area niet gevonden');
    end;

var
  PgInfo: TPageInfo;
  Area: TMatrixArea;
begin
  Result := False;
  aHitCol := -1;
  aHitRow := -1;
  aCellRect := EmptyRect;
  aHeaders := nil;
  aHeaderIndex := -1;
  aArea := maNone;
  aLineHits := [];

  CalcPageInfo(PgInfo);
  Area := FindArea;
  aArea := Area;

//  Log([getenumname(typeinfo(tmatrixarea), integer(area))]);

  case Area of
    maGrid:
      begin
        TraverseVisibleCells(@DoTestGrid, PgInfo, AllCells);
      end;
    maTopLeft:
      begin
      end;
    maTop:
      begin
        aHeaders := fTopHeaders;
        TraverseVisibleCols(@DoCol, PgInfo.Horz, AllAxis);
      end;
//    maTopRight:
    maLeft:
      begin
        aHeaders := fLeftHeaders;
        TraverseVisibleRows(@DoRow, PgInfo.Vert, AllAxis, AllWindow);
      end;
//      TraverseVisibleCols(@DoCol, PgInfo, AllAxis);
{    maRight:
    maBottomLeft:
    maBottom:
    maBottomRight: }
  end;
end;

function TBaseMatrix.HitTestResize(X, Y: Integer; out aCell: TPoint; out aCellRect: TRect; out aLineHits: TLineHits): Boolean;

    procedure DoTest(aCol, aRow: Integer; const aRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far;
    var
      R: TRect;
    begin
      R := aRect;
      Inc(R.Right, VertLn + 2);
      Inc(R.Bottom, HorzLn + 2);
      Stop := PointInRect(X, Y, R);
      { check lijn }
      if Stop then
      begin
        if Between(X, R.Right - 4 - HorzLn, R.Right) then Include(aLineHits, lhVert);
        if Between(Y, R.Bottom - 4 - VertLn, R.Bottom) then Include(aLineHits, lhHorz);
      end;
      Result := aLineHits <> [];
      if Result then
      begin
        aCellRect := aRect;
        aCell := Point(aCol, aRow);
        //aCellRect := aRect;
      end;
    end;

var
  PgInfo: TPageInfo;
begin
  Result := False;
  CalcPageInfo(PgInfo);
  aLineHits := [];
  TraverseVisibleCells(@DoTest, PgInfo, AllCells);
end;

procedure TBaseMatrix.IncrementalSearch;
var
  Y, StartY: Integer;
  Data: TCellData;
  S: string;
  Circular: Boolean;
begin
  S := fSearchString;
  Circular := False;
  StartY := Succ(fRow);


  if S = '' then
    Exit;

//  Log([s]);
//    S := fSearchChar;

  { zoek verder }
  for y := StartY to RowCount - 1 do
  begin
    Data := nil;
    DoGetCellData(fCol, y, Data);
    if Data <> nil then
    begin
      if Pos(UpperCase(S), UpperCase(Data.Text)) = 1 then
      begin
        Row := y;
        Exit;
      end;
    end;
  end;

  { als niet gevonden, zoek vanaf begin }
  for y := 0 to fRow - 1 do
  begin
    Data := nil;
    DoGetCellData(fCol, y, Data);
    if Data <> nil then
    begin
      if Pos(UpperCase(S), UpperCase(Data.Text)) = 1 then
      begin
        Row := y;
        Exit;
      end;
    end;
  end;

end;


procedure TBaseMatrix.IncrementalSearchUpdate(aKey: Char = #0; one:boolean=false);
var
  Len: Integer;
begin
  TimerStop(CLEARSEARCH_TIMER);

  if aKey = #0 then
  begin
    fSearchString := '';
  end
  else if aKey in SearchChars then
  begin
    if aKey = #8 then
      fSearchString := CutLeft(fSearchString, 1)
    else begin
      fSearchString := fSearchString + aKey
    end;

///    if gettickcount - ttt < 50 then
   ///   fSearchString := aKey;

//    ttt:=gettickcount;

(*    Len := Length(fSearchString);
    { forceer string op 1 als 3 dezelfde letters }
    if Length(fSearchString) >= 3 then
    begin
      if fSearchString[Len] = fSearchString[Len - 1] then
        if fSearchString[Len] = fSearchString[Len - 2] then
        begin
          fSearchString := fSearchString[Len];
//          Log(['force 1']);
        end;
    end; *)

    Len := Length(fSearchString);
    { forceer string op 1 als 3 dezelfde letters }
    if Length(fSearchString) >= 2 then
    begin
      if fSearchString[Len] = fSearchString[Len - 1] then
        begin
          fSearchString := fSearchString[Len];
//          Log(['force 1']);
        end;
    end;   

  end;

  IncrementalSearch;  // thread?

//  if not One then
    TimerStart(CLEARSEARCH_TIMER);
//  Log([fsearchstring]);

end;

function TBaseMatrix.InState(aState: TInternalState): Boolean;
begin
  Result := aState in fInternalStates;
end;

function TBaseMatrix.InStates(aStates: TInternalStates): Boolean;
begin
  Result := aStates * fInternalStates <> [];
end;

function TBaseMatrix.AllStates(aStates: TInternalStates): Boolean;
begin
  Result := aStates * fInternalStates = aStates;
end;

procedure TBaseMatrix.InvalidateHoveringCell;
begin
//  if Valid
end;

procedure TBaseMatrix.InternalSetCol(aCol: Integer; ChangeResult: Boolean);
{-------------------------------------------------------------------------------
  Verplichte interne routine om Col te zetten. Er wordt gekeken of
  er een CellExit plaats moet vinden.
-------------------------------------------------------------------------------}
begin
//  Log(['colchanged']);
  with fNavigationInfo do
  begin
    if not nCellExited then
    if ChangeResult then
    begin
      DoCellExit(nOldCol, nOldRow);
      nCellExited := True;
    end;
  end;
  fCol := aCol;
end;

procedure TBaseMatrix.InternalSetRow(aRow: Integer; ChangeResult: Boolean);
{-------------------------------------------------------------------------------
  Zie InternalSetCol
-------------------------------------------------------------------------------}
begin
//  Log(['rowchanged']);
  with fNavigationInfo do
  begin
    if not nCellExited then
    if ChangeResult then
    begin
//      if DataMode = dmVirtual
      DoCellExit(nOldCol, nOldRow);
      nCellExited := True;
    end;
  end;
  fRow := aRow;
end;

procedure TBaseMatrix.Invalidate;
begin
  //if not CanPaint then Exit;
  InvalidatePageInfo;
  //CacheClientSize;
  inherited Invalidate;
end;

procedure TBaseMatrix.InvalidateCell(aCol, aRow: Integer; DirectUpdate: Boolean = False);
var
  R: TRect;
begin
  if not CanPaint then Exit;
  if not ValidCoord(aCol, aRow) then Exit;

  //##autoheights
{  if poCalculatedRowHeights in Options.PaintOptions then
    Invalidate
  else }
  if CellRectToScreenRect(aCol, aRow, R) then
  begin
    { #windows rect shit: niet leuk! }
    Inc(R.Right);
    Inc(R.Bottom);
//    Include(fInternalPaintOptions, ipPaintCells);
    Windows.InvalidateRect(Handle, @R, False);
    if DirectUpdate then Update;
  end
//  else Log(['outside', acol, arow]);
end;

procedure TBaseMatrix.InvalidateTopHeaders;
begin
  if not CanPaint then Exit;
  InvalidateRect(Handle, @fHeaderAreas.hTop, False);
end;

procedure TBaseMatrix.InvalidateHeader(aHeaders: TMatrixHeaders; aCol, aRow, aHeaderIndex: Integer; const aRect: TRect; Down: Boolean);
begin
//  Log(['headerclick']);
  if aHeaders = nil then Exit;
  InvalidateRect(Handle, @aRect, False);
  Update;
end;

procedure TBaseMatrix.InvalidateCol(aCol: Integer; DirectUpdate: Boolean);
var
  R: TRect;
begin
  if not CanPaint then Exit;
  R := GetColRect(aCol);
  { #windows rect shit }
  Inc(R.Right);
  Inc(R.Bottom);
  Windows.InvalidateRect(Handle, @R, False);
  if DirectUpdate then Update;
end;

procedure TBaseMatrix.InvalidateMouseInfo;
begin
  FillChar(fMouseInfo, SizeOf(fMouseInfo), 0);
end;

procedure TBaseMatrix.InvalidatePageInfo;
begin
//  Log(['inv']);
  with fPageCache do
  begin
    Horz.Valid := False;
    Vert.Valid := False;
  end;
end;

procedure TBaseMatrix.InvalidateRow(aRow: Integer; DirectUpdate: Boolean);
var
  R: TRect;
begin
  if not CanPaint then Exit;
  R := GetRowRect(aRow);
 { #windows rect shit }
  Inc(R.Right);
  Inc(R.Bottom);
  Windows.InvalidateRect(Handle, @R, False);
  if DirectUpdate then Update;
end;

procedure TBaseMatrix.InvalidateRowCache;
begin
  with fRowCache do
  begin
    Valid := False;
    First := -1;
    Last := -1;
  end;
end;

procedure TBaseMatrix.InvalidateSelection;
begin
  if fSelection.Left < 0 then
    Exit;
  fSelection := ErrorRect;
  Invalidate;
end;

function TBaseMatrix.IsActiveControl: Boolean;
var
  H: Hwnd;
  ParentForm: TCustomForm;
begin
  { check active control: kopie Borland grids unit. zie ook mousedown }
  Result := False;
  ParentForm := GetParentForm(Self);
  if Assigned(ParentForm) then
  begin
    if (ParentForm.ActiveControl = Self) then
      Result := True
  end
  else
  begin
    H := GetFocus;
    while IsWindow(H) and (Result = False) do
    begin
      if H = WindowHandle then
        Result := True
      else
        H := GetParent(H);
    end;
  end;
end;

function TBaseMatrix.IsScrollKey(Key: Word; Shift: TShiftState): Boolean;
begin
  Result := False;
  if Shift = [] then
  begin
    case Key of
      VK_DOWN   : Result := True;
      VK_UP     : Result := True;
      VK_RIGHT  : Result := True;
      VK_LEFT   : Result := True;
      VK_NEXT   : Result := True;
      VK_PRIOR  : Result := True;
      VK_HOME   : Result := True;
      VK_END    : Result := True;
    end;
  end
  else if Shift = [ssCtrl] then
  begin
    case Key of
      //VK_DOWN   : Command := scDown;
      //VK_UP     : Command := scUp;
      //VK_RIGHT  : Command := scRight;
      //VK_LEFT   : Command := scLeft;
      VK_NEXT   : Result := True;
      VK_PRIOR  : Result := True;
      VK_HOME   : Result := True;
      VK_END    : Result := True;
    end;
  end

end;


function TBaseMatrix.IsStoredColumns: Boolean;
begin
  Result := (fColumns <> nil) and (uoColumns in Options.UseOptions);
end;

function TBaseMatrix.IsStoredRows: Boolean;
begin
  Result := fRows <> nil
end;

procedure TBaseMatrix.KeyDown(var Key: word; Shift: TShiftState);
var
  Command: TScrollCommand;
  Block: Boolean;
begin
  inherited;
  if Key = 0 then Exit;
//  TimerStop(CLEARSEARCH_TIMER);
  Command := scNone;
  Block := False;

  {-------------------------------------------------------------------------------
     genereer navigatie commando
  -------------------------------------------------------------------------------}
  if Shift = [] then
  begin
    case Key of
      VK_F2     : DoEditBegin;
      VK_DOWN   : Command := scDown;
      VK_UP     : Command := scUp;
      VK_RIGHT  : Command := scRight;
      VK_LEFT   : Command := scLeft;
      VK_NEXT   : Command := scPgDn;
      VK_PRIOR  : Command := scPgUp;
      VK_HOME   : Command := scHorzStart;
      VK_END    : Command := scHorzEnd;
    end;
  end
  else if Shift = [ssShift] then
  begin
    case Key of
      VK_DOWN   : Command := scDown;
      VK_UP     : Command := scUp;
      VK_RIGHT  : Command := scRight;
      VK_LEFT   : Command := scLeft;
      VK_NEXT   : Command := scPgDn;
      VK_PRIOR  : Command := scPgUp;
      VK_HOME   : Command := scHorzStart;
      VK_END    : Command := scHorzEnd;
    end;
  end
  else if Shift = [ssCtrl] then
  begin
    case Key of
      //VK_DOWN   : Command := scDown;
      //VK_UP     : Command := scUp;
      //VK_RIGHT  : Command := scRight;
      //VK_LEFT   : Command := scLeft;
      VK_NEXT   : Command := scVertEnd;
      VK_PRIOR  : Command := scVertStart;
      VK_HOME   : Command := scBegin;
      VK_END    : Command := scEnd;
      Ord('K')  : begin Block := True; UpdateBlockCommand('k'); end;
      Ord('B')  : begin Block := True; UpdateBlockCommand('b');  end;
    end;
  end
  else if Shift = [ssShift, ssCtrl] then
  begin
    case Key of
      VK_RIGHT:
        if uoColumns in Options.UseOptions then
          ColWidths[fCol] := ColWidths[fCol] + 4;
      VK_LEFT:
        if uoColumns in Options.UseOptions then
          ColWidths[fCol] := ColWidths[fCol] - 4;
      VK_DOWN:
        if uoRows in Options.UseOptions then
          RowHeights[fRow] := RowHeights[fRow] + 4;
    end;
  end;

  if Command <> scNone then
    DoNavigate(srKeyBoard, Command);

  if not Block then
    if Key <> 0 then
      UpdateBlockCommand;
{  if Shift = [ssShift] then
    UpdateSelection(fCol, fRow); }
    //Log(['extend selection']);


end;

procedure TBaseMatrix.KeyPress(var Key: Char);
begin
  inherited;
  if soIncrementalSearch in Options.SearchOptions then
    if Key in SearchChars then
      IncrementalSearchUpdate(Key{, True});
end;

procedure TBaseMatrix.KeyUp(var Key: word; Shift: TShiftState);
begin
  inherited;
//  if Shift = [ssCtrl] then if Key = 0 then UpdateBlockCommand(#0);
//  if shift=[] then
//  if Chr(Key) in SearchChars then
  //  IncrementalSearchUpdate(Chr(Key), False);
end;

procedure TBaseMatrix.HandleMouseResize;
{-------------------------------------------------------------------------------
  Check kolom/rij/cellresize tijdens mousedown/mousemove.
  MouseInfo is gevuld door de MouseXXX routines.
-------------------------------------------------------------------------------}

    procedure InitState(const aState: TMouseStateInfo);
    begin
      with fMouseInfo, aState do
      begin
        if mLineHits = [] then
          ResizeState := mdNone
        else if mLineHits = [lhHorz] then
          ResizeState := mdRowSizing
        else if mLineHits = [lhVert] then
          ResizeState := mdColSizing
        else if mLineHits = [lhHorz, lhVert] then
          ResizeState := mdCellSizing;
      end;
    end;

begin
  with fMouseInfo do
  begin
    case ProcInfo of
      ctDown:
        with DownInfo do
        begin
          if ResizeState = mdNone then
            InitState(DownInfo);
          case ResizeState of
            mdColSizing, mdRowSizing, mdCellSizing:
              begin
                if mHitCol >= 0 then
                  StartingWidth := ColWidths[mHitCol];
                if mHitRow >= 0 then
                  StartingHeight := RowHeights[mHitRow];
              end;
          end;
        end;
      ctMove:
        with MoveInfo do
        begin
          if ResizeState = mdNone then
            InitState(DownInfo);
        end;
      ctUp:
        with UpInfo do
        begin

        end;
    end;
  end;
end;

procedure TBaseMatrix.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
//var
//  GridHit, Hit: Boolean;
//  HittedCell: TPoint;
//  R: TRect;
//  aHeaders: TMatrixHeaders;
//  aHeaderIndex: Integer;
//  aArea: TValidMatrixArea;
begin

  if not Focused and CanFocus then
    Windows.SetFocus(Handle);

  { focus matrix: kopie Borland grids unit }

  (*
  if not (csDesigning in ComponentState) and
    (CanFocus or (GetParentForm(Self) = nil)) then
  begin
    SetFocus;
    if not IsActiveControl then
    begin
      MouseCapture := False;
      Exit;
    end;
  end;
  *)

  {-------------------------------------------------------------------------------
    vul mouseinfo
  -------------------------------------------------------------------------------}
  with fMouseInfo, DownInfo do
  begin
    ProcInfo := ctDown; // belangrijk

    mMouseCoord := Point(X, Y);
    ShiftState := Shift;
    mHitResult := HitTestEx(X, Y, mHitCol, mHitRow, mHitRect, mHeaders, mHeaderIndex, mArea, mLineHits);

    if not mHitResult then
      Exit;
    if mArea <> maGrid then
      Exit;

    {-------------------------------------------------------------------------------
      Left button
      a) focuscell
      b) check dblclick cell
    -------------------------------------------------------------------------------}

    if Button = mbLeft then
    begin
      if mArea = maGrid then
        if (mHitCol <> fCol) or (mHitRow <> fRow) then
          DoNavigate(srClick, scAbsolute, hfpAuto, vfpAuto, mHitCol, mHitRow);

      HandleMouseResize;

      if ResizeState = mdNone then
        if mArea = maGrid then
          if ssDouble in Shift then
            DoCellDblClick(mHitCol, mHitRow);
     end;

  end;

  inherited;
end;

procedure TBaseMatrix.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  CellChanged: Boolean;
  PrevInfo: TMouseStateInfo;
//  OldX, OldY: Integer;
//  PrevState: TMouseDragState;
//  HeaderMode: THeaderMode;
begin
  inherited MouseMove(Shift, X, Y);

  with fMouseInfo, MoveInfo do
  begin
    PrevInfo := MoveInfo;
    ProcInfo := ctMove;

    mMouseCoord := Point(X, Y);
    ShiftState := Shift;
    mHitResult := HitTestEx(X, Y, mHitCol, mHitRow, mHitRect, mHeaders, mHeaderIndex, mArea, mLineHits);


    {-------------------------------------------------------------------------------
      resize check
    -------------------------------------------------------------------------------}
    if ResizeState <> mdNone then
    begin
      HandleMouseResize;
      Exit;
    end;

//    if mlinehits <> [] then
//    Log([lhHorz in mLineHits, lhvert in mlinehits]);

    { check hit }
    //CellHit := HitTest(X, Y, Cell, CellRect);
    CellChanged := (mHitCol <> PrevInfo.mHitCol) or (mHitRow <> PrevInfo.mHitRow);
//    if Shift = [] then
    if CellChanged then
    begin
      //DoHoverCell(PrevInfo.mHitCol, PrevInfo.mHitRow, True);
      //DoHoverCell(mHitCol, mHitRow, False);
      if poHoveringCells in Options.PaintOptions then
      begin
        InvalidateCell(PrevInfo.mHitCol, PrevInfo.mHitRow, True);
        InvalidateCell(mHitCol, mHitRow, True);
        //Log([mhitcol,mhitrow]);
      end;
      //Log([mhitcol,mhitrow]);
    end;
    {todo: hier is de cellrect exact bekend-->hoeft ie niet opnieuw uitgerekend te worden
     -->paintcells kan ook restricted}

    //not EqualPoints(Point(mHitCol, mHitRow), Point(PrevIn));
    //mMoveOverCell := Cell;


{
    if CellHit then
    begin
      Include(mClickInfo, ciDownCell);
      mDownCell := Cell;
    end
    else begin
      Exclude(mClickInfo, ciDownCell);
      mDownCell := ErrorPoint;
    end; }

    if not mHitResult then
    begin
      if not (csDesigning in ComponentState) then
        Application.CancelHint
    end
    else if CellChanged then
      if ValidCoord(mHitCol, mHitRow) then
        DoCellHint(mHitCol, mHitRow);

  end;

(*

    {-------------------------------------------------------------------------------
       bepaal initiele resizestate
    -------------------------------------------------------------------------------}
    if Shift * [ssLeft, ssRight, ssMiddle, ssDouble] = [] then
    if not (mState in [mdColSizing, mdRowSizing, mdCellSizing]) then
    with fOptions do
      if fSizingOptions <> [] then
      begin
        //HeaderMode := GetHeaderMode(Cell);
        ResizeCellHit := HitTestResize(X, Y, ResizeCell, ResizeCellRect, ResizeHit);
        if not ResizeCellHit then
        begin
          mState := mdNone;
        end
        else begin
          //if (HeaderMode <> [])
          //or ((HeaderMode = []) and not (soHeaderSizingOnly in fSizingOptions)) then
          begin
            { resizing col available }
            if (ResizeHit = [lhVert])
            and (soColSizing in fSizingOptions)
            and (uoColumns in fUseOptions) then
              mState := mdColSizingPossible
            { resizing row available }
            else if (ResizeHit = [lhHorz])
            and (soRowSizing in fSizingOptions)
            and (uoRows in fUseOptions) then
              mState := mdRowSizingPossible
            { resizing cell available }
            else if (ResizeHit = [lhHorz, lhVert])
            and (soCellSizing in fSizingOptions)
            and (uoColumns in fUseOptions)
            and (uoRows in fUseOptions) then
              mState := mdCellSizingPossible;
          end

        end;
      end;

    { zet cursor }
    if PrevState <> mState then
    case mState of
      mdNone:
        begin
          //TimerStop(CURSOR_TIMER)
        end;
      mdColSizingPossible:
        begin
          mResizeCell := ResizeCell;
          mResizeRect.Left := ResizeCellRect.Left;
          mResizeRect.Top := ResizeCellRect.Top;
          //if GetHeaderMode(ResizeCell) = [] then
            TimerStart(CURSOR_TIMER);
        end;
      mdRowSizingPossible:
        begin
          mResizeCell := ResizeCell;
          mResizeRect.Left := ResizeCellRect.Left;
          mResizeRect.Top := ResizeCellRect.Top;
//          if GetHeaderMode(ResizeCell) = [] then
            TimerStart(CURSOR_TIMER);
        end;
      mdCellSizingPossible:
        begin
          mResizeCell := ResizeCell;
          mResizeRect.Left := ResizeCellRect.Left;
          mResizeRect.Top := ResizeCellRect.Top;
//          if GetHeaderMode(ResizeCell) = [] then
            TimerStart(CURSOR_TIMER);
        end;
    end;

    { paint sizing lines }
    case mState of
      mdColSizing:
        begin
          TimerStop(CURSOR_TIMER);
          mResizeRect.Right := mMove.x;
          if soLiveSizing in fOptions.fSizingOptions then
            ColWidths[mResizeCell.x] := mStartColWidth + mMove.x - mDown.x
          else
            PaintSizingLines(X, Y, False);
        end;
      mdRowSizing:
        begin
          {
          TimerStop(CURSOR_TIMER);
          mResizeRect.Bottom := mMove.y;
          if soLiveSizing in fOptions.fSizingOptions then
            RowHeights[mResizeCell.y] := mStartRowHeight + mMove.y - mDown.y
          else
            PaintSizingLines(X, Y, False);
          }
        end;
      mdCellSizing:
        begin
          {
          TimerStop(CURSOR_TIMER);
          mResizeRect.Right := miMove.x;
          mResizeRect.Bottom := miMove.y;
          if soLiveSizing in fOptions.fSizingOptions then
          begin
            ColWidths[miResizeCell.x] := miStartColWidth + miMove.x - miDown.x;
            RowHeights[miResizeCell.y] := miStartRowHeight + miMove.y - miDown.y
          end
          else
            PaintSizingLines(X, Y, False);
          }
        end;
      mdRectSelectingStart, mdRectSelecting:
        begin
          mState := mdRectSelecting;
//        PaintSelectionRect(Rect(miDown.x, miDown.y, miMove.x, miMove.y));
        end;
    end; // case



  //miPrevMove := miMove;
  end;

  *)

end;

procedure TBaseMatrix.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  CellHit: Boolean;
  Cell: TPoint;
  CellRect: TRect;
  LineHit: TLineHit;
//  HeaderMode: THeaderMode;
begin

  try
  inherited;
//  Log(['mouseup']);

  with fMouseInfo, UpInfo do
  begin
    ProcInfo := ctUp;
    mMouseCoord := Point(X, Y);
    ShiftState := Shift;
    mHitResult := HitTestEx(X, Y, mHitCol, mHitRow, mHitRect, mHeaders, mHeaderIndex, mArea, mLineHits);
    case DownInfo.mArea of
      maTop: // topheaderclick
        begin
          if DownInfo.mHeaders <> nil then
          if DownInfo.mHeaderIndex >= 0 then
          begin
            if DownInfo.mHeaderIndex = UpInfo.mHeaderIndex then
            if DownInfo.mArea = UpInfo.mArea then
            if DownInfo.mHeaders = UpInfo.mHeaders then
              DoHeaderClick(UpInfo.mHeaders[mHeaderIndex], mHitCol, mHitRow);
          end;
        end;
      maLeft: // leftheaderclick
        begin
          if DownInfo.mHeaders <> nil then
          if DownInfo.mHeaderIndex >= 0 then
          begin
            if DownInfo.mHeaderIndex = UpInfo.mHeaderIndex then
            if DownInfo.mArea = UpInfo.mArea then
            if DownInfo.mHeaders = UpInfo.mHeaders then
              DoHeaderClick(UpInfo.mHeaders[mHeaderIndex], mHitCol, mHitRow);
          end;
        end;
    end;

    if mHitResult then
    if DownInfo.mHitCol = UpInfo.mHitCol then
      if DownInfo.mHitRow = UpInfo.mHitRow then
      begin
        if ssDouble in Shift then
          DoCellDblClick(UpInfo.mHitCol, UpInfo.mHitRow)
        else
          DoCellClick(UpInfo.mHitCol, UpInfo.mHitRow);
      end;

//    if EqualPoints(DownInfo.mDownCell, mUpCell) then
  //  if DownInfo
  end;

    (*
    try
      mUp := Point(X, Y);
      CellHit := HitTest(X, Y, Cell, CellRect{, LineHit});
      case mState of
        mdNone:
          begin

            if CellHit and (ciDownCell in mClickInfo) then
              if EqualPoints(mDownCell, Cell) then
              begin
                //HeaderMode := GetHeaderMode(Cell.x, Cell.y);
                { normale cell }
                //if HeaderMode = [] then
                begin
                  //Log([mdowncell.X, mdowncell.y, cell.x, cell.y]);
                  DoCellClick(Cell.x, Cell.y)
                end
                { title }
                //else if (hmTop in HeaderMode) and (Cell.y = 0) then // ftitlerow
                  //DoTitleClick(Cell.x, Cell.y, HeaderMode)
                { andere header }
                //else
                  //DoHeaderClick(Cell.x, Cell.y, HeaderMode);
              end;
          end;
        mdColSizing:
          begin
            if not (soLiveSizing in fOptions.fSizingOptions) then
            begin
              //PaintSizingLines(X, Y, True);
              //ColWidths[mResizeCell.x] := mStartColWidth + mUp.x - mDown.x;
              if [ssCtrl, ssAlt] * Shift = [ssCtrl, ssAlt] then
              begin
                PaintLock;
                SetDefaultColWidth(mStartColWidth + mUp.x - mDown.x);
                //SetDefaultRowHeight(miStartRowHeight + miUp.y - miDown.y);
                PaintUnLock;
              end
              else begin
                PaintSizingLines(X, Y, True);
                ColWidths[mResizeCell.x] := mStartColWidth + mUp.x - mDown.x;
              end;
//              Log([mstartcolwidth, mUp.x, mDown.x]);
            end;
          end;
        mdRowSizing:
          begin
            if not (soLiveSizing in fOptions.fSizingOptions) then
            begin
              //PaintSizingLines(X, Y, True);
              //RowHeights[mResizeCell.y] := mStartRowHeight + miUp.y - miDown.y
            end;
          end;
        mdCellSizing:
          begin
            {
            if not (soLiveSizing in fOptions.fSizingOptions) then
            begin
              PaintSizingLines(X, Y, True);
              ColWidths[miResizeCell.x] := miStartColWidth + miUp.x - miDown.x;
              RowHeights[miResizeCell.y] := miStartRowHeight + miUp.y - miDown.y
            end;


            if [ssCtrl, ssShift] * Shift <> [] then
            begin
              PaintLock;
              SetDefaultColWidth(miStartColWidth + miUp.x - miDown.x);
              SetDefaultRowHeight(miStartRowHeight + miUp.y - miDown.y);
              PaintUnLock;
            end; }

          end;
        mdRectSelecting:
          begin
            { hide selectierect }
            //PaintSelectionRect(Rect(miDown.x, miDown.y, miUp.x, miUp.y), True);
          end;
      end;
    finally
      InvalidateMouseInfo;
    end;
  end;

  *)

  inherited MouseUp(Button, Shift, X, Y);


  finally
    InvalidateMouseInfo;
  end;

// if Cursor <> crDefault then
  // Cursor := crDefault;

//  ReleaseCapture;




end;

procedure TBaseMatrix.MoveCurrent(aCol, aRow: Integer;
  aHorz: THorizontalFocusPosition = hfpAuto;
  aVert: TVerticalFocusPosition = vfpAuto);
var
  OldCol, OldRow, NewCol, NewRow: Integer;
  OldRowVisibility, NewRowVisibility: Integer;
  OldColVisibility, NewColVisibility: Integer;
  OldInfo, TempInfo, NewInfo: TPageInfo;
  DeltaX, DeltaY: Integer;
  NewTopX, NewTopY: Integer;
  MustRepaint: Boolean;
begin
  MustRepaint := False;
  OldCol := fCol;
  OldRow := fRow;
  NewCol := aCol;
  NewRow := aRow;
  Restrict(NewCol, 0, ColCount - 1);
  Restrict(NewRow, 0, RowCount - 1);
  DeltaX := NewCol - OldCol;
  DeltaY := NewRow - OldRow;

  { huidige visuele situatie }
  CalcPageInfo(OldInfo);
  OldColVisibility := ColVisibility(OldCol, OldInfo.Horz);
  OldRowVisibility := RowVisibility(OldRow, OldInfo.Vert);
  NewColVisibility := ColVisibility(NewCol, OldInfo.Horz);
  NewRowVisibility := RowVisibility(NewRow, OldInfo.Vert);

  NewTopY := fTopLeft.Y;
  NewTopX := fTopLeft.X;

  if fNavigationInfo.nReason <> srClick then
  begin
    {---------------------------------------------------------------------------
      we gaan naar beneden
    ---------------------------------------------------------------------------}
    if DeltaY > 0 then
    begin
      NewTopY := NewPageVert(NewRow, vfpBottom);
    end
    {---------------------------------------------------------------------------
      we gaan naar boven
    ---------------------------------------------------------------------------}
    else if DeltaY < 0 then
    begin
      NewTopY := NewPageVert(NewRow, vfpTop);
    end;

    {---------------------------------------------------------------------------
      we gaan naar rechts
    ---------------------------------------------------------------------------}
    if DeltaX > 0 then
    begin
      NewTopX := NewPageHorz(NewCol, hfpRight)
    end
    {---------------------------------------------------------------------------
      we gaan naar links
    ---------------------------------------------------------------------------}
    else if DeltaX < 0 then
    begin
      NewTopX := NewPageHorz(NewCol, hfpLeft)
    end;

  end;

  { altijd zichtbaar maken }
  if (NewCol = fCol) and (NewRow = fRow) then
  begin
    if (OldRowVisibility = 0) or (NewRowVisibility = 0) then
    begin
      NewTopY := NewPageVert(NewRow, vfpTop);
      NewTopX := NewPageHorz(NewCol, hfpLeft);
    end;
      //Log(['niet zichtbaar']);
  end;

  InternalSetCol(NewCol, True);
  InternalSetRow(NewRow, True);
//  fCol := NewCol; //##fcol changed 4
//  fRow := NewRow; //##frow changed 4

  if (NewTopY <> fTopLeft.Y) or (NewTopX <> fTopLeft.X) then
  begin
    MoveTopLeft(NewTopX, NewTopY);
    MustRepaint := True;
  end;

  { nieuwe situatie }
//  CalcPageInfo(NewInfo);

  if MustRepaint then
    Invalidate
  else begin
    InvalidateCell(OldCol, OldRow);
    InvalidateCell(NewCol, NewRow);
  end;

//  Log([fCol, frow]);
end;

procedure TBaseMatrix.MoveTopLeft(aLeft, aTop: Integer; aFocusedCol: Integer = -1; aFocusedRow: Integer = -1);
begin
//  if (aLeft = fTopLeft.X) and (aTop = fTopLeft.Y) then
//    Exit;
  fTopLeft.X := aLeft;
  fTopLeft.Y := aTop;
  if aFocusedCol >= 0 then
    InternalSetCol(aFocusedCol, True);
//    fCol := aFocusedCol; //##fcol changed 5
  if aFocusedRow >= 0 then
    InternalSetRow(aFocusedRow, True);
    //fRow := aFocusedRow; //##frow changed 5
  InvalidatePageInfo;
end;

function TBaseMatrix.Navigation(aReason: TScrollReason; aCommand: TScrollCommand;
  XParam: Integer = -1; YParam: Integer = -1): TNavigationResult;
{-------------------------------------------------------------------------------
  complete afhandeling van navigatie + repaint
-------------------------------------------------------------------------------}

{todo: code voor static en virtual matrix inbouwen, ook rekening houden met kalender --> pagedown is maand vooruit
  dus col moet ook kunnen veranderen.
  virtual matrix --> mark row? }
var
  OldInfo, TempInfo: TPageInfo;
  OldCol, OldRow, OldRowVisibility, OldColVisibility: Integer;
  NewRowVisibility: Integer;
  DeltaX, DeltaY: Integer;
  ColChanged, RowChanged: Boolean;
  NewTopX, NewTopY: Integer;
  Inv: Integer;

    procedure LocalRepaint;
    begin
//      if fNavigationRecursion =
      case fSelectMode of
        smCellSelect:
          begin
            if ValidCoord(OldCol, OldRow) then InvalidateCell(OldCol, OldRow, True);
            if ValidCoord(fCol, fRow) then InvalidateCell(fCol, fRow, True);
          end;
        smRowSelect:
          begin
            if ValidRow(OldRow) then InvalidateRow(OldRow);
            if ValidRow(fRow) then InvalidateRow(fRow);
          end;
        smColSelect:
          begin
            if ValidCol(OldCol) then InvalidateCol(OldCol);
            if ValidCol(fCol) then InvalidateCol(fCol);
          end;
        smColRowSelect:
          begin
            if ValidCol(OldCol) then InvalidateCol(OldCol);
            if ValidCol(fCol) then InvalidateCol(fCol);
            if ValidRow(OldRow) then InvalidateRow(OldRow);
            if ValidRow(fRow) then InvalidateRow(fRow);
          end;
      end;
    end;

begin
  {$ifdef check_bugs}
  if fNavigationRecursion > 2 then
    Error('Navigate recursie');
  {$endif}
  Inc(fNavigationRecursion);

  try
//  Log(['navi']);
//  DoCellExit(fCol, fRow);

  Result := [];
  CalcPageInfo(OldInfo);
  OldCol := fCol;
  OldRow := fRow;
  OldRowVisibility := RowVisibility(OldRow, OldInfo.Vert);
  OldColVisibility := ColVisibility(OldCol, OldInfo.Horz);

  case aCommand of
    scAbsolute:
      begin
        if aReason = srData then
        begin
          MoveCurrent(XParam, YParam);
          Invalidate;
          Exit;
        end;
        Inv := 1;
        DeltaY := YParam - fRow;
        DeltaX := XParam - fCol;
        if DoChangeRow(YParam - fRow) then
        begin
          Include(Result, nrRowChanged);
          if aReason = srClick then
            Inv := 2
          else begin
            if (YParam >= OldRow) and (OldRow + DeltaY > OldInfo.Vert.LastVisible) then
            begin
              NewTopY := NewPageVert(fRow, vfpBottom);
              MoveTopLeft(fTopLeft.X, NewTopY);
            end
            else if (YParam <= OldRow) and (OldRow + DeltaY < OldInfo.Vert.FirstVisible) then begin
              NewTopY := NewPageVert(fRow, vfpTop);
              MoveTopLeft(fTopLeft.X, NewTopY);
            end;
          end;
        end;
        if DoChangeCol(XParam - fCol) then
        begin
          Include(Result, nrColChanged);
          if aReason = srClick then
            Inv := 2
          else begin
            if (XParam >= OldCol) and (OldCol + DeltaX > OldInfo.Horz.LastVisible) then
            begin
              NewTopX := NewPageHorz(fCol, hfpRight);
              MoveTopLeft(NewTopX, fTopLeft.Y);
            end
            else if (XParam <= OldCol) and (OldCol + DeltaX < OldInfo.Horz.FirstVisible) then begin
              NewTopX := NewPageHorz(fCol, hfpLeft);
              MoveTopLeft(NewTopX, fTopLeft.Y);
            end;
          end;
        end;

        if Inv = 2 then
          LocalRepaint
        else
          Invalidate;
      end;
    scRight:
      begin
        ColChanged := DoChangeCol(1);
        if ColChanged then Include(Result, nrColChanged);

        { evt. zig zag naar beneden door recursieve aanroep }
        if not ColChanged then
        begin
          if noNextLine in Options.NavigateOptions then
          if fNavigationRecursion = 1 then
          begin
            Navigation(aReason, scDown);
            Navigation(aReason, scHorzStart);
          end;
          Exit; // altijd exit hier
        end;

        with OldInfo.Horz do
        begin
          { we moeten zeker scrollen omdat we rechts zijn }
          if OldCol >= LastFullVisible then
          begin
            NewTopX := NewPageHorz(fCol, hfpRight);
            MoveTopLeft(NewTopX, fTopLeft.Y);
            Invalidate;
          end;
          LocalRepaint;
        end;
      end;
    scLeft:
      begin
        ColChanged := DoChangeCol(-1);
        if ColChanged then Include(Result, nrColChanged);

        { evt. zig zag naar boven door recursieve aanroep }
        if not ColChanged then
        begin
          if noPrevLine in Options.NavigateOptions then
          if fNavigationRecursion = 1 then
          begin
            Navigation(aReason, scUp);
            Navigation(aReason, scHorzEnd);
          end;
          Exit;  // altijd exit hier
        end;

        with OldInfo.Horz do
        begin
          { we moeten zeker scrollen omdat we rechts zijn }
          if OldCol <= FirstVisible { >= ##bug?}then
          begin
            NewTopX := NewPageHorz(fCol, hfpLeft);
            MoveTopLeft(NewTopX, fTopLeft.Y);
            Invalidate;
          end;
          LocalRepaint;
        end;
      end;
    scDown:
      begin
        RowChanged := DoChangeRow(1);
        if RowChanged then Include(Result, nrRowChanged);

        { evt. zig zag naar boven door recursieve aanroep }
        if not RowChanged then
        begin
          if fNavigationRecursion = 1 then
          if noNextRow in Options.NavigateOptions then
          begin
            Navigation(aReason, scVertStart);
            Navigation(aReason, scRight);
          end;
          Exit;  // altijd exit hier
        end;

        if not RowChanged then Exit;
        with OldInfo.Vert do
        begin
          if OldRow = LastFullVisible then
          begin
            NewTopY := NewPageVertDownwards(fRow, vfpBottom);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end
          { we moeten zeker scrollen omdat we beneden zijn }
          else if OldRow >= LastFullVisible then
          begin
            NewTopY := NewPageVert(fRow, vfpBottom);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
          LocalRepaint;
        end;
      end;
    scUp:
      begin
        RowChanged := DoChangeRow(-1);
        if RowChanged then Include(Result, nrRowChanged);

        { evt. zig zag naar boven door recursieve aanroep }
        if not RowChanged then
        begin
          if fNavigationRecursion = 1 then
          if noPrevRow in Options.NavigateOptions then
          begin
            Navigation(aReason, scVertEnd);
            Navigation(aReason, scLeft);
          end;
          Exit;  // altijd exit hier
        end;

        //        Log(['rowchanged', rowchanged]);
        if not RowChanged then Exit;
        with OldInfo.Vert do
        begin
          { we moeten zeker scrollen omdat we boven zijn }
          if OldRow <= FirstVisible then
          begin
            NewTopY := NewPageVert(fRow, vfpTop);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
          LocalRepaint;
        end;
      end;
    scPgDn:
      begin
        { er is slechts 1 rij volledig zichtbaar, we willen wel scrollen dan}
        if (OldRowVisibility > 0) and (OldInfo.Vert.FullVisibleCount <= 1) then
        begin
          if DoChangeRow(1) then
          begin
            NewTopY := NewPageVert(fRow, vfpTop{Bottom});
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
        end
        { rij is zichtbaar maar is niet de onderste --> ga naar onderste }
        else if (OldRowVisibility > 0) and (fRow < OldInfo.Vert.LastFullVisible) then
        begin
          if DoChangeRow(OldInfo.Vert.LastFullVisible - fRow) then
            LocalRepaint;
        end
        { rij is zichtbaar en is de onderste --> scroll down }
        else if (OldRowVisibility > 0) and (fRow = OldInfo.Vert.LastFullVisible) then
        begin
          if DoChangeRow(OldInfo.Vert.FullVisibleCount - 1) then
          begin
            NewTopY := NewPageVert(fRow, vfpBottom);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
        end;
      end;
    scPgUp:
      begin
        { er is slechts 1 rij volledig zichtbaar, we willen wel scrollen dan}
        if (OldRowVisibility > 0) and (OldInfo.Vert.FullVisibleCount <= 1) then
        begin
          if DoChangeRow(-1) then
          begin
            NewTopY := NewPageVert(fRow, vfpTop{Bottom});
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
        end
        { rij is zichtbaar maar is niet de bovenste --> ga naar bovenste }
        else if (OldRowVisibility > 0) and (fRow > OldInfo.Vert.FirstVisible) then
        begin
          if DoChangeRow(-(fRow - OldInfo.Vert.FirstVisible)) then
            LocalRepaint;
        end
        { rij is zichtbaar maar en is de bovenste --> scroll up }
        else if (OldRowVisibility > 0) and (fRow = OldInfo.Vert.FirstVisible) then
        begin
          if DoChangeRow(-(OldInfo.Vert.FullVisibleCount - 1)) then
          begin
            NewTopY := NewPageVert(fRow, vfpTop);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
        end;
      end;
    scHorzStart:
      begin
        if DoGotoFirstCol then
        begin
          MoveTopLeft(0, fTopLeft.Y);
          Invalidate;
        end;
      end;
    scHorzEnd:
      begin
        if DoGotoLastCol then
        begin
          NewTopX := NewPageHorz(fCol, hfpRight);
          MoveTopLeft(NewTopX, fTopLeft.Y);
          Invalidate;
        end;
      end;
    scVertStart:
      begin
        if DoGotoFirstRow then
        begin
          MoveTopLeft(fTopLeft.X, 0);
          Invalidate;
        end;
      end;
    scVertEnd:
      begin
        if DoGotoLastRow then
        begin
          NewTopY := NewPageVert(fRow, vfpBottom);
          MoveTopLeft(fTopLeft.X, NewTopY);
          Invalidate;
        end;
        //MoveCurrent(fCol, fRowCount - 1);
      end;
    scBegin:
      begin
        MoveCurrent(0, 0);
      end;
    scEnd:
      begin
        MoveCurrent(fColCount - 1, fRowCount - 1);
      end;
  end;

  finally
    Dec(fNavigationRecursion);
  end;

end;

function TBaseMatrix.NavigationFixed(aReason: TScrollReason; aCommand: TScrollCommand; XParam, YParam: Integer): TNavigationResult;
var
  OldInfo, TempInfo: TPageInfo;
  OldCol, OldRow, OldRowVisibility, OldColVisibility: Integer;
  NewRowVisibility: Integer;
  DeltaX, DeltaY: Integer;
  ColChanged, RowChanged: Boolean;
  NewTopX, NewTopY: Integer;
  Inv: Integer;
    procedure LocalRepaint;
    begin
//      if fNavigationRecursion =
      case fSelectMode of
        smCellSelect:
          begin
            if ValidCoord(OldCol, OldRow) then InvalidateCell(OldCol, OldRow, True);
            if ValidCoord(fCol, fRow) then InvalidateCell(fCol, fRow, True);
          end;
        smRowSelect:
          begin
            if ValidRow(OldRow) then InvalidateRow(OldRow);
            if ValidRow(fRow) then InvalidateRow(fRow);
          end;
        smColSelect:
          begin
            if ValidCol(OldCol) then InvalidateCol(OldCol);
            if ValidCol(fCol) then InvalidateCol(fCol);
          end;
        smColRowSelect:
          begin
            if ValidCol(OldCol) then InvalidateCol(OldCol);
            if ValidCol(fCol) then InvalidateCol(fCol);
            if ValidRow(OldRow) then InvalidateRow(OldRow);
            if ValidRow(fRow) then InvalidateRow(fRow);
          end;
      end;
    end;

begin
  Result := [];
  CalcPageInfo(OldInfo);
  OldCol := fCol;
  OldRow := fRow;
  OldRowVisibility := RowVisibility(OldRow, OldInfo.Vert);
  OldColVisibility := ColVisibility(OldCol, OldInfo.Horz);

  case aCommand of
    scAbsolute:
      begin
        Inv := 1;
        DeltaY := YParam - fRow;
        DeltaX := XParam - fCol;
        if DoChangeRow(YParam - fRow) then
        begin
          Include(Result, nrRowChanged);
          if aReason = srClick then
            Inv := 2
          else begin
            if (YParam >= OldRow) and (OldRow + DeltaY > OldInfo.Vert.LastVisible) then
            begin
              NewTopY := NewPageVert(fRow, vfpBottom);
              MoveTopLeft(fTopLeft.X, NewTopY);
            end
            else if (YParam <= OldRow) and (OldRow + DeltaY < OldInfo.Vert.FirstVisible) then begin
              NewTopY := NewPageVert(fRow, vfpTop);
              MoveTopLeft(fTopLeft.X, NewTopY);
            end;
          end;
        end;
        if DoChangeCol(XParam - fCol) then
        begin
          Include(Result, nrColChanged);
          if aReason = srClick then
            Inv := 2
          else begin
            if (XParam >= OldCol) and (OldCol + DeltaX > OldInfo.Horz.LastVisible) then
            begin
              NewTopX := NewPageHorz(fCol, hfpRight);
              MoveTopLeft(NewTopX, fTopLeft.Y);
            end
            else if (XParam <= OldCol) and (OldCol + DeltaX < OldInfo.Horz.FirstVisible) then begin
              NewTopX := NewPageHorz(fCol, hfpLeft);
              MoveTopLeft(NewTopX, fTopLeft.Y);
            end;
          end;
        end;

        if Inv = 2 then
          LocalRepaint
        else
          Invalidate;
      end;
    scRight:
      begin
        ColChanged := DoChangeCol(1);
        if ColChanged then Include(Result, nrColChanged);

        { evt. zig zag naar beneden door recursieve aanroep }
        if not ColChanged then
        begin
          if noNextLine in Options.NavigateOptions then
          if fNavigationRecursion = 1 then
          begin
            Navigation(aReason, scDown);
            Navigation(aReason, scHorzStart);
          end;
          Exit; // altijd exit hier
        end;

        with OldInfo.Horz do
        begin
          { we moeten zeker scrollen omdat we rechts zijn }
          if OldCol >= LastFullVisible then
          begin
            NewTopX := NewPageHorz(fCol, hfpRight);
            MoveTopLeft(NewTopX, fTopLeft.Y);
            Invalidate;
          end;
          LocalRepaint;
        end;
      end;
    scLeft:
      begin
        ColChanged := DoChangeCol(-1);
        if ColChanged then Include(Result, nrColChanged);

        { evt. zig zag naar boven door recursieve aanroep }
        if not ColChanged then
        begin
          if noPrevLine in Options.NavigateOptions then
          if fNavigationRecursion = 1 then
          begin
            Navigation(aReason, scUp);
            Navigation(aReason, scHorzEnd);
          end;
          Exit;  // altijd exit hier
        end;

        with OldInfo.Horz do
        begin
          { we moeten zeker scrollen omdat we rechts zijn }
          if OldCol <= FirstVisible { >= ##bug?}then
          begin
            NewTopX := NewPageHorz(fCol, hfpLeft);
            MoveTopLeft(NewTopX, fTopLeft.Y);
            Invalidate;
          end;
          LocalRepaint;
        end;
      end;
    scDown:
      begin
        RowChanged := DoChangeRow(1);
        if RowChanged then Include(Result, nrRowChanged);

        { evt. zig zag naar boven door recursieve aanroep }
        if not RowChanged then
        begin
          if fNavigationRecursion = 1 then
          if noNextRow in Options.NavigateOptions then
          begin
            Navigation(aReason, scVertStart);
            Navigation(aReason, scRight);
          end;
          Exit;  // altijd exit hier
        end;

        if not RowChanged then Exit;
        with OldInfo.Vert do
        begin
          if OldRow = LastFullVisible then
          begin
            NewTopY := NewPageVertDownwards(fRow, vfpBottom);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end
          { we moeten zeker scrollen omdat we beneden zijn }
          else if OldRow >= LastFullVisible then
          begin
            NewTopY := NewPageVert(fRow, vfpBottom);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
          LocalRepaint;
        end;
      end;
    scUp:
      begin
        RowChanged := DoChangeRow(-1);
        if RowChanged then Include(Result, nrRowChanged);

        { evt. zig zag naar boven door recursieve aanroep }
        if not RowChanged then
        begin
          if fNavigationRecursion = 1 then
          if noPrevRow in Options.NavigateOptions then
          begin
            Navigation(aReason, scVertEnd);
            Navigation(aReason, scLeft);
          end;
          Exit;  // altijd exit hier
        end;

        //        Log(['rowchanged', rowchanged]);
        if not RowChanged then Exit;
        with OldInfo.Vert do
        begin
          { we moeten zeker scrollen omdat we boven zijn }
          if OldRow <= FirstVisible then
          begin
            NewTopY := NewPageVert(fRow, vfpTop);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
          LocalRepaint;
        end;
      end;
    scPgDn:
      begin
        { er is slechts 1 rij volledig zichtbaar, we willen wel scrollen dan}
        if (OldRowVisibility > 0) and (OldInfo.Vert.FullVisibleCount <= 1) then
        begin
          if DoChangeRow(1) then
          begin
            NewTopY := NewPageVert(fRow, vfpTop{Bottom});
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
        end
        { rij is zichtbaar maar is niet de onderste --> ga naar onderste }
        else if (OldRowVisibility > 0) and (fRow < OldInfo.Vert.LastFullVisible) then
        begin
          if DoChangeRow(OldInfo.Vert.LastFullVisible - fRow) then
            LocalRepaint;
        end
        { rij is zichtbaar en is de onderste --> scroll down }
        else if (OldRowVisibility > 0) and (fRow = OldInfo.Vert.LastFullVisible) then
        begin
          if DoChangeRow(OldInfo.Vert.FullVisibleCount - 1) then
          begin
            NewTopY := NewPageVert(fRow, vfpBottom);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
        end;
      end;
    scPgUp:
      begin
        { er is slechts 1 rij volledig zichtbaar, we willen wel scrollen dan}
        if (OldRowVisibility > 0) and (OldInfo.Vert.FullVisibleCount <= 1) then
        begin
          if DoChangeRow(-1) then
          begin
            NewTopY := NewPageVert(fRow, vfpTop{Bottom});
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
        end
        { rij is zichtbaar maar is niet de bovenste --> ga naar bovenste }
        else if (OldRowVisibility > 0) and (fRow > OldInfo.Vert.FirstVisible) then
        begin
          if DoChangeRow(-(fRow - OldInfo.Vert.FirstVisible)) then
            LocalRepaint;
        end
        { rij is zichtbaar maar en is de bovenste --> scroll up }
        else if (OldRowVisibility > 0) and (fRow = OldInfo.Vert.FirstVisible) then
        begin
          if DoChangeRow(-(OldInfo.Vert.FullVisibleCount - 1)) then
          begin
            NewTopY := NewPageVert(fRow, vfpTop);
            MoveTopLeft(fTopLeft.X, NewTopY);
            Invalidate;
          end;
        end;
      end;
    scHorzStart:
      begin
        if DoGotoFirstCol then
        begin
          MoveTopLeft(0, fTopLeft.Y);
          Invalidate;
        end;
      end;
    scHorzEnd:
      begin
        if DoGotoLastCol then
        begin
          NewTopX := NewPageHorz(fCol, hfpRight);
          MoveTopLeft(NewTopX, fTopLeft.Y);
          Invalidate;
        end;
      end;
    scVertStart:
      begin
        if DoGotoFirstRow then
        begin
          MoveTopLeft(fTopLeft.X, 0);
          Invalidate;
        end;
      end;
    scVertEnd:
      begin
        if DoGotoLastRow then
        begin
          NewTopY := NewPageVert(fRow, vfpBottom);
          MoveTopLeft(fTopLeft.X, NewTopY);
          Invalidate;
        end;
        //MoveCurrent(fCol, fRowCount - 1);
      end;
    scBegin:
      begin
        MoveCurrent(0, 0);
      end;
    scEnd:
      begin
        MoveCurrent(fColCount - 1, fRowCount - 1);
      end;
  end;


end;



procedure TBaseMatrix.MatrixOptionsChanged(Sender: TObject);
{-------------------------------------------------------------------------------
  mee bezig: meer adequaat reageren op veranderingen
-------------------------------------------------------------------------------}

  (*  procedure Check;
    var
      E: TMXEditOption;
    begin
//      Log(['change']);
      with Options.fOptionsDeleted do
      begin
        Log(['editoptions deleted']);
        for E := Low(TMXEditOption) to High(TMXEditOption) do
          if E in cEditOptions then
            Log([getenumname(typeinfo(tmxeditoption), integer(E))]);
      end;

      with Options.fOptionsInserted do
      begin
        Log(['editoptions inserted']);
        for E := Low(TMXEditOption) to High(TMXEditOption) do
          if E in cEditOptions then
            Log([getenumname(typeinfo(tmxeditoption), integer(E))]);
      end;

    end; *)

begin

  // sender <> options: update alles
  if Sender <> Options then
  begin
    PaintLock;
    try
      InvalidatePageInfo;
      UpdateColumns;
      UpdateRows;
      UpdateHeaders;
      UpdateHeaderSizes;
      UpdateAutoBounds;
      DoNavigate(srNone, scAbsolute, hfpAuto, vfpAuto, fCol, fRow);
    finally
      PaintUnlock;
    end;
    Exit;
  end;

  // sender = options: we kunnen slim updaten
  PaintLock;
  try
    InvalidatePageInfo;
    if uoColumns in Options.ChangedUseOptions then
      UpdateColumns;
    if uoRows in Options.ChangedUseOptions then
      UpdateRows;
    if [uoTopHeaders, uoLeftHeaders, uoRightHeaders, uoBottomHeaders] * Options.ChangedUseOptions <> [] then
      UpdateHeaders;
    if [poHeaderHorzLines, poHeaderVertLines] * Options.ChangedPaintOptions <> [] then
      UpdateHeaderSizes;
    UpdateAutoBounds;
    if HandleAllocated then
      DoNavigate(srNone, scAbsolute, hfpAuto, vfpAuto, fCol, fRow);
  finally
    PaintUnlock;
  end;

end;

function TBaseMatrix.NewPageHorz(aNewCol: Integer; aPosition: THorizontalFocusPosition): Integer;
{-------------------------------------------------------------------------------
  berekend NewPageHorz: nieuwe pagina.
  retourneert de beste topleft.x
-------------------------------------------------------------------------------}
var
  PgInfo: TPageInfo;
  TestLeft: Integer;
  Vis: Integer;
begin
  CalcPageInfo(PgInfo);

  Vis := FULL_VISIBLE;
  with fNavigationInfo do
  begin
    if fNavigationInfo.nReason = srClick then
    begin
      Vis := PARTIAL_VISIBLE;
    end;
  end;

  { de nieuwe rij is reeds zichtbaar, exit }
  if ColVisibility(aNewCol, PgInfo.Horz) >= Vis then
  begin
    Result := fTopLeft.X;
    Exit;
  end;

  Result := aNewCol;

  { de nieuwe rij is niet zichtbaar }
  case aPosition of
    hfpLeft:
      begin
        Result := aNewCol;
        Exit;
      end;
    hfpCenter:
      begin
        //Result := aNewCol +
        //Exit;
      end;
    hfpRight:
      begin
        { zet left op nieuwe cel en scroll virtueel naar links totdat de
          nieuwe cel zo laag mogelijk staat }
        TestLeft := aNewCol;
        repeat
          CalcPageInfo(PgInfo, TestLeft, -1, True);
          if ColVisibility(aNewCol, PgInfo.Horz) >= Vis
          then Result := TestLeft
          else Break;
//          Log([testleft]);
          Dec(TestLeft);
          if RestrictLeft(TestLeft) then
            Break;
        until False;
      end
    else raise exception.create('newpagehorz')
  end;

end;

function TBaseMatrix.NewPageVert(aNewRow: Integer; aPosition: TVerticalFocusPosition): Integer;
{-------------------------------------------------------------------------------
  berekend NewPageVert: nieuwe pagina.
  retourneert de beste topleft.y
-------------------------------------------------------------------------------}
var
  PgInfo: TPageInfo;
  TestTop: Integer;
  Vis: Integer;
begin
  CalcPageInfo(PgInfo);
  Vis := FULL_VISIBLE;

  { als er met de muis op een cell is geklikt dan topleft niet verschuiven, vandaar
    dat de cell niet geheel zichtbaar hoeft te zijn }
  with fNavigationInfo do
  begin
    if fNavigationInfo.nReason = srClick then
    begin
      Vis := PARTIAL_VISIBLE;
    end;
  end;

  Result := aNewRow;

  { de nieuwe rij is niet zichtbaar }
  case aPosition of
    vfpTop:
      begin
        Result := aNewRow;
        Exit;
      end;
    vfpCenter:
      begin
      end;
    vfpBottom:
      begin
        { zet top op nieuwe cel en scroll virtueel omhoog totdat de nieuw cell
          niet meer zichtbaar is. result wordt on the fly aangepast }
        TestTop := aNewRow;
        RestrictTop(TestTop);
        repeat
          CalcPageInfo(PgInfo, -1, TestTop, True);
          if RowVisibility(aNewRow, PgInfo.Vert) >= Vis
          then Result := TestTop
          else Break;
          Dec(TestTop);
          if RestrictTop(TestTop) then
            Break;
        until False;
        { bereken of we nog een stukje omhoog moeten }
        if (aNewRow = fRowCount - 1) and (Result > 0) then
        begin
        end;
      end;
  end;

//  Log(['RESULT', result]);
end;

function TBaseMatrix.NewPageVertDownwards(aNewRow: Integer; aPosition: TVerticalFocusPosition): Integer;
{-------------------------------------------------------------------------------
  ###testroutine voor slimmer scrollen
  berekend NewPageVert: nieuwe pagina. test naar beneden dat kan veel tijd schelen
  retourneert de beste topleft.y
-------------------------------------------------------------------------------}
var
  PgInfo: TPageInfo;
  TestTop: Integer;
  Vis: Integer;
begin
  CalcPageInfo(PgInfo);
  Vis := FULL_VISIBLE;

  { als er met de muis op een cell is geklikt dan topleft niet verschuiven, vandaar
    dat de cell niet geheel zichtbaar hoeft te zijn }
  with fNavigationInfo do
  begin
    if fNavigationInfo.nReason = srClick then
    begin
      Vis := PARTIAL_VISIBLE;
    end;
  end;

  Result := aNewRow;

  { de nieuwe rij is niet zichtbaar }
  case aPosition of
    vfpTop:
      begin
        Result := aNewRow;
        Exit;
      end;
    vfpCenter:
      begin
      end;
    vfpBottom:
      begin
        { zet top op huidige topleft + 1 en scroll virtueel omlaag }
        TestTop := fTopLeft.Y + 1;
        RestrictTop(TestTop);
        repeat
          CalcPageInfo(PgInfo, -1, TestTop, True);
          if RowVisibility(aNewRow, PgInfo.Vert) >= Vis then
          begin
            Result := TestTop;
            Break;
          end;
          // voorkom lock
          if aNewRow <= PgInfo.Vert.FirstVisible then
            Break;
          Inc(TestTop);
          if RestrictTop(TestTop) then
            Break;
        until False;
      end;
  end;

//  Log(['RESULT', result]);
end;



function TBaseMatrix.PageInfoPtr(aLeft: integer = -1; aTop: Integer = -1; ForceRecalc: Boolean = False): TPageInfo;
begin
  CalcPageInfo(Result, aLeft, aTop, ForceRecalc);
end;

procedure TBaseMatrix.Paint;
var
  Window: TRect;
begin
  if not IsRectEmpty(fUpdateRect) then
  begin
//    Log([rectstr(fupdaterect), fclientwidth]);
    Window := fUpdateRect;
    PaintMatrix(Canvas, Window);
  end;
end;

procedure TBaseMatrix.PaintBackGround(const PaintInfo: TPaintInfo);
var
  R: TRect;
  MustCoverClient: Boolean;
begin
  if poSkipBackGround in fOptions.fPaintOptions then
    Exit;

  if Assigned(fOnPaintBackGround) then //###do
  begin
    fOnPaintBackGround(Self, PaintInfo);
    Exit;
  end;

  // wanneer er transparante gedeeltes zijn dan moeten we de gehele achtergrond tekenen
  MustCoverClient := True;
{    fOptions.PaintOptions *
    [poTransparentCells,
     poTransparentHorzLines,
     poTransparentVertLines] <> []; } 

//  Log([rectstr(fgridrect), rectstr(clientrect)]);

  with PaintInfo, PageInfo do
  begin
    TargetCanvas.Brush.Color := Self.Color;
    if MustCoverClient then
    begin
      TargetCanvas.FillRect(ClientRect);
    end
    else begin
      R := EmptyRect;
      R.Top := Vert.GridSize;
      R.Right := fClientWidth;
      R.Bottom := fClientHeight;
      TargetCanvas.FillRect(R);
      R := EmptyRect;
      R.Left := Horz.GridSize;
      R.Right := fClientWidth;
      R.Bottom := Vert.GridSize;
      TargetCanvas.FillRect(R);
    end;
  end;


end;

procedure TBaseMatrix.PaintCell(Params: TCellParams; const aPaintInfo: TPaintInfo; Data: TCellData);
{-------------------------------------------------------------------------------
  PaintCell:
  Teken in cellbitmap en kopieer naar targetcanvas.
-------------------------------------------------------------------------------}
var
  S: string;
  Painter: TCellPainterClass;
  Diff, TH: Integer;
  R: TRect;
label
  _CONTENTS, _SELECTOR, _COPYCANVAS;
{    if poTransparentCells in Options.PaintOptions then
    begin
      cpCellCanvas.CopyRect(cpCanvasRect, aPaintInfo.TargetCanvas, cpCellBounds);
    end; }
begin


  with Params do
  begin
    cpStage := pcsBeforePaint;
    cpHandled := False;
    if pcsBeforePaint in fCustomPaintStages then
    begin
      if fCustomPainting then
        CustomPaintCell(Params, aPaintInfo, Data);
      if Assigned(fOnPaintCell) then fOnPaintCell(Self, Params, Data);
    end;

    // Custom cell achtergrond
    cpStage := pcsBackGround;
    cpHandled := False;
    if pcsBackGround in fCustomPaintStages then
    begin
      if CustomPainting then
      begin
        CustomPaintCell(Params, aPaintInfo, Data);
        if cpHandled then goto _CONTENTS;
      end;
      if Assigned(fOnPaintCell) then fOnPaintCell(Self, Params, Data);
      if cpHandled then goto _CONTENTS;
    end;

    // Cell achtergrond
    if not cpActiveStyle.CachedFlat then
    begin
      cpCellCanvas.Brush.Color := clBtnFace;
      cpCellCanvas.FillRect(pCanvasRect);
      DrawHeader(cpCellCanvas, cpCanvasRect)
    end
    else if cpActiveStyle.CachedBorderWidth > 0 then
    begin
      cpCellCanvas.Pen.Color := clBlack;
      cpCellCanvas.Pen.Style := psInsideFrame;
      cpCellCanvas.Pen.Width := cpActiveStyle.CachedBorderWidth;
      cpCellCanvas.Brush.Color := cpBackColor;
      cpCellCanvas.Rectangle(cpCanvasRect); // voor hokje om cell
    end
    else begin
      if poOnlyHighlightContents in fOptions.fPaintOptions
      then cpCellCanvas.Brush.Color := cpActiveStyle.CachedColors[MI_BACKCOLOR]
      else cpCellCanvas.Brush.Color := pBackColor;
      cpCellCanvas.FillRect(pCanvasRect);
    end;

    // Custom cell contents
    _CONTENTS:
    cpHandled := False;
    cpStage := pcsContents;
    if pcsContents in fCustomPaintStages then
    begin
      if CustomPainting then
      begin
        CustomPaintCell(Params, aPaintInfo, Data);
        if cpHandled then goto _SELECTOR;
      end;
      if Assigned(fOnPaintCell) then fOnPaintCell(Self,  Params, Data);
      if cpHandled then goto _SELECTOR;
    end;

    // Cell Contents
    if dsGlyph in cpDrawState then
    if cpImageList <> nil then // ### moet weg
    begin
      cpImageList.Draw(cpCellCanvas, cpImageRect.Left, cpImageRect.Top, cpImageIndex);
    end;

    (*
    { custom painting }
    if not (csDesigning in ComponentState) and fCustomPainting then
    begin
      Painter := DoGetCellPainter(Params, Data);
      if Painter <> nil then
      begin
        Painter.DoPaint(Params, Data);
        goto _COPYCANVAS;
      end;
    end
    { standaard string painting }
    else
    *)
    if Data <> nil then
    begin
      //DrawFrameControl(cpCellCanvas.Handle, cpContentsRect, DFC_BUTTON, DFCS_BUTTONCHECK + DFCS_CHECKED);
      S := Data.Text;
      if S <> '' then
      begin
        cpCellCanvas.Font.Color := cpTextColor;
        if not (poOnlyHighlightContents in fOptions.fPaintOptions) then
          SetBkMode(pCellCanvas.Handle, TRANSPARENT)
        else begin
          cpCellCanvas.Brush.Color := pBackColor;
          SetBkMode(pCellCanvas.Handle, OPAQUE)
        end;
        if (fHighlightString = '') or (dsFocused in cpDrawState) then
        begin

          // calc multiline centered
          if cpTextFlags and DT_SINGLELINE = 0 then
            if cpActiveStyle.CachedVertAlign = vaCenter then
              if not (poCalculatedRowHeights in Options.PaintOptions) then
              begin
                R := cpContentsRect;
                TH := DrawTextEx(cpCellCanvas.Handle, PChar(S), Length(S), R, cpTextFlags or DT_CALCRECT, nil);
                Diff := RectHeight(cpContentsRect) - TH;
                if Diff > 1 then
                 Inc(cpContentsRect.Top, (Diff div 2) and not 1);
              end;

          DrawTextEx(cpCellCanvas.Handle, PChar(S), length(S), cpContentsRect, cpTextFlags, @cpTextParams);
        end
        else
          ExtTextOutColored(cpCellCanvas,
                            S,
                            cpContentsRect,
                            cpTextFlags,
                            [fHighlightString],
                            [cpActiveStyle.CachedColors[MI_TEXTMATCH]],
                            [cpActiveStyle.CachedColors[MI_BACKMATCH]],
                            []);
      end;


    end;

_SELECTOR:

    if dsFocused in cpDrawState then
      if poSelector in  Options.PaintOptions then
        if fMatrixFocused then
        begin
          SetBkMode(cpCellCanvas.Handle, TRANSPARENT);
          cpCellCanvas.Pen.Color := clWhite;
          cpCellCanvas.Brush.Style := bsClear;
          cpCellCanvas.Rectangle(cpCanvasRect);
          SetBkMode(cpCellCanvas.Handle, OPAQUE);
        end;

(*    { custom drawing }
    cpHandled := False;
    cpStage := pcsAfterPaint;
    if Assigned(fOnPaintCell) then fOnPaintCell(Self,  Params, Data);
    if cpHandled then goto _COPYCANVAS; *)

    { kopieer naar targetcanvas}

    _COPYCANVAS:

    // aPaintInfo.TargetCanvas.CopyRect(cpCellBounds, cpCellCanvas, cpCanvasRect);
    IntersectRect(cpCellBounds, cpCellBounds, fGridRect);
{    if cpCellbounds.Right >= fGridRect.Right then ???????
    begin
      Inc(cpCellBounds.Right);
      inc(cpCellBounds.Bottom);
    end; }
    CopyCanvas(aPaintInfo.TargetCanvas, cpCellBounds, cpCellCanvas, cpCanvasRect);
  end;

end;

procedure TBaseMatrix.PaintCells(const PaintInfo: TPaintInfo);
var
  Data: TCellData;

    procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far;
    begin
      { teken alleen de cellen die binnen het updaterect vallen }
      if RectsDoIntersect(aCellRect, PaintInfo.Target) then
      begin
        fCellData.Clear;
        Data := nil;
        DoGetCellData(aCol, aRow, Data);
  //      Log([acol,arow]);
        with fCellParams do
        begin
          cpCol := aCol;
          cpRow := aRow;
          cpCellBounds := aCellRect;
          Inc(cpCellBounds.Right); // # windows rect shit
          Inc(cpCellBounds.Bottom); // # windows rect shit
        end;
        PreparePaintCell(fCellParams, Data);
        PaintCell(fCellParams, PaintInfo, Data);
      end;
    end;

begin
  if not RectsDoIntersect(PaintInfo.Target, fGridRect) then
    Exit;
  TraverseVisibleCells(@DoCell, PaintInfo.PageInfo, AllCells);
end;

procedure TBaseMatrix.PaintHeader(Params: THeaderParams; const PaintInfo: TPaintInfo);
var
  IsDown: Boolean;
  S: string;
begin
  with Params do
  begin
    hpStage := pcsBeforePaint;
    hpHandled := False;
    hpHeaderCanvas.Brush.Color := hpBackColor;

    // teken edge of achtergrond
    if not hpCellStyle.CachedFlat then
    begin
      DrawHeader(hpHeaderCanvas, hpCanvasRect, False{IsDown}, True, False{True})
    end
    else begin
      hpHeaderCanvas.FillRect(hpCanvasRect);
    end;

    case hpHeader.fHeaderType of
      htTop  : S := DoGetHeaderText(hpHeader, hpCol);
      htLeft : S := DoGetHeaderText(hpHeader, hpRow);
    end;

    if S <> '' then
    begin
      hpHeaderCanvas.Font.Color := hpTextColor;
      SetBkMode(hpHeaderCanvas.Handle, TRANSPARENT);
      DrawTextEx(hpHeaderCanvas.Handle, PChar(S), Length(S), hpContentsRect, hpTextFlags, nil);
    end;

    // image
    if dsGlyph in hpDrawState then
    if hpImageList <> nil then // ### moet weg
    begin
      hpImageList.Draw(hpHeaderCanvas, hpImageRect.Left, hpImageRect.Top, hpImageIndex);
    end;


    CopyCanvas(PaintInfo.TargetCanvas, hpHeaderBounds, hpHeaderCanvas, hpCanvasRect);
  end;
end;

procedure TBaseMatrix.PaintHeaders(const PaintInfo: TPaintInfo);
(*var
  TopLeftFlat: Boolean;
  TopLeftColor: TColor;
  FirstColumnPainted, FirstRowPainted: Boolean;
var
  X, Y: Integer;
  CurrentHeader: TMatrixHeader; *)


    procedure DoTopHeader(aHeader: TMatrixHeader; aTop, aBottom, aHeaderLineWidth: Integer; var HeaderTraverseInfo: DWORD); far;
    {-------------------------------------------------------------------------------
      Dubbel geneste localproc om top headers te tekenen
      1. loop door TopHeaders;
      2. loop door Columns
    -------------------------------------------------------------------------------}


        procedure DoColumn(aCol, aLeft, aRight, aColLineWidth: Integer; var ColTraverseInfo: DWORD); far;
        var
          R: TRect;
        begin
          R.Top := aTop;
          R.Bottom := aBottom;
          R.Left := aLeft;
          R.Right := aRight;
          if aHeader.Spanned then
          begin
            R.Right := PaintInfo.PageInfo.Horz.GridSize - 1;
            ColTraverseInfo := ColTraverseInfo or TRAVERSE_BREAK;
            //Log([rectstr(r)]);
          end;
          if not RectsDoIntersect(R, PaintInfo.Target) then
            Exit;
          { vul headerparams met goede waarden }
          with fHeaderParams do
          begin
            hpCol := aCol;
            hpRow := -1;
            hpHeader := aHeader;
            hpHeaderBounds := R;
            Inc(hpHeaderBounds.Right); // # windows rect shit
            Inc(hpHeaderBounds.Bottom); // # windows rect shit
          end;
          PreparePaintHeader(fHeaderParams);
          PaintHeader(fHeaderParams, PaintInfo);

          // teken "fake" topleft header in de stijl van topheader
          if ColTraverseInfo and TRAVERSE_FIRST <> 0 then
            if isVisibleLeftHeaders in fInternalStates then
            with PaintInfo do
            begin
              TargetCanvas.Brush.Color := fHeaderParams.hpBackColor;
              R.Right := fLeftHeaders.Size - 1 - GetEffectiveHeaderLineWidth(fLeftHeaders[fLeftHeaders.Count - 1]);
              R.Left := 0;
              Inc(R.Right); // # windows rect shit
              Inc(R.Bottom); // # windows rect shit
              DrawHeader(TargetCanvas, R, False, True, aHeader.Style.Flat);
            end;

        end;

    begin
      with PaintInfo.PageInfo do
        TraverseVisibleCols(@DoColumn, Horz, AllAxis);
    end;


    procedure DoLeftHeader(aHeader: TMatrixHeader; aLeft, aRight, aHeaderLineWidth: Integer; var HeaderInfo: DWORD); far;
    {-------------------------------------------------------------------------------
      Dubbel geneste localproc om left headers te tekenen
      1. loop door LeftHeaders;
      2. loop door Rows
    -------------------------------------------------------------------------------}

        procedure DoRow(aRow, aTop, aBottom, aRowLineWidth: Integer; var RowStop: Boolean); far;
        var
          R: TRect;
        begin
          R.Top := aTop;
          R.Bottom := aBottom;
          R.Left := aLeft;
          R.Right := aRight;
          if aHeader.Spanned then
            R.Right := PaintInfo.PageInfo.Vert.GridSize - 1;
          if not RectsDoIntersect(R, PaintInfo.Target) then
            Exit;
          { vul headerparams met goede waarden }
          with fHeaderParams do
          begin
            hpCol := -1;
            hpRow := aRow;
            hpHeader := aHeader;
            hpHeaderBounds := R;
            Inc(hpHeaderBounds.Right); // # windows rect shit
            Inc(hpHeaderBounds.Bottom); // # windows rect shit
          end;
          PreparePaintHeader(fHeaderParams);
          PaintHeader(fHeaderParams, PaintInfo);
        end;

    begin
      with PaintInfo, PageInfo do
        TraverseVisibleRows(@DoRow, Vert, AllAxis, Target);
    end;



(*

    procedure DoColumn(aCol, aLeft, aRight, aLineWidth: Integer; var Stop: Boolean); far;
    var
      R, T: TRect;
    var
      IsDown: Boolean;
    begin
      IsDown := False;
      with PaintInfo do
      begin
        R.Left := aLeft;
        R.Top := Y;
        R.Right := aRight + 1; //#windows rect shit
        // als spanned dan wordt alleen de eerste aangeroepen (zie onder)
//        Log([fgridrect.right, fclientwidth]);
        if CurrentHeader.Spanned then R.Right := PageInfo.Horz.GridSize - aLineWidth;//asdf//fGridRect.Right;//ClientWidth;
        R.Bottom := R.Top + CurrentHeader.Size{ - 1};
        T := R;
        if not RectsDoIntersect(R, Target) then Exit;
        { vul headerparams met goede waarden }
        with fHeaderParams do
        begin
          hpCol := aCol;
          hpRow := -1;
          hpHeader := CurrentHeader;
          hpHeaderBounds := R;
//          Inc(hpHeaderBounds.Right); // # windows rect shit
  //        Inc(hpHeaderBounds.Bottom); // # windows rect shit
        end;
        PreparePaintHeader(fHeaderParams);
        PaintHeader(fHeaderParams, PaintInfo);
//        if not CurrentHeader.Spanned then


        { trek verticale lijnen door }
{        if aLineWidth > 0 then
        begin
          TargetCanvas.Brush.Color := fDefaultHeaderLineColor;
          TargetCanvas.Pen.Color := fDefaultHeaderLineColor;
          R.Left := R.Right;
          R.Right := R.Left + aLineWidth;//CurrentHeader.fLineWidth;
          TargetCanvas.FillRect(R);
        end; }

        // teken een nep topleft header in de stijl van de topheader als er ook leftheaders zijn
        // + lijntje eronder
        // moeilijk!
        if aCol = fTopLeft.X then
        if isVisibleLeftHeaders in fInternalStates then
        begin
          TargetCanvas.Brush.Color := fHeaderParams.hpBackColor;
          T.Right := fLeftHeaders.Size - fLeftHeaders[fLeftHeaders.Count - 1].LineWidth;
          T.Left := 0;
          DrawHeader(TargetCanvas, T, False, True, fHeaderParams.hpHeader.Style.Flat);
          {
          if CurrentHeader.LineWidth > 0 then
          begin
            TargetCanvas.Brush.Color := fDefaultHeaderLineColor;
            T.Top := T.Bottom;
            T.Bottom := T.Top + CurrentHeader.LineWidth;
            TargetCanvas.FillRect(T);
          end;
          }
        end;

      end;
    end;

    procedure DoRow(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far;
    var
      R: TRect;
    begin
      with PaintInfo do
      begin
        R.Left := X;
        R.Top := aTop;
        R.Right := R.Left + CurrentHeader.Size;
        R.Bottom := aBottom + 1; //#windows rect shit
        // als spanned dan wordt alleen de eerste aangeroepen (zie onder)
        if CurrentHeader.Spanned then R.Bottom := PageInfo.Vert.GridSize - aLineWidth;//fClientHeight;

        { vul headerparams met goede waarden }
        with fHeaderParams do
        begin
          hpCol := -1;
          hpRow := aRow;
          hpHeader := CurrentHeader;
          hpHeaderBounds := R;
        end;
        PreparePaintHeader(fHeaderParams);
        PaintHeader(fHeaderParams, PaintInfo);


        { trek horizontale lijnen door }
        {if aLineWidth > 0 then
        begin
          TargetCanvas.Brush.Color := fDefaultHeaderLineColor;
          //TargetCanvas.Pen.Color := fDefaultHeaderLineColor;
          R.Top := R.Bottom;
          R.Bottom := R.Top + aLineWidth;//CurrentHeader.fLineWidth;
          TargetCanvas.FillRect(R);
        end;
        }


      end;
    end;

*)
(*
var
  i: Integer;
  R: TRect;
  W, H: Integer;
  DummyStop: Boolean; *)


begin
  { check of paint nodig }
  if not InState(isVisibleHeaders) then
    Exit;
  { todo finetunen of we moeten tekenen }
  if RectFitsIn(fUpdateRect, fGridRect) then
    Exit;

  if InState(isVisibleTopHeaders) then
    TraverseVisibleTopHeaders(@DoTopHeader);

  if InState(isVisibleLeftHeaders) then
    TraverseVisibleLeftHeaders(@DoLeftHeader);


(*
  exit;



  FirstColumnPainted := False;//True;//True;
  FirstRowPainted := False;//True;

  with PaintInfo, PageInfo, TargetCanvas do
  begin

    {-------------------------------------------------------------------------------
      TopHeaders (horizontaal)
    -------------------------------------------------------------------------------}
    if InState(isVisibleTopHeaders) then //and RectsDoIntersect(Target, fHeaderAreas.hTop) then
    begin
      Y := 0;
      for H := 0 to fTopHeaders.Count - 1 do
      begin
        CurrentHeader := fTopHeaders[H];
        TopLeftFlat := TopLeftFlat and CurrentHeader.Style.Flat;
        if TopLeftColor = clNone then TopLeftColor := CurrentHeader.Style.Colors.BackColor;
        case CurrentHeader.Spanned of
          False:
            TraverseVisibleCols(@DoColumn, PageInfo.Horz, AllAxis);
          True:
            TraverseVisibleCols(@DoColumn, PageInfo.Horz, MakeAxisRange(0, 0));
        end; // case
        Inc(Y, CurrentHeader.Size);

        { horizontale lijn onder de topheader }
        {if CurrentHeader.LineWidth > 0 then
        begin
          TargetCanvas.Brush.Color := fDefaultHeaderLineColor;
          R.Left := fGridRect.Left;
          R.Top := Y;
          R.Right := PageInfo.Horz.GridSize;//fGridRect.Right;// + 1;//PageInfo.Horz.GridSize;
          R.Bottom := R.Top + CurrentHeader.LineWidth;
          TargetCanvas.FillRect(R);
        end;}


{        if (H = 0) and InState(isVisibleLeftHeaders) then
          DrawHorzline(0, Y, PageInfo.Horz.GridSize - 1, Y + CurrentHeader.fLineWidth - 1)
        else
          DrawHorzline(fGridRect.Left, Y, PageInfo.Horz.GridSize - 1, Y + CurrentHeader.fLineWidth - 1); }
        Inc(Y, CurrentHeader.fLineWidth);
      end;
    end; // topheaders

    {-------------------------------------------------------------------------------
      LeftHeaders (vertikaal)
    -------------------------------------------------------------------------------}
    if InState(isVisibleLeftHeaders) and RectsDoIntersect(Target, fHeaderAreas.hLeft) then
    begin
      X := 0;
      for H := 0 to fLeftHeaders.Count - 1 do
      begin
        CurrentHeader := fLeftHeaders[H];
        TopLeftFlat := TopLeftFlat and CurrentHeader.Style.Flat;
        if TopLeftColor = clNone then TopLeftColor := CurrentHeader.Style.Colors.BackColor;
        case CurrentHeader.Spanned of
          False:
            TraverseVisibleRows(@DoRow, PageInfo.Vert, AllAxis, AllWindow);
          True:
            TraverseVisibleRows(@DoRow, PageInfo.Vert, MakeAxisRange(0,0), AllWindow);
        end; // case
        Inc(X, CurrentHeader.fSize);
        { verticale lijn rechts van de leftheader }
{        if CurrentHeader.LineWidth > 0 then
        begin
          TargetCanvas.Brush.Color := fDefaultHeaderLineColor;
          R.Left := X;
          R.Top := 0;
          R.Right := R.Left + CurrentHeader.LineWidth;
          R.Bottom := PageInfo.Vert.GridSize;//fGridRect.Bottom;// + 1;//PageInfo.Vert.GridSize;
          TargetCanvas.FillRect(R);
        end; }
        Inc(X, CurrentHeader.fLineWidth);
      end;
    end; // leftheaders

  end; // with
*)

end;

procedure TBaseMatrix.PaintLines(const PaintInfo: TPaintInfo);
{-------------------------------------------------------------------------------
  Het tekenen van lijnen gebeurt dmv rectangles om alle shit met lijnen te
  vermijden
-------------------------------------------------------------------------------}
var
  ExtendLeftHeaderLines, ExtendTopHeaderLines: Boolean;
  Opt: TMxPaintOptions;

  procedure DoHorzLine(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far;
  var
    R: TRect;
  begin
    with PaintInfo, PageInfo, TargetCanvas do
    begin
      R := Rect(fGridRect.Left, aBottom + 1, Horz.GridSize, aBottom + 1 + aLineWidth); {#windows rect shit}
      if R.Top > Target.Bottom then Stop := True;
      if aLineWidth > 0 then
      begin
        // normale lijn
        if RectsDoIntersect(R, Target) then
        begin
          Brush.Color := fDefaultLineColor;
          FillRect(R);
        end;
        // doortrekken headerlines
        if ExtendLeftHeaderLines then
        begin
          R.Right := R.Left;
          R.Left := 0;
          Brush.Color := fDefaultHeaderLineColor;
          FillRect(R);
        end;
      end;
    end;
  end;

  procedure DoVertLine(aCol, aLeft, aRight, aLineWidth: Integer; var Info: DWORD); far;
  var
    R: TRect;
  begin
    with PaintInfo, PageInfo, TargetCanvas do
    begin                   //###headers
      R := Rect(aRight + 1, fGridRect.Top , aRight + 1 + aLineWidth, Vert.GridSize); {#windows rect shit};
//      if R.Top > Target.Bottom then Stop := True;
      if aLineWidth > 0 then
      begin
        // normale lijn
        if RectsDoIntersect(R, Target) then
        begin
          Brush.Color := fDefaultLineColor;
          FillRect(R)
        end;
        // doortrekken headerlines
        if ExtendTopHeaderLines then
        begin
          R.Bottom := R.Top;
          R.Top := 0;
          Brush.Color := fDefaultHeaderLineColor;
          FillRect(R);
        end;
      end;
    end;
  end;

  procedure DoLeftHeaderLine(aHeader: TMatrixHeader; aLeft, aRight, aLineWidth: Integer; var TraverseInfo: DWORD); far;
  // vertikale lijn rechts van leftheader
  var
    R: TRect;
    y: Integer;
  begin
    with PaintInfo, PageInfo, TargetCanvas do
    begin
      if aLineWidth > 0 then
      begin
        if TraverseInfo and TRAVERSE_LAST <> 0 then // laatste (laatste moet helemaal naar boven)
          y := 0
        else
          y := fGridRect.Top;
        R := Rect(aRight + 1, y, aRight + 1 + aLineWidth, Vert.GridSize) {#windows rect shit};
        Brush.Color := fDefaultHeaderLineColor;
        FillRect(R);
      end;
    end;
  end;

  procedure DoTopHeaderLine(aHeader: TMatrixHeader; aTop, aBottom, aLineWidth: Integer; var TraverseInfo: DWORD); far;
  // horizontale lijn onder leftheader
  var
    R: TRect;
  begin
    with PaintInfo, PageInfo, TargetCanvas do
    begin
      if aLineWidth > 0 then
      begin
        R := Rect(0{fGridRect.Left}, aBottom + 1, Horz.GridSize, aBottom + 1 + aLineWidth) {#windows rect shit};
        Brush.Color := fDefaultHeaderLineColor;
        FillRect(R);
      end;
    end;
  end;


begin
  Opt := fOptions.fPaintOptions;
  if [poHorzLines, poVertLines] * Opt = [] then
    Exit;

    {  if not RectsDoInterSect(PaintInfo.Target, fGridRect) then
    Exit; }

  ExtendLeftHeaderLines := isVisibleLeftHeaders in fInternalStates;
  ExtendTopHeaderLines := isVisibleTopHeaders in fInternalStates;

  with PaintInfo, PageInfo do
  begin
    { horizontale lijnen }
    if not (poTransparentHorzLines in Opt) then
      if poHorzLines in Opt then
        TraverseVisibleRows(@DoHorzLine, Vert, AllAxis, Target{AllWIndow});
    { vertikale lijnen }
    if not (poTransparentVertLines in Opt) then
      if poVertLines in Opt then
        TraverseVisibleCols(@DoVertLine, Horz, AllAxis);


    { horizontale headerlijnen }
    if poHeaderHorzLines in Opt then
      TraverseVisibleTopHeaders(@DoTopHeaderLine);

    { vertikale headerlijnen }
    if poHeaderVertLines in Opt then
      TraverseVisibleLeftHeaders(@DoLeftHeaderLine);
  end;

end;

procedure TBaseMatrix.PaintLinesEx(const PaintInfo: TPaintInfo);
{-------------------------------------------------------------------------------
  Het tekenen van lijnen gebeurt dmv rectangles om alle shit met lijnen te
  vermijden
-------------------------------------------------------------------------------}
var
  R: TRect;

  procedure DoHorzLine(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far;
  begin
    with PaintInfo, PageInfo, TargetCanvas do
    begin
      if aLineWidth > 0 then
        if Between(aBottom + 1, Target.Top, Target.Bottom) then
        begin
          MoveTo(fGridRect.Left, aBottom + 1);
          LineTo(fGridRect.Right, aBottom + 1)
        end;
      if aTop > Target.Bottom then
        Stop := True;
    end;
  end;

  procedure DoVertLine(aCol, aLeft, aRight, aLineWidth: Integer; var Info: DWORD); far;
  begin
    with PaintInfo, PageInfo, TargetCanvas do
    begin
//      R := Rect(aRight + 1, fGridRect.Top {fCellOffsetY}, aRight + 1 + aLineWidth, Vert.GridSize); {#windows rect shit};
      if aLineWidth > 0 then
        if Between(aRight + 1, Target.Left, Target.Right) then
        begin
          MoveTo(aRight + 1, fGridRect.Top);
          LineTo(aRight + 1, fGridRect.Bottom);
        end;
      if aRight > Target.Right then
        Info := Info or TRAVERSE_BREAK;
        //Stop := True;
    end;
  end;

  procedure InitPen;
  var
    ALogBrush: LOGBRUSH;
//    AStyle: array of integer{TIntegerDynArray};   dbgrids
    aStyle: array[0..1] of Integer;
  begin
    ALogBrush.lbStyle := BS_SOLID;
    ALogBrush.lbColor := DIB_RGB_COLORS;
  //  SetLength(AStyle, 2);
    AStyle[0] := 0;
    AStyle[1] := 2;
    PaintInfo.TargetCanvas.Pen.Handle := ExtCreatePen(PS_GEOMETRIC or PS_USERSTYLE, 1,
      ALogBrush, 2, @AStyle);
  end;

begin
  if [poHorzLines, poVertLines] * Options.PaintOptions = [] then
    Exit;
  with PaintInfo, PageInfo do
  begin
    InitPen;
    { horizontale lijnen }
    if not (poTransparentHorzLines in fOptions.PaintOptions) then
      if poHorzLines in fOptions.PaintOptions then
        TraverseVisibleRows(@DoHorzLine, Vert, AllAxis, AllWindow);
    { vertikale lijnen }
    if not (poTransparentVertLines in fOptions.PaintOptions) then
      if poVertLines in fOptions.PaintOptions then
        TraverseVisibleCols(@DoVertLine, Horz, AllAxis);
  end;

end;

procedure TBaseMatrix.PaintLock;
begin
  Inc(fPaintLock);
end;

function TBaseMatrix.PaintLocked: Boolean;
begin
  Result := fPaintLock > 0;
end;

procedure TBaseMatrix.PaintMatrix(aCanvas: TCanvas; aWindow: TRect);
var
  PaintInfo: TPaintInfo;
  x, y: Integer;

begin
  MatrixBitmap.Canvas.Lock;
  CellBitmap.Canvas.Lock;
  HeaderBitmap.Canvas.Lock;

  StateEnter(isPaintingMatrix);
  try
    { zorg dat de drawbitmap groot genoeg is }
    BitmapResize(MatrixBitmap, fClientWidth, fClientHeight);
    { zet paintinfo voor andere methods }
    PaintInfo.TargetCanvas := MatrixBitmap.Canvas;
    PaintInfo.Target := aWindow;
    PaintInfo.Bitmap := MatrixBitmap;
    { bereken huidige page }
    CalcPageInfo(PaintInfo.PageInfo);
    PaintBackGround(PaintInfo);
    PaintLines(PaintInfo); // eerst de lijnen omdat de spanned headers eroverheen gaan
    PaintHeaders(PaintInfo);
    PaintCells(PaintInfo);
    //CustomAfterPaint(PaintInfo);
    { kopieer naar zichtbare canvas }
    CopyCanvas(aCanvas, aWindow, MatrixBitmap.Canvas, aWindow);
//    BitBlt(aCanvas.Handle, aWindow, MatrixBitmap.Handle, aWindow);
  finally
    StateLeave(isPaintingMatrix);
    MatrixBitmap.Canvas.UnLock;
    CellBitmap.Canvas.UnLock;
    HeaderBitmap.Canvas.UnLock;
  end;

end;

procedure TBaseMatrix.PaintSizingLines(MouseX, MouseY: Integer; DoHide: Boolean);
{ wordt alleen gebruikt bij muis resizing van rij, kolomm of cel}
var
  OldPen: TPen;
begin
  raise exception.create('paintsizinglines');
  (*
  OldPen := TPen.Create;
  with Canvas do
  try
    { save pen van canvas }
    OldPen.Assign(Pen);
    try
    Pen.Color := clBlack;
    Pen.Style := psDot;
    Pen.Mode := pmXor;
    Pen.Width := 1;

    { hide vorige }
    if not EqualRects(fSizingLinesRect, EmptyRect) then
    begin
      with fSizingLinesRect do
      begin
        if Left = Right then // colsizing
        begin
          MoveTo(Left, Top);
          LineTo(Left, Bottom);
        end
        else if Top = Bottom then  // rowsizing
        begin
          MoveTo(Left, Top);
          LineTo(Right, Top);
        end
        else begin
          MoveTo(Left, Top);
          LineTo(Right, Top);
          LineTo(Right, Bottom);
          LineTo(Left, Bottom);
          LineTo(Left, Top);
        end;
      end;
//      Update;
    end;

    { show nieuwe }
    if DoHide then
      fSizingLinesRect := EmptyRect
    else begin
      case fMouseInfo.mState of
        mdColSizing:
          begin
            with fSizingLinesRect do
            begin
              Left := MouseX;  Top := 0;  Right := MouseX;  Bottom := fClientWidth;
              MoveTo(Left, Top);
              LineTo(Left, Bottom);
            end;
          end;
        mdRowSizing:
          begin
            with fSizingLinesRect do
            begin
              Left := 0; Top := MouseY;  Right := fClientWidth;  Bottom := MouseY;
              MoveTo(Left, Top);
              LineTo(Right, Top);
            end;
          end;
        mdCellSizing:
          begin
            fSizingLinesRect := fMouseInfo.mResizeRect;
            with fSizingLinesRect do
            begin
              MoveTo(Left, Top);
              LineTo(Right, Top);
              LineTo(Right, Bottom);
              LineTo(Left, Bottom);
              LineTo(Left, Top);
            end;
          end;
      end;
  //    Update;
    end
    finally
      { zet pen terug }
      Pen.Assign(OldPen);
    end;
  finally
    { free lokale pen }
    OldPen.Free;
  end;
  *)
end;

procedure TBaseMatrix.PaintUnlock;
begin
  if fPaintLock > 0 then
  begin
    Dec(fPaintLock);
    if fPaintLock = 0 then
      Invalidate; // todo: invalidate updaterect, zie usplit.pas
  end;
end;

function TBaseMatrix.PointToHeaderIndex(X, Y: Integer; aType: THeaderType; aArea: TValidMatrixArea): Integer;
begin
  Result := 0;
end;

procedure TBaseMatrix.PreparePaintCell(Params: TCellParams; Data: TCellData);
{-------------------------------------------------------------------------------
  1) De methode wordt aangeroepen door PaintMatrix:
    a) cpCol, cpRow en cpCellbounds zijn reeds gevuld
    b) totale prepare
  2) De methode wordt aangeroepen tbv autoheights
    a) cpCol, cpRow zijn reeds gevuld
    b) style en canvas worden prepared tbv berekening

-------------------------------------------------------------------------------}
var
  Mix: Boolean;
begin
  with Params do
  begin
    {-------------------------------------------------------------------------------
      haal juiste cellstyle op en cache zijn instellingen voor snellere toegang
    -------------------------------------------------------------------------------}
    cpActiveStyle := GetCellStyleEx(cpCol, cpRow, cpMatrixStyle, cpColStyle, cpRowStyle);
    cpActiveStyle.SetCached(True);
    with cpActiveStyle do
      cpTextFlags := CalcWinFlags(CachedMultiLine, CachedHorzAlign, CachedVertAlign, CachedWordWrap);
    cpTextParams := EmptyTextParams;

    {-------------------------------------------------------------------------------
      zet cell canvas
    -------------------------------------------------------------------------------}
    cpCellBitmap := CellBitmap;
    cpCellCanvas := CellBitmap.Canvas;
    cpCellCanvas.Font := cpActiveStyle.fFont;

    {-------------------------------------------------------------------------------
      De benodigde preparaties voor autoheights zijn gezet.
      Wanneer we aan het berekenen zijn kunnen we stoppen
    -------------------------------------------------------------------------------}
    if Params.cpCalculating then Exit;

    {-------------------------------------------------------------------------------
      init
    -------------------------------------------------------------------------------}
    Mix := (poMixStyles in fOptions.fPaintOptions) and Assigned(cpColStyle) and Assigned(cpRowStyle);
    cpDrawState := [];
    cpStage := pcsPrepare;
    cpImageIndex := -1;

    {-------------------------------------------------------------------------------
      bepaal rects
    -------------------------------------------------------------------------------}
    cpCanvasRect := ZeroTopLeftRect(cpCellBounds);
    cpContentsRect := cpCanvasRect;
    cpImageRect := EmptyRect;

    with cpActiveStyle do
    begin
      Inc(cpContentsRect.Left, CachedMargin.tLeft);
      Inc(cpContentsRect.Top, CachedMargin.tTop);
      Dec(cpContentsRect.Right, CachedMargin.tRight);
      Dec(cpContentsRect.Bottom, CachedMargin.tBottom);
    end;

    BitmapResize(CellBitmap, cpCanvasRect.Right, cpCanvasRect.Bottom);
    cpImageList := fImages;

    {-------------------------------------------------------------------------------
      bepaal drawstate
    -------------------------------------------------------------------------------}
    if not (csPaintCopy in ControlState) then
    begin
      if fMatrixFocused then
        Include(cpDrawState, dsMatrixFocused);
      if (cpCol = fCol) and (cpRow = fRow) then
        Include(cpDrawState, dsFocused);

      { hover }
      if poHoveringCells in Self.fOptions.fPaintOptions then
        with fMouseInfo, MoveInfo do
//        if InMatrix then
        begin
          if (mHitCol = cpCol) and (mHitRow = cpRow) then
          begin
            if fHotImages <> nil then
              cpImageList := fHotImages;
            Include(cpDrawState, dsHover);
          end;
        end;

      case fSelectMode of
        smRowSelect:
          if (cpRow = fRow) and (cpCol <> fCol) then
            Include(cpDrawState, dsHighlightRow);
        smColRowSelect:
          if ((cpRow = fRow) or (cpCol = fCol)) and not ((cpCol = fCol) and (cpRow = fRow)) then
            cpDrawState := cpDrawState + [dsHighlightCol, dsHighlightRow];
      end;

      if PointInRect(cpCol, cpRow, fSelection) then
        Include(cpDrawState, dsSelection);

    end;

    {-------------------------------------------------------------------------------
      bepaal achtergrond- en tekstkleur ahv state
    -------------------------------------------------------------------------------}
    case Mix of
      False:
        begin
          cpBackColor := GetBkColor(cpActiveStyle, cpDrawState);
          cpTextColor := GetTxtColor(cpActiveStyle, cpDrawState);
        end;
      True:
        begin
          cpBackColor := MixColors(GetBkColor(cpColStyle, cpDrawState), GetBkColor(cpRowStyle, cpDrawState));
          cpTextColor := GetTxtColor(cpActiveStyle, cpDrawState);
        end;
    end; //case

    {-------------------------------------------------------------------------------
       bepaal imageindex
    -------------------------------------------------------------------------------}
    if cpImageList <> nil then
    begin
      cpStage := pcsImageIndex;
      if Assigned(fOnPaintCell) then fOnPaintCell(Self, Params, Data);
      if cpImageIndex >= 0 then
      begin
        Include(cpDrawState, dsGlyph);
        CalcImageRect(cpImageList.Width, cpImageList.Height);
      end;
    end;

    if fRows <> nil then
    begin
      Inc(cpContentsRect.Left, Integer(fRows[cpRow].fIndent) * 8);
    end;
//    Inc(cpContentsRect.Left, 8);

  end;
end;

procedure TBaseMatrix.PreparePaintHeader(Params: THeaderParams);
begin

  with Params do
  begin
    hpHeaderCanvas := HeaderBitmap.Canvas;
    hpCellStyle := hpHeader.Style;
    hpCellStyle.SetCached(True);
    with hpCellStyle do
    begin
      hpTextFlags := CalcWinFlags(CachedMultiLine, CachedHorzAlign, CachedVertAlign, CachedWordWrap);
    end;
    hpTextParams := EmptyTextParams;
    hpDrawState := [];
    hpStage := pcsPrepare;
    hpImageIndex := -1;
    hpHeaderCanvas.Font := hpCellStyle.fFont;

{        with fMouseInfo do
        begin
          IsDown := CurrentHeader.fDownIndex = aCol;
        end; }

    {-------------------------------------------------------------------------------
      bepaal rects
    -------------------------------------------------------------------------------}
    hpCanvasRect := ZeroTopLeftRect(hpHeaderBounds);
    hpContentsRect := hpCanvasRect;
    hpImageRect := EmptyRect;
    InflateRect(hpContentsRect, -2, -2); // ##margin

    {-------------------------------------------------------------------------------
      Init header bitmap
    -------------------------------------------------------------------------------}
    BitmapResize(HeaderBitmap, hpCanvasRect.Right, hpCanvasRect.Bottom);

    {-------------------------------------------------------------------------------
      Init kleuren
    -------------------------------------------------------------------------------}
    hpBackColor := hpCellStyle.CachedColors[MI_BACKCOLOR];
    hpTextColor := hpCellStyle.CachedColors[MI_TEXTCOLOR];


    {-------------------------------------------------------------------------------
       bepaal imageindex
    -------------------------------------------------------------------------------}
    hpImageList := Images;
    if hpImageList <> nil then
    begin
      hpStage := pcsImageIndex;
      if Assigned(fOnPaintHeader) then fOnPaintHeader(Self, Params);
      if hpImageIndex >= 0 then
      begin
        Include(hpDrawState, dsGlyph);
        CalcImageRect(hpImageList.Width, hpImageList.Height);
      end;
    end;


  end;
end;

procedure TBaseMatrix.Reset;
begin

end;

procedure TBaseMatrix.Resize;
begin
//  InvalidatePageInfo; //##herteken bug test
//  fUpdateRect := ClientRect; //#hertekenbug test
  CacheClientSize;
  inherited Resize;
//  CacheClientSize;
end;

function TBaseMatrix.RestrictCol(var aCol: Integer): Boolean;
begin
  Result := Restricted(aCol, 0, fColCount - 1);
end;

function TBaseMatrix.RestrictLeft(var aLeft: Integer): Boolean;
begin
  Result := Restricted(aLeft, 0{fFixedRows}, fColCount - 1);
end;

function TBaseMatrix.RestrictRow(var aRow: Integer): Boolean;
begin
  Result := Restricted(aRow, 0, fRowCount - 1);
end;

function TBaseMatrix.RestrictTop(var aTop: Integer): Boolean;
begin
  Result := Restricted(aTop, 0{fFixedRows}, fRowCount - 1);
end;

procedure TBaseMatrix.RowAdded(Item: TMxRow);
begin
  fRowCount := fRows.Count;
end;

procedure TBaseMatrix.RowDeleting(Item: TMxRow);
begin
  fRowCount := fRows.Count - 1; // item is nu nog niet verwijderd!
end;

function TBaseMatrix.RowVisibility(aRow: Integer; const VertInfo: TPageAxisInfo): Integer;
begin
  {$ifdef check_bugs}
  if VertInfo.Flags and AXISINFOFLAG_VERTICAL = 0 then
    raise Exception.Create('RowVisibility flag error');
  {$endif}

  Result := NOT_VISIBLE;
  with VertInfo do
  begin
    { niet zichtbaar }
    if VisibleCount = 0 then
      Exit;
    if ExistInteger(Indices, aRow, VisibleCount - 1) then
      Result := PARTIAL_VISIBLE;
    if Result >= 1 then
    begin
//      Log(['row is visible', arow]);
      if aRow <= LastFullVisible then
        Result := FULL_VISIBLE;
    end;
  end;
end;

procedure TBaseMatrix.RowVisualChanged(aRow: TMxRow; SomeSizeChanged: Boolean);
begin
  if SomeSizeChanged then
  begin
    PaintLock;
    try
      UpdateAutoBounds;
    finally
      PaintUnlock;
    end
  end
  else InvalidateRow(aRow.Index);
end;

procedure TBaseMatrix.SendKeyDown(var Key: Word;
  Shift: TShiftState);
begin
  KeyDown(Key, Shift);
end;

procedure TBaseMatrix.SetBorderStyle(const Value: TBorderStyle);
begin
  if fBorderStyle = Value then
    Exit;
  fBorderStyle := Value;
  RecreateWnd;
end;

procedure TBaseMatrix.SetBounds(aLeft, aTop, aWidth, aHeight: Integer);
{-------------------------------------------------------------------------------
  Houdt rekening met diverse autosizes.
  als aWidth en aHeight niet door AutoBounds berekend zijn forceer ze alsnog,
  wanneer autowidth of autoheight aan staan.
-------------------------------------------------------------------------------}
begin
  InvalidatePageInfo;

  if not (isAutoBounds in fInternalStates) then
  begin
    if aWidth <> Width then
      if (fVisibleColsRequested <> 0) or (aoAutoWidth in Options.AutoSizeOptions) then
        aWidth := CalcAutoClientWidth + (Width - ClientWidth);
    if aHeight <> Height then
      if (fVisibleRowsRequested <> 0) or (aoAutoHeight in Options.AutoSizeOptions) then
        aHeight := CalcAutoClientHeight + (Width - ClientWidth);
  end;

  inherited SetBounds(aLeft, aTop, aWidth, aHeight);  //controls
  CacheClientSize;
end;

procedure TBaseMatrix.SetCellOffsetX(const Value: Integer);
begin
  if fCellOffsetX = Value then Exit;
  fCellOffsetX := Value;
  PaintLock;
  try
    UpdateGridRect;
    UpdateAutoBounds;
  finally
    PaintUnlock;
  end;
end;

procedure TBaseMatrix.SetCellOffsetY(const Value: Integer);
begin
  if fCellOffsetY = Value then Exit;
  fCellOffsetY := Value;
  PaintLock;
  try
    UpdateGridRect;
    UpdateAutoBounds;
  finally
    PaintUnlock;
  end;
end;

procedure TBaseMatrix.SetCellStyle(const Value: TCellStyle);
begin
  fCellStyle.Assign(Value);
end;

procedure TBaseMatrix.SetCellText(aCol, aRow: Integer; const Value: string);
begin

end;

procedure TBaseMatrix.SetCol(const Value: Integer);
begin
  CheckCol(Value);
  DoNavigate(srNone, scAbsolute, hfpAuto, vfpAuto, Value, fRow);
end;

procedure TBaseMatrix.SetColCount(Value: Integer);
begin
  //if uoColumns in Options.UseOptions then
    //Error('Illegal SetColCount');
//  Restrict(Value, 1, DEF_MAXCOLCOUNT);

  if fColCount = Value then
    Exit;
  PaintLock;
  try
    if uoColumns in Options.UseOptions then
      Columns.Count := Value // TMxColumns.Notification zorgt voor de rest
    else
      fColCount := Value;
    UpdateAutoBounds;
    UpdateScrollPos;
  finally
    PaintUnlock;
  end;
end;

procedure TBaseMatrix.SetColRow(const Value: TPoint);
begin
  CheckCoord(Value);
{  if not HandleAllocated then
  begin
    fCol := Value.X;
    fRow := Value.Y;
    Exit;
  end;}

  DoNavigate(srNone, scAbsolute, hfpAuto, vfpAuto, Value.X, Value.Y);
end;

procedure TBaseMatrix.SetColumns(const Value: TMxColumns);
begin
  if fColumns = nil then
    Exit;
  fColumns.Assign(Value);
end;

procedure TBaseMatrix.SetColWidth(aCol: Integer; Value: Integer);
{-------------------------------------------------------------------------------
  SetColWidth:
  alleen valide wanneer colinfo aanwezig
-------------------------------------------------------------------------------}
begin
  {$ifdef check_bugs}
  if fColumns = nil then Error('SetColwidth Error: Columns = nil');
  {$endif}

  Restrict(Value, MIN_COLWIDTH, MAX_COLWIDTH);
  with fColumns[aCol] do
    if fSize <> Value then
    begin
      fSize := Value;
      UpdateAutoBounds;
      Invalidate;
    end;
end;

procedure TBaseMatrix.SetCustomPainting(const Value: Boolean);
begin
  if fCustomPainting = Value then Exit;
  fCustomPainting := Value;
  Invalidate;
end;

procedure TBaseMatrix.SetData(aCol, aRow: Integer; Data: TCellData);
begin
  DoSetCellData(aCol, aRow, Data);
//  asdf
  //Data.Text := umxstrings
end;

procedure TBaseMatrix.SetDefaultColWidth(Value: Integer);
var
  i: Integer;
begin
  fDefaultColWidth := Value;
  PaintLock;
  Restrict(Value, MIN_COLWIDTH, MAX_COLWIDTH);
  try
    if uoColumns in Options.UseOptions then
      with fColumns do
        for i := 0 to Count - 1 do
          Items[i].fSize := Value;
    UpdateAutoBounds;
  finally
    PaintUnlock;
  end;
end;

procedure TBaseMatrix.SetDefaultHorzLineWidth(const Value: Integer);
var
  i: Integer;
begin
  if (fDefaultHorzLineWidth = Value) and (fRows = nil) then
    Exit;
  fDefaultHorzLineWidth := Value;
  PaintLock;
  try
    if fRows <> nil then
      with fRows do
        for i := 0 to Count - 1 do
          Items[i].fLineWidth := Value;
    UpdateAutoBounds;
  finally
    PaintUnlock;
  end;
end;

procedure TBaseMatrix.SetDefaultLineColor(const Value: TColor);
begin
  if fDefaultLineColor = Value then
    Exit;
  fDefaultLineColor := Value;
  Invalidate;
end;

procedure TBaseMatrix.SetDefaultRowHeight(Value: Integer);
var
  i: Integer;
begin
  fDefaultRowHeight := Value;//todo! restrict
  PaintLock;
  Restrict(Value, MIN_ROWHEIGHT, MAX_ROWHEIGHT);
  try
    if uoRows in Options.UseOptions then
      with fRows do
        for i := 0 to Count - 1 do
          Items[i].fSize := Value;
    UpdateAutoBounds;
  finally
    PaintUnlock;
  end;
end;

procedure TBaseMatrix.SetDefaultVertLineWidth(const Value: Integer);
var
  i: Integer;
begin
  if (fDefaultVertLineWidth = Value) and (fColumns = nil) then
    Exit;
  fDefaultVertLineWidth := Value;
  PaintLock;
  try
    if fColumns <> nil then
      with fColumns do
        for i := 0 to Count - 1 do
          Items[i].fLineWidth := Value;
    UpdateAutoBounds;
  finally
    PaintUnlock;
  end;
end;

procedure TBaseMatrix.SetFocus;
begin
  if not InState(isKillingFocus) then
    inherited SetFocus;
end;

procedure TBaseMatrix.SetHotImages(const Value: TCustomImageList);
begin
  fHotImages := Value;
  Invalidate;
end;

procedure TBaseMatrix.SetImages(const Value: TCustomImageList);
begin
  fImages := Value;//.Assign(Value);
  Invalidate;
end;

procedure TBaseMatrix.SetLeftHeaders(const Value: TMatrixHeaders);
begin
  if fLeftHeaders = nil then
    Exit;
  fLeftHeaders.Assign(Value);  
end;

(*procedure TBaseMatrix.SetMultiCastEventHandler(const Value: TMxMultiCastEventHandler);
begin
  if fMultiCastEventHandler = Value then Exit;
  fMultiCastEventHandler := Value;
  if Value <> nil then
    fMultiCastEventHandler.fMatrix := Self;
end;*)


procedure TBaseMatrix.SetOnGetActiveStyle(const Value: TGetActiveCellStyleEvent);
begin
  fOnGetActiveStyle := Value;
  Invalidate;
end;

procedure TBaseMatrix.SetOnMeasureRow(const Value: TMeasureRowEvent);
begin
  fOnMeasureRow := Value;
  Invalidate;
end;

procedure TBaseMatrix.SetOptions(const Value: TMatrixOptions);
begin
  fOptions.Assign(Value);
end;

procedure TBaseMatrix.SetParent(AParent: TWinControl);
begin
//  if parent=nil then Log(['before parent', clientwidth]);
  inherited;
  //if parent <> nil then Log(['after parent', clientwidth]);
  {CacheClientSize;}
//  if Parent <> nil then
  //  UpdateAutoBounds(True);
//  if Parent <> nil then
  //  fSelector.Parent := Parent;
end;

procedure TBaseMatrix.SetRightHeaders(const Value: TMatrixHeaders);
begin
  if fRightHeaders = nil then
    Exit;
  fRightHeaders.Assign(Value);
end;

procedure TBaseMatrix.SetRow(const Value: Integer);
begin
  CheckRow(Value);
  DoNavigate(srNone, scAbsolute, hfpAuto, vfpAuto, fCol, Value);
end;

procedure TBaseMatrix.SetRowCount(const Value: Integer);
begin
  if fRowCount = Value then
    Exit;
  PaintLock;
  try
    if fRow > Value - 1 then
      Row := 0;
    if uoRows in Options.UseOptions then
      fRows.Count := Value  // TMxRows.Notification zorgt voor de rest
    else
      fRowCount := Value;
    if fRow > fRowCount - 1 then
      if fRowCount > 0 then
        Row := 0;
    UpdateAutoBounds;
    UpdateScrollPos;
  finally
    PaintUnlock;
  end;

end;

procedure TBaseMatrix.SetRowHeight(aRow: Integer; Value: Integer);
begin
  Restrict(Value, MIN_ROWHEIGHT, MAX_ROWHEIGHT);
  with fRows[aRow] do
    if fSize <> Value then
    begin
      fSize := Value;
      UpdateAutoBounds;
      Invalidate;
    end;
end;

procedure TBaseMatrix.SetRows(const Value: TMxRows);
begin
  if fRows = nil then
    Exit;//Error('SetRows');
  fRows.Assign(Value);
end;

procedure TBaseMatrix.SetScrollBars(const Value: TScrollStyle);
begin
  if fScrollBars = Value then Exit;
  fScrollBars := Value;
//  Log(['voor', fclientwidth]);
  RecreateWnd;
//  Log(['na', fclientwidth]);
  CacheClientSize;
  InvalidatePageInfo;
//  Invalidate;
  if fScrollBars <> ssNone then
    UpdateScrollPos;
end;

procedure TBaseMatrix.SetSelectMode(const Value: TSelectMode);
begin
  if fSelectMode = Value then Exit;
  fSelectMode := Value;
  Invalidate;
end;

procedure TBaseMatrix.SetTopHeaders(const Value: TMatrixHeaders);
begin
  if fTopHeaders = nil then
    Exit;
  fTopHeaders.Assign(Value);
end;

procedure TBaseMatrix.SetVisibleRows(const Value: Integer);
var
  H, i, Counter: Integer;
  PgInfo: TPageInfo;
begin
  CalcPageInfo(PgInfo);
  H := 0;
  Counter := 0;
  with PgInfo.Vert do
    for i := FirstVisible to FirstVisible + Value do
    begin
      if i > fRowCount - 1 then
        Break;
      Inc(H, Sizes[i].B - Sizes[i].A + 1); // + 1 is voor de volgende cell!
      Inc(H, Lines[i]);
      Inc(Counter);
      if Counter >= Value then
        Break;
    end;

  ClientHeight := H;
//  Log(['hoogte', H]);

end;

procedure TBaseMatrix.SetVisibleColsRequested(const Value: Integer);
begin
  if fVisibleColsRequested = Value then Exit;
  fVisibleColsRequested := Value;
  UpdateAutoBounds;
end;

procedure TBaseMatrix.SetVisibleRowsRequested(const Value: Integer);
begin
  if fVisibleRowsRequested = Value then Exit;
  fVisibleRowsRequested := Value;
  UpdateAutoBounds;
end;

procedure TBaseMatrix.StateChange(Enter, Leave: TInternalStates);
begin
  fInternalStates := fInternalStates + Enter - Leave;
end;

procedure TBaseMatrix.StateEnter(aState: TInternalState);
begin
  StateChange([aState], []);
end;

procedure TBaseMatrix.StateLeave(aState: TInternalState);
begin
  StateChange([], [aState]);
end;

procedure TBaseMatrix.TimerStart(ID: Integer);
begin
  if not HandleAllocated or (csDesigning in ComponentState) then
    Exit;

  {$ifdef check_bugs}
  Inc(TimerCounts);
  {$endif}

  case ID of
    CURSOR_TIMER:
      begin
        SetTimer(Handle, ID, CURSOR_TIMER_INTERVAL, nil);
        Include(fInternalStates, isResizeTimerActive);
      end;
    CLEARSEARCH_TIMER:
      begin
        SetTimer(Handle, ID, CLEARSEARCH_TIMER_INTERVAL, nil);
        StateEnter(isClearSearchTimerActive)
        //Include(fInternalStates, isSearchTimerActive);
      end;
  end;
end;

procedure TBaseMatrix.TimerStop(ID: Integer);
begin
  if not HandleAllocated or (csDesigning in ComponentState) then
    Exit;

  {$ifdef check_bugs}
  Dec(TimerCounts);
  {$endif}

  case ID of
    CURSOR_TIMER:
      begin
        KillTimer(Handle, ID);
        //if isResizeTimerActive in fInternalStates then
        StateLeave(isResizeTimerActive);
      end;
    CLEARSEARCH_TIMER:
      begin
        KillTimer(Handle, ID);
        StateLeave(isClearSearchTimerActive);
//        if isSearchTimerActive in fInternalStates then
  //        Exclude(fInternalStates, isSearchTimerActive);
      end;
  else
    begin
      KillTimer(Handle, ID);
    end;
  end;
end;

procedure TBaseMatrix.TraverseCells(LocalCellProc: pointer; const aRange: TRect);
{-------------------------------------------------------------------------------
  LocalCellProc is een locale(!) procedure met de volgende declaratie:
  procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far;
-------------------------------------------------------------------------------}
var
  x, aCol, y, aRow, HLine, VLine: Integer;
  R, DrawRect: TRect;
  Stop: Boolean;
  CallerBP: Cardinal;
  DrawRectPtr: pointer;
  StopPtr: pointer;
  DoAllRects: Boolean;

begin

  { Set up stack frame for local }
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  DoAllRects := aRange.Left = MinInt;
  Stop := False;
  { pointers om in registers te zetten, moet anders kunnen zie asm hieronder }
  StopPtr := @Stop;
  DrawRectPtr := @DrawRect;

  R := EmptyRect;
//  RectMove(R,
//  R.Top := fCellOffsetY;
  { vertikaal }
//  with PageInfo do
  //for y := 0 to Vert.VisibleCount - 1 do
  for y := 0 to fRowCount - 1 do //#hidden
  begin
    aRow := y;//Vert.Indices[y];
    R.Left := fGridRect.Left;{fCellOffsetX}; //#headers
//    R.Left := fOffsetX;
    R.Bottom := R.Top + GetEffectiveRowHeight(aRow) - 1;
    HLine := GetEffectiveHorzLineWidth(aRow);
    { horizontaal }
    for x := 0 to fColCount - 1 do
    //for x := 0 to Horz.TotalCount - 1 do //#hidden
    begin
      aCol := x;//Horz.Indices[x];
//      if HiddenCols[aCol] then Continue; // #hidden
      VLine := GetEffectiveVertLineWidth(aCol);
      R.Right := R.Left + GetColWidth(aCol) - 1;
      DrawRect := R;
      if DoAllRects or PointInRect(aCol, aRow, aRange) then
      asm
        // = LocalCellProc(aCol, aRow, DrawRect, Stop)
        PUSH CallerBP                     { bewaar stack, deze wordt na de call automatisch gepopt }
        MOV EAX, aCol                     { param 1 in EAX }
        MOV EDX, aRow                     { param 2 in EDX }
        MOV ECX, DWORD PTR [DrawRectPtr]  { param 3 in ECX }
        PUSH DWORD PTR [HLine]            { param 4 op stack }
        PUSH DWORD PTR [VLine]            { param 5 op stack }
        PUSH DWORD PTR [StopPtr]          { variabele param 6 op stack }
        CALL LocalCellProc
        POP ECX // ??? zie borland grids ???
      end;
      if Stop then Exit;
      R.Left := R.Right + VLine + 1;
    end;
    R.Top := R.Bottom + HLine + 1;
  end;

end;

procedure TBaseMatrix.TraverseCols(LocalColProc: pointer; const aRange: TAxisRange);
{-------------------------------------------------------------------------------
  LocalColProc is een locale(!) procedure met de volgende declaratie:
  procedure DoCol(aCol, aLeft, aRight, aLineWidth: Integer; var Stop: Boolean); far;
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  x, aCol, aLeft, aRight, aLineWidth: Integer;
  Stop: Boolean;
  aStopPtr: pointer;
  DoAll: Boolean;
begin

  if aRange.Last < aRange.First then
    Exit;

  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  DoAll := aRange.First = AllAxis.First;

  Stop := False;
  aStopPtr := @Stop;
  aLeft := fGridRect.Left;//fCellOffsetX;//#headers

  for x := 0 to fColCount - 1 do
  begin
    aCol := x;
    aRight := aLeft + GetColWidth(aCol) - 1;
    aLineWidth := GetEffectiveVertLineWidth(aCol);
    if DoAll or Between(aCol, aRange.First, aRange.Last) then
    asm
      PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
      MOV EAX, aCol               { param 1 in EAX }
      MOV EDX, aLeft              { param 2 in EDX }
      MOV ECX, aRight             { param 3 in ECX }
      PUSH DWORD PTR aLineWidth   { param 4 op stack }
      PUSH DWORD PTR [aStopPtr]   { variabele param 5 op stack }
      CALL LocalColProc
      POP ECX
    end;
    if Stop then Exit;
    aLeft := aRight + aLineWidth + 1;
  end;

end;

{-------------------------------------------------------------------------------

  LocalRowProc is een locale(!) procedure met de volgende declaratie:
  procedure DoRow(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far;

-------------------------------------------------------------------------------}
procedure TBaseMatrix.TraverseRows(LocalRowProc: Pointer; const aRange: TAxisRange);
{-------------------------------------------------------------------------------
  TraverseRows: loop door zichtbare rows heen.
  LocalRowProc is een locale(!) procedure met de volgende declaratie:
  procedure DoRow(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far;
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  y, aRow, aTop, aBottom, aLineWidth: Integer;
  Stop: Boolean;
  aStopPtr: pointer;
  DoAll: Boolean;

begin

  if aRange.Last < aRange.First then Exit;

  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  DoAll := aRange.First = AllAxis.First;
  Stop := False;
  aStopPtr := @Stop;
  aTop := fGridRect.Top; // fCellOffsetY; ###headers
//  aTop := fOffsetY;

  for y := 0 to fRowCount - 1 do
  begin
    aRow := y;
    aBottom := aTop + GetEffectiveRowHeight(aRow) - 1;
    aLineWidth := GetEffectiveHorzLineWidth(aRow);
    if DoAll or Between(aRow, aRange.First, aRange.Last) then
    asm
      PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
      MOV EAX, aRow               { param 1 in EAX }
      MOV EDX, aTop               { param 2 in EDX }
      MOV ECX, aBottom            { param 3 in ECX }
      PUSH DWORD PTR aLineWidth   { param 4 op stack }
      PUSH DWORD PTR [aStopPtr]   { variabele param 5 op stack }
      CALL LocalRowProc
      POP ECX
    end;
    if Stop then Exit;
    aTop := aBottom + aLineWidth + 1;
  end;

end;

function TBaseMatrix.TraverseRowHeights(LocalRowProc: Pointer; const aRange: TAxisRange): Integer;
{-------------------------------------------------------------------------------
  TraverseRowHeights: loop door zichtbare rows heen.
  LocalRowProc is een locale(!) procedure met de volgende declaratie:
  procedure DoRow(aRow, aHeight, aLineWidth: Integer; var Stop: Boolean); far;
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  y, aRow, aHeight, aLineWidth: Integer;
  StartY, StopY: Integer;
  Stop: Boolean;
  aStopPtr: pointer;

begin

//  if aRange.Last < aRange.First then Exit;

  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  if aRange.First = AllAxis.First then
  begin
    StartY := 0;
    StopY := fRowCount - 1;
  end
  else begin
    StartY := aRange.First;
    StopY := aRange.Last;
    RestrictRow(StartY);
    RestrictRow(StopY);
  end;

  Stop := False;
  aStopPtr := @Stop;
  Result := 0;

  if StartY <= StopY then
  begin

    for y := StartY to StopY do
    begin
      aRow := y;
      aHeight := GetEffectiveRowHeight(aRow, True);
      aLineWidth := GetEffectiveHorzLineWidth(aRow);
      Inc(Result, aHeight + aLineWidth);
      if LocalRowProc <> nil then
      asm
        PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
        MOV EAX, aRow               { param 1 in EAX }
        MOV EDX, aHeight            { param 2 in EDX }
        MOV ECX, aLineWidth         { param 3 in ECX }
        PUSH DWORD PTR [aStopPtr]   { variabele param 4 op stack }
        CALL LocalRowProc
        POP ECX
      end;
      if Stop then Exit;
    end;

  end
  else begin

    for y := StartY downto StopY do
    begin
      aRow := y;
      aHeight := GetEffectiveRowHeight(aRow, True);
      aLineWidth := GetEffectiveHorzLineWidth(aRow);
      Inc(Result, aHeight + aLineWidth);
      if LocalRowProc <> nil then
      asm
        PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
        MOV EAX, aRow               { param 1 in EAX }
        MOV EDX, aHeight            { param 2 in EDX }
        MOV ECX, aLineWidth         { param 3 in ECX }
        PUSH DWORD PTR [aStopPtr]   { variabele param 4 op stack }
        CALL LocalRowProc
        POP ECX
      end;
      if Stop then Exit;
    end;

  end;

end;

procedure TBaseMatrix.TraverseVisibleCells(LocalCellProc: pointer; const PageInfo: TPageInfo; const aRange: TRect);
{-------------------------------------------------------------------------------
  LocalCellProc is een locale(!) procedure met de volgende declaratie:
  procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far;
-------------------------------------------------------------------------------}
var
  x, aCol, y, aRow, HLine, VLine: Integer;
  R, DrawRect: TRect;
  Stop: Boolean;
  CallerBP: Cardinal;
  DrawRectPtr: pointer;
  StopPtr: pointer;
  DoAllRects: Boolean;
begin
  { Set up stack frame for local }
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  DoAllRects := aRange.Left = MinInt;
  Stop := False;
  { pointers om in registers te zetten, moet anders kunnen zie asm hieronder }
  StopPtr := @Stop;
  DrawRectPtr := @DrawRect;

  R := EmptyRect;
  { vertikaal }
  with PageInfo do
  for y := 0 to Vert.VisibleCount - 1 do
  begin
    aRow := Vert.Indices[y];
    R.Top := Vert.Sizes[y].A;
    R.Bottom := Vert.Sizes[y].B;
    HLine := Vert.Lines[y];
    { horizontaal }
    for x := 0 to Horz.VisibleCount - 1 do
    begin
      aCol := Horz.Indices[x];
      R.Left := Horz.Sizes[x].A;
      R.Right := Horz.Sizes[x].B;
      VLine := Horz.Lines[x];
      DrawRect := R;
      if DoAllRects or PointInRect(aCol, aRow, aRange) then
      asm
        { dit is de call naar LocalCellProc(aCol, aRow, DrawRect, Stop) }
        PUSH CallerBP                     { bewaar stack, deze wordt na de call automatisch gepopt }
        MOV EAX, aCol                     { param 1 in EAX }
        MOV EDX, aRow                     { param 2 in EDX }
        MOV ECX, DWORD PTR [DrawRectPtr]  { param 3 in ECX }
        PUSH DWORD PTR [HLine]            { param 4 op stack }
        PUSH DWORD PTR [VLine]            { param 5 op stack }
        PUSH DWORD PTR [StopPtr]          { variabele param 6 op stack }
        CALL LocalCellProc
        POP ECX // ??? zie borland grids ???
      end;
      if Stop then Exit;
    end;
  end;
end;

procedure TBaseMatrix.TraverseVisibleCols(LocalColProc: pointer; const HorzInfo: TPageAxisInfo; const aRange: TAxisRange);
{-------------------------------------------------------------------------------
  LocalColProc is een locale(!) procedure met de volgende declaratie:
  procedure DoCol(aCol, aLeft, aRight, aLineWidth: Integer; var Info: DWORD); far;
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  x, aCol, aLeft, aRight, aLineWidth: Integer;
  Info: DWORD;
  InfoPtr: pointer;
  DoAll: Boolean;
begin

  Assert(HorzInfo.Flags and AXISINFOFLAG_VERTICAL = 0, 'TraverseVisibleCols');
  if aRange.Last < aRange.First then Exit;

  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  { wanneer AllAxis wordt doorgegeven is dit een sein dat we alle zichtbare
    kolommen door moeten lopen }
  DoAll := aRange.First = AllAxis.First;

//  Stop := False;
  InfoPtr := @Info;
//  aLeft := 0;
//  aLeft := fOffsetX;

  with HorzInfo do
    for x := 0 to VisibleCount - 1 do // #hidden
//    for x := 0 to TotalCount - 1 do // #hidden
    begin
      Info := 0;
      aCol := Indices[x];
      aLeft := Sizes[x].A;
      aRight := Sizes[x].B;
      aLineWidth := Lines[x];
      if x = 0 then Info := Info or TRAVERSE_FIRST;
      if x = VisibleCount - 1 then Info := Info or TRAVERSE_LAST;
      if DoAll or Between(aCol, aRange.First, aRange.Last) then
      asm
        PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
        MOV EAX, aCol               { param 1 in EAX }
        MOV EDX, aLeft              { param 2 in EDX }
        MOV ECX, aRight             { param 3 in ECX }
        PUSH DWORD PTR aLineWidth   { param 4 op stack }
        PUSH DWORD PTR InfoPtr      { variabele param 5 op stack }
        CALL LocalColProc
        POP ECX
      end;
      if Info and TRAVERSE_BREAK <> 0 then Exit;
      //if Stop then Exit;
    end;

end;

procedure TBaseMatrix.TraverseVisibleRows(LocalRowProc: Pointer; const VertInfo: TPageAxisInfo; const aRange: TAxisRange; const aWindow: TRect);
{-------------------------------------------------------------------------------
  TraverseVisibleRows: loop door zichtbare rows heen.
  Parameters:
  1) LocalRowProc:
     Een locale(!) procedure met de volgende declaratie:
     procedure DoRow(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far;
  2) VertInfo:
     Eeen valide inforec
  3) aRange:
     Een range van rijen
     NB: gebruik de const AllAxis voor alle rijen
  4) aWindow:
     Een visueel window waarbinnen de rij moet liggen

  LocalProc wordt aangeroepen voor iedere rij die in aRange ligt en binnen
  aWindow valt.
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  y, aRow, aTop, aBottom, aLineWidth: Integer;
  Stop: Boolean;
  StartY, StopY: Integer;
  aStopPtr: pointer;
  DoAll: Boolean;

begin
  Assert(VertInfo.Flags and AXISINFOFLAG_VERTICAL <> 0, 'TraverseVisibleRows');
  if aRange.Last < aRange.First then Exit;

  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  DoAll := (aRange.First = AllAxis.First) and (aWindow.Left = AllWindow.Left);

  Stop := False;
  aStopPtr := @Stop;
  aTop := 0;

  with VertInfo do
    for y := 0 to VisibleCount - 1 do
    begin
      aRow := Indices[y];
      aTop := Sizes[y].A;
      aBottom := Sizes[y].B;
      aLineWidth := Lines[y];
      if DoAll
      or (
          Between(aRow, aRange.First, aRange.Last)
//          and (Between(aBottom, aWindow.Bottom, aWindow.Top + aLineWidth) or Between(aTop, aWindow.Bottom, aWindow.Top + aLineWidth))
        )
      then
      asm
        PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
        MOV EAX, aRow               { param 1 in EAX }
        MOV EDX, aTop               { param 2 in EDX }
        MOV ECX, aBottom            { param 3 in ECX }
        PUSH DWORD PTR aLineWidth   { param 4 op stack }
        PUSH DWORD PTR [aStopPtr]   { variabele param 5 op stack }
        CALL LocalRowProc
        POP ECX
      end;
      if Stop then Exit;
    end;
end;

procedure TBaseMatrix.TraverseVisibleLeftHeaders(LocalProc: Pointer);
{-------------------------------------------------------------------------------
  1) LocalProc is een locale(!) far procedure met de volgende declaratie:
     DoLeftHeader(aHeader: TMatrixHeader; aLeft, aRight, aLineWidth: Integer; var Info: DWORD); far;
  2) NB: Stop wordt automatisch True bij de laatste header
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  aHeader: TMatrixHeader;
  x, aLeft, aRight, aLineWidth: Integer;
  TraverseInfo: DWORD;
  TraverseInfoPtr: pointer;
begin
  if fLeftHeaders = nil then
    Exit;
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  TraverseInfoPtr := @TraverseInfo;
  aLeft := 0;

  for x := 0 to fLeftHeaders.Count - 1 do
  begin
    TraverseInfo := 0;
    aHeader := fLeftHeaders[x];
    if aHeader.Hidden then
      Continue;
    aRight := aLeft + aHeader.Size - 1;
    aLineWidth := GetEffectiveHeaderLineWidth(aHeader);
    if x = 0 then
      TraverseInfo := TraverseInfo or TRAVERSE_FIRST;
    if x = fLeftHeaders.Count - 1 then
      TraverseInfo := TraverseInfo or TRAVERSE_LAST;
    asm
      PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
      MOV EAX, DWORD PTR aHeader  { param 1 in EAX }
      MOV EDX, aLeft              { param 2 in EDX }
      MOV ECX, aRight             { param 3 in ECX }
      PUSH DWORD PTR aLineWidth   { param 4 op stack }
      PUSH DWORD PTR TraverseInfoPtr { variabele param 5 op stack }
      CALL LocalProc
      POP ECX
    end;
    if TraverseInfo and TRAVERSE_BREAK <> 0 then
      Exit;
    aLeft := aRight + 1 + aLineWidth;
  end;
end;

procedure TBaseMatrix.TraverseVisibleTopHeaders(LocalProc: Pointer);
{-------------------------------------------------------------------------------
  1) LocalProc is een locale(!) far procedure met de volgende declaratie:
     DoTopHeader(aHeader: TMatrixHeader; aTop, aBottom, aLineWidth: Integer; var TraverseInfo: DWORD); far;
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  aHeader: TMatrixHeader;
  y, aTop, aBottom, aLineWidth: Integer;
  TraverseInfo: DWORD;
  TraverseInfoPtr: pointer;
begin
  if fTopHeaders = nil then
    Exit;
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  TraverseInfoPtr := @TraverseInfo;
  aTop := 0;

  for y := 0 to fTopHeaders.Count - 1 do
  begin
    TraverseInfo := 0;
    aHeader := fTopHeaders[y];
    if aHeader.Hidden then
      Continue;
    aBottom := aTop + aHeader.Size - 1;
    aLineWidth := GetEffectiveHeaderLineWidth(aHeader);
    if y = 0 then
      TraverseInfo := TraverseInfo or TRAVERSE_FIRST;
    if y = fTopHeaders.Count - 1 then
      TraverseInfo := TraverseInfo or TRAVERSE_LAST;
    asm
      PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
      MOV EAX, DWORD PTR aHeader  { param 1 in EAX }
      MOV EDX, aTop               { param 2 in EDX }
      MOV ECX, aBottom            { param 3 in ECX }
      PUSH DWORD PTR aLineWidth   { param 4 op stack }
      PUSH DWORD PTR TraverseInfoPtr { variabele param 5 op stack }
      CALL LocalProc
      POP ECX
    end;
    if TraverseInfo and TRAVERSE_BREAK <> 0 then
      Exit;
    aTop := aBottom + 1 + aLineWidth;
  end;
end;

procedure TBaseMatrix.UpdateAutoBounds(Force: Boolean = False);
{-------------------------------------------------------------------------------
  UpdateAutoBounds:
  check of width of height moet veranderen.
-------------------------------------------------------------------------------}
var
  W, H: Integer;
begin
  if isAutoBounds in fInternalStates then Exit; // recursie protectie

  W := ClientWidth;
  H := ClientHeight;

  if (fVisibleColsRequested <> 0) or (aoAutoWidth in Options.AutoSizeOptions)
  then W := CalcAutoClientWidth //(fVisibleColsRequested - 1)
  else W := ClientWidth;

  if (fVisibleRowsRequested <> 0) or (aoAutoHeight in Options.AutoSizeOptions)
  then H := CalcAutoClientHeight //(fVisibleRowsRequested - 1)
  else H := ClientHeight;

  if Force or (W <> ClientWidth) or (H <> ClientHeight) then
  begin
//    PaintLock;
    Include(fInternalStates, isAutoBounds); // preventieve vlag
    try
      ClientWidth := W;
      ClientHeight := H;
//      CacheClientSize;
  //    UpdateGridRect;
      // SetBounds(Left, Top, W, H);
    finally
      Exclude(fInternalStates, isAutoBounds);
    //  PaintUnlock;
    end;
  end;
end;

procedure TBaseMatrix.UpdateBlockCommand(C: Char = #0);
begin
  if C = #0 then
  begin
    if fBlockCommand <> '' then
    begin
      fBlockCommand := '';
//      Log(['clear block command']);
    end;
    Exit;
  end;
  if Length(fBlockCommand) = 0 then
  begin
    fBlockCommand := UpCase(C);
  end
  else if Length(fBlockCommand) = 1 then
  begin
    fBlockCommand := fBlockCommand + UpCase(C);
    if fBlockCommand = 'KB' then
    begin
  //    Log(['begin block']);
      fSelection.Left := fCol;
      fSelection.Top := fRow;
      Invalidate;
    end
    else if fBlockCommand = 'KK' then
    begin
//      Log(['end block']);
      fSelection.Right := fCol;
      fSelection.Bottom := fRow;
      Invalidate;
    end;
    fBlockCommand := '';
  end;
end;


procedure TBaseMatrix.UpdateColumns;
{-------------------------------------------------------------------------------
  Maak of Free kolommen ahv opties
  NB: FreeAndNil(fColumns) leidt tot een error (omdat ColumnDeleting aageroepen wordt
  en op dat moment is fColumns toch al nil gemaakt). Vandaar de hulpvariabele
-------------------------------------------------------------------------------}
var
  i: Integer;
  C: TMxColumns;
begin
  StateEnter(isUpdatingColumns);
  try
    with Options do
      if uoColumns in UseOptions then
      begin
        if fColumns = nil then
          fColumns := DoCreateColumns;
        fColumns.Count := fColCount;
      end
      else begin
        if fColumns <> nil then
        begin
          C := fColumns;
          C.Free;
          fColumns := nil;
        end;
      end;
  finally
    StateLeave(isUpdatingColumns);
  end;
end;

procedure TBaseMatrix.UpdateHeaders;
var
  i: Integer;
begin
  with Options do
  begin
    { top }
    if uoTopHeaders in UseOptions then
    begin
      if fTopHeaders = nil then
        fTopHeaders := TMatrixHeaders.Create(Self, TMatrixHeader, htTop);
    end
    else if fTopHeaders <> nil then
      FreeAndNil(fTopHeaders);

    { left }
    if uoLeftHeaders in UseOptions then
    begin
      if fLeftHeaders = nil then
        fLeftHeaders := TMatrixHeaders.Create(Self, TMatrixHeader, htLeft);
    end
    else if fLeftHeaders <> nil then
      FreeAndNil(fLeftHeaders);

    { right }
    if uoRightHeaders in UseOptions then
    begin
      if fRightHeaders = nil then
        fRightHeaders := TMatrixHeaders.Create(Self, TMatrixHeader, htRight);
    end
    else if fRightHeaders <> nil then
      FreeAndNil(fRightHeaders);
  end;
end;

procedure TBaseMatrix.UpdateHeaderSizes;
begin
  if fTopHeaders <> nil then
    fTopHeaders.UpdateSize;
  if fLeftHeaders <> nil then
    fLeftHeaders.UpdateSize;
end;

procedure TBaseMatrix.UpdateGridRect;
begin
  with fGridRect do
  begin
    Left := fCellOffsetX;
    if fLeftHeaders <> nil then
      Inc(Left, fLeftHeaders.fSize);

    Top := fCellOffsetY;
    if fTopHeaders <> nil then
      Inc(Top, fTopHeaders.fSize);

    Right := fClientWidth;//Left + fPageCache.Horz.GridSize;// - 1;//fClientWidth;
    if fRightHeaders <> nil then
      Dec(Right, fRightHeaders.fSize);

    Bottom := fClientHeight;//Top + fPageCache.Vert.GridSize;;// - 1;//hfClientHeight;

    with fHeaderAreas do
    begin
      if fTopHeaders <> nil then
      begin
        hTop.Left           := fGridRect.Left;
        hTop.Top            := 0;
        hTop.Right          := fGridRect.Right;
        hTop.Bottom         := fTopHeaders.Size - 1;
      end
      else hTop := EmptyRect;

      if fLeftHeaders <> nil then
      begin
        hLeft.Left          := 0;
        hLeft.Top           := fGridRect.Top;
        hLeft.Right         := fLeftHeaders.Size - 1;
        hLeft.Bottom        := fGridRect.Bottom;
      end
      else hLeft := EmptyRect;

      if fRightHeaders <> nil then
      begin
        hRight.Left          := fGridRect.Right + 1;
        hRight.Top           := fGridRect.Top;
        hRight.Right         := fClientWidth;
        hRight.Bottom        := fGridRect.Bottom;
      end
      else hRight := EmptyRect;

      hTopLeft.Left       := 0;
      hTopLeft.Top        := 0;
      hTopLeft.Right      := fGridRect.Left - 1;
      hTopLeft.Bottom     := fGridRect.Top - 1;

    end;
    //Log([rectstr(fGridRect)]);
//    Inc(Top, fTopHeaders.fSize); Inc(Left, fLeftHeaders.fSize);
  end;
{      if fTopHeaders <> nil then begin
        Log([rectstr(fheaderareas.hTop)]);
      end;    }
(*      if fLeftHeaders <> nil then begin
//        Log([fleftheaders.size]);
        Log(['L', rectstr(fheaderareas.hLeft)]);
        Log(['G', rectstr(fgridrect)]);
      end; *)


end;

procedure TBaseMatrix.UpdateRows;
var
  i: Integer;
  R: TMxRows;
begin
  with Options do
    if uoRows in UseOptions then
    begin
      if fRows = nil then
        fRows := DoCreateRows;
      fRows.Count := fRowCount;
    end
    else begin
      if fRows <> nil then
      begin
        R := fRows;
        R.Free;
        fRows := nil;
      end;
    end;
end;

procedure TBaseMatrix.UpdateScrollPos;
var
  Info: TScrollInfo;                //grids
  PgInfo: TPageInfo;
begin
  CalcPageInfo(PgInfo);

  if fScrollBars in [ssVertical, ssBoth] then
  begin
    with Info do
    begin
      cbSize := Sizeof(TScrollInfo);
      fMask := SIF_PAGE;
      nPage := PgInfo.Vert.FullVisibleCount;
    end;
    SetScrollInfo(Handle, SB_VERT, Info, True);
    SetScrollRange(Handle, SB_VERT, 0, RowCount - 1, True);
    SetScrollPos(Handle, SB_VERT, fRow, True);
  end;

  if fScrollBars in [ssHorizontal, ssBoth] then
  begin
    with Info do
    begin
      cbSize := Sizeof(TScrollInfo);
      fMask := SIF_PAGE;
      nPage := 1;
    end;
    SetScrollInfo(Handle, SB_HORZ, Info, True);
    SetScrollRange(Handle, SB_HORZ, 0, ColCount - 1, True);
    SetScrollPos(Handle, SB_HORZ, fCol, True);
  end;
end;

procedure TBaseMatrix.UpdateSelection(aCol, aRow: Integer);
begin
  if (aCol < 0) or (aRow < 0) then
  begin
    InvalidateSelection;
    Exit;
  end;

  with fSelection do
  begin

  end;
end;

function TBaseMatrix.ValidCol(aCol: Integer): Boolean;
begin
  Result := (aCol >= 0) and (aCol < fColCount);
end;

function TBaseMatrix.ValidCoord(aCoord: TPoint): Boolean;
begin
  Result := ValidCoord(aCoord.x, aCoord.y);
end;

function TBaseMatrix.ValidCoord(aCol, aRow: Integer): Boolean;
begin
  Result := (aCol >= 0) and (aCol < fColCount) and (aRow >= 0) and (aRow < fRowCount);
end;

function TBaseMatrix.ValidPageInfo: Boolean;
begin
  with fPageCache do
    Result := Horz.Valid and Vert.Valid;
end;

function TBaseMatrix.ValidRow(aRow: Integer): Boolean;
begin
  Result := (aRow >= 0) and (aRow < fRowCount);
end;

procedure TBaseMatrix.WMEraseBkgnd(var Msg: TWmEraseBkgnd);
begin
  Msg.Result := 1; // geen erase background-onzin
end;

procedure TBaseMatrix.WMGetDlgCode(var Msg: TWMGetDlgCode);
begin
  Msg.Result := DLGC_WANTCHARS or DLGC_WANTARROWS;// or DLGC_WANTTAB;
end;

procedure TBaseMatrix.WMHHcroll(var Msg: TWMHScroll);
begin
  with Msg do
    HandleScrollBarCommand(SB_HORZ, ScrollCode, Pos)
end;

procedure TBaseMatrix.WMKillFocus(var Msg: TWMKillFocus);
begin
  Include(fInternalStates, isKillingFocus);  //grids
  try
    inherited;
    fMatrixFocused := False;
    if not (isEditBeginning in fInternalStates) then
      DoEditEnd;
    InvalidateCell(fCol, fRow);
  finally
    Exclude(fInternalStates, isKillingFocus);
  end;
end;

procedure TBaseMatrix.WMNCPaint(var Message: TRealWMNCPaint);
var
dc : hDc;
Pen : hPen;
OldPen : hPen;
OldBrush : hBrush;

begin

inherited;

(*
dc := GetWindowDC(Handle);

//message.Result := 1;

Pen := CreatePen(PS_SOLID, 1, clblue{RGB(255, 0, 0)});

OldPen := SelectObject(dc, Pen);

OldBrush := SelectObject(dc, GetStockObject(NULL_BRUSH));

Rectangle(dc, 0,0, Width, Height);

SelectObject(dc, OldBrush);

SelectObject(dc, OldPen);

DeleteObject(Pen);

ReleaseDC(Handle, Canvas.Handle);
  *)
end;

procedure TBaseMatrix.WMPaint(var Msg: TWMPaint);
begin
  {to do states inbouwen}
  if csPaintCopy in ControlState then
    fUpdateRect := ClientRect
  else
    GetUpdateRect(Handle, fUpdateRect, False);
  inherited;
end;

procedure TBaseMatrix.WMSetCursor(var Msg: TWMSetCursor);
var
  Cur: HCURSOR;
begin
  inherited;
  Exit;

  (*
  Cur := 0;

{
  TWMSetCursor = packed record
    Msg: Cardinal;
    CursorWnd: HWND;
    HitTest: Word;
    MouseMsg: Word;
    Result: Longint;
  end;
}


//  if Msg.HitTest = HTCLIENT then
  if not (isResizeTimerActive in fInternalStates) then
  case fMouseInfo.mState of
    mdColSizingPossible, mdColSizing:
      begin
        Cur := Screen.Cursors[crSizeWE];
      end;
    mdRowSizingPossible, mdRowSizing:
      begin
        Cur := Screen.Cursors[crSizeNS];
      end;
    mdCellSizingPossible, mdCellSizing:
      begin
        Cur := Screen.Cursors[crSizeNWSE];
      end;
  end;

  if Cur <> 0 then SetCursor(Cur)
  else inherited;
  *)
end;

procedure TBaseMatrix.WMSetFocus(var Msg: TWMSetFocus);
begin
  inherited;
  fMatrixFocused := True;
  InvalidateCell(fCol, fRow);
end;

//----------------------------------------------------------------------------------------------------------------------

procedure TBaseMatrix.WMTimer(var Msg: TWMTimer);
begin
  with Msg do
  begin
    case TimerId of
      CURSOR_TIMER:
        begin
          TimerStop(CURSOR_TIMER);
          if fMouseInfo.ResizeState <> mdNone then
            Perform(WM_SETCURSOR, Handle, HTCLIENT); // forceer cursor
        end;
      CLEARSEARCH_TIMER:
        begin
          TimerStop(CLEARSEARCH_TIMER);
          if fSearchString <> '' then
          begin
            //fSearchChar := fSearchString[1];
            fSearchString := '';//fSearchString[1];
          end;
        end;
      SCROLL_TIMER:
        begin
          //TimerStop(TimerId);
        end;
    end;
  end;
end;

//----------------------------------------------------------------------------------------------------------------------

procedure TBaseMatrix.WMVScroll(var Msg: TWMVScroll);
var
  P: Integer;
begin
{  P := GetScrollPos(Handle, SB_VERT);
  if P <> fRow then
    SetScrollPos(Handle, SB_VERT, fRow, False);
  Log([P]);}

//  if Msg.ScrollCode = SB_THUMBTRACK then
  //  Log([GetRealScrollPosition]);

  with Msg do
    HandleScrollBarCommand(SB_VERT, ScrollCode, Pos)

  {
  TWMScroll = packed record
    Msg: Cardinal;
    ScrollCode: Smallint; // sb_xxxx
    Pos: Smallint;
    ScrollBar: HWND;
    Result: Longint;
  end;
  }



  {
  SB_LINEUP = 0;
  SB_LINELEFT = 0;
  SB_LINEDOWN = 1;
  SB_LINERIGHT = 1;
  SB_PAGEUP = 2;
  SB_PAGELEFT = 2;
  SB_PAGEDOWN = 3;
  SB_PAGERIGHT = 3;
  SB_THUMBPOSITION = 4;
  SB_THUMBTRACK = 5;
  SB_TOP = 6;
  SB_LEFT = 6;
  SB_BOTTOM = 7;
  SB_RIGHT = 7;
  SB_ENDSCROLL = 8;
  }


//  Log([Msg.viewer=handle{'scroll v'}]);  umisc ulog
end;

procedure TBaseMatrix.aaa;
var
  i: integer;

  procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far;
  begin
    inc(i);
    with canvas do
    begin
//      pen.Color := clred;
      setpixel(handle, aCellRect.left, aCellRect.top, clred);
      setpixel(handle, aCellRect.right, aCellRect.bottom, clgreen);
      (*
      if odd(i) then brush.Color := clred else brush.color:=clyellow;
      fillrect(acellrect);
      stop := MessageDlgPos(i2s(aCol) +':' + i2s(aRow) + Chr(13) +
                'Stoppen met canvas test?', mtConfirmation, [mbyes, mbno], 0, 0, 0) = mryes;
      *)
    end;
  end;

  procedure DoRow(aRow, aHeight, aLineWidth: Integer; var Stop: Boolean); far;
  begin
//    Log([aRow, aHeight, aLineWidth]);
  end;

  procedure DoLeftHeader(aHeader: TMatrixHeader; aLeft, aRight, aLineWidth: Integer; var Traverse: DWORD); far;
  var
    r: trect;
  begin
    //r := rect(aLeft, 0, aRight, fClientWidth);
    //canvas.brush.color := clyellow;
  //  canvas.fillrect(r);
//    windlg([aHeader.ClassName, aLeft, aRight, aLineWidth]);

      windlg([bit8str(traverse)]);
      traverse:=traverse or TRAVERSE_BREAK;
      with canvas do begin
      setpixel(handle, aleft, 0, clred);
      setpixel(handle, aright, 0, clyellow); end;
  end;

var
  pginfo: tpageinfo;
  r:trect;
  h:integer;
  newp: tpageinfo;
begin
(*  calcpageinfo(pginfo);
  TraverseVisibleCells(@docell, pginfo, AllCells);

  exit; *)

  TraverseVisibleLeftHeaders(@DoLeftHeader);
  exit;

  align := alnone;
  SetVisibleRows(10);

  exit;

{  h := TraverseRowHeights(@DoRow, MakeAxisRange(100, 120));
  Log(['hoogtes', h]);

  exit; }

  calcpageinfo(pginfo);

  if CalcPgDn(pginfo, newp) then
  begin
//    Log([newp.vert.FirstVisible, newp.vert.LastVisible]);

  end;
//  else Log(['no pagedown']);


exit;

  RowHeights[fRow] := CalculateRowHeight(fRow);
  exit;

(*
    r:=getrowrect(0);
    with canvas do
    begin
      brush.color:=clyellow;
      fillrect(r);
    end;

    exit; *)

  with fHeaderAreas do
  begin
    with canvas do
    begin
      brush.color:=clyellow;
      fillrect(hTopLeft);
    end;
  end;

    exit;
  i := 0;

  CalcPageInfo(pginfo);
  TraverseVisibleCells(@docell, pginfo, AllCells);
{  with canvas do
  begin
    brush.color:=clgreen;//red;
    fillrect(fgridrect);

  end; }
end;

procedure TBaseMatrix.SetTemplate(const aTemplate: TMxTemplate);
begin
  //AssignTemplate(aTemplate);
end;

function TBaseMatrix.GetCellData(aCol, aRow: Integer): TCellData;
begin
  DoGetCellData(aCol, aRow, Result);
end;

procedure TBaseMatrix.CMRecreateWnd(var Message: TMessage);
begin
  inherited;
//  UpdateAutoBounds;
end;


procedure TBaseMatrix.Notification(aComponent: TComponent; Operation: TOperation);
begin
  inherited;
  if Operation = opRemove then
    if aComponent = fImages then
      fImages := nil;
end;




procedure TBaseMatrix.SetDefaultHeaderLineColor(const Value: TColor);
begin
  if fDefaultHeaderLineColor = Value then Exit;
  fDefaultHeaderLineColor := Value;
  Invalidate;
end;



procedure TBaseMatrix.ForceAuto;
begin
  updateautobounds(true);
end;


procedure TBaseMatrix.SaveAsTemplate(const aFileName: string);
var
  T: TMxTemplate;
  F: TFileStream;
begin
(*  T := TMxTemplate.Create(nil);
  try
    T.Matrix.Assign(Self);
    T.SaveToFile(aFileName);
  finally
    T.Free;
  end; *)
  ComponentToTextFile(Self, ReplaceFileExt(aFileName, '.tst'));
  F := TFileStream.Create(aFileName, fmCreate);
  try
    F.WriteComponent(Self);
  finally
    F.Free;
  end;

end;

procedure TBaseMatrix.LoadFromTemplate(const aFileName: string);
var
  T: TMxTemplate;
  F: TFileStream;
begin
{  T := TMxTemplate.Create(nil);
  try
    T.LoadFromFile(aFileName);
    Assign(T.Matrix);
  finally
    T.Free;
  end; }
  F := TFileStream.Create(aFileName, fmOpenRead);
  try
    F.ReadComponent(Self);
  finally
    F.Free;
  end;
end;

procedure TBaseMatrix.Zoom(aRatio: Extended);
var
  Calc: Extended;
  i: Integer;

    function Z(N: Integer): Integer;
    begin
      Result := Round(N * aRatio);
    end;

begin
  fZoomRatio := fZoomRatio * aRatio;
  //windlg([fzoomratio]);

  CellStyle.Font.Size := Z(CellStyle.Font.Size);

//  Width := Z(Width);
  //Height := Z(Height);

  if fTopHeaders <> nil then
    for i := 0 to fTopHeaders.Count - 1 do
    begin
      with TopHeaders[i] do
      begin
        Size := Z(Size);
      end;
    end;

  if fRows <> nil then
  begin
    for i := 0 to RowCount - 1 do
    begin
      RowHeights[i] := Z(RowHeights[i]);
    end
  end
  else DefaultRowHeight := Z(DefaultRowHeight);

  if fColumns <> nil then
  begin
    for i := 0 to ColCount - 1 do
      with Columns[i] do
      begin
        Width := Z(Width);
//        LineWidth := Z(LineWidth);
//        windlg([acp_Font in fStyle. fAssignedCellProps]);
        if fStyle <> nil then
        begin
          fStyle.SetCached(False);
          if acp_Font in fStyle. fAssignedCellProps then
          fStyle.Font.Size := Z(fStyle.Font.Size);
        end;
//        windlg([acp_Font in fStyle. fAssignedCellProps]);
      end
  end
  else DefaultColWidth := Z(DefaultColWidth);


  if fLeftHeaders <> nil then
    for i := 0 to fLeftHeaders.Count - 1 do
    begin
      with LeftHeaders[i] do
      begin
        Size := Z(Size);
      end;
    end;




end;

{ TMxTemplate }

procedure TMxTemplate.Check;
begin
  if fMatrix = nil then
    raise Exception.Create('TMxTemplate error');
end;

constructor TMxTemplate.Create(aOwner: TComponent);
begin
  inherited;
  fMatrix := TBaseMatrix.Create(Self);
end;

destructor TMxTemplate.Destroy;
begin
  inherited;
end;

procedure TMxTemplate.LoadFromFile(const aFileName: string);
var
  F: TFileStream;
begin
  Check;
  F := TFileStream.Create(aFileName, fmOpenRead);
  try
    LoadFromStream(F);
  finally
    F.Free;
  end;
end;

procedure TMxTemplate.LoadFromStream(S: TStream);
begin
  Check;
  S.ReadComponent(fMatrix);
end;

procedure TMxTemplate.SaveToFile(const aFileName: string);
var
  F: TFileStream;
begin
  Check;
  F := TFileStream.Create(aFileName, fmCreate);
  try
    SaveToStream(F);
    ComponentToTextFile(Matrix, ReplaceFileExt(aFileName, '.txt'));
  finally
    F.Free;
  end;
end;

procedure TMxTemplate.SaveToStream(S: TStream);
begin
  Check;
  S.WriteComponent(fMatrix);
end;




procedure TMxTemplate.SetMatrix(const Value: TBaseMatrix);
begin
  fMatrix := Value;
end;

initialization
  InitializeGlobals;
finalization
  FinalizeGlobals;
end.



(*{ TMxEventHandler }

procedure TMxEventHandler.ComponentError;
begin
  raise Exception.Create('Handler not assigned');
end;

procedure TMxEventHandler.SetOnSelectCell(const Value: TCellEvent);
begin
//  if fHandler = nil then ComponentError;
  fOnSelectCell := Value;
end;

{ TMxEventHandlerCollection }

constructor TMxEventHandlerCollection.Create(aOwner: TPersistent);
begin
  inherited Create(aOwner, TMxEventHandler);
end;

function TMxEventHandlerCollection.GetItem(aIndex: Integer): TMxEventHandler;
begin
  Result := TMxEventHandler(inherited GetItem(aIndex))
end;

procedure TMxEventHandlerCollection.SetItem(aIndex: Integer; const Value: TMxEventHandler);
begin
  inherited SetItem(aIndex, Value)
end;

{ TMxMultiCastEventHandler }

constructor TMxMultiCastEventHandler.Create(aOwner: TComponent);
begin
  inherited;
  fHandlerCollection := TMxEventHandlerCollection.Create(Self);
end;

destructor TMxMultiCastEventHandler.Destroy;
begin
  fHandlerCollection.Free;
  inherited;
end;

function TMxMultiCastEventHandler.GetHandler(Index: Integer): TMxEventHandler;
begin
  Result := fHandlerCollection.GetItem(Index);
end;

procedure TMxMultiCastEventHandler.HandleOnSelectCell(aCol, aRow: Integer);
var
  i: Integer;
begin
  with fHandlerCollection do
    for i := 0 to Count - 1 do
      with Items[i] do
        if Assigned(fOnSelectCell) then fOnSelectCell(fMatrix, aCol, aRow);
end;

procedure TMxMultiCastEventHandler.SetHandler(Index: Integer; const Value: TMxEventHandler);
begin

end;

procedure TMxMultiCastEventHandler.SetHandlerCollection(const Value: TMxEventHandlerCollection);
begin
  fHandlerCollection.Assign(Value);
end;

procedure TMxMultiCastEventHandler.SetMatrix(const Value: TBaseMatrix);
begin
  fMatrix := Value;
  fHandlerCollection.fMatrix := Value;
  if Value <> nil then
    fMatrix.MultiCastEventHandler := Self;
end;

function TMxNavigator.Navigation(aReason: TScrollReason; aCommand: TScrollCommand; XParam, YParam: Integer): TNavigationResult;
var
  OldInfo, TempInfo: TPageInfo;
  OldCol, OldRow, OldRowVisibility, OldColVisibility: Integer;
  OldCellVisible: Boolean;
  NewRowVisibility: Integer;
  NewInfo: TPageInfo;
  NewCol, NewRow: Integer;
  DeltaX, DeltaY: Integer;
  ColChanged, RowChanged: Boolean;
  NewTopX, NewTopY: Integer;
  Inv: Integer;

begin
  with fMatrix do
  begin
    Result := [];
    if aCommand = scNone then Exit;

    CalcPageInfo(OldInfo);
    OldCol := fCol;
    OldRow := fRow;
    OldColVisibility := ColVisibility(OldCol, OldInfo.Horz);
    OldRowVisibility := RowVisibility(OldRow, OldInfo.Vert);

    case aCommand of
      scAbsolute:
        begin
          NewCol := XParam;
          NewRow := YParam;
          RestrictCol(NewCol);
          RestrictRow(NewRow);
          if aReason = srClick then
          begin
            InternalSetCol(NewCol, NewCol <> OldCol);
            InternalSetRow(NewRow, NewRow <> OldRow);
          end;
        end;
      scDown:
        begin
          if OldCellVisible then
          begin
            Inc(NewRow);
            RestrictRow(NewRow);
          end;
        end;
      scPgDn:
        begin
          if OldCellVisible then
          begin
            if CalcPgDn(OldInfo, NewInfo) then
            begin
              NewRow := NewInfo.Vert.LastVisible;
              RestrictRow(NewRow);
              InternalSetRow(NewRow, NewRow <> OldRow);
              // repaint
            end;
          end;
        end;
    end;

//    case aReason of
//    end;

(*
  TScrollReason = (
    srNone,
    srData,
    srScrollBar,
    srKeyBoard,
    srClick,
    srMouseWheel
  );

  TScrollCommand = (
    scNone,
    scAbsolute,
    scRight,         // 1 naar rechts
    scLeft,          // 1 naar links
    scDown,          // 1 naar beneden
    scUp,            // 1 naar boven
    scPgDn,          // pagina naar beneden
    scPgUp,          // pagina naar boven
    scHorzStart,     // eerste kolom
    scHorzEnd,       // laatste kolom
    scVertStart,     // eerste rij
    scVertEnd,       // laatste rij
    scBegin,         // eerste cell
    scEnd            // laatste cell
  );
*)
  end;
end;





  TMxNavigator = class(TMatrixObject)
  private
  protected
    function Navigation(aReason: TScrollReason; aCommand: TScrollCommand; XParam: Integer = -1; YParam: Integer = -1): TNavigationResult; virtual;
  public
  published
  end;

// LIJNTJE met pixels om en om
var
  ALogBrush: LOGBRUSH;
  AStyle: TIntegerDynArray;
begin
  ALogBrush.lbStyle := BS_SOLID;
  ALogBrush.lbColor := DIB_RGB_COLORS;
  SetLength(AStyle, 2);
  AStyle[0] := 0;
  AStyle[1] := 2;
  Canvas.Pen.Handle := ExtCreatePen(PS_GEOMETRIC or PS_USERSTYLE, 1,
    ALogBrush, 2, AStyle);
