unit MxMisc;

interface

uses
  Windows, Classes, Sysutils;

const
  EditChars   = ['''', '"', #32..#255];
  SearchChars = ['a'..'z', 'A'..'Z', ' ', '1'..'9', '0'];

  { getallen }
  KiloByte    = 1024;
  MegaByte    = KiloByte * KiloByte;
  GigaByte    = KiloByte * MegaByte;
  KB64        = 64 * KiloByte;
  MinInt      = Low(Integer);
  CrLf        = Chr(13) + Chr(10);

  { bits }
  Bit0  = 1;         {1}
  Bit1  = 1 shl 1;   {2}
  Bit2  = 1 shl 2;   {4}
  Bit3  = 1 shl 3;   {8}
  Bit4  = 1 shl 4;   {16}
  Bit5  = 1 shl 5;   {32}
  Bit6  = 1 shl 6;   {64}
  Bit7  = 1 shl 7;   {128}
  Bit8  = 1 shl 8;   {256}
  Bit9  = 1 shl 9;   {512}
  Bit10 = 1 shl 10;  {1024}
  Bit11 = 1 shl 11;  {2048}
  Bit12 = 1 shl 12;  {4096}
  Bit13 = 1 shl 13;  {8192}
  Bit14 = 1 shl 14;  {16384}
  Bit15 = 1 shl 15;  {32768}
  Bit16 = 1 shl 16;
  Bit17 = 1 shl 17;
  Bit18 = 1 shl 18;
  Bit19 = 1 shl 19;
  Bit20 = 1 shl 20;
  Bit21 = 1 shl 21;
  Bit22 = 1 shl 22;
  Bit23 = 1 shl 23;
  Bit24 = 1 shl 24;
  Bit25 = 1 shl 25;
  Bit26 = 1 shl 26;
  Bit27 = 1 shl 27;
  Bit28 = 1 shl 28;
  Bit29 = 1 shl 29;
  Bit30 = 1 shl 30;
  Bit31 = 1 shl 31;

{ Exception handling algemeen }

type
  EAppError = class(Exception);
  TByteSet    = set of byte;

const
  IR_HIDDEN = Bit0;

type
  TOwnedPersistent = class(TPersistent)
  private
    fOwner: TPersistent;
    //fOnChange: TNotifyEvent;
  protected
    function GetOwner: TPersistent; override;
    procedure CheckOwnerType(aOwner: TPersistent; const Allowed: array of TClass);
    function FindOwner(aClass: TClass): TPersistent;
    function GetMaster: TPersistent;
//    procedure Changed; virtual;
  //  procedure Change(Sender: TObject); virtual; // default eventhandler
  public
    constructor Create(aOwner: TPersistent);
    property Owner: TPersistent read fOwner;
    //property OnChange: TNotifyEvent read fOnChange write fOnChange;
  end;

type
  TItemList = class(TPersistent)
  private
    fItemSize : Integer;
    fList     : Pointer;
    fCount    : Integer;
    fCapacity : Integer;
    fName     : string;
    procedure Grow;
    procedure SetCapacity(NewCapacity: Integer);
    procedure SetCount(const Value: Integer);
  protected
    procedure Check(Index: Integer);
    function DoInsert(Index: Integer; Data: Pointer): Integer;
    function DoAdd(Data: Pointer): Integer;
    procedure DoDelete(Index: Integer);
    function DoGet(Index: Integer): Pointer;
    procedure DoPut(Index: Integer; Data: Pointer);
    procedure DoExchange(A, B: Integer);
    procedure DoMove(aFromIndex, aToIndex: Integer);
    procedure InitializeItem(Data: Pointer); virtual;
    procedure FinalizeItem(Data: Pointer); virtual;
  public
    constructor Create(aItemSize: Integer; const aName: string);
    destructor Destroy; override;
    function GetData(Index: Integer): Pointer;
    procedure PutData(Index: Integer; Data: Pointer);
    function AddData(Data: Pointer): Integer;
    function InsertData(Index: Integer; Data: Pointer): Integer;
    procedure Delete(Index: Integer);
    procedure Exchange(A, B: Integer);
    procedure Move(aFromIndex, aToIndex: Integer);
    property Count: Integer read fCount write SetCount;
    property Capacity: Integer read fCapacity;
    property ItemSize: Integer read fItemSize;
  published
  end;

  TInfoFlags = (
    ifHidden,
    if2, if3, if4, if5, if6, if7, if8,
    if9, if10,if11,if12,if13,if14,if15, if16,
    if17,if18,if19,if20,if21,if22,if23, if24,
    if25,if26,if27,if28,if29,if30,if31, if32
  );

  { record voor kolom en rij info }
  PInfoRec = ^TInfoRec;
  TInfoRec = record
    Size: Integer;
    LineWidth: Integer;
    Flags: DWORD;
  end;

  { dedicated list voor matrix col of row info }
  TInfoList = class(TItemList)
  private
    fDefault: TInfoRec;
  protected
    procedure InitializeItem(Data: Pointer); override;
    procedure FinalizeItem(Data: Pointer); override;
  public
    procedure Assign(Source: TPersistent); override;
    constructor Create(const aName: string);
    function Get(Index: Integer): PInfoRec;
    procedure Put(Index: Integer; const Rec: TInfoRec);
    function Add(const Rec: TInfoRec): Integer;
    function Insert(Index: Integer; const Rec: TInfoRec): Integer;
    procedure ResetAllSizes;
    procedure ResetAllLineWidths;
    property DefaultSize: Integer read fDefault.Size write fDefault.Size;
    property DefaultLineSize: Integer read fDefault.LineWidth write fDefault.LineWidth;
  end;


type
  TCachedIntegers = array of Integer;

procedure AppError(const Msg: string; Sender: TObject = nil);
procedure AppErrorFmt(const Msg: string;  const Args: array of const; Sender: TObject = nil);
procedure PutInteger(var aInts: TCachedIntegers; aIndex: integer; aValue: integer);
function ExistInteger(const aInts: TCachedIntegers; aValue, aLast: integer): boolean;
function i2s(i: integer): string;
procedure Swap(var A, B; Size: integer);
function Between(X, A, B: integer): boolean;
procedure Restrict(var aValue: integer; aMin, aMax: integer);
function Restricted(var aValue: integer; aMin, aMax: integer): Boolean;
function RectHeight(const aRect: TRect): integer;
function RectWidth(const aRect: TRect): integer;
procedure SwapInts(var A, B: integer);
function ZeroTopLeftRect(const aRect: TRect): TRect;
//procedure ZeroTopLeftRect(const aRect: TRect): TRect;
function RectStr(const R: TRect): string;
function EqualPoints(const P1, P2: TPoint): boolean;
function EqualRects(const R1, R2: TRect): boolean;
function HexStr(I: integer): string;
procedure RectMove(var R: TRect; X, Y: integer); {windows.offsetrect}
function CutLeft(const S: string; aLen: integer): string;


implementation

{ TOwnedPersistent }

procedure TOwnedPersistent.CheckOwnerType(aOwner: TPersistent; const Allowed: array of TClass);
var
  i: integer;
  OK: boolean;
  S: string;
begin
  OK := False;
  for i := 0 to Length(Allowed) - 1 do
  begin
    if Allowed[i] <> nil then
      OK := OK or (aOwner is Allowed[i])
    else
      OK := True;
    if OK then
      Break;  
  end;

  if not OK then
  begin
    if aOwner <> nil then S := aOwner.ClassName else S := 'nil';
    //raise Exception.Create('CheckOwnerType: %s is een ongeldig Owner type',[S], Self);

  end;
end;

constructor TOwnedPersistent.Create(aOwner: TPersistent);
begin
  fOwner := aOwner;
end;

function TOwnedPersistent.GetOwner: TPersistent;
begin
  Result := fOwner;
end;

type
  THackedPersistent = class(TPersistent);

function TOwnedPersistent.FindOwner(aClass: TClass): TPersistent;
var
  O: TPersistent;
begin
  O := Self;
  Result := nil;
  while O <> nil do
  begin
    O := THackedPersistent(O).GetOwner;
    if O = nil then
      Exit;
    if O is aClass then
    begin
      Result := O;
      Exit;
    end;
  end;
end;

function TOwnedPersistent.GetMaster: TPersistent;
var
  O: TPersistent;
begin
  Result := nil;
  O := Self;
  while O <> nil do
  begin
    O := THackedPersistent(O).GetOwner;
    if O <> nil then
      Result := O
    else
      Exit;
  end;
  Result := nil;
end;


const
  _Delta = 32;

{ Exception handling }

procedure AppError(const Msg: string; Sender: TObject = nil);
var
  ErrorStr: string;
begin
  if Sender <> nil then
  begin
    ErrorStr := 'Fout in ' + Sender.ClassName;
    ErrorStr := ErrorStr + ': ' + Chr(13) + Msg;
  end
  else ErrorStr := Msg;
  raise EAppError.Create(ErrorStr);
end;

procedure AppErrorFmt(const Msg: string;  const Args: array of const; Sender: TObject = nil);
var
  S: string;
begin
  try S := Format(Msg, Args) except S := Msg; end;
  AppError(S, Sender);
end;

procedure PutInteger(var aInts: TCachedIntegers; aIndex: integer; aValue: integer);
var
  L: integer;
begin
  L := Length(aInts);
  if L <= aIndex then
    SetLength(aInts, aIndex + _Delta);
  aInts[aIndex] := aValue;
end;

function ExistInteger(const aInts: TCachedIntegers; aValue, aLast: integer): boolean;
var
  i: integer;
begin
  Assert(aLast < Length(aInts));
  for i := 0 to aLast do
    if aInts[i] = aValue then
    begin
      Result := True;
      Exit;
    end;
  Result := False;
end;

function i2s(i: integer): string;
begin
  Str(i, Result);
end;

procedure Swap(var A, B; Size: integer);
var
  Temp: pointer;
begin
  GetMem(Temp, Size);
  Move(A, Temp^, Size);
  Move(B, A, Size);
  Move(Temp^, B, Size);
  FreeMem(Temp, Size);
end;

function Between(X, A, B: integer): boolean;
begin
  Result := (X >= A) and (X <= B);
end;

procedure Restrict(var aValue: integer; aMin, aMax: integer);
begin
  if aMin > aMax then SwapInts(aMin, aMax);
  if aValue < aMin then
    aValue := aMin else
  if aValue > aMax then
    aValue := aMax
{  else
    aValue := aMin; }
end;

function Restricted(var aValue: integer; aMin, aMax: integer): Boolean;
begin
  Result := False;
  if aMin > aMax then SwapInts(aMin, aMax);
  if aValue < aMin then
  begin
    Result := True;
    aValue := aMin
  end
  else if aValue > aMax then begin
    Result := True;
    aValue := aMax;
  end;
end;

function RectHeight(const aRect: TRect): integer;
begin
  with aRect do Result := Bottom - Top + 1;
end;

function RectWidth(const aRect: TRect): integer;
begin
  with aRect do Result := Right - Left + 1;
end;

procedure SwapInts(var A, B: integer);
//var
  //C: integer;
begin
  a := a xor b;
  b := b xor a;
  a := a xor b;
//  C := A;  A := B;  B := C;
end;

function ZeroTopLeftRect(const aRect: TRect): TRect;
begin
  Result := aRect;
  with Result do
  begin
    Dec(Right, Left);
    Dec(Bottom, Top);
    Left := 0;
    Top := 0;
  end;
end;

function RectStr(const R: TRect): string;
begin
  Result := i2s(R.Left) + ', ' + i2s(R.Top) + ', ' + i2s(R.Right)+ ', ' + i2s(R.Bottom)
end;

function EqualPoints(const P1, P2: TPoint): boolean;
begin
  Result := (P1.X = P2.X) and (P1.Y = P2.Y);//CompareMem(@P1, @P2, SizeOf(TPoint));
end;

function EqualRects(const R1, R2: TRect): boolean;
begin
  Result := CompareMem(@R1, @R2, SizeOf(TRect));
end;

function HexStr(I: integer): string;
begin
  Result := '$' + Format('%x', [i]);
end;

procedure RectMove(var R: TRect; X, Y: integer);
begin
  with R do
  begin
    if X <> 0 then
    begin
      Inc(R.Left, X);
      Inc(R.Right, X);
    end;
    if Y <> 0 then
    begin
      Inc(R.Top, Y);
      Inc(R.Bottom, Y);
    end;
  end;
end;

function CutLeft(const S: string; aLen: integer): string;
begin
  Result := Copy(S, aLen + 1, Length(S));
end;




{ TItemList }

function TItemList.AddData(Data: Pointer): Integer;
begin
  Result := DoAdd(Data);
end;

function TItemList.InsertData(Index: Integer; Data: Pointer): Integer;
begin
  if (Index < 0) or (Index > fCount) then AppError('insert fout', self);
  Result := DoInsert(Index, Data);
end;

constructor TItemList.Create(aItemSize: Integer; const aName: string);
begin
  fItemSize := aItemSize;
  fName := aName;
end;

procedure TItemList.Grow;
var
  Delta: Integer;
begin
  if fCapacity > 64 then Delta := fCapacity div 4 else
    if fCapacity > 8 then Delta := 16 else
      Delta := 4;
  SetCapacity(fCapacity + Delta);
end;

function TItemList.DoGet(Index: Integer): Pointer;
begin
  Result := Pointer({Cardinal}Integer(fList) + Index * fItemSize);
end;

procedure TItemList.DoPut(Index: Integer; Data: Pointer);
begin
  System.Move(Data^, DoGet(Index)^, fItemSize);
end;

function TItemList.DoAdd(Data: Pointer): Integer;
begin
  if fCount = fCapacity then Grow;
  Inc(fCount);
  System.Move(Data^, DoGet(fCount - 1)^, fItemSize);
  Result := fCount - 1; 
end;

function TItemList.DoInsert(Index: Integer; Data: Pointer): Integer;
begin
  if fCount = fCapacity then Grow;
  if Index < fCount then
    System.Move(DoGet(Index)^, DoGet(Index + 1)^, (fCount - Index) * fItemSize);
  Inc(fCount);
  System.Move(Data^, DoGet(Index)^, fItemSize);
  Result := Index;
end;

procedure TItemList.DoDelete(Index: Integer);
begin
  Check(Index);
  Dec(fCount);
  if Index < fCount then
  begin
    System.Move(DoGet(Index + 1)^, DoGet(Index)^, (fCount - Index) * fItemSize);
  end;
end;

destructor TItemList.Destroy;
begin
  FreeMem(fList);
  inherited;
end;

procedure TItemList.SetCapacity(NewCapacity: Integer);
{var
  OldCapacity: Integer;}
begin
  if NewCapacity = fCapacity then Exit;
  //OldCapacity := fCapacity;
  ReallocMem(fList, NewCapacity * fItemSize);
  fCapacity := NewCapacity;
{  if NewCapacity > OldCapacity then
    FillChar(DoGet(OldCapacity - 1)^, (NewCapacity - OldCapacity) * fItemSize, 0); }
end;

procedure TItemList.Check(Index: Integer);
begin
  if (Index < 0) or (Index > fCount - 1) then
    AppError('TItemList fout (' + fName + ') index parameter = ' + i2s(Index) + ' count = ' + i2s(fcount));
end;

procedure TItemList.SetCount(const Value: Integer);
var
  i: integer;
begin
  if fCount = Value then Exit;
  if Value > fCount then
  begin
    SetCapacity(Value);
    for i := fCount to Value - 1 do
    begin
      InitializeItem(DoGet(i));
    end;
  end
  else if Value < fCount then
  begin
    for i := Value to fCount - 1 do
    begin
      FinalizeItem(DoGet(i));
    end;
  end;
  fCount := Value;
end;

procedure TItemList.Delete(Index: Integer);
begin
  Check(Index);
  DoDelete(Index);
end;

function TItemList.GetData(Index: Integer): Pointer;
begin
  Check(Index);
  Result := DoGet(Index);
end;

procedure TItemList.PutData(Index: Integer; Data: Pointer);
begin
  Check(Index);
  DoPut(Index, Data);
end;

procedure TItemList.DoExchange(A, B: Integer);
begin
  Swap(DoGet(A)^, DoGet(B)^, fItemSize);
end;

procedure TItemList.FinalizeItem(Data: Pointer);
begin

end;

procedure TItemList.InitializeItem(Data: Pointer);
begin

end;

procedure TItemList.Exchange(A, B: Integer);
begin
  DoExchange(A, B);
end;

procedure TItemList.DoMove(aFromIndex, aToIndex: Integer);
var
  Save: Pointer;
begin

  Assert((aFromIndex >= 0) and (aFromIndex < fCount), ClassName + 'DoMove FromIndex error');
  Assert((aToIndex >= 0) and (aToIndex < fCount), ClassName + 'DoMove ToIndex error');
(*
  if aToIndex < 0 then
  begin
    DoDelete(aFromIndex);
    Exit;
  end;
*)



  if aToIndex > aFromIndex then
  begin
    GetMem(Save, fItemSize);
    System.Move(DoGet(aFromIndex)^, Save^, fItemSize);
    DoDelete(aFromIndex);
    DoInsert(aToIndex, Save);
    Freemem(Save);
  end
  else if aToIndex < aFromIndex then begin
    GetMem(Save, fItemSize);
    System.Move(DoGet(aFromIndex)^, Save^, fItemSize);
    DoDelete(aFromIndex);
    DoInsert(aToIndex, Save);
    Freemem(Save);
  end;
end;

procedure TItemList.Move(aFromIndex, aToIndex: Integer);
begin
  DoMove(aFromIndex, aToIndex);
end;

{ TInfoList }

function TInfoList.Add(const Rec: TInfoRec): Integer;
begin
  Result := AddData(@Rec);
end;

procedure TInfoList.Assign(Source: TPersistent);
var
  Src: TInfoList absolute Source;
begin
  if Source is TInfoList then
  begin
    Count := Src.Count;
    System.Move(Src.fList^, fList^, fCount * fItemSize);
{    fItemSize : Integer;
    fList     : Pointer;
    fCount    : Integer;
    fCapacity : Integer;
    fName     : string; }
  end else inherited;
end;

constructor TInfoList.Create(const aName: string);
begin
  inherited Create(SizeOf(TInfoRec), aName);
end;

procedure TInfoList.InitializeItem(Data: Pointer);
begin
  TInfoRec(Data^) := fDefault;
end;

procedure TInfoList.FinalizeItem(Data: Pointer);
begin
end;

function TInfoList.Get(Index: Integer): PInfoRec;
begin
  Result := PInfoRec(GetData(Index));
end;

function TInfoList.Insert(Index: Integer; const Rec: TInfoRec): Integer;
begin
  Result := InsertData(Index, @Rec);
end;

procedure TInfoList.Put(Index: Integer; const Rec: TInfoRec);
begin
  PutData(Index, @Rec);
end;

procedure TInfoList.ResetAllSizes;
var
  i: integer;
begin
  for i := 0 to Count - 1 do
    Get(i)^.Size := fDefault.Size;
end;

procedure TInfoList.ResetAllLineWidths;
var
  i: integer;
begin
  for i := 0 to Count - 1 do
    Get(i)^.LineWidth := fDefault.LineWidth;
end;

end.

