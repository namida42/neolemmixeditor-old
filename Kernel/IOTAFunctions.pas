{$WEAKPACKAGEUNIT ON}
unit IOTAFunctions;

interface

uses
  ToolsApi;


function BorActionServices: IOTAActionServices;
function BorServices: IOTAServices;
function BorModuleServices: IOTAModuleServices;
function BorMessageServices: IOTAMessageServices;
function BorEditorServices: IOTAEditorServices;

implementation

function BorActionServices: IOTAActionServices;
begin
  Result := BorlandIDEServices as IOTAActionServices;
end;

function BorServices: IOTAServices;
begin
  Result := BorlandIDEServices as IOTAServices;
end;

function BorModuleServices: IOTAModuleServices;
begin
  Result := BorlandIDEServices as IOTAModuleServices;
end;

function BorMessageServices: IOTAMessageServices;
begin
  Result := BorlandIDEServices as IOTAMessageServices
end;

function BorEditorServices: IOTAEditorServices;
begin
  Result := BorlandIDEServices as IOTAEditorServices;
end;


end.


