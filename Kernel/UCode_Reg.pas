unit UCode_Reg;

interface

uses
  ToolsApi, Dialogs;

type
  TCollectionWizard = class(TNotifierObject, IOTAMenuWizard, IOTAWizard)
  public
    // IOTAWizard interface methods(required for all wizards/experts)
    function GetIDString: string; virtual;
    function GetName: string; virtual;
    function GetState: TWizardState; virtual;
    procedure Execute; virtual;
    // IOTAMenuWizard (creates a simple menu item on the help menu)
    function GetMenuText: string; virtual;
  end;

procedure Register;

implementation

uses
  UCollection,
  FCodeEditCollection;

procedure Register;
begin
  RegisterPackageWizard(TCollectionWizard.Create);
end;

{ TTredoxBuilder }

procedure TCollectionWizard.Execute;
var
  S: string;
  L: TListType;
  EditResult: TCollectionCreateMode;
begin
  S := '';
  if InputCollectionItem(S, L, EditResult) then
    InsertCode(S, L, EditResult);


  //if InputQuery('Geef naam CollectionItem', 'Naam:', S) then
  //ShowMessage('Tredox Collection Wizard');
end;

function TCollectionWizard.GetIDString: string;
begin
  Result := 'Tredox.CollectionWizard';
end;

function TCollectionWizard.GetMenuText: string;
begin
  Result := 'Tredox &Collection Wizard...';
end;

function TCollectionWizard.GetName: string;
begin
  Result := 'Tredox Collection Wizard';
end;

function TCollectionWizard.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

end.

