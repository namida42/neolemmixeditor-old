unit UExplorer;

interface

uses
  Windows, Classes, SysUtils, Controls, ImgList,
  UFastStrings, UFiles, UTools,
messages, ulog,  UMisc, Kernel_Resource, VirtualTrees;

type
  TSearchRecObjectList = class(TList)
  private
    function GetItem(Index: Integer): TSearchRecObject;
  protected
  public
    function Add(Item: TSearchRecObject): Integer;
    procedure Insert(Index: Integer; Item: TSearchRecObject);
    property Items[Index: Integer]: TSearchRecObject read GetItem; default;
  published
  end;

  TNodeCompareMethod = procedure (Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer) of object;

  TExplorerChange = (
    ecData,
    ecDirectory,
    ecWildCard
//    ecSortColumn,
  //  ecSortDirection
  );
  TExplorerChanges = set of TExplorerChange;

  TExplorerOption = (
    eoShowDirs
  );
  TExplorerOptions = set of TExplorerOption;

const
  DEF_EXPLOREROPTIONS = [eoShowDirs];

type
  TVirtualExplorerTree = class(TVirtualStringTree)
  private
    fTempList      : TSearchRecObjectList;
//    fCurrentMask   : string;
    fDirectory     : string;
    fWildCards     : string;
    fWildCardList  : TStringList;

    fInternalRefreshing: Boolean;
    fInternalCompareMethods: array of TNodeCompareMethod;
    fExplorerChanges: TExplorerChanges;
    fExplorerOptions: TExplorerOptions;
  { internal }
    procedure InternalCompareNames(Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
    procedure InternalCompareSizes(Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
    procedure InternalCompareTypes(Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
    procedure InternalCompareTimes(Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);

    function InternalGetSearchRecObject(aNode: PVirtualNode): TSearchRecObject;
    procedure InternalAddSearchRec(const aSearchRec: TSearchRec; const aFullName: string; out DoAdd: boolean; out aObject: TObject);
    procedure InternalRefresh(Force: Boolean = False);
  { property access }
    procedure SetDirectory(const Value: string);
    procedure SetWildCards(const Value: string);
    procedure SetExplorerOptions(const Value: TExplorerOptions);
  protected
    function DoCompare(Node1, Node2: PVirtualNode; Column: TColumnIndex): Integer; override;
    procedure DoFreeNode(Node: PVirtualNode); override;
    function DoGetImageIndex(Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean; var Index: Integer): TCustomImageList; override;
    procedure DoGetText(Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var Text: WideString); override;
    procedure KeyDown(var Key: word; Shift: TShiftState); override;
    procedure DoHeaderClick(Column: TColumnIndex; Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    function DoIncrementalSearch(Node: PVirtualNode; const Text: WideString): Integer; override;
    procedure DoStateChange(Enter: TVirtualTreeStates; Leave: TVirtualTreeStates = []); override;
    procedure HandleMouseDblClick(var Message: TWMMouse; const HitInfo: THitInfo); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Refresh;
    function GetNodeSearchRecObject(aNode: PVirtualNode): TSearchRecObject;
  published
    property Directory: string read fDirectory write SetDirectory;
    property ExplorerOptions: TExplorerOptions read fExplorerOptions write SetExplorerOptions default DEF_EXPLOREROPTIONS;
    property WildCards: string read fWildCards write SetWildCards;

  end;

  TVirtualExplorerTreeHandler = class
  private
 //   fTree: TVirtualStringTree;
//    function Tree_CompareNames(Index1, Index2: integer): integer;
  //  function Tree_CompareSizes(Index1, Index2: integer): integer;
    //function Tree_CompareTimes(Index1, Index2: integer): integer;
  protected
  public
    //constructor Create;
    //destructor Destroy; override;

  published
  end;

function FileContainsText(const aFileName, aSearch: string): Boolean;

implementation

uses Masks;

function FileContainsText(const aFileName, aSearch: string): Boolean;
var
  F: TFileStream;
  R: Cardinal;
  FileSize: Integer;
  SearchLen: Integer;
  BufSize: Integer;
  Buf: string;
begin
  Result := False;
  SearchLen := Length(aSearch);
  if (SearchLen > 512) or (SearchLen = 0) then
    Exit;

  BufSize := 1024;
  SetLength(Buf, BufSize);
//  R := 0;
  F := TFileStream.Create(aFileName, fmShareDenyNone);
  FileSize := F.Size;
  try
    repeat
      R := F.Read(Buf[1], BufSize);
      if R = 0 then
        Break;
      if FastPos(Buf, aSearch, R, SearchLen, 1) > 0 then
      begin
        Result := True;
        Exit;
      end;
      if F.Position + SearchLen >= FileSize then
        Break;
      F.Seek(-SearchLen, soFromCurrent);
    until False;
  finally
    F.Free;
  end;
end;

{ TSearchRecObjectList }

function TSearchRecObjectList.Add(Item: TSearchRecObject): Integer;
begin
  Result := inherited Add(Item);
end;

function TSearchRecObjectList.GetItem(Index: Integer): TSearchRecObject;
begin
  Result := inherited Get(Index);
end;

procedure TSearchRecObjectList.Insert(Index: Integer; Item: TSearchRecObject);
begin
  inherited Insert(Index, Item);
end;


{ TVirtualExplorerTree }

constructor TVirtualExplorerTree.Create(aOwner: TComponent);
begin
  inherited;
  fTempList := TSearchRecObjectList.Create;
  fWildCardList := TStringList.Create;
//  fWildCardList.
  fDirectory := 'c:\';
  fWildCards := '*.*';
  NodeDataSize := SizeOf(Pointer);

  //tree.OnCompareNodes := Tree_CompareNames;
  //tree.images := XpImages16;//fTreeImages;

  TreeOptions.PaintOptions := TreeOptions.PaintOptions - [toShowTreeLines, toShowRoot];
  TreeOptions.AutoOptions := TreeOptions.AutoOptions + [toAutoSort];
  DefaultText := '';

  Header.Columns.Add; // name
  Header.Columns.Add; // size
  Header.Columns.Add; // type
  Header.Columns.Add; // time

  Header.Columns[0].Text := 'Naam';
  Header.Columns[1].Text := 'Grootte';
  Header.Columns[2].Text := 'Type';
  Header.Columns[3].Text := 'Gewijzigd';

  SetLength(fInternalCompareMethods, 4);

  fInternalCompareMethods[0] := InternalCompareNames;
  fInternalCompareMethods[1] := InternalCompareSizes;
  fInternalCompareMethods[2] := InternalCompareTypes;
  fInternalCompareMethods[3] := InternalCompareTimes;

//  Header.Options := Header.Options + [hoVisible, hoAutoResize];
  Header.Options := Header.Options + [hoVisible];
  Header.SortColumn := 0;

  Images := XpImages16;
  fExplorerOptions := DEF_EXPLOREROPTIONS;

  IncrementalSearch := isAll;
  //InternalRefresh;
end;

destructor TVirtualExplorerTree.Destroy;
begin
  fTempList.Free;
  fWildCardList.Free;
  inherited;
end;

procedure TVirtualExplorerTree.DoGetText(Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var Text: WideString);
var
  SO: TSearchRecObject;
  D: TDateTime;
begin
  SO := InternalGetSearchRecObject(Node);

//  SO := TSearchRecObject(List.Objects[Node.Index]);
  with SO do
  case Column of
    0: Text := SearchRec.Name;
    1: begin
         //CellText := i2s(SearchRec.Size)
         if not SO.IsDirectory then
           Text := i2s(SearchRec.Size div 1024) + ' KB'
//         CellText := Format('m', [1200]);
//         CellText := i2s(SearchRec.Size div 1024) + ' KB';
         else
           Text := '';
       end;
    2: begin
         if (faDirectory and SO.SearchRec.Attr) <> 0 then
           Text := 'map'
         else
           Text := 'bestand';
       end;
    3: begin
         D := FileDateToDateTime(SearchRec.Time);
         Text := DateTimeToStr(D);
       end;
    4: begin
         Text := AttribStr(SO.SearchRec.Attr);
       end;
  end;

end;

procedure TVirtualExplorerTree.DoStateChange(Enter, Leave: TVirtualTreeStates);
begin
  inherited DoStateChange(Enter, Leave);
  if tsUpdating in Leave then
    InternalRefresh(False);
end;

function TVirtualExplorerTree.InternalGetSearchRecObject(aNode: PVirtualNode): TSearchRecObject;
var
  P: Pointer;
begin
  P := GetNodeData(aNode);
  if P <> nil then Result := TSearchRecObject(P^) else Result := nil;
  //Assert(Result <> nil, 'InternalGetNodeData = nil');
end;

procedure TVirtualExplorerTree.InternalAddSearchRec(const aSearchRec: TSearchRec; const aFullName: string; out DoAdd: boolean; out aObject: TObject);

    function Match: Boolean;
    var
      i: Integer;
    begin
      for i := 0 to fWildCardList.Count - 1 do
        if MatchesMask(aSearchRec.Name, fWildCardList[i]) then
        begin
          Result := True;
          Exit;
        end;
      Result := False;
    end;

begin
  Assert(fInternalRefreshing, 'Illegal call to InternalAddSearchRec');
  DoAdd := False;
  if ((aSearchRec.Attr and faDirectory <> 0) and (eoShowDirs in fExplorerOptions))
  or Match then
    fTempList.Add(TSearchRecObject.Create(aSearchRec));
end;

procedure TVirtualExplorerTree.InternalRefresh(Force: Boolean = False);
var
  Dir: string;
  i: Integer;
  N: PVirtualNode;
  P: Pointer;
  TheChanges: TExplorerChanges;
begin
  if fInternalRefreshing then
    Exit;
  fInternalRefreshing := True;

  try
    TheChanges := fExplorerChanges;
    fExplorerChanges := [];

    if Force or (TheChanges * [ecData, ecDirectory, ecWildCard] <> []) then
    begin
      fTempList.Clear;
      BeginUpdate;
      try
        RootNodeCount := 0;
        Dir := IncludeTrailingBackslash(fDirectory);
  //      WCard := fWildCard;
//        fCurrentMask := Dir + fWildCard;
        SplitString_To_StringList(fWildCards, fWildCardList, ';', True);
        //Log([fwildcards, fwildcardlist.count]);
//        deblist(fWildCardList);
        CreateFileList(nil, Dir + '*.*', faAllfiles, True, False, InternalAddSearchRec);

        RootNodeCount := fTempList.Count;
        i := 0;
        N := GetFirst;
        while N <> nil do
        begin
          P := GetNodeData(N);
          Pointer(P^) := fTempList[i];
          Inc(i);
          N := GetNext(N);
        end;

        //SelectedFocusedNode := GetFirst;

      finally
        EndUpdate;
        fTempList.Clear;
      end;
    end;

  finally
    fInternalRefreshing := False;
  end;  

end;

procedure TVirtualExplorerTree.SetDirectory(const Value: string);
begin
  if CompareText(fDirectory, Value) = 0 then
    Exit;
  fDirectory := Value;
  Include(fExplorerChanges, ecDirectory);
  if not (tsUpdating in TreeStates) then
    InternalRefresh;
end;

procedure TVirtualExplorerTree.SetWildCards(const Value: string);
begin
  if CompareText(fWildCards, Value) = 0 then
    Exit;
  fWildCards := Value;
  Include(fExplorerChanges, ecWildCard);
  if not (tsUpdating in TreeStates) then
    InternalRefresh;
end;

procedure TVirtualExplorerTree.SetExplorerOptions(const Value: TExplorerOptions);
begin
  if fExplorerOptions = Value then
    Exit;
  Include(fExplorerChanges, ecData);
  fExplorerOptions := Value;
  if not (tsUpdating in TreeStates) then
    InternalRefresh;
end;


procedure TVirtualExplorerTree.Refresh;
begin
  InternalRefresh(True);
end;

procedure TVirtualExplorerTree.DoFreeNode(Node: PVirtualNode);
var
  P: Pointer;
begin
  P := GetNodeData(Node);
  if P <> nil then
    TSearchRecObject(P^).Free;
  inherited DoFreeNode(Node);
end;

procedure TVirtualExplorerTree.DoHeaderClick(Column: TColumnIndex; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  BeginUpdate;
  with Header do
    begin
      if SortColumn = Column then
      begin
        if SortDirection = sdAscending then
          SortDirection := sdDescending
        else
          SortDirection := sdAscending;
      end
      else begin
(*        case Column of
          0: Tree.OnCompareNodes := Internal_CompareNames;
          1: Tree.OnCompareNodes := Internal_CompareSizes;
          //2: Tree.OnCompareNodes := Tree_CompareNames;
          3: Tree.OnCompareNodes := Internal_CompareTimes;
        end; *)
        SortColumn := Column;
        SortDirection := sdAscending;
      end;
    end;
  EndUpdate;
//  InternalSort;

  inherited;
end;

function TVirtualExplorerTree.DoCompare(Node1, Node2: PVirtualNode; Column: TColumnIndex): Integer;
var
  Cmp: TNodeCompareMethod;
begin
  Result := 0;
  if Column >= 0 then
  begin
    Cmp := fInternalCompareMethods[Column];
    Cmp(Node1, Node2, Column, Result);
  end;  
end;

procedure TVirtualExplorerTree.InternalCompareNames(Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  S1, S2: TSearchRecObject;
begin
  Result := 0;
  S1 := InternalGetSearchRecObject(Node1);
  S2 := InternalGetSearchRecObject(Node2);

  if ((S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory = 0))
  or ((S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory <> 0)) then
    Result := CompareText(S1.Name, S2.Name)

  else if (S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory = 0) then
    Result := -1

  else if (S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory <> 0) then
    Result := 1
end;

procedure TVirtualExplorerTree.InternalCompareSizes(Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  S1, S2: TSearchRecObject;
begin
  Result := 0;
  S1 := InternalGetSearchRecObject(Node1);
  S2 := InternalGetSearchRecObject(Node2);

  if ((S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory = 0))
  or ((S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory <> 0)) then
    Result := S1.Size - S2.Size

  else if (S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory = 0) then
    Result := -1

  else if (S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory <> 0) then
    Result := 1
end;

procedure TVirtualExplorerTree.InternalCompareTypes(Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  S1, S2: TSearchRecObject;
begin
  Result := 0;
  S1 := InternalGetSearchRecObject(Node1);
  S2 := InternalGetSearchRecObject(Node2);

  if ((S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory = 0))
  or ((S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory <> 0)) then
    Result := CompareText(ExtractFileExt(S1.Name), ExtractFileExt(S2.Name))

  else if (S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory = 0) then
    Result := -1

  else if (S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory <> 0) then
    Result := 1
end;

procedure TVirtualExplorerTree.InternalCompareTimes(Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  S1, S2: TSearchRecObject;
begin
  Result := 0;
  S1 := InternalGetSearchRecObject(Node1);
  S2 := InternalGetSearchRecObject(Node2);

  if ((S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory = 0))
  or ((S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory <> 0)) then
    Result := S1.Time - S2.Time

  else if (S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory = 0) then
    Result := -1

  else if (S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory <> 0) then
    Result := 1
end;

function TVirtualExplorerTree.DoGetImageIndex(Node: PVirtualNode;
  Kind: TVTImageKind; Column: TColumnIndex; var Ghosted: Boolean;
  var Index: Integer): TCustomImageList;
var
  SO: TSearchRecObject;
begin
  Result := inherited DoGetImageIndex(Node, Kind, Column, Ghosted, Index);
  if Column = 0 then
  begin
    SO := InternalGetSearchRecObject(Node);
    if SO.IsDirectory then
      Index := Ord(TMX_folder_closed)
    else
      Index := Ord(TXP_new_document16);
  end;
end;

procedure TVirtualExplorerTree.KeyDown(var Key: word; Shift: TShiftState);
var
  Node: PVirtualNode;
  SO: TSearchRecObject;
  Dir, S, PrevDir: string;
//  Dir
//  Han
begin
  //if Key <> VK_BACK then
  inherited;
  case Key of
     VK_RETURN:
    begin
      Node := FocusedNode;
      if Node <> nil then
      begin
        SO := InternalGetSearchRecObject(Node);
        if SO.IsDirectory then
          Directory := IncludeTrailingBackslash(Directory) + SO.Name;
      end;
    end;

    VK_BACK:
      begin
        Dir := ExcludeTrailingBackslash(fDirectory);
        S := ExtractFileName(Dir);
        PrevDir := Copy(Dir, 1, Length(Dir) - Length(S));
        //Log([dir, prevdir]);
        if PrevDir <> '' then
          Directory := PrevDir;

        //S := ExtractFileName(fDirectory);
        Node := GetFirst;
        FocusedNode := Node;
        while Node <> nil do
        begin
          if CompareText(Text[Node, 0], S) = 0 then
          begin
            FocusedNode := Node;
            Break;
          end;
          Node := Node.NextSibling;
        end;

        //Findn
      end;

  end;
end;

function TVirtualExplorerTree.DoIncrementalSearch(Node: PVirtualNode;
  const Text: WideString): Integer;
var
  Source: string;
begin
  Result := 0;

  Source := Self.Text[Node, FocusedColumn];
  if FastPosNoCase(Source, Text, Length(Source), Length(Text), 1) <> 1 then
    Result := 1;


(*  if Assigned(OnIncrementalSearch) then
    FOnIncrementalSearch(Self, Node, Text, Result)
  else
    // Default behavior is to match the search string with the start of the node text.
    if Pos(Text, GetText(Node, FocusedColumn)) <> 1 then
      Result := 1; *)
end;

function TVirtualExplorerTree.GetNodeSearchRecObject(aNode: PVirtualNode): TSearchRecObject;
begin
  Result := InternalGetSearchRecObject(aNode)
end;

procedure TVirtualExplorerTree.HandleMouseDblClick(var Message: TWMMouse; const HitInfo: THitInfo);
var
  SO: TSearchRecObject;
//  Dir, S, PrevDir: string;
//  Dir
//  Han
begin
  inherited;
  with HitInfo do
  begin
    if Assigned(HitNode) and (HitColumn = 0) then
    begin
      SO := InternalGetSearchRecObject(HitNode);
      if SO.IsDirectory then
        Directory := IncludeTrailingBackslash(Directory) + SO.Name;
    end;
  end;
end;

end.


(*

{
  ScanFile searches for a string in a file and returns the position of the string
  in the file or -1, if not found.

  ScanFile sucht in einer Datei nach dem Vorkommen
  eines bestimmten Strings und gibt bei Erfolg die Position zur�ck, wo der String
  gefunden wurde.
}

function ScanFile(const FileName: string;
  const forString: string;
  caseSensitive: Boolean): Longint;
const
  BufferSize = $8001;  { 32K+1 bytes }
var
  pBuf, pEnd, pScan, pPos: PChar;
  filesize: LongInt;
  bytesRemaining: LongInt;
  bytesToRead: Integer;
  F: file;
  SearchFor: PChar;
  oldMode: Word;
begin
  { assume failure }
  Result := -1;
  if (Length(forString) = 0) or (Length(FileName) = 0) then Exit;
  SearchFor := nil;
  pBuf      := nil;
  { open file as binary, 1 byte recordsize }
  AssignFile(F, FileName);
  oldMode  := FileMode;
  FileMode := 0;    { read-only access }
  Reset(F, 1);
  FileMode := oldMode;
  try { allocate memory for buffer and pchar search string }
    SearchFor := StrAlloc(Length(forString) + 1);
    StrPCopy(SearchFor, forString);
    if not caseSensitive then  { convert to upper case }
      AnsiUpper(SearchFor);
    GetMem(pBuf, BufferSize);
    filesize       := System.Filesize(F);
    bytesRemaining := filesize;
    pPos           := nil;
    while bytesRemaining > 0 do
    begin
      { calc how many bytes to read this round }
      if bytesRemaining >= BufferSize then
        bytesToRead := Pred(BufferSize)
      else
        bytesToRead := bytesRemaining;
      { read a buffer full and zero-terminate the buffer }
      BlockRead(F, pBuf^, bytesToRead, bytesToRead);
      pEnd  := @pBuf[bytesToRead];
      pEnd^ := #0;
      pScan := pBuf;
      while pScan < pEnd do
      begin
        if not caseSensitive then { convert to upper case }
          AnsiUpper(pScan);
        pPos := StrPos(pScan, SearchFor);  { search for substring }
        if pPos <> nil then
        begin { Found it! }
          Result := FileSize - bytesRemaining +
            Longint(pPos) - Longint(pBuf);
          Break;
        end;
        pScan := StrEnd(pScan);
        Inc(pScan);
      end;
      if pPos <> nil then Break;
      bytesRemaining := bytesRemaining - bytesToRead;
      if bytesRemaining > 0 then
      begin
        Seek(F, FilePos(F) - Length(forString));
        bytesRemaining := bytesRemaining + Length(forString);
      end;
    end; { While }
  finally
    CloseFile(F);
    if SearchFor <> nil then StrDispose(SearchFor);
    if pBuf <> nil then FreeMem(pBuf, BufferSize);
  end;
end; { ScanFile }


// Search in autoexec.bat for "keyb" with case insensitive
// In der autoexec.bat nach "keyb" suchen

procedure TForm1.Button1Click(Sender: TObject);
var
  Position: integer;
begin
  Position := ScanFile('c:\autoexec.bat', 'keyb', False);
  ShowMessage(IntToStr(Position));
end;


*)


(*

  TBeforeAddFileEvent = procedure (const aSearchRec: TSearchRec; const aFullName: string; out DoAdd: boolean);

  TExplorerSortMode = (
    esmName,
    esmTime,
    esmSize
//    esmType
  );

  TExplorerChange = (
    ecFileMask,
    ecSortMode,
    ecSortDirection
  );
  TExplorerChanges = set of TExplorerChange;

  TExplorerAutoOption = (
    eaoAutoRefresh
  );
  TExplorerAutoOptions = set of TExplorerAutoOption;

  TExplorerOptions = class(TPersistent)
  private
    fAutoOptions: TExplorerAutoOptions;
  protected
  public
  published
    property AutoOptions: TExplorerAutoOptions read fAutoOptions write fAutoOptions;
  end;

  TExplorerList = class
  private
    fMainList      : TStringList;
    fRefList       : TIntegerlist;
    fBeforeAddFile : TBeforeAddFileEvent;
    fFileMask      : string;
    fUpdateCount   : Integer;
    fChanges       : TExplorerChanges;
    fSortMode      : TExplorerSortMode;
    fActive        : Boolean;
    fSortDirection : TSortDirection;
    fOptions       : TExplorerOptions;
//    fOnEndUpdate: TNotifyEvent;
//    fOnBeginUpdate: TNotifyEvent;
  { internal }
    //procedure InternalRefresh;
    procedure InternalSortRefList;
    procedure Changed;
  { internal event methods }
    procedure MainList_AddSearchRec(const aSearchRec: TSearchRec; const aFullName: string; out DoAdd: boolean; out AObject: TObject);
    function RefList_CompareNamesAsc(Index1, Index2: integer): integer;
    function RefList_CompareNamesDesc(Index1, Index2: integer): integer;
    function RefList_CompareSizesAsc(Index1, Index2: integer): integer;
    function RefList_CompareSizeDesc(Index1, Index2: integer): integer;
    function RefList_CompareTimesAsc(Index1, Index2: integer): integer;
    function RefList_CompareTimesDesc(Index1, Index2: integer): integer;
  { property access }
    function GetSearchRecObject(N: Integer): TSearchRecObject;
    function GetCount: Integer;
    procedure SetFileMask(const Value: string);
    procedure SetSortMode(const Value: TExplorerSortMode);
    procedure SetSortDirection(const Value: TSortDirection);
  protected
  public
    constructor Create;
    destructor Destroy; override;
    procedure Open;
    procedure Close;
    procedure Refresh(Force: Boolean = False; aChanges: TExplorerChanges = []);
    procedure Clear;
    procedure BeginUpdate;
    procedure EndUpdate;
    function IsUpdating: Boolean;
  { properties }
    property SearchRecObjects[N: Integer]: TSearchRecObject read GetSearchRecObject; default;
    property Count: Integer read GetCount;
    property FileMask: string read fFileMask write SetFileMask;
    property SortMode: TExplorerSortMode read fSortMode write SetSortMode;
    property SortDirection: TSortDirection read fSortDirection write SetSortDirection;
  { events }
    property BeforeAddFile: TBeforeAddFileEvent read fBeforeAddFile write fBeforeAddFile;
//    property OnBeginUpdate: TNotifyEvent read fOnBeginUpdate write fOnBeginUpdate;
//    property OnEndUpdate: TNotifyEvent read fOnEndUpdate write fOnEndUpdate;

  published
    property Options: TExplorerOptions read fOptions;
  end;


{ TExplorerList }

constructor TExplorerList.Create;
begin
  inherited Create;
  fMainList := TStringlist.Create;
  fRefList := TIntegerlist.Create;
  fOptions := TExplorerOptions.Create;
end;

destructor TExplorerList.Destroy;
begin
  fRefList.Free;
  fMainList.Free;
  fOptions.Free;
  inherited;
end;

function TExplorerList.GetSearchRecObject(N: Integer): TSearchRecObject;
begin
  Result := TSearchRecObject(fMainList.Objects[fRefList[N]]);
end;


procedure TExplorerList.MainList_AddSearchRec(const aSearchRec: TSearchRec; const aFullName: string; out DoAdd: boolean; out AObject: TObject);
{-------------------------------------------------------------------------------
  Event, used during Refresh.
-------------------------------------------------------------------------------}
begin
  // give application chance to filter files
  if Assigned(fBeforeAddFile) then
    fBeforeAddFile(aSearchRec, aFullName, DoAdd);

  // add object to the stringlist
  if DoAdd then
    aObject := TSearchRecObject.Create(aSearchRec);
end;

function TExplorerList.RefList_CompareNamesAsc(Index1, Index2: integer): integer;
var
  S1, S2: TSearchRecObject;
begin
  Result := 0;
  S1 := TSearchRecObject(fMainList.Objects[Index1]);
  S2 := TSearchRecObject(fMainList.Objects[Index2]);

  if ((S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory = 0))
  or ((S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory <> 0)) then
    Result := CompareText(S1.Name, S2.Name)

  else if (S1.Attr and faDirectory <> 0) and (S2.Attr and faDirectory = 0) then
    Result := -1

  else if (S1.Attr and faDirectory = 0) and (S2.Attr and faDirectory <> 0) then
    Result := 1
end;

function TExplorerList.RefList_CompareNamesDesc(Index1, Index2: integer): integer;
begin
  Result := -RefList_CompareNamesAsc(Index1, Index2);
end;

function TExplorerList.RefList_CompareSizesAsc(Index1, Index2: integer): integer;
begin
  Result := TSearchRecObject(fMainList.Objects[Index1]).Size -
            TSearchRecObject(fMainList.Objects[Index2]).Size;
end;

function TExplorerList.RefList_CompareSizeDesc(Index1, Index2: integer): integer;
begin
  Result := -RefList_CompareSizesAsc(Index1, Index2);
end;

function TExplorerList.RefList_CompareTimesAsc(Index1, Index2: integer): integer;
begin
  Result := TSearchRecObject(fMainList.Objects[Index2]).Time -
            TSearchRecObject(fMainList.Objects[Index1]).Time;
end;

function TExplorerList.RefList_CompareTimesDesc(Index1, Index2: integer): integer;
begin
  Result := -RefList_CompareTimesAsc(Index1, Index2);
end;

function TExplorerList.GetCount: Integer;
begin
  Result := fRefList.Count;
end;

procedure TExplorerList.Clear;
begin
  ClearStringsWithObjects(fMainList);
  fRefList.Clear;
end;

procedure TExplorerList.Refresh(Force: Boolean = False; aChanges: TExplorerChanges = []);
var
  i: Integer;
begin
  if not fActive then
    Exit;

  if Force or ([ecFileMask] * aChanges <> []) then
  begin
    Clear;
    CreateFileList(fMainList, fFileMask, faAllfiles, True, False, MainList_AddSearchRec);
    for i := 0 to fMainList.Count - 1 do
      fReflist.Add(i);
  end;

  if Force
  or ([ecSortMode, ecSortDirection] * aChanges <> [])
  or ([ecFileMask] * aChanges <> []) then
    InternalSortRefList;
//  if tree.Header.sortcolumn >= 0 then
//    internalsort;

end;

procedure TExplorerList.SetFileMask(const Value: string);
begin
  if CompareText(fFileMask, Value) = 0 then
    Exit;
  BeginUpdate;
  try
    fFileMask := Value;
    Include(fChanges, ecFileMask);
  finally
    EndUpdate;
  end;
end;

procedure TExplorerList.SetSortMode(const Value: TExplorerSortMode);
begin
  if fSortMode = Value then
    Exit;
  BeginUpdate;
  try
    fSortMode := Value;
    Include(fChanges, ecFileMask);
  finally
    EndUpdate;
  end;
end;

procedure TExplorerList.BeginUpdate;
begin
  Inc(fUpdateCount);
//  if Assigned(fOnBeginUpdate) then
  //  fOnBeginUpdate(Self);
end;

procedure TExplorerList.EndUpdate;
begin
  if fUpdateCount > 0 then
  begin
    Dec(fUpdateCount);
    if fUpdateCount = 0 then
      if eaoAutoRefresh in fOptions.fAutoOptions then
        Changed;
  end;
//  if Assigned(fOnEndUpdate) then
  //  fOnEndUpdate(Self);
end;

function TExplorerList.IsUpdating: Boolean;
begin
  Result := fUpdateCount > 0;
end;

procedure TExplorerList.Changed;
var
  TheChanges: TExplorerChanges;
begin
  TheChanges := fChanges;
  fChanges := [];
  Refresh(False, TheChanges);
end;


procedure TExplorerList.Close;
begin
  if fActive then
  begin
    fActive := False;
    Clear;
  end;
end;

procedure TExplorerList.Open;
begin
  if not fActive then
  begin
    fActive := True;
    Refresh(True);
  end;
end;

procedure TExplorerList.SetSortDirection(const Value: TSortDirection);
begin
  if fSortDirection = Value then
    Exit;
  BeginUpdate;
  try
    fSortDirection := Value;
  finally
    EndUpdate;
  end;
end;

procedure TExplorerList.InternalSortRefList;
var
  RefListCompareMethod: TIntListSortMethod;
begin
  RefListCompareMethod := nil;

  case SortMode of
    esmName:
      case SortDirection of
        sdAscending  : RefListCompareMethod := RefList_CompareNamesAsc;
        sdDescending : RefListCompareMethod := RefList_CompareNamesDesc;
      end;
    esmSize:
      case SortDirection of
        sdAscending  : RefListCompareMethod := RefList_CompareSizesAsc;
        sdDescending : RefListCompareMethod := RefList_CompareSizeDesc;
      end;
    {
    2:
      case SortDirection of
        sdAscending  : RefListCompareMethod := RefList_CompareAsc;
        sdDescending : RefListCompareMethod := RefList_CompareDesc;
      end;
    }
    esmTime:
      case SortDirection of
        sdAscending  : RefListCompareMethod := RefList_CompareTimesAsc;
        sdDescending : RefListCompareMethod := RefList_CompareTimesDesc;
      end;
  end;

  fRefList.CustomSort(RefListCompareMethod);
end;


