{$WEAKPACKAGEUNIT ON}
unit FApplicationView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  TypInfo, UMasterForms, UMisc, Dialogs, StdCtrls, ComCtrls;

type
  TApplicationViewerForm = class(TForm)
    Tree: TTreeView;
    BtnControls: TButton;
    TxtCount: TStaticText;
    Button1: TButton;
    BtnComponents: TButton;
    procedure BtnControlsClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnComponentsClick(Sender: TObject);
  private
    procedure FillControlTree;
    procedure FillComponentTree;
  public
  end;

procedure ShowApplicationView(Modal: Boolean = False);

implementation

{$R *.dfm}

procedure ShowApplicationView(Modal: Boolean = False);
var
  F: TApplicationViewerForm;
  i: Integer;
begin
  if Modal then
  begin
    F := TApplicationViewerForm.Create(nil);
    F.ShowModal
  end
  else begin
    F := nil;
    for i := 0 to Screen.CustomFormCount - 1 do
      if Screen.CustomForms[i] is TApplicationViewerForm then
      begin
        F := TApplicationViewerForm(Screen.CustomForms[i]);
        Break;
      end;
    if F = nil then
      F := TApplicationViewerForm.Create(nil);
    F.Show;
  end;
end;

procedure TApplicationViewerForm.BtnControlsClick(Sender: TObject);
begin
  FillControlTree;
end;

procedure TApplicationViewerForm.FillControlTree;

    procedure FillNode(C: TControl; N: TTreeNode);
    var
      W: TWinControl absolute C;
      i: Integer;
      SubControl: TControl;
      NewNode: TTreeNode;
    begin
      if C is TWinControl then
      begin
        for i := 0 to W.ControlCount - 1 do
        begin
          SubControl := W.Controls[i];
          NewNode := Tree.Items.AddChildObject(N,
            IIF(SubControl.Name <> '', SubControl.Name, '<noname>') +
            ' (' + SubControl.ClassName + ')', SubControl);
          FillNode(SubControl, NewNode);
        end
      end
    end;

var
  F: TCustomForm;
  FormNode, ControlNode: TTreeNode;
  i: Integer;
begin
  Tree.Items.Clear;
  for i := 0 to Screen.CustomFormCount - 1 do
  begin
    F := Screen.CustomForms[i];
    if F is TCustomMasterForm then
      if TCustomMasterForm(F).IsChild then
        Continue;
    FormNode := Tree.Items.AddObject(nil,
      IIF(F.Name <> '', F.Name, '<noname>') +
      ' (' + F.ClassName + ')', F);
    FillNode(F, FormNode);
    FormNode.Expand(True);
  end;
  TxtCount.caption := inttostr(tree.items.count);
end;

procedure TApplicationViewerForm.FillComponentTree;

    procedure GetObjects(aObject: TComponent);
    var
      iCount:       Integer;
      iSize:        Integer;
      iProp:        Integer;
      pProps:       PPropList;

      iValue:       Integer;
      fValue:       Extended;
      sValue:       String;

    begin
        //windlg(aobject.classname);
        // Vraag het aantal properties op en reken het benodigde geheugen uit...
        iCount  := GetPropList(AObject.ClassInfo, tkProperties, nil);
        iSize   := iCount * SizeOf(TPropInfo);

        // Reserveer het geheugen...
        GetMem(pProps, iSize);

        try
          // Vraag de properties lijst op...
          GetPropList(AObject.ClassInfo, tkProperties, pProps, False{Sorted});
          for iProp := 0 to iCount - 1 do
          begin
            case pProps^[iProp]^.PropType^.Kind of
              tkClass:
                begin
//                  log([aobject.classname, 'klasse', pProps^[iProp]^.PropType^.Name]);
                end;
            end;
          end;
        finally
          FreeMem(pProps, iSize);
        end;
      end;

      procedure FillNode(C: TComponent; N: TTreeNode);
      var
        i: Integer;
        SubComponent: TComponent;
        NewNode: TTreeNode;
      begin
        for i := 0 to C.ComponentCount - 1 do
        begin
          SubComponent := C.Components[i];
//          GetObjects(SubComponent);
          NewNode := Tree.Items.AddChildObject(N,
            IIF(SubComponent.Name <> '', SubComponent.Name, '<noname>') +
            ' (' + SubComponent.ClassName + ')', SubComponent);
          FillNode(SubComponent, NewNode);
        end
    end;

var
  i: Integer;
  C: TComponent;
  ComponentNode: TTreeNode;
begin
  for i := 0 to Application.ComponentCount - 1 do
  begin
    C := Application.Components[i];
    ComponentNode := Tree.Items.AddObject(nil,
      IIF(C.Name <> '', C.Name, '<noname>') +
      ' (' + C.ClassName + ')', C);
    FillNode(C, ComponentNode);
    //FormNode.Expand(True);
  end;
  TxtCount.caption := inttostr(tree.items.count);
end;

procedure TApplicationViewerForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TApplicationViewerForm.BtnComponentsClick(Sender: TObject);
begin
  FillComponentTree;
end;

end.

