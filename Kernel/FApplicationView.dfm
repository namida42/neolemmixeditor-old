object ApplicationViewerForm: TApplicationViewerForm
  Left = 99
  Top = 155
  Width = 696
  Height = 480
  Caption = 'ApplicationViewerForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Tree: TTreeView
    Left = 0
    Top = 0
    Width = 585
    Height = 453
    Align = alLeft
    Indent = 19
    TabOrder = 0
  end
  object BtnControls: TButton
    Left = 600
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Controls'
    TabOrder = 1
    OnClick = BtnControlsClick
  end
  object TxtCount: TStaticText
    Left = 608
    Top = 232
    Width = 47
    Height = 17
    Caption = 'TxtCount'
    TabOrder = 2
  end
  object Button1: TButton
    Left = 600
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 3
  end
  object BtnComponents: TButton
    Left = 600
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Components'
    TabOrder = 4
    OnClick = BtnComponentsClick
  end
end
