unit ajMultiLine;

{---------------------------------------------------------------------------------------------------

                            -=< MultiLine Delphi Component Palette Expert >=-

  Copyright SoftSpot Software 2001 - All Rights Reserved            www.softspotsoftware.com
                                                                    contact@softspotsoftware.com

  Author        : Andrew J Jameson  Creation Date : 10 September 2001  Version       : 1.00  Last Revision :  Revision      : 1.00  Description   : Sets the Delphi IDE component palette tab control to multiline.  Modifications :
---------------------------------------------------------------------------------------------------}

interface

uses
  Forms, Controls, ComCtrls;

implementation


type
  TajMultiLinePalette = class(TObject)
  public
    procedure ResizeMultiLineComponentPalette(Sender : TObject);
  end;

var
  MultiLinePalette  : TajMultiLinePalette;

{--------------------------------------------------------------------------------------------------}

procedure TajMultiLinePalette.ResizeMultiLineComponentPalette(Sender : TObject);
var
  AHeight : integer;
begin
  with (Sender as TTabControl) do begin
    AHeight                       := Height + DisplayRect.Top - DisplayRect.Bottom + 29;
    Constraints.MinHeight         := AHeight;
    Parent.Constraints.MaxHeight  := AHeight;
  end; {with}
end; {ResizeMultiLineComponentPalette}

{--------------------------------------------------------------------------------------------------}

procedure SetMultiLineComponentPalette(MultiLine : boolean);
var
  AppBuilder  : TForm;
  TabControl  : TTabControl;
begin
  AppBuilder  := TForm(Application.FindComponent('AppBuilder'));
  if (AppBuilder <> nil) then begin
    TabControl  := TTabControl(AppBuilder.FindComponent('TabControl'));
    if (TabControl <> nil) then begin
      TabControl.MultiLine  := MultiLine;
      if MultiLine then
        TabControl.OnResize := MultiLinePalette.ResizeMultiLineComponentPalette
      else
        TabControl.OnResize := nil;
    end; {if}
  end; {if}
end; {SetMultiLineComponentPalette}

{--------------------------------------------------------------------------------------------------}

initialization
  MultiLinePalette  := TajMultiLinePalette.Create;
  SetMultiLineComponentPalette(true);

{--------------------------------------------------------------------------------------------------}

finalization
  SetMultiLineComponentPalette(false);
  MultiLinePalette.Free;

{--------------------------------------------------------------------------------------------------}
{ajMultiLine}
end.

