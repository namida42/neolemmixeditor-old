{-------------------------------------------------------------------------------
2005-03-23 EL:
  CreateColumns overschreven.
-------------------------------------------------------------------------------}
unit DBGridsEx;

interface

uses
  Windows, Classes, Math,
  Graphics, Controls, Grids, DBGrids, StdCtrls;

type
  TColumnEx = class(TColumn)
  private
  protected
  public
  published
  end;

  TDBGridColumnsEx = class(TDBGridColumns)
  private
  protected
  public
  published
  end;

  TDBGridEx = class(TDBGrid)
  private
    function GetVisibleFields: string;
  protected
    // overrides
    function CreateColumns: TDBGridColumns; override;
    function DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    function DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    procedure KeyPress(var Key: Char); override;
    procedure TitleClick(Column: TColumn); override;
  public
    constructor Create(AOwner: TComponent); override;
    property VisibleFields: string read GetVisibleFields;
  published
    property GridLineWidth;
  end;
implementation

uses
  ULog, DB;


{ TDBGridEx }

constructor TDBGridEx.Create(AOwner: TComponent);
begin
  inherited;
  ScrollBars := ssNone;//Vertical;
//  SetScrollInfo(Self.Handle, SB_VERT, SINew, True);
end;

function TDBGridEx.CreateColumns: TDBGridColumns;
begin
  Result := TDBGridColumnsEx.Create(Self, TColumnEx);
end;

function TDBGridEx.DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
  Result := DataLink.Active;
  if Result then DataLink.DataSet.Next;
end;

function TDBGridEx.DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
  Result := DataLink.Active;
  if Result then DataLink.DataSet.Prior;
end;

function TDBGridEx.GetVisibleFields: string;
var
  i: Integer;
begin
  Result := '';
  with Columns do
    for i := 0 to Count - 1 do
      with Items[i] do
        if Visible then
        begin
          if Result <> '' then Result := Result + ',';
          Result := Result + FieldName;
        end;
//  log([result]);      
end;

procedure TDBGridEx.KeyPress(var Key: Char);
//var
//  Sel: TField;
//  SelIndex: integer;
//  V: Variant;

begin

  (*
//  log(['key']);
  if DataLink.Active then
  begin
  //  SelIndex := SelectedIndex;
    Sel := Columns[SelectedIndex].Field;
//    Sel := SelectedField;
    if Sel <> nil then
    if not EditorMode or (Columns[SelectedIndex].ReadOnly) then
//    if (Columns[SelectedIndex].ReadOnly) or (Self.ReadOnly) or (Sel.ReadOnly) then
    if Upcase(Key) in ['A'..'Z'] then
    begin
    //  log([sel.fieldname]);
      DataLink.DataSet.Locate(Sel.Fieldname, Key, [loCaseInsensitive, loPartialKey]);
  //    log(['incremental search']);
    end;
  end;

  *)

  inherited;

  exit;

  (*

//  Log(['keypress']);
{  if Key = '''' then Log(['KEY']);
  inherited;
  exit;}
  if DataLink.Active {and not EditorMode }then
  case Key of
    '''':
      with DataLink.DataSet do
      begin
        DisableControls;
        try
          if RecNo > 1 then
          begin
            Sel := SelectedField;
            if Sel <> nil then
            begin
              MoveBy(-1);
              V := Sel.Value;
              MoveBy(1);
              Sel.Value := V;
            end;
          end;
        finally
          DisableControls;
        end;
      end;
  end;
  if Key <> #0 then
    inherited;

  *)

end;

procedure TDBGridEx.TitleClick(Column: TColumn);
begin
  inherited;
end;

end.

