unit UImages;

interface

uses
  Windows, Classes, Controls, ExtCtrls,
  UMisc;

type
  TImageEx = class(TImage)
  private
    fSmartAlign: Boolean;
    procedure SetSmartAlign(const Value: Boolean);
    procedure DoSmartAlign;
  protected
    procedure RequestAlign; override;
  public
  published
    property SmartAlign: Boolean read fSmartAlign write SetSmartAlign;
  end;

implementation

uses
  Math;

{ TImageEx }

{-------------------------------------------------------------------------------
  DoSmartAlign: zorgt ervoor dat het image altijd in de juiste verhouding
  blijft.
-------------------------------------------------------------------------------}
procedure TImageEx.DoSmartAlign;
var
  {NewX, NewY, }ImgWidth, ImgHeight, ParentWidth, ParentHeight: Integer;
  R: TRect;
  XRatio: Double;
  YRatio: Double;
  Ratio: Double;
begin
  if Parent = nil then
    Exit;
//  with Image do controls
  if (Picture = nil) or (Picture.Graphic = nil) then
    Exit;

  ImgWidth := Picture.Graphic.Width;
  ImgHeight := Picture.Graphic.Height;
  ParentWidth := Parent.Width;
  ParentHeight := Parent.Height;

  if (ImgWidth = 0) or (ImgHeight = 0) then
    Exit; { geen deling door 0 haha }

  R.Left := 0;
  R.Top := 0;


//    Result := Bounds((Width - Picture.Width) div 2, (Height - Picture.Height) div 2,
  //    Picture.Width, Picture.Height)

  if (ImgWidth <= ParentWidth) and (ImgHeight <= ParentHeight) then
  begin
    //UpdateBoundsRect(Rect(Left, Top, Left + ImgWidth, Top + ImgHeight));
    R.Right := ImgWidth;
    R.Bottom := ImgHeight;


    if Center then
    begin
      R := CenterRect(R, Rect(0, 0, Parent.Width, Parent.Height));
    end;

//    Width := ImgWidth;
  //  Height := ImgHeight;
//    SetBounds(R);    umisc
    boundsrect := R;
    Stretch := False;
  end
  else begin
    XRatio := ParentWidth/ImgWidth;
    YRatio := ParentHeight/ImgHeight;
    Ratio := Min(XRatio, YRatio);

//    Width := Trunc(Ratio * ImgWidth);
//    Height := Trunc(Ratio * ImgHeight);
    R.Right := Trunc(Ratio * ImgWidth);
    R.Bottom := Trunc(Ratio * ImgHeight);

    if Center then
    begin
      R := CenterRect(R, Rect(0, 0, Parent.Width, Parent.Height));
    end;
    boundsrect := R;

//    UpdateBoundsRect(Rect(Left, Top, Left + Trunc(Ratio * ImgWidth), Top + Trunc(Ratio * ImgHeight)));
    Stretch := True;
  end;
  //Invalidate;
end;

{-------------------------------------------------------------------------------
  RequestAlign wordt aangeroepen wanneer de size veranderd, of de parent van
  size verandert en Align/Anchors gezet zijn.
-------------------------------------------------------------------------------}
procedure TImageEx.RequestAlign;
begin
  //  inherited RequestAlign;
  if fSmartAlign then
    DoSmartAlign
  else
    inherited RequestAlign;
end;

{-------------------------------------------------------------------------------
  SetSmartStrecth: Als SmartAlign aangezet wordt wordt Align op alClient gezet.
  Dit zorgt ervoor de TControl.RequestAlign wordt aangeroepen. Deze methode
  hebben we overschreven dus...
-------------------------------------------------------------------------------}
procedure TImageEx.SetSmartAlign(const Value: Boolean);
begin
//  if fSmartAlign = Value then
  //  Exit;
  fSmartAlign := Value;
  if fSmartAlign then
  begin
    ControlStyle := ControlStyle + [csOpaque];
    if AutoSize then
      Autosize := False;
    if Align <> alClient then
      Align := alClient;
    DoSmartAlign;
  end
  else begin
  end;
end;

end.

