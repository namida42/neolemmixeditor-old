unit UProgress;

interface

uses
  UMisc, Classes, Contnrs;

const
  TASK_MAXIMUMCHANGED   = Bit0;
  TASK_POSITIONCHANGED  = Bit1;
  TASK_STATUSCHANGED    = Bit2;
  TASK_DOINGCHANGED     = Bit3;

type
  TTaskList = class;

  TTaskChangeEvent = procedure(Sender: TObject; Flags: Cardinal) of object;

  TTask = class
  private
    fSubTaskList: TTaskList;
    fStatus: string;
    fPosition: Integer;
    fMaximum: Integer;
    fOnChange: TTaskChangeEvent;
    fDoing: string;
    procedure SetStatus(const Value: string);
    procedure SetMaximum(const Value: Integer);
    procedure SetPosition(const Value: Integer);
    procedure Changed(aFlags: Cardinal);
    procedure SetDoing(const Value: string);
  protected
    property OnChange: TTaskChangeEvent read fOnChange write fOnChange;
  public
    constructor Create;
    destructor Destroy; override;
  published
    property Status: string read fStatus write SetStatus;
    property Doing: string read fDoing write SetDoing;
    property Position: Integer read fPosition write SetPosition;
    property Maximum: Integer read fMaximum write SetMaximum;
  end;

  TTaskList = class(TObjectList)
  private
    function GetItem(Index: Integer): TTask;
  protected
  public
    function Add(Item: TTask): Integer;
    procedure Insert(Index: Integer; Item: TTask);
    property Items[Index: Integer]: TTask read GetItem; default;
  published
  end;

  TProgressUpdateEvent = procedure(Sender: TObject; Task: TTask; aFlags: Cardinal) of object;

  TProgress = class
  private
    fTaskList: TTaskList;
    fOnUpdate: TProgressUpdateEvent;
    procedure TaskChanged(Sender: TObject; aFlags: Cardinal);
  protected
  public
    constructor Create;
    destructor Destroy; override;
    function NewTask: TTask; virtual;
    property OnUpdate: TProgressUpdateEvent read fOnUpdate write fOnUpdate;
  published
  end;

implementation


{ TTask }

procedure TTask.Changed(aFlags: Cardinal);
begin
  if Assigned(fOnChange) then
    fOnChange(Self, aFlags);
end;

constructor TTask.Create;
begin
  inherited Create;
  fSubTaskList := TTaskList.Create;
end;

destructor TTask.Destroy;
begin
  fSubTaskList.Free;
  inherited Destroy;
end;

procedure TTask.SetDoing(const Value: string);
begin
  fDoing := Value;
  Changed(TASK_DOINGCHANGED);
end;

procedure TTask.SetMaximum(const Value: Integer);
begin
  fMaximum := Value;
  Changed(TASK_MAXIMUMCHANGED);
end;

procedure TTask.SetPosition(const Value: Integer);
begin
  fPosition := Value;
  Changed(TASK_POSITIONCHANGED);
end;

procedure TTask.SetStatus(const Value: string);
begin
  fStatus := Value;
  Changed(TASK_STATUSCHANGED);
end;

{ TTaskList }

function TTaskList.Add(Item: TTask): Integer;
begin
  Result := inherited Add(Item);
end;

function TTaskList.GetItem(Index: Integer): TTask;
begin
  Result := inherited Get(Index);
end;

procedure TTaskList.Insert(Index: Integer; Item: TTask);
begin
  inherited Insert(Index, Item);
end;

{ TProgress }

constructor TProgress.Create;
begin
  inherited;
  fTaskList := TTaskList.Create;
end;

destructor TProgress.Destroy;
begin
  fTaskList.Free;
  inherited;
end;

function TProgress.NewTask: TTask;
begin
  Result := TTask.Create;
  Result.OnChange := TaskChanged;
end;

procedure TProgress.TaskChanged(Sender: TObject; aFlags: Cardinal);
begin
  if Assigned(fOnUpdate) then
    fOnUpdate(Self, TTask(Sender), aFlags);
end;

end.

