{ delphi designtime registratie unit voor pkgkernel }

unit Kernel_Reg;

interface

procedure Register;

implementation

uses
  Classes,
  //DsgnIntf, {###DELPHI7}
  DesignIntf,
  UMisc,
  UMasterControls, UCalendar, UClock, DBCtrlsEx, DBGridsEx, Mx;

{$ifdef xxx}
type
  TMatrixPropertyCategory = class(TPropertyCategory)
  public
    class function Name: string; override;
    class function Description: string; override;
  end;

class function TMatrixPropertyCategory.Description: string;
begin
  Result := 'Matrix Property';
end;

class function TMatrixPropertyCategory.Name: string;
begin
  Result := 'Matrix';
end;
{$endif}

procedure Register;
begin
  //###DELPHI7 weggehaald RegisterComponents('Master', [TCalendarGrid, TCalendarDialog]); // ucalendar
  // en even vervangen door nep
  RegisterComponents('Master',
    [TCalendarDialog]); // ucalendar

  RegisterComponents('Master',
    [TClock]); // uclock
  RegisterComponents('Master',
    [TMasterLabel,
    TEditEx,
    {TComboBoxEx, ###DELPHI7}
    TCheckBoxEx,
    TTreeViewEx,
    TTicketBar]);

  RegisterComponents('MasterDB', [TDBEditEx]); // dbctrlsex
  RegisterComponents('MasterDB', [TDBGridEx]); // dbgridsex


  {$ifdef xxx}
  { alle typische matrix-properties onder 1 noemer in de objectinspector }
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'ColCount');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'Columns');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'DefaultColWidth');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'DefaultHeaderLineColor');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'DefaultLineColor');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'DefaultLineWidth');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'DefaultRowHeight');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'FixedCols');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'FixedRows');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'HeaderStyle');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'HighlightFontStyle');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'HighlightString');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'Options');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'RowCount');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'SelectMode');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'Style');
  RegisterPropertyInCategory(TMatrixPropertyCategory, TCustomMatrix, 'TopHeaders');
  {$endif}

  RegisterComponents('Master', [TMatrix]);
  RegisterComponents('MasterDB', [TDBMatrix]);


end;

end.




