unit gb_str;
interface
uses sysutils,controls, windows, math;

type parameterlist=array of string;

function titel(s:string):string;
function avoeg(s:string):string;
function tvoeg(s:string):string;
function vlet(s:string):string;
function anaam(s:string):string;
function roepnaam(s:string):string;
function tav(s,sex:string):string;
function aanhef(s,sex:string):string;
function firstchar(s:string):char;
function ufc(s:string):char;
function keepstr(const s,keep:string):string;
function DtoS(const aDate: TDateTime): ShortString;
function StoD(const aDateStr: ShortString): TDate;
function parameter(s:string;nr:integer):string;
function compact(s:string):string;
function Afrond (S : string; D : integer) : string;
function AllNice (S : string) : string;
function b2s(b:boolean):string;
function btrim(s:string):string;
function center(s:string;b:integer):string;
function countchar(s:string;ch:char):integer;
function countchars(s:string;chars:string):integer;
function countsubstr(s,sub:string):integer;
function estr(s:string):string;
function findsubstrN(s,sub:string;count:integer):integer;
function FirstNice (S : string) : string;
function gstr(s,zoek:string):boolean;
function gln(i,l:longint):string;
function getal(s:string):integer;
function hlstr(s:string):string;
function i2s(i:longint):string;
function c2h(i:longint):string;
function its(i,l:longint) : string;
function jn2s(b:boolean):string;
function lowstr(s:string):string;
function lstr(s:string;b:integer):string;
function ltrim(s:string):string;
function trimlastchar(s:string):string;
function mstr(s:string;b,l:integer):string;
function r2s(r:extended):string;
function rndvalstr(m:integer):string;
function rndstr(m:integer):string;
function repFstr(s,van,naar:string):string;
function repstr(const s,van,naar:string):string;
function rolstr(var s:string;d:integer):string;
function rstr(s:string;b:integer):string;
function rtrim(s:string):string;
function rvs(r:real;l,d:byte) : string;
function s2b(s:string):byte;
function s2jn(s:string):boolean;
function s2i(s:string):integer;
function s2r(s:string):extended;
function selectstr(s:string;divider,selector:char):string;
function SplitStr (S : string; Divider : char; Which : integer) : string;
function sti(s:string):longint;
function streal(s:string):real;
function SortStr(s:string):string;
function tijdstr(t:longint):string;
function trim(s:string):string;
function ustr(s:string):string;
function vulstr(s:string;l:integer):string;
procedure tijd2num(var tijd:string;var u,m,s:integer);
procedure dat2num(var dat:string;var d,m,j:integer);
procedure num2tijd(var tijd:string;var u,m,s:integer);
procedure num2dat(var dat:string;var d,m,j:integer);
function getFuncName(s:string):string;
procedure SplitFunction(s:string;var funcname:string;var parcount:integer;
                        var parlist:parameterlist);
function c2s(x,y,z:integer):string;
function rndbedrag:string;

//handige functies
function max(x,y:integer):integer;
function min(x,y:integer):integer;
function range(waarde,min,max:integer):integer;
procedure swapint(var a,b:integer);
procedure Msgbox(const S: string);
function precision(getal:extended;decimals:integer):extended;

(*
const F1=$013B;  sF1=$0154;  aF1=$0168;  cF1=$015E;
      F2=$013C;  sF2=$0155;  aF2=$0169;  cF2=$015F;
      F3=$013D;  sF3=$0156;  aF3=$016A;  cF3=$0160;
      F4=$013E;  sF4=$0157;  aF4=$016B;  cF4=$0161;
      F5=$013F;  sF5=$0158;  aF5=$016C;  cF5=$5162;
      F6=$0140;  sF6=$0159;  aF6=$016D;  cF6=$0663;
      F7=$0141;  sF7=$015A;  aF7=$016E;  cF7=$0164;
      F8=$0142;  sF8=$015B;  aF8=$016F;  cF8=$0165;
      F9=$0143;  sF9=$015C;  aF9=$0170;  cF9=$0166;
     F10=$0144; sF10=$015D; aF10=$0171; cF10=$0167;

    none=$0000;
     ins=$0152;
     del=$0153;
    home=$0147; chome=$0177;
    eind=$014F; ceind=$0175;
    pgup=$0149; cpgup=$0184;
    pgdn=$0151; cpgdn=$0176;

      up=$0148;   esc=$001B;
    down=$0150;   rtn=$000D;
    left=$014B;  cleft=$0173;
   right=$014D; cright=$0174;
    back=$0008;  cback=$007F;
     tab=$0009;   ctab=$010F; stab=$010F;


    aanh=$0022; {aanhalingtekens}
    altA=$011E; altB=$0130; altC=$012E; altD=$0120;
    altE=$0112; altF=$0121; altG=$0122; altH=$0123;
    altI=$0117; altJ=$0124; altK=$0125; altL=$0126;
    altM=$0132; altN=$0131; altO=$0118; altP=$0119;
    altQ=$0110; altR=$0113; altS=$011F; altT=$0114;
    altU=$0116; altV=$012F; altW=$0111; altX=$012D;
    altY=$0115; altZ=$012C;{ alt0=$0000;}

   ctrlA=$011E; ctrlB=$0130; ctrlC=$012E; ctrlD=$0120;
   ctrlE=$0112; ctrlF=$0121; ctrlG=$0122; ctrlH=$0123;
   ctrlI=$0117; ctrlJ=$0124; ctrlK=$0125; ctrlL=$0126;
   ctrlM=$0132; ctrlN=$0131; ctrlO=$0118; ctrlP=$0119;
   ctrlQ=$0110; ctrlR=$0113; ctrlS=$011F; ctrlT=$0114;
   ctrlU=$0116; ctrlV=$012F; ctrlW=$0111; ctrlX=$012D;
   ctrlY=$0115; ctrlZ=$012C;


var keycode:word;
*)
implementation

uses ulog;

function precision(getal:extended;decimals:integer):extended;
begin
  try
    if decimals>0 then begin
                   result:=round(getal*power(10,decimals))/power(10,decimals);
                end else
    if decimals<0 then begin
                   result:=round(getal/power(10,decimals))*power(10,decimals);
                 end else result:=getal;

  except
    result:=getal;
  end;
end;

function titel(s:string):string;
var i:integer;res:string;
    sa:array[1..25] of string[20];
    msa:integer;
begin
  i:=1;res:='';
  sa[i]:='mevrouw';inc(i);
  sa[i]:='Prof. Dr. Ing.';inc(i);
  sa[i]:='Prof.Dr.Ing.';inc(i);
  sa[i]:='Prof. Dr. Ir.';inc(i);
  sa[i]:='Prof.Dr.Ir.';inc(i);
  sa[i]:='Mevrouw';inc(i);
  sa[i]:='De heer';inc(i);
  sa[i]:='Dr. Ing.';inc(i);
  sa[i]:='Dr.Ing.';inc(i);
  sa[i]:='Ir. Ing.';inc(i);
  sa[i]:='Ir.Ing.';inc(i);
  sa[i]:='Dr. Ir.';inc(i);
  sa[i]:='Dr.Ir.';inc(i);
  sa[i]:='Prof.';inc(i);
  sa[i]:='Mevr.';inc(i);
  sa[i]:='Heer';inc(i);
  sa[i]:='Dhr.';inc(i);
  sa[i]:='Drs.';inc(i);
  sa[i]:='Mad.';inc(i);
  sa[i]:='Ing.';inc(i);
  sa[i]:='Mss.';inc(i);
  sa[i]:='Dr.';inc(i);
  sa[i]:='Ir.';inc(i);
  sa[i]:='Mr.';inc(i);
  sa[i]:='Hr.';inc(i);

    msa:=i-1;
    for i:=1 to msa do
     if length(s)>length(sa[i]) then
      if lstr(ustr(s),length(sa[i]))=ustr(sa[i]) then
      res:=lstr(s,length(sa[i]));
    titel:=res;
end;
{****************************************************************************}
function avoeg(s:string):string;
var i:integer;res:string;
    sa:array[1..20] of string[12];
    msa:integer;
begin
  i:=1;res:='';
  s:=btrim(s);
  sa[i]:='MBA';inc(i);
  sa[i]:='Msc';inc(i);
  sa[i]:='Bsc';inc(i);
  sa[i]:='MBI';inc(i);
  sa[i]:='Phd';inc(i);
  sa[i]:='SPD';inc(i);
    sa[i]:='RI';inc(i);
    sa[i]:='RA';inc(i);
    sa[i]:='BA';inc(i);
    msa:=i-1;
    for i:=1 to msa do
    begin
     if length(s)>=length(sa[i]) then
      if rstr(ustr(s),length(sa[i]))=ustr(sa[i]) then
      begin
      res:=rstr(s,length(sa[i]));break;end;
    end;
  avoeg:=res;
end;
{****************************************************************************}
function tvoeg(s:string):string;
{pak het tussenvoegsel}
var i,p:integer;res:string;
    sa:array[1..20] of string[12];
    msa:integer;

begin
    i:=1;res:='';
    sa[i]:='van der';inc(i);
    sa[i]:='van den';inc(i);
    sa[i]:='von der';inc(i);
    sa[i]:='van de';inc(i);
    sa[i]:='in het';inc(i);
    sa[i]:='in ''t';inc(i);
    sa[i]:='der';inc(i);
    sa[i]:='van';inc(i);
    sa[i]:='den';inc(i);
    sa[i]:='ter';inc(i);
    sa[i]:='von';inc(i);
    sa[i]:='de';inc(i);
    sa[i]:='le';inc(i);
    sa[i]:='la';inc(i);
    msa:=i-1;
    for i:=1 to msa do
    begin
      p:=pos(ustr(sa[i]+' '),ustr(s));
      if p>0 then begin res:=mstr(s,p,length(sa[i])+1);break;end;

    end;
  tvoeg:=res;
end;
{****************************************************************************}
function roepnaam(s:string):string;
begin
  result:='';
  if pos('.',s)=0 then
  if pos(' ',s)>0 then
  begin
    s:=splitstr(s,' ',1);
  end;
end;
{****************************************************************************}
function vlet(s:string):string;
var p:integer;stop:boolean;rn:string;
begin
  result:='';
  s:=btrim(repstr(s,titel(s),''));
  stop:=false;
  if s<>'' then
  begin
  if pos('.',s)>0 then
  begin
    {zoek de laatste punt}
    p:=length(s);
    while (s[p]<>'.') and not stop do
       if p>1 then dec(p) else stop:=true;
     if p>0 then result:=mstr(s,1,p);
  end else
  begin
    rn:=roepnaam(s);
    if rn<>'' then result:=rn[1]+'.';
  end;
  end;
end;
{****************************************************************************}
function anaam(s:string):string;
var ti,vl,tv,av,rn:string;
begin
  rn:=roepnaam(s);
//  log(['roepnaam',rn]);
  ti:=titel(s);
  vl:=vlet(s);
  tv:=tvoeg(s);
  av:=avoeg(s);
  s:=repstr(s,rn,'');
  s:=repstr(s,ti,'');
  s:=repstr(s,vl,'');
  s:=repstr(s,tv,'');
  s:=repstr(s,av,'');
  s:=repstr(s,av,'');
  result:=s;
end;
{****************************************************************************}
function sexe(s,sex:string):string;
var ti:string;
begin
  if sex='' then sex:='M';
  sex:=ustr(sex);
  ti:=titel(s);
  if sex[1]='M' then result:='M';
  if sex[1]='F' then result:='V';
  if sex[1]='V' then result:='V';
  if pos('MEVROUW',ustr(ti))>0 then result:='V';
  if pos('MEVR',ustr(ti))>0 then result:='V';
  if pos('MAD',ustr(ti))>0 then result:='V';
  if pos('MSS',ustr(ti))>0 then result:='V';
end;
{****************************************************************************}
function addspace(s:string):string;
begin
  result:=s;
  if rstr(s,1)<>' ' then result:=s+' ';
end;
{****************************************************************************}
function tav(s,sex:string):string;
var ti,vl,tv,av,an:string;
//    sa:array[1..20] of string[12];
begin
  ti:=addspace(titel(s));
  vl:=addspace(ustr(vlet(s)));
  if trim(vl)='' then tv:=addspace(firstnice(tvoeg(s)))
                 else tv:=addspace(lowstr(tvoeg(s)));
  sex:=sexe(s,sex);
  an:=btrim(allnice(anaam(s)));
//  vlog('tav : ti='+ti+'|vl='+vl+'|tv='+tv+'|an='+an+'|av='+av+'|');
  if sex='V' then sex:='mevrouw' else sex:='de heer';
  result:='T.a.v. '+sex+' '+ti+vl+tv+an+av;
  result:=repstr(result,'  ',' ');
  result:=repstr(result,'  ',' ');
end;
{****************************************************************************}
function aanhef(s,sex:string):string;
var ti,vl,tv,an,av:string;
begin
  ti:=addspace(titel(s));
  vl:=addspace(ustr(vlet(s)));
  tv:=addspace(firstnice(tvoeg(s)));
  av:=btrim(avoeg(s));
  an:=btrim(allnice(anaam(s)));
  sex:=sexe(s,sex);
//  vlog('aanh: ti='+ti+'|vl='+vl+'|tv='+tv+'|an='+an+'|av='+av+'|');
  if sex='V' then sex:='Geachte mevrouw' else sex:='Geachte heer';
  result:=sex+' '+tv+an+',';
  result:=repstr(result,'  ',' ');
  result:=repstr(result,'  ',' ');
end;
{****************************************************************************}
function firstchar(s:string):char;
begin
  if s='' then result:=#0 else result:=s[1];
end;

function ufc(s:string):char;
begin
  if s='' then result:=#0 else result:=upcase(s[1]);
end;

function rndbedrag:string;
begin
  result:=i2s(random(1000))+'.'+gln(random(100),2);
end;

function max(x,y:integer):integer;
begin
  if x>y then result:=x else result:=y;
end;
function min(x,y:integer):integer;
begin
  if x<y then result:=x else result:=y;
end;
function range(waarde,min,max:integer):integer;
begin
  if min>max then swapint(min,max);
  if waarde<min then waarde:=min else
    if waarde>max then waarde:=max;
  result:=waarde;
end;
procedure swapint(var a,b:integer);
var t:integer;
begin
  t:=a;a:=b;b:=t;
end;

function is_operator(op:string):integer;
var ops:array [1..10] of string[2];
    i:integer;
begin
  result:=0;
  ops[1]:='^-';  ops[2]:='^';
  ops[3]:='*-';  ops[4]:='*';
  ops[5]:='/-';  ops[6]:='/';
  ops[7]:='+-';  ops[8]:='+';
  ops[9]:='--';  ops[10]:='-';
  for i:=1 to 10 do
    if op=ops[i] then begin result:=i;break;end;
end;

function DtoS(const aDate: TDateTime): ShortString;
var
  Y, M, D: word;
begin
  DecodeDate(aDate, Y, M, D);
  Result := IntToStr(Y) + gln(M, 2) + gln(D, 2);
end;

function StoD(const aDateStr: ShortString): TDate;
var
  Y, M, D: word;
begin
  Y := StrToInt(Copy(aDateStr, 1, 4));
  M := StrToInt(Copy(aDateStr, 5, 2));
  D := StrToInt(Copy(aDateStr, 7, 2));
  Result := EncodeDate(Y, M, D);
end;


procedure splitformula(f:string;sign:string;apars:integer;parlist:parameterlist);
var haakje:integer;
var i:integer;h:string;str:boolean;

begin
   if f<>'' then
   begin
      setlength(parlist,0);apars:=0;sign:='';haakje:=0;h:='';str:=false;
//      vlog('splitformula '+f);
      f:=btrim(f)+chr(255);
      if lstr(f,2)='--' then begin sign:='+';f:=rstr(f,length(f)-2);end;
      if lstr(f,1)='-'  then begin sign:='-';f:=rstr(f,length(f)-1);end;
      if lstr(f,2)='-+' then begin sign:='-';f:=rstr(f,length(f)-2);end;
      if lstr(f,1)='+'  then begin sign:='+';f:=rstr(f,length(f)-1);end;
//      vlog('sign removed '+f);
        for i:=1 to length(f) do
        begin
        case f[i] of
        '"'  : begin if str=true then begin
                                          //sluithaakje
                                          str:=false;
                                          inc(apars);
                                          setlength(parlist,apars+1);
                                          parlist[apars]:=h+'"';
                                          h:='';end
                     else begin str:=true;h:=h+'=';end;
                end;
        '{'  :  inc(haakje);
        ')'  :  begin dec(haakje);
                   if f[i+1]<>chr(255)
                   then h:=h+')'
                   else begin if haakje=0 then
                              if apars<20 then inc(apars);
                              setlength(parlist,apars+1);
                              parlist[apars]:=h;h:='';end;
                end;
'^','*','/','+','-': if haakje=0 then
                      begin
                      end;
        else h:=h+f[i];

        end; //case
      end; //for
   end; //f<>''
end;

function lossevar(func,vari:string):boolean;
// lossvar('5*abc+bq',abc)==>true
// lossvar('5*"abc"+bq',abc)==>false
var i:integer;
begin
  for i:=1 to length(func) do
  begin
  end;
  result:=true;
end;

function update_formula(f,oldvar,newvar:string):string;
var funcname:string;
    parlist:parameterlist;
    apars:integer;
    p:integer;
begin
  if f<>'' then
  begin
     if f[1]='@' then
     begin SplitFunction(f,funcname,apars,parlist);
     for p:=1 to apars do
     begin
        if pos(oldvar,parlist[p])>0 then
        begin
           if parlist[p]<>'' then
           if parlist[p][1]='@'
           then begin
//                VLOG('RECURSIEF P'+I2S(P)+'['+PARLIST[P]+']');
                parlist[p]:=update_formula(parlist[p],oldvar,newvar)
                end
           else begin
//                 vlog('verwerk p'+i2s(p)+'['+parlist[p]+']');
                  repeat
                   if lossevar(parlist[p],oldvar) then
                   parlist[p]:=repstr(parlist[p],oldvar,newvar);
                  until not lossevar(f,oldvar);
                end;
          end; //zit oldvar er in?
     end; //for
     //herbouw de functie
     result:=funcname
     end //@
     else
     begin
//       vlog('verwerk f['+f+']');
       repeat
          if lossevar(f,oldvar) then
          f:=repstr(f,oldvar,newvar);
       until not lossevar(f,oldvar);
     end;
  end;//f is leeg
end;

function c2s(x,y,z:integer):string;
begin
  result:='('+i2s(x)+','+i2s(y)+','+i2s(z)+')';
end;

function keepstr(const s,keep:string):string;
var i:integer;h:string;
begin
//laat in een string alleen de char uit keep staan
//bv  keepstr('hallo123','1234567890') geeft: '123'
  h:='';
//  log(['Keep start',keep,s]);
  if length(s)>0 then
  for i:=1 to length(s) do
  begin
   if pos(s[i],keep)>0 then h:=h+s[i];
//   log(['Keep',keep,s[i],h]);
  end;
  result:=h;
end;
{****************************************************************************}
function parameter(s:string;nr:integer):string;
var p1,p2:integer;
begin
//haalt uit een functie parameternummer nr
//bv  parameter(@hallo(123,'abc',test),2) geeft: 'abc'
  result:='';
  if nr=1 then
  begin p1:=pos('(',s)+1;
        p2:=findsubstrN(s,',',nr);
        if p2=0 then p2:=pos(')',s);
     {   vlog([p1]);
        vlog([p2]);}
  end
  else begin
         p1:=findsubstrN(s,',',nr-1)+1;
         p2:=findsubstrN(s,',',nr);
         if p2=0 then p2:=pos(')',s);
{        vlog([p1]);
        vlog([p2]);}

       end;
  result:=mstr(s,p1,p2-p1);
end;


function r2s(r:extended):string;
{ Converts an real to a string for use with OutText, OutTextXY }
var
  s:string;nl:byte;
begin
  s:='';Str(r:20:8,s);
//  vlog(s);
  nl:=length(s);
  while s[nl]='0' do
  begin
    dec(nl);
    if nl<1 then break;
  end;
  if s[length(s)]='.' then s:=s+'0';
  s:=lstr(s,nl);
  if s[length(s)]='.' then dec(nl);
  s:=lstr(s,nl);
  r2s:=btrim(s);
end; { r2s }
{***********************************************************}
function SplitStr;
var
  i, j, teller, max: integer;
  w : string;
begin
  if rstr(s,1) <> divider then s := s + divider;
  max:=countchar(s,divider);
  if which>max then result:=''
  else
  begin
  SplitStr := '';
  teller := 0; i := 0; w := '';
    if which > 1 then
    repeat
    inc (i); if s[i] = divider then inc (teller);
    until (teller = which - 1) or (i > length(s));
    j := i;
    repeat
    inc (j); if s[j] = divider then inc (teller);
    until (teller = which) or (i >= length(s));
  if j > i then w := Copy (s, i + 1, j - i - 1)
  {Mstr (s, i + 1, j - i - 1)};
  if i >= length(s) then SplitStr := '';
  result:=w;
  end;
end;
{*********************************************************}
function findsubstrN(s,sub:string;count:integer):integer;
var pos,i,l,teller:integer;
begin
  l:=length(sub);teller:=0;pos:=0;
  for i:=1 to length(s) do
  begin
    if mstr(s,i,l)=sub then inc(teller);
    if teller=count then if pos=0 then pos:=i;
  end;
  findsubstrN:=pos;
end;

function countchar(s:string;ch:char):integer;
var i:integer;
begin
  result:=0;
  if length(s)>0 then
  for i:=1 to length(s) do
  if s[i]=ch then inc(result);
end;

function selectstr(s:string;divider,selector:char):string;
var i:integer;h:string;
begin
  result:='';h:='';
//if rstr(s,1)<>divider then s:=s+divider;
  i:=1;
  while i<countchar(s,divider)+1 do
  begin
    h:=splitstr(s,divider,i);
    if lstr(h,1)=selector then begin result:=h;exit;end;
    inc(i);
  end;
end;

function lowstr(s:string):string;
var i:integer;h:string;
begin
  h:='';
  for i:=1 to length(s) do
  case s[i] of
    'A'..'Z','[': h:=h+chr(ord(s[i])+32);
 chr(1)..'?'    : h:=h+s[i];
    '�'..'�'    : h:=h+s[i];
    '�'         : h:=h+'�';
    '�'         : h:=h+'�';
    '�'         : h:=h+'�';
    '�'         : h:=h+'�';
    '�'         : h:=h+'�';
    '�'         : h:=h+'�';
    '�'         : h:=h+'�';
    '�'         : h:=h+'�';
    else     h:=h+(s[i]);
  end;
  lowstr:=h;
end;
{****************************************************************************}
function b2s(b:boolean):string;
begin
  if b then b2s:='aan' else b2s:='uit';
end;
{****************************************************************************}
function gstr(s,zoek:string):boolean;
begin
  s:=lstr(ustr(s),length(ltrim(zoek)));
  gstr:=s=ustr(ltrim(zoek));
end;
{****************************************************************************}
function getFuncName(s:string):string;
// @left(a,4)  ==> @left
var i,l:integer;h:string;
begin
  result:='';
  l:=length(s);
  if l>0 then
  begin
    if s[1]='@' then
    begin
      i:=1;
      repeat
      h:=h+s[i];
      inc(i);if i>l then break;
      until s[i]='(';
      result:=h;
    end else result:='@Error(no @)';
  end else result:='@Error(func empty)';
//  vlog('getfuncnaam done');
end;
{****************************************************************************}
procedure SplitFunction(s:string;var funcname:string;var parcount:integer;
                        var parlist:parameterlist);
var i,{haakje,}l:integer;h:string;
    str:boolean;
begin
//  vlog('start splitfunct ['+s+']');
  h:='';//haakje:=0;
  str:=false;parcount:=0;
  setlength(parlist, parcount);
  l:=length(s);
  if l>0 then
  begin
    funcname:=getfuncname(s);
    s:=btrim(repstr(s,funcname,''))+chr(255);
//    vlog('s='+s);
    l:=length(s);
    if l>0 then
    for i:=1 to l do
    begin
//      vlog(s[i]+' ['+h+']');
      case s[i] of
      '(': begin //inc(haakje);
                 if (parcount>0) or str then h:=h+'(';end;
      ')': begin //dec(haakje);
                 if s[i+1]<>chr(255) then h:=h+')'
                 else
                 begin if parcount<20 then inc(parcount);
                       setlength(parlist,parcount+1);
//                       if h<>'""' then h:=repstr(h,'""','"');
                       parlist[parcount]:=h;h:='';end;
           end;
      '"': begin
           if str then str:=false else str:=true;
           h:=h+'"';
           end;
      ',': if not str then
           begin
             if parcount<20 then inc(parcount);
             setlength(parlist,parcount+1);
//             if h<>'""' then h:=repstr(h,'""','"');
             parlist[parcount]:=h;h:='';
           end else h:=h+',';
      else h:=h+s[i];
      end;
    end;
  end;
//  vlog('splitfunct done');
end;
{****************************************************************************}
function ustr(s:string):string;
var i:integer;h:string;
begin
  h:='';for i:=1 to length(s) do h:=h+upcase(s[i]);ustr:=h;
end;
{****************************************************************************}
function rvs(r:real;l,d:byte) : string;
{ Converts an real to a string for use with OutText, OutTextXY }
var
  s:string;i:byte;
begin
  s:='';Str(r:l:d,s);
  if length(s)>l then
   begin s:='';for i:=1 to l-3 do s:=s+'*';s:=s+'.**';end;
  rvs:=s;
end; { rvs }
{*****************************************************************************}
function sti(s:string):longint;
var hulp:longint;code:integer;
begin
  {#EL hint hulp:=0;}val(s,hulp,code);
  if code=0 then sti:=hulp else sti:=0;
end;
{****************************************************************************}
function its(i,l:longint) : string;
{ Converts an integer to a string for use with OutText, OutTextXY }
var
  S : string;
begin
  S:='';
   Str(i:l,S);
  its := S;
end; { Int2Str }
{****************************************************************************}
function getal(s:string) : integer;
begin
  s:=keepstr(s,'0123456789');
  result:=s2i(s);
end;
{****************************************************************************}
function streal(s:string):real;
var
  code:integer;r:real;
begin
  s:=trim(s);
  val(s,r,code);
  if code=0 then streal:=r else streal:=0;
end; { Int2Str }
{*****************************************************************************}
function rnd_str(m:integer):string;
var h:string;i:integer;k:integer;
begin
  h:='';
  for i:=1 to random(m)+1 do
  begin
  k:=random(40);
  case k of
   0..9: h:=h+chr(ord('0')+k);
 10..36: if random(2)=1 then h:=h+chr(ord('@')+k-9)
                        else h:=h+low(chr(ord('@')+k-9));
   else  h:=h+' ';
  end;end;
  result:=h;
end;
{****************************************************************************}
function lstr(s:string;b:integer):string;
begin
   lstr:=copy(s+vulstr(' ',b),1,b);
end;
{****************************************************************************}
function rstr(s:string;b:integer):string;
begin
{  if b>l then h:=vul(' ',b-l);}
  rstr:=copy(s,length(s)-b+1,b);
end;
{****************************************************************************}
function mstr(s:string;b,l:integer):string;
begin
  result:=copy(s,b,l);
end;
{****************************************************************************}
function estr(s:string):string;
var h:string;
begin
  h:=lowstr(s);
  h[1]:=upcase(h[1]);
  result:=h;
end;
{****************************************************************************}
function hlstr(s:string):string;
var i:integer;
begin
  for i:=1 to length(s) do
   if s[i]>'@'then s[i]:=chr(ord(s[i])-32);
  result:=s;
end;
{*****************************************************************************}
function gln(i,l:longint):string;
var s:string;
begin
  if i>=0 then
  begin
    str(i:l,s);
    if (length(s)>l) then gln:=vulstr('*',l)
                     else gln:=rstr('00000000'+trim(s),l);
  end else result:=vulstr('.',l);
end;
{*****************************************************************************}
function ltrim(s:string):string;
var i,l:integer;
begin
  i:=1;
  l:=length(s);
  if l>0 then
  while s[i]=' ' do begin inc(i);if i>l then break;end;
  result:=rstr(s,length(s)-i+1);
end;
{*****************************************************************************}
function trimlastchar(s:string):string;
var l:integer;
begin
  l:=length(s);
  result:='';
  if l>0 then
   result:=lstr(s,length(s)-1);
end;
{*****************************************************************************}
function rtrim(s:string):string;
var i,l:integer;h:string;skip:boolean;
begin
  h:='';
  i:=length(s);skip:=true;
  if i>0 then
  for l:=i downto 1 do
  begin
    if s[l]<>' ' then skip:=false;
    if not skip then h:=s[l]+h;
  end;
  result:=h;
end;
{*****************************************************************************}
function btrim(s:string):string;
begin
  result:=ltrim(rtrim(s));
end;
{*****************************************************************************}
function trim(s:string):string;
var i:integer;h:string;
begin
  h:='';for i:=1 to length(s) do if s[i]<>' ' then h:=h+s[i];
   result:=h;
end;
{*****************************************************************************}
function repFstr(s,van,naar:string):string;
var i:integer;h:string;klaar:boolean;
begin
  i:=1;h:='';klaar:=false;
  if pos(van,s)>0 then
  while (i<=length(s)) do
  begin
    if not klaar and (mstr(s,i,length(van))=van)
    then begin h:=h+naar;inc(i,length(van)-1);klaar:=true;end
    else h:=h+s[i];
    inc(i);
  end else h:=s;
  result:=h;
end;
{*****************************************************************************}
function repstr(const s,van,naar:string):string;
var i:integer;h:string;klaar:boolean;
begin
//  log(['repstr start',s,van,naar]);
  i:=1;h:='';klaar:=false;
  if pos(van,s)>0 then
  while (i<=length(s)) do
  begin
    if not klaar and (mstr(s,i,length(van))=van)
      then begin h:=h+naar;inc(i,length(van)-1);klaar:=true;
           //log([h]);
           end
    else h:=h+s[i];
    inc(i);
  end else h:=s;
//  log(['repstr=',s,h]);
  result:=h;
end;
{*****************************************************************************}
function center(s:string;b:integer):string;
var i,l:integer;h:string;
begin
  h:='';
  l:=length(s);
  if b>l then for i:=1 to ((b-l) div 2) do h:=h+' ';
  result:=lstr(h+s+h,b);
end;
{****************************************************************************}
function i2s(i:longint):string;
begin
  i2s:=trim(its(i,10));
end;
{****************************************************************************}
function hexchar(i:integer):char;
begin
  if i<10 then result:=chr(ord('0')+i) else
               result:=chr(ord('A')+i-10);
end;
{****************************************************************************}
function c2h(i:longint):string;
var h,l:integer;
begin
  h:=i div 16 ;
  l:=i mod 16 ;
  result:=hexchar(h)+hexchar(l);
end;
{****************************************************************************}
function s2i(s:string):integer;
begin
  s2i:=sti(trim(s));
end;
{****************************************************************************}
function jn2s(b:boolean):string;
begin
  if b then jn2s:='J' else jn2s:='N';
end;
{****************************************************************************}
function s2jn(s:string):boolean;
begin
  s2jn:=s[1]='J';
end;
{****************************************************************************}
function s2b(s:string):byte;
begin
  s2b:=sti(trim(s));
end;
{****************************************************************************}
function tijdstr(t:longint):string;
var u,m,s:longint;
begin
  {#EL hint u:=0;}{m:=0;}{#EL hint s:=0;}
  if t>=3600 then
  begin
    u:=t div 3600;
    t:=t mod 3600;
    m:=t div 60;
    s:=t mod 60;
    tijdstr:=gln(u,2)+':'+gln(m,2)+':'+gln(s,2);
  end else
  begin
    m:=t div 60;
    s:=t mod 60;
    tijdstr:=gln(m,2)+':'+gln(s,2);
  end;
end;
{****************************************************************************}
function EnumStr (selector,enumeratestr : string; itemdivider,selectordivider : char) : string;
var num:integer;split:string;
begin
  {formaat: sel1:waarde | sel2:waarde2
   | = item divider
   : = selector divider}
  enumstr:='?unknown?';
  num:=1;
  repeat
    split:=splitstr(enumeratestr,itemdivider,num);
    if selector=splitstr(split,selectordivider,1) then
    begin enumstr:=splitstr(split,selectordivider,2);exit;end;
    inc(num);
  until split='';
end;

function countchars(s:string;chars:string):integer;
var tel,i:integer;
begin
  tel:=0;
  for i:=1 to length(s) do
  begin
    if pos(s[i],chars)>0 then inc(tel);
  end;
  countchars:=tel;
end;

function countsubstr(s,sub:string):integer;
var {#EL hint pos,}i,l,teller:integer;
begin
  l:=length(sub);teller:=0;{#EL hint pos:=0;}
  for i:=1 to length(s) do
  begin
    if mstr(s,i,l)=sub then inc(teller);
  end;
  countsubstr:=teller;
end;


{ intern }
procedure QuickSortStr (var v_arr : string; p_left, p_right : word);
var
  min : word; max : word; mid : char; temp : char;
begin

  min:=p_left; max:=p_right; mid:=v_arr[(p_left+p_right) div 2];

  repeat
  while v_arr[min]<mid do inc(min); while v_arr[max]>mid do dec(max);
  if min<=max then
    begin
    temp:=v_arr[min]; v_arr[min]:=v_arr[max]; v_arr[max]:=temp; inc(min); dec(max);
    end;
  until min>max;

  if p_left<max then quicksortstr(v_arr,p_left,max);
  if min<p_right then quicksortstr(v_arr,min,p_right);

end;
{***********************************************************}
function SortStr(s:string):string;
begin
  QuickSortStr(S, 1, length (S));  SortStr := S;
end;
{*********************************************************}
function s2r(s:string):extended;
var
  code:integer;r:real;
begin
  s:=trim(s);
  val(s,r,code);
  if code=0 then s2r:=r else s2r:=0;
end; { Int2Str }
{*********************************************************}
function rndstr(m:integer):string;
var h:string;i:integer;k:integer;
begin
  h:='';
  for i:=1 to random(m)+1 do
  begin
   k:=random(37);
   case k of
    0..25 : h:=h+chr(ord('@')+k);
    26..35 : h:=h+chr(ord('0')+k-26);
    else h:=h+' ';
   end;
  end;
  rndstr:=h;
end;
{*********************************************************}
function rndvalstr(m:integer):string;
var h:string;i:integer;k:integer;
begin
  h:='';
  for i:=1 to random(m)+1 do
  begin
   k:=random(10);
   h:=h+chr(ord('0')+k);
  end;
  result:=h;
end;
{*********************************************************}
function rolstr(var s:string;d:integer):string;
var i:integer;{#EL hint h:string;}
begin
  if length(s)<d then d:=length(s);
  if d<0 then
          for i:=1 to d do s:=rstr(s,1)+lstr(s,length(s)-1)
         else
          for i:=1 to abs(d) do s:=rstr(s,length(s)-1)+lstr(s,1);
  rolstr:=s;
end;
{*********************************************************}
function vulstr(s:string;l:integer):string;
var i:integer;h:string;
begin
  h:='';
  for i:=1 to l do h:=h+s;
  vulstr:=h;
end;
{*********************************************************}
function FirstNice (S : string) : string;
begin
{  s := lowstr(s);}
 result:=s;
 if s='' then exit;
   s[1] := UpCase (S[1]);
 result:=s;
end;
{*********************************************************}
function repstrof(s,van,naar:string):string;
var i:integer;h:string;klaar:boolean;
begin
  i:=1;h:='';klaar:=false;
  if pos(van,s)>0 then
  while (i<=length(s)) do
  begin
    if not klaar and (mstr(s,i,length(van))=van)
    then begin h:=h+naar;inc(i,length(van)-1);klaar:=true;end
    else h:=h+s[i];
    inc(i);
  end else h:=s;
  repstrof:=h;
end;
{*********************************************************}
function Afrond;
var
  i, m : longint; t, r : real;
begin
  Afrond := s;
  r:=s2r(s);
  if d>0 then
    begin
    m:=1;for i:=1 to d do m:=m*10;
    t:=trunc(r);
    r:=r-t;
    r:=r*m;r:=round(r);r:=r/m+t;
    end;
  if d=0 then
    begin
    r:=round(r);
    end;
  if d<0 then
    begin
    m:=1;for i:=1 to abs(d) do m:=m*10;
    r:=r/m;r:=round(r);r:=r*m;
    end;
  afrond:=r2s(r);
end;
{***********************************************************}
function compact(s:string):string;
var r:real;eh:string;
begin
  r:=s2r(s);eh:=' ';
  if r>1000       then eh:='K';
  if r>1000000    then eh:='M';
  case eh[1] of
  'M': s:=r2s(s2r(s)/1000000);
  'K': s:=r2s(s2r(s)/1000);
  end;
  s:=afrond(s,1);
  if s<>'' then
  while ((rstr(s,1)='0') or (rstr(s,1)='.')) do
  begin
    s:=lstr(s,length(s)-1);
    if s='' then break;
  end;
  if eh<>' ' then s:=s+eh;
  compact:=s;
end;
{�����������������������������������������������������������������������������}
function Allnice;
var
  i : integer;
begin
  result:=s;
  if s<>'' then
  begin
  s[1] := Upcase (s[1]);
  for i := 2 to length (s) do
  if s[i - 1] = ' ' then
    if s[i] <> ' ' then s[i] := Upcase (s[i]);
  result:=s;
  end;
end;
{****************************************************************************}
procedure tijd2num(var tijd:string;var u,m,s:integer);
begin
  u:=s2i(lstr(tijd,2));
  m:=s2i(mstr(tijd,4,2));
  s:=s2i(rstr(tijd,2));
end;
{****************************************************************************}
procedure dat2num(var dat:string;var d,m,j:integer);
begin
  d:=s2i(rstr(dat,2));
  m:=s2i(mstr(dat,5,2));
  j:=s2i(lstr(dat,4));
end;
{****************************************************************************}
procedure num2tijd(var tijd:string;var u,m,s:integer);
begin
  tijd:=gln(u,2)+':'+gln(m,2)+':'+gln(s,2);
end;
{****************************************************************************}
procedure num2dat(var dat:string;var d,m,j:integer);
begin
  dat:=gln(d,2)+'-'+gln(m,2)+'-'+gln(j,4);
end;
procedure Msgbox(const S: string);
begin
  MessageBox(0, PChar(S), PChar('ok'), MB_OK + MB_TASKMODAL);
end;

end.
