object DebugForm: TDebugForm
  Left = 363
  Top = 159
  Width = 342
  Height = 463
  Caption = 'DebugForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = Form_Create
  OnDestroy = Form_Destroy
  OnKeyPress = Form_KeyPress
  DesignSize = (
    326
    424)
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox: TListBox
    Left = 0
    Top = 0
    Width = 334
    Height = 409
    Anchors = [akLeft, akTop, akRight, akBottom]
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clYellow
    Font.Height = -13
    Font.Name = 'Courier'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 0
    OnDblClick = ListBox_DblClick
  end
  object FilterEdit: TEdit
    Left = 2
    Top = 413
    Width = 330
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 1
    OnKeyDown = FilterEdit_KeyDown
  end
end
