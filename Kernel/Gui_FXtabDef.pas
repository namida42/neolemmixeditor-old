unit Gui_FXtabDef;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  gui_uforms,extctrls, mx, dateutils, Variants, typinfo,
   { kernel }
  UMisc, UFastStrings,

  { core }
  Core_UModules,
  Core_log,
  { data }
  Data_Types,
  Data_DImport,
  Data_Extensions,
  Data_Events,
  Data_DPublic,
  { gui }
  Gui_Consts,
  Gui_Utils,
  Gui_FQuickFilter2,
  Gui_FCard,
  Gui_FList,
  Gui_FButtons,
  Gui_FMenuButtons,
  { master }
  Menus, Db, IBCustomDataSet, ActnList, Provider, DBClient, StdCtrls;
  //SynEditHighlighter, SynHighlighterPas, SynEdit, SynMemo;


type
  TFormImportManager = class(TFormcompound)
  private
    { Private declarations }
    ActionList: TActionlist;

    List        : TGuiList;
    Xtab        : TGuiList;
    MainButtons : TGuiButtons;

    fNoErrors     : Boolean;

    ActTest            : TActionEx;

    procedure DefineLayout;
    procedure UpdateImportActions;
    function DefineActions:TActionList;


//interne functies
//actions
{    procedure InsertTabel(sender: TObject);
    procedure DeleteTabel(sender: TObject);
    procedure OpenBestand(sender: TObject);
    procedure AutoVeld(sender: TObject);}
//    procedure ShowHex(sender: TObject);
    procedure ActTestExec(sender: TObject);
   public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  end;

//procedure Import_flat_csv(Const Filenaam: string; Importdef:Tstringlist; de:TIBDatasetExt);



implementation

uses gb_str;

function TFormXtabManager.DefineActions:TActionlist;
var
  A : TActionEx;

    function maakAct(const aName,aPrompt,aHint:string;gui:TGuiFormChild; ExecEvent: TNotifyEvent): TActionEx;
    begin
      A:=TactionEx.Create(self);
      if aPrompt='' then A.Caption:=aName
                    else A.Caption:=aPrompt;
      A.Name:=aName;
      A.Hint:=aHint;
      A.ActionList:=ActionList;
      A.OnExecute:=ExecEvent;
      if gui is TGuiButtons then
      TGuiButtons(Gui).AddCustomButton(a);
      result := A;
    end;

begin
  if assigned(ActionList) then ActionList.Free;
  ActionList:=tActionList.create(self);
  result:=ActionList;

  MainButtons.ButtonTypes:=[btCancel]+[btOK];

  ActTest:=MaakAct('Test','','',mainbuttons,ActTestExec);
end;

procedure TFormXtabManager.DefineLayout;
var
  Boven, Onder,// OnderOnder,// OnderBoven,
  Rechts, Links : TWerkvlak;
  dummy: TGuiFormChild;
begin
  UniqueName := 'Master.Import';

  Boven:= AddGui(Self, nil, Dummy, alClient,-1,-1,alBottom);
  Onder:= AddGui(Self, nil, Dummy, alBottom);
  Links:= AddGui(Boven, nil,  Dummy, alLeft,-1,-1,alRight);
  Rechts:= AddGui(Boven, nil,  Dummy, alClient);
//  OnderBoven:= AddGui(Onder, nil, Dummy, alClient,-1,100);
//  OnderOnder:= AddGui(Onder, nil, Dummy, alBottom);

  AddGui(Links,tGuiList,List,alClient);                         ListIT.UniqueName:=UniqueName+'.Tabel.List';
  AddGui(Rechts,tGuiList,Xtab,alClient);                        ListIV.UniqueName:=UniqueName+'.Xtab.List';

  AddGui(Onder,TGuiButtons,MainButtons,alBottom);               MainButtons.UniqueName:=UniqueName+'.MainButtons';

end;

constructor TFormXtabManager.Create(aOwner: TComponent);
var x,y:integer;
begin
  inherited;
  DefineLayout;
  XtabMan:=TXtabManager.create;
  ActionList:=DefineActions;
  KeyPreview:=true;
  fNoErrors:=false;
//  CurrentRow:=1;
//  ShowAscii(nil);

  with ImportData do
  begin
//  windlg(pathtoapplication);
//  Assert(ImportBatch <> nil, 'ImportManager.Create: ImportBatch = nil');
{  createguiSource(ImportBatch,PathToApplication+'temp\@GUI_ImportBatch.pas');
  createguiSource(ImportSequence,PathToApplication+'temp\@GUI_ImportSequence.pas');
  createguiSource(ImportTabel,PathToApplication+'temp\@GUI_ImportTabel.pas');
  createguiSource(ImportVeld,PathToApplication+'temp\@GUI_ImportVeld.pas');
}

  List.Source.DataSet:=ImportTabel;
  Xtab.Source.DataSet:=ImportVeld;
//  windlg('c');
{
  with ImportTabel do
   with ListIT do
    begin
      MatrixsSettings;
      Matrix.Columns.clear;
 //     AddColumnSize(Aanmaakdatum.Fieldname,'Aanmaakdatum',esSmall);
      AddColumnSize(Importtabel_id.Fieldname,'Key',esExSmall);
      AddColumnSize(Omschrijving.Fieldname,'Omschrijving',esMedium);
//      AddColumnSize(Bestand.Fieldname,'Bestand',esSmall);
{      AddColumnSize(Bestandstype_id.Fieldname,'Bestandstype_id',esSmall);
      AddColumnSize(Ingelezendoor_id.Fieldname,'Ingelezendoor_id',esSmall);
      AddColumnSize(Inleesdatum.Fieldname,'Inleesdatum',esSmall);
      AddColumnSize(Koppeltabel.Fieldname,'Koppeltabel',esSmall);
      AddColumnSize(Koppeltabel_id.Fieldname,'Koppeltabel_id',esSmall);
      AddColumnSize(Pad.Fieldname,'Pad',esSmall);
      AddColumnSize(Recordscheiding.Fieldname,'Recordscheiding',esSmall);
      AddColumnSize(Recordscheiding_id.Fieldname,'Recordscheiding_id',esSmall);
      AddColumnSize(Skipchar.Fieldname,'Skipchar',esSmall);
      AddColumnSize(Skiplines.Fieldname,'Skiplines',esSmall);
      AddColumnSize(Toelichting.Fieldname,'Toelichting',esSmall);
      AddColumnSize(Veldscheiding.Fieldname,'Veldscheiding',esSmall);
      AddColumnSize(Veldscheiding_id.Fieldname,'Veldscheiding_id',esSmall);
      AddColumnSize(Wijzigdatum.Fieldname,'Wijzigdatum',esSmall);}
    end;
}
{
    with ImportTabel do
    with CardIT do
    begin
      x:=1;y:=1;
      AddCompoundDBEdit(x,y,Omschrijving.Fieldname,'Omschrijving',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Pad.Fieldname,'Pad',esMedium,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Bestand.Fieldname,'Bestand',esMedium,sbOpenDialog);inc(y);
      AddCompoundDBEdit(x,y,Bestandstype.Fieldname,'Bestandstype',esSmall,sbPicklist);inc(y);
      AddCompoundDBEdit(x,y,Recordscheiding.Fieldname,'Recordscheiding',esSmall,sbPicklist);inc(y);
      AddCompoundDBEdit(x,y,Veldscheiding.Fieldname,'Veldscheiding',esSmall,sbPicklist);inc(y);
      AddCompoundDBEdit(x,y,Skipchar.Fieldname,'Kop (karakters)',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Skiplines.Fieldname,'Kop (regels)',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Koppeltabel.Fieldname,'Main Dataset',esSmall,sbPicklist);inc(y);
//      FldExt(Koppeltabel).FixedList:=
//      ImportData.RefreshIMports;
      x:=2;y:=1;
      AddCompoundDBEdit(x,y,Importtabel_id.Fieldname,'Importtabel_id',esSmall,sbNone);inc(y);
//    AddCompoundDBEdit(x,y,Aanmaakdatum.Fieldname,'Aanmaakdatum',esSmall,sbNone);inc(y);
      inc(y);
      inc(y);
      AddCompoundDBEdit(x,y,Bestandstype_id.Fieldname,'Bestandstype_id',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Recordscheiding_id.Fieldname,'Recordscheiding_id',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Veldscheiding_id.Fieldname,'Veldscheiding_id',esSmall,sbNone);inc(y);

      //      AddCompoundDBEdit(x,y,Ingelezendoor_id.Fieldname,'Ingelezendoor_id',esSmall,sbNone);inc(y);
//      AddCompoundDBEdit(x,y,Inleesdatum.Fieldname,'Inleesdatum',esSmall,sbNone);inc(y);
//      AddCompoundDBEdit(x,y,Recordscheiding.Fieldname,'Recordscheiding',esSmall,sbNone);inc(y);
//      AddCompoundDBEdit(x,y,Recordgrootte.Fieldname,'Recordgrootte',esSmall,sbNone);inc(y);
//      AddCompoundDBEdit(x,y,Veldscheiding.Fieldname,'Veldscheiding',esSmall,sbNone);inc(y);
//      AddCompoundDBEdit(x,y,Koppeltabel_id.Fieldname,'Koppeltabel_id',esSmall,sbNone);inc(y);
//      AddCompoundDBEdit(x,y,Toelichting.Fieldname,'Toelichting',esSmall,sbNone);inc(y);
//      AddCompoundDBEdit(x,y,Wijzigdatum.Fieldname,'Wijzigdatum',esSmall,sbNone);inc(y);
    end;
{
   with DeImportVeld do
    with VeldCard do
     begin
      x:=1;y:=1;
      AddCompoundDBEdit(x,y,Importveld_id.Fieldname,'Importveld_id',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Importtabel_id.Fieldname,'Importtabel_id',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Veld.Fieldname,'Veldnaam',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Veldlengte.Fieldname,'Veldlengte',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Veldtype_id.Fieldname,'Veldtype_id',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Totpos.Fieldname,'Totpos',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Vanpos.Fieldname,'Vanpos',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Koppelveld.Fieldname,'Koppelveld',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Koppelveld_id.Fieldname,'Koppelveld_id',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Lookup_id.Fieldname,'Lookup_id',esSmall,sbNone);inc(y);
      AddCompoundDBEdit(x,y,Lookup_ref.Fieldname,'Lookup_ref',esSmall,sbNone);inc(y);
//    AddCompoundDBEdit(x,y,Wijzigdatum.Fieldname,'Wijzigdatum',esSmall,sbNone);inc(y);
//      AddCompoundDBEdit(x,y,Aanmaakdatum.Fieldname,'Aanmaakdatum',esSmall,sbNone);inc(y);
    end;
}

{
   with ImportVeld do
   with ListIV do
    begin
      MatrixsSettings;
      Matrix.Columns.clear;

      AddColumnSize(Importveld_id.Fieldname,'VID',esExSmall);
      AddColumnSize(Importtabel_id.Fieldname,'TID',esExSmall);
      AddColumnSize(Veldnummer.Fieldname,'Nr',esExSmall);
      AddColumnSize(Header.Fieldname,'Header',esSmall);
      AddColumnSize(Dset.Fieldname,'Dataset',esSmall);
      AddColumnSize(Veld.Fieldname,'Veldnaam',esSmall);
      AddColumnSize(VeldLengte.Fieldname,'Lengte',esExSmall);
      AddColumnSize(Data.Fieldname,'Data',esSmall);
      AddColumnSize(Veldtype_id.Fieldname,'Veldtype_id',esSmall);
      AddColumnSize(Veldtype.Fieldname,'Veldtype',esSmall);
      AddColumnSize(Totpos.Fieldname,'Totpos',esSmall);
      AddColumnSize(Vanpos.Fieldname,'Vanpos',esSmall);
      AddColumnSize(Meenemen.Fieldname,'Meenemen',esSmall);

{      AddColumnSize(Koppelveld.Fieldname,'Koppelveld',esSmall);
      AddColumnSize(Koppelveld_id.Fieldname,'Koppelveld_id',esSmall);
      AddColumnSize(Lookup_id.Fieldname,'Lookup_id',esSmall);
      AddColumnSize(Lookup_ref.Fieldname,'Lookup_ref',esSmall);
//    AddColumnSize(Aanmaakdatum.Fieldname,'Aanmaakdatum',esSmall);
//    AddColumnSize(Wijzigdatum.Fieldname,'Wijzigdatum',esSmall);
    end;

}


 end; // with XtabData

  BetterShowFrames;
  List.Matrix.DoRefresh;
end;

destructor TFormXtabManager.Destroy;
begin
  XtabMan.Free;
  inherited;
end;


procedure TFormXtabManager.ActTestExec(sender: TObject);
var d,f:integer;
    de:TIBDatasetExt;
begin
  windlg('Test Import');
  with PublicData do
  begin
    for d:=0 to Database.DataSetCount-1 do
    begin
      log(['dataset   ',Database.DataSets[d].name]);
      if database.DataSets[d] is TIBDatasetExt then
      begin
        de:=TIBDatasetExt(Database.DataSets[d]);
        log(['datasetext',de.Name]);
        for f:=0 to de.FieldExtCount-1 do
        with de.FieldExts[f] do
        begin
          log([Field.FieldName,MasterType2Str(MasterType),IsMainTableField]);
        end; //for
      end; //is
    end; //for
  end;
end;

procedure TFormXtabManager.ShowRawData(sender: TObject);
begin
  browse(ImpMan.rawdata);
end;

end.

