{$WEAKPACKAGEUNIT ON}
{-------------------------------------------------------------------------------
  Tredox Unit. Mag alleen gebruik maken van PKGUtils.
  De unit moet worden gebruikt door
  - TredoxBuilder
  - TredoxSetup
  - TredoxFrameWork
-------------------------------------------------------------------------------}
unit UPKG;

interface

uses
  Windows, Classes, Messages, SysUtils,
  UMisc, UFiles, UFastStrings, typInfo;

const
  STredox = 'tredox';
  SKernel = 'kernel';
  SFramework = 'framework';
  SModule = 'module';

  STredoxPackageMask = 'PKG*70.bpl';
  SIdentifyModuleProcName = 'IdentifyModule'; { do not change }

type
  TTredoxFileType = (
    tfNone,
    tfDelphiBPL,
    tfDelphidDLL,
    tfFireBirdDLL,
    tfKernelBPL,
    tfFrameWorkBPL,
    tfModuleBPL,
    tfLanguageFile
  );

  TTredoxGroupType = (
    gtNone,
    gtDelphi,
    gt3Party,
    gtKernel,
    gtFramework,
    gtTredoxExe,
    gtTredoxModule
  );

  TTredoxGroupKind = (
    gkNone,
    gkProject,
    gkFileGroup
  );

{  TTredoxInstallMode = (
    imAlways, // altijd
    imNewerDate, // datum
    imHigherVersion, // versie
    imNeverOverwrite // niet installeren als al bestaat
  );}

  TTredoxUpdateMethode = (
    umUpdate_files_with_newer_versions,
    umUpdate_files_with_more_recent_dates,
    umAlways_update_files,
    umNever_overwrite_files,
    umUpdate_files_when_different
  );

  TTredoxPackage = (
    tpUnknown,
    tpFramework,  // tredox.framework.xxx
    tpModule      // tredox.module.xxx
  );
  TTredoxPackages = set of TTredoxPackage;

  TModuleIdentifyProc = procedure(out ID: Integer; out Name: string);

  TVersionInfo = record
    Major   : Word;
    Minor   : Word;
    Release : Word;
    Build   : Word;
  end;

  TModuleIdentifier = packed record
    ModuleID: Integer;
    Flags: Integer; { MODULE_xxxx }
  end;

  TFileProperties = record
     FileType       : TTredoxFileType;
     FileSize       : Int64;
     FileDate       : TDateTime;
     FileAtributes  : Dword;
     UpdateMethode  : TTredoxUpdateMethode;
     Version        : TVersionInfo;
     CRCValue       : Dword;
   end;

  TFolderProperties = record
     CreateDate     : TDateTime;
     Atributues     : Dword;
   end;

  TTredoxPackedFileItem = class(TCollectionItem)
  private
    fFileName       : string;
    fFileType       : TTredoxFileType;
    fFileDate       : TDateTime;
    fFileSize       : Int64;
    fFileAttributes : DWORD;
    fInstallDir     : string;
    fUpdateMethode  : TTredoxUpdateMethode;
    fVersion        : TVersionInfo;
    fVersionStr     : string;
    fCRCRequired    : boolean;
    fCheckSum       : DWord;
    procedure SetVersion(const Value: TVersionInfo);
    procedure SetVersionStr(const Value: string);
  protected
  public
  published
    property FileName: string read fFileName write fFilename;
    property FileType: TTredoxFileType read fFileType write fFileType;
    property InstallDir: string read fInstallDir write fInstallDir;
    property UpdateMethode: TTredoxUpdateMethode read fUpdateMethode write fUpdateMethode;
    property FileDate: TDateTime read fFileDate write fFileDate;
    property FileSize: Int64 read fFileSize write fFileSize;
    property CheckSum: Cardinal read fCheckSum write fCheckSum;
    property Version: TVersionInfo read fVersion write SetVersion;
    property VersionStr: string read fVersionStr write SetVersionStr;
    property CRCRequired: Boolean read fCRCRequired write fCRCRequired;
  end;

  TTredoxPackedFileCollection = class(TOwnedCollection)
  private
    function GetItem(Index: Integer): TTredoxPackedFileItem;
    procedure SetItem(Index: Integer; const Value: TTredoxPackedFileItem);
  protected
  public
    constructor Create(aOwner: TPersistent);
    function Add: TTredoxPackedFileItem;
    property Items[Index: Integer]: TTredoxPackedFileItem read GetItem write SetItem; default;
  published
  end;

  { packed file collection }
  TTredoxResourceDescriptorItem = class(TCollectionItem)
  private
    fResName: string;
    fGroupType: TTredoxGroupType;
    fPackedFiles: TTredoxPackedFileCollection;
    procedure SetPackedFiles(const Value: TTredoxPackedFileCollection);
  protected
  public
    constructor Create(aCollection: TCollection); override;
    destructor Destroy; override;
  published
    property ResName: string read fResName write fResName;
    property GroupType: TTredoxGroupType read fGroupType write fGroupType;
    property PackedFiles: TTredoxPackedFileCollection read fPackedFiles write SetPackedFiles;
  end;

  { collection van meegelinkte resources }
  TTredoxResourceDescriptorCollection = class(TOwnedCollection)
  private
    function GetItem(Index: Integer): TTredoxResourceDescriptorItem;
    procedure SetItem(Index: Integer; const Value: TTredoxResourceDescriptorItem);
  protected
  public
    constructor Create(aOwner: TPersistent);
    function Add: TTredoxResourceDescriptorItem;
    property Items[Index: Integer]: TTredoxResourceDescriptorItem read GetItem write SetItem; default;
  published
  end;

  { wrapper voor streaming }
  TTredoxResource = class(TComponent)
  private
    fDescriptor: TTredoxResourceDescriptorCollection;
    procedure SetDescriptor(const Value: TTredoxResourceDescriptorCollection);
  protected
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    class function MyResName: string;
    class function MyResType: string;
    function FindGroupType(aGroupType: TTredoxGroupType): TTredoxResourceDescriptorItem;
  published
    property Descriptor: TTredoxResourceDescriptorCollection read fDescriptor write SetDescriptor;
  end;


function IsTredoxPackageFileName(const aFileName: string): Boolean;
  { checkt masker <aFileName> op PKG*70.bpl }
function GetTredoxPackageType(const aFileName: string): TTredoxPackage;
  { haal packagetype op ahv description van package }
procedure GetTredoxPackages(const aPath: string; aList: TStrings; aFilter: TTredoxPackages);
  { vul stringlist met packages }
function GetPackageVersion(const aFileName: string): TVersionInfo;
  { haal versie op van package }
function GetTredoxPackageDescription(const aFileName: string): string;
  { haal 3e string op van tredox.xxx.xxx}
function Version2Str(major, minor, release, build: word): string; overload;
   { converteer versions naar string: 1.0.1.245 }
function Version2Str(vi: TVersionInfo): string; overload;
   { converteer versions naar string: 1.0.1.245 }
procedure Str2Version(const S: string; var major, minor, release, build: word); overload;
   { converteer string naar versions }
function Str2Version(const S: string): TVersionInfo; overload;
   { converteer string naar versions }
function ValidVersionStr(const S: string): Boolean;

function Str2UpdateMethode(S: string): TTredoxUpdateMethode;
   { converteer string naar UpdateMethode }

function Str2FileType(s: string):TTredoxFileType;

function FileType2Str(ft: TTredoxFileType):string;
function UpdateMethode2Str(um: TTredoxUpdateMethode):string;
function Str2GroupType(S: string): TTredoxGroupType;
function Str2GroupKind(S: string): TTredoxGroupKind;
function GroupKind2Str(gk: TTredoxGroupKind):string;
function GroupType2Str(gt: TTredoxGroupType):string;

function GetFileCRC(const aFileName: string): DWord;
function GetFileDate(const aFileName: string):TDateTime;
function GetFileSize(const aFileName: string):Int64;
function GetAttributes(const aFileName: string):DWord;


implementation

function Str2Version(const S: string): TVersionInfo; overload;
begin
  result.major   := s2i(SplitString(S, 0, '.'));
  result.minor   := s2i(SplitString(S, 1, '.'));
  result.release := s2i(SplitString(S, 2, '.'));
  result.build   := s2i(SplitString(S, 3, '.'));
end;

function Version2Str(vi: TVersionInfo): string; overload;
   { converteer versions naar string: 1.0.1.245 }
begin
   result:=i2s(vi.Major)+'.'+
           i2s(vi.Minor)+'.'+
           i2s(vi.Release)+'.'+
           i2s(vi.Build);
end;

function Str2UpdateMethode(S: string): TTredoxUpdateMethode;
var
  U: Integer;
begin
  s:=StringReplace(s,' ','_',[rfReplaceAll]);
  s:='um'+s;
  U := getEnumValue(TypeInfo(TTredoxUpdateMethode),s);
//  windlg(['getupdate', s, u]);
  if U < 0 then
    Result := umAlways_update_files
  else
  result:=TTredoxUpdateMethode(U);
//  if ord(result)>ord(high(TTredoxUpdateMethode)) then result:=umAlways_update_files;
//  windlg('s2um '+inttostr(ord(result))+' '+s);
end;

function Str2GroupType(S: string): TTredoxGroupType;
var
  G: Integer;
begin
  G := getEnumValue(TypeInfo(TTredoxGroupType),s);
  if G < 0 then
    Result := gtNone
  else
  result:=TTredoxGroupType(G);
end;

function Str2GroupKind(S: string): TTredoxGroupKind;
var
  K: Integer;
begin
  K := getEnumValue(TypeInfo(TTredoxGroupKind),s);
  if K < 0 then
    Result := gkNone
  else
  result:=TTredoxGroupKind(K);
end;

function Str2FileType(s: string):TTredoxFileType;
var t: integer;
begin
  s:=StringReplace(s,' ','_',[rfReplaceAll]);
  s:='tf'+s;
  t:=getEnumValue(TypeInfo(TTredoxFileType),s);
  if T<0 then
    result := tfNone
  else
  result:=TTredoxFileType(t);
end;

function GroupKind2Str(gk: TTredoxGroupKind):string;
begin
  result:=GetEnumName(TypeInfo(TTredoxGroupKind),ord(gk));
//  result:=cutleft(result,2);
//  result:=StringReplace(result,'_',' ',[rfReplaceAll]);
end;

function GroupType2Str(gt: TTredoxGroupType):string;
begin
  result:=GetEnumName(TypeInfo(TTredoxGroupType),ord(gt));
//  result:=cutleft(result,2);
//  result:=StringReplace(result,'_',' ',[rfReplaceAll]);
end;

function FileType2Str(ft: TTredoxFileType):string;
begin
  result:=GetEnumName(TypeInfo(TTredoxFileType),ord(ft));
  result:=cutleft(result,2);
  result:=StringReplace(result,'_',' ',[rfReplaceAll]);
//  windlg('ft2s '+inttostr(ord(ft))+' '+result);
end;

function UpdateMethode2Str(um: TTredoxUpdateMethode):string;
begin
  result:=GetEnumName(TypeInfo(TTredoxUpdateMethode),ord(um));
  result:=cutleft(result,2);
  result:=StringReplace(result,'_',' ',[rfReplaceAll]);
//  windlg('um2s '+inttostr(ord(um))+' '+result);
end;

function GetFileCRC(const aFileName: string): DWORD;
var
  Rd: Integer;
  Buf: array[0..1023] of DWORD;
  P: Pointer;
  i: Integer;
  MaxSize, TotalRead, Max: Integer;

const
  BufSize = SizeOf(Buf);
begin
  Result := 0;//StartValue;
  with TFileStream.Create(aFileName, fmOpenRead) do
  try
    MaxSize := Size;
    TotalRead := 0;

    repeat
      FillChar(Buf, BufSize, 0);
      Rd := Read(Buf, BufSize);
      Inc(TotalRead, Rd);

      if Rd > 0 then
      begin
        P := @Buf;
        Max := Rd div 4;
        i := 0;
        repeat
          if i >= Max then
            Break;
          Result := Result xor Buf[i];
          Inc(i);
        until False;
      end;

      if TotalRead >= MaxSize then
        Break;
    until False;

  finally
    Free;
  end;
end;

{procedure GetFileInfo(const aFileName);
begin

end;}

function GetFileDate(const aFileName: string):TDateTime;
var
  Age: Integer;
  S: TSearchRec;
  SysTime : _SYSTEMTIME;
//  lw:tfiletime;
begin
  Result := 0;
//  if not fileexists(afilename) then windlg('bliep');
  if FindFirst(aFileName, faAllFiles, S) = 0 then
  begin
//    FileDateToDateTime(S.FindData.ftLastWriteTime);
    FileTimeToSystemTime(S.FindData.ftLastWriteTime, SysTime);
    Result := SystemTimeToDateTime(SysTime);
  end;
  FindClose(S);
  Exit;


//  GetFileTime(InputStream.Handle, @CreationTime, @LastAccessTime, @LastWriteTime);


//   if not FileExists(aFileName) then exit;
  if not FileExists(aFileName) then raise exception.Create(aFileName+ ' not found');
  Age := FileAge(aFileName);
  if Age < 0 then raise Exception.Create(aFileName+ ' age error');
  result:=FileDateToDateTime(Age);
end;

function GetFileSize(const aFileName: string):Int64;
var fs:TFileStream;
begin
  result:=0;
//  if not FileExists(aFileName) then exit;
  fs:=TFileStream.Create(aFileName, fmOpenRead);
  result:=fs.Size;
  fs.Free;
end;

function GetAttributes(const aFileName: string):DWord;
begin
  result:=0;
//  if not FileExists(aFileName) then exit;
  result:=GetFileAttributes(Pchar(aFileName));
end;

{ TTredoxPackedFileCollection }


function TTredoxPackedFileCollection.Add: TTredoxPackedFileItem;
begin
  Result := TTredoxPackedFileItem(inherited Add);
end;

constructor TTredoxPackedFileCollection.Create(aOwner: TPersistent);
begin
  inherited Create(aOwner, TTredoxPackedFileItem);
end;

function TTredoxPackedFileCollection.GetItem(Index: Integer): TTredoxPackedFileItem;
begin
  Result := TTredoxPackedFileItem(inherited GetItem(Index))
end;

procedure TTredoxPackedFileCollection.SetItem(Index: Integer; const Value: TTredoxPackedFileItem);
begin
  inherited SetItem(Index, Value);
end;

{ TTredoxResourceDescriptorItem }

constructor TTredoxResourceDescriptorItem.Create(aCollection: TCollection);
begin
  inherited;
  fPackedFiles := TTredoxPackedFileCollection.Create(Self);
end;

destructor TTredoxResourceDescriptorItem.Destroy;
begin
  fPackedFiles.Free;
  inherited;
end;


procedure TTredoxResourceDescriptorItem.SetPackedFiles(const Value: TTredoxPackedFileCollection);
begin
  fPackedFiles.Assign(Value);
end;

{ TTredoxResourceDescriptorCollection }

function TTredoxResourceDescriptorCollection.Add: TTredoxResourceDescriptorItem;
begin
  Result := TTredoxResourceDescriptorItem(inherited Add);
end;

constructor TTredoxResourceDescriptorCollection.Create(aOwner: TPersistent);
begin
  inherited Create(aOwner, TTredoxResourceDescriptorItem);
end;

function TTredoxResourceDescriptorCollection.GetItem(Index: Integer): TTredoxResourceDescriptorItem;
begin
  Result := TTredoxResourceDescriptorItem(inherited GetItem(Index));
end;

procedure TTredoxResourceDescriptorCollection.SetItem(Index: Integer; const Value: TTredoxResourceDescriptorItem);
begin
  inherited SetItem(Index, Value);
end;

{ TTredoxResource }

constructor TTredoxResource.Create(aOwner: TComponent);
begin
  inherited;
  fDescriptor := TTredoxResourceDescriptorCollection.Create(Self);
end;

destructor TTredoxResource.Destroy;
begin
  fDescriptor.Free;
  inherited;
end;

function TTredoxResource.FindGroupType(aGroupType: TTredoxGroupType): TTredoxResourceDescriptorItem;
var
  i: Integer;
begin
  with Descriptor do
   for i := 0 to Count - 1 do
     if Items[i].GroupType = aGroupType then
     begin
       Result := Items[i];
       Exit;
     end;
  Result := nil;
end;

class function TTredoxResource.MyResName: string;
begin
  Result := 'TREDOXRESOURCEDESCRIPTOR'
end;

class function TTredoxResource.MyResType: string;
begin
  Result := 'DESCRIPTOR';
end;

procedure TTredoxResource.SetDescriptor(const Value: TTredoxResourceDescriptorCollection);
begin
  fDescriptor.Assign(Value);
end;



function IsTredoxPackageFileName(const aFileName: string): Boolean;
var
  F: string;
begin
  F := ReplaceFileExt(ExtractFileName(aFileName), '');
  Result := (CompareText(ExtractFileExt(aFileName), '.BPL') = 0)
            and (Length(F) >= 8)
            and (CompareText(LStr(F, 3), 'PKG') = 0)
            and (CompareText(RStr(F, 2), '70') = 0);
end;

function GetTredoxPackageType(const aFileName: string): TTredoxPackage;
var
  Descr: string;
begin
  Result := tpUnknown;
  if not IsTredoxPackageFileName(aFileName) then
    Exit;
  Descr := GetPackageDescription(PChar(aFileName));    //umisc
  {-------------------------------------------------------------------------------
    check "tredox"
  -------------------------------------------------------------------------------}
  if CompareText(SplitString(Descr, 0, '.', ), STredox) <> 0 then
    Exit;
  {-------------------------------------------------------------------------------
    check omschrijving
  -------------------------------------------------------------------------------}
  if SplitString(Descr, 2, '.', ) = '' then
    Exit;
  {-------------------------------------------------------------------------------
    check "module" of "framework"
  -------------------------------------------------------------------------------}
  if CompareText(SplitString(Descr, 1, '.', ), SModule) = 0 then
    Result := tpModule
  else if CompareText(SplitString(Descr, 1, '.', ), SFramework) = 0 then
    Result := tpFramework;

end;

procedure GetTredoxPackages(const aPath: string; aList: TStrings; aFilter: TTredoxPackages);
var
  L: TStringList;
  Mask: string;
  i: Integer;
begin
  if aList = nil then
    Exit;
  aList.Clear;
  Exclude(aFilter, tpUnknown);
  if aFilter = [] then
    Exit;
  L := TStringList.Create;
  try
    Mask := IncludeTrailingBackslash(aPath) + STredoxPackageMask;
    CreateFileList(L, Mask, faAllFiles and not faDirectory);
    for i := 0 to L.Count - 1 do
      if GetTredoxPackageType(L[i]) in aFilter then
        aList.Add(L[i]);
  finally
    L.Free;
  end;
end;

function GetPackageVersion(const aFileName: string): TVersionInfo;
begin
  with Result do
    GetBuildInfo(Major, Minor, Release, Build, aFileName);
end;

function GetTredoxPackageDescription(const aFileName: string): string;
var
  Descr: string;
begin
  // TREDOX.NNNNNN.DESCRIPTION
  Result := '';
  if not IsTredoxPackageFileName(aFileName) then
    Exit;
  Descr := GetPackageDescription(PChar(aFileName));
  {-------------------------------------------------------------------------------
    check "tredox"
  -------------------------------------------------------------------------------}
  if CompareText(SplitString(Descr, 0, '.', ), STredox) <> 0 then
    Exit;
  {-------------------------------------------------------------------------------
    check "module" of "framework"
  -------------------------------------------------------------------------------}
(*  if CompareText(SplitString(Descr, 1, '.', ), SModule) = 0 then
    Result := tpModule
  else if CompareText(SplitString(Descr, 1, '.', ), SFramework) = 0 then
    Result := tpFramework; *)
  {-------------------------------------------------------------------------------
    check omschrijving
  -------------------------------------------------------------------------------}
  Result := SplitString(Descr, 2, '.', );
end;

function Version2Str(major, minor, release, build: word): string;
begin
  result := Format('%d.%d.%d.%d', [major, minor, release, build]);
//  i2s(release)+'.'+IntToStr(major)+'.'+IntToStr(minor)+'.'+IntToStr(build);
end;

procedure Str2Version(const S: string; var major, minor, release, build: word);
begin
  major   := s2i(SplitString(S, 0, '.'));
  minor   := s2i(SplitString(S, 1, '.'));
  release := s2i(SplitString(S, 2, '.'));
  build   := s2i(SplitString(S, 3, '.'));
end;

function ValidVersionStr(const S: string): Boolean;
var
  L: TStringList;
  i: Integer;
begin
  Result := False;
  L := TStringList.Create;
  try
    SplitString_To_StringList(S, L, '.');
    if L.Count <> 4 then
      Exit;
    for i := 0 to 3 do
    begin
      if not StrAllInSet(L[i], DigitCharSet) then
        Exit;
    end;
    Result := True;
  finally
    L.Free;
  end;
end;

{ TTredoxPackedFileItem }

procedure TTredoxPackedFileItem.SetVersion(const Value: TVersionInfo);
begin
  fVersion := Value;
  with fVersion do
  fVersionStr := Version2Str(major,minor,release,build);
end;

procedure TTredoxPackedFileItem.SetVersionStr(const Value: string);
begin
  fVersionStr := Value;
  fVersion:=Str2Version(fVersionStr);
end;

end.
//dateutils
