unit UMxEditors;

interface

uses
  Windows, Messages, Classes, Controls, StdCtrls, Forms, Dialogs, Graphics,
  UMisc, UMxMisc, UMx;

type
{-------------------------------------------------------------------------------
  String
-------------------------------------------------------------------------------}

  TStringEdit = class(TCustomEdit)
  private
    fMatrix: TBaseMatrix;
  protected
    procedure WMKillFocus(var Msg: TWMKillFocus); message WM_KILLFOCUS;
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TMemoEdit = class(TCustomMemo)
  private
    fMatrix: TBaseMatrix;
  protected
    procedure KeyDown(var Key: word; Shift: TShiftState); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TStringEditor = class(TInterfacedObject, IMatrixEditor)
  private
  protected
    fMatrix: TBaseMatrix;
    fEdit: TStringEdit;
    fMemo: TMemoEdit;
    fCurrentEdit: TCustomEdit;
    fMultiLine: Boolean;
    
  public
    constructor Create;
    destructor Destroy; override;
  { IMatrixEditor }
    function PrepareEdit(aMatrix: TBaseMatrix; aCol, aRow: Integer): Boolean; virtual;
    function BeginEdit: Boolean; virtual;
    function EndEdit: Boolean; virtual;
    function CancelEdit: Boolean; virtual;
    function GetBounds: TRect; virtual;
    procedure SetBounds(R: TRect); virtual;
  end;

implementation

type
  THack = class(TBaseMatrix);

{ TStringEdit }

constructor TStringEdit.Create(aOwner: TComponent);
begin
  inherited;
//  color := clyellow;
  Visible := False;
  BorderStyle := bsNone;
  AutoSize := False;
  AutoSelect := False;
end;

destructor TStringEdit.Destroy;
begin
  inherited;
end;

procedure TStringEdit.WMKillFocus(var Msg: TWMKillFocus);
{-------------------------------------------------------------------------------
  Als de killfocus door een andere control dan matrix is getriggerd, moet
  de matrix niet herfocussen
-------------------------------------------------------------------------------}
begin
  inherited;
  { TODO : BUG OPLOSSEN: als klik buiten cellen, terwijl editor actief dan crash }
  if Msg.FocusedWnd <> fMatrix.Handle then
    if not fMatrix.InStates([isEditEnding, isEditCanceling]) then
      fMatrix.EditExit;

//    if Msg.FocusedWnd = fMatrix.Handle then
  //  Log(['matrix focus']);
{  if not fMatrix.InStates([isEditEnding, isEditCanceling]) then
    fMatrix.EditEnd; }
end;

procedure TStringEdit.WMKeyDown(var Message: TWMKeyDown);
{-------------------------------------------------------------------------------
  Belangrijk bij editor interfaces is dat er met een postmessage gewerkt moet
  worden, wanneer een toets (of muis) doorgestuurd moet worden.

  Verder: wanneer als gevolg van een gebeurtenis in de editor de editor zelf
  vernietigd moet worden, moet er altijd met een message gewerkt worden.
  Zie ook EditEnd, EditCancel van matrix.

  De editor wordt namelijk vernietigd bij cell-change: als we dan nog in deze
  procedure zitten, krijgen we een AV. De procedure moet dus verlaten worden
  en daarna komt de message aan bij matrix.
  Misschien kan het veiliger opgelost.
-------------------------------------------------------------------------------}
begin
  inherited;
  case Message.CharCode of
    VK_RETURN:
        fMatrix.EditEnd;
    VK_ESCAPE:
      fMatrix.EditCancel;
    VK_UP, VK_DOWN, VK_NEXT, VK_PRIOR:
      begin
        fMatrix.DoPostMessage(TMessage(Message));
      end;
  end;
end;

{ TMemoEdit }

constructor TMemoEdit.Create(aOwner: TComponent);
begin
  inherited;
  Visible := False;
  BorderStyle := bsNone;
  AutoSize := False;
end;

destructor TMemoEdit.Destroy;
begin
  inherited;
end;

procedure TMemoEdit.KeyDown(var Key: word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_ESCAPE: fMatrix.EditCancel;
    VK_RETURN: if Shift = [ssCtrl] then fMatrix.EditEnd;
  end;
end;


{ TStringEditor }

function TStringEditor.BeginEdit: Boolean;
begin
  Result := True;
  fCurrentEdit.Show;
  fCurrentEdit.SetFocus;
end;

function TStringEditor.CancelEdit: Boolean;
begin
  fCurrentEdit.Hide;
  Result := True;
end;

constructor TStringEditor.Create;
begin
  inherited;
  fEdit := TStringEdit.Create(nil);
  fMemo := TMemoEdit.Create(nil);
end;

destructor TStringEditor.Destroy;
begin
  fEdit.Free;
  fMemo.Free;
  inherited;
end;

function TStringEditor.EndEdit: Boolean;
var
  Data: TCellData;
begin
  Result := True;
  fCurrentEdit.Hide;
  Data := TCellData.Create;
  try
    Data.Text := fCurrentEdit.Text;
    with THack(fMatrix) do
      SetData(EditCol, EditRow, Data);
  finally
    Data.Free;
  end;
end;

function TStringEditor.GetBounds: TRect;
begin
  Result := fCurrentEdit.BoundsRect;
end;

function TStringEditor.PrepareEdit(aMatrix: TBaseMatrix; aCol, aRow: Integer): Boolean;
var
  Data: TCellData;
begin
  fMatrix := aMatrix;

  fEdit.fMatrix := aMatrix;
  fMemo.fMatrix := aMatrix;

//  fCurrentEdit := fMemo;  mx
  fCurrentEdit := fEdit;

  //fCurrentEdit.Color := clred;
  fCurrentEdit.Parent := aMatrix;
  Result := True;
  fMatrix.GetData(aCol, aRow, Data);
  if Data <> nil then
    fCurrentEdit.Text := Data.Text;
end;

procedure TStringEditor.SetBounds(R: TRect);
begin
  fCurrentEdit.SetBounds(R.Left, R.Top, RectWidth(R), RectHeight(R));
end;

end.

