unit UCollection;

interface

uses
  Windows, Classes, Controls, Graphics, Dialogs, SysUtils, ToolsApi,
  IOTAFunctions;

type
  TListType = (
    ltTCollection,
    ltTOwnedCollection,
    ltTList,
    ltTObjectList
  );

  TCollectionCreateMode = (
    ccmNone,
    ccmInterface,
    ccmImplementation
  );


procedure InsertCode(const aCollectionItemName: string;
  aListType: TListType; aMode: TCollectionCreateMode);

implementation

const
  CrLf = Chr(13) + Chr(10);

var
  // TCollection
  SCollectionInterface: string =
    CrLf +
    'type ' + CrLf +
    '  T_@0_ = class(TCollectionItem) ' + CrLf +
    '  private ' + CrLf +
    '  protected ' + CrLf +
    '  public ' + CrLf +
    '  published ' + CrLf +
    '  end; ' + CrLf +
    '' + CrLf +
    '  T_@0_Collection = class(TCollection) ' + CrLf +
    '  private ' + CrLf +
    '    function GetItem(Index: Integer): T_@0_; ' + CrLf +
    '    procedure SetItem(Index: Integer; const Value: T_@0_); ' + CrLf +
    '  protected ' + CrLf +
    '  public ' + CrLf +
    '    constructor Create; ' + CrLf +
    '    function Add: T_@0_; ' + CrLf +
    '    function Insert(Index: Integer): T_@0_; ' + CrLf +
    '    property Items[Index: Integer]: T_@0_ read GetItem write SetItem; default; ' + CrLf +
    '  published ' + CrLf +
    '  end; ' + CrLf;

  SCollectionImplementation: string =
    CrLf +
    '{ T_@0_Collection } ' + CrLf +
    '' + CrLf +
    'function T_@0_Collection.Add: T_@0_; ' + CrLf +
    'begin ' + CrLf +
    '  Result := T_@0_(inherited Add); ' + CrLf +
    'end; ' + CrLf +
    '' + CrLf +
    'constructor T_@0_Collection.Create; ' + CrLf +
    'begin ' + CrLf +
    '  inherited Create(T_@0_); ' + CrLf +
    'end; ' + CrLf +
    '' + CrLf +
    'function T_@0_Collection.GetItem(Index: Integer): T_@0_; ' + CrLf +
    'begin ' + CrLf +
    '  Result := T_@0_(inherited GetItem(Index)) ' + CrLf +
    'end; ' + CrLf +
    '' + CrLf +
    'function T_@0_Collection.Insert(Index: Integer): T_@0_; ' + CrLf +
    'begin ' + CrLf +
    '  Result := T_@0_(inherited Insert(Index)) ' + CrLf +
    'end; ' + CrLf +
    '' + CrLf +
    'procedure T_@0_Collection.SetItem(Index: Integer; const Value: T_@0_); ' + CrLf +
    'begin ' + CrLf +
    '  inherited SetItem(Index, Value); ' + CrLf +
    'end; ' + CrLf;

  // TOwnedCollection
  SOwnedCollectionInterface: string =
    CrLf +
    'type ' + CrLf +
    '  T_@1_ = class(TCollectionItem) ' + CrLf +
    '  private ' + CrLf +
    '  protected ' + CrLf +
    '  public ' + CrLf +
    '  published ' + CrLf +
    '  end; ' + CrLf +
    '' + CrLf +
    '  T_@1_Collection = class(TOwnedCollection) ' + CrLf +
    '  private ' + CrLf +
    '    function GetItem(Index: Integer): T_@1_; ' + CrLf +
    '    procedure SetItem(Index: Integer; const Value: T_@1_); ' + CrLf +
    '  protected ' + CrLf +
    '  public ' + CrLf +
    '    constructor Create(aOwner: TPersistent); ' + CrLf +
    '    function Add: T_@1_; ' + CrLf +
    '    function Insert(Index: Integer): T_@1_; ' + CrLf +
    '    property Items[Index: Integer]: T_@1_ read GetItem write SetItem; default; ' + CrLf +
    '  published ' + CrLf +
    '  end; ' + CrLf;

  SOwnedCollectionImplementation: string =
    CrLf +
    '{ T_@1_Collection } ' + CrLf +
    '' + CrLf +
    'function T_@1_Collection.Add: T_@1_; ' + CrLf +
    'begin ' + CrLf +
    '  Result := T_@1_(inherited Add); ' + CrLf +
    'end; ' + CrLf +
    '' + CrLf +
    'constructor T_@1_Collection.Create(aOwner: TPersistent); ' + CrLf +
    'begin ' + CrLf +
    '  inherited Create(aOwner, T_@1_); ' + CrLf +
    'end; ' + CrLf +
    '' + CrLf +
    'function T_@1_Collection.GetItem(Index: Integer): T_@1_; ' + CrLf +
    'begin ' + CrLf +
    '  Result := T_@1_(inherited GetItem(Index)) ' + CrLf +
    'end; ' + CrLf +
    '' + CrLf +
    'function T_@1_Collection.Insert(Index: Integer): T_@1_; ' + CrLf +
    'begin ' + CrLf +
    '  Result := T_@1_(inherited Insert(Index)) ' + CrLf +
    'end; ' + CrLf +
    '' + CrLf +
    'procedure T_@1_Collection.SetItem(Index: Integer; const Value: T_@1_); ' + CrLf +
    'begin ' + CrLf +
    '  inherited SetItem(Index, Value); ' + CrLf +
    'end; ' + CrLf;

  // TList
  SListInterface: string =
    '' + CrLf +
    'type' + CrLf +
    '  T_@2_ = class' + CrLf +
    '  private' + CrLf +
    '  protected' + CrLf +
    '  public' + CrLf +
    '  published' + CrLf +
    '  end;' + CrLf +
    '' + CrLf +
    '  T_@2_List = class(TList)' + CrLf +
    '  private' + CrLf +
    '    function GetItem(Index: Integer): T_@2_;' + CrLf +
    '  protected' + CrLf +
    '  public' + CrLf +
    '    function Add(Item: T_@2_): Integer;' + CrLf +
    '    procedure Insert(Index: Integer; Item: T_@2_);' + CrLf +
    '    property Items[Index: Integer]: T_@2_ read GetItem; default;' + CrLf +
    '  published' + CrLf +
    '  end;';

  SListImplementation: string =
    '' + CrLf +
    '{ T_@2_List }' + CrLf +
    '' + CrLf +
    'function T_@2_List.Add(Item: T_@2_): Integer;' + CrLf +
    'begin' + CrLf +
    '  Result := inherited Add(Item);' + CrLf +
    'end;' + CrLf +
    '' + CrLf +
    'function T_@2_List.GetItem(Index: Integer): T_@2_;' + CrLf +
    'begin' + CrLf +
    '  Result := inherited Get(Index);' + CrLf +
    'end;' + CrLf +
    '' + CrLf +
    'procedure T_@2_List.Insert(Index: Integer; Item: T_@2_);' + CrLf +
    'begin' + CrLf +
    '  inherited Insert(Index, Item);' + CrLf +
    'end;';

  // TObjectList
  SObjectListInterface: string =
    '' + CrLf +
    'type' + CrLf +
    '  T_@3_ = class' + CrLf +
    '  private' + CrLf +
    '  protected' + CrLf +
    '  public' + CrLf +
    '  published' + CrLf +
    '  end;' + CrLf +
    '' + CrLf +
    '  T_@3_List = class(TObjectList)' + CrLf +
    '  private' + CrLf +
    '    function GetItem(Index: Integer): T_@3_;' + CrLf +
    '  protected' + CrLf +
    '  public' + CrLf +
    '    function Add(Item: T_@3_): Integer;' + CrLf +
    '    procedure Insert(Index: Integer; Item: T_@3_);' + CrLf +
    '    property Items[Index: Integer]: T_@3_ read GetItem; default;' + CrLf +
    '  published' + CrLf +
    '  end;';

  SObjectListImplementation: string =
    '' + CrLf +
    '{ T_@3_List }' + CrLf +
    '' + CrLf +
    'function T_@3_List.Add(Item: T_@3_): Integer;' + CrLf +
    'begin' + CrLf +
    '  Result := inherited Add(Item);' + CrLf +
    'end;' + CrLf +
    '' + CrLf +
    'function T_@3_List.GetItem(Index: Integer): T_@3_;' + CrLf +
    'begin' + CrLf +
    '  Result := inherited Get(Index);' + CrLf +
    'end;' + CrLf +
    '' + CrLf +
    'procedure T_@3_List.Insert(Index: Integer; Item: T_@3_);' + CrLf +
    'begin' + CrLf +
    '  inherited Insert(Index, Item);' + CrLf +
    'end;';

const
  ReplaceStrings: array[TListType] of string = (
    '_@0_',
    '_@1_',
    '_@2_',
    '_@3_'
  );

  InterfaceStrings: array[TListType] of PString = (
    @SCollectionInterface,
    @SOwnedCollectionInterface,
    @SListInterface,
    @SObjectListInterface
  );

  ImplementationStrings: array[TListType] of PString = (
    @SCollectionImplementation,
    @SOwnedCollectionImplementation,
    @SListImplementation,
    @SObjectListImplementation
  );

procedure StringToFile(const aString, aFileName: string);
var
  L: TStringList;
begin
  L := TStringList.Create;
  try
    L.Add(aString);
    L.SaveToFile(aFileName);
  finally
    L.Free;
  end;
end;

procedure InsertStringIntoEditor(const S: string);
var
  EditorServices: IOTAEditorServices;
  CurrentView: IOTAEditView;
  SourceEditor: IOTASourceEditor;
  OldCol, OldRow, EOL: Integer;
  aFileName: string;
begin

  EditorServices := BorEditorServices;

  if EditorServices = nil then
  begin
    ShowMessage('Kan Borland Editor Service niet vinden');
    Exit;
  end;

  CurrentView := EditorServices.TopView;

  if CurrentView = nil then
  begin
    ShowMessage('Geen actieve editor aanwezig');
    Exit;
  end;

  aFileName := GetCurrentDir + '$$$CWZD.TMP';

  StringToFile(S, aFileName);

  with CurrentView.Position do
  begin
    MoveBOL;
    InsertFile(aFileName);
  end;

  CurrentView.Paint;

  DeleteFile(aFileName);

end;

procedure InsertCode(const aCollectionItemName: string;
  aListType: TListType; aMode: TCollectionCreateMode);
var
  S: string;
  aName: string;
begin
  if aCollectionItemName = '' then
    Exit;

  // parse name
  aName := aCollectionItemName;

(*  if UpCase(aName[1]) <> 'T' then
    aName := 'T' + aName;
  aName[1] := Upcase(aName[1]);

  if Length(aName) > 1 then
    aName[2] := UpCase(aName[2]); *)

  aName[1] := UpCase(aName[1]);  

  // replace const strings
{  case aMode of
    ccmInterface:
      S := StringReplace(SCollectionInterface, 'T_MyItem_', aName, [rfReplaceAll, rfIgnoreCase]);
    ccmImplementation:
      S := StringReplace(SCollectionImplementation, 'T_MyItem_', aName, [rfReplaceAll, rfIgnoreCase]);
  end; }

  case aMode of
    ccmInterface:
      S := StringReplace(InterfaceStrings[aListType]^,
                         ReplaceStrings[aListType],
                         aName, [rfReplaceAll, rfIgnoreCase]);
    ccmImplementation:
      S := StringReplace(ImplementationStrings[aListType]^,
                         ReplaceStrings[aListType],
                         aName, [rfReplaceAll, rfIgnoreCase]);
  end;

  InsertStringIntoEditor(S);
end;


end.





end.

