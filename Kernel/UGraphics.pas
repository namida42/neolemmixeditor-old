unit UGraphics;

interface

uses
  Windows, Sysutils, Graphics,
  UMisc, ULog, UFastStrings, jpeg;

function RGBToColor(R, G, B: byte): TColor;
function RedValue(C: TColor): byte;
function GreenValue(C: TColor): byte;
function BlueValue(C: TColor): byte;
function DelphiColorToWebColor(const AColor: TColor): string;
function WebColorToDelphiColor(const AWebColor: string): TColor;

//procedure DrawColoredTextEx(Canvas: TCanvas; const Text: string; R: TRect; WinFlags: DWORD; Params: PDrawTextParams;
  //const HighLight: string; const HC, BC: TColor);

//procedure ExtTextOutColored(Canvas: TCanvas; const Text: string; R: TRect; WinFlags: DWORD; const HighLightStr: string;
  //const HC, BC: TColor; FontStyle: TFontStyles = []);


(*procedure ExtTextOutColored7(Canvas: TCanvas;
                            const Text: string;
                            R: TRect;
                            WinFlags: DWORD;
                            hs1,hs2,hs3,hs4: string;
                            const HC, BC: TColor;
                            FontStyle: TFontStyles = []); *)

procedure ExtTextOutColored(Canvas: TCanvas;
                            const Text: string;
                            R: TRect;
                            WinFlags: DWORD;
                            const HighLightStrings: array of string;
                            const TextColors: array of TColor;
                            const BackColors: array of TColor;
                            const FontStyles: array of TFontStyles);


  {procedure ExtTextOutColored(Canvas: TCanvas;
                            const Text: string;
                            R: TRect;
                            WinFlags: DWORD;
                            const HighLightStr: string;
                            const HC, BC: TColor;
                            FontStyle: TFontStyles = []); }

type
  TColorArray = array of TColor;

function ColorArray(const Values: array of TColor): TColorArray;
  { creeer een TColorArray }
procedure DrawGradient(ACanvas: TCanvas; Rect: TRect; Horicontal: Boolean;
  const Colors: TColorArray);

type
  TColorRec = record
    Position: integer;
    Color: TColor;
  end;

procedure MergeBitmaps(Bitmap1, Bitmap2, BitmapOutput : TBitmap; Percentage : Integer);


implementation


procedure MergeBitmaps(Bitmap1, Bitmap2, BitmapOutput : TBitmap; Percentage : Integer);
var
  BlendFunction: TBlendFunction;
  Alpha: byte;
begin
  Assert(Between(Percentage, 0, 100), 'Merge bitmaps Percentage fout');
  Assert(Assigned(BitmapOutput), ('BitmapOutput is niet aangemaakt'));
  Alpha := 255 * Percentage div 100;
  BitmapOutput.Assign(Bitmap2);
  BlendFunction.BlendOp := AC_SRC_OVER;
  BlendFunction.BlendFlags := 0;
  BlendFunction.SourceConstantAlpha := Alpha;
  BlendFunction.AlphaFormat := 0;
  Windows.AlphaBlend(BitmapOutput.Canvas.Handle, 0, 0,
                     BitmapOutput.Width,
                     BitmapOutput.Height,
                     Bitmap1.Canvas.Handle, 0, 0,
                     Bitmap1.Width,
                     Bitmap1.Height,
                     BlendFunction);
end;

  (*
  Bitmap := TBitmap.Create;
  try
    ImageAthena.Picture.Bitmap.PixelFormat := pf24bit;
    ImageChip.Picture.Bitmap.PixelFormat := pf24bit;
    Bitmap.PixelFormat := pf24bit;
    try
      MergeBitmaps(ImageAthena.Picture.Bitmap, ImageChip.Picture.Bitmap, Bitmap, NumEditPercentage.Value);
      ImageResult.Picture.Bitmap.Assign(Bitmap);
    except
      ShowMessage('Ooh oow, die ging niet goed :  )');
    end;
  finally
    Bitmap.Free;
  end;
  *)

{ interne globalen voor performance geheugenallocatie van rgbarray }
type
  TRGB = array[0..2] of Byte;
  TRGBArray = array of TRGB;
var
  RGBLen: integer;
  RGBArray: TRGBArray;

function RGBToColor(R, G, B: byte): TColor;
begin
  Result := RGB(R, G, B); EXIT;
  Result := DWORD(R) or (DWORD(G) shl 8) or (DWORD(B) shl 16);
end;

function RedValue(C: TColor): byte;
begin
  Result := GetRValue(C); EXIT;
  Result := C and $000000FF;
end;

function GreenValue(C: TColor): byte;
begin
  Result := GetGValue(C); EXIT;
  Result := (C and $0000FF00) shr 8;
end;

function BlueValue(C: TColor): byte;
begin
  Result := GetBValue(C); EXIT;
  Result := (C and $00FF0000) shr 16; //zie windows getbvalue
end;

{**
 * Converts a Delphi TColor to a HTML4 compatible sRGB color code.
 * e.g. clBlue will be converted to #0000FF
 * @param AColor A Delphi TColor to convert
 * @returns A HTML4 compatible Hex RGB code (with '#' prefix)
 *}
function DelphiColorToWebColor(const AColor: TColor): string;
var
  lRed : string;
  lGreen : string;
  lBlue : string;
  lRGB : string;
  i : Integer;
  lRGBLength : Integer;
begin
  lRGB := IntToHex(ColorToRGB(AColor), 0);
  lRGBLength := Length(lRGB);

  // Fill with zero's to the length of 6
  for i := 1 to (6 - lRGBLength) do
    lRGB := '0' + lRGB;

  lRed := Copy(lRGB, 5, 2);
  lGreen := Copy(lRGB, 3, 2);
  lBlue := Copy(lRGB, 1, 2);
  Result := '#' + lRed + lGreen + lBlue;
end;

{**
 * Converts a HTML4 compatible sRGB color code to a Delphi TColor
 * e.g. #0000FF (Blue) will be converted to $00FF0000
 * @param AWebColor A HTML4 compatible Hex RGB color code (with '#' prefix)
 * @returns A Delphi TColor to convert
 *}
function WebColorToDelphiColor(const AWebColor: string): TColor;
var
  lRed : Integer;
  lGreen : Integer;
  lBlue : Integer;
begin
  Result := clNone;

  if (Length(AWebColor) = 7) and (AWebColor[1] = '#') then
  begin
    lRed := StrToInt('$' + Copy(AWebColor, 2, 2));
    lGreen := StrToInt('$' + Copy(AWebColor, 4, 2));
    lBlue := StrToInt('$' + Copy(AWebColor, 6, 2));
    Result := TColor((lBlue * 65536) + (lGreen * 256) + lRed);
  end;
end;

function ColorArray(const Values: array of TColor): TColorArray;
var
  L, i: integer;
begin
  L := Length(Values);
  SetLength(Result, L);
  for i := 0 to L - 1 do Result[i] := Values[i];
end;


procedure AllocRGB(ALen: integer);
begin
  if ALen > RGBLen then
  begin
    RGBLen := ALen;
    SetLength(RGBArray, ALen);
  end;
end;

procedure DrawGradient(ACanvas: TCanvas; Rect: TRect;
  Horicontal: Boolean; const Colors: TColorArray);
var
  x, y, z, stelle, mx, bis, faColorsh, mass: Integer;
  Faktor: double;
  A: TRGB;
  //A: RGBArray;
  //B: array of RGBArray;
  SavePenWidth: integer;
  SavePenStyle: TPenStyle;
  SavePenColor: TColor;
begin
  mx := High(Colors);
  if mx < 1 then
  begin
    //raise exception.create('te weinig kleuren');
    aCanvas.FillRect(Rect);
    Exit;
  end;
//  if mx < 1 then Exit; // raise Exception.Create('U moet minsten 2 kleuren kiezen.');

  with ACanvas do
  begin
    if Horicontal then
      mass := Rect.Right - Rect.Left
    else
      mass := Rect.Bottom - Rect.Top;
    AllocRGB(mx + 1);
    for x := 0 to mx do
    begin
      Colors[x] := ColorToRGB(Colors[x]);
      RGBArray[x][0] := GetRValue(Colors[x]);
      RGBArray[x][1] := GetGValue(Colors[x]);
      RGBArray[x][2] := GetBValue(Colors[x]);
    end;
    with Pen do
    begin
      SavePenWidth := Pen.Width;
      SavePenStyle := Pen.Style;
      SavePenColor := Pen.Color;
    end;
    Pen.Width := 1;
    Pen.Style := psSolid;
    faColorsh := Round(mass / mx);
    for y := 0 to mx - 1 do
    begin
      if y = mx - 1 then
        bis := mass - y * faColorsh - 1
      else
        bis := faColorsh;
      for x := 0 to bis do
      begin
        Stelle := x + y * faColorsh;
        faktor := x / bis;
        for z := 0 to 3 do
          a[z] := Trunc(RGBArray[y][z] + ((RGBArray[y + 1][z] - RGBArray[y][z]) * Faktor));
        Pen.Color := RGB(a[0], a[1], a[2]);
        if Horicontal then
        begin
          MoveTo(Rect.Left + Stelle, Rect.Top);
          LineTo(Rect.Left + Stelle, Rect.Bottom);
        end
        else begin
          MoveTo(Rect.Left, Rect.Top + Stelle);
          LineTo(Rect.Right, Rect.Top + Stelle);
        end;
      end;
    end;
    with Pen do
    begin
      Width := SavePenWidth;
      Style := SavePenStyle;
      Color := SavePenColor;
    end;
  end;
end;

{function GetString(buffer: PChar; len: Integer): string;
begin
  SetString(Result, buffer, len);
end;}

type
  TDrawState = (dsNone, dsIgnore, dsChar, dsNextLine, dsReady);


// versie 8 meerdere highlightstrings
procedure ExtTextOutColored(Canvas: TCanvas;
                            const Text: string;
                            R: TRect;
                            WinFlags: DWORD;
                            const HighLightStrings: array of string;
                            const TextColors: array of TColor;
                            const BackColors: array of TColor;
                            const FontStyles: array of TFontStyles);
type
  THighLightInfo = record
    HighLightStr : PString;
    HLen         : Integer;
    MatchPos     : Integer;
    Match        : Boolean;
    TC           : TColor;
    BC           : TColor;
    Style        : TFontStyles;
  end;

const
  MaxInfo = 4;

var
  Info: array[0..MaxInfo - 1] of THighLightInfo;
  Count, i, {BkMode, }TextLen, NormalLen, X, Y, First : Integer;
  StartChar, CurrChar, BeginNormal: PChar;
  SaveBrushColor, SaveFontColor: TColor;
  SaveFontStyle: TFontStyles;
  State: TDrawState;
  TextSize: TSize;

    procedure DoDraw(const P: PChar;  L, i: Integer; DoHighlight: Boolean);
    var
      S: TSize;
    begin
      if L <= 0 then Exit;
      with Info[i], Canvas do
      begin
        GetTextExtentPoint32(Handle, P, L, S); // bereken size
        if not DoHighLight then
        begin
          //Log(['draw normal', getstring(p,l)]);
          SetBkMode(Canvas.Handle, TRANSPARENT);
          Font.Color := SaveFontColor;
        end
        else begin
          //Log(['draw highlight', getstring(p,l)]);
          if BC <> clNone then
          begin
            SetBkMode(Canvas.Handle, OPAQUE);
            Brush.Color := BC;
          end
          else  // niet gedefinieerde achtergrondkleur: neem default
            SetBkMode(Canvas.Handle, TRANSPARENT);
          if TC <> clNone then
            Font.Color := Info[i].TC
          else  // niet gedefinieerde textkleur: neem default
            Font.Color := SaveFontColor;
{          if Style <> [] then
            Font.Style := Style
          else
            Font.Style := SaveFontStyle; }
        end;
        ExtTextOut(Canvas.Handle, X, Y, ETO_CLIPPED, @R, P, L, nil); // en display die hap
      end;
      Inc(X, S.cx); // pas x positie aan
    end;


begin
  TextLen := Length(Text);
  if TextLen = 0 then  Exit;
  Count := Length(HighlightStrings);
  First := -1;
  //Log([count]);
  for i := 0 to Count - 1 do
    if i < MaxInfo then
      with Info[i] do
      begin
        HighlightStr := @HighLightStrings[i];
        HLen := Length(HighLightStr^);
        MatchPos := 1;
        Match := False;
        if Length(TextColors) > i then TC := TextColors[i] else TC := clNone;
        if Length(BackColors) > i then BC := BackColors[i] else BC := clNone;
        if Length(FontStyles) > i then Style := FontStyles[i] else Style := [];
        if First = -1 then
          if HighlightStr^ <> '' then
            First := i;
      end;

//  Assert(First >= 0, 'UGraphics error');
  if First = -1 then First := 0;    

  StartChar := PChar(Text);
  CurrChar := StartChar;
  BeginNormal := StartChar;
  X := R.Left;
  Y := R.Top;
  GetTextExtentPoint32(Canvas.Handle, PChar(Text), TextLen, TextSize);

  { zet x }
  if WinFlags and DT_RIGHT <> 0 then
  begin
    X := R.Right - TextSize.cx;
  end
  else if WinFlags and DT_CENTER <> 0 then
  begin
    X := (R.Right + R.Left) div 2 - TextSize.cx div 2;
  end;

  { zet y }
  if WinFlags and DT_VCENTER <> 0 then
  begin
    Y := (R.Bottom + R.Top) div 2 - TextSize.cy div 2;
  end
  else if WinFlags and DT_BOTTOM <> 0 then
  begin
    Y := R.Bottom - TextSize.cy;
  end;

  with Canvas do
  begin
    SaveFontColor := Font.Color;
    SaveBrushColor := Brush.Color;
    SaveFontStyle := Font.Style;
    State := dsNone;

    while State <> dsReady do
    begin
      case CurrChar^ of
        #32..#255:
          begin
            State := dsChar; // #EL todo check wordbreak spatie/tab
          end;
        #13:
          begin
            State := dsNextLine;
          end;
        #0:
          begin
            State := dsReady;
          end;
      else
        State := dsIgnore;
      end;

      case State of
        dsChar:
          begin
            for i := First to Count - 1 do
              with Info[i] do
                if HLen > 0 then
                begin
                  { test charmatch }
                  Match := UpCase(CurrChar^) = Upcase(HighLightStr^[MatchPos]);
                  if not Match and (MatchPos > 1) then
                  begin
                    MatchPos := 1;
                    Match := UpCase(CurrChar^) = Upcase(HighLightStr^[MatchPos]);
                  end;
                  if Match then Break;
                end;


            for i := First to Count - 1 do
              with Info[i] do
              begin
                { match }
                if Match then
                begin
                  if MatchPos = HLen then
                  begin
                    { teken het voorafgaande stuk normaal }
                    NormalLen := CurrChar - HLen + 1 - BeginNormal;
                    if NormalLen > 0 then
                      DoDraw(BeginNormal, NormalLen, i ,False);
                    { teken daarna het stuk highlight }
                    DoDraw(CurrChar - HLen + 1, HLen, i, True);
                    BeginNormal := CurrChar + 1;
                    MatchPos := 0;
                  end
                end
                { non match }
                else begin
                  MatchPos := 0;
                end;

              Inc(MatchPos);
              if MatchPos > HLen then
                MatchPos := 1;
            end; //for i
          end; // dsChar
        dsReady:
          begin
            NormalLen := CurrChar - BeginNormal;
            DoDraw(BeginNormal, NormalLen, 0{i}, False);
          end; // dsReady
      end;
      Inc(CurrChar);
    end; // while not Ready

    Brush.Color := SaveBrushColor;
    Font.Color := SaveFontColor;
    Font.Style := SaveFontStyle;
  end; // with Canvas

end;

// versie 9 bugfix als strings gedeeltelijk hetzelfde zijn
(*
procedure ExtTextOutColored9(Canvas: TCanvas;
                            const Text: string;
                            R: TRect;
                            WinFlags: DWORD;
                            const HighLightStrings: array of string;
                            const TextColors: array of TColor;
                            const BackColors: array of TColor;
                            const FontStyles: array of TFontStyles);
type
  THighLightInfo = record
    HighLightStr : PString; // pointer ivm sorting
    HLen         : Integer;
    MatchPos     : Integer;
    Match        : Boolean;
    TC           : TColor;
    BC           : TColor;
    Style        : TFontStyles;
  end;

const
  MaxInfo = 4;

var
  Info: array[0..MaxInfo - 1] of THighLightInfo;
  Count, i, {j, BkMode, }TextLen, NormalLen, X, Y, MatchIndex : Integer;
  StartChar, CurrChar, BeginNormal: PChar;
  SaveBrushColor, SaveFontColor: TColor;
  SaveFontStyle: TFontStyles;
  State: TDrawState;
  TextSize: TSize;

    procedure DoDraw(const P: PChar;  L, i: Integer; DoHighlight: Boolean);
    var
      S: TSize;
    begin
      if L <= 0 then Exit;
      with Info[i], Canvas do
      begin
        GetTextExtentPoint32(Handle, P, L, S); // bereken size
        if not DoHighLight then
        begin
          Log(['draw normal', getstring(p,l)]);
          SetBkMode(Canvas.Handle, TRANSPARENT);
          Font.Color := SaveFontColor;
        end
        else begin
          Log(['draw highlight', getstring(p,l)]);
          if BC <> clNone then
          begin
            SetBkMode(Canvas.Handle, OPAQUE);
            Brush.Color := BC;
          end
          else  // niet gedefinieerde achtergrondkleur: neem default
            SetBkMode(Canvas.Handle, TRANSPARENT);
          if TC <> clNone then
            Font.Color := Info[i].TC
          else  // niet gedefinieerde textkleur: neem default
            Font.Color := SaveFontColor;
{          if Style <> [] then
            Font.Style := Style
          else
            Font.Style := SaveFontStyle; }
        end;
        ExtTextOut(Canvas.Handle, X, Y, ETO_CLIPPED, @R, P, L, nil); // en display die hap
      end;
      Inc(X, S.cx); // pas x positie aan
    end;


    procedure Sort;
    var
      i, j: Integer;
    begin
      for i := 0 to Count - 1 do
        for j := i + 1 to Count - 1 do
          if Info[j].HLen > Info[i].HLen then
            Swap(Info[i], Info[j], SizeOf(THighlightInfo));
    end;

begin
  TextLen := Length(Text);
  if TextLen = 0 then  Exit;
  Count := Length(HighlightStrings);
  for i := 0 to Count - 1 do
    if i < MaxInfo then
      with Info[i] do
      begin
        HighlightStr := @HighLightStrings[i];
        HLen := Length(HighLightStrings[i]);
        MatchPos := 1;
        Match := False;
        if Length(TextColors) > i then TC := TextColors[i] else TC := clNone;
        if Length(BackColors) > i then BC := BackColors[i] else BC := clNone;
        if Length(FontStyles) > i then Style := FontStyles[i] else Style := [];
      end;
  Sort;

  StartChar := PChar(Text);
  CurrChar := StartChar;
  BeginNormal := StartChar;
  X := R.Left;
  Y := R.Top;
  GetTextExtentPoint32(Canvas.Handle, PChar(Text), TextLen, TextSize);

  { zet x }
  if WinFlags and DT_RIGHT <> 0 then
  begin
    X := R.Right - TextSize.cx;
  end
  else if WinFlags and DT_CENTER <> 0 then
  begin
    X := (R.Right + R.Left) div 2 - TextSize.cx div 2;
  end;

  { zet y }
  if WinFlags and DT_VCENTER <> 0 then
  begin
    Y := (R.Bottom + R.Top) div 2 - TextSize.cy div 2;
  end
  else if WinFlags and DT_BOTTOM <> 0 then
  begin
    Y := R.Bottom - TextSize.cy;
  end;

  with Canvas do
  begin
    SaveFontColor := Font.Color;
    SaveBrushColor := Brush.Color;
    SaveFontStyle := Font.Style;
    State := dsNone;

    while State <> dsReady do
    begin
      case CurrChar^ of
        #32..#255:
          begin
            State := dsChar; // #EL todo check wordbreak spatie/tab
          end;
        #13:
          begin
            State := dsNextLine;
          end;
        #0:
          begin
            State := dsReady;
          end;
      else
        State := dsIgnore;
      end;

      case State of
        dsChar:
          begin
            MatchIndex := -1;
            { test charmatch }
            for i := 0 to Count - 1 do
              with Info[i] do
                if HLen > 0 then
                begin
                  Match := UpCase(CurrChar^) = Upcase(HighLightStr^[MatchPos]);
                  if not Match and (MatchPos > 1) then
                  begin
                    MatchPos := 1;
                    Match := UpCase(CurrChar^) = Upcase(HighLightStr^[MatchPos]);
                  end;
                  if Match then
                  begin
                    MatchIndex := i;
                    Break;
                  end;
                end; // for i

            //Log([MatchIndex]);

            if MatchIndex >= 0 then
              with Info[MatchIndex] do
              begin
                { einde string }
                if MatchPos = HLen then
                begin
                  { teken het voorafgaande stuk normaal }
                  NormalLen := CurrChar - HLen + 1 - BeginNormal;
                  if NormalLen > 0 then
                    DoDraw(BeginNormal, NormalLen, i ,False);
                  { teken daarna het stuk highlight }
                  DoDraw(CurrChar - HLen + 1, HLen, i, True);
                  BeginNormal := CurrChar + 1;
                  MatchPos := 1;
                end
                else begin
                  Inc(MatchPos);
                  if MatchPos > HLen then
                    MatchPos := 1;
                end;
              end // if MatchIndex
            else begin
              for i := 0 to Count - 1 do
                with Info[i] do
                begin
                  MatchPos := 1;
                end;
            end;

          end; // dsChar
        dsReady:
          begin
            NormalLen := CurrChar - BeginNormal;
            DoDraw(BeginNormal, NormalLen, 0{i}, False);
          end; // dsReady
      end;
      Inc(CurrChar);
    end; // while not Ready

    Brush.Color := SaveBrushColor;
    Font.Color := SaveFontColor;
    Font.Style := SaveFontStyle;
  end; // with Canvas

end; *)
end.


