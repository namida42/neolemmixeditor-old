unit SplitPanels_Reg;

interface

uses
  Classes, DesignIntf, DesignEditors;

type
  TTemplateProperty = class(TStringProperty)
  private
  protected
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;

  published
  end;

procedure Register;

implementation

uses
  USplit;

procedure Register;
begin
  RegisterComponents('Master', [ TSplitPanel]);
  RegisterPropertyEditor(TypeInfo(string), TSplitPanel, 'TemplateName', TTemplateProperty);
  RegisterPropertiesInCategory('SplitPanel', TSplitPanel,
    ['ShowCaption',
    'Size',
    'SplitPosition',
    'MaxSize',
    'MinSize',
    'FixedSize',
    'Distance',
    'LiveSizing',
    'SplitMode',
    'PanelCount',
    'DivideMode',
    'ScaleOnResize',
    'AfterCalculate',
    'Options',
    'ResizeStep',
    'TemplateName',
    'QuickBorder']
  );

end;


{ TTemplateProperty }

function TTemplateProperty.GetAttributes: TPropertyAttributes;
begin
  Result := inherited GetAttributes;
  Include(Result, paValueList);
end;

procedure TTemplateProperty.GetValues(Proc: TGetStrProc);
var
  i: Integer;
begin
  with TemplateMgr do
    for i := 0 to ClassCount - 1 do
      Proc(ClassList[i]);
end;

end.

