unit UExtensions;

interface

uses
  Windows, Classes, SysUtils, TypInfo, Db, IBCustomDataSet, IBSQL, Controls,
  Math, Variants, Dialogs,
  UMisc, ULog, UFastStrings, ActnList;

//procedure ParseQuickFilter(const S: string);

const
  SWhereClause = '%WHERE ';
  SOrderByClause = '%ORDERBY ';
  SAsc = 'ASCENDING';
  SDesc = 'DESCENDING';

type
  TDataSetExt = class;
  EDataSetExtError = class(Exception);

  TOrder = ({oNone, }oAscending, oDescending);

  TOrderByItem = class(TCollectionItem)
  private
    fFieldName: string;
    fOrigin: string;
    fOrder: TOrder;
    function DataSetExt: TDataSetExt;
  protected
  public
    property Origin: string read fOrigin write fOrigin;
    property Order: TOrder read fOrder write fOrder;
    property FieldName: string read fFieldName write fFieldName;
  published
  end;

  TOrderByClause = class(TOwnedCollection)
  private
    function GetText: string;
    procedure SetText(const Value: string);
    function GetItem(Index: Integer): TOrderByItem;
  protected
    function DataSetExt: TDataSetExt;
  public
    constructor Create(aOwner: TPersistent);
    property Items[Index: Integer]: TOrderByItem read GetItem; default;
    property Text: string read GetText write SetText;
  published
  end;

  TWhereType = (wtSystem, wtDetail, wtCustom, wtQuick);

  TWhereItem = class(TCollectionItem)
  private
    fWhereType: TWhereType;
    fText: string;
    procedure SetText(const Value: string);
    procedure SetWhereType(const Value: TWhereType);
  protected
  public
  published
    property Text: string read fText write SetText;
    property WhereType: TWhereType read fWhereType write SetWhereType;
  end;

  TWhereClause = class(TOwnedCollection)
  private
    function GetItem(Index: Integer): TWhereItem;
    function GetText: string;
    procedure QuickSort;
  protected
    procedure Update(Item: TCollectionItem); override;
  public
    constructor Create(aOwner: TPersistent);
    function Add: TWhereItem;
    property Items[Index: Integer]: TWhereItem read GetItem; default;
    property Text: string read GetText;
  published
  end;

  TDataSetWrapper = class(TComponent)
  private
    function GetEof: Boolean;
    function GetBookmarkStr: TBookmarkStr;
    procedure SetBookmarkStr(const Value: TBookmarkStr); // de gelinkte dataset
  protected
    fDataSet: TIBDataSet;
  public
  { public methods }
    procedure Close;
    procedure DisableControls;
    procedure EnableControls;
    procedure First;
    procedure Next;
    procedure Prior;
    procedure Open;
    procedure Edit;
    procedure Post;
    procedure Append;
    procedure Delete;
    function FieldByName(const FieldName: string): TField;
    function IsEmpty: Boolean;
    function Locate(const KeyFields: string; const KeyValues: Variant;
      Options: TLocateOptions): Boolean;

    function LocateNext(const KeyFields: string; const KeyValues: Variant;
      Options: TLocateOptions): Boolean;
  { public properties }
    property DataSet: TIBDataSet read fDataSet;
    property Eof: Boolean read GetEof;
    property Bookmark: TBookmarkStr read GetBookmarkStr write SetBookmarkStr;
  end;

  TFieldFunc = (
    ffNone,
    ffPassword,
    ffPickList,
    ffCalendar,
    ffHistory,
    ffLookup,
    ffLookupSelf,
    ffNumberPicker,                 //met minumum en maximum
    ff5MinPicker,                   // range minimum, maximum
    ff10MinPicker,                  //         idem
    ff15MinPicker,                  //         idem
    ff20MinPicker,                  //         idem
    ff30MinPicker,                  //         idem
    ff60MinPicker,                  //         idem
    ffWeekPicker,                   // range fstartdate, fstopdate
    ffMonthPicker,                  //         idem
    ffQuarterPicker,                //         idem
    ffHalfyearPicker,               //         idem
    ffYearPicker,                   //         idem
    ffColorPicker,
    ffFieldPicker,                  //         in linkfilter staat het field waar de table vandaan moet komen
    ffTablePicker,
{    ffFieldfuncPicker,}
    ffFixedPicker,                   // vooraf stringlist defineren...
    ffRightsLevelPicker
    );

    TRightsLevel = (
     rlGod_Mode,
     rlSuper_User,
     rlAdministrator,
     rlController,
     rlSupervisor,
     rlExpert_User,
     rlStandard_User,
     rlBasic_User,
     rlSimple_User,
     rlGuest
     );

    TStringDisplayFormat= (
     sfNoFormat,
     sfFirstNice,
     sfAllNice,
     sfAllCaps,
     sfAllLows,
     sfNoSpaces,
     sfLeftTrim,
     sfRightTrim,
     sfBothTrim,
     sfPassword
    );

    TDateDisplayFormat= (
     dfNoFormat,
     dfEuropean,
     dfAmerican,
     dfMilitary,
     dfFullEuropean,
     dfFullAmerican,
     dfShortEuropean,
     dfShortAmerican
//     dfHideNullDate
    );

    TNumericDisplayFormat= (
     nfNoFormat,
     nfLeadingZero1,
     nfLeadingZero2,
     nfLeadingZero3,
     nfLeadingZero4,
     nfLeadingZero5,
     nfLeadingZero6,
     nfHideZero,
     nfCurrency,
     nfMKCurrency,
     nfScientific,
     nfRound10,
     nfRound5,
     nfGMKBytes,
     nfSignColors,
     nfNegativeBrackets,
     mfMinusSign,
     nfBits
    );

    TTimeDisplayFormat= (
     tfNoFormat,
     tfFull,
     tfHourMinSec,
     tfHourMin,
     tfHidezero
     );

     TDefaultMethod = (
      dmNone,
      dmMinimum,
      dmMaximum,
      dmSysteemDT,
      dmFirst,
      dmLast,
      dmDefaultValue
      );


    TMasterType = (
     mtString,
     mtInteger,
     mtCurrency,
     mtFloat,
     mtDateTime,
     mtDate,
     mtTime,
     mtMemo,
     mtBoolean,
     mtBlob
     );


    TStringDisplaySet = set of TStringDisplayFormat;
    TDateDisplaySet = set of TStringDisplayFormat;
    TNumericDisplaySet = set of TNumericDisplayFormat;
    TTimeDisplaySet = set of TTimeDisplayFormat;

  TFieldExt = class(TPersistent)
  private
    fDataSetExt    : TDataSetExt;
    fDataField     : string;
    fFieldFunc     : TFieldFunc;
    fMasterType    : TMasterType;
    fFixedList     : TStringlist;
    fLinkFilter    : string;
    fPrompt        : string;
    fLinkTitel     : string;
    fLinkTabel     : string;     // uit welke 'basis' tabel komen de gegevens bv picklist/historie/setings?
    fLinkSelect    : string;     // deze velden worden geselecteerd
    fLinkShow      : string;     // deze velden worden getoond, indien leeg copy van select
    fLinkPrompt    : string;     // prompts behorend bij show
    fLinkReturn    : string;     // deze velden worden gepropt in assign
    fLinkAssign    : string;     // linkassign[x]:=linkreturn[x]
    fLinkkey       : string;     // unique key field (= linktabel+'_ID' indien leeg)
//    fLinkParam     : string;     // parameter veld (om auto data related acties te doen, fields(table))

{ Defaults & rangecheck}
    fRangeCheck    : boolean;
    fMinimum       : integer;    // bv vanaf 1 (bv settings rangecheck)
    fMaximum       : integer;    // bv t/m 12
    fStartDate     : tdatetime;    // bv vanaf  1-jan-2000
    fStopDate      : tdatetime;    // bv t/m 31-dec-2004    (uitzondering vandaag=0)
    fDefault       : string;
    fDefaultMethod : tDefaultMethod;
    fAlleenlezen   : Boolean;

{ Interal }
    fField         : TField;
    fIndex         : Integer;
    fNoWrite       : Boolean;

{ Display formats afhankelijk van dbtype/fieldfunc}
    fDatumWeergave        : TDateDisplayFormat;
    fGetalWeergave        : TNumericDisplaySet;
    fStringWeergave       : TStringDisplaySet;
    fTijdWeergave         : TTimeDisplaySet;
{ Action }
    fAction        : TAction;

    procedure GetFieldFunc(const Value: TFieldFunc);
    function SetFieldFunc: TFieldFunc;
    procedure GetMasterType(const Value: TMasterType);
    function SetMasterType: TMasterType;

  protected
  public
    constructor Create(aDataSetExt: TDataSetExt);
    destructor Destroy; override;
    function FieldFuncCalendar: Boolean;
    function FieldFuncPickList(const aTitel,aFilter,aAssign:string): Boolean;
    function FieldFuncPeriodPicker(precision:tFieldFunc;start,stop:tdatetime): Boolean; overload;
    function FieldFuncLookupSelf(const aTabel,aPrompt,aAssign:string): Boolean;
    function FieldFuncRightsLevel: Boolean;
    function FieldFuncTablePicker: Boolean;
    function FieldFuncFieldPicker(const aTabel:string): Boolean;
    function SetRange(aMin,aMax:integer):boolean;
    function FieldFuncLookup(const aTabel,
                                   aSelects,
                                   aShows,
                                   aPrompts,
                                   aReturns,
                                   aAssigns:string): Boolean;
  public
    property DataSetExt    : TDataSetExt read fDataSetExt; //public read
    property Index         : Integer read fIndex; // public read
    property Field         : TField read fField; // public read
    property Action        : TAction read fAction write fAction;
  published
    property FieldFunc     : TFieldFunc read SetFieldFunc write GetFieldFunc stored true;
    property MasterType    : TMasterType read SetMasterType write GetMasterType stored true;
    property Prompt        : string read fPrompt write fPrompt stored true;
    property DataField     : string read fDataField write fDataField stored true;
//    property DataType      : tFieldType read fDataType write fDataType stored true;
  {loopup/picklist}
    property LinkTabel     : string read fLinkTabel write fLinkTabel stored true;
    property LinkTitel     : string read fLinkTitel write fLinkTitel stored true;
    property LinkFilter    : string read fLinkFilter write fLinkFilter stored true;
    property LinkSelect    : string read fLinkSelect write fLinkSelect stored true;
    property LinkShow      : string read fLinkShow write fLinkShow stored true;
    property LinkPrompt    : string read fLinkPrompt write fLinkPrompt stored true;
    property LinkReturn    : string read fLinkReturn write fLinkReturn stored true;
    property LinkAssign    : string read fLinkAssign write fLinkAssign stored true;
    property LinkKey       : string read fLinkKey    write fLinkkey stored true;
    property FixedList     : tstringlist read fFixedList write fFixedList;
  {period picker}
    property _RangeCheck    : boolean read fRangeCheck write fRangeCheck stored true;
    property Minimum       : integer read fMinimum   write fMinimum stored true;
    property Maximum       : integer read fMaximum   write fMaximum stored true;
    property StartDate     : TDateTime read fStartDate  write fStartDate stored true;
    property StopDate      : tDateTime read fStopDate   write fStopDate stored true;
    property NoWrite       : Boolean read fNoWrite write fNoWrite;
    property AlleenLezen   : Boolean read fAlleenlezen write fAlleenlezen;
    property StringWeergave: TStringDisplaySet read fStringWeergave write fStringWeergave;
    property DatumWeergave : TDateDisplayFormat read fDatumWeergave write fDatumWeergave;
    property GetalWeergave : TNumericDisplaySet read fGetalWeergave write fGetalWeergave;
    property TijdWeergave  : TTimeDisplaySet read fTijdWeergave write fTijdWeergave;
    property DefaultMethode: TDefaultMethod read fDefaultMethod write fDefaultMethod;
  end;



  TCustomDataSetExt = class(TDataSetWrapper)
  private
  protected
    fFieldNames: TStringList;
    fOrigins: TStringList;
    fFieldExtList: TList;
    function GetField(Index: Integer): TField;
    function GetFieldName(Index: Integer): string;
    function GetFieldAsTDateTime(Index: Integer): TDateTime;
    function GetFieldAsDouble(Index: Integer): Double;
    function GetFieldAsInteger(Index: Integer): Integer;
    function GetFieldAsString(Index: Integer): string;
    procedure SetFieldAsTDateTime(Index: Integer; const Value: TDateTime);
    procedure SetFieldAsDouble(Index: Integer; const Value: Double);
    procedure SetFieldAsInteger(Index: Integer; const Value: Integer);
    procedure SetFieldAsString(Index: Integer; const Value: string);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
  end;

  TSelectType = (stSelect, stRefresh, stModify, stInsert, stDelete);
  TSelectArray = array[TSelectType] of string;
  TCapability = stModify..stDelete;
  TCapabilities = set of TCapability;

  TQuickAndOr = (qAnd, qOr);

  TSqlChange = (
    scSystemWhere,
    scDetailWhere,
    scQuickFilter, scQuickExact, scQuickCase, scQuickFilterFields, scQuickAndOr, scQuickInverse,
    scCustomWhere,
    scOrder, scOrderDirection
  );
  TSqlChanges = set of TSqlChange;

  TSQLChangeEvent = procedure(Sender: TObject; aChanges: TSQLChanges) of object;

  TDataSetExt = class(TCustomDataSetExt)
  private
    //fInitialized         : Boolean; // voor check
    fCapabilities        : TCapabilities;
    fStatements          : TSelectArray; // de sql's
    //fSysWhere            : string; // systeem filter
    fDetailWhere         : string; // master detail
    fQuickFilter         : string; //
    fCustomWhere         : string; // filter
    fOrder               : string; // filter
    fMainTable           : string; // onze hoofdtabel van de selectie
    fKeyField            : string; // primary key als aanwezig
//    fFieldExtList        : TList;
//    fOrigins             : TStringList; // lijst van origins
//    fFieldNames          : TStringList; // lijst van veldnamen
    fQuickFilterFields   : TStringList; // welke velden worden meegenomen in quickfilter
    fQuickExact          : Boolean;
    fQuickCase           : Boolean;
    fMasterSource        : TDataSource;
    fOrderByClause       : TOrderByClause;
    fWhereClause         : TWhereClause;
    fUpdateLock          : Integer;
    fChanges             : TSqlChanges;
    fRecordSQL           : TIBSQL;
    fOrderDirection      : TOrder;
    fAfterSQLChange      : TSQLChangeEvent;
    fSystemWhere         : string;
    fQuickFilterEx       : string;
    fQuickAndOr          : TQuickAndOr;
    fQuickInverse        : Boolean;
  { 1. system where }
    procedure SetSystemWhere(const Value: string);
  { 2. master detail }
    function GetMaster: TDataSet;
    procedure SetDetailWhere(const aDetailWhere: string); // private
  { 3. quick filters }
    function GetQuickFilterFields: string;
    procedure SetQuickFilterFields(const Value: string);
    procedure SetQuickCase(const Value: Boolean);
    procedure SetQuickExact(const Value: Boolean);
    procedure SetQuickFilter(const Value: string);
    procedure SetQuickFilterEx(const Value: string);
  { 4. custom where }
    procedure SetCustomWhere(const aWhere: string);
  { 5. order }
    function GetOrderField: string;
    procedure SetOrderDirection(const Value: TOrder);
    procedure SetOrderClause(const aOrder: string);
  { 6. overig }
    function GetFieldExt(Index: Integer): TFieldExt;
    function GetFieldExtCount: Integer;
//    procedure EnsureFieldExts;
    procedure CreateStatements;
//    procedure EnsureFieldNames;
    function FindOrigin(const aFieldName: string): Integer;
//    function FindOriginName(const aFieldName: string): string;
    function FindFieldName(const aOrigin: string): string;
    function GetOriginField(N: Integer): string;
    function GetOriginTable(N: Integer): string;
    procedure SetOrigins(const aOrigins: string);
    function GetCurrentStatement: string;
    //procedure LogFieldDefs;
    function EnsureOrigin(aField: TField): string;
    //procedure Field_GetText(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure SetQuickAndOr(const Value: TQuickAndOr);
    procedure SetQuickInverse(const Value: Boolean);

    function FieldNamesOK: Boolean;
    procedure InitNames(const aOrigins: string);
    procedure InternalCreate(aOwner: TComponent;
                             const aStatements: TSelectArray;
                             const aOrigins, aPrimaryKeyField: string;
                             const aCapabilities: TCapabilities = [stModify, stInsert, stDelete]);
    function GetStatement(Index: TSelectType): string;
  { datasetevents }
  protected
    procedure Update;
    procedure UpdateWhereAndOrder(aChanges: TSqlChanges);
    procedure UpdateQuickFilter;
    procedure CreatePersistentFields; virtual;
    procedure LinkFieldExts;
  public
    constructor Create(aOwner: TComponent;
                       const aStatements: TSelectArray;
                       const aOrigins, aPrimaryKeyField: string;
                       const aCapabilities: TCapabilities = [stModify, stInsert, stDelete]); reintroduce; overload;
    constructor Create(aOwner: TComponent;
                       const aSelect: string;
                       const aOrigins, aPrimaryKeyField: string;
                       const aCapabilities: TCapabilities = [stModify, stInsert, stDelete]); reintroduce; overload;
//    procedure CreatePersistentFields;
    destructor Destroy; override;
  { 1. system where }
    property SystemWhere: string read fSystemWhere write SetSystemWhere;
  { 2. master detail }
    property Master: TDataSet read GetMaster;
    procedure SetMaster(aDataSetExt: TDataSetExt; const aDetailField, aMasterField: string); overload;
    procedure SetMaster(aDetailField, aMasterField: TField); overload;
  { 3. quick filters }
    property QuickFilter: string read fQuickFilter write SetQuickFilter;
    property QuickFilterEx: string read fQuickFilterEx write SetQuickFilterEx;
    property QuickFilterFields: string read GetQuickFilterFields write SetQuickFilterFields;
    property QuickExact: Boolean read fQuickExact write SetQuickExact;
    property QuickCase: Boolean read fQuickCase write SetQuickCase;
    property QuickAndOr: TQuickAndOr read fQuickAndOr write SetQuickAndOr;
    property QuickInverse: Boolean read fQuickInverse write SetQuickInverse;
  { 4. custom where }
    property CustomWhere: string read fCustomWhere write SetCustomWhere;
  { 5. order }
    property OrderClause: string read fOrder write SetOrderClause;
    property OrderField: string read GetOrderField;
    property OrderDirection: TOrder read fOrderDirection write SetOrderDirection;
  { 6. overig }
    procedure RefreshRecord;
    procedure AppendRecordCopy;
    function GetPosition: Integer;
    procedure SetPosition(P: Integer);
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure GetPersistentSource(L: TStringList; const TypeName: string; MakePersistentFields: Boolean = False); // delphi source
    procedure GetPersistentSource2(L, M: TStringList; const TypeName: string); // delphi source
    procedure Test;
    function FindFieldExt(const aFieldName: string): TFieldExt;
    property MainTable: string read fMainTable;
    property CurrentStatement: string read GetCurrentStatement;
    property FieldExtCount: Integer read GetFieldExtCount;
    property FieldExts[Index: Integer]: TFieldExt read GetFieldExt;
    property Statements[Index: TSelectType]: string read GetStatement;
    property FieldNames: TStringList read fFieldNames;
  published
  { events }
    property AfterSQLChange: TSQLChangeEvent read fAfterSQLChange write fAfterSQLChange;
  end;

type
  TFieldExtWrapper = class(TComponent)
  private
    fExt: TFieldExt;
  published
    property Ext: TFieldExt read fExt write fExt;
  end;


function DatExt(DS: TDataSet): TDataSetExt;
function FldExt(aField: TField): TFieldExt;

procedure WritePersistentSource(const aFileName: string;
                                const Ext: array of TDataSetExt;
                                const TypeNames: array of string;
                                MakePersistentFields: Boolean = False);
procedure WritePersistentSource2(aPath: string;
                                 const InterfaceFile: string;
                                 const ImplementationFile: string;
                                 const Ext: array of TDataSetExt;
                                 const TypeNames: array of string);

procedure SaveExt(E: TFieldExt; const aFileName: string);

//      VANAF HIER toevoeging door gb

function RightsLevel2Str(level: integer): string;

//     TOT HIER  toevoeging door gb
//

implementation

uses
  DBCommon;

const
  SAndOr: array[TQuickAndOr] of string = ('AND', 'OR');

function DatExt(DS: TDataSet): TDataSetExt;
begin
  if DS = nil then
    Result := nil
  else
    Result := TDataSetExt(DS.Tag);
end;

function FldExt(aField: TField): TFieldExt;
begin
  if aField = nil then
    Result := nil
  else
    Result := TFieldExt(aField.Tag);  
end;
{function GetUniqueKey(DB: TIBDatabase; const aTableName: string): string;
var
  Qry: TIBSQL;
begin
  Qry := TIBSQL.Create(nil);
  Qry.Database := DB;
  try
  finally
    Qry.Free;
  end;
end;}

procedure WritePersistentSource(const aFileName: string;
                                const Ext: array of TDataSetExt;
                                const TypeNames: array of string;
                                MakePersistentFields: Boolean = False);
var
  Txt: TextFile;
  i, j: integer;
  L: TStringList;
  FileExt: string;

    procedure Add(const S: string; Indent: Byte);
    begin
      WriteLn(Txt, StringOfChar(' ', Indent) + S);
    end;

begin
  if FileExists(aFileName) then
    CopyFile(PChar(aFileName), PChar('bk_' + aFilename), False);

  AssignFile(Txt, aFileName);
  Rewrite(Txt);
  L := TStringList.Create;
  try
    FileExt := ExtractFileExt(aFileName);
    Add('unit ' + StringReplace(aFileName, FileExt, '', [rfReplaceAll]) + ';', 0);
    Add('', 0);
    Add('interface', 0);
    Add('', 0);
    Add('uses', 0);
    Add('Db, UExtensions;', 2);
    Add('', 0);

    for i := Low(Ext) to High(Ext) do
    if Assigned(Ext[i]) then
    begin
      //Add('', 0);
      Ext[i].GetPersistentSource(L, TypeNames[i], MakePersistentFields);
      for j := 0 to L.Count - 1 do
        WriteLn(Txt, L[j]);
      Add('', 0);
    end;

    Add('', 0);
    Add('implementation', 0);
    Add('', 0);
    Add('end.', 0);
  finally
    CloseFile(Txt);
    L.Free;
  end;
end;

procedure WritePersistentSource2(aPath: string;
                                 const InterfaceFile: string;
                                 const ImplementationFile: string;
                                 const Ext: array of TDataSetExt;
                                 const TypeNames: array of string);
var
  Txt, ImpFile: TextFile;
  i, j: integer;
  L, M: TStringList;
  IntBak, IntTemp, IntNew,
  ImpBak, ImpTemp, ImpNew: string;

    procedure Add(const S: string; Indent: Byte);
    begin
      WriteLn(Txt, StringOfChar(' ', Indent) + S);
    end;

    function AskNewIntf: Boolean;
    begin
      Result := MessageDlg('Aanmaken INTERFACE bestand? ' + CrLf +
        IntNew, mtConfirmation, [mbYes, mbNo], 0) = mrYes;
    end;

    function AskNewImpl: Boolean;
    begin
      Result := MessageDlg('Aanmaken IMPLEMENTATION bestand? ' + CrLf +
        ImpNew, mtConfirmation, [mbYes, mbNo], 0) = mrYes;
    end;

    function AskReplaceIntf: Boolean;
    begin
      Result := MessageDlg('Vervangen INTERFACE bestand? ' + CrLf +
        IntNew, mtConfirmation, [mbYes, mbNo], 0) = mrYes;
    end;

    function AskReplaceImpl: Boolean;
    begin
      Result := MessageDlg('Vervangen IMPLEMENTATION bestand? ' + CrLf +
        ImpNew, mtConfirmation, [mbYes, mbNo], 0) = mrYes;
    end;

begin
  //H := ExtractFilePath((InterfaceFile));
//  IntBak :=
  if aPath = '' then
    AppError('WritePersistentSource: pad is leeg');

  if CompareText(InterfaceFile, ImplementationFile) = 0 then
    AppError('WritePersistentSource: InterfaceFile en ImplementationFile zijn hetzelfde');

  aPath := IncludeTrailingBackSlash(aPath);
  ForceDirectories(aPath + 'temp');

  IntBak  := aPath + 'temp\bak_' + InterfaceFile;
  IntTemp := aPath + 'temp\temp_' + InterfaceFile;
  IntNew  := aPath + InterfaceFile;

  ImpBak  := aPath + 'temp\bak_' + ImplementationFile;
  ImpTemp := aPath + 'temp\temp_' + ImplementationFile;
  ImpNew  := aPath + ImplementationFile;

  { maak backups }
  if FileExists(IntNew) then
    CopyFile(PChar(IntNew), PChar(IntBak), False);
  if FileExists(ImpNew) then
    CopyFile(PChar(ImpNew), PChar(ImpBak), False);

  AssignFile(Txt, IntTemp);
  Rewrite(Txt);
  AssignFile(ImpFile, ImpTemp);
  Rewrite(ImpFile);

  L := TStringList.Create;
  M := TStringList.Create;
  try
    for i := Low(Ext) to High(Ext) do
    if Assigned(Ext[i]) then
    begin
      Ext[i].GetPersistentSource2(L, M, TypeNames[i]);
      for j := 0 to L.Count - 1 do
        WriteLn(Txt, L[j]);
      Add('', 0);
      for j := 0 to M.Count - 1 do
        WriteLn(ImpFile, M[j]);
    end;
  finally
    CloseFile(Txt);
    CloseFile(ImpFile);
    L.Free;
    M.Free;
  end;

  { interface schrijf of vervang na bevestiging }
  if not FileExists(IntNew) then
  begin
    if AskNewIntf then
      CopyFile(PChar(IntTemp), PChar(IntNew), False);
  end
  else begin
    if not CompareFiles(IntTemp, IntNew) then
      if AskReplaceIntf then
        CopyFile(PChar(IntTemp), PChar(IntNew), False);
  end;

  { implementation schrijf of vervang na bevestiging }
  if not FileExists(ImpNew) then
  begin
    if AskNewImpl then
      CopyFile(PChar(ImpTemp), PChar(ImpNew), False);
  end
  else begin
    if not CompareFiles(ImpTemp, ImpNew) then
      if AskReplaceImpl then
        CopyFile(PChar(ImpTemp), PChar(ImpNew), False);
  end;
end;

(*procedure WritePersistentSource2(aPath: string;
                                 const InterfaceFile: string;
                                 const ImplementationFile: string;
                                 const Ext: array of TDataSetExt;
                                 const TypeNames: array of string);
var
  Txt, ImpFile: TextFile;
  i, j: integer;
  L, M: TStringList;
  //H, Int, ImpBak: string;
//  FileExt: string;

    procedure Add(const S: string; Indent: Byte);
    begin
      WriteLn(Txt, StringOfChar(' ', Indent) + S);
    end;

begin
  //H := ExtractFilePath((InterfaceFile));
//  IntBak :=
  aPath := IncludeTrailingBackSlash(aPath);
  ForceDirectories(aPath + 'temp');

  { maak backup }
  if FileExists(aPath + InterfaceFile) then
    CopyFile(PChar(aPath + InterfaceFile), PChar(aPath + 'temp\bak_' + InterfaceFile), False);

  if FileExists(aPath + ImplementationFile) then
    CopyFile(PChar(aPath + ImplementationFile), PChar(aPath + 'temp\bak_' + ImplementationFile), False);

  AssignFile(Txt, aPath + 'temp\temp_' + InterfaceFile);
  Rewrite(Txt);
  AssignFile(ImpFile, aPath + 'temp\temp_' + ImplementationFile);
  Rewrite(ImpFile);

  L := TStringList.Create;
  M := TStringList.Create;
  try
  //  FileExt := ExtractFileExt(InterfaceFile);
    for i := Low(Ext) to High(Ext) do
    if Assigned(Ext[i]) then
    begin
      Ext[i].GetPersistentSource2(L, M, TypeNames[i]);
      for j := 0 to L.Count - 1 do
        WriteLn(Txt, L[j]);
      Add('', 0);
      for j := 0 to M.Count - 1 do
        WriteLn(ImpFile, M[j]);
    end;

  finally
    CloseFile(Txt);
    CloseFile(ImpFile);
    L.Free;
    M.Free;
  end;


  if not FileExists(aPath + InterfaceFile) or
    not CompareFiles(aPath + 'temp\temp_' + InterfaceFile, aPath + InterfaceFile) then
  begin
    windlg('intfile ' + interfacefile + ' not equal');
    CopyFile(PChar(aPath + 'temp\temp_' + InterfaceFile), PChar(aPath + InterfaceFile), False);
  end;
//  else
  //  windlg('intfile ' + interfacefile + ' hetzelfde');

  if not FileExists(aPath + ImplementationFile) or
    not CompareFiles(aPath + 'temp\temp_' + ImplementationFile, aPath + ImplementationFile) then
  begin
    windlg('impfile ' + ImplementationFile + ' not equal');
    CopyFile(PChar(aPath + 'temp\temp_' + ImplementationFile), PChar(aPath + ImplementationFile), False);
  end;
//  else
  //  windlg('impfile ' + ImplementationFile + ' hetzelfde');

end; *)

procedure SaveExt(E: TFieldExt; const aFileName: string);
var
  W: TFieldExtWrapper;
begin
  W := TFieldExtWrapper.Create(nil);
  try
    W.Ext := E;
    ComponentToTextFile(W, aFileName);
  finally
    W.Free;
  end;
end;

const
  Reserved: array [0..2] of string = ('AND', 'OR', 'NOT');

(*
procedure ParseQuickFilter(const S: string);
var
  P, NewWord: PChar;
  L: TStringList;
  Token: string;

    function Next: Char;
    begin
      Inc(P);
      Result := P^;
      Dec(P);
    end;

    function ReservedWord: Boolean;
    var
      i: Integer;
    begin
      for i := 0 to 2 do
        if CompareText(Token, Reserved[i]) = 0 then
        begin
          Result := True;
          Exit;
        end;
      Result := False;
    end;

begin
  L := TStringList.Create;
  try
  P := PChar(S);
  NewWord := P;

  repeat
    // test quotes/and/or

    case P^ of
      ' ', #0:
        begin
          if P = NewWord then
          while P^{Next} = ' ' do begin
            Inc(P);
            Inc(NewWord)
          end;
//          Log(['nieuw woord']);
  //        if P^ = #0 then Log(['eind']);
          if P <> NewWord then
          begin
          SetString(Token, NewWord, P - NewWord); //dbcommon
{          if ReservedWord then
            Log(['operator', Token])
          else                    }
            Log(['token', token]);
          end;
          if P^ <> #0 then
          NewWord := P + 1;
        end;
      '"':
        begin
          Inc(P);
          NewWord := P;
          while not (P^ in [#0, '"']) do Inc(P);
          //Log(['while...', p^, ord(p^)]);
          if P^ <> '"' then AppError('quotes');
          SetString(Token, NewWord, P - NewWord);
          Log(['quoted', token]);
          Inc(P);
          NewWord := P;
          if P^ = #0 then break;
        end;
    end;

    if P^ = #0 then Break;
    Inc(P);

  until False;
  finally
  L.Free;
  end;
end;
*)


{ TOrderByItem }

function TOrderByItem.DataSetExt: TDataSetExt;
begin
  Result := nil;
  if Collection is TOrderByClause then
  begin
    Result := TOrderByClause(Collection).DataSetExt;
  end;
end;

{ TOrderByClause }

constructor TOrderByClause.Create(aOwner: TPersistent);
begin
  Assert(aOwner is TDataSetExt, ClassName + '.Create foute owner');
  inherited Create(aOwner, TOrderByItem);
end;

function TOrderByClause.DataSetExt: TDataSetExt;
begin
  Result := TDataSetExt(GetOwner);
end;

function TOrderByClause.GetItem(Index: Integer): TOrderByItem;
begin
  Result := TOrderByItem(inherited Items[Index])
end;

function TOrderByClause.GetText: string;
var
  i: Integer;
  O: TOrderByItem;
begin
  Result := '';
  for i := 0 to Count - 1 do
  begin
    O := TOrderByItem(GetItem(i));
    if Result <> '' then Result := Result + ',';
    Result := Result + O.Origin;
    if O.Order = oDescending then
      Result := Result + ' ' + SDesc;
  end;
  Result := 'ORDER BY ' + Result;
end;

procedure TOrderByClause.SetText(const Value: string);
//var
//  L: TStringList;
  //i: Integer;
var
  Start: PChar;
  Token: string;
  SQLToken, CurSection: TSQLToken;
  TempOrigin: string;
  TempOrder: TOrder;
begin

  Clear;

  Start := PChar(Value);
  CurSection := stUnknown;
  {-------------------------------------------------------------------------------
    zoek de orderby clause (verplicht!)
  -------------------------------------------------------------------------------}
  repeat
    SQLToken := NextSQLToken(Start, Token, CurSection);
    if SQLToken in SQLSections then
      CurSection := SQLToken;
  until SQLToken in [stEnd, stOrderBy];

  TempOrigin := '';
  TempOrder := oAscending;

  {-------------------------------------------------------------------------------
    ontrafel tabelnaam.veld of veldnaam en ascending of descending
    en voeg items toe
  -------------------------------------------------------------------------------}
  if CurSection = stOrderBy then
  repeat
    SQLToken := NextSQLToken(Start, Token, CurSection);

    case SQLToken of
      stTableName   : TempOrigin := Token;
      stFieldName   : TempOrigin := TempOrigin + '.' + Token;
      stDescending  : TempOrder := oDescending;
      stAscending   : TempOrder := oAscending;
    end;

    if SQLToken in [stEnd, stAscending, stDescending] then
    if Count = 0 then
    begin
      with TOrderByItem.Create(Self) do
      begin
        Origin := TempOrigin;
        Order := TempOrder;
        FieldName := DataSetExt.FindFieldName(Origin);
//        Log(['value', Value]);
  //      Log(['origin', Origin]);
    //    Log(['fieldname', fieldname]);
        //Log([fieldname]);
      end;
      TempOrigin := '';
      TempOrder := oAscending;
    end;

  until SQLToken in [stEnd];

end;

{ TWhereItem }

procedure TWhereItem.SetText(const Value: string);
begin
  if Text <> Value then
  begin
    fText := Value;
    Changed(False);
  end;
end;

procedure TWhereItem.SetWhereType(const Value: TWhereType);
begin
  if fWhereType <> Value then
  begin
    fWhereType := Value;
    Changed(False);
  end;
end;

{ TWhereClause }

function TWhereClause.Add: TWhereItem;
begin
  //Log(['ADD']);
  Result := TWhereItem(inherited Add);
end;

constructor TWhereClause.Create(aOwner: TPersistent);
begin
  Assert(aOwner is TDataSetExt, ClassName + '.Create foute owner');
  inherited Create(aOwner, TWhereItem);
end;

function TWhereClause.GetItem(Index: Integer): TWhereItem;
begin
  Result := TWhereItem(inherited Items[Index])
end;

function TWhereClause.GetText: string;
var
  i: Integer;
  PreviousWhere, Where: TWhereItem;

    procedure AddAndOr;
    begin
      if PreviousWhere <> nil then
      begin
        // quickfilter AND OR
        Result := Result + CrLf + ' AND ' + CrLf;
      end;
    end;

begin
  Result := '';
  PreviousWhere := nil;

  for i := 0 to Count - 1 do
  begin
    Where := Items[i];
    //Log([i, Where.Text]);
    if Where.Text = '' then
      Continue;

    case Where.WhereType of
      wtSystem, wtDetail, wtCustom:
        begin
          AddAndOr;
          Result := Result + '(' + Where.Text + ')';
        end;
      wtQuick:
        begin
          AddAndOr;
          Result := Result + '(' + Where.Text + ')';
          //
        end;
    end; // case Where.WhereType

    PreviousWhere := Where;
  end; // for i
end;

procedure TWhereClause.QuickSort;

    function CompareItems(A, B: Integer): Integer;
    begin
      if Items[A].WhereType < Items[B].WhereType then
        Result := -1
      else if Items[A].WhereType > Items[B].WhereType then
        Result := 1
      else
        Result := 0;
    end;

    procedure ExchangeItems(A, B: Integer);
    var
      T: string;
      W: TWhereType;
    begin
//      Log([items[a].text, items[B].text]);
      with Items[A] do
      begin
        T := fText;
        W := fWhereType;
        fText := Items[B].fText;
        fWhereType := Items[B].fWhereType;
      end;
      with Items[B] do
      begin
        fText := T;
        fWhereType := W;
      end;
//      Log([items[a].text, items[b].text]);
  //    Log(['***']);
    end;

    procedure DoSort(L, R: Integer);
    var
      I, J, P: Integer;
    begin
      repeat
        I := L;
        J := R;
        P := (L + R) shr 1;
        repeat
          while CompareItems(I, P) < 0 do Inc(I);
          while CompareItems(J, P) > 0 do Dec(J);
          if I <= J then
          begin
            ExchangeItems(I, J);
            if P = I then
              P := J
            else if P = J then
              P := I;
            Inc(I);
            Dec(J);
          end;
        until I > J;
        if L < J then DoSort(L, J);
        L := I;
      until I >= R;
    end;

begin
//  BeginUpdate;
  if Count > 1 then
  begin
    //Log(['sort']);
    DoSort(0, Count - 1);
  end;
  //EndUpdate;      utools
end;

procedure TWhereClause.Update(Item: TCollectionItem);
begin
//  Log(['sort']);
  QuickSort;
end;

{ TDataSetWrapper }

procedure TDataSetWrapper.Append;
begin
  fDataSet.Append;
end;

procedure TDataSetWrapper.Close;
begin
  fDataSet.Close;
end;

procedure TDataSetWrapper.Delete;
begin
  fDataSet.Delete;
end;

procedure TDataSetWrapper.DisableControls;
begin
  fDataSet.DisableControls;
end;

procedure TDataSetWrapper.Edit;
begin
  fDataSet.Edit;
end;

procedure TDataSetWrapper.EnableControls;
begin
  fDataSet.EnableControls;
end;

function TDataSetWrapper.FieldByName(const FieldName: string): TField;
begin
  Result := fDataSet.FieldByName(FieldName);
end;

procedure TDataSetWrapper.First;
begin
  fDataSet.First;
end;

function TDataSetWrapper.GetBookmarkStr: TBookmarkStr;
begin
  Result := fDataSet.Bookmark;
end;

function TDataSetWrapper.GetEof: Boolean;
begin
  Result := fDataSet.Eof;
end;

function TDataSetWrapper.IsEmpty: Boolean;
begin
  Result := fDataSet.IsEmpty;
end;

function TDataSetWrapper.Locate(const KeyFields: string;
  const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  Result := fDataSet.Locate(KeyFields, KeyValues, Options);
end;

function TDataSetWrapper.LocateNext(const KeyFields: string;
  const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  Result := fDataSet.LocateNext(KeyFields, KeyValues, Options);
end;

procedure TDataSetWrapper.Next;
begin
  fDataSet.Next;
end;

procedure TDataSetWrapper.Open;
begin
  fDataSet.Open;
end;

{ TCustomDataSetExt }

constructor TCustomDataSetExt.Create(aOwner: TComponent);
begin
  inherited;
  fOrigins := TStringList.Create;
  fFieldNames := TStringList.Create;
  fFieldExtList := TList.Create;
end;

destructor TCustomDataSetExt.Destroy;
begin
  fOrigins.Free;
  fFieldNames.Free;
  fFieldExtList.Free;
  inherited;
end;

function TCustomDataSetExt.GetFieldAsTDateTime(Index: Integer): TDateTime;
begin
  Result := fDataSet.Fields[Index].AsDateTime;
end;

function TCustomDataSetExt.GetFieldAsDouble(Index: Integer): Double;
begin
  Result := fDataSet.Fields[Index].AsFloat;
end;

function TCustomDataSetExt.GetFieldAsInteger(Index: Integer): Integer;
begin
  Result := fDataSet.Fields[Index].AsInteger;
end;

function TCustomDataSetExt.GetFieldAsString(Index: Integer): string;
begin
  Result := fDataSet.Fields[Index].AsString;
end;

function TCustomDataSetExt.GetField(Index: Integer): TField;
begin
  Result := fDataSet.Fields[Index];
end;

function TCustomDataSetExt.GetFieldName(Index: Integer): string;
begin
  Assert(fFieldNames <> nil, Classname + 'GetFieldName: fFieldNames = nil');
  Assert(fFieldNames.Count > Index, Classname + 'GetFieldName: index parameter' + i2s(Index));
  Result := fFieldNames[Index];
{  if fDataSet = nil then
    if fDataSet.FieldDefs.Count < Index then
      AppError('getfieldname');
  Result := fDataSet.FieldDefs[Index].Name; }
end;

procedure TCustomDataSetExt.SetFieldAsDouble(Index: Integer; const Value: Double);
begin
  fDataSet.Fields[Index].AsFloat := Value;
end;

procedure TCustomDataSetExt.SetFieldAsInteger(Index: Integer; const Value: Integer);
begin
  fDataSet.Fields[Index].AsInteger := Value;
end;

procedure TCustomDataSetExt.SetFieldAsString(Index: Integer; const Value: string);
begin
  fDataSet.Fields[Index].AsString := Value;
end;

procedure TCustomDataSetExt.SetFieldAsTDateTime(Index: Integer; const Value: TDateTime);
begin
  fDataSet.Fields[Index].AsDateTime := Value;
end;

{ TFieldExt}

constructor TFieldExt.Create(aDataSetExt: TDataSetExt);
begin
  inherited Create;
  fDataSetExt := aDataSetExt;
  fFixedList := TStringList.Create;
end;

//******************************************************************************
//******************************************************************************
///             VANAF HIER    toevoeging door gb
//******************************************************************************
//******************************************************************************


function RightsLevel2Str(level: integer): string;
begin
  if (level < ord(low(TRightsLevel))) or (level > ord(high(TRightsLevel))) then begin
    result := 'Unknown';
//    log(['Error unknown UserLevel',level]);
    exit; end;
  result := getenumname(typeinfo(TRightsLevel), level);
  result := i2s(level)+ ' '+ rstr(result,length(result)-2);
  result := fastreplace(result,'_',' ');
end;

function TFieldExt.FieldFuncCalendar: Boolean;
begin
  Fieldfunc:=ffCalendar;
  Result := True; //### ERIC warning delphi
end;

function TFieldExt.SetRange(aMin,aMax:integer): Boolean;
begin
  result:=true;
  if aMax<aMin then begin result:=false;exit;end;
  Minimum:=aMin;
  Maximum:=aMax;
  _RangeCheck:=true;
end;

Function TFieldExt.FieldFuncPickList(const aTitel,aFilter, aAssign:string): Boolean;
begin
  Fieldfunc  := ffPicklist;
  LinkTitel  := aTitel;
  LinkFilter := 'referentie = "' +uppercase(AFilter)+'" ';
  LinkTabel  := 'Picklist';
  LinkKey    := LinkTabel+'_ID';
  LinkSelect := 'StringWaarde;Referentie;Ref_id;'+LinkKey;
  LinkShow   := 'Stringwaarde;Ref_id';
  LinkPrompt := 'Item;ID';
  LinkReturn := 'Ref_id';
  LinkAssign := aAssign;

  log(['add picklist']);
  log(['show',linkshow]);
  log(['select',linkselect]);
  log(['return',linkreturn]);
  log(['assign',linkassign]);

  result:=true;
end;

function TFieldExt.FieldFuncLookupSelf(const aTabel,aPrompt,aAssign:string): Boolean;
begin
{ TODO : select distinct werkt nog niet, of agenda_id niet meenemen in selectie }
  Fieldfunc  := ffLookupSelf;
  LinkTabel  := aTabel;
  Linkkey    := aAssign;
  LinkTitel  := 'History';
  LinkPrompt := aPrompt;
  LinkShow   := aAssign;
  LinkSelect := aAssign;//+';'+linkkey;;
  LinkAssign := aAssign;
  LinkReturn := aAssign;
{
  log(['add picklist']);
  log(['show',linkshow]);
  log(['select',linkselect]);
  log(['return',linkreturn]);
  log(['assign',linkassign]);
}
  result:=true;
end;

function TFieldExt.FieldFuncLookup(const aTabel,
                                         aSelects,
                                         aShows,
                                         aPrompts,
                                         aReturns,
                                         aAssigns:string): Boolean;
begin
  Fieldfunc  := ffLookup;
  LinkTabel  := aTabel;
  Linkkey    := aTabel+'_ID';
  LinkTitel  := 'Lookup';
  LinkPrompt := aPrompts;
  LinkSelect := aSelects;
  if aShows='' then LinkShow:=LinkSelect;
  LinkShow   := aShows;
  LinkAssign := aAssigns;
  LinkReturn := aReturns;
{
  log(['add picklist']);
  log(['show',linkshow]);
  log(['select',linkselect]);
  log(['return',linkreturn]);
  log(['assign',linkassign]);
}
  result:=true;
end;

function TFieldExt.FieldFuncPeriodPicker(precision:tFieldFunc;start,stop:tdatetime): Boolean;
begin
 FieldFunc  := precision;
 StartDate  := start;
 StopDate   := stop;
 result     := true;
 LinkAssign := ffield.fieldname;//datafield;
end;

destructor TFieldExt.Destroy;
begin
(*//  windlg('');
  if ffixedlist<>nil then
  begin
//    log(['Free Fixedlist',linktitel]);
//    windlg('');
    fixedlist.free;
    fixedlist:=nil;
  end; *)
  FreeandNil(ffixedlist);
   inherited;
end;

function TFieldExt.FieldFuncRightsLevel: Boolean;
var i:integer;
begin
  result:=false;
  try
//  log(['FieldFunc RightsLevel']);
    fRangeCheck:=true;
    fMinimum:=0;
    fMaximum:=9;
    fieldfunc:=ffRightsLevelPicker;
    fLinkTitel :='Maak uw keuze...';
    fLinkPrompt:='RightLevels';
    fLinkAssign:=ffield.fieldname;//fDataField;

{ TODO : olala hoe gaan we dit doen??? }
//    EvGetText := 'DisplayRightsLevel';
//  EvSetText := eventhandler.InputRightsLevel;}

    with fFixedList do
    for i:=ord(low(TRightsLevel)) to ord(high(TRightsLevel)) do
                       begin
                         //log(['rl',i,RightsLevel2str(i)]);
                         add(RightsLevel2str(i));
                       end;
  result:=true;
  except
  end;
  if not result then log(['Critical Error in FieldFuncRightsLevel']);
end;

function TFieldExt.FieldFuncTablePicker: Boolean;
//var i:integer;
begin
  result:=false;
  try
    log(['FieldFunc TablePicker']);
{ TODO : tabelnaam check (of deze nog bestaat) }
{ TODO : tabelnaam check of gebruiker deze mag zien... }
    fLinkTitel :='Maak uw keuze...';
    fieldfunc:=ffTablePicker;
    fLinkAssign:=ffield.fieldname;//fDataField;
    fLinkPrompt:='Tabellen';

{    FixedList:=TStringList.Create;
    DatasetExt.Dataset.Database.GetTableNames(FixedList);}
    result:=true;
  except
  end;
  if not result then log(['Critical Error in FieldFuncFieldPicker']);
end;

function TFieldExt.FieldFuncFieldPicker(const aTabel: string): Boolean;
begin
  result:=false;
  try
    log(['FieldFunc FieldPicker']);
{ TODO : tabelnaam check (of deze nog bestaat) }
{ TODO : tabelnaam check of gebruiker deze mag zien... }
    fLinkTitel :='Maak uw keuze...';
    fieldfunc:=ffFieldPicker;
    fLinkFilter:=aTabel;
    fLinkAssign:=ffield.fieldname;//fDataField;
    fLinkPrompt:='Tabellen';

{    FixedList:=TStringList.Create;
    DatasetExt.Dataset.Database.GetTableNames(FixedList);}
    result:=true;
  except
  end;
  if not result then log(['Critical Error in FieldFuncFieldPicker']);

end;


procedure TFieldExt.GetFieldFunc(const Value: TFieldFunc);
begin
  log(['Vullen van Fieldfunc',Field.fieldname,getenumname(typeinfo(TFieldFunc), ord(value))]);
  fFieldFunc:=value;
end;

function TFieldExt.SetFieldFunc: TFieldFunc;
begin
  
  result:=fFieldFunc;
end;

function GetBetween(const S, aFrom, aTo: string; DoTrim: Boolean = True): string;
var
  FromPos, ToPos, CopyLen: Integer;
begin
  Result := '';

  // from
  if aFrom = '' then
    FromPos := 1
  else begin
    FromPos := FastPosNoCase(S, aFrom, Length(S), Length(aFrom), 1);
    if FromPos = 0 then
      Exit;
    Inc(FromPos, Length(aFrom));
  end;

  // to
  if aTo = '' then
    ToPos := Length(S)
  else begin
    ToPos := FastPosNoCase(S, aTo, Length(S), Length(aTo), FromPos);
    if ToPos = 0 then
      ToPos := Length(S)
    else
      Dec(ToPos);
  end;

  CopyLen := ToPos - FromPos + 1;

  if CopyLen <= 0 then
    Exit;

  Result := Copy(S, FromPos, CopyLen);
  if DoTrim then
    Result := Trim(Result);
end;

procedure TFieldExt.GetMasterType(const Value: TMasterType);
begin
  fMasterType:=Value;
end;

function TFieldExt.SetMasterType: TMasterType;
begin
  result:=fMasterType;
end;

{ TDataSetExt }

procedure TDataSetExt.InitNames(const aOrigins: string);
var
  Temp: TStringList;
  i: Integer;
  S, Org, Alias, Typ: string;
  //AsPos: Integer;
  //TypePos: Integer;
  Ext: TFieldExt;
//  AliasCopy: Integer;
begin

  Temp := TStringList.Create;
  SplitString_To_StringList(aOrigins, Temp, ',', True);

  try
    with Temp do
      for i := 0 to Count - 1 do
      begin
        S := Strings[i];
        S := FastReplace(S, #13, '');
        S := FastReplace(S, #10, '');

        Org   := GetBetween(S, '', ' AS ');
        Alias := GetBetween(S, ' AS ', ' TYPE ');
        Typ   := GetBetween(S, ' TYPE ' , '');


//        log([org, alias, Typ]);

        Assert(Pos('.', Org) > 0, Self.ClassName + '.InitNames Ontbrekende Origin');
        Assert(Pos(' ', Alias) = 0, Self.ClassName + '.InitNames Spatie in alias');

        { maak origin, fieldname en fieldext aan }
        fOrigins.Add(Org);
        fFieldNames.Add(UpperCase(Alias));
        Ext := TFieldExt.Create(Self);
        fFieldExtList.Add(Ext);
        Ext.fIndex := i;
        Ext.fPrompt := Alias;
        Ext.fDataField := Alias;
        if Typ = 'NOWRITE' then
          Ext.NoWrite := True;
        //  Log([Alias, 'readonly veld']);
      end;
      {Log(['------------ORIGINS']);
      loglist(forigins);
      Log(['------------FIELDNAMES']);
      loglist(ffieldnames);}
  finally
    Temp.Free;
  end;

end;

procedure TDataSetExt.InternalCreate(aOwner: TComponent; const aStatements: TSelectArray;
  const aOrigins, aPrimaryKeyField: string; const aCapabilities: TCapabilities);

    procedure CorrectSelectSQL; // voegt zo nodig whereclause en orderbyclase toe
    var
      S: string;
      W, O: Integer;
    begin
      S := fStatements[stSelect];
      W := FastPosNoCase(S, SWhereClause, Length(S), Length(SWhereClause), 1);
      O := FastPosNoCase(S, SOrderByClause, Length(S), Length(SOrderByClause), 1);
      if (W < 1) and (O < 1) then
        S := S + CrLf + SWhereClause + SOrderByClause
      else if (W >= 1) and (O < 1) then
        S := S + SOrderByClause
      else if (W < 1) and (O >= 1) then
        S := FastReplace(S, SOrderByClause, CrLf + SWhereClause + SOrderByClause, False)
      else
        Exit;
      fStatements[stSelect] := S;
    end;

begin
  Assert(aOwner is TIBCustomDataSet, ClassName + '.InternalCreate error 1');

  fCapabilities := aCapabilities;

  { interne objecten }
  fWhereClause := TWhereClause.Create(Self);
  fOrderByClause := TOrderByClause.Create(Self);
  fMasterSource := TDataSource.Create(Self);
  fQuickFilterFields := TStringList.Create;
  fRecordSQL := TIBSQL.Create(Self);

  { props }
  fDataSet := TIBDataSet(aOwner);
  fDataSet.Tag := Longint(Self);
  fStatements := aStatements;
  CorrectSelectSQL;
  fKeyField := aPrimaryKeyField;
  fMainTable := GetTableNameFromSQL(fStatements[stSelect]);
  SetOrigins(aOrigins);
  InitNames(aOrigins);
  fQuickFilterFields.Assign(fOrigins);
  fQuickFilterFields.Sorted := True;

  CreatePersistentFields;
  LinkFieldExts;

  { statements }
  CreateStatements;
  with fDataSet do
  begin
    RefreshSQL.Text := fStatements[stRefresh];
    ModifySQL.Text := fStatements[stModify];
    InsertSQL.Text := fStatements[stInsert];
    DeleteSQL.Text := fStatements[stDelete];
  end;

  fRecordSQL.SQL.Text := fStatements[stRefresh];
  UpdateWhereAndOrder([]);
end;


{ TDataSetExt

    aStatements:
      Een array van de vijf selectstatements. De eerste (select) moet gevuld zijn.
      De rest wordt automatisch aangemaakt, wanneer leeg.

    aOrigins:
      een kommagescheiden lijst van tabel.veld. deze moet gelijk zijn aan de
      selectlist van de selectsql

    aPrimaryKeyField:
      Een (liefst uniek integer primary key) field.
      Dit veld wordt gebruikt om het actuele record te bewaren na een close en open.
      Ook wordt het gebruikt om de statements aan de maken.

}

constructor TDataSetExt.Create(aOwner: TComponent;
                               const aStatements: TSelectArray;
                               const aOrigins, aPrimaryKeyField: string;
                               const aCapabilities: TCapabilities = [stModify, stInsert, stDelete]);

begin
  inherited Create(aOwner);
  InternalCreate(aOwner, aStatements, aOrigins, aPrimaryKeyField, aCapabilities);
end;

constructor TDataSetExt.Create(aOwner: TComponent; const aSelect, aOrigins,
  aPrimaryKeyField: string; const aCapabilities: TCapabilities);
var
  Stat: TSelectArray;
begin
  inherited Create(aOwner);
  Stat[stSelect] := aSelect;
  Stat[stRefresh] := '';
  Stat[stModify] := '';
  Stat[stInsert] := '';
  Stat[stDelete] := '';
  InternalCreate(aOwner, Stat, aOrigins, aPrimaryKeyField, aCapabilities);
end;

procedure TDataSetExt.CreatePersistentFields;
{var
  i: Integer;
  F: TField; }
begin
{
  with DataSet do
  begin
    Prepare;
    try
      with FieldDefs do
        for i := 0 to Count - 1 do
        begin
          F := Items[i].CreateField(nil);
          Log([F.ClassName]);
          F.Free;
        end;
    finally
      UnPrepare;
    end;
  end;
  }
end;

function TDataSetExt.GetFieldExt(Index: Integer): TFieldExt;
begin
  Result := TFieldExt(fFieldExtList[Index]);
end;

function TDataSetExt.GetFieldExtCount: Integer;
begin
  Result := fFieldExtList.Count;
end;

(*procedure TDataSetExt.EnsureFieldExts;
var
  i: Integer;
  Def: TFieldDef;
  Ext: TFieldExt;
begin
  Assert(fDataSet <> nil, ClassName + '.CreateFieldExts: DataSet = nil');
  Assert(fDataSet.Prepared, ClassName + '.CreateFieldExts: DataSet is niet prepared');
  if fFieldExtList.Count > 0 then
    Exit;
  with fDataSet do
    for i := 0 to FieldDefs.Count - 1 do
    begin
      Ext := TFieldExt.Create(Self);
      fFieldExtList.Add(Ext);
      Def := FieldDefs[i];
      Ext.fIndex := i;
      Ext.fPrompt := Def.Name;
      Ext.fDataField := Def.Name;
      //Log([Self.Name, fMainTable, Ext.fDataField]);
    end;
end;*)

procedure TDataSetExt.CreateStatements;

    function GetUpdateList: string;
    var
       i: Integer;
      S, T, F, A: string;
      Ext: TFieldExt;
    begin
      Result := '';
      for i := 0 to fOrigins.Count - 1 do
      begin
        Ext := TFieldExt(fFieldExtList[i]); {###EL}
        S := fOrigins[i];
        T := GetOriginTable(i);
        //if FastPosNoCase(S, fMainTable, Length(S), Length(fMainTable), 1) = 1 then
        if CompareText(T, fMainTable) = 0 then
        begin
          if Ext.NoWrite then Continue; {###EL}
          F := GetOriginField(i);
          A := fFieldNames[i];
          if Result <> '' then Result := Result + ',' + CrLf;
          Result := Result + F + ' = :' + A;
        end;
      end;
    end;

    function GetInsertList(Params: Boolean): string;
    var
       i: Integer;
      Prefix, T, F: string;
      Ext: TFieldExt;
    begin
      Result := '';
      if Params then Prefix := ':' else Prefix := '';
      for i := 0 to fOrigins.Count - 1 do
      begin
        Ext := TFieldExt(fFieldExtList[i]); {###EL}
        //S := fOrigins[i];
        T := GetOriginTable(i);
        if CompareText(T, fMainTable) = 0 then
        begin
          if Ext.NoWrite then Continue; {###EL}
          F := GetOriginField(i);
          if Result <> '' then Result := Result + ',';
          Result := Result + Prefix + F;
        end;
      end;
    end;

var
  S: string;

begin
  { refresh statement }
  if fStatements[stRefresh] = '' then
  begin
    S := FastReplace(fStatements[stSelect], SWhereClause, '');
    S := FastReplace(S, SOrderByClause, '');
    S := S + CrLf +
     'WHERE ' + fKeyField + ' = :OLD_' + fKeyField;
   fStatements[stRefresh] := S;
  end;

  { modify statement (als toegestaan) }
  if fStatements[stModify] = '' then fStatements[stModify] :=
    'UPDATE ' + fMainTable + ' SET ' + CrLf +
      GetUpdateList + CrLf +
    'WHERE ' + fKeyField + ' = :OLD_' + fKeyField;
  if not (stModify in  fCapabilities) then
    fStatements[stModify] := '';

  { insert statement (als toegestaan) }
  if fStatements[stInsert] = '' then fStatements[stInsert] :=
    'INSERT INTO ' + fMainTable + CrLf +
    '(' + GetInsertList(False) + ')' + CrLf +
    'VALUES ' + CrLf +
    '(' + GetInsertList(True) + ')';
  if not (stInsert in  fCapabilities) then
    fStatements[stInsert] := '';
//  Log(['INSERT']);
//  Log([]);
  //Log([fStatements[stInsert]]);

//  fStatements[stInsert] := '';

  { delete statement }
  if fStatements[stDelete] = '' then fStatements[stDelete] :=
    'DELETE FROM ' + fMainTable + CrLf +
    'WHERE ' + fKeyField + ' = :OLD_' + fKeyField;
  if not (stDelete in  fCapabilities) then
    fStatements[stDelete] := '';
end;

(*procedure TDataSetExt.EnsureFieldNames;
var
  i: Integer;
begin
  if fFieldNames.Count > 0 then
    Exit;
//  if not fDataSet.Prepared then
  //  fDataSet.Prepare;
  with fDataSet.FieldDefs do
    for i := 0 to Count - 1 do
    begin
      fFieldNames.Add(Items[i].Name);
      //Log([items[i].name]);
    end;
end;*)

destructor TDataSetExt.Destroy;
var
  i: Integer;
begin
//  fOrigins.Free;
//  fFieldNames.Free;
//  fFieldExtList.Free;
  fOrderByClause.Free;
  fWhereClause.Free;
  fQuickFilterFields.Free;
  with fFieldExtList do
    for i := 0 to Count - 1 do
      TFieldExt(Items[i]).Free;
  inherited;
end;

(*procedure TDataSetExt.Field_GetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  case DisplayText of
    False:
      begin
        Text := Sender.AsString;
      end;
    True:
      begin
        if Sender.Value = NULL then
          Text := '<NULL>'
        else
          Text := Sender.AsString;
      end;
  end;
end; *)

function TDataSetExt.FindOrigin(const aFieldName: string): Integer;
var
  S: string;
  P, PP, J: Integer;
//  Fld: TField;
//  Found: Boolean;
begin
//  Result := -1; ## klopt de delphi hint!? dat hier niets mee gedaan wordt?
  S := aFieldName;
  {-------------------------------------------------------------------------------
    als er een punt in afieldname zit, zoek dan rechtstreeks in forigins, zoniet
    dan mag afieldname
    a) de veldnaam zijn. in dat geval wordt de index van het veld als uitgangs
       punt genomen.
    b) het gedeelte achter de punt zijn. zijn er meerdere origins met dezelfde
       veldnaam dan wordt de eerst gevonden genomen.
  -------------------------------------------------------------------------------}
  P := CharPos('.', aFieldName);

  if P > 0 then
  begin
    Result := fOrigins.IndexOf(S);
    Exit;
  end
  else begin

    // zoek op basis van veldnaam

    Result := fFieldNames.IndexOf(S);
    if Result >= 0 then
      Exit;
    (*
    Fld := fDataSet.FindField(S);
    if Fld <> nil then
    begin
      Result := Fld.FieldNo - 1; // TField.FieldNo is 1-based
      Exit;
    end;
    *)

    // zoek op basis van origin achter de punt
    S := '.' + S;
    for J := 0 to fOrigins.Count - 1 do
    begin
      // zoek origin achter de punt
      PP := FastPosNoCase(fOrigins[j], S, Length(fOrigins[j]), Length(S), 1);
      if PP > 0 then
      begin
        Result := J;
        Exit;
      end;
    end;

  end; // else

  Result := -1;
end;

(*function TDataSetExt.FindOriginName(const aFieldName: string): string;
var
  i: Integer;
begin
  i := FindOrigin(aFieldName);
  Assert(i >= 0, ClassName + '.FindOriginName: [' + aFieldName + '] niet gevonden');
  Result := fOrigins[i];
end;*)

function TDataSetExt.GetCurrentStatement: string;
begin
  Result := fDataSet.SelectSQL.Text;
end;

function TDataSetExt.EnsureOrigin(aField: TField): string;
begin
  with aField do
  begin
    {-------------------------------------------------------------------------------

      if Origin = '' then //IBX zet origin bij heropenen!!!!!!!

    -------------------------------------------------------------------------------}
    Origin := fOrigins[aField.Index];
    Result := Origin;
  end;
end;

function TDataSetExt.GetQuickFilterFields: string;
begin
  Result := fQuickFilterFields.CommaText;
end;

function TDataSetExt.GetMaster: TDataSet;
begin
  Result := nil;
  if fDataSet.DataSource <> nil then
    if fDataSet.DataSource = fMasterSource then
      Result := fMasterSource.DataSet;
end;

function TDataSetExt.GetOriginField(N: Integer): string;
var
  P: Integer;
  S: string;
begin
  Result := '';
  S := fOrigins[N];
  P := Pos('.', S);
  if P > 0 then
  begin
    Result := Copy(S, P + 1, Length(S));
  end;
end;

function TDataSetExt.GetOriginTable(N: Integer): string;
var
  P: Integer;
  S: string;
begin
  Result := '';
  S := fOrigins[N];
  P := Pos('.', S);
  if P > 0 then
  begin
    Result := Copy(S, 1, P - 1);
  end;
end;

procedure TDataSetExt.GetPersistentSource(L: TStringList; const TypeName: string; MakePersistentFields: Boolean = False);
var
//  L: TStringList;
  Def: TFieldDef;
  i: Integer;
  IsPrepared: Boolean;
  TypeStr: string;
//  F: TField;
  //Txt: TextFile;


    procedure Add(const S: string; Indent: Byte);
    begin
      L.Add(StringOfChar(' ', Indent) + S);
//      Log([StringOfChar(' ', Indent) + s]);
    end;

    function GetTypeStr: string;
    begin
      Result := '';
      //Log([getenumname(typeinfo(tfieldtype), integer(def.datatype))]);
      case Def.DataType of
        //ftUnknown,
        ftString, ftBlob:
          Result := 'string';
        ftSmallint, ftInteger, ftWord:
          Result := 'Integer';
{        ftBoolean, }
        ftFloat:
          Result := 'Double';
        {, ftCurrency, ftBCD, }
        ftDate, ftTime, ftDateTime:
          Result := 'TDateTime';
        {ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo,
        ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString,
        ftLargeint, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
        ftVariant, ftInterface, ftIDispatch, ftGuid); }
      end;
    end;

begin
  if fDataSet = nil then
    Exit;
  IsPrepared := fDataSet.Prepared;
  if not IsPrepared then
    fDataSet.Prepare;

  L.Clear;
//  L := TStringList.Create;


  with fDataSet do
  try
    Add('type', 0);
    Add(TypeName{'T' + fDataSet.Name + 'Ext} + ' = class(' + 'TDataSetExt)', 2);
    Add('public', 2);
    Add('{ values }', 2);
    for i := 0 to FieldDefs.Count - 1 do
    begin
      Def := FieldDefs[i];
      TypeStr := GetTypeStr;
      Add('property ' + PadR(FirstNice(Def.Name), 32) + ': ' +
                        PadR(TypeStr, 12) +
                        ' index ' + LeadZeroStr(Def.FieldNo - 1, 2) +
                        ' read ' + 'GetFieldAs' + TypeStr +
                        ' write SetFieldAs' + TypeStr + ';', 4);
    end;

    // fields
    Add('{ fields }', 2);
    for i := 0 to FieldDefs.Count - 1 do
    begin
      Def := FieldDefs[i];
      TypeStr := GetTypeStr;
      Add('property ' + PadR('Fld' + FirstNice(Def.Name), 32) + ': ' +
                        PadR('TField', 12) +
                        ' index ' + LeadZeroStr(Def.FieldNo - 1, 2) +
                        ' read ' + 'GetField;', 4);
    end;

    // Persistent fields
    if MakePersistentFields then
      begin
        Add('{ persistent fields }', 2);
        for i := 0 to FieldDefs.Count - 1 do
        begin
          Def := FieldDefs[i];
          TypeStr := GetTypeStr;
          Add(PadR('Fld_' + FirstNice(Def.Name), 32) + ': ' + Def.FieldClass.ClassName, 4);
        end;
      end;

    Add('{ names }', 2);
    for i := 0 to FieldDefs.Count - 1 do
    begin
      Def := FieldDefs[i];
      TypeStr := GetTypeStr;
      Add('property ' + PadR('Name' + FirstNice(Def.Name), 32) + ': ' +
                        PadR('string', 12) +
                        ' index ' + LeadZeroStr(Def.FieldNo - 1, 2) +
                        ' read ' + 'GetFieldName;', 4);
    end;

    Add('end;', 2);
//    Add('', 0);

{    AssignFile(Txt, aFileName);
    if Overwrite then
      Rewrite(Txt)
    else
      System.Append(Txt);

    for i := 0 to L.Count - 1 do
      WriteLn(Txt, L[i]);

    CloseFile(Txt); }

  finally
//    L.Free;
  end;
end;

type
  TFieldDefWrapper = class(TComponent)
  private
    fFieldDef: TFieldDef;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    property FieldDef: TFieldDef read fFieldDef write fFieldDef;
  end;

procedure TDataSetExt.AppendRecordCopy;
var temp : topenvariantarray;
var
  i: Integer;
begin
  with dataset do
  begin
  setlength(temp,fieldcount);
  for i:=0 to fieldcount-1 do
  begin
     temp[i]:=null;
     if comparetext(fields[i].fieldname,fmaintable+'_ID') <> 0 then
       temp[i]:=fields[i].value else
  end;
  append;
  edit;
  for i:=0 to fieldcount-1 do
  begin
  if temp[i]<>null then fields[i].value:=temp[i];
  end;
//  post;
  end;//
end;

procedure TDataSetExt.LinkFieldExts;
var
  i: Integer;
begin
  with fFieldExtList do
  begin
    if DataSet.FieldCount = Count then
      for i := 0 to Count - 1 do
      begin
        TFieldExt(Items[i]).fField := fDataSet.Fields[i];
        fDataSet.Fields[i].Tag := Integer(Items[i]);
//        Log(['ext linked to field', fDataSet.Fields[i].FieldName]);
      end;
  end;
end;

{ TFieldDefWrapper }

constructor TFieldDefWrapper.Create(aOwner: TComponent);
begin
  inherited;
  fFieldDef := TFieldDef.Create(nil);
end;

destructor TFieldDefWrapper.Destroy;
begin
  fFieldDef.Free;
  inherited;
end;

procedure TDataSetExt.GetPersistentSource2(L, M: TStringList; const TypeName: string);
var
//  L: TStringList;
  DS: TIBDataSet;
  Def: TFieldDef;
  i: Integer;
  //IsPrepared: Boolean;
  TypeStr: string;
//  F, C: string;
  //Txt: TextFile;

    { haal published props van fielddef op en maak er assignments van }
    procedure TestField(aDef: TFieldDef);
    var
      F: TField;
    begin
      F := aDef.CreateField(nil);
      try
        Log(['fieldname=', F.FieldName]);
        Log(['required=',F.Required]);
//        Log(['required=',F.Required]);
      finally
        F.Free;
      end;
    end;

    procedure Add(const S: string; Indent: Byte);
    begin
      L.Add(StringOfChar(' ', Indent) + S);
    end;

    procedure Adm(const S: string; Indent: Byte);
    begin
      M.Add(StringOfChar(' ', Indent) + S);
//      Log([s]);
    end;

    procedure FieldDefToConstructor(aDef: TFieldDef);
    var
      W: TFieldDefWrapper;
      S: string;
      List: TStringList;
      i: Integer;

      FName: string;
      Typ: string;

    begin
      List := TStringList.Create;
      try
        W := TFieldDefWrapper.Create(nil);
        try
          W.fFieldDef.Assign(aDef);
      //    ComponentToTextFile(W, aFileName);
          S := ComponentToString(W);
          List.text := S;
          List.Delete(List.Count - 1);
          List.Delete(0);

//          Adm('with FieldDefs do begin', 2);
          Adm('FieldDef := FieldDefs.AddFieldDef;', 4);
            for i := 0 to List.Count - 1 do
            begin
              S := List[i];
              S := FastReplace(Trim(S), ' = ', ' := ') + ';';
              Adm(S, 4);
            end;
          Adm('',0);
          { veld }
          FName := FirstNice(Def.Name);
          Typ := aDef.FieldClass.ClassName;
          Adm(FName + ' := ' + Typ + '(FieldDef.CreateField(Self));', 4);

          Adm('',0);
//          loglist(list);
//          Log([S]);
        finally
          W.Free;
        end;
      finally
        List.Free;
      end;
      {
      object TFieldDefWrapper
        FieldDef.Name = 'FOTO_ID'
        FieldDef.Attributes = [faRequired]
        FieldDef.DataType = ftInteger
      end
      }
    end;

    procedure FieldToConstructor(aDef: TFieldDef);
    var
      F: TField;
      S: string;
      List: TStringList;
      i: Integer;
      FName: string;
      Typ: string;

    begin
      exit;
      List := TStringList.Create;
      try
        F := aDef.CreateField(nil);
        try
          S := ComponentToString(F);
          List.text := S;
          loglist(list);
          List.Delete(List.Count - 1); // end weg
          List.Delete(0); // object weg

          //FName := 'Fld' + FirstNice(Def.Name);
          FName := FirstNice(Def.Name);
          Typ := aDef.FieldClass.ClassName;

          //Adm(FName + ' := ' + Typ + '.Create(Self);', 4);
          Adm(FName + ' := ' + Typ + '(FieldDef.CreateField(Self));', 4);
          //Adm(FName + '.DataSet := fDataSet;', 4);

          for i := 0 to List.Count - 1 do
          begin
            S := List[i];
            S := FName + '.' + FastReplace(Trim(S), ' = ', ' := ') + ';';
            Adm(S, 4);
          end;
          
          Adm('',0);

        finally
          F.Free;
        end;
      finally
        List.Free;
      end;
    end;

    function GetTypeStr: string;
    begin
      Result := '';
      //Log([getenumname(typeinfo(tfieldtype), integer(def.datatype))]);
      case Def.DataType of
        //ftUnknown,
        ftString, ftBlob:
          Result := 'string';
        ftSmallint, ftInteger, ftWord:
          Result := 'Integer';
{        ftBoolean, }
        ftFloat:
          Result := 'Double';
        {, ftCurrency, ftBCD, }
        ftDate, ftTime, ftDateTime:
          Result := 'TDateTime';
        {ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo,
        ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString,
        ftLargeint, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
        ftVariant, ftInterface, ftIDispatch, ftGuid); }
      end;
    end;

var
  Longest: Integer;

begin


  if fDataSet = nil then
    Exit;

  L.Clear;
  M.Clear;


  DS := TIBDataSet.Create(nil);
  DS.Database := fDataSet.DataBase;
  DS.SelectSQL.Text := CurrentStatement;
//  windlg(ds.selectsql.text);

{
  if fDataSet.Active then
  begin
    WinDlg('dataset staat open: kan geen source schrijven voor ' + fdataset.name);
    Exit;
  end;

  if fDataSet.Prepared then
  begin
    WinDlg('dataset is prepared: kan geen source schrijven voor ' + fdataset.name);
    Exit;
  end; }

//  IsPrepared := fDataSet.Prepared;
  //if not IsPrepared then

//  TRY

  //fDataSet.Prepare;
  DS.Prepare;

  TRY

  with DS do
  begin
    {-------------------------------------------------------------------------------
      declaratie (met persistent fields).
    -------------------------------------------------------------------------------}
    Add('type', 0);
    Add(TypeName + ' = class(' + 'TDataSetExt)', 2);
    Add('protected', 2);
    Add('procedure CreatePersistentFields; override;', 4);
    Add('public', 2);
    Add('{ persistent fields }', 2);
    Longest := 0;
    for i := 0 to FieldDefs.Count - 1 do
      Longest := Max(Longest, Length(FieldDefs[i].Name));
    Inc(Longest);

    for i := 0 to FieldDefs.Count - 1 do
    begin
      Def := FieldDefs[i];
      TypeStr := GetTypeStr;
      Add(PadR(FirstNice(Def.Name), Longest) + ': ' + Def.FieldClass.ClassName + ';', 4);
    end;
    // values
    (*
    Add('{ values }', 2);
    for i := 0 to FieldDefs.Count - 1 do
    begin
      Def := FieldDefs[i];
      TypeStr := GetTypeStr;
      Add('property ' + FirstNice(Def.Name) + ': ' +
                        TypeStr + ' ' +
                        ' index ' + LeadZeroStr(Def.FieldNo - 1, 2) +
                        ' read ' + 'GetFieldAs' + TypeStr +
                        ' write SetFieldAs' + TypeStr + ';', 4);
    end;
    *)
    Add('end;', 2);

    {-------------------------------------------------------------------------------
      implementation
    -------------------------------------------------------------------------------}
    Adm('procedure ' + TypeName + '.CreatePersistentFields;', 0);
    Adm('var', 0);
    Adm('  FieldDef: TFieldDef;', 0);
    Adm('begin', 0);
    Adm('with DataSet do', 2);
    Adm('begin', 2);
    Adm('{  fielddefs }', 2);
    for i := 0 to FieldDefs.Count - 1 do
    begin
      Def := FieldDefs[i];
      FieldDefToConstructor(Def);
      //FieldToConstructor(Def);
    end;
    Adm('end;', 2);
    Adm('end;', 0);
    Adm('', 0);
  end;

  FINALLY
    DS.Free;//fDataSet.Unprepare;
  END;

(*  EXCEPT
    Windlg('fout bij schrijven source');
    raise;
  END; *)

end;

(*
procedure TDataSetExt.LogFieldDefs;
var
  i: Integer;
  Def: TFieldDef;
begin
exit;
  with fDataSet do
  begin
    for i := 0 to FieldDefs.Count - 1 do
    begin
      Def := FieldDefs[i];
      Log([i,
           Def.Name, getenumname(typeinfo(tfieldtype), integer(def.datatype)),
           def.size,
           def.required,
           def.precision
           ]);
    end;
  end;
  Log(['-------------']);
end;
*)

procedure TDataSetExt.SetDetailWhere(const aDetailWhere: string);
begin
  if fDetailWhere <> aDetailWhere then
  begin
    BeginUpdate;
    fDetailWhere := aDetailWhere;
    Include(fChanges, scDetailWhere);
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetQuickFilterFields(const Value: string);
var
  i, j: Integer;
  //S: string;
  //P: Integer;
  //PP: Integer;
  //Found : Boolean;
  NewList: TStringList;
begin
  NewList := TStringList.Create;
  BeginUpdate;
  try
    NewList.CommaText := Value;
    for i := 0 to NewList.Count - 1 do
    begin
      j := FindOrigin(NewList[i]);
      if j = -1 then AppError('SetFilterFields: ' + Value + ' ongeldig veldenreeks');
      NewList[i] := fOrigins[j];
    end;
    fQuickFilterFields.Sorted := False;
    fQuickFilterFields.Assign(NewList);
    fQuickFilterFields.Sorted := True;
    Include(fChanges, scQuickFilterFields);
  finally
    NewList.Free;
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetMaster(aDataSetExt: TDataSetExt; const aDetailField, aMasterField: string);
var
  S: string;
begin

  if aDataSetExt <> nil then
  begin
    //if (fMasterSource.DataSet <> aDataSetExt.fDataSet) or
    fMasterSource.DataSet := aDataSetExt.fDataSet;
    fDataSet.DataSource := fMasterSource;
//    Log(['-----------------']);
  //  Loglist(self.forigins);
    //Log(['-----------------']);
    //S := Self.FindOriginName(aDetailField) + ' = :' + aMasterField;
    //S := Self.FindOrigin(aDetailField) + ' = :' + aMasterField;
    S := aDetailField + ' = :' + aMasterField;
    SetDetailWhere(S);
  end
  else begin
    fMasterSource.DataSet := nil;
    fDataSet.DataSource := nil;
    SetDetailWhere('');
  end;
end;

procedure TDataSetExt.SetMaster(aDetailField, aMasterField: TField);
begin
  if aDetailField.DataSet = aMasterField.DataSet then
    AppError('setmaster');
    //### FOUT, moet origin zijn
  SetMaster(DatExt(aMasterField.DataSet), aDetailField.FieldName, aMasterField.FieldName);
end;

procedure TDataSetExt.SetOrderClause(const aOrder: string);
begin
  if CompareText(aOrder, fOrder) <> 0 then
  begin
    BeginUpdate;
    fOrder := aOrder;
    Include(fChanges, scOrder);
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetOrigins(const aOrigins: string);
var
  i: Integer;
  S: string;
  P: Integer;
begin
exit;
  fOrigins.CommaText := aOrigins;
  with fOrigins do
    for i := 0 to Count - 1 do
    begin
      S := Strings[i];
      P := AnsiPos(' AS ', S);
      if P > 0 then
      begin
        S := Copy(S, 1, P - 1);
        Strings[i] := S; // voorlopig even neppen
      end;
    end;
end;

procedure TDataSetExt.SetCustomWhere(const aWhere: string);
begin
  if CompareText(fCustomWhere, aWhere) <> 0 then
  begin
    BeginUpdate;
    fCustomWhere := aWhere;
    Include(fChanges, scCustomWhere);
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetOrderDirection(const Value: TOrder);
begin
  if fOrderDirection <> Value then
  begin
    BeginUpdate;
    fOrderDirection := Value;
    Include(fChanges, scOrderDirection);
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetSystemWhere(const Value: string);
begin
  if CompareText(fSystemWhere, Value) <> 0 then
  begin
    BeginUpdate;
    fSystemWhere := Value;
    Include(fChanges, scSystemWhere);
    EndUpdate;
  end;
end;

procedure TDataSetExt.UpdateWhereAndOrder(aChanges: TSqlChanges);
var
  S: string;
  KeyValue: Variant;//Integer;
  CTestWhere, {TestWhere, }LocalWhere, LocalOrder{, LocalDetailWhere}: string;
  PrevWhere: Boolean;


   {-------------------------------------------------------------------------------
      retourneert 1-based veldnummer
    -------------------------------------------------------------------------------}
    function CorrectOrder: String;
    var
      i: Integer;
    begin
      if CharPos(',', fOrder) > 0 then
      begin
        Result := fOrder;
        Exit;
      end;
      i := FindOrigin(fOrder);
      if i = -1 then
        AppErrorFmt('SetWhereAndOrder: veld of velden (%s)  bestaan niet', [fOrder]);
//      Result := fOrigins[i];
      Result := i2s(i + 1);
//      windlg(['order=',result])
//      Log([forder]);
    end;

    procedure SavePos;
    begin
      if fKeyField <> '' then
        KeyValue := fDataSet.FieldByName(fKeyField).Value;//AsInteger;
    end;

    procedure RestorePos;
    begin
      if fKeyField <> '' then
      begin
        fDataSet.Locate(fKeyField, KeyValue, []);
        if not fDataSet.Bof = fDataSet.Eof then
          if not fDataSet.Bof and not fDataSet.Eof then
            fDataSet.Resync([rmExact, rmCenter]);
      end;
    end;
begin

//  CTestOrder := '%SYS%DETAIL%CUSTOM%QUICK';
  CTestWhere := '%s%s%s%s';

  if fOrder <> '' then
  begin
    LocalOrder := 'ORDER BY ' + CorrectOrder;//aOrder;
    if CharPos(',', LocalOrder) > 0 then
      fOrderDirection := oAscending;
    case fOrderDirection of
      oDescending:
        LocalOrder := LocalOrder + ' ' + SDesc;
    end;
    //fOrderByClause.Text := 'ORDER BY ' +  LocalOrder;
  end;
  {if fOrderDirection = oascending then
    Log(['asc'])
  else if fOrderDirection = odescending then
    Log(['desc'])
  else Log(['?']);}

  if (fSystemWhere <> '') or (fDetailWhere <> '') or (fCustomWhere <> '') then
  begin
    LocalWhere := 'WHERE ';
    PrevWhere := False;

    if fSystemWhere <> '' then
    begin
      if PrevWhere then LocalWhere := LocalWhere + ' AND ' + CrLf;
      LocalWhere := LocalWhere + '(' + fSystemWhere + ')' + CrLf;
      PrevWhere := True;
    end;

    if fDetailWhere <> '' then
    begin
      if PrevWhere then LocalWhere := LocalWhere + ' AND ' + CrLf;
      LocalWhere := LocalWhere + '(' + fDetailWhere + ')' + CrLf;
      PrevWhere := True;
    end;

    if fCustomWhere <> '' then
    begin
      if PrevWhere then LocalWhere := LocalWhere + ' AND ' + CrLf;
      LocalWhere := LocalWhere + '(' + fCustomWhere + ')' + CrLf;
      //PrevWhere := True;
    end;
  end;

  S := FastReplace(fStatements[stSelect], SWhereClause, LocalWhere, False);
  S := FastReplace(S, SOrderByClause, LocalOrder, False);
//  windlg(S);

  {-------------------------------------------------------------------------------
    dataset reselect
  -------------------------------------------------------------------------------}
  if fDataSet.Active then
  begin
    if fDataSet.State in [dsEdit, dsInsert] then
      fDataSet.Post; /// ###EL  2005-10-17 toegevoegd
    fDataSet.DisableControls;
    try
      SavePos;
      fDataSet.Close;
      fDataSet.Unprepare;
      fDataSet.SelectSQL.Text := S;
      fDataSet.Prepare;
      Assert(FieldNamesOK, ClassName + '.UpdateWhereAndOrder error 1');
      //EnsureFieldExts;
      //EnsureFieldNames;
      fOrderByClause.Text := LocalOrder;
      fDataSet.Open;
//      SetFieldEvents;
//      SetFieldVisualDefaults;
      RestorePos;
    finally
      fDataSet.EnableControls;
    end;
  end
  else begin
//    if fDataSet.Prepared then
  //    fDataSet.UnPrepare;
    fDataSet.SelectSQL.Text := S;
{    if fDataSet.Database.Connected then
    begin
      fDataSet.Prepare;
      EnsureFieldExts;
      EnsureFieldNames;
    end; }
    fOrderByClause.Text := LocalOrder;
  end;

  if Assigned(fAfterSQLChange) then
    fAfterSQLChange(Self, aChanges);
end;

procedure TDataSetExt.UpdateQuickFilter;
var
  i: Integer;
  Where: string;
  CheckInts, CheckFloats, CheckDates: Boolean;
//  TempFloat: Extended;
  aDateStr: string;
  aNextDateStr: string;
  D: TDate;
//  odf:string;
  Fld: TField;
//  InvOr: string;
//
type
  THow = (Contains_Case, Contains_NoCase, Exact_Case, Exact_NoCase);
var
  How: THow;

var
  QuickFilters: array[0..1] of string;
  CurrFilter: Pstring;
  iCurrent: Integer;
  Wheres: array[0..1] of string;
  CurrWhere: Pstring;

begin
  BeginUpdate;
  try
    {-------------------------------------------------------------------------------
      als er geen velden in het filter staan dan exit
    -------------------------------------------------------------------------------}
    if fQuickFilterFields.Count = 0 then
      Exit;

    {-------------------------------------------------------------------------------
      bepaal "hoe" in een locale var
    -------------------------------------------------------------------------------}
    if fQuickExact then
    begin
      if fQuickCase then How := Exact_Case else How := Exact_NoCase;
    end
    else begin
      if fQuickCase then How := Contains_Case else How := Contains_NoCase;
    end;


    Wheres[0] := '';
    Wheres[1] := '';
    QuickFilters[0] := fQuickFilter;
    QuickFilters[1] := fQuickFilterEx;
{    if not fQuickInverse then
      InvOr := ' OR '
    else
      InvOr := ' AND NOT '; }

    {-------------------------------------------------------------------------------
      bepaal where clause
    -------------------------------------------------------------------------------}
    for iCurrent := 0 to 1 do
    begin
      CurrFilter := @QuickFilters[iCurrent];
      if CurrFilter^ = '' then
        Continue;
      CurrWhere := @Wheres[iCurrent];

      with fDataSet do
      begin
        {-------------------------------------------------------------------------------
          kijk welke veldtypen we naast stringvelden moeten opnemen in de where
        -------------------------------------------------------------------------------}
        CheckInts := (Length(CurrFilter^) <= 10) and StrAllInSet(CurrFilter^, DigitCharSet);
        CheckFloats := StrAllInSet(CurrFilter^, ['0'..'9', DecimalSeparator, '-']);
        CheckDates := True;
//        ODF := ShortDateFormat;    umisc
        try
          try
            D := StrToDate(CurrFilter^); // als exception dan geen checkdates
  //          ShortDateFormat := 'mm-dd-yyyy';
            aDateStr := '"' + DateToStr(D) + '"';
            aNextDateStr := '"' + DateToStr(D{ + 1}) + '"';
          except
            CheckDates := False;
          end;
        finally
    //      ShortDateFormat := ODF;
        end;

        {-------------------------------------------------------------------------------
          loop de velden door om op te nemen in de where
        -------------------------------------------------------------------------------}
        for i := 0 to FieldCount - 1 do
        begin
          Fld := Fields[i];
          with Fld do
          begin
            EnsureOrigin(Fld);
            if (Origin <> '$$$') and (fQuickFilterFields.IndexOf(Origin) >= 0) then
            case DataType of
              ftString, ftBlob:
                begin
                  if CurrWhere^ <> '' then CurrWhere^ := CurrWhere^ + ' OR ' + CrLf;
                  CurrWhere^ := CurrWhere^ + '(';
                  case How of
                    Contains_Case   : CurrWhere^ := CurrWhere^ + Origin + ' LIKE ' + '"' + '%' + CurrFilter^ + '%' + '"';
                    Contains_NoCase : CurrWhere^ := CurrWhere^ + Origin + ' CONTAINING ' + '"' + CurrFilter^ + '"';
                    Exact_Case      : CurrWhere^ := CurrWhere^ + Origin + ' = ' + '"' + CurrFilter^ + '"';
                    Exact_NoCase    : CurrWhere^ := CurrWhere^ + 'UPPER(' + Origin + ')' + ' = ' + '"' + Uppercase(CurrFilter^) + '"';
                  end;
                  CurrWhere^ := CurrWhere^ + ')';
                end;
              ftInteger:
                if CheckInts then
                begin
                  if CurrWhere^ <> '' then CurrWhere^ := CurrWhere^ + ' OR ' + CrLf;
                  CurrWhere^ := CurrWhere^ + '(';
                  case How of
                    Contains_Case   : CurrWhere^ := CurrWhere^ + 'CAST(' + Origin +  ' AS VARCHAR(10))'+ ' CONTAINING ' + '"' + CurrFilter^ + '"';
                    Contains_NoCase : CurrWhere^ := CurrWhere^ + 'CAST(' + Origin +  ' AS VARCHAR(10))'+ ' CONTAINING ' + '"' + CurrFilter^ + '"';
                    Exact_Case      : CurrWhere^ := CurrWhere^ + Origin + ' = ' + CurrFilter^;
                    Exact_NoCase    : CurrWhere^ := CurrWhere^ + Origin + ' = ' + CurrFilter^;
                  end;
                  CurrWhere^ := CurrWhere^ + ')';
                end;
              ftFloat:
                if CheckFloats then
                begin
                  if CurrWhere^ <> '' then CurrWhere^ := CurrWhere^ + ' OR ' + CrLf;
                  CurrWhere^ := CurrWhere^ + '(';
                  case How of
                    Contains_Case   : CurrWhere^ := CurrWhere^ + 'GetFixedPoint(' + Origin +  ', 2, ' + '"' + DecimalSeparator + '")'+ ' CONTAINING ' + '"' + CurrFilter^ + '"';
                    Contains_NoCase : CurrWhere^ := CurrWhere^ + 'GetFixedPoint(' + Origin +  ', 2, ' + '"' + DecimalSeparator + '")'+ ' CONTAINING ' + '"' + CurrFilter^ + '"';
                    Exact_Case      : CurrWhere^ := CurrWhere^ + Origin + ' = ' + CurrFilter^;
                    Exact_NoCase    : CurrWhere^ := CurrWhere^ + Origin + ' = ' + CurrFilter^;
                  end;
                  CurrWhere^ := CurrWhere^ + ')';
                end;
              ftDateTime:
                if CheckDates then
                begin
                  //UDF gebruiken
                  if CurrWhere^ <> '' then CurrWhere^ := CurrWhere^ + ' OR ' + CrLf;
                  CurrWhere^ := CurrWhere^ + '(';
                  CurrWhere^ := CurrWhere^ + Origin + ' BETWEEN ' + aDateStr + ' AND ' + aNextDateStr;
                  CurrWhere^ := CurrWhere^ + ')';
                end;
            end; // case DataType
          end; // with Fld
        end; //for i
      end; // with DataSet
    end; // for Current

    {-------------------------------------------------------------------------------
      plak de twee quickfilters aan elkaar
    -------------------------------------------------------------------------------}
    Where := '';
    if Wheres[0] <> '' then
    begin
      Where := Wheres[0];
    end;
    if Wheres[1] <> '' then
    begin
      if Wheres[0] <> '' then
        Where := '(' + Where + ')' + CrLf + ' ' + SAndOr[fQuickAndOr] + ' ' + CrLf + '(' + Wheres[1] + ')'
      else
        Where := Wheres[1];
      //Log(['asdf']);
    end;

    {-------------------------------------------------------------------------------
      evt. inverse
    -------------------------------------------------------------------------------}
    if fQuickInverse then
      if Where <> '' then
        Where := 'NOT (' + CrLf + Where + CrLf + ')';
    SetCustomWhere(Where);

  finally
    EndUpdate;
  end;
end;

procedure TDataSetExt.BeginUpdate;
begin
  Inc(fUpdateLock);
end;

procedure TDataSetExt.EndUpdate;
begin
  if fUpdateLock > 0 then
  begin
    Dec(fUpdateLock);
  if fUpdateLock = 0 then
    Update;
  end;
end;

procedure TDataSetExt.SetQuickCase(const Value: Boolean);
begin
  if fQuickCase <> Value then
  begin
    BeginUpdate;
    fQuickCase := Value;
    Include(fChanges, scQuickCase);
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetQuickExact(const Value: Boolean);
begin
  if fQuickExact <> Value then
  begin
    BeginUpdate;
    fQuickExact := Value;
    Include(fChanges, scQuickExact);
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetQuickFilter(const Value: string);
begin
  if fQuickFilter <> Value then
  begin
    BeginUpdate;
    fQuickFilter := Value;
    Include(fChanges, scQuickFilter);
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetQuickFilterEx(const Value: string);
begin
  if fQuickFilterEx <> Value then
  begin
    BeginUpdate;
    fQuickFilterEx := Value;
    Include(fChanges, scQuickFilter);
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetQuickAndOr(const Value: TQuickAndOr);
begin
  if fQuickAndOr <> Value then
  begin
    BeginUpdate;
    fQuickAndOr := Value;
    Include(fChanges, scQuickAndOr);
    EndUpdate;
  end;
end;

procedure TDataSetExt.SetQuickInverse(const Value: Boolean);
begin
  if fQuickInverse <> Value then
  begin
    BeginUpdate;
    fQuickInverse := Value;
    Include(fChanges, scQuickInverse);
    EndUpdate;
  end;
end;

procedure TDataSetExt.Update;
var
  SaveChanges: TSqlChanges;
begin
  if fChanges = [] then
    Exit;
  SaveChanges := fChanges;
  fChanges := [];
  if [scSystemWhere, scCustomWhere, scOrder, scOrderDirection, scDetailWhere] * SaveChanges <> [] then
    UpdateWhereAndOrder(SaveChanges)
  else if [scQuickFilter, scQuickExact, scQuickCase, scQuickFilterFields, scQuickAndOr, scQuickInverse] * SaveChanges <> [] then
    UpdateQuickFilter;
end;

function TDataSetExt.GetOrderField: string;
begin
  with fOrderByClause do
    if Count = 1 then
      Result := Items[0].fFieldName;//Origin;
end;

procedure TDataSetExt.RefreshRecord;
//var
  //i: Integer;
begin
  (*
  with fRecordSQL do
  begin
    Database := fDataSet.Database;
    Transaction := fDataSet.Transaction;
    Params[0].AsInteger := fDataSet.FieldByName(fKeyField).AsInteger;
    ExecQuery;
    //kopieer velden
//    for i := 0 to Current.Count - 1 do
  //    Log([Fields[i].AsString, Fields[i].Name]);
    Close;
  end;
  *)
end;


function TDataSetExt.GetPosition: Integer;
begin
  Result := fDataSet.FieldByName(fKeyField).AsInteger;
end;

procedure TDataSetExt.SetPosition(P: Integer);
begin
  fDataSet.Locate(fKeyField, P, []);
  if not fDataSet.Bof = fDataSet.Eof then
    if not fDataSet.Bof and not fDataSet.Eof then
      fDataSet.Resync([rmExact, rmCenter]);
end;

function TDataSetExt.FindFieldName(const aOrigin: string): string;
var
  i: Integer;
begin
//  Ensure
  Result := '';
  i := fOrigins.IndexOf(aOrigin);
  if i < 0 then
    Exit;
//    AppError('FindFieldName (' + aOrigin + ')');
  if i < fFieldNames.Count then
    Result := fFieldNames[i]
  else Log([i, fFieldNames.Count]);
end;

procedure TDataSetExt.Test;
begin
  with fWhereClause do
  begin
    fWhereClause.BeginUpdate; // belangrijk!!!???
    with Add do
    begin
      Text := 'detailveld="detail"';
      WhereType := wtDetail;
    end;
    with Add do
    begin
      Text := 'quickveld="quick"';         //classes     contnrs
      WhereType := wtQuick;
    end;
    with Add do
    begin
      Text := 'customveld="custom"';
      WhereType := wtCustom;
    end;
    with Add do
    begin
      Text := 'sysveld="sys"';
      WhereType := wtSystem;
    end;
    fWhereClause.EndUpdate; // belangrijk!!!???
    Log(['aantal items', Count]);
    Log([text]);
    Log(['---']);
  end;
end;

function TDataSetExt.FindFieldExt(const aFieldName: string): TFieldExt;
var
  i: integer;
  Ext: TFieldExt;
begin
//  Result := nil;
//  Log([ffieldextlist.count, 'startzoek', afieldname]);

  with fFieldExtList do
    for i := 0 to Count - 1 do
    begin
      Ext := TFieldExt(fFieldExtList[i]);
      //Log([i, 'zoek', afieldname]);
      if CompareText(Ext.DataField, aFieldName) = 0 then
      begin
        Result := Ext;
        //Log([i, 'gevonden', afieldname, ext.datafield]);
        Exit;
      end;
    end;

  //Log(['zoek niet gevonden']);
  Result := nil;
end;

procedure TDataSetWrapper.Post;
begin
  fDataSet.Post;
end;

procedure TDataSetWrapper.Prior;
begin
  fDataSet.Prior;
end;

procedure TDataSetWrapper.SetBookmarkStr(const Value: TBookmarkStr);
begin
  fDataSet.Bookmark := Value;
end;

function TDataSetExt.FieldNamesOK: Boolean;
var
  i: Integer;
begin
  { check als dataset open, zie updatewhereandorder }
  Result := True;
  if fFieldNames.Count <> fDataSet.FieldDefs.Count then
  begin
    Result := False;
    windlg([ClassName +
      'FieldNamesOK error: aantal fieldnames is niet gelijk aan aantal datasetvelden',
      fFieldNames.Count, fDataSet.FieldDefs.Count]);
    Exit;
  end;
  for i := 0 to fFieldNames.Count - 1 do
  begin
    if CompareText(fDataSet.FieldDefs[i].Name, fFieldNames[i]) <> 0 then
    begin
      Result := False;
      Windlg([
        'FieldNamesOK error: fieldnames komt niet overeen met datasetfieldname',
        fDataSet.FieldDefs[i].Name,
        fFieldNames[i]]);
      Exit;
    end;
  end;
end;

function TDataSetExt.GetStatement(Index: TSelectType): string;
begin
  Result := fStatements[Index];
end;


(*
procedure TDataSetExt.InitNames(const aOrigins: string);
var
  Temp: TStringList;
  i: Integer;
  S, Org, Alias, Typ: string;
  AsPos: Integer;
  TypePos: Integer;
  Ext: TFieldExt;
  AliasCopy: Integer;
begin

  // dit zootje nog even duidelijker maken
  Temp := TStringList.Create;

  SplitString_To_StringList(aOrigins, Temp, ',', True);
  //Log(['------------TEMPLIST']);
  //loglist(temp);
  //Temp.CommaText := aOrigins;

//  loglist(temp);

  try
    with Temp do
      for i := 0 to Count - 1 do
      begin
        S := Strings[i];
        S := FastReplace(S, #13, '');
        S := FastReplace(S, #10, '');
//        windlg(aOrigins);
        AsPos := FastPosNoCase(S, ' AS ', Length(S), 4, 1);
        if AsPos <= 0 then
          raise EDataSetExtError.Create('Ontbrekende Alias in ' + S);


        TypePos := FastPosNoCase(S, ' TYPE ', Length(S), 6, AsPos + 4);


        Org := Trim(Copy(S, 1, AsPos - 1)); // origin
        Assert(Pos('.', Org) > 0, Self.ClassName + '.InitNames Ontbrekende Origin');


        AliasCopy := Length(S) - Length(Org) - TypePos - 1;
//        if AliasCopy = 0 then AliasCopy := Length(S);

        Alias := Trim(Copy(S, AsPos + 4, AliasCopy{Length(S)})); // fieldname
        Assert(Pos(' ', Alias) = 0, Self.ClassName + '.InitNames Spatie in alias');

        Typ := '';
        if TypePos > 0 then
          Typ := Trim(Copy(S, TypePos + 6, Length(S)));

//       if alias = 'fotomap_'
  //     if typ <> '' then
        log([org, alias, Typ]);

        //windlg([s, '', 'org', org, 'alias', alias]);
        { maak origin, fieldname en fieldext aan }
        fOrigins.Add(Org);
        fFieldNames.Add(UpperCase(Alias));
        //Log([ffieldnames.count, alias]);
        Ext := TFieldExt.Create(Self);
        fFieldExtList.Add(Ext);
        Ext.fIndex := i;
        Ext.fPrompt := Alias;
        Ext.fDataField := Alias;
      end;
      {Log(['------------ORIGINS']);
      loglist(forigins);
      Log(['------------FIELDNAMES']);
      loglist(ffieldnames);}
  finally
    Temp.Free;
  end;

end; *)

end.


