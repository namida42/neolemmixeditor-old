object MatrixInspectorForm: TMatrixInspectorForm
  Left = 107
  Top = 168
  BorderStyle = bsDialog
  Caption = 'MatrixInspectorForm'
  ClientHeight = 300
  ClientWidth = 417
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 417
    Height = 300
    ActivePage = PageTab
    Align = alClient
    TabOrder = 0
    object PageTab: TTabSheet
      Caption = 'Page'
      object Bevel2: TBevel
        Left = 4
        Top = 48
        Width = 404
        Height = 224
        Shape = bsFrame
      end
      object MasterLabel2: TStaticText
        Left = 176
        Top = 118
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LastVisibleRow'
        TabOrder = 0
      end
      object MasterLabel3: TStaticText
        Left = 8
        Top = 158
        Width = 92
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VisibleCols'
        TabOrder = 1
      end
      object MasterLabel4: TStaticText
        Left = 176
        Top = 158
        Width = 92
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'VisibleRows'
        TabOrder = 2
      end
      object MasterLabel5: TStaticText
        Left = 8
        Top = 224
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'FullVisibleCols'
        TabOrder = 3
      end
      object MasterLabel6: TStaticText
        Left = 176
        Top = 224
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'FullVisibleRows'
        TabOrder = 4
      end
      object MasterLabel8: TStaticText
        Left = 8
        Top = 244
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'GridRight'
        TabOrder = 5
      end
      object MasterLabel9: TStaticText
        Left = 176
        Top = 244
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'GridBottom'
        TabOrder = 6
      end
      object MasterLabel10: TStaticText
        Left = 8
        Top = 138
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LastFullVisibleCol'
        TabOrder = 7
      end
      object MasterLabel11: TStaticText
        Left = 176
        Top = 138
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LastFullVisibleRow'
        TabOrder = 8
      end
      object MasterLabel12: TStaticText
        Left = 8
        Top = 56
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Col'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 9
      end
      object MasterLabel13: TStaticText
        Left = 176
        Top = 56
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Row'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
      end
      object MasterLabel1: TStaticText
        Left = 8
        Top = 118
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LastVisibleCol'
        TabOrder = 11
      end
      object LastCol: TStaticText
        Left = 112
        Top = 118
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 12
      end
      object LastRow: TStaticText
        Left = 280
        Top = 118
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 13
      end
      object VisibleCols: TStaticText
        Left = 112
        Top = 158
        Width = 54
        Height = 18
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 14
      end
      object VisibleRows: TStaticText
        Left = 280
        Top = 158
        Width = 54
        Height = 18
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 15
      end
      object FullVisibleCols: TStaticText
        Left = 112
        Top = 224
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 16
      end
      object FullVisibleRows: TStaticText
        Left = 280
        Top = 224
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 17
      end
      object GridRight: TStaticText
        Left = 112
        Top = 244
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 18
      end
      object GridBottom: TStaticText
        Left = 280
        Top = 244
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 19
      end
      object LastFullVisibleCol: TStaticText
        Left = 112
        Top = 138
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 20
      end
      object LastFullVisibleRow: TStaticText
        Left = 280
        Top = 138
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 21
      end
      object Col: TStaticText
        Left = 112
        Top = 56
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 22
      end
      object Row: TStaticText
        Left = 280
        Top = 56
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 23
      end
      object StaticText1: TStaticText
        Left = 8
        Top = 77
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'FirstCol'
        TabOrder = 24
      end
      object FirstCol: TStaticText
        Left = 112
        Top = 77
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 25
      end
      object StaticText3: TStaticText
        Left = 176
        Top = 77
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'FirstRow'
        TabOrder = 26
      end
      object FirstRow: TStaticText
        Left = 280
        Top = 77
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 27
      end
      object TxtVisibleRows: TStaticText
        Left = 48
        Top = 0
        Width = 356
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 28
      end
      object StaticText2: TStaticText
        Left = 8
        Top = 0
        Width = 31
        Height = 17
        Caption = 'Rows'
        TabOrder = 29
      end
      object StaticText4: TStaticText
        Left = 8
        Top = 24
        Width = 24
        Height = 17
        Caption = 'Cols'
        TabOrder = 30
      end
      object TxtVisibleCols: TStaticText
        Left = 48
        Top = 24
        Width = 358
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 31
      end
      object StaticText5: TStaticText
        Left = 8
        Top = 98
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'OffsetCol'
        TabOrder = 32
      end
      object OffsetCol: TStaticText
        Left = 112
        Top = 98
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 33
      end
      object StaticText7: TStaticText
        Left = 176
        Top = 98
        Width = 92
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'OffsetRow'
        TabOrder = 34
      end
      object OffsetROw: TStaticText
        Left = 280
        Top = 98
        Width = 54
        Height = 17
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 35
      end
      object StaticText6: TStaticText
        Left = 8
        Top = 179
        Width = 92
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'HiddenCols'
        TabOrder = 36
      end
      object TxtHiddenCols: TStaticText
        Left = 112
        Top = 179
        Width = 54
        Height = 18
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 37
      end
      object StaticText9: TStaticText
        Left = 176
        Top = 179
        Width = 92
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'HiddenRows'
        TabOrder = 38
      end
      object TxtHiddenRows: TStaticText
        Left = 280
        Top = 179
        Width = 54
        Height = 18
        AutoSize = False
        BorderStyle = sbsSunken
        Caption = '-'
        Color = clWhite
        ParentColor = False
        TabOrder = 39
      end
    end
    object TabSheet2: TTabSheet
      Caption = '-'
      ImageIndex = 1
    end
  end
end
