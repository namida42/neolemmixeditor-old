unit UClock;

interface

uses controls,classes,sysutils,graphics,extctrls,messages,math,stdctrls;

type

  TClockTime = class(TPersistent)
  private
    fHour: word;
    fMinute: word;
  published
    property Hour: word read fHour write fHour;
    property Minute: word read fMinute write fMinute;
  end;

  TClock=class(TCustomControl{TGraphicContro})
  private
//    function GetDiameter: integer;
    procedure SetBorderColor(const Value: TColor);
    procedure SetCaptionFont(const Value: TFont);
    procedure SetFaceColor(const Value: TColor);
    procedure SetMarkerColor(const Value: TColor);
    procedure Setcaption(const Value: String);
    function GetCaptionFont: TFont;
    procedure SetDiameter(const Value: integer);
    procedure SetShowAnalog(const Value: boolean);
    procedure SetShowDigital(const Value: boolean);
    procedure SetShowCaption(const Value: boolean);

    private
      Fcaption:string;
      Ftijd:tdatetime;
      Falarm:tdatetime;
      FalarmOn:boolean;
      FOffset: integer;
      FDiameter: integer;
      FSecond  : word;
      FMinute  : word;
      FHour    : word;
      Ftimer   : ttimer;
      Fteksthoog: integer;
      fShowSeconds: boolean;
      fFaceColor: TColor;
      fMarkerColor: TColor;
      fSecondPointerColor: TColor;
      fBorderColor: TColor;
      fBorderWidth: integer;
      fMinutePointerColor: TColor;
      fHourPointerColor: TColor;
      FAlarmTime: TClockTime;
//      FWorkingHours: TClockTime;
      FWorkingHoursShow: TClockTime;
      FWorkingHoursEnd: TClockTime;
      FWorkingHoursStart: TClockTime;
      fHourPointerWidth: integer;
      fSecondPointerWidth: integer;
      fMinutePointerWidth: integer;
      fBulletSize: integer;
    { intern }
//      fcalchpw:integer;
//      fcalcmpw:integer;
//      fcalcspw:integer;
//      fcalcbw:integer;
//      fcalcbs:integer;
//      fcalcdiameter:integer;
      flabel:tlabel;
      fshowcaption:boolean;
      fshowanalog:boolean;
      fshowdigital:boolean;

//      fcaptionfont:tfont;
//      workinghours:timage;
//      moon:timage;
//      sun:timage;
//      alarm:timage;
      procedure TimerEvent(sender:tobject);
      procedure HourPointer(Hour,Min: integer; toon: boolean);
      procedure MinutePointer(Minute: integer; toon: boolean);
      procedure SecondPointer(Second: integer; toon: boolean);
      function GetCentrumX: integer;
      function GetCentrumY: integer;
      procedure SetShowSeconds(const Value: boolean);
      procedure WMEraseBkgnd(var Message: TWmEraseBkgnd); message WM_ERASEBKGND;
      procedure SetBorderWidth(const Value: Integer);
      function calcBs : Integer;
      function calcBw :  Integer;
      function calcHpw: integer;
      function calcMpw: integer;
      function calcSpw: integer;
      function calcdiameter: integer;
    procedure SetBulletSize(const Value: integer);
    procedure SetHourPointerColor(const Value: TColor);
    procedure SetHourPointerWidth(const Value: integer);
    procedure SetMinutePointerColor(const Value: TColor);
    procedure SetMinutePointerWidth(const Value: integer);
    procedure SetSecondPointerColor(const Value: TColor);
    procedure SetSecondPointerWidth(const Value: integer);
    procedure clockupdate(nu,ou,nm,om,ns,os:integer);
    protected
      procedure Resize; override;
      procedure paint;override;
    public
      constructor create(Aowner:tcomponent);override;
      destructor destroy;override;
       property  CentrumX:integer read GetCentrumX;
       property  CentrumY:integer read GetCentrumY;
    published
       property  Offset:integer read FOffset write FOffset;
       property  Color;
       property  Width;
       property  Height;
       property  Diameter:integer read FDiameter write SetDiameter;
       property  CaptionFont:TFont read GetCaptionFont write SetCaptionFont;
       property  FaceColor: TColor read fFaceColor   write SetFaceColor;
       property  BorderColor: TColor read fBorderColor    write SetBorderColor;
       property  BorderWidth: Integer read fBorderWidth  write SetBorderWidth;
       property  MarkerColor: TColor read fMarkerColor    write SetMarkerColor;
       property  HourPointerColor: TColor read fHourPointerColor    write SetHourPointerColor;
       property  HourPointerWidth: integer read fHourPointerWidth    write SetHourPointerWidth;
       property  MinutePointerColor: TColor read fMinutePointerColor    write SetMinutePointerColor;
       property  MinutePointerWidth: integer read fMinutePointerWidth    write SetMinutePointerWidth;
       property  SecondPointerColor: TColor read fSecondPointerColor    write SetSecondPointerColor;
       property  SecondPointerWidth: integer read fSecondPointerWidth    write SetSecondPointerWidth;
       property  BulletSize: integer read fBulletSize    write SetBulletSize;
       property  ShowSeconds:boolean read fShowSeconds    write SetShowSeconds;
       property  Caption: String read Fcaption write Setcaption;
       property  AlarmOn  :boolean read fAlarmOn write fAlarmOn;
       property  ShowCaption  :boolean read fShowCaption write SetShowCaption;
       property  ShowAnalog   :boolean read fShowAnalog write SetShowAnalog;
       property  ShowDigital  :boolean read fShowDigital write SetShowDigital;
       property  WorkingHoursShow : TClockTime read FWorkingHoursShow  write FWorkingHoursShow;
       property  WorkingHoursStart : TClockTime read FWorkingHoursStart  write FWorkingHoursStart;
       property  WorkingHoursEnd : TClockTime read FWorkingHoursEnd  write FWorkingHoursEnd;
       property  AlarmTime: TClockTime read FAlarmTime write FAlarmTime;
  end;

//procedure Register;

implementation

{procedure Register;
begin
  RegisterComponents('Master', [TClock]);
end;
}
{ TClock }
constructor TClock.create(Aowner: tcomponent);
var dummy:word;
begin
  inherited;
//  fclock.ondest
  ftijd:=time;
  DecodeTime(ftijd,fhour,fminute,fsecond,dummy);
  falarm:=encodetime(7,30,0,0);
  offset:=0;
  height:=100;
  width:=100;
  FCaption:='London';
  fteksthoog:=18;
  //image

  Ftimer:=ttimer.create(self);
  flabel:=tlabel.create(self);
  flabel.caption:=fcaption;
  flabel.parent:=self;

//  fcaptionfont:=tfont.create;
//  fcaptionfont.Style:=[fsbold];
//  flabel.font.assign(fcaptionfont);
  captionfont.Style:=[fsbold];
  showcaption:=true;
  showanalog:=true;
  showdigital:=false;
  

(*
  workinghours:=timage.create(self);
  workinghours.autosize:=true;
  workinghours.Picture.LoadFromFile('d:d5\master\plaatjes\workhours.bmp');
  workinghours.Transparent:=true;
  workinghours.Top:=height-fteksthoog-16;
  workinghours.Anchors:=[akleft,akbottom];
  workinghours.show;
  workinghours.parent:=self;//twincontrol(owner){self};

  moon:=timage.create(self);
  moon.autosize:=true;
  moon.Picture.LoadFromFile('d:d5\master\plaatjes\moon.bmp');
  moon.Transparent:=true;
  moon.Anchors:=[akleft,aktop];
  moon.show;
  moon.parent:=self;//twincontrol(owner){self};

  Sun:=timage.create(self);
  Sun.autosize:=true;
  Sun.Picture.LoadFromFile('d:d5\master\plaatjes\sun.bmp');
  Sun.Transparent:=true;
  Sun.Left:=width-16;
  sun.Anchors:=[akright,aktop];
  Sun.show;
  Sun.parent:=self;//twincontrol(owner){self};

  Alarm:=timage.create(self);
  Alarm.autosize:=true;
  Alarm.Picture.LoadFromFile('d:d5\master\plaatjes\alarm.bmp');
  Alarm.Anchors:=[akRight,akbottom];
  Alarm.Left:=width-16;
  Alarm.Top:=height-fteksthoog-16;
  Alarm.Transparent:=true;
  Alarm.show;
  Alarm.parent:=self;//twincontrol(owner){self};
*)
  ftimer.OnTimer:=timerevent;
  if csDesigning in ComponentState then
    Ftimer.enabled := false
  else
    Ftimer.enabled := True;
//  Ftimer.interval:=30000;
  ShowSeconds:=true;
  FaceColor:=clwhite;//clbtnface;
  fBorderColor:=clblack;
  fHourPointerColor:=clLime;
  fMinutePointerColor:=clBlue;
  fSecondPointerColor:=clred;
  BorderWidth:=-1;
  HourPointerWidth:=-1;
  MinutePointerWidth:=-1;
  SecondPointerWidth:=-1;
  Diameter:=-1;
  BulletSize:=-1;
end;

destructor TClock.destroy;
begin
  inherited;
  //fcaptionfont.free;
end;

function TClock.GetCentrumX: integer;
begin
  Result:=width div 2;
end;

function TClock.GetCentrumY: integer;
var th:integer;
begin
  if showcaption then th:=fteksthoog else th:=0;
  Result:={round(16*0.707)+}(height-th) div 2;
//  result:=diameter div 2;
end;



procedure TClock.HourPointer(Hour,Min: integer;toon:boolean);
var
  hourgra:integer;
  hourrad:real;
  deltax,deltay:integer;
begin
  begin
  hour:=hour+offset;
  if hour<0 then hour:=hour+12;
  if hour>12 then hour:=hour-12;
  if toon then canvas.Pen.color:=fHourPointerColor
          else canvas.Pen.color:=fFaceColor;
  HourGra := 30 * Hour+round(min/60*30); // uren+stukje van minuten
  HOurRad := pi - (HourGra / 180) * pi;
  DeltaX := trunc(sin(HourRad) * (CalcDiameter / 2 *0.40)); // Diameter = afmeting in pixels van de klok
  DeltaY := trunc(cos(HourRad) * (CalcDiameter / 2* 0.40));
  canvas.moveto(CentrumX, CentrumY); // x en y co�rdinaat van het centrum van de klok
  Canvas.pen.width:=calchpw;
  Canvas.LineTo(CentrumX + DeltaX, CentrumY + DeltaY);
//  Canvas.pen.width:=1;
  end;
end;

procedure TClock.MinutePointer(Minute: integer;toon:boolean);
var
  mingra:integer;
  minrad:real;
  deltax,deltay:integer;
begin
  if toon then canvas.Pen.color:=fMinutePointerColor
          else canvas.Pen.color:=fFaceColor;
  minGra := 6 * Minute; // 6 graden per seconde
  minRad := pi - (MinGra / 180) * pi;
  DeltaX := trunc(sin(minRad) * (CalcDiameter / 2 *0.65));
  DeltaY := trunc(cos(minRad) * (CalcDiameter / 2 *0.65));
  Canvas.pen.width:=calcmpw;
  canvas.moveto(CentrumX, CentrumY); // x en y co�rdinaat van het centrum van de klok
  Canvas.LineTo(CentrumX + DeltaX, CentrumY + DeltaY);
end;

procedure TClock.SecondPointer(Second: integer;toon:boolean);
var
  secgra:integer;
  secrad:real;
  deltax,deltay:integer;
begin
  if toon then canvas.Pen.color:=fSecondPointerColor
          else canvas.Pen.color:=fFaceColor;
  secGra := 6 * Second; // 6 graden per seconde
  secRad := pi - (secGra / 180) * pi;
  DeltaX := trunc(sin(secRad) * (CalcDiameter / 2 *0.70));
  DeltaY := trunc(cos(secRad) * (CalcDiameter / 2 *0.70));
  Canvas.pen.width:=calcspw;
  canvas.moveto(CentrumX, CentrumY); // x en y co�rdinaat van het centrum van de klok
  Canvas.LineTo(CentrumX + DeltaX, CentrumY + DeltaY);

end;

procedure TClock.paint;
var rad:real;
    gra,i,straal,dx,dy:integer;
begin
//log([diameter]);
  with canvas do
  begin
    brush.color:=self.color;
    pen.color:=self.color;//btnface;
    Rectangle(0,0,width,height);
     if showcaption then flabel.show
                    else flabel.Hide;
      Font.Color:=clblack;
      flabel.left:=(width-flabel.canvas.textwidth(flabel.caption)) div 2;
      flabel.top:=height-fteksthoog+2;
//   exit;
//  exit;
    pen.color:=fBorderColor;
// show digital


//show analog

    straal:=round(CalcDiameter*1.00/2);
    begin
      brush.color:=fFaceColor;
      pen.width:=calcbw;
      Ellipse(centrumX-straal,centrumY-straal,CentrumX+straal,CentrumY+straal);
      straal:=round(calcdiameter*0.85/2);
      pen.color:=fMarkerColor;
      for i:=0 to 11 do
      begin
        Gra := 30 * i; // 30 graden per 5 minuten
        Rad := pi - (gra / 180) * pi;
        Dx := trunc(sin(Rad) * straal); // Diameter = afmeting in pixels van de klok
        Dy := trunc(cos(Rad) * straal);
        if i mod 3=0 then pen.width:=calchpw
                     else canvas.pen.width:=calcspw;

//   moveto(CentrumX, CentrumY);

        canvas.MoveTo(CentrumX + Dx, CentrumY + Dy);
        canvas.LineTo(CentrumX + round(Dx*0.90),centrumY +round(Dy*0.90));
//   LineTo(CentrumX + Dx, CentrumY + Dx);
    end; //for

    HourPointer(Fhour,Fminute,true);
    MinutePointer(Fminute,true);
    if fShowSeconds then SecondPointer(FSecond,true);
   brush.color:=fBorderColor;
    Ellipse(centrumX-calcbs,centrumY-calcBS,CentrumX+calcBS,CentrumY+calcBS);
//   log(['paint']);
  end; //with
  end;
end;

procedure TClock.Resize;
//var r:integer;
begin
  inherited;

//  diameter:=round((sqrt(sqr((width/2)-16)*2)));
//  Diameter :=round((sqrt(sqr(width/2)*2)-22.627));
//  log([diameter]);
end;

procedure TClock.SetShowSeconds(const Value: boolean);
begin
  fShowSeconds := Value;
  if fShowSeconds then ftimer.Interval:=100 else ftimer.interval:=10000;
end;

procedure TClock.clockupdate(nu,ou,nm,om,ns,os:integer);
//var dummy:word;
begin
//  log([os,ns]);
  if ou>12 then ou:=ou-12;
  if nu>12 then nu:=nu-12;
//wissen
  HourPointer(ou,om,false);
  MinutePointer(om,false);
  if fShowSeconds then SecondPointer(os,false);
//herstellen
  HourPointer(nu,nm,true);
  MinutePointer(Fminute,true);
  if fShowSeconds then SecondPointer(ns,true);
   canvas.brush.color:=fBorderColor;
    canvas.Ellipse(centrumX-calcbs,centrumY-calcBS,CentrumX+calcBS,CentrumY+calcBS);
//  if (ns mod 2=0) then alarm.hide else alarm.show;

//  invalidate;
end;

procedure TClock.TimerEvent(sender: tobject);
var dummy:word;uur,min,sec:word;
begin
  decodetime(ftijd,uur,min,sec,dummy);
  fTijd:=Time();
  DecodeTime(ftijd,fhour,fminute,fsecond,dummy);
//  fhour:=fhour+offset;
//  log([fsecond,sec]);
  if fsecond<>sec then
    clockupdate(fhour,uur,fminute,min,fsecond,sec);
end;

procedure TClock.WMEraseBkgnd(var Message: TWmEraseBkgnd);
begin
  Message.Result := 0;
end;

procedure TClock.SetBorderWidth(const Value: Integer);
begin
  fBorderWidth := Value;
  Invalidate;
end;

procedure TClock.SetHourPointerWidth(const Value: integer);
begin
  fHourPointerWidth := Value;
  Invalidate;
end;

procedure TClock.SetMinutePointerWidth(const Value: integer);
begin
  fMinutePointerWidth := Value;
  Invalidate;
end;

procedure TClock.SetSecondPointerWidth(const Value: integer);
begin
  fSecondPointerWidth := Value;
  Invalidate;
end;

function TClock.calcBS: integer;
begin
  if fBulletSize<1 then Result:=(calcdiameter div 80)+1
  else result:=fBulletSize;
end;

function TClock.calcdiameter: integer;
var wr,hr:integer;
    th,h2,d,a1,a2:integer;
begin
 if fdiameter<1 then
 begin
   if showcaption then th:=fteksthoog else th:=0;
   h2:=height-th;
   d:=2*calcbw;      //mag niet afhangen van diameter;
   a1:=(width-2*16) div 2; //2x icoonwidth
   a2:=(height-(2*16+fteksthoog)) div 2; //2x icoonwidth + tekstregel
   wr:=round(sqrt(sqr(a1)*2)-d)*2;
   hr:=round(sqrt(sqr(a2)*2)-d)*2;
   if hr<wr then hr:=wr;
//   hr:=min(wr,hr);
   if hr>h2-d then result:=h2-d else result:=hr;
//   log([width,result,d]);

   {
   if hr with
  wr:=round(sqrt(sqr((width/2)-32-calcbw/2)*2)); //gebaseerd op width
  diameter:=min(r*2,width-20);
  hr:=round(sqrt(sqr(round((width/2)-32-20))*2));  //gebaseerd op height
  result:=diameter;}
 end else result:=fdiameter;
end;

function TClock.calcBw: integer;
begin
  if fBorderWidth<1 then Result:= width div 40
  else result:=fBorderWidth
end;

function TClock.calcHpw: integer;
begin
  if fHourPointerWidth<1 then  Result:= width div 40
  else result:=fHourPointerWidth
end;

function TClock.calcMpw: Integer;
begin
  if fMinutePointerWidth<1 then
  Result:= width div 50
  else result:=fMinutePointerWidth
end;

function TClock.calcSpw: integer;
begin
  if fSecondPointerWidth<1 then
  Result:= width div 80
  else result:=fSecondPointerWidth
end;

procedure TClock.SetBulletSize(const Value: integer);
begin
  fBulletSize := Value;
  Invalidate;
end;

procedure TClock.SetHourPointerColor(const Value: TColor);
begin
  fHourPointerColor := Value;
  Invalidate;
end;

procedure TClock.SetMinutePointerColor(const Value: TColor);
begin
  fMinutePointerColor := Value;
  Invalidate;
end;

procedure TClock.SetSecondPointerColor(const Value: TColor);
begin
  fSecondPointerColor := Value;
  Invalidate;
end;

{function TClock.GetDiameter: integer;
begin
  Result := FDiameter;
  Invalidate;
end;}

procedure TClock.SetBorderColor(const Value: TColor);
begin
  fBorderColor := Value;
  Invalidate;
end;

procedure TClock.SetCaptionFont(const Value: TFont);
begin
//  fCaptionFont := Value;
  flabel.font.assign(Value);
  Invalidate;
end;

procedure TClock.SetFaceColor(const Value: TColor);
begin
  fFaceColor := Value;
  Invalidate;
end;

procedure TClock.SetMarkerColor(const Value: TColor);
begin
  fMarkerColor := Value;
  Invalidate;
end;

procedure TClock.Setcaption(const Value: String);
begin
  Fcaption := Value;
  FLabel.Caption:=fcaption;
  Invalidate;
end;


function TClock.GetCaptionFont: TFont;
begin
  Result := flabel.Font;
end;

procedure TClock.SetShowCaption(const Value: boolean);
begin
  FShowCaption := Value;
  Invalidate;
end;

procedure TClock.SetShowDigital(const Value: boolean);
begin
  FShowDigital := Value;
  Invalidate;
end;

procedure TClock.SetShowAnalog(const Value: boolean);
begin
  FShowAnalog := Value;
  Invalidate;
end;


procedure TClock.SetDiameter(const Value: integer);
begin
  FDiameter := Value;
  Invalidate;
end;

end.
