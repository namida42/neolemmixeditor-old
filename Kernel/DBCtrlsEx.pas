unit DBCtrlsEx;
                      //UNIT HERNOEMEN
interface

uses
  Windows, Messages, Classes, Controls, Graphics, DBCtrls, StdCtrls, Forms,
  UMasterControls, UMIsc, ULog;

type
  TDBEditEx = class(TDBEdit)
  private
    fWordWrap: Boolean;
    fMultiLine: Boolean;
    fItems: TStrings;
    fOnAutoComplete: TAutoCompleteEvent;
    fValidChars: TCharSet;
    procedure SetMultiLine(const Value: Boolean);
    procedure SetWordWrap(const Value: Boolean);
    procedure SetItems(const Value: TStrings);
    procedure SetValidChars(const Value: TCharSet);
    //procedure CMFocusChanged(var Message: TCMFocusChanged); message CM_FOCUSCHANGED;
//    fFlat: Boolean;
//    procedure SetFlat(const Value: Boolean);
//    function GetFlat: Boolean;
  protected
    function AutoCompleteStr(const S: string): string;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure DoEnter; override;
    procedure DoExit; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AfterConstruction; override;
    procedure KeyDown(var Key: word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    property ValidChars: TCharSet read fValidChars write SetValidChars;
  published
//    property Flat: Boolean read GetFlat write SetFlat default True;
//    property FocusColor;
//    property BevelInner;
//    property BevelOuter;
    property BorderWidth;
    property MultiLine: Boolean read fMultiLine write SetMultiLine;
    property WordWrap: Boolean read fWordWrap write SetWordWrap;
    property Items: TStrings read fItems write SetItems;
    property OnAutoComplete: TAutoCompleteEvent read fOnAutoComplete write fOnAutoComplete;
  end;

//procedure Register;

implementation

uses
  SysUtils, UWinTools;

procedure SelectBack(Edit: TDbEditEx; SelStart, SelLength: Integer);
var KS: TKeyboardState;
    OrgShift: Byte;
    I: Integer;
begin
  Edit.SelStart := SelStart;
  GetKeyboardState(KS);
  OrgShift := KS[VK_SHIFT];
  KS[VK_SHIFT] := $80;
  SetKeyboardState(KS);
  for I := SelStart downto SelStart - SelLength + 1 do
    Edit.Perform(WM_KEYDOWN, VK_LEFT, 0);
  Edit.Perform(WM_KEYUP, VK_LEFT, 0);
  KS[VK_SHIFT] := OrgShift;
  SetKeyboardState(KS);
end;


{procedure Register;
begin
  RegisterComponents('MasterDB', [TDBEditEx]);
end;}

{ TDBEditEx }

{ TDBEditEx }

procedure TDBEditEx.AfterConstruction;
begin
  inherited;
//  ctl3d := false;
end;

(*procedure TDBEditEx.CMFocusChanged(var Message: TCMFocusChanged);
var
  F: TCustomForm;
  Msg: TMMHintFocus;
begin
  inherited;
  F := GetParentForm(Self);
  if F <> nil then
  begin
    Msg.Msg := MM_HINTFOCUS;
    Msg.Control := Self;
    Msg.Unused := 0;
    Msg.Result := 0;
    F.BroadCast(Msg);//Perform(MM_HINTFOCUS, Integer(Self), 0);
  end;
//  Log(['focus dbedit']);
end;*)

{procedure TDBEditEx.CMFocusChanged(var Message: TCMFocusChanged);
begin
end;}

function TDBEditEx.AutoCompleteStr(const S: string): string;
var
  Len, i: Integer;
begin
  Result := '';
  if Assigned(fOnAutoComplete) then
  begin
    fOnAutoComplete(Self, S, Result);
  end;

  if Result <> '' then
    Exit;

  Len := Length(S);

//  log(['gb, editex',fItems.count]);
  with fItems do
    for i := 0 to Count - 1 do
    begin
      if CompareText(S, Copy(Strings[i], 1, Len)) = 0 then
      begin
        Result := Strings[i];
        Exit;
      end;
    end;
end;

constructor TDBEditEx.Create(AOwner: TComponent);
begin
  inherited;
  fItems := TStringList.Create;
  fValidChars:=[chr(0)..chr(255)];
//  fFlat := True;
//  Alignment := taLeftJustify;
//  Ctl3D := False;
//  ParentCtl3D := False;
end;

// om protected methode van owner aan te roepen
type
  THackedWinControl = class(TWinControl);

procedure TDBEditEx.CreateParams(var Params: TCreateParams);
const
  WordWraps: array[Boolean] of DWORD = (0, ES_AUTOHSCROLL);
begin
  inherited;
  with Params do
  begin
    if fMultiLine then
    Style := Style and not WordWraps[FWordWrap] or ES_MULTILINE;// or ScrollBar[FScrollBars];
    // Alignments[UseRightToLeftAlignment, FAlignment] or ScrollBar[FScrollBars];
  end;

end;

procedure TDBEditEx.CreateWnd;
begin
  inherited;
//  Ctl3d := not fFlat;
end;

destructor TDBEditEx.Destroy;
begin
  fItems.Free;
  inherited;
end;

procedure TDBEditEx.DoEnter;
begin
  inherited;
//Color := clRed;
end;

procedure TDBEditEx.DoExit;
begin
  inherited;
//Color := clWhite;
end;

{function TDBEditEx.GetFlat: Boolean;
begin
  Result := fFlat;
end;}

procedure TDBEditEx.KeyDown(var Key: word; Shift: TShiftState);
var
  C: TWinControl;
//  OldKey: Word;
begin

{  if Key = VK_return then begin
    OldKey := Key;
    Log(['VOOR EVENT', OldKey, Key]);
  end; }

  inherited KeyDown(Key, Shift); // = event onkeydown

{  if OldKey = VK_return then begin
    Log(['NA EVENT', OldKey, Key]);
  end; }

  // focus volgende edit
  case Key of
    VK_DOWN, VK_UP:
      if Shift = [] then
        if Owner is TWinControl then
        begin
           C := THackedWinControl(Owner).FindNextControl(Self, Key = VK_DOWN, True, False);
           if C is TCustomEdit then C.SetFocus;
        end;
    VK_RETURN:
      if (Shift = []) or (Shift = [ssShift]) then
        if Owner is TWinControl then
        begin
          C := THackedWinControl(Owner).FindNextControl(Self, Shift = [], True, False);
          if C is TCustomEdit then C.SetFocus;
        end;
  end;

end;

{procedure TDBEditEx.SetFlat(const Value: Boolean);
begin
  fFlat := Value;
  Ctl3D := not Value;
end;}

procedure TDBEditEx.KeyPress(var Key: Char);
var
  Temp, oldtext,newtext:string;
  start, SelLen: Integer;
label
  ExitPoint;
begin
  if not (Key in ValidChars) then
  begin
    Key := #0;
    Exit;
  end;
  
  if (Field = nil) or not (Field.DataSet.Active) then
    goto ExitPoint;

  if key < chr(32) then
    goto ExitPoint;

{  case Key of
    ^H, ^V, ^X, #32..#255:
      Field.DataSet.Edit; // KUT KUT KUT
  end;}


  start:=SelStart;
  oldtext:=text;
  newtext:=text;
  if SelLength>0 then
    delete(newtext,start+1,sellength);
  insert(key,newtext,start+1);
//    log([text,newtext,key,selstart]);
  temp:=AutoCompleteStr(Copy(newtext,1, start+1));
  if temp<>'' then
   begin
     key:=#0;
//     datasource.edit;
     text:=temp;
     sellen:=length(text)-start-1;
//       log(['sellen',start,sellen]);   uwintools
     SelectBack(Self, length(text), sellen);
     { TODO : dit moet anders }
//     Field.AsString := Text;
//     Change;
//     sellength:=length(text)-start-1;
//     selstart:=1;
//     start:=0;
     Exit;
   end;

  ExitPoint:
  inherited KeyPress(Key);
end;

procedure TDBEditEx.SetItems(const Value: TStrings);
begin
  fItems.Assign(Value);
end;

procedure TDBEditEx.SetMultiLine(const Value: Boolean);
begin
  if fMultiLine = Value then Exit;
  fMultiLine := Value;
  RecreateWnd;
end;

procedure TDBEditEx.SetValidChars(const Value: TCharSet);
begin
  fValidChars := Value;
  Include(fValidChars, #8);
  Include(fValidChars, #13);
end;

procedure TDBEditEx.SetWordWrap(const Value: Boolean);
begin
  if fWordWrap = Value then Exit;
  fWordWrap := Value;
  RecreateWnd;
end;

end.

