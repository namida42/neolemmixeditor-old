unit MxInplace;

// editlink interface verbeteren voor db

interface

uses
  Windows, Classes, Messages, Controls, StdCtrls, Mask, Forms, Graphics, ComCtrls,
  Db, MxMisc, Mx,
  Ulog;

type
  THackedMatrix = class(TCustomMatrix);
  THackedDBMatrix = class(TCustomDBMatrix);

{-------------------------------------------------------------------------------
  matrix string editor
-------------------------------------------------------------------------------}

  TStringEditor = class;

  { TStringEditLink implementeert de IMatrixEditLink interface om een string te
    editen. }
  TStringEditLink = class(TInterfacedObject, IMatrixEditLink)
  private
    fEditor    : TStringEditor;
    fMatrix    : TCustomMatrix;
    fStopping  : Boolean;
  protected
    procedure DoSetEditorText; virtual;
    procedure DoSetMatrixText; virtual;
    function DoCreateEditor: TStringEditor; virtual;
  public
    constructor Create;
    destructor Destroy; override;
  { IMatrixEditLink interface }
    function BeginEdit: Boolean; virtual;
    function CancelEdit: Boolean; virtual;
    function EndEdit: Boolean; virtual;
    function PrepareEdit(Matrix: TCustomMatrix; aCol, aRow: Integer): Boolean; virtual;
    function GetBounds: TRect; virtual;
    procedure SetBounds(R: TRect); virtual;
    procedure ProcessMessage(var Message: TMessage); virtual;
  published
  end;

  { TStringEditor is de interne klasse gebruikt in TStringEditLink. }
  TStringEditor = class(TCustomEdit)
  private
    fLink      : TStringEditLink;
    fRefLink   : IMatrixEditLink;
    fMultiLine : Boolean;
    // procedure CMAutoAdjust(var Message: TMessage); message CM_AUTOADJUST;
    procedure CMExit(var Message: TMessage); message CM_EXIT;
    procedure CMRelease(var Message: TMessage); message CM_RELEASE;
    // procedure CNCommand(var Message: TWMCommand); message CN_COMMAND;
    procedure WMGetDlgCode(var Msg: TWMGetDlgCode); message WM_GETDLGCODE;
    procedure WMDestroy(var Message: TWMDestroy); message WM_DESTROY;
    procedure SetMultiLine(const Value: Boolean);
  protected
    procedure KeyDown(var Key: word; Shift: TShiftState); override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    property MultiLine: Boolean read fMultiLine write SetMultiLine;
  public
    constructor Create(aLink: TStringEditLink); reintroduce; {overload; virtual;}
    destructor Destroy; override;
    procedure Release; virtual;
  end;

{-------------------------------------------------------------------------------
  matrix db string editor
-------------------------------------------------------------------------------}

  TDBStringEditor = class;

  { TDBStringEditLink implementeert de IMatrixEditLink interface om een datasetveld te
    editen. }
  TDBStringEditLink = class(TStringEditLink)
    fField: TField;
  protected
    procedure DoSetEditorText; override;
    procedure DoSetMatrixText; override;
    function DoCreateEditor: TStringEditor; override;
  public
  { IMatrixEditLink interface }
    function BeginEdit: Boolean; override;
    function CancelEdit: Boolean; override;
    function EndEdit: Boolean; override;
    function PrepareEdit(Matrix: TCustomMatrix; aCol, aRow: Integer): Boolean; override;
  published
  end;

  { TDBStringEditor is de interne klasse gebruikt in TDBStringEditLink. }
  TDBStringEditor = class(TStringEditor)
  end;

{-------------------------------------------------------------------------------
  checkbox
-------------------------------------------------------------------------------}

  TCheckEditLink = class;

  TCheckEdit = class(TCheckBox)
  private
    fRefLink: IMatrixEditLink; // ref naar interface
    fCheckLink: TCheckEditLink; // ref naar klasse
    procedure WMGetDlgCode(var Msg: TWMGetDlgCode); message WM_GETDLGCODE;
  protected
    procedure KeyDown(var Key: word; Shift: TShiftState); override;
  public
    constructor Create(aRefLink: TCheckEditLink); reintroduce;
  end;

  TCheckEditLink = class(TInterfacedObject, IMatrixEditLink)
  private
    fCheck: TCheckEdit;
    fMatrix: TCustomMatrix;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    function BeginEdit: Boolean;
    function CancelEdit: Boolean;
    function EndEdit: Boolean;
    function PrepareEdit(Matrix: TCustomMatrix; aCol, aRow: Integer): Boolean;
    function GetBounds: TRect;
    procedure SetBounds(R: TRect);
    procedure ProcessMessage(var Message: TMessage);
    procedure PaintTo(Matrix: TCustomMatrix; aCol, aRow: Integer; aRect: TRect; aCanvas: TCanvas);
  end;

{-------------------------------------------------------------------------------
  richedit
-------------------------------------------------------------------------------}
  TRichEditLink = class(TRichEdit, IMatrixEditLink)
  private
    fMatrix: TCustomMatrix;
  protected
    procedure KeyDown(var Key: word; Shift: TShiftState); override;
  public
    constructor Create; reintroduce;
    function BeginEdit: Boolean;
    function CancelEdit: Boolean;
    function EndEdit: Boolean;
    function PrepareEdit(Matrix: TCustomMatrix; aCol, aRow: Integer): Boolean;
    function GetBounds: TRect;
    procedure SetBounds(R: TRect); reintroduce;
    procedure ProcessMessage(var Message: TMessage);
  end;

//procedure Register;

implementation

{procedure Register;
begin
  //
end;}

{ TStringEditLink }

function TStringEditLink.BeginEdit: Boolean;
(*var
  S: TSize; *)
begin
  Result := not fStopping;
  if Result then
  begin
    fEditor.Show;
    fEditor.SetFocus;
(*    with fEditor do
    begin
      s.cx:=0;
      s.cy:=0;
      if GetTextExtentPoint(Handle, PChar(Text), Length(Text), S) then
      Log([S.cx, S.cy]);
    end; *)
  end;
end;

function TStringEditLink.CancelEdit: Boolean;
begin
  Result := not fStopping;
  if Result then
  begin
    fStopping := True;
    fEditor.Hide;
    fEditor.fLink := nil;
    fEditor.fRefLink := nil;
  end;
end;

constructor TStringEditLink.Create;
begin
//###  fEditor := TStringEditor.Create(Self);
  fEditor := DoCreateEditor;
  with fEditor do
  begin
//    Color := clInfoBk;
    Visible := False;
    BorderStyle := bsNone;
    AutoSize := False;
    AutoSelect := False;
  end;
end;

destructor TStringEditLink.Destroy;
begin
  fEditor.Release;
  inherited;
end;

function TStringEditLink.DoCreateEditor: TStringEditor;
begin
  Result := TStringEditor.Create(Self);
end;

procedure TStringEditLink.DoSetEditorText;
begin
  with THackedMatrix(fMatrix) do
    fEditor.Text := Text[EditCol, EditRow];
end;

procedure TStringEditLink.DoSetMatrixText;
begin
  with THackedMatrix(fMatrix) do
    Text[EditCol, EditRow] := fEditor.Text;
end;

function TStringEditLink.EndEdit: Boolean;
begin
  Result := not fStopping;
  if Result then
  try
    fStopping := True;
    if fEditor.Modified then
      DoSetMatrixText;
    {###if fEditor.Modified then
      with THackedMatrix(fMatrix) do
        Text[EditCol, EditRow] := fEditor.Text;}
    fEditor.Hide;
    fEditor.fLink := nil;
    fEditor.fRefLink := nil;
  except
    fStopping := False;
    raise;
  end;
end;

function TStringEditLink.GetBounds: TRect;
begin
  Result := fEditor.BoundsRect;
end;

function TStringEditLink.PrepareEdit(Matrix: TCustomMatrix; aCol, aRow: Integer): Boolean;
begin
//  Result := False;
  fMatrix := Matrix;
  fEditor.Parent := fMatrix;
  fEditor.MultiLine := THackedMatrix(fMatrix).DoGetCellStyle(aCol, aRow).MultiLine;
  fEditor.Font := THackedMatrix(fMatrix).DoGetCellStyle(aCol, aRow).Font;
  fEditor.RecreateWnd;
  fEditor.HandleNeeded;
  if mxEditChar in THackedMatrix(Matrix).States then
  begin
    fEditor.Text := THackedMatrix(Matrix).EditChar;
    fEditor.SelStart := 1;
    fEditor.Modified := True; // belangrijk
  end
  else
    DoSetEditorText;
  Result := True;
end;

procedure TStringEditLink.ProcessMessage(var Message: TMessage);
begin
//
end;

procedure TStringEditLink.SetBounds(R: TRect);
begin
  fEditor.SetBounds(R.Left, R.Top, RectWidth(R), RectHeight(R));
  fEditor.BorderWidth := 2;
end;

{ TStringEditor }

procedure TStringEditor.CMExit(var Message: TMessage);
var
  Mx: THackedMatrix;
begin
  if Assigned(fLink) and not fLink.fStopping then
  begin
//    Log(['edit cmexit']);
    Mx := THackedMatrix(fLink.fMatrix);
    Mx.DoEditEnd; // belangrijk! ## anders?!?

    {
    with FLink, FTree do
    begin
      if (toAutoAcceptEditChange in TreeOptions.StringOptions) then
        DoEndEdit
      else
        DoCancelEdit;
    end; }
  end;
  inherited;
end;

procedure TStringEditor.CMRelease(var Message: TMessage);
begin
  Free;
end;

constructor TStringEditor.Create(aLink: TStringEditLink);
begin
  inherited Create(nil);
  fRefLink := aLink;
  fLink := aLink;
end;

procedure TStringEditor.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if fMultiLine then Params.Style := Params.Style or ES_MULTILINE;// or ES_CENTER;// and not ES_AUTOHSCROLL;// or ;
end;

procedure TStringEditor.CreateWnd;
begin
  inherited;
//  SetTextAlign(Handle, TA_LEFT or VTA_CENTER);
end;

destructor TStringEditor.Destroy;
begin
  inherited Destroy;
end;

procedure TStringEditor.KeyDown(var Key: word; Shift: TShiftState);
var
  Mx: THackedMatrix;
  Modi: Boolean;
begin
  Assert(Assigned(fLink), 'TStringEditor.KeyDown. Link = nil');
  Assert(Assigned(fLink.fMatrix), 'TStringEditor.KeyDown. Link.Matrix = nil');

  Mx := THackedMatrix(fLink.fMatrix);

  case Key of
    VK_ESCAPE:
      begin
        begin
          Mx.DoEditCancel;
          Mx.SetFocus;
          Key := 0;
        end;
      end;
    VK_NEXT, VK_PRIOR{, VK_F2 {VK_ESCAPE, }:
      begin
        Mx.DoEditEnd;
        Mx.SetFocus;
        Mx.KeyDown(Key, Shift);
        Key := 0;
      end;
{   VK_RIGHT:
     begin
       if SelLength = 0 then
       if SelStart = Length(Text) then
       begin
         Mx.DoEditEnd;
         Mx.SetFocus;
         Mx.KeyDown(Key, Shift);
         Key := 0;
       end;
     end; }
   VK_F2:
     begin
       Mx.DoEditEnd;
       Mx.SetFocus;
       Key := 0;
     end;
   VK_UP:
     begin
       if not fMultiLine then
       begin
         Mx.DoEditEnd;
         Mx.SetFocus;
         Mx.KeyDown(Key, Shift);
         Key := 0;
       end;
     end;
   VK_DOWN:
     begin
       if not fMultiLine then
       begin
         Mx.DoEditEnd;
         Mx.KeyDown(Key, Shift);
         Mx.SetFocus;
         Key := 0;
       end;
     end;
   VK_RIGHT:
     begin
     if not fMultiLine then
     if eoAlwaysShow in Mx.Options.EditOptions then
       if Shift = [] then
         if SelStart = Length(Text) then
           if SelLength = 0 then
           begin
             Mx.DoEditEnd;
             Mx.SetFocus;
             Mx.KeyDown(Key, []);
             Key := 0;
           end;
     end;
   VK_LEFT:
     begin
     if not fMultiLine then
     if eoAlwaysShow in Mx.Options.EditOptions then
       if Shift = [] then
         if SelStart = 0 then
           if SelLength = 0 then
           begin
             Mx.DoEditEnd;
             Mx.SetFocus;
             Mx.KeyDown(Key, []);
             Key := 0;
           end;
     end;
   VK_RETURN:
      begin
        if not fMultiLine or (ssCtrl in Shift) then
        begin
          Modi := Modified;
          Mx.DoEditEnd;
          Mx.SetFocus;
          if Modi and Mx.Options.DownAfterEdit then
          begin
            Key := VK_DOWN;
            Shift := [];
            Mx.KeyDown(Key, []);
          end;
        end;
        Key := 0;
      end
  end;
  if Key <> 0 then
  begin
    inherited KeyDown(Key, Shift);
//    Log([key=0]);
  end;
end;

procedure TStringEditor.Release;
begin
  if HandleAllocated then
    PostMessage(Handle, CM_RELEASE, 0, 0)
  else
    Free;
end;

procedure TStringEditor.SetMultiLine(const Value: Boolean);
begin
  if fMultiLine = Value then Exit;
  fMultiLine := Value;
  RecreateWnd;
end;

procedure TStringEditor.WMDestroy(var Message: TWMDestroy);
begin
  if Assigned(FLink) and not FLink.FStopping then
  begin
{    if Modified then
      Mx.Text[Mx.EditCol, Mx.EditRow] := Text; }
(*    with FLink, FTree do
    begin
      if (toAutoAcceptEditChange in TreeOptions.StringOptions) and Modified then
        Text[FNode, FColumn] := FEdit.Text;
    end; *)
    FLink := nil;
    FRefLink := nil;
  end;

  inherited;
end;

procedure TStringEditor.WMGetDlgCode(var Msg: TWMGetDlgCode);
begin
  inherited;
  Msg.Result := Msg.Result or DLGC_WANTALLKEYS or DLGC_WANTTAB or DLGC_WANTARROWS;
end;

{ TDBStringEditLink }

function TDBStringEditLink.BeginEdit: Boolean;
begin
  // beforebeginedit;
  if fField <> nil then
  begin
    fField.DataSet.Edit;
  end;
  Result := inherited BeginEdit;
  //after beginedit;
end;

function TDBStringEditLink.CancelEdit: Boolean;
begin
  // beforecanceledit;
  if fField <> nil then
  begin
    if fField.DataSet.State = dsEdit then
      fField.DataSet.Cancel;
  end;
  Result := inherited CancelEdit;
  // aftercanceledit;
end;

function TDBStringEditLink.DoCreateEditor: TStringEditor;
begin
  Result := TDBStringEditor.Create(Self);
end;

procedure TDBStringEditLink.DoSetEditorText;
begin
  if fField <> nil then
    fEditor.Text := fField.Text;
end;

procedure TDBStringEditLink.DoSetMatrixText;
//var
  //Handled: Boolean;
begin
{  if fField <> nil then
    fField.AsString := fEditor.Text;}

//   Handled := False;


//   if not Handled then
     if fField <> nil then
       fField.Text := fEditor.Text;

   if (fEditor.Text = '"') or (fEditor.Text = '''') then
   begin
     with THackedDBMatrix(fMatrix) do
       {Handled := }DoQuote;
   end;

end;

function TDBStringEditLink.EndEdit: Boolean;
begin
//  if fField.DataSet.State in [dsEdit, dsInsert] then
  //  fField.DataSet.Post;
  Result := inherited EndEdit;
end;

function TDBStringEditLink.PrepareEdit(Matrix: TCustomMatrix; aCol, aRow: Integer): Boolean;
begin
  //beforeprepareedit
  fField := THackedDBMatrix(Matrix).SelectedField;
  Result := fField <> nil;
  if Result then
  begin
    fField.DataSet.Edit;
    Result := inherited PrepareEdit(Matrix, aCol, aRow);
  end;
  //afterprepareedit
end;

{ TCheckEdit }

constructor TCheckEdit.Create(aRefLink: TCheckEditLink);
begin
  inherited Create(nil);
  fRefLink := aRefLink;
  fCheckLink := aRefLink;
end;

procedure TCheckEdit.KeyDown(var Key: word; Shift: TShiftState);
begin
  case Key of
    VK_ESCAPE:
      begin
        with THackedMatrix(fCheckLink.fMatrix) do
        begin
          DoEditCancel;
          SetFocus;
        end;
      end;
    VK_NEXT, VK_PRIOR, VK_DOWN, VK_UP, VK_RIGHT, VK_LEFT, VK_F2, {VK_ESCAPE, }VK_RETURN:
      begin
        THackedMatrix(fCheckLink.fMatrix).KeyDown(Key, Shift);
        THackedMatrix(fCheckLink.fMatrix).SetFocus;
        //fCheckLink.fMatrix.dKeyDown(Key, Shift);
        Key := 0;
      end;

    else // niet afgevangen cases
      inherited KeyDown(Key, Shift);
  end
end;

procedure TCheckEdit.WMGetDlgCode(var Msg: TWMGetDlgCode);
begin
  inherited;
  Msg.Result := Msg.Result or DLGC_WANTALLKEYS or DLGC_WANTTAB or DLGC_WANTARROWS;
end;

{ TCheckEditLink }

function TCheckEditLink.BeginEdit: Boolean;
begin
  fCheck.Show;
  fCheck.SetFocus;
  Result := True;
//  fCheck.PaintTo()
end;

function TCheckEditLink.CancelEdit: Boolean;
begin
  fCheck.Hide;
  fCheck.fCheckLink := nil;
  fCheck.fRefLink := nil;
  Result := True;
end;

constructor TCheckEditLink.Create;
begin
  fCheck := TCheckEdit.Create(Self);
  fCheck.Visible := False;
//  fCheck.AllowGrayed := False;
//  fCheck.BorderStyle := bsNone;
end;

destructor TCheckEditLink.Destroy;
begin
//  windlg('destroy');
{  fCheck.fCheckLink := nil;
  fCheck.fRefLink := nil;
  fCheck.Free;}
  inherited;
end;

function TCheckEditLink.EndEdit: Boolean;
begin
  fCheck.Hide;
  fCheck.fCheckLink := nil;
  fCheck.fRefLink := nil;
  Result := True;
end;

function TCheckEditLink.GetBounds: TRect;
begin
  Result := fCheck.BoundsRect;
end;

procedure TCheckEditLink.PaintTo(Matrix: TCustomMatrix; aCol, aRow: Integer; aRect: TRect; aCanvas: TCanvas);
begin
{  Result := False;
  fMatrix := Matrix;
  fCheck.Parent := fMatrix;
  fCheck.RecreateWnd;
  fCheck.HandleNeeded;
  fCheck.Caption := THackedMatrix(fMatrix).Text[aCol, aRow];
  Result := True;}
  DrawFrameControl(aCanvas.Handle, aRect, DFC_BUTTON, DFCS_BUTTONCHECK)
//
end;

function TCheckEditLink.PrepareEdit(Matrix: TCustomMatrix; aCol, aRow: Integer): Boolean;
begin
//  Result := False;
  fMatrix := Matrix;
  fCheck.Parent := fMatrix;
  fCheck.RecreateWnd;
  fCheck.HandleNeeded;
  fCheck.Caption := THackedMatrix(fMatrix).Text[aCol, aRow];
  Result := True;
end;

procedure TCheckEditLink.ProcessMessage(var Message: TMessage);
begin
//
end;

procedure TCheckEditLink.SetBounds(R: TRect);
begin
  fCheck.SetBounds(R.Left, R.Top, RectWidth(R), RectHeight(R));
end;

{ TRichEditLink }

function TRichEditLink.BeginEdit: Boolean;
begin
  Show;
  SetFocus;
  Result := True;
end;

function TRichEditLink.CancelEdit: Boolean;
begin
  Hide;
  Result := True;
end;

constructor TRichEditLink.Create;
begin
  inherited Create(nil);
  BorderStyle := bsNone;
end;

function TRichEditLink.EndEdit: Boolean;
begin
  with THackedMatrix(fMatrix) do
  begin
    Text[Col, Row] := Self.Text;
  end;
  Hide;
  Result := True;
end;

function TRichEditLink.GetBounds: TRect;
begin
  Result := BoundsRect;
end;

procedure TRichEditLink.KeyDown(var Key: word; Shift: TShiftState);
begin
  case Key of
    VK_CANCEL:
      begin
        THackedMatrix(fMatrix).DoEditCancel;
        THackedMatrix(fMatrix).SetFocus;
        Key := 0;
      end;
    VK_RETURN:
      if Shift = [ssCtrl] then
      begin
        THackedMatrix(fMatrix).DoEditEnd;
        THackedMatrix(fMatrix).SetFocus;
        Key := 0;
      end;
  end;
  if Key <> 0 then
    inherited KeyDown(Key, Shift);
end;

function TRichEditLink.PrepareEdit(Matrix: TCustomMatrix; aCol, aRow: Integer): Boolean;
begin
  Result := True;
  fMatrix := Matrix;
  Parent := fMatrix;
  Text := THackedMatrix(fMatrix).Text[aCol, aRow];
//  HandleNeeded;
end;

procedure TRichEditLink.ProcessMessage(var Message: TMessage);
begin
  //
end;

procedure TRichEditLink.SetBounds(R: TRect);
begin
  inherited SetBounds(R.Left, R.Top, RectWidth(R), RectHeight(R));
end;


initialization
//  RegisterClasses([TStringEditLink]);

end.

