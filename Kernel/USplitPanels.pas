unit USplitPanels;

{-------------------------------------------------------------------------------
  USplitPanels: Unit with some components to split up forms in portions.
  Freeware. Use at own risk.
  Eric Langedijk: ericenzwaan@hccnet.nl
  Beta Version 0.0.1
-------------------------------------------------------------------------------}

{ TODO : Optimize invalidating by clearing CS_VREDRAW and CS_HREDRAW and calculate invalidating rects }
{ TODO : Write our own CanAutoSize }
{ TODO : Add more comments }
{ TODO : Write property editors }
{ TODO : Write code to enable splitterdragging at designtime }
{ TODO : Implement linestruct while resizing }

interface

uses
  Windows, Classes, Controls, Graphics, Messages, Themes, Forms, SysUtils;

type
  // Determines in which direction the childpanels are divided
  TSplitMode = (
    smVertical,                        // Panel is vertically split
    smHorizontal                       // Panel is horizontally split
  );

  // Position of panel in its parentpanel, works a bit like align.
  // spNone and spClient will always be in between spFirst and spLast
  TSplitPosition = (
    spNone,
    spClient,                          // Main panel, which will be the initial resizing one (only 1 allowed)
    spFirst,                           // Panel is placed Top / Left, and will stay there
    spLast                             // Panel is placed Bottom / Right, and will stay there
  );
  TSplitPositions = set of TSplitPosition; // this set is only used internal

  // Options for panel
  TSplitPanelOption = (
    poNoDragPushing,                   // Splitters only resize their neighbour-panels
    poShowCaption,                     // Caption visible
    poUseInternalPaintBuffer,          // Use own bitmap buffer to draw on
    poDesignHints                      // Show designhints when settings "wrong" properties
  );
  TSplitPanelOptions = set of TSplitPanelOption;

  // How are the panels divided after resize
  TDivideMode = (
    dmNone,
    dmScaled,                          // Try to keep the childpanels at the same scale
    dmEqual                            // Try to divide the childpanels equally
  );

  // Easy way to set some common borderstyle combinations of bevelkind, bevelinner, bevelouter, borderstyle.
  TQuickBorderStyle = (
    qbUnknown,                         // Unknown combination of bevelkind, bevelinner, bevelouter, borderstyle.
    qbNone,                            // Flat
    qbBorder,                          // BorderStyle bsSingle, no bevels
    qbLowered,                         // BorderStyle bsNone, bevels lowered look
    qbRaised                           // BorderStyle bsNone, bevels raised look
  );

  // Internal state of panel
  TPanelControlState = (
    pcsUpdatingLists,                  // UpdateLists is active. No Realign allowed during that state (see UpdateLists)
    pcsAligningChildPanels,            // State to disable actual resizing during size-calculations (see AlignPanels)
    pcsCalculatingChildPanelBounds,    // Panel is calculating the sizes of its children (see AlignPanels)
    pcsSplitterDrag,                   // User is resizing a childpanel with a splitter
    pcsEqualDivideNeeded,              // EqualDividePanels needed or forced
    pcsUpdatingBorder                  // Some borders are changed, prevents multiple calls to CMBorderChanged
  );
  TPanelControlStates = set of TPanelControlState;

  // Internal helper for TPanelSplitter DrawLine
  TInternalSplitterDrawMode = (
    dmShow,
    dmMove,
    dmHide
  );

  // What do we see during dragging with a splitter?
  TDragResizeMode = (
    rmLive,                            // Direct resize (optionally using a timer to reduce flickering)
    rmLine,                            // Draw a simple xor-line
    rmStructure                        // Draw the entire new structure with lines during dragging (not implemented yet)
  );

  TSplitterOption = (
    soPaintSplitters,                  // Paint the splitters
    soHighlightSplitters               // Use highlight color on mouseover
  );
  TSplitterOptions = set of TSplitterOption;

  // Internal record for panelsplitter
  TDragInfo = record
    iDragging     : Boolean;
    iStartX       : Integer;           // Start drag x
    iStartY       : Integer;           // Start drag y
    iCurrentX     : Integer;           // Active mouse x
    iCurrentY     : Integer;           // Active mouse y
    iDrawX        : Integer;           // Last xor draw x
    iDrawY        : Integer;           // Last xor draw y
    iStructRects  : array of TRect;
  end;

const
  MIN_PANELSIZE          = 4;          // Hardcoded minimum size for panel (width + height)
  MAX_PANELDISTANCE      = 256;        // Maximum distance between panels
  MAX_CHILDPANELS        = 32;

  // Default property values
  DEF_SPLITMODE          = smVertical;
  DEF_DISTANCE           = 4;
  DEF_PANELHEIGHT        = 200;
  DEF_PANELWIDTH         = 200;
  DEF_DRAGRESIZEMODE     = rmLive;
  DEF_OPTIONS            = [];
  DEF_DIVIDEMODE         = dmScaled;
  DEF_SPLITTEROPTIONS    = [];

  // Default border property values
  DEF_BEVELEDGES         = [beLeft, beTop, beRight, beBottom];
  DEF_BEVELKIND          = bkTile;
  DEF_BEVELINNER         = bvNone;
  DEF_BEVELOUTER         = bvLowered;
  DEF_BEVELWIDTH         = 1;
  DEF_BORDERSTYLE        = bsNone;
  DEF_BORDERWIDTH        = 0;

  // Shared Property Flags, used to set multiple properties with TSplitPanelPropertyManager
  SPF_BEVELEDGES         = 1;          // bit 0
  SPF_BEVELINNER         = 1 shl 1;    // bit 1
  SPF_BEVELOUTER         = 1 shl 2;    // bit 2
  SPF_BEVELKIND          = 1 shl 3;    // bit 3
  SPF_BEVELWIDTH         = 1 shl 4;    // bit 4
  SPF_BORDERSTYLE        = 1 shl 5;    // bit 5
  SPF_BORDERWIDTH        = 1 shl 6;    // bit 6
  SPF_DISTANCE           = 1 shl 7;    // bit 7
  SPF_DRAGRESIZEMODE     = 1 shl 8;    // bit 8
  SPF_OPTIONS            = 1 shl 9;    // bit 9
  SPF_DIVIDEMODE         = 1 shl 10;   // bit 10
  SPF_SPLITTEROPTIONS    = 1 shl 11;   // bit 11

  SPF_ALL                = $FFFFFFFF;
  SPF_QUICKBORDER        = SPF_BEVELKIND + SPF_BEVELINNER + SPF_BEVELOUTER + SPF_BORDERSTYLE;


type
  TRectArray = array of TRect;

  TSplitPanel = class;
  TSplitPanelClass = class of TSplitPanel;
  TPanelSplitter = class;
  TSplitPanelPropertyManager = class;

  // List of panels
  TSplitPanelList = class(TList)
  private
    function GetItem(Index: Integer): TSplitPanel;
  protected
    procedure SortBySize(SmallFirst: Boolean);
  public
    function Add(Item: TSplitPanel): Integer;
    property Items[Index: Integer]: TSplitPanel read GetItem; default;
  end;

  // list of splitters
  TPanelSplitterList = class(TList)
  private
    function GetItem(Index: Integer): TPanelSplitter;
  protected
  public
    function Add(Item: TPanelSplitter): Integer;
    property Items[Index: Integer]: TPanelSplitter read GetItem; default;
  end;

  // Record, used for some paint methods
  TPaintInfo = record
    TargetCanvas  : TCanvas;           // This has to be the first field of the record, because of some absolute local vars
    PanelRect     : TRect;             // This is always the ClientRect of the panel
    ContentRect   : TRect;             // Textrect or whatever
  end;

  // Internal method event types
  TPanelGetEvent = function: Integer of object;
  TPanelSetEvent = procedure(Value: Integer) of object;
  TGetConstraintSizeEvent = function: TConstraintSize of object;
  TSetConstraintSizeEvent = procedure(Value: TConstraintSize) of object;

  // -- TSplitPanel: class to divide up a screen in resizable pieces
  TSplitPanel = class(TCustomControl)
  private
  { internal event methods to make life easier while accessing direction-related properties }
    EvGetSize             : TPanelGetEvent;
    EvSetSize             : TPanelSetEvent;
    EvGetPosition         : TPanelGetEvent;
    EvSetPosition         : TPanelSetEvent;
    EvGetCalculatedSize   : TPanelGetEvent;
    EvGetMinSize          : TGetConstraintSizeEvent;
    EvSetMinSize          : TSetConstraintSizeEvent;
    EvGetMaxSize          : TGetConstraintSizeEvent;
    EvSetMaxSize          : TSetConstraintSizeEvent;
    EvGetRealMinSize      : TGetConstraintSizeEvent;
    EvGetRealMaxSize      : TGetConstraintSizeEvent;
  { private properties }
    fAlignList            : TSplitPanelList;       // Internal helper list
    fBorderStyle          : TBorderStyle;
    fBoundsBeforeDrag     : TRect;                 // Bounds which are saved before dragging
    fCalculatedHeight     : Integer;               // Temporary height during align, otherwise equal to height
    fCalculatedWidth      : Integer;               // Temporary width during align. otherwise equal to width
    fCorrectList          : TSplitPanelList;       // Internal helper list
    fCurrentScale         : Double;                // Scale of size in parentpanel
    fDistance             : Integer;               // Distance of childpanels
    fDivideMode           : TDivideMode;
    fDraggingSplitter     : TPanelSplitter;        // Current dragger, only valid during splitter dragging
    fDragResizeMode       : TDragResizeMode;
    fLastAlignCount       : Integer;               // Used in AlignPanels to decide appropriate align action
    fMethodsSplitMode     : TSplitMode;            // The splitmode the internal events are set for, to prevent firing too much code
    fNonClientLeft        : Integer;
    fNonClientTop         : Integer;
    fNonClientRight       : Integer;
    fNonClientBottom      : Integer;
    fOldConstraintsEvent  : TNotifyEvent;          // Saved OnChange event of Constraints. which will be overridden
    fOptions              : TSplitPanelOptions;
    fPaintLockCount       : Integer;
    fPanelClass           : TSplitPanelClass;      // Class of the childpanels, default set to Self.ClassType
    fPropertyManager      : TSplitPanelPropertyManager;
    fStates               : TPanelControlStates;
    fPanels               : TSplitPanelList;       // List of childpanels
    fParentPanel          : TSplitPanel;           // If parent is a splitpanel then this is filled, otherwise nil
    fRealConstraints      : TSizeConstraints;      // sum of the realconstraints of the childpanels
    fSplitMode            : TSplitMode;
    fSplitPosition        : TSplitPosition;
    fSplitterDragDelta    : Integer;               // Only valid during splitter dragging
    fSplitterOptions      : TSplitterOptions;      // Options for all splitters
    fSplitters            : TPanelSplitterList;    // List of all splitters
    fUpdateRect           : TRect;                 // Paint update area
    fVisiblePanels        : TSplitPanelList;       // List of visible childpanels
    fVisibleSplitters     : TPanelSplitterList;    // List of visible splitters
    //
  { internal event methods }
    function InternalGetCalculatedHeight: Integer;
    function InternalGetCalculatedWidth: Integer;
    function InternalGetHeight: Integer;
    function InternalGetLeft: Integer;
    function InternalGetMinHeight: TConstraintSize;
    procedure InternalSetMinHeight(Value: TConstraintSize);
    function InternalGetMaxHeight: TConstraintSize;
    procedure InternalSetMaxHeight(Value: TConstraintSize);
    function InternalGetMinWidth: TConstraintSize;
    procedure InternalSetMinWidth(Value: TConstraintSize);
    function InternalGetMaxWidth: TConstraintSize;
    procedure InternalSetMaxWidth(Value: TConstraintSize);
    function InternalGetRealMaxHeight: TConstraintSize;
    function InternalGetRealMaxWidth: TConstraintSize;
    function InternalGetRealMinHeight: TConstraintSize;
    function InternalGetRealMinWidth: TConstraintSize;
    function InternalGetTop: Integer;
    function InternalGetWidth: Integer;
    procedure InternalSetHeight(Value: Integer);
    procedure InternalSetLeft(Value: Integer);
    procedure InternalSetTop(Value: Integer);
    procedure InternalSetWidth(Value: Integer);
    procedure NewConstraintsOnChange(Sender: TObject);
  { internal methods }
    procedure AddPanel(P: TSplitPanel);
    procedure AddSplitter(S: TPanelSplitter);
    function CalcSumSizes(aList: TSplitPanelList; aStartIndex: Integer = -1; aStopIndex: Integer = -1): Integer;
    procedure ForEachPanel(LocalProc: Pointer; BackWards: Boolean = False);
    procedure ForEachVisiblePanel(LocalProc: Pointer; BackWards: Boolean = False);
    function GetCalculatedClientHeight: Integer;
    function GetCalculatedClientWidth: Integer;
    function NewPanel: TSplitPanel;
    procedure RecalcScales(DoChildren: Boolean = False);
    procedure RemovePanel(P: TSplitPanel);
    procedure RemoveSplitter(S: TPanelSplitter);
    procedure ResetMethods(aParentSplitMode: TSplitMode);
    procedure SplitterDrag(Sender: TPanelSplitter; Delta: Integer);
    procedure SplitterDragBegin(Sender: TPanelSplitter);
    procedure SplitterDragCancel(Sender: TPanelSplitter);
    procedure SplitterDragEnd(Sender: TPanelSplitter);
    procedure UpdateLists;
    procedure UpdateRealConstraints;
  { vcl + win messages }
    procedure CMBorderChanged(var Message: TMessage); message CM_BORDERCHANGED;
    procedure CMControlChange(var Message: TCMControlChange); message CM_CONTROLCHANGE;
    procedure CMControlListChange(var Message: TCMControlListChange); message CM_CONTROLLISTCHANGE;
    procedure CMCtl3DChanged(var Message: TMessage); message CM_CTL3DCHANGED;
    procedure CMTextChanged(var Message: TMessage); message CM_TEXTCHANGED;
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
    procedure WMEraseBkgnd(var Msg: TWmEraseBkgnd); message WM_ERASEBKGND;
    procedure WMNCCalcSize(var Message: TWMNCCalcSize); message WM_NCCALCSIZE;
    procedure WMNCPaint(var Message: TMessage); message WM_NCPAINT;
    procedure WMPaint(var Msg: TWMPaint); message WM_PAINT;
    procedure WMWindowPosChanged(var Message: TWMWindowPosChanged); message WM_WINDOWPOSCHANGED;
    procedure WMWindowPosChanging(var Message: TWMWindowPosChanging); message WM_WINDOWPOSCHANGING;
  { property access }
    function GetAlign: TAlign;
    function GetCalculatedSize: Integer;
    function GetFixedSize: Integer;
    function GetLevel: Integer;
    function GetMaxSize: TConstraintSize;
    function GetMinSize: TConstraintSize;
    function GetPanel(Index: Integer): TSplitPanel;
    function GetPanelCount: Integer;
    function GetPanelIndex: Integer;
    function GetPosition: Integer;
    function GetQuickBorder: TQuickBorderStyle;
    function GetRealMaxSize: TConstraintSize;
    function GetRealMinSize: TConstraintSize;
    function GetSize: Integer;
    function GetVisibleIndex: Integer;
    function GetVisiblePanel(Index: Integer): TSplitPanel;
    function GetVisiblePanelCount: Integer;
    procedure SetAlign(const Value: TAlign);
    procedure SetBorderStyle(const Value: TBorderStyle);
    procedure SetDistance(Value: Integer);
    procedure SetDivideMode(Value: TDivideMode);
    procedure SetFixedSize(const Value: Integer);
    procedure SetPropertyManager(const Value: TSplitPanelPropertyManager);
    procedure SetMaxSize(Value: TConstraintSize);
    procedure SetMinSize(Value: TConstraintSize);
    procedure SetOptions(const Value: TSplitPanelOptions);
    procedure SetPanelCount(Value: Integer);
    procedure SetPosition(Value: Integer);
    procedure SetQuickBorder(const Value: TQuickBorderStyle);
    procedure SetSize(Value: Integer);
    procedure SetSplitMode(const Value: TSplitMode);
    procedure SetSplitPosition(const Value: TSplitPosition);
    procedure SetSplitterOptions(const Value: TSplitterOptions);
  protected
  { protected overrides }
    procedure AlignControls(AControl: TControl; var Rect: TRect); override;
    function CanAutoSize(var NewWidth, NewHeight: Integer): Boolean; override;
    procedure ConstrainedResize(var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer); override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    function GetClientRect: TRect; override;
    procedure Loaded; override;
    procedure Paint; override;
    procedure RequestAlign; override;
    procedure SetParent(aControl: TWinControl); override;
  { protected new methods }
    procedure AlignPanels(aControl: TControl; var aRect: TRect);
    procedure DesignHint(const S: string); virtual;
    procedure PaintPanel(aCanvas: TCanvas; aWindow: TRect); virtual;
    procedure PaintBody(const PaintInfo: TPaintInfo); virtual;
    procedure PaintCaption(const PaintInfo: TPaintInfo); virtual;
  { protected properties }
    property CalculatedWidth: Integer read fCalculatedWidth;
    property CalculatedHeight: Integer read fCalculatedHeight;
    property CalculatedSize: Integer read GetCalculatedSize;
    property Position: Integer read GetPosition write SetPosition;
    property States: TPanelControlStates read fStates;
    property RealConstraints: TSizeConstraints read fRealConstraints;
    property RealMaxSize: TConstraintSize read GetRealMaxSize;
    property RealMinSize: TConstraintSize read GetRealMinSize;
  public
  { public overrides }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(aLeft, aTop, aWidth, aHeight: Integer); override;
    procedure Update; override;
  { public new methods }
    procedure EqualDividePanels;
    procedure PaintLock;
    procedure PaintUnlock;
  { public properties }
    function GetRoot: TSplitPanel;
    function GetMyPropertyManager: TSplitPanelPropertyManager;
    property Level: Integer read GetLevel;
    property PanelIndex: Integer read GetPanelIndex;
    property Panels[Index: Integer]: TSplitPanel read GetPanel; default;
    property ParentPanel: TSplitPanel read fParentPanel;
    property Size: Integer read GetSize write SetSize;
    property VisibleIndex: Integer read GetVisibleIndex;
    property VisiblePanelCount: Integer read GetVisiblePanelCount;
    property VisiblePanels[Index: Integer]: TSplitPanel read GetVisiblePanel;
  published
  { inherited properties }
    property Align: TAlign read GetAlign write SetAlign default alNone;
    property AutoSize;
    property Caption;
    property Color;
    property Constraints;
    property DockSite;
    property Font;
    property Height;
    property ParentColor;
    property TabOrder;
    property UseDockManager;
    property Visible;
    property Width;
  { inherited border properties }
    property BorderWidth default DEF_BORDERWIDTH;
    property BevelEdges default DEF_BEVELEDGES;
    property BevelKind default DEF_BEVELKIND;
    property BevelInner default DEF_BEVELINNER;
    property BevelOuter default DEF_BEVELOUTER;
    property BevelWidth default DEF_BEVELWIDTH;
    property Ctl3D;
  { new border properties }
    property BorderStyle: TBorderStyle read fBorderStyle write SetBorderStyle default DEF_BORDERSTYLE;
  { other properties }
    property DivideMode: TDivideMode read fDivideMode write SetDivideMode default DEF_DIVIDEMODE;
    property Distance: Integer read fDistance write SetDistance default DEF_DISTANCE;
    property DragResizeMode: TDragResizeMode read fDragResizeMode write fDragResizeMode default DEF_DRAGRESIZEMODE;
    property FixedSize: Integer read GetFixedSize write SetFixedSize stored False;
    property MaxSize: TConstraintSize read GetMaxSize write SetMaxSize stored False;
    property MinSize: TConstraintSize read GetMinSize write SetMinSize stored False;
    property Options: TSplitPanelOptions read fOptions write SetOptions default DEF_OPTIONS;
    property PanelCount: Integer read GetPanelCount write SetPanelCount stored False;
    property PropertyManager: TSplitPanelPropertyManager read fPropertyManager write SetPropertyManager;
    property QuickBorder: TQuickBorderStyle read GetQuickBorder write SetQuickBorder stored False;
    property SplitMode: TSplitMode read fSplitMode write SetSplitMode;
    property SplitPosition: TSplitPosition read fSplitPosition write SetSplitPosition;
    property SplitterOptions: TSplitterOptions read fSplitterOptions write SetSplitterOptions default DEF_SPLITTEROPTIONS;
  end;

  // -- TPanelSplitter, internal class, used by TSplitPanel. The two classes depend on eachother heavily
  TPanelSplitter = class (TCustomControl)
  private
    fParentPanel          : TSplitPanel;          // The panel on which te splitter is placed, which must be the parent
    fFirstPanel           : TSplitPanel;          // The panel left/top of the splitter
    fSecondPanel          : TSplitPanel;          // The panel right/down of the splitter
    fHighlightColor       : TColor;               // Mouse over color
    fSplitMode            : TSplitMode;
    fDragInfo             : TDragInfo;
    fDisabled             : Boolean;
    fStep                 : Integer;
    fResizeMode           : TDragResizeMode;
    fOldKeyDownEvent      : TKeyEvent;
    fControl              : TWinControl;
    fPendingResize        : Boolean;
    fLineDC               : HDC;                   // handle to draw line on
    fBrush                : TBrush;                // brush for line
    fPrevBrush            : HBrush;                // used brush
    fLineVisible          : Boolean;
    fReady                : Boolean;
    fTimerActive          : Boolean;
    fResizeTimerInterval  : Cardinal;
    fHighlighted          : Boolean;
    procedure CMMouseEnter(var Msg:TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Msg:TMessage); message CM_MOUSELEAVE;
    procedure SetSplitMode(const Value: TSplitMode);
    function GetIndex: Integer;
    function FindPanels: Boolean;
    procedure AllocLineDC;
    procedure ReleaseLineDC;
    procedure DrawLine(aMode: TInternalSplitterDrawMode);
    procedure HandleKey(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure WMPaint(var Msg: TWMPaint); message WM_PAINT;
    procedure WMTimer(var Msg: TWMTimer); message WM_TIMER;
    procedure WMNCPaint(var Message: TMessage); message WM_NCPAINT;
    procedure SetResizeTimerInterval(const Value: Cardinal);
    procedure SetHighlighted(const Value: Boolean);
  protected
    procedure DoResizePanel(X, Y: Integer);
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure SetParent(aControl: TWinControl); override;
    property SplitMode: TSplitMode read fSplitMode write SetSplitMode;
    property Highlighted: Boolean read fHighlighted write SetHighlighted;
    property Index: Integer read GetIndex;
    property IsDragging: Boolean read fDragInfo.iDragging;
    property FirstPanel: TSplitPanel read fFirstPanel;
    property SecondPanel: TSplitPanel read fSecondPanel;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property Disabled: Boolean read fDisabled write fDisabled;
    property Color;
    property HighlightColor: TColor read fHighlightColor write fHighlightColor;
    property ResizeTimerInterval: Cardinal read fResizeTimerInterval write SetResizeTimerInterval;
  end;

  {-------------------------------------------------------------------------------
   -- TSplitPanelPropertyManager. Class to set properties for SplitPanels and its
   child panels.
   It is possible to add new shared properties to a descendant class:
   1) add the property
   2) add a SPF_XXXX flag which is higher than the last one declared in this unit
   3) overwrite UpdatePanel, CopyProps, SameProps. (always call inherited)
  -------------------------------------------------------------------------------}
  TSplitPanelPropertyManager = class(TComponent)
  private
  { internal }
    fUpdateCount       : Integer;
  { shared properties }
    fDistance          : Integer;
    fPanels            : TSplitPanelList;
    fBevelInner        : TBevelCut;
    fBevelOuter        : TBevelCut;
    fBevelKind         : TBevelKind;
    fBevelWidth        : TBevelWidth;
    fBorderWidth       : Integer;
    fBevelEdges        : TBevelEdges;
    fBorderStyle       : TBorderStyle;
    fDivideMode        : TDivideMode;
    fDragResizeMode    : TDragResizeMode;
    fOptions           : TSplitPanelOptions;
    fSplitterOptions: TSplitterOptions;
  { internal methods }
    procedure AddPanel(P: TSplitPanel);
    procedure RemovePanel(P: TSplitPanel);
  { property access }
    procedure SetDistance(Value: Integer);
    procedure SetBevelInner(const Value: TBevelCut);
    procedure SetBevelKind(const Value: TBevelKind);
    procedure SetBevelOuter(const Value: TBevelCut);
    procedure SetBevelWidth(const Value: TBevelWidth);
    function GetAllEqual: Boolean;
    procedure SetAllEqual(const Value: Boolean);
    procedure SetBevelEdges(const Value: TBevelEdges);
    procedure SetBorderStyle(const Value: TBorderStyle);
    procedure SetBorderWidth(const Value: Integer);
    procedure SetDivideMode(const Value: TDivideMode);
    procedure SetDragResizeMode(const Value: TDragResizeMode);
    procedure SetOptions(const Value: TSplitPanelOptions);
    function GetQuickBorder: TQuickBorderStyle;
    procedure SetQuickBorder(const Value: TQuickBorderStyle);
    procedure SetSplitterOptions(const Value: TSplitterOptions);
  protected
    procedure UpdatePanel(P: TSplitPanel; Flags: Cardinal); virtual;
    procedure CopyProps(P: TSplitPanel; Flags: Cardinal); virtual;
    function SameProps(P: TSplitPanel; Flags: Cardinal): Boolean; virtual;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure Changed;
    procedure UpdatePanels(Flags: Cardinal);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    property AllEqual: Boolean read GetAllEqual write SetAllEqual stored False;
  { shared properties with TSplitPanel }
    property BevelEdges: TBevelEdges read fBevelEdges write SetBevelEdges default DEF_BEVELEDGES;
    property BevelInner: TBevelCut read fBevelInner write SetBevelInner default DEF_BEVELINNER;
    property BevelOuter: TBevelCut read fBevelOuter write SetBevelOuter default DEF_BEVELOUTER;
    property BevelKind: TBevelKind read fBevelKind write SetBevelKind default DEF_BEVELKIND;
    property BevelWidth: TBevelWidth read fBevelWidth write SetBevelWidth default DEF_BEVELWIDTH;
    property BorderWidth: Integer read fBorderWidth write SetBorderWidth default DEF_BORDERWIDTH;
    property BorderStyle: TBorderStyle read fBorderStyle write SetBorderStyle default DEF_BORDERSTYLE;
    property Distance: Integer read fDistance write SetDistance default DEF_DISTANCE;
    property DragResizeMode: TDragResizeMode read fDragResizeMode write SetDragResizeMode default DEF_DRAGRESIZEMODE;
    property Options: TSplitPanelOptions read fOptions write SetOptions default DEF_OPTIONS;
    property DivideMode: TDivideMode read fDivideMode write SetDivideMode default DEF_DIVIDEMODE;
    property SplitterOptions: TSplitterOptions read fSplitterOptions write SetSplitterOptions default DEF_SPLITTEROPTIONS;
    property QuickBorder: TQuickBorderStyle read GetQuickBorder write SetQuickBorder stored False;
  end;

var
  GlobalLiveResizeTimerInterval: Cardinal = 0; // Set this var to something like 8 to have timed resize when dragging with splitter

resourcestring
  SHint_SplitPositionOnlyForChildPanels = 'Other SplitPosition values than spNone are only allowed for child panels';
  SHint_AlignNotAllowedForChildPanels = 'Other Align values than alNone are not allowed for ChildPanels';

implementation

uses
  Math, Dialogs,
  Types, TypInfo;

const
  RESIZE_TIMER = 1;

var
  UserCount: Integer = 0;
  PanelBitmap: TBitmap;

procedure UsesBitmaps;
begin
  if UserCount = 0 then
    PanelBitmap := TBitmap.Create;
  Inc(UserCount);
end;

procedure ReleaseBitmaps;
begin
  Dec(UserCount);
  if UserCount = 0 then
    PanelBitmap.Free;
end;

procedure BitmapResize(aBitmap: TBitmap; aWidth, aHeight: Integer);
begin
  aWidth := EnsureRange(aWidth, 0, 2048);
  aHeight := EnsureRange(aHeight, 0, 2048);
  with aBitmap do
  begin
    Width := Max(Width, aWidth);
    Height := Max(Height, aHeight);
  end;
end;

procedure CopyCanvas(Target: TCanvas; const TargetRect: TRect; Source: TCanvas; const SourceRect: TRect);
{-------------------------------------------------------------------------------
  Simple canvas copyrect
-------------------------------------------------------------------------------}
begin
  with TargetRect do
    BitBlt(Target.Handle,
           Left, Top, Right - Left, Bottom - Top,
           Source.Handle,
           SourceRect.Left, SourceRect.Top,
           cmSrcCopy);
end;

function ComparePanelSizesLargeFirst(Item1, Item2: Pointer): Integer;
{-------------------------------------------------------------------------------
  compare function for panellist
-------------------------------------------------------------------------------}
var
  A: TSplitPanel absolute Item1;
  B: TSplitPanel absolute Item2;
begin
  Result := A.CalculatedSize - B.CalculatedSize;
end;

function ComparePanelSizesSmallFirst(Item1, Item2: Pointer): Integer;
{-------------------------------------------------------------------------------
  compare function for panellist
-------------------------------------------------------------------------------}
var
  A: TSplitPanel absolute Item1;
  B: TSplitPanel absolute Item2;
begin
  Result := B.CalculatedSize - A.CalculatedSize;
end;

{ TSplitPanelList }

function TSplitPanelList.Add(Item: TSplitPanel): Integer;
begin
  Assert(IndexOf(Item) = -1, 'SplitPanelList.Add error');
  Result := inherited Add(Item);
end;

function TSplitPanelList.GetItem(Index: Integer): TSplitPanel;
begin
  Result := inherited Get(Index);
end;

procedure TSplitPanelList.SortBySize(SmallFirst: Boolean);
begin
  if SmallFirst then
    Sort(ComparePanelSizesSmallFirst)
  else
    Sort(ComparePanelSizesLargeFirst)
end;

{ TPanelSplitterList }

function TPanelSplitterList.Add(Item: TPanelSplitter): Integer;
begin
  Assert(IndexOf(Item) = -1, 'PanelSplitterList.Add error');
  Result := inherited Add(Item);
end;

function TPanelSplitterList.GetItem(Index: Integer): TPanelSplitter;
begin
  Result := inherited Get(Index);
end;

{ TSplitPanel }

procedure TSplitPanel.AddPanel(P: TSplitPanel);
begin
  if csDestroying in ComponentState then
    Exit;
  fPanels.Add(P);
  UpdateLists;
end;

procedure TSplitPanel.AlignControls(AControl: TControl; var Rect: TRect);
begin
  if pcsUpdatingLists in fStates then
    Exit;
  if aControl is TPanelSplitter then
    Exit;
  Assert(HandleAllocated);  
  if fPanels.Count > 0 then
    AlignPanels(AControl, Rect)
  else
    inherited AlignControls(AControl, Rect);
end;

procedure TSplitPanel.AlignPanels(aControl: TControl; var aRect: TRect);
type
  TAlignAction = (
    aaNone,          // Do nothing
    aaDefault,       // Just correct childbounds to fit in
    aaScalePanels,   // Keep scale
    aaEqualDivide,   // Equally divide
    aaSplitter,      // User dragged splitter
    aaAutoSize
  );
var
  ThePanel: TSplitPanel absolute aControl;
  P: TSplitPanel;
  SelfWidth, SelfHeight, SelfSize: Integer;
  AvailablePanelSize, UsedPanelSize: Integer;
  AlignAction: TAlignAction;

    procedure InitLocals;
    {-------------------------------------------------------------------------------
      Local proc to initialize vars
    -------------------------------------------------------------------------------}
    var
      i: Integer;
      Loading: Boolean;
      P: TSplitPanel;
    begin
      // Order of alignment: client,none,last,first
      fAlignList.Clear;
      for i := fVisiblePanels.Count - 1 downto 0 do
      begin
        P := fVisiblePanels[i];
        if P.SplitPosition = spClient then fAlignList.Add(P);
      end;
      for i := fVisiblePanels.Count - 1 downto 0 do
      begin
        P := fVisiblePanels[i];
        if P.SplitPosition = spNone then fAlignList.Add(P);
      end;
      for i := fVisiblePanels.Count - 1 downto 0 do
      begin
        P := fVisiblePanels[i];
        if P.SplitPosition = spLast then fAlignList.Add(P);
      end;
      for i := fVisiblePanels.Count - 1 downto 0 do
      begin
        P := fVisiblePanels[i];
        if P.SplitPosition = spFirst then fAlignList.Add(P);
      end;

      Loading := (csLoading in ComponentState) or (csReading in ComponentState);
      AlignAction := aaNone;

      if Loading then
        AlignAction := aaNone
      else if (pcsSplitterDrag in fStates) and (fSplitterDragDelta <> 0) and (fDraggingSplitter <> nil) then
        AlignAction := aaSplitter
      else if AutoSize and (fParentPanel = nil) then
        AlignAction := aaAutoSize
      else if (fLastAlignCount <> fVisiblePanels.Count) or (fDivideMode = dmEqual) or (pcsEqualDivideNeeded in fStates) then
        AlignAction := aaEqualDivide
      else if (fDivideMode = dmScaled) and (aControl = nil) then
        AlignAction := aaScalePanels
      else
        AlignAction := aaDefault;

      // Ensure scale
      if AlignAction = aaScalePanels then
        for i := 0 to fVisiblePanels.Count - 1 do
        begin
          P := fVisiblePanels[i];
          if P.fCurrentScale = 0 then
          begin
            RecalcScales;
            Break;
          end;
        end;

      fLastAlignCount := fVisiblePanels.Count;
      Exclude(fStates, pcsEqualDivideNeeded);

      // put the resized one at the end
      if aControl is TSplitPanel then
      if aControl.Visible then
      begin
        Assert(fAlignList.IndexOf(ThePanel) >= 0, 'alignerror');
        fAlignList.Remove(ThePanel);
        fAlignList.Add(ThePanel);
      end;
      fCorrectList.Assign(fAlignList);

      // The safest is to always work with the calculated sizes. We want to have the client area here.
      SelfWidth := GetCalculatedClientWidth;
      SelfHeight := GetCalculatedClientHeight;

      case fSplitMode of
        smVertical   : SelfSize := SelfHeight;
        smHorizontal : SelfSize := SelfWidth;
      end;

      UsedPanelSize := 0;

      AvailablePanelSize := SelfSize - (fVisiblePanels.Count - 1) * fDistance;

      for i := 0 to fAlignList.Count - 1 do
      begin
        P := fAlignList[i];
        Inc(UsedPanelSize, P.CalculatedSize);
      end;

    end;

    function Correct(aList: TSplitPanelList; ToDoRequested: Integer; Scaled: Boolean = False): Integer;
    {-------------------------------------------------------------------------------
      Correct bounds
    -------------------------------------------------------------------------------}
    var
      i, Done, ToDo, Diff: Integer;
      P: TSplitPanel;
      OldSize, NewSize: Integer;
      List: TSplitPanelList;
    begin

      Result := 0;
      if ToDoRequested = 0 then
        Exit;

      ToDo := ToDoRequested;
      Done := 0;
      List := TSplitPanelList.Create;

      try
        List.Assign(aList);

        // Panels need to grow
        if ToDo > 0 then
        begin
          if Scaled then
            List.SortBySize(False); // Small at the end
          for i := 0 to List.Count - 1 do
          begin
            P := List[i];
            OldSize := P.CalculatedSize;
            P.Size := OldSize + ToDo;
            NewSize := P.CalculatedSize;
            Assert(NewSize >= OldSize, 'correct grow logical error');
            Diff := NewSize - OldSize;
            Inc(Done, Diff);
            Inc(Result, Diff);
            Dec(ToDo, Diff);
            if ToDo <= 0 then
              Break;
          end;
        end
        // Panels need to shrink
        else if ToDo < 0 then
        begin
          if Scaled then
            List.SortBySize(True); // Small first
          for i := 0 to List.Count - 1 do
          begin
            P := List[i];
            OldSize := P.CalculatedSize;
            P.Size := OldSize + {-} ToDo;
            NewSize := P.CalculatedSize;
            Assert(NewSize <= OldSize, 'correct shrink logical error');
            Diff := OldSize - NewSize;
            Inc(Done, Diff);
            Dec(Result, Diff);
            Inc(ToDo, Diff);
            if ToDo >= 0 then
              Break;
          end;
        end;

      finally
        List.Free;
      end;
    end;

    procedure HandleEqualDivide;
    var
      i, S, AvgWidth, AvgHeight, AvgSize: Integer;
      ToDo: Integer;
      ModM: Integer;
    begin
      AvgWidth := AvailablePanelSize div fVisiblePanels.Count;
      AvgHeight := AvailablePanelSize div fVisiblePanels.Count;
      AvgSize := 0; // prevent compiler warning
      case fSplitMode of
        smVertical   : AvgSize := AvgHeight;
        smHorizontal : AvgSize := AvgWidth;
      end;

      ToDo := AvailablePanelSize;
      ModM := AvailablePanelSize mod AvgSize;

      for i := 0 to fAlignList.Count - 1 do
      begin
        P := fAlignList[i];
        if i = 0 then
          S := AvgSize + ModM
        else
          S := AvgSize;
        P.Size := S;
        if P.CalculatedSize <> S then
          fCorrectList.Remove(P);
        Dec(ToDo, P.CalculatedSize);
      end;

      Correct(fCorrectList, ToDo);
    end;

    procedure HandleDefault;
    var
      ToDo: Integer;
    begin
      ToDo := AvailablePanelSize - UsedPanelSize;
      Correct(fCorrectList, ToDo);
    end;

    procedure HandleAutoSize;
    var
      P: TSplitPanel;
    begin
      HandleDefault;
      {
      case fSplitMode of
        smHorizontal: Width := CalcSumSizes(fVisiblePanels) + fDistance * (fVisiblePanels.Count - 1);
        smVertical: Height := CalcSumSizes(fVisiblePanels) + fDistance * (fVisiblePanels.Count - 1);
      end;
      }
    end;

    procedure HandleSplitter;
    {-------------------------------------------------------------------------------
      Adjust panels after resizing with a splitter.
    -------------------------------------------------------------------------------}
    var
      T, i, Delta: Integer;
      P1, P2: TSplitPanel;
      Index1, Index2: Integer;
      Done1, Done2, Done3: Integer;
    begin
      P1 := fDraggingSplitter.fFirstPanel;
      P2 := fDraggingSplitter.fSecondPanel;
      fAlignList.Assign(fVisiblePanels);
      Delta := fSplitterDragDelta; // this is the number of pixels we need to redivide
      Index1 := fAlignList.IndexOf(P1);
      Index2 := fAlignList.IndexOf(P2);

      if Delta < 0 then
      begin
        // 1. Adjust panels above/left, including the resized panel, which will be the first one in the list
        fCorrectList.Clear;
        for i := Index1 downto 0 do
        begin
          fCorrectList.Add(fAlignList[i]);
          if (poNoDragPushing in fOptions) and (fCorrectList.Count = 1) then Break;
        end;
        Done1 := Correct(fCorrectList, Delta);

        // 2. Adjust panels down/right
        fCorrectList.Clear;
        for i := Index2 to fAlignList.Count - 1 do
        begin
          fCorrectList.Add(fAlignList[i]);
          if (poNoDragPushing in fOptions) and (fCorrectList.Count = 1) then Break;
        end;
        T := AvailablePanelSize - CalcSumSizes(fVisiblePanels);
        Done2 := Correct(fCorrectList, -Done1{T});
      end
      else begin
        // 1. Adjust panels down/right
        fCorrectList.Clear;
        for i := Index2 to fAlignList.Count - 1 do
        begin
          fCorrectList.Add(fAlignList[i]);
          if (poNoDragPushing in fOptions) and (fCorrectList.Count = 1) then Break;
        end;
        Done1 := Correct(fCorrectList, -Delta);

        // 2. Adjust panels above/left, including the resized panel, which will be the first one in the list
        fCorrectList.Clear;
        for i := Index1 downto 0 do
        begin
          fCorrectList.Add(fAlignList[i]);
          if (poNoDragPushing in fOptions) and (fCorrectList.Count = 1) then Break;
        end;
        T := AvailablePanelSize - CalcSumSizes(fVisiblePanels);
        Done2 := Correct(fCorrectList, -Done1{T});
      end;


      // the two parts on each side of the splitter have succcesfully been resized
//      if Abs(Done1) = Abs(Done2) then
  //      Exit;
      // C. If needed then correct the first step. But watch out: We already messed up!
      T := AvailablePanelSize - CalcSumSizes(fVisiblePanels);
      if T <> 0 then
      begin
        fCorrectList.Clear;
        for i := Index1 downto 0 do
        begin
          fCorrectList.Add(fAlignList[i]);
          if (poNoDragPushing in fOptions) and (fCorrectList.Count = 1) then Break;
        end;
        Done3 := Correct(fCorrectList, T);
        //Log([delta, done1, done2, done3]);
      end;
    end;

    procedure HandleScalePanels;
    {-------------------------------------------------------------------------------
      When bounds have changed, redevide the childpanels
    -------------------------------------------------------------------------------}
    var
      i: Integer;
      P: TSplitPanel;
      H: Double;
      Used: Integer;
    begin
      if UsedPanelSize = 0 then
        Exit;
      Used := 0;
      for i := 0 to fVisiblePanels.Count - 1 do
      begin
        P := VisiblePanels[i];
        H := SelfSize * P.fCurrentScale;
        P.Size := Round(H);
        Inc(Used, P.CalculatedSize);
      end;
      i := Correct(fVisiblePanels, AvailablePanelSize - Used, True);
    end;

    procedure CancelChanges;
    var
      i: Integer;
    begin
      for i := 0 to fVisiblePanels.Count - 1 do
        with fVisiblePanels[i] do
        begin
          fCalculatedWidth := Width;
          fCalculatedHeight := Height;
        end;
    end;

    procedure ApplyChanges;
    {-------------------------------------------------------------------------------
      Now apply all the calculated changes. Notice that we must do this with the
      pcsCalculatingChildPanelBounds cleared from fStates (see WMWindowPosChanging)
    -------------------------------------------------------------------------------}
    var
      i, N: Integer;
      P: TSplitPanel;
      S: Integer;
    begin
      N := 0;
      case SplitMode of
        smVertical:
          for i := 0 to fVisiblePanels.Count - 1 do
          begin
            P := fVisiblePanels[i];
            S := P.CalculatedSize;
            P.SetBounds(0, N, SelfWidth, S);
            Inc(N, P.Height + fDistance);
          end;
        smHorizontal:
          for i := 0 to fVisiblePanels.Count - 1 do
          begin
            P := fVisiblePanels[i];
            S := P.CalculatedSize;
            P.SetBounds(N, 0, S, SelfHeight);
            Inc(N, P.Width + fDistance);
          end;
      end;

      // Recalculate scales here if needed
      if AlignAction = aaEqualDivide then
        if fDivideMode <> dmEqual then
          RecalcScales;
    end;

    function CheckDisableSplitter(S: TPanelSplitter): Boolean;
    {-------------------------------------------------------------------------------
      Checks for the most common situations for disabling a splitter.
      We assume here that Autosize will really do something
    -------------------------------------------------------------------------------}
    begin
       Result := True;

      if (csDesigning in ComponentState) then
        Exit;
      if DivideMode = dmEqual then
        Exit;

      // firstpanel is the first + fixed
//      if S.FirstPanel.AutoSize
  //    or
      if ( (S.FirstPanel.FixedSize > 0) and (S.FirstPanel.VisibleIndex = 0) ) then
        Exit;

      // secondpanel is last + fixed
//      if S.SecondPanel.AutoSize
  //    or
      if ( (S.SecondPanel.FixedSize > 0) and (S.SecondPanel.VisibleIndex = fVisiblePanels.Count - 1) ) then
        Exit;

      Result := False;
    end;

    procedure RepositionSplitters;
    {-------------------------------------------------------------------------------
      This will put all splitters in the right place
    -------------------------------------------------------------------------------}
    var
      P: TSplitPanel;
      S: TPanelSplitter;
      X, Y, W, H, i: Integer;

    begin
      if fVisiblePanels.Count <= 1 then Exit;
      Assert(fVisiblePanels.Count - 1 = fVisibleSplitters.Count, Format('RepositionSplitters error %d, %d', [fVisiblePanels.Count, fVisibleSplitters.Count]));
      for i := 1 to fVisiblePanels.Count - 1 do
      begin
        S := fVisibleSplitters[i - 1];
        P := VisiblePanels[i - 1];
        case SplitMode of
          smVertical:
            begin
              X := 0;
              Y := P.Top + P.Height;
              H := fDistance;
              W := ClientWidth;
              S.fDisabled := CheckDisableSplitter(S);
              S.SplitMode := SplitMode;
              S.fResizeMode := Self.fDragResizeMode;
              S.SetBounds(X, Y, W, H);
            end;
          smHorizontal:
            begin
              Y := 0;
              X := P.Left + P.Width;
              W := fDistance;
              H := ClientHeight;
              S.fDisabled := CheckDisableSplitter(S);
              S.SplitMode := SplitMode;
              S.fResizeMode := Self.fDragResizeMode;
              S.SetBounds(X, Y, W, H);
            end;
        end;
      end;
    end;


begin
  if not HandleAllocated or (csDestroying in ComponentState) then
    Exit;
  if fVisiblePanels.Count = 0 then
    Exit;

//  Log(['begin ' + stringofchar('.', level) + i2s(level) + iif(visibleindex>=0, ','+i2s(visibleindex),''), 'WwHh=', width,calculatedwidth, height, CalculatedHeight,'color=',colortostring(color)]);

  // Enter panel aligning mode
  Include(fStates, pcsAligningChildPanels);
  try

    // Enter calculating mode, to prevent actual resizing (see also WMWindowPosChanging)
    Include(fStates, pcsCalculatingChildPanelBounds);
    try
      InitLocals;

      case AlignAction of
        aaNone         : Exit;
        aaDefault      : HandleDefault;
        aaEqualDivide  : HandleEqualDivide;
        aaSplitter     : HandleSplitter;
        aaScalePanels  : HandleScalePanels;
        aaAutoSize     : HandleAutoSize;
      end;
    finally
      Exclude(fStates, pcsCalculatingChildPanelBounds);
    end;

    ApplyChanges;
    RepositionSplitters;
    if pcsSplitterDrag in fStates then
      Update; // this prevents flickering on win xp, when liveresizing

  finally
    Exclude(fStates, pcsAligningChildPanels);
//    Log(['end   ' + stringofchar('.', level) + i2s(level) + iif(visibleindex>=0, ','+i2s(visibleindex),'')]);
  end;

end;

procedure TSplitPanel.RemovePanel(P: TSplitPanel);
begin
  if csDestroying in ComponentState then
    Exit;
  fPanels.Remove(P);
  UpdateLists;
end;


constructor TSplitPanel.Create(aOwner: TComponent);
begin
  UsesBitmaps;
  inherited Create(aOwner);

  ControlStyle := [csAcceptsControls, csCaptureMouse, csClickEvents, csOpaque, csDoubleClicks, csReplicatable];
  if ThemeServices.ThemesEnabled then
    ControlStyle := ControlStyle + [csParentBackground] - [csOpaque];

  fPanelClass := TSplitPanelClass(Self.ClassType);
  fRealConstraints := TSizeConstraints.Create(nil);
  fOldConstraintsEvent := Constraints.OnChange;
  Constraints.OnChange := NewConstraintsOnChange;
  fPanels := TSplitPanelList.Create;
  fVisiblePanels := TSplitPanelList.Create;
  fAlignList := TSplitPanelList.Create;
  fCorrectList := TSplitPanelList.Create;
  fSplitters := TPanelSplitterList.Create;
  fVisibleSplitters := TPanelSplitterList.Create;
  ResetMethods(smVertical); // initialize methods to ensure they are assigned

  // Initialize default values
  fDragResizeMode       := DEF_DRAGRESIZEMODE;
  Align                 := alNone;
  fOptions              := DEF_OPTIONS;
  fDivideMode           := DEF_DIVIDEMODE;
  fSplitterOptions      := DEF_SPLITTEROPTIONS;


  BevelKind             := DEF_BEVELKIND;
  BevelInner            := DEF_BEVELINNER;
  BevelOuter            := DEF_BEVELOUTER;
  BevelWidth            := DEF_BEVELWIDTH;
  BorderWidth           := DEF_BORDERWIDTH;
  fBorderStyle          := DEF_BORDERSTYLE;
  fDistance             := DEF_DISTANCE;
  Constraints.MinWidth  := MIN_PANELSIZE;
  Constraints.MinHeight := MIN_PANELSIZE;
  Width                 := DEF_PANELWIDTH;
  Height                := DEF_PANELHEIGHT;
  fCalculatedWidth      := Width;
  fCalculatedHeight     := Height;
end;

destructor TSplitPanel.Destroy;
begin
  try
    PropertyManager := nil;
    fPanels.Free;
    fVisiblePanels.Free;
    fAlignList.Free;
    fCorrectList.Free;
    fSplitters.Free;
    fVisibleSplitters.Free;
    fRealConstraints.Free;
    if fParentPanel <> nil then
      fParentPanel.RemovePanel(Self);
    inherited;
  finally
    ReleaseBitmaps;
  end;
end;

procedure TSplitPanel.ForEachPanel(LocalProc: Pointer; BackWards: Boolean = False);
{-------------------------------------------------------------------------------
  LocalProc = Locale (!) procedure met de volgende declaratie:
  procedure DoPanel(P: TSplitPanel); far;
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  i: Integer;
  P: TSplitPanel;
begin
  if fPanels.Count = 0 then
    Exit;
  { Set up stack frame for local }
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  if not BackWards then
    for i := 0 to fPanels.Count - 1 do
    begin
      P := Panels[i];
      asm
        PUSH CallerBP
        MOV EAX, P
        CALL LocalProc
        POP ECX
      end;
    end

  else
    for i := fPanels.Count - 1 downto 0 do
    begin
      P := Panels[i];
      asm
        PUSH CallerBP
        MOV EAX, P
        CALL LocalProc
        POP ECX
      end;
    end
end;

procedure TSplitPanel.ForEachVisiblePanel(LocalProc: Pointer; BackWards: Boolean = False);
{-------------------------------------------------------------------------------
  LocalProc = Locale (!) procedure met de volgende declaratie:
  procedure DoPanel(P: TSplitPanel); far;
-------------------------------------------------------------------------------}
var
  CallerBP: Cardinal;
  i: Integer;
  P: TSplitPanel;
begin
  if fVisiblePanels.Count = 0 then
    Exit;
  { Set up stack frame for local }
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  if not BackWards then
    for i := 0 to fVisiblePanels.Count - 1 do
    begin
      P := fVisiblePanels[i];
      asm
        PUSH CallerBP
        MOV EAX, P
        CALL LocalProc
        POP ECX
      end;
    end

  else
    for i := fVisiblePanels.Count - 1 downto 0 do
    begin
      P := fVisiblePanels[i];
      asm
        PUSH CallerBP
        MOV EAX, P
        CALL LocalProc
        POP ECX
      end;
    end;
end;

function TSplitPanel.GetClientRect: TRect;
begin
  if Parent <> nil then
    Result := inherited GetClientRect
  else
    Result := BoundsRect;
end;

function TSplitPanel.GetPanel(Index: Integer): TSplitPanel;
begin
  Result := fPanels[Index];
end;

function TSplitPanel.GetRoot: TSplitPanel;
var
  P: TSplitPanel;
begin
  Result := Self;
  P := Self;
  while P.ParentPanel <> nil do
  begin
    P := P.ParentPanel;
    Result := P;
  end;
end;

function TSplitPanel.GetMyPropertyManager: TSplitPanelPropertyManager;
var
  P: TSplitPanel;
begin
  P := Self;
  Result := Self.PropertyManager;
  while P.ParentPanel <> nil do
  begin
    P := P.ParentPanel;
    if P.PropertyManager <> nil then
      Result := P.PropertyManager;
  end;
end;


function TSplitPanel.InternalGetWidth: Integer;
begin
  Result := Width;
end;

procedure TSplitPanel.InternalSetWidth(Value: Integer);
begin
  Width := Value;
end;

function TSplitPanel.InternalGetHeight: Integer;
begin
  Result := Height;
end;

procedure TSplitPanel.InternalSetHeight(Value: Integer);
begin
  Height := Value;
end;

function TSplitPanel.InternalGetLeft: Integer;
begin
  Result := Left;
end;

procedure TSplitPanel.InternalSetLeft(Value: Integer);
begin
  Left := Value;
end;

function TSplitPanel.InternalGetTop: Integer;
begin
  Result := Top;
end;

procedure TSplitPanel.InternalSetTop(Value: Integer);
begin
  Top := Value;
end;

function TSplitPanel.NewPanel: TSplitPanel;
var
  Root: TSplitPanel;
  TheOwner: TComponent;
begin
  Root := GetRoot;
  if Root <> nil then
    TheOwner := Root.Owner
  else
    TheOwner := Self;
  Result := fPanelClass.Create(TheOwner);
  Result.Parent := Self;
end;

procedure TSplitPanel.Paint;
var
  Window: TRect;
begin
  if fPaintLockCount > 0 then
    Exit;
  if not IsRectEmpty(fUpdateRect) then
  begin
    Window := fUpdateRect;
    PaintPanel(Canvas, Window);
  end;
end;

procedure TSplitPanel.PaintPanel(aCanvas: TCanvas; aWindow: TRect);
{-------------------------------------------------------------------------------
  Main paint routine
-------------------------------------------------------------------------------}
var
  R: TRect;
  C: TCanvas;
  PaintInfo: TPaintInfo;
begin
  if fPaintLockCount > 0 then
    Exit;

  R := ClientRect;

  if poUseInternalPaintBuffer in fOptions then
  begin
    BitmapResize(PanelBitmap, R.Right - R.Left, R.Bottom - R.Top);
    C := PanelBitmap.Canvas;
  end
  else
    C := aCanvas;

  PaintInfo.TargetCanvas := C;
  PaintInfo.PanelRect := R;
  PaintInfo.ContentRect := R;
  InflateRect(PaintInfo.ContentRect, -2, -2);

  PaintBody(PaintInfo);

  if poShowCaption in fOptions then
    PaintCaption(PaintInfo);

  if poUseInternalPaintBuffer in fOptions then
    CopyCanvas(aCanvas, aWindow, C, aWindow);

end;

procedure TSplitPanel.PaintBody(const PaintInfo: TPaintInfo);
var
  C: TCanvas absolute PaintInfo;
  R: TRect;
begin
  with PaintInfo do
  begin
    R := PanelRect;
    C.Brush.Color := Self.Color;
    C.Brush.Style := bsSolid;
    C.FillRect(R);
  end;
end;

procedure TSplitPanel.PaintCaption(const PaintInfo: TPaintInfo);
var
  C: TCanvas absolute PaintInfo;
  S: string;
  R: TRect;
begin
  S := Caption;
  if S <> '' then
    with PaintInfo do
    begin
      R := ContentRect;
      C.Font := Self.Font;
      SetBkMode(C.Handle, TRANSPARENT);
      DrawTextEx(C.Handle, PChar(S), Length(S), R, DT_VCENTER + DT_SINGLELINE + DT_CENTER, nil);
    end;
end;

procedure TSplitPanel.RequestAlign;
begin
  inherited RequestAlign;
end;

procedure TSplitPanel.SetDistance(Value: Integer);
begin
  Value := EnsureRange(Value, 0, MAX_PANELDISTANCE);
  if fDistance = Value then
    Exit;
  fDistance := Value;
  UpdateRealConstraints;
  Realign;
end;

procedure TSplitPanel.SetPanelCount(Value: Integer);
var
  Old, i: Integer;
begin
  Value := EnsureRange(Value, 0, MAX_CHILDPANELS);
  Old := fPanels.Count;
  if Value = Old then
    Exit;

  DisableAlign;
  try
    if Value > Old then
      for i := Old + 1 to Value do
        NewPanel
    else
      while fPanels.Count <> Value do
        Panels[fPanels.Count - 1].Free;

  finally
    UpdateLists;
    ControlState := ControlState + [csAlignmentNeeded];
    //AdjustSize;
    EnableAlign;
  end;

end;


procedure TSplitPanel.SetParent(aControl: TWinControl);
begin
  if Parent = aControl then
    Exit;

  // panel is added to a parentpanel
  if aControl is TSplitPanel then
  begin
    inherited Align := alNone;

    if fParentPanel <> nil then
    begin
      fParentPanel.RemovePanel(Self);
      fParentPanel := nil;
    end;

    fParentPanel := TSplitpanel(aControl);
    fParentPanel.AddPanel(Self);
    ResetMethods(fParentPanel.fSplitMode);

  end
  // panel is added to another control
  else begin
    if fParentPanel <> nil then
      fParentPanel.RemovePanel(Self);
    fParentPanel := nil;
  end;

  inherited SetParent(aControl);

  // This is needed, because borders are handled different when panelcount is different
  if fParentPanel <> nil then
    fParentPanel.Perform(CM_BORDERCHANGED, 0, 0);

end;


procedure TSplitPanel.SetSplitMode(const Value: TSplitMode);
var
  i: Integer;
begin
  if fSplitMode = Value then
    Exit;
  fSplitMode := Value;

  if fPanels.Count > 0 then
  begin
    for i := 0 to fPanels.Count - 1 do
      Panels[i].ResetMethods(fSplitMode);
    Realign;
  end;
end;

procedure TSplitPanel.UpdateLists;

    procedure UpdatePanelOrder;
    {-------------------------------------------------------------------------------
      Local proc to update the panelorder according to the splitposition
    -------------------------------------------------------------------------------}
    var
      List: TSplitPanelList;

        // local sub proc to add to local list
        procedure DoAdd(aPos: TSplitPositions); far;
        var
          i: Integer;
          P: TSplitPanel;
        begin
          for i := 0 to fPanels.Count - 1 do
          begin
            P := Panels[i];
            if P.SplitPosition in aPos then
              List.Add(P);
          end;
        end;

        // local sub proc to update the main list
        procedure Sync;
        var
          i: Integer;
        begin
          Assert(List.Count = fPanels.Count, 'UpdateLists.UpdatePanelOrder.Sync error');
          for i := 0 to List.Count - 1 do
            fPanels.List^[i] := List[i];
        end;

    begin
      List := TSplitPanelList.Create;
      try
        DoAdd([spFirst]);
        DoAdd([spNone, spClient]);
        DoAdd([spLast]);
        Sync;
      finally
        List.Free;
      end;
    end;

    procedure UpdateVisiblePanelList;
    {-------------------------------------------------------------------------------
      Local proc to update the visible panels
    -------------------------------------------------------------------------------}
    var
      i: Integer;
      P: TSplitPanel;
    begin
      fVisiblePanels.Clear;
      for i := 0 to fPanels.Count - 1 do
      begin
        P := Panels[i];
        if P.Visible or (csDesigning in ComponentState) then
          fVisiblePanels.Add(P);
      end;
    end;

    procedure UpdateAlignList;
    {-------------------------------------------------------------------------------
      Local proc to refresh the resizelist
    -------------------------------------------------------------------------------}
    var
      aPosition: TSplitPosition;
      Back: Boolean;

        procedure DoPanel(P: TSplitPanel); far;
        begin
          if P.SplitPosition = aPosition then
            fAlignList.Add(P);
        end;

    begin
      fAlignList.Clear;
      Back := True;

      aPosition := spClient;
      ForEachVisiblePanel(@DoPanel, Back);

      aPosition := spNone;
      ForEachVisiblePanel(@DoPanel, Back);

      aPosition := spLast;
      ForEachVisiblePanel(@DoPanel, Back);

      aPosition := spFirst;
      ForEachVisiblePanel(@DoPanel, Back);

      Assert(fAlignList.Count = fVisiblepanels.Count, 'UpdateResizeList Error');
    end;

    procedure UpdateSplitterList;
    {-------------------------------------------------------------------------------
      Ensure the right amount of splitters.
      N.B. Setting the Parent of the splitters will recurse CMControlListChange, if
      a Child is being inserted
    -------------------------------------------------------------------------------}
    var
      i, SCount, PCount: Integer;
      NewCount: Integer;
      S: TPanelSplitter;
    begin
      PCount := fPanels.Count;
      SCount := fSplitters.Count;
      if PCount <= 1 then
        NewCount := 0
      else
        NewCount := PCount - 1;
      if NewCount = SCount then
        Exit;

      // create
      if NewCount > SCount then
      begin
        for i := SCount + 1 to NewCount do
        begin
          S := TPanelSplitter.Create(Self);
          S.Parent := Self; // this will automatically add the splitter to the list
        end
      end
      // remove
      else begin
        while fSplitters.Count <> NewCount do
        begin
          fSplitters[fSplitters.Count - 1].Free;
        end;
      end;
    end;

    procedure UpdateVisibleSplitterList;
    var
      i, Ix: Integer;
      P: TSplitPanel;
    begin
      fVisibleSplitters.Clear;

      for i := 1 to fVisiblePanels.Count - 1 do
      begin
        P := VisiblePanels[i - 1];
        Ix := fPanels.IndexOf(P);
        if Ix < fSplitters.Count then
          fVisibleSplitters.Add(fSplitters[Ix]);
      end;
    end;

    procedure UpdateSplitterRefs;
    {-------------------------------------------------------------------------------
      Reset the refs to the 2 panels that are the neighbours of each splitter
    -------------------------------------------------------------------------------}
    var
      S: TPanelSplitter;
      i: Integer;
      A, B: TSplitPanel;
    begin
      if fPanels.Count < 2 then
        Exit;
      Assert(fSplitters.Count = fPanels.Count - 1, 'RefreshSplitters error 1');
      // to be sure do all panels (???)
      for i := 1 to fPanels.Count - 1 do
      begin
        A := Panels[i - 1];
        B := Panels[i];
        S := fSplitters[i - 1];
        S.fFirstPanel := A;
        S.fSecondPanel := B;
        S.SplitMode := Self.SplitMode;
        if fVisibleSplitters.IndexOf(S) < 0 then
        begin
          S.Visible := False;
        end
        else
          S.Visible := True;
      end;

      // then reset if there are hidden panels
      if fVisiblePanels.Count < 2 then
        Exit;
      if fVisibleSplitters.Count = fSplitters.Count then
        Exit;
      Assert(fVisibleSplitters.Count = fVisiblePanels.Count - 1, 'RefreshSplitters error 2');
      for i := 1 to fVisiblePanels.Count - 1 do
      begin
        A := VisiblePanels[i - 1];
        B := VisiblePanels[i];
        S := fVisibleSplitters[i - 1];
        S.fFirstPanel := A;
        S.fSecondPanel := B;
        S.SplitMode := Self.SplitMode;
      end;
    end;

begin
  Include(fStates, pcsUpdatingLists);
  try
    UpdatePanelOrder;
    UpdateVisiblePanelList;
    UpdateAlignList;
    UpdateSplitterList;
    UpdateVisibleSplitterList;
    UpdateSplitterRefs;
    UpdateRealConstraints;
  finally
    Exclude(fStates, pcsUpdatingLists);
  end;
end;



procedure TSplitPanel.WMEraseBkgnd(var Msg: TWmEraseBkgnd);
begin
  Msg.Result := 1; // no erase background-nonsense
end;

procedure TSplitPanel.WMPaint(var Msg: TWMPaint);
begin
  GetUpdateRect(Handle, fUpdateRect, False);
  inherited;
end;

function TSplitPanel.GetSize: Integer;
begin
  Result := EvGetSize;
end;

procedure TSplitPanel.SetSize(Value: Integer);
begin
  EvSetSize(Value);
end;

procedure TSplitPanel.ResetMethods(aParentSplitMode: TSplitMode);
{-------------------------------------------------------------------------------
  Assign right events so that some properties (for example the property Size) returns the right value.
-------------------------------------------------------------------------------}
begin

  // Only execute when switched
  if (fMethodsSplitMode = aParentSplitMode) and Assigned(EvGetSize) then
    Exit;

  fMethodsSplitMode := aParentSplitMode;

  case aParentSplitMode of
    smVertical:
      begin
        EvGetSize           := InternalGetHeight;
        EvSetSize           := InternalSetHeight;
        EvGetPosition       := InternalGetTop;
        EvSetPosition       := InternalSetTop;
        EvGetCalculatedSize := InternalGetCalculatedHeight;
        EvGetMinSize       := InternalGetMinHeight;
        EvSetMinSize       := InternalSetMinHeight;
        EvGetMaxSize       := InternalGetMaxHeight;
        EvSetMaxSize       := InternalSetMaxHeight;
        EvGetRealMinSize   := InternalGetRealMinHeight;
        EvGetRealMaxSize   := InternalGetRealMaxHeight;
      end;
    smHorizontal:
      begin
        EvGetSize          := InternalGetWidth;
        EvSetSize          := InternalSetWidth;
        EvGetPosition      := InternalGetLeft;
        EvSetPosition      := InternalSetLeft;
        EvGetCalculatedSize := InternalGetCalculatedWidth;
        EvGetMinSize       := InternalGetMinWidth;
        EvSetMinSize       := InternalSetMinWidth;
        EvGetMaxSize       := InternalGetMaxWidth;
        EvSetMaxSize       := InternalSetMaxWidth;
        EvGetRealMinSize   := InternalGetRealMinWidth;
        EvGetRealMaxSize   := InternalGetRealMaxWidth;
      end;
  end;

  // switch constraints
  case aParentSplitMode of
    smVertical:
      begin
        DisableAlign;
        if Constraints.MinWidth > 0 then
        begin
          Constraints.MinHeight := Constraints.MinWidth;
          Constraints.MinWidth := 0;
        end;

        if Constraints.MaxWidth > 0 then
        begin
          Constraints.MaxHeight := Constraints.MaxWidth;
          Constraints.MaxWidth := 0;
        end;
        EnableAlign;
      end;
    smHorizontal:
      begin
        DisableAlign;
        if Constraints.MinHeight > 0 then
        begin
          Constraints.MinWidth := Constraints.MinHeight;
          Constraints.MinHeight := 0;
        end;

        if Constraints.MaxHeight > 0 then
        begin
          Constraints.MaxWidth := Constraints.MaxHeight;
          Constraints.MaxHeight := 0;
        end;
        EnableAlign;
      end;
  end;

end;

procedure TSplitPanel.CMVisibleChanged(var Message: TMessage);
{-------------------------------------------------------------------------------
  Ensure that the list of visible panels is in sync
  N.B: See also TControl.SetVisible: Message fires just before RequestAlign.
-------------------------------------------------------------------------------}
begin
  if not (csReading in ComponentState) then
    if fParentPanel <> nil then
      fParentPanel.UpdateLists;
  inherited;
end;


procedure TSplitPanel.SetSplitPosition(const Value: TSplitPosition);

    procedure DoClearClient(P: TSplitPanel); far;
    begin
      // Do not use the property SplitPosition, because of recursion
      if P.fSplitPosition = spClient then
        P.fSplitPosition := spNone;
    end;

begin
  if fSplitPosition = Value then
    Exit;

  if (fParentPanel = nil) and (Value <> spNone) then
  begin
    DesignHint(SHint_SplitPositionOnlyForChildPanels);
    Exit;
  end;

  // there can be only one spClient. So clear the other one
  if Value = spClient then
    fParentPanel.ForEachPanel(@DoClearClient);

  fSplitPosition := Value;
  fParentPanel.UpdateLists;
  RequestAlign;
end;

function TSplitPanel.GetPosition: Integer;
begin
  Result := EvGetPosition;
end;

procedure TSplitPanel.SetPosition(Value: Integer);
begin
  EvSetPosition(Value);
end;

procedure TSplitPanel.NewConstraintsOnChange(Sender: TObject);
{-------------------------------------------------------------------------------
  Overridden Constraints.OnChange.
  1. Correct illegal values. Recursion is avoided by a temporary disabling of the OnChange
     eventhandler.
  2. Recalculate the RealConstraints (including all parentpanels, see UpdateRealConstraints).
  3. Call the original eventhandler
  4. AdjustSize is needed to update the parentpanels new realconstraints
-------------------------------------------------------------------------------}
var
  Ev: TNotifyEvent;
begin
  Ev := Constraints.OnChange; { N.B. That's this method }
  Constraints.OnChange := nil;

  try
    // correct illegal values, which would result in non-logical screens
    if fParentPanel <> nil then
    begin
      case fParentPanel.SplitMode of
        smVertical:
          begin
            if Constraints.MinWidth > 0 then
              Constraints.MinWidth := MIN_PANELSIZE;
            if Constraints.MaxWidth > 0 then
              Constraints.MaxWidth := 0;
          end;
        smHorizontal:
          begin
            if Constraints.MinHeight > 0 then
              Constraints.MinHeight := MIN_PANELSIZE;
            if Constraints.MaxHeight > 0 then
              Constraints.MaxHeight := 0;
          end;
      end;
    end;

    // correct too small values
    if Constraints.MinHeight < MIN_PANELSIZE then
      Constraints.MinHeight := MIN_PANELSIZE;
    if Constraints.MinWidth < MIN_PANELSIZE then
      Constraints.MinWidth := MIN_PANELSIZE;

    UpdateRealConstraints;
    fOldConstraintsEvent(Sender);

    // needed to update according to the new realconstraints
    if fParentPanel <> nil then
      fParentPanel.AdjustSize;

  finally
    Constraints.OnChange := Ev;
  end;
end;

procedure TSplitPanel.UpdateRealConstraints;
{-------------------------------------------------------------------------------
  Adjust the RealConstraints: the sum of all the realconstraints of the childpanels
-------------------------------------------------------------------------------}
var
  i, Min: Integer;
begin
  RealConstraints.Assign(Constraints);

  if fPanels.Count > 0 then
  case SplitMode of
    smVertical:
      begin
        Min := fVisibleSplitters.Count * fDistance;
        for i := 0 to fVisiblePanels.Count - 1 do
          Inc(Min, VisiblePanels[i].RealConstraints.MinHeight);
        if Min > RealConstraints.MinHeight then
          RealConstraints.MinHeight := Min;

        for i := 0 to fVisiblePanels.Count - 1 do
        begin
          Min := VisiblePanels[i].RealConstraints.MinWidth;
          if Min > RealConstraints.MinWidth then
            RealConstraints.MinWidth := Min;
        end;

        // handle 1 childpanel exception
        if BorderWidth > 0 then
          if fVisiblePanels.Count = 1 then
            RealConstraints.MinHeight := RealConstraints.MinHeight + 2 * BorderWidth;
      end;
    smHorizontal:
      begin
        Min := fVisibleSplitters.Count * fDistance;
        for i := 0 to fVisiblePanels.Count - 1 do
          Inc(Min, VisiblePanels[i].RealConstraints.MinWidth);
        if Min > RealConstraints.MinWidth then
          RealConstraints.MinWidth := Min;

        for i := 0 to fVisiblePanels.Count - 1 do
        begin
          Min := VisiblePanels[i].RealConstraints.MinHeight;
          if Min > RealConstraints.MinHeight then
            RealConstraints.MinHeight := Min;
        end;

        // handle 1 childpanel exception
        if BorderWidth > 0 then
          if fVisiblePanels.Count = 1 then
            RealConstraints.MinWidth := RealConstraints.MinWidth + 2 * BorderWidth;
      end;
  end;

  // Now let the parentpanel do the same
  if ParentPanel <> nil then
    ParentPanel.UpdateRealConstraints;
end;


function TSplitPanel.InternalGetCalculatedHeight: Integer;
begin
  Result := fCalculatedHeight;
end;

function TSplitPanel.InternalGetCalculatedWidth: Integer;
begin
  Result := fCalculatedWidth;
end;

function TSplitPanel.GetCalculatedSize: Integer;
begin
  Result := EvGetCalculatedSize;
end;

function TSplitPanel.GetLevel: Integer;
var
  P: TSplitPanel;
begin
  Result := 0;
  P := Self;
  while P.ParentPanel <> nil do
  begin
    Inc(Result);
    P := P.ParentPanel;
  end;
end;

procedure TSplitPanel.SetOptions(const Value: TSplitPanelOptions);
var
  OldOptions: TSplitPanelOptions;
begin
  if fOptions = Value then
    Exit;

  OldOptions := fOptions;
  fOptions := Value;

  Invalidate;
end;

procedure TSplitPanel.WMWindowPosChanged(var Message: TWMWindowPosChanged);
{-------------------------------------------------------------------------------
  This message is called by Windows.SetWindowPos after the change of the window
-------------------------------------------------------------------------------}
begin
  inherited;
  // if Message.WindowPos^.flags and SWP_NOSIZE = 0 then
  // Always keep these vars in sync with the real width / height
  with Message.WindowPos^ do
  begin
    fCalculatedWidth := cx;
    fCalculatedHeight := cy;
  end;

  if Assigned(fParentPanel) then
  begin
    begin
      if not (pcsAligningChildPanels in fParentPanel.fStates)
      or (pcsSplitterDrag in fParentPanel.fStates) then
          fParentPanel.RecalcScales;
    end;
  end;
end;

procedure TSplitPanel.WMWindowPosChanging(var Message: TWMWindowPosChanging);
{-------------------------------------------------------------------------------
  Important part of the AlignPanel logic.
  This message is called by Windows.SetWindowPos before the change of the window.
  The Calls to SetBounds in AlignPanels during calculation trigger this event.
  We want the VCL logic to check constraints / autosize / resize events etc.
  But we DON'T want the application to really update the bounds and the screen yet.
  That is why we store the calculated width + height.
-------------------------------------------------------------------------------}
const
  TheFlags: Cardinal = SWP_NOZORDER + SWP_NOACTIVATE + SWP_NOSIZE + SWP_NOREDRAW + SWP_NOMOVE;
begin
  inherited;
  if (fParentPanel <> nil) and (pcsCalculatingChildPanelBounds in fParentPanel.fStates) then
    with Message.WindowPos^ do
    begin
      flags := flags or TheFlags;
      fCalculatedWidth := cx;
      fCalculatedHeight := cy;
    end;

  with Message.WindowPos^ do
  begin
    fCalculatedWidth := cx;
    fCalculatedHeight := cy;
  end;
end;

procedure TSplitPanel.SetBounds(aLeft, aTop, aWidth, aHeight: Integer);
{-------------------------------------------------------------------------------
  Every call to Setbounds during parent-calculation must result in a
  WM_WINDOWPOSCHANGING. The original TWinControl.SetBounds checks changes first.
  So we made a copy of the original, with some modifications.
-------------------------------------------------------------------------------}
var
  WindowPlacement: TWindowPlacement;

const
  TheFlags: Cardinal = SWP_NOZORDER + SWP_NOACTIVATE + {SWP_NOSIZE +} SWP_NOREDRAW{ + SWP_NOMOVE};

begin
  if (fParentPanel <> nil) and (pcsCalculatingChildPanelBounds in fParentPanel.fStates) then
  begin
    SetWindowPos(Handle, 0, aLeft, aTop, aWidth, aHeight, TheFlags);
  end
  else
    inherited SetBounds(aLeft, aTop, aWidth, aHeight);

  // if no win-messages then just sync
  if not HandleAllocated then
  begin
    fCalculatedWidth := Width;
    fCalculatedHeight := Height;
  end;

end;

procedure TSplitPanel.SplitterDragBegin(Sender: TPanelSplitter);

    procedure DoSave(P: TSplitPanel);
    var
      i: Integer;
    begin
      P.fBoundsBeforeDrag := P.BoundsRect;
      for i := 0 to P.fVisiblePanels.Count - 1 do
        DoSave(P.VisiblePanels[i]);
    end;

begin
  Include(fStates, pcsSplitterDrag);
  DoSave(Self);
end;

procedure TSplitPanel.SplitterDragEnd(Sender: TPanelSplitter);
begin
  Exclude(fStates, pcsSplitterDrag);
end;

procedure TSplitPanel.SplitterDragCancel(Sender: TPanelSplitter);

    procedure DoRestore(P: TSplitPanel);
    var
      i: Integer;
    begin
      P.DisableAlign;
      try
        P.BoundsRect := P.fBoundsBeforeDrag;
        for i := 0 to P.fVisiblePanels.Count - 1 do
          DoRestore(P.VisiblePanels[i]);
      finally
        P.EnableAlign;
      end;
    end;

var
  i: Integer;
begin
  if pcsSplitterDrag in fStates then
  begin
    Exclude(fStates, pcsSplitterDrag);
    for i := 0 to fSplitters.Count - 1 do
      fSplitters[i].fDragInfo.iDragging := False;
    DoRestore(Self);
  end;

  Exclude(fStates, pcsSplitterDrag);
end;

procedure TSplitPanel.SplitterDrag(Sender: TPanelSplitter; Delta: Integer);
begin
  fSplitterDragDelta := Delta;
  fDraggingSplitter := Sender;
  try
    Realign;
  finally
    fSplitterDragDelta := 0;
    fDraggingSplitter := nil;
  end;
end;

procedure TSplitPanel.AddSplitter(S: TPanelSplitter);
begin
  if csDestroying in ComponentState then
    Exit;
  fSplitters.Add(S);
end;

procedure TSplitPanel.RemoveSplitter(S: TPanelSplitter);
begin
  if csDestroying in ComponentState then
    Exit;
  fSplitters.Remove(S);
end;


function TSplitPanel.GetVisiblePanel(Index: Integer): TSplitPanel;
begin
  Result := fVisiblePanels[Index];
end;

function TSplitPanel.InternalGetMinWidth: TConstraintSize;
begin
  Result := Constraints.MinWidth;
end;

procedure TSplitPanel.InternalSetMinWidth(Value: TConstraintSize);
begin
  Constraints.MinWidth := Value;
end;

function TSplitPanel.InternalGetMinHeight: TConstraintSize;
begin
  Result := Constraints.MinHeight;
end;

procedure TSplitPanel.InternalSetMinHeight(Value: TConstraintSize);
begin
  Constraints.MinHeight := Value;
end;

function TSplitPanel.InternalGetMaxWidth: TConstraintSize;
begin
  Result := Constraints.MaxWidth;
end;

procedure TSplitPanel.InternalSetMaxWidth(Value: TConstraintSize);
begin
  Constraints.MaxWidth := Value;
end;

function TSplitPanel.InternalGetMaxHeight: TConstraintSize;
begin
  Result := Constraints.MaxHeight;
end;

procedure TSplitPanel.InternalSetMaxHeight(Value: TConstraintSize);
begin
  Constraints.MaxHeight := Value;
end;

function TSplitPanel.InternalGetRealMaxHeight: TConstraintSize;
begin
  Result := RealConstraints.MaxHeight;
end;

function TSplitPanel.InternalGetRealMaxWidth: TConstraintSize;
begin
  Result := RealConstraints.MaxWidth;
end;

function TSplitPanel.InternalGetRealMinHeight: TConstraintSize;
begin
  Result := RealConstraints.MinHeight;
end;

function TSplitPanel.InternalGetRealMinWidth: TConstraintSize;
begin
  Result := RealConstraints.MinWidth;
end;

function TSplitPanel.GetMaxSize: TConstraintSize;
begin
  Result := EvGetMaxSize;
end;

function TSplitPanel.GetMinSize: TConstraintSize;
begin
  Result := EvGetMinSize;
end;

procedure TSplitPanel.SetMaxSize(Value: TConstraintSize);
begin
  EvSetMaxSize(Value)
end;

procedure TSplitPanel.SetMinSize(Value: TConstraintSize);
begin
  EvSetMinSize(Value)
end;


function TSplitPanel.GetRealMaxSize: TConstraintSize;
begin
  Result := EvGetRealMaxSize;
end;

function TSplitPanel.GetRealMinSize: TConstraintSize;
begin
  Result := EvGetRealMinSize;
end;

function TSplitPanel.CalcSumSizes(aList: TSplitPanelList; aStartIndex: Integer = -1; aStopIndex: Integer = -1): Integer;
var
  i: Integer;
  P: TSplitPanel;
begin
  if aStartIndex < 0 then
    aStartIndex := 0;
  if aStopIndex < 0 then
    aStopIndex := aList.Count - 1;
  Result := 0;
  for i := aStartIndex to aStopIndex do
  begin
    P := aList[i];
    Inc(Result, P.CalculatedSize);
  end;
end;

procedure TSplitPanel.RecalcScales(DoChildren: Boolean = False);
var
  i, SelfSize: Integer;
  P: TSplitPanel;
begin
//  if not HandleAllocated then
  //  Exit;
  if fDivideMode <> dmScaled then
    Exit;
  SelfSize := 0; // prevent compiler warning
  case SplitMode of
    smVertical  : SelfSize := GetCalculatedClientHeight;
    smHorizontal: SelfSize := GetCalculatedClientWidth;
  end;

  for i := 0 to fVisiblePanels.Count - 1 do
  begin
    P := VisiblePanels[i];
    P.fCurrentScale := 0;
    if SelfSize > 0 then
      P.fCurrentScale := P.CalculatedSize/SelfSize;
    if DoChildren then
      P.RecalcScales(True);
  end;
end;

procedure TSplitPanel.Loaded;
// Ensure everything is in sync after loading
begin
  inherited Loaded;
  fCalculatedWidth := Width;
  fCalculatedHeight := Height;
  UpdateLists;
  fLastAlignCount := fPanels.Count; // Prevent equaldivide at the first alignment
end;

procedure TSplitPanel.CreateWnd;
begin
//  fCalculatedWidth := Width;
//  fCalculatedHeight := Height;
  inherited CreateWnd;
//  fCalculatedWidth := Width;
//  fCalculatedHeight := Height;
end;

procedure TSplitPanel.ConstrainedResize(var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer);
begin
  inherited ConstrainedResize(MinWidth, MinHeight, MaxWidth, MaxHeight);
  if fVisiblePanels.Count > 0 then
  begin
    MinWidth := RealConstraints.MinWidth;
    MaxWidth := RealConstraints.MaxWidth;
    MinHeight := RealConstraints.MinHeight;
    MaxHeight := RealConstraints.MaxHeight;
  end;
end;

function TSplitPanel.GetVisibleIndex: Integer;
begin
  if fParentPanel = nil then
    Result := -1
  else
    Result := fParentPanel.fVisiblePanels.IndexOf(Self);
end;

procedure TSplitPanel.WMNCCalcSize(var Message: TWMNCCalcSize);
{-------------------------------------------------------------------------------
  This message method is the calculator of client area.
  Panels with childpanels have no border at all: they only are containers
  for the childpanels. An exception is made for panels with only one childpanel,
  which serves as a border.
  Then the NonClient-vars are updated. We need them to calculate the clientsize
  in AlignPanels.
-------------------------------------------------------------------------------}
var
  R: TRect;
begin
  R := Message.CalcSize_Params.rgrc[0];

  if fVisiblePanels.Count < 2 then
    inherited
  else
    Message.Result := 1;

  with Message.CalcSize_Params.rgrc[0] do
  begin
    fNonClientLeft := Left - R.Left;
    fNonClientTop := Top - R.Top;
    fNonClientBottom := R.Bottom - Bottom;
    fNonClientRight := R.Right - Right;
  end;
end;

procedure TSplitPanel.WMNCPaint(var Message: TMessage);
begin
  if fPaintLockCount > 0 then
    Exit;
  if fVisiblePanels.Count < 2 then
    inherited
  else
    Message.Result := 1;
end;

function TSplitPanel.GetFixedSize: Integer;
begin
  if (MinSize <> MaxSize) then
    Result := 0
  else
    Result := MaxSize;
end;

procedure TSplitPanel.SetFixedSize(const Value: Integer);
begin
  if csReading in ComponentState then
    Exit;
  if csLoading in ComponentState then
    Exit;
  if FixedSize = Value then Exit;
  if Value <= MIN_PANELSIZE then
  begin
    MinSize := 0;
    MaxSize := 0;
  end
  else begin
    MinSize := Value;
    MaxSize := MinSize;
  end;
end;

function TSplitPanel.CanAutoSize(var NewWidth, NewHeight: Integer): Boolean;
var
  W, H: Integer;
begin
  Result := False;
  if (fParentPanel <> nil) and (fPanels.Count = 0) then
  begin
    W := NewWidth;
    H := NewHeight;
    Result := inherited CanAutoSize(W, H);
    case fParentPanel.SplitMode of
      smVertical:
        begin
          NewHeight := H;
        end;
      smHorizontal:
        begin
          NewWidth := W;
        end;
    end;

    // NewHeight := H;
    // NewWidth := W;

  end
  else if (fParentPanel = nil) then
  begin
    //Result := inherited inherited CanAutoSize(NewWidth, NewHeight);
    (*
    case fSplitMode of
      smHorizontal: NewWidth := CalcSumSizes(fVisiblePanels);
      smVertical: NewHeight := CalcSumSizes(fVisiblePanels);
    end;
    *)
  end;
end;

procedure TSplitPanel.CMControlChange(var Message: TCMControlChange);
begin
  inherited;
end;

procedure TSplitPanel.CMControlListChange(var Message: TCMControlListChange);
{-------------------------------------------------------------------------------
  This message fires when the controllist changes. Not used because of recursion,
  when inserting Splitters during inserting childpanels
-------------------------------------------------------------------------------}
begin
  inherited;
end;

procedure TSplitPanel.CMTextChanged(var Message: TMessage);
begin
  Invalidate;
end;

function TSplitPanel.GetPanelCount: Integer;
begin
  Result := fPanels.Count;
end;

function TSplitPanel.GetVisiblePanelCount: Integer;
begin
  Result := fVisiblePanels.Count;
end;

procedure TSplitPanel.SetDivideMode(Value: TDivideMode);
begin
  if fDivideMode = Value then
    Exit;
  fDivideMode := Value;
  if fDivideMode = dmScaled then
    RecalcScales;
//  else
  //  RecalcScales(True); // clear scales
  Realign;
end;

procedure TSplitPanel.EqualDividePanels;
begin
  Include(fStates, pcsEqualDivideNeeded);
  Realign;
end;

function TSplitPanel.GetAlign: TAlign;
begin
  Result := inherited Align;
end;

procedure TSplitPanel.SetAlign(const Value: TAlign);
begin
  if ParentPanel = nil then
    inherited Align := Value
  else begin
    DesignHint(SHint_AlignNotAllowedForChildPanels);
  end;
end;

procedure TSplitPanel.DesignHint(const S: string);
begin
  if csDesigning in ComponentState then
    if poDesignHints in fOptions then
      if not (csReading in ComponentState) then
        if not (csLoading in ComponentState) then
          MessageDlg(S, mtInformation, [mbOK], 0);
end;

procedure TSplitPanel.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);

  with Params do
  begin
    Style := Style or WS_CLIPCHILDREN or WS_CLIPSIBLINGS;


    // single border
    if fBorderStyle = bsSingle then
    begin
      if NewStyleControls and Ctl3D then
      begin
        Style := Style and not WS_BORDER;
        ExStyle := ExStyle or WS_EX_CLIENTEDGE;
      end
      else
        Style := Style or WS_BORDER;
    end;


    // WindowClass.style := WindowClass.style or CS_OWNDC;
    // WindowClass.style := WindowClass.style and not (CS_HREDRAW or CS_VREDRAW); // een wonderbaarlijke genezing?!?
    // WindowClass.style := WindowClass.style or CS_PARENTDC;
  end;

end;

procedure TSplitPanel.Update;
begin
  if fPaintLockCount = 0 then
    inherited Update;
end;



procedure TSplitPanel.SetBorderStyle(const Value: TBorderStyle);
begin
  if fBorderStyle = Value then
    Exit;
  fBorderStyle := Value;
  Perform(CM_BORDERCHANGED, 0, 0);
end;

procedure TSplitPanel.CMBorderChanged(var Message: TMessage);
begin
  if pcsUpdatingBorder in fStates then
    Exit;
  inherited;
  RecreateWnd;
  UpdateRealConstraints;
  Invalidate;
end;

procedure TSplitPanel.CMCtl3DChanged(var Message: TMessage);
begin
  inherited;
  RecreateWnd;
end;

function TSplitPanel.GetPanelIndex: Integer;
begin
  if fParentPanel <> nil then
    Result := fParentPanel.fPanels.IndexOf(Self)
  else
    Result := -1;
end;

procedure TSplitPanel.SetPropertyManager(const Value: TSplitPanelPropertyManager);
begin
  if fPropertyManager = Value then
    Exit;

  if Value = nil then
    if fPropertyManager <> nil then
      fPropertyManager.RemovePanel(Self);

  fPropertyManager := Value;

  if fPropertyManager <> nil then
    fPropertyManager.AddPanel(Self);

end;

function TSplitPanel.GetCalculatedClientHeight: Integer;
// The safest is to always work with the calculated sizes
begin
  Result := CalculatedHeight - fNonClientTop - fNonClientBottom;
end;

function TSplitPanel.GetCalculatedClientWidth: Integer;
// The safest is to always work with the calculated sizes
begin
  Result := CalculatedWidth - fNonClientLeft - fNonClientRight;
end;

procedure TSplitPanel.PaintLock;
begin
  Inc(fPaintLockCount);
end;

procedure TSplitPanel.PaintUnlock;
begin
  if fPaintLockCount > 0 then
  begin
    Dec(fPaintLockCount);
    if fPaintLockCount = 0 then
      Update;
  end;
end;

function TSplitPanel.GetQuickBorder: TQuickBorderStyle;
begin
  Result := qbUnknown;
  if (BevelKind = bkNone) and (BevelInner = bvNone) and (BevelOuter = bvNone) and (BorderStyle = bsNone) then
    Result := qbNone
  else if (BevelKind = bkNone) and (BevelInner = bvNone) and (BevelOuter = bvNone) and (BorderStyle = bsSingle) then
    Result := qbBorder
  else if (BevelKind = bkTile) and (BevelInner = bvNone) and (BevelOuter = bvLowered) and (BorderStyle = bsNone) then
    Result := qbLowered
  else if (BevelKind = bkTile) and (BevelInner = bvRaised) and (BevelOuter = bvNone) and (BorderStyle = bsNone) then
    Result := qbRaised;
end;

procedure TSplitPanel.SetQuickBorder(const Value: TQuickBorderStyle);
begin
  if Value <> QuickBorder then
  begin
    Include(fStates, pcsUpdatingBorder);
    try
      case Value of
        qbNone:
          begin
            BevelKind := bkNone;
            BorderStyle := bsNone;
            BevelInner := bvNone;
            BevelOuter := bvNone;
          end;
        qbBorder:
          begin
            BevelKind := bkNone;
            BevelInner := bvNone;
            BevelOuter := bvNone;
            BorderStyle := bsSingle;
          end;
        qbLowered:
          begin
            BorderStyle := bsNone;
            BevelKind := bkTile;
            BevelInner := bvNone;
            BevelOuter := bvLowered;
          end;
        qbRaised:
          begin
            BorderStyle := bsNone;
            BevelKind := bkTile;
            BevelInner := bvRaised;
            BevelOuter := bvNone;
          end;
      end;
      //DoChildren(ccQuickBorder);
    finally
      Exclude(fStates, pcsUpdatingBorder);
      Perform(CM_BORDERCHANGED, 0, 0);
    end;
  end;
end;

procedure TSplitPanel.SetSplitterOptions(const Value: TSplitterOptions);
var
  i: Integer;
begin
  if fSplitterOptions = Value then
    Exit;
  fSplitterOptions := Value;
end;

{ TPanelSplitter }

procedure TPanelSplitter.CMMouseEnter(var Msg: TMessage);
begin
  Assert(fParentPanel <> nil, 'TPanelSplitter.CMMouseEnter');
  if (fParentPanel <> nil)
  and ([soPaintSplitters, soHighlightSplitters] * fParentPanel.fSplitterOptions = [soPaintSplitters, soHighlightSplitters])
  and not fDragInfo.iDragging
  and not fDisabled
  and not (csDesigning in fParentPanel.ComponentState) then
    Highlighted := True;
  inherited;
end;

procedure TPanelSplitter.CMMouseLeave(var Msg: TMessage);
begin
  if not fDragInfo.iDragging then
    Highlighted := False;
  inherited;
end;

constructor TPanelSplitter.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  ControlStyle := [csCaptureMouse];
  fHighlightColor := clWhite;
  fStep := 1;
  fResizeTimerInterval := GlobalLiveResizeTimerInterval;
  Visible := False;
end;

function TPanelSplitter.FindPanels: Boolean;
begin
  Result := Assigned(fFirstPanel) and Assigned(fSecondPanel);
end;

function TPanelSplitter.GetIndex: Integer;
begin
  Result := -1;
  if fParentPanel = nil then
    Exit;
  Result := fParentPanel.fSplitters.IndexOf(Self);
end;

procedure TPanelSplitter.DoResizePanel(X, Y: Integer);
begin
  with fDragInfo do
  begin
    case fSplitMode of
      smVertical:
        begin
          if Abs(Y - iStartY) >= fStep then
            fParentPanel.SplitterDrag(Self, Y - iStartY);
        end;
      smHorizontal:
        begin
          if Abs(X - iStartX) >= fStep then
            fParentPanel.SplitterDrag(Self, X - iStartX);
        end;
    end;
  end;
end;

type
  THackedWinControl = class(TWinControl);

procedure TPanelSplitter.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  F: TCustomForm;
begin
  inherited;
  if not FindPanels then
    Exit;
  if csDesigning in fParentpanel.ComponentState then
    Exit;
  fReady := False;
  if fDisabled then Exit;
  with fDragInfo do
  begin

    iCurrentX := X;
    iCurrentY := Y;
    iStartX := X;
    iStartY := Y;
    iDragging := True;

    F := GetParentForm(Self);
    if F <> nil then
    begin
      fControl := F.ActiveControl;
      if fControl <> nil then
      begin
        fOldKeyDownEvent := THackedWinControl(fControl).OnKeyDown;
        THackedWinControl(fControl).OnKeyDown := HandleKey;
      end;
    end;
    fParentPanel.SplitterDragBegin(Self);

    fPendingResize := False;
    if fResizeMode <> rmLive then
    begin
      AllocLineDC;
      Drawline(dmShow);
    end;
  end;
end;

procedure TPanelSplitter.MouseMove(Shift: TShiftState; X, Y: Integer);
begin

  inherited;

  if fDisabled or not FindPanels then
    Exit;
  if csDesigning in fParentpanel.ComponentState then
    Exit;

  with fDragInfo do
  begin
    fPendingResize := (iCurrentX <> X) or (iCurrentY <> Y);

    iCurrentX := X;
    iCurrentY := Y;
    if iDragging and (Shift = [ssLeft]) then
    begin
      if fResizeMode <> rmLive then
      begin
        DrawLine(dmMove);
      end
      else if fResizeMode = rmLive then
      begin
        // Direct resize on drag
        if fResizeTimerInterval = 0 then
        begin
          DoResizePanel(X, Y);
        end
        // timed resize
        else if not fTimerActive then
        begin
          SetTimer(Handle, RESIZE_TIMER, fResizeTimerInterval, nil);
          fTimerActive := True;
        end;
      end;
    end;
  end;
end;

procedure TPanelSplitter.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if not FindPanels then
    Exit;
  if csDesigning in fParentpanel.ComponentState then
    Exit;

  with fDragInfo do
  begin
    iCurrentX := X;
    iCurrentY := Y;

    KillTimer(Handle, RESIZE_TIMER);
    fTimerActive := False;

    Highlighted := False;

    if not iDragging then
      Exit;
    fDragInfo.iDragging := False;
    if fResizeMode <> rmLive then
    begin
      DrawLine(dmHide);
      ReleaseLineDC;
      DoResizePanel(X, Y);
    end
    else begin
      if fPendingResize or (fParentPanel.DragResizeMode = rmStructure) then
      begin
        fReady := True;
        DoResizePanel(X, Y);
      end;
    end;
    fParentPanel.SplitterDragEnd(Self);

  end;

  inherited;
end;

procedure TPanelSplitter.Paint;
var
  R: TRect;
begin
  if fParentPanel = nil then
    Exit;
  if not (soPaintSplitters in fParentPanel.fSplitterOptions) then
    Exit;

  with Canvas do
  begin
    R := ClientRect;
    if fHighlighted and (soHighlightSplitters in fParentPanel.fSplitterOptions) then
      Brush.Color := HighlightColor
    else
      Brush.Color := Color;
    FillRect(R);
  end;
end;

procedure TPanelSplitter.SetParent(aControl: TWinControl);
begin
  if Parent = aControl then
    Exit;

  if aControl is TSplitPanel then
  begin
    fParentPanel := TSplitPanel(aControl);
    fParentPanel.AddSplitter(Self)
  end
  else begin
    if fParentPanel <> nil then
      fParentPanel.RemoveSplitter(Self);
    fParentPanel := nil;
  end;

  inherited SetParent(aControl);

end;

procedure TPanelSplitter.SetSplitMode(const Value: TSplitMode);
begin
  fSplitMode := Value;
  if not fDisabled then
    case fSplitMode of
      smVertical   : Cursor := crVSplit;
      smHorizontal : Cursor := crHSplit;
    end
  else
    Cursor := crDefault;
end;

procedure TPanelSplitter.AllocLineDC;
{-------------------------------------------------------------------------------
  Allocate DC for xor-line (Draw on DC of Parent)
-------------------------------------------------------------------------------}
begin
  fLineDC := GetDCEx(Parent.Handle, 0, DCX_CACHE or DCX_CLIPSIBLINGS or DCX_LOCKWINDOWUPDATE);
  if FBrush = nil then
  begin
    fBrush := TBrush.Create;
    fBrush.Bitmap := AllocPatternBitmap(clBlack, clYellow{White});
  end;
  fPrevBrush := SelectObject(fLineDC, fBrush.Handle);
end;

procedure TPanelSplitter.ReleaseLineDC;
begin
  if fPrevBrush <> 0 then
    SelectObject(fLineDC, fPrevBrush);
  ReleaseDC(Parent.Handle, fLineDC);
  if fBrush <> nil then
  begin
    fBrush.Free;
    fBrush := nil;
  end;
end;

procedure TPanelSplitter.DrawLine(aMode: TInternalSplitterDrawMode);
{-------------------------------------------------------------------------------
  Draw xor-line during dragging
-------------------------------------------------------------------------------}
var
  P: TPoint;
begin
  with fDragInfo do
  begin
    P := Point(iCurrentX + Left - iStartX + 1, iCurrentY + Top - iStartY + 1);
    case fSplitMode of
      smVertical:
        begin
          case aMode of
            dmShow:
              begin
                PatBlt(fLineDC, Left, P.Y, Width, 2, PATINVERT);
              end;
            dmMove:
              begin
                PatBlt(fLineDC, Left, iDrawY, Width, 2, PATINVERT);
                PatBlt(fLineDC, Left, P.Y, Width, 2, PATINVERT);
              end;
            dmHide:
              begin
                PatBlt(fLineDC, Left, iDrawY, Width, 2, PATINVERT);
              end;
          end;
        end;
      smHorizontal:
        begin
          case aMode of
            dmShow:
              begin
                PatBlt(fLineDC, P.X, Top, 2, Height, PATINVERT);
              end;
            dmMove:
              begin
                PatBlt(fLineDC, iDrawX, Top, 2, Height, PATINVERT);
                PatBlt(fLineDC, P.X, Top, 2, Height, PATINVERT);
              end;
            dmHide:
              begin
                PatBlt(fLineDC, P.X, Top, 2, Height, PATINVERT);
              end;
          end;
        end;
     end;
    iDrawX := P.X;
    iDrawY := P.Y;
  end;
  fLineVisible := not fLineVisible;
end;

destructor TPanelSplitter.Destroy;
begin
  fBrush.Free;
  inherited;
end;

procedure TPanelSplitter.HandleKey(Sender: TObject; var Key: Word; Shift: TShiftState);
{-------------------------------------------------------------------------------
  Temporary "borrowed" OnKeyDown event of ActiveControl - Copyright Borland :)
-------------------------------------------------------------------------------}
begin
  if Key = VK_ESCAPE then
  begin
    if fControl <> nil then
      THackedWinControl(fControl).OnKeyDown := fOldKeyDownEvent;
    KillTimer(Handle, RESIZE_TIMER);
    fTimerActive := False;
    if fResizeMode in [rmLine, rmStructure] then
      DrawLine(dmHide);
    fDragInfo.iDragging := False;
    fPendingResize := False;
    fParentPanel.SplitterDragCancel(Self);
  end;
end;

procedure TPanelSplitter.WMPaint(var Msg: TWMPaint);
begin
  inherited;
end;

procedure TPanelSplitter.WMTimer(var Msg: TWMTimer);
begin
  case Msg.TimerID of
    RESIZE_TIMER:
      if fPendingResize then
      begin
        fPendingResize := False;
        DoResizePanel(fDragInfo.iCurrentX, fDragInfo.iCurrentY);
      end;
  end;
end;

procedure TPanelSplitter.WMNCPaint(var Message: TMessage);
begin
  Message.Result := 1;
end;


procedure TPanelSplitter.SetResizeTimerInterval(const Value: Cardinal);
begin
  fResizeTimerInterval := Value;
end;

procedure TPanelSplitter.SetHighlighted(const Value: Boolean);
begin
  if fHighlighted = Value then
    Exit;
  fHighlighted := Value;
  Invalidate;
end;

{ TSplitPanelPropertyManager }

procedure TSplitPanelPropertyManager.AddPanel(P: TSplitPanel);
begin
  fPanels.Add(P);
  UpdatePanel(P, SPF_ALL);
end;

procedure TSplitPanelPropertyManager.BeginUpdate;
begin
  Inc(fUpdateCount);
end;

procedure TSplitPanelPropertyManager.Changed;
begin
  if fUpdateCount = 0 then
    UpdatePanels(SPF_ALL);
end;

constructor TSplitPanelPropertyManager.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fPanels := TSplitPanelList.Create;

  fDistance       := DEF_DISTANCE;
  fBevelEdges     := DEF_BEVELEDGES;
  fBevelInner     := DEF_BEVELINNER;
  fBevelOuter     := DEF_BEVELOUTER;
  fBevelKind      := DEF_BEVELKIND;
  fBevelWidth     :=  DEF_BEVELWIDTH;
  fBorderWidth    := DEF_BORDERWIDTH;
  fBorderStyle    := DEF_BORDERSTYLE;
  fDragResizeMode := DEF_DRAGRESIZEMODE;
  fOptions        := DEF_OPTIONS;
  fDivideMode     := DEF_DIVIDEMODE;
end;

destructor TSplitPanelPropertyManager.Destroy;
var
  i: Integer;
begin
  for i := 0 to fPanels.Count - 1 do
    fPanels[i].PropertyManager := nil;
  fPanels.Free;
  inherited Destroy;
end;

procedure TSplitPanelPropertyManager.EndUpdate;
begin
  if fUpdateCount > 0 then
    Dec(fUpdateCount);
end;

procedure TSplitPanelPropertyManager.RemovePanel(P: TSplitPanel);
begin
  fPanels.Remove(P);
end;

procedure TSplitPanelPropertyManager.SetBevelInner(const Value: TBevelCut);
begin
  fBevelInner := Value;
  UpdatePanels(SPF_BEVELINNER);
end;

procedure TSplitPanelPropertyManager.SetBevelKind(const Value: TBevelKind);
begin
  fBevelKind := Value;
  UpdatePanels(SPF_BEVELKIND);
end;

procedure TSplitPanelPropertyManager.SetBevelOuter(const Value: TBevelCut);
begin
  fBevelOuter := Value;
  UpdatePanels(SPF_BEVELOUTER);
end;

procedure TSplitPanelPropertyManager.SetBevelWidth(const Value: TBevelWidth);
begin
  fBevelWidth := Value;
  UpdatePanels(SPF_BEVELWIDTH);
end;

procedure TSplitPanelPropertyManager.SetDistance(Value: Integer);
begin
  fDistance := Value;
  UpdatePanels(SPF_DISTANCE);
end;

procedure TSplitPanelPropertyManager.SetBevelEdges(const Value: TBevelEdges);
begin
  fBevelEdges := Value;
  UpdatePanels(SPF_BEVELEDGES);
end;

procedure TSplitPanelPropertyManager.SetBorderStyle(const Value: TBorderStyle);
begin
  fBorderStyle := Value;
  UpdatePanels(SPF_BORDERSTYLE);
end;

procedure TSplitPanelPropertyManager.SetBorderWidth(const Value: Integer);
begin
  fBorderWidth := Value;
  UpdatePanels(SPF_BORDERWIDTH);
end;

procedure TSplitPanelPropertyManager.SetDivideMode(const Value: TDivideMode);
begin
  fDivideMode := Value;
  UpdatePanels(SPF_DIVIDEMODE);
end;

procedure TSplitPanelPropertyManager.SetDragResizeMode(const Value: TDragResizeMode);
begin
  fDragResizeMode := Value;
  UpdatePanels(SPF_DRAGRESIZEMODE);
end;

procedure TSplitPanelPropertyManager.SetOptions(const Value: TSplitPanelOptions);
begin
  fOptions := Value;
  UpdatePanels(SPF_OPTIONS);
end;

procedure TSplitPanelPropertyManager.SetQuickBorder(const Value: TQuickBorderStyle);
begin
  case Value of
    qbNone:
      begin
        BevelKind := bkNone;
        BorderStyle := bsNone;
        BevelInner := bvNone;
        BevelOuter := bvNone;
      end;
    qbBorder:
      begin
        BevelKind := bkNone;
        BevelInner := bvNone;
        BevelOuter := bvNone;
        BorderStyle := bsSingle;
      end;
    qbLowered:
      begin
        BorderStyle := bsNone;
        BevelKind := bkTile;
        BevelInner := bvNone;
        BevelOuter := bvLowered;
      end;
    qbRaised:
      begin
        BorderStyle := bsNone;
        BevelKind := bkTile;
        BevelInner := bvRaised;
        BevelOuter := bvNone;
      end;
  end;
  UpdatePanels(SPF_QUICKBORDER);
end;

procedure TSplitPanelPropertyManager.SetSplitterOptions(const Value: TSplitterOptions);
begin
  fSplitterOptions := Value;
  UpdatePanels(SPF_SPLITTEROPTIONS)
end;

procedure TSplitPanelPropertyManager.SetAllEqual(const Value: Boolean);
begin
  UpdatePanels(SPF_ALL);
end;

procedure TSplitPanelPropertyManager.UpdatePanel(P: TSplitPanel; Flags: Cardinal);
var
  i: Integer;
begin
  P.DisableAlign;
  try
    CopyProps(P, Flags);
    // call this method recursively for the childpanels
    for i := 0 to P.PanelCount - 1 do
      UpdatePanel(P[i], Flags);
  finally
    P.EnableAlign;
  end;
end;

procedure TSplitPanelPropertyManager.UpdatePanels(Flags: Cardinal);
var
  i: Integer;
begin
  for i := 0 to fPanels.Count - 1 do
    UpdatePanel(fPanels[i], Flags);
end;

function TSplitPanelPropertyManager.GetAllEqual: Boolean;

    function CheckPanel(P: TSplitPanel): Boolean;
    var
      i: Integer;
    begin
      Result := True;

      if not SameProps(P, SPF_ALL) then
      begin
        Result := False;
        Exit;
      end;

      for i := 0 to P.PanelCount - 1 do
      begin
        if not Checkpanel(P[i]) then
        begin
          Result := False;
          Exit;
        end;
      end;
    end;

var
  i: Integer;
begin
  Result := True;
  for i := 0 to fPanels.Count - 1 do
  begin
    if not CheckPanel(fPanels[i]) then
    begin
      Result := False;
      Exit;
    end;
  end;
end;

procedure TSplitPanelPropertyManager.CopyProps(P: TSplitPanel; Flags: Cardinal);
begin
  if Flags and SPF_BEVELEDGES <> 0 then
    P.BevelEdges := BevelEdges;

  if Flags and SPF_BEVELINNER <> 0 then
    P.BevelInner := BevelInner;

  if Flags and SPF_BEVELOUTER <> 0 then
    P.BevelOuter := BevelOuter;

  if Flags and SPF_BEVELKIND <> 0 then
    P.BevelKind := BevelKind;

  if Flags and SPF_BEVELWIDTH <> 0 then
    P.BevelWidth := BevelWidth;

  if Flags and SPF_BORDERSTYLE <> 0 then
    P.BorderStyle := BorderStyle;

  if Flags and SPF_BORDERWIDTH <> 0 then
    P.BorderWidth := BorderWidth;

  if Flags and SPF_DISTANCE <> 0 then
    P.Distance := Distance;

  if Flags and SPF_DRAGRESIZEMODE <> 0 then
    P.DragResizeMode := DragResizeMode;

  if Flags and SPF_OPTIONS <> 0 then
    P.Options := Options;

  if Flags and SPF_DIVIDEMODE <> 0 then
    P.DivideMode := DivideMode;

  if Flags and SPF_SPLITTEROPTIONS <> 0 then
    P.SplitterOptions := SplitterOptions;

end;

function TSplitPanelPropertyManager.SameProps(P: TSplitPanel; Flags: Cardinal): Boolean;
begin
  Result := False;

  if Flags and SPF_BEVELEDGES <> 0 then
    if P.BevelEdges <> BevelEdges then
      Exit;

  if Flags and SPF_BEVELINNER <> 0 then
    if P.BevelInner <> BevelInner then
      Exit;

  if Flags and SPF_BEVELOUTER <> 0 then
    if P.BevelOuter <> BevelOuter then
      Exit;

  if Flags and SPF_BEVELKIND <> 0 then
    if P.BevelKind <> BevelKind then
      Exit;

  if Flags and SPF_BEVELWIDTH <> 0 then
    if P.BevelWidth <> BevelWidth then
      Exit;

  if Flags and SPF_BORDERSTYLE <> 0 then
    if P.BorderStyle <> BorderStyle then
      Exit;

  if Flags and SPF_BORDERWIDTH <> 0 then
    if P.BorderWidth <> BorderWidth then
      Exit;

  if Flags and SPF_DISTANCE <> 0 then
    if P.Distance <> Distance then
      Exit;

  if Flags and SPF_DRAGRESIZEMODE <> 0 then
    if P.DragResizeMode <> DragResizeMode then
      Exit;

  if Flags and SPF_OPTIONS <> 0 then
    if P.Options <> Options then
      Exit;

  if Flags and SPF_DIVIDEMODE <> 0 then
    if P.DivideMode <> DivideMode then
      Exit;

  if Flags and SPF_SPLITTEROPTIONS <> 0 then
    if P.SplitterOptions <> SplitterOptions then
      Exit;

  Result := True;
end;

function TSplitPanelPropertyManager.GetQuickBorder: TQuickBorderStyle;
begin
  Result := qbUnknown;
  if (BevelKind = bkNone) and (BevelInner = bvNone) and (BevelOuter = bvNone) and (BorderStyle = bsNone) then
    Result := qbNone
  else if (BevelKind = bkNone) and (BevelInner = bvNone) and (BevelOuter = bvNone) and (BorderStyle = bsSingle) then
    Result := qbBorder
  else if (BevelKind = bkTile) and (BevelInner = bvNone) and (BevelOuter = bvLowered) and (BorderStyle = bsNone) then
    Result := qbLowered
  else if (BevelKind = bkTile) and (BevelInner = bvRaised) and (BevelOuter = bvNone) and (BorderStyle = bsNone) then
    Result := qbRaised;
end;

end.


