unit FDebug;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TDebugForm = class(TForm)
    ListBox: TListBox;
    FilterEdit: TEdit;
    procedure ListBox_DblClick(Sender: TObject);
    procedure FilterEdit_KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Form_Create(Sender: TObject);
    procedure Form_Destroy(Sender: TObject);
    procedure Form_KeyPress(Sender: TObject; var Key: Char);
  private
    FSaveList: TStrings;
    FFiltered: boolean;
    //function FilterStr(const S: string): boolean;
  public
    procedure AddStr(const S: string);
    procedure SetFilter(const S: string);
    procedure DoClear;
  end;

var
  DebugForm: TDebugForm;

implementation

uses
  UMisc;

{$R *.DFM}

procedure TDebugForm.Form_Create(Sender: TObject);
begin
  FSaveList := TStringList.Create;
end;

procedure TDebugForm.Form_Destroy(Sender: TObject);
begin
  FSaveList.Free;
end;

procedure TDebugForm.ListBox_DblClick(Sender: TObject);
begin
//  ListBox.Clear;
  DoClear;
end;

procedure TDebugForm.Form_KeyPress(Sender: TObject; var Key: Char);
begin
{  if Key in ['a'..'z', 'A'..'Z'] then
    if not FilterEdit.Focused then
  begin
    FilterEdit.SetFocus;
  end;}
end;

procedure TDebugForm.FilterEdit_KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    SetFilter(FilterEdit.Text);
//    ListBox.SetFocus;
  end;
end;

(*function TDebugForm.FilterStr(const S: string): boolean;
var
  Filt: string;
begin
  Filt := UpperCase(FilterEdit.text);
  Result := (Filt = '') or (Pos(Filt, S) > 0);
end;*)

procedure TDebugForm.AddStr(const S: string);
var
  NS: string;
begin
  NS := LeadZeroStr(FSaveList.Count, 4);
  // altijd toevoegen
  FSaveList.Add(NS + ':' + S);
  with ListBox do
  begin
    if not FFiltered //or FilterStr(S) then
    or (Pos(UpperCase(FilterEdit.Text), UpperCase(S)) > 0) then
    begin
      ListBox.Items.Add(NS + ':' + S);
      ListBox.ItemIndex := Items.Count - 1;
      Update;
    end;
  end;
end;

procedure TDebugForm.SetFilter(const S: string);
var
  i: integer;
  FFilterList: TStringList;
begin
  with ListBox do
  begin
    FFiltered := (S <> '');
    // terugzetten
    if not FFiltered then
     Items.Assign(FSaveList)
    else begin
      {if FSaveList.Count > 0 then }Items.Assign(FSaveList);
      //FSaveList.Assign(Items);
      FFilterList := TStringList.Create;
      // filteren
      for i := 0 to FSaveList.Count - 1 do
        if Pos(UpperCase(S), UpperCase(FSaveList[i])) > 0 then
          FFilterList.Add(FSaveList[i]);
      Items.Assign(FFilterList);
      FFilterList.Free;
    end;
  end;
end;

procedure TDebugForm.DoClear;
begin
  fSaveList.Clear;
  ListBox.Items.Clear;
end;

end.



