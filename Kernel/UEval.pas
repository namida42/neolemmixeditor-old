unit UEval;

interface

uses
  Windows, Classes, SysUtils, Contnrs,
  ULog, UMisc, UTools,
  UExprFuncs;

type

  TEvError =
    (
      eeUnknown,
      eeVarTypeMismatch,
      eeParamTypeMismatch,
      eeVarNotFound,
    { transitiefouten }
      eeStatementExpected,
      eeOpenBracketExpected,
      eeMissingFactor
    );

const
  CParseErrors = [eeStatementExpected];

  SEvErrors: array[TEvError] of string =
    (
      'Onbekende fout',
      '%s en %s zijn niet compatible',
      'Fout in functie %s.'#13#10'%s en %s zijn niet compatible',
      'Variable "%s" niet gevonden',
    { transitiefouten }
      'Statement verwacht',
      'Haakje open verwacht',
      'Missing factor'
    );

type

  { exceptions }
  EEvError = class(Exception)
  private
   fCode: TEvError;
  public
  end;

  TParseInfoRec = record
    Expression: string;
    Position: integer;
  end;

  EParseError = class(EEvError)
  private
    fInfo: TParseInfoRec;
  public
    property Info: TParseInfoRec read fInfo;
  end;

type
  TEvVar = class;

  TEvParams = TOpenVariantArray;
  TEvFunctionRef = function(aParamCount: integer; const Ar: array of Variant): Variant;

  TEvFunctionInfo = class
  private
    fFunctionName : string;
    fFunctionRef  : TEvFunctionRef;
    fParamCount   : integer;
    fParamTypes   : TOpenIntegerArray;
    fResultType   : integer;
    function GetParamType(N: integer): integer;
    function GetDeclaration: string;
  public
    constructor Create(const aFunctionName: string; const aFunctionRef: TEvFunctionRef;
      aParamCount: integer; const aParamTypes: array of integer; aResultType: integer);
    property FunctionName: string read fFunctionName;
    property FunctionRef: TEvFunctionRef read fFunctionRef;
    property ParamCount: integer read fParamCount;
    property ParamTypes: TOpenIntegerArray read fParamTypes;
    property ParamType[N: integer]: integer read GetParamType;
    property ResultType: integer read fResultType;
    property Declaration: string read GetDeclaration;
  end;

  TEvFunctionInfoList = class(TStringListEx)
  private
  protected
  public
    constructor Create;
    //function Find()
  published
  end;

  TGetVarValueEvent = procedure(Sender: TEvVar; var aValue: Variant) of object;

  TEvVar = class
  private
    fVarName: string;
    fValue: Variant;
    fVariantType: integer;
    fOnGetValue: TGetVarValueEvent;
    procedure SetValue(const aValue: Variant);
    function GetValue: Variant;
  public
    constructor Create(const aName: string; aType: integer);
    property VarName: string read fVarName;
    property Value: Variant read GetValue write SetValue;
    property VariantType: integer read fVariantType;
    property OnGetValue: TGetVarValueEvent read fOnGetValue write fOnGetValue;
  end;

  TEvVarList = class(TStringListEx)
  private
    function GetValueByName(const aName: string): Variant;
    function GetVarByName(const aName: string): TEvVar;
    procedure SetValueByName(const aName: string; const aValue: Variant);
    function GetVar(N: integer): TEvVar;
    function GetVarName(N: integer): string;
  public
    constructor Create;
    property Vars[N: integer]: TEvVar read GetVar; default;
    property VarName[N: integer]: string read GetVarName;
    property VarByName[const aName: string]: TEvVar read GetVarByName;
    property ValueByName[const aName: string]: Variant read GetValueByName write SetValueByName;
  end;

  TEvaluator = class
  private
  protected
    function GetValue: Variant; virtual; abstract;
  public
    property Value: Variant read GetValue;
    procedure AddParam(const aParam: Variant); virtual; abstract;
  published
  end;

  TEvFunction = class(TEvaluator)
  private
    fInfo        : TEvFunctionInfo;
    fCountParams : integer;
    fCapacity    : integer;
    fParams      : TEvParams;
  { intern }
    procedure InternalGrow;
    procedure InternalCheckParam(aIndex: integer; var aParam: Variant);
  protected
    function GetValue: Variant; override;
  public
    constructor Create; overload;
    constructor Create(aInfo: TEvFunctionInfo); overload;
    destructor Destroy; override;
    property Info: TEvFunctionInfo read fInfo write fInfo;
    procedure AddParam(const aParam: Variant); override;
  published
  end;

  TEvCalculation = class(TEvaluator)
  private
  protected
    //function GetValue: Variant; override;
  public
  published
  end;

  TEvStack = class(TObjectList)
  private
    fPeek: TEvFunction;
    function GetPeek: TEvFunction;
  protected
  public
    function PushFunction(aInfo: TEvFunctionInfo): TEvFunction;
    function Pop: Variant;
    property Peek: TEvFunction read GetPeek;
  published
  end;

  TEval = class
  private
  { interne properties }
    fStack: TEvStack;
    fFunctions: TEvFunctionInfoList;
    fVars: TEvVarList;
    fExpression: string;
  { properties }
    fValue: Variant;
  { interne methods }
    procedure InternalReset;
    procedure InternalEvaluate;
  { property access }
    procedure SetExpression(const Value: string);
    function GetDeclaredFunction(N: integer): TEvFunctionInfo;
    function GetDeclaredFunctionCount: integer;
    function GetDeclaredVarCount: integer;
    function GetDeclaredVar(N: integer): TEvVar;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    function DeclareFunction(const aFunctionName: string; const aFunctionRef: TEvFunctionRef;
      aParamCount: integer; const aParamTypes: array of integer; aResultType: integer): TEvFunctionInfo;
    function DeclareVariable(const aName: string; aType: integer): TEvVar;
    procedure ReleaseVariable(const aName: string);
    property Expression: string read fExpression write SetExpression;
    property Value: Variant read fValue;
    property DeclaredFunctionCount: integer read GetDeclaredFunctionCount;
    property DeclaredFunctions[N: integer]: TEvFunctionInfo read GetDeclaredFunction;
    property DeclaredVarCount: integer read GetDeclaredVarCount;
    property DeclaredVars[N: integer]: TEvVar read GetDeclaredVar;

    procedure TestFunc;
  published
  end;

implementation

uses
  TypInfo;

const
  MAX_PARAMCOUNT = 255;
  AllowedVariantTypes: array[0..3] of Word =
    (varInteger, varDouble, varDate, varString);

  ValidIdentifierStartChars = ['A'..'Z', 'a'..'z', '_'];
  ValidIdentifierChars = ['A'..'Z', 'a'..'z', '_', '0'..'9'];

procedure EvError(Err: TEvError; const Args: array of const);
begin
  raise EEvError.CreateFmt(SEvErrors[Err], Args);
end;

procedure ParseError(Err: TEvError; const Info: TParseInfoRec; const Args: array of const);
var
  E: EParseError;
  O, S: string;
begin
  O := 'Parse fout op positie ' + i2s(Info.Position) + ': ' + SEvErrors[Err];
  try
    S := Format(O, Args)
  except
    S := O;
  end;
  E := EParseError.Create(S);
  E.fCode := Err;
  E.fInfo := Info;
  raise E;
end;

function ValidIdentifier(const S: string): boolean;
var
  i: integer;
begin
  Result := False;
  if Length(S) = 0 then
    Exit;
  if not (S[1] in ValidIdentifierStartChars) then
    Exit;
  for i := 2 to Length(S) do
    if not (S[i] in ValidIdentifierChars) then Exit;
  Result := True;
end;

function ValidVariantType(aType: integer): boolean;
var
  i: integer;
begin
  Result := True;
  for i := Low(AllowedVariantTypes) to High(AllowedVariantTypes) do
    if aType = AllowedVariantTypes[i] then Exit;
  Result := False;
end;

function ConvertVariant(var V: Variant; aType: integer): boolean;
begin

end;

{ TEvFunctionInfo }

constructor TEvFunctionInfo.Create(const aFunctionName: string;
  const aFunctionRef: TEvFunctionRef; aParamCount: integer;
  const aParamTypes: array of integer; aResultType: integer);
begin
  inherited Create;

  if not ValidIdentifier(aFunctionName) then
    AppError(aFunctionName + ' ' + 'is geen valide functienaam');

  if not Assigned(aFunctionRef) then
    AppError('functie referentie mag niet nil zijn');

  if not Between(aParamCount, -1, MAX_PARAMCOUNT) then
    AppError('paramcount out of bounds');

  if (Length(aParamTypes) = 0) and (aParamCount > 0) then
    AppError('paramtypes niet gespecificeerd');

  if aParamCount = -1 then
  begin
    if not ValidVariantType(aParamTypes[0]) then
      AppError(VTypeStr(aParamTypes[0]) + ' parametertype is niet toegestaan');
  end
  else begin
    if Length(aParamTypes) <> aParamCount then
      AppError('lengte paramtypes en paramcount verschilt');
      
  end;

  fFunctionName := aFunctionName;
  fFunctionRef  := aFunctionRef;
  fParamCount   := aParamCount;
  fParamTypes   := SetIntegerArray(aParamTypes);
  fResultType   := aResultType;
end;

function TEvFunctionInfo.GetDeclaration: string;
var
  i: integer;
begin
  Result := fFunctionName + '(';
  case fParamCount of
    0  :
      begin
        Result := Result + ': ' + VTypeStr(fResultType);
      end;
    -1 :
      begin
        Result := Result + 'P1: array of ' +
                  VTypeStr(fParamTypes[0]) +
                  '): ' + VTypeStr(fResultType);
      end;
    else begin
      for i := 0 to fParamCount - 1 do
        Result := Result + 'P' + i2s(i + 1) + ': ' +
                  VTypeStr(fParamTypes[i]) +
                  IIF(i < fParamCount -1, ', ', '): ' + VTypeStr(fResultType));
    end;
  end;
end;

function TEvFunctionInfo.GetParamType(N: integer): integer;
begin
  Result := fParamTypes[N];
end;

{ TEvFunction }

procedure TEvFunction.AddParam(const aParam: Variant);
var
  V: Variant;
begin
  V := aParam;
  //InternalCheckParamCount
  if Info.ParamCount <> - 1 then
    if fCountParams >= Info.ParamCount then
      AppError(Info.Declaration + Chr(13) + 'teveel parameters');
  InternalCheckParam(fCountParams, V);
  if fCountParams >= fCapacity then InternalGrow;
  Inc(fCountParams);
  fParams[fCountParams - 1] := V;
end;

constructor TEvFunction.Create;
begin
  inherited Create;
end;

constructor TEvFunction.Create(aInfo: TEvFunctionInfo);
begin
  inherited Create;
  fInfo := aInfo;
end;

destructor TEvFunction.Destroy;
begin
  fParams := nil;
  inherited;
end;

function TEvFunction.GetValue: Variant;
begin
  if Info <> nil then
  begin
    if Info.fParamCount <> -1 then
      if fCountParams <> Info.fParamCount then
        AppError('TEvFunction.GetValue: Paramcount fout');
    Result := Info.FunctionRef(fCountParams, fParams);
  end
  else
    Result := Unassigned;
end;

procedure TEvFunction.InternalCheckParam(aIndex: integer; var aParam: Variant);
begin
  if Info = nil then
    AppError('TEvFunction.InternalCheckParam Info = nil');
  with Info do
  begin
//    windlg([paramcount]);
    if ParamCount = -1 then
    begin
      if VarType(aParam) <> ParamTypes[0] then
        AppError(Info.Declaration + Chr(13) + ' type fout');
    end
    else begin
      if VarType(aParam) <> ParamTypes[aIndex] then
      begin
        if (VarType(aParam) = varInteger) and (ParamTypes[aIndex] = varDouble) then
        begin
          aParam := VarAsType(aParam, varDouble);
          Exit;
        end;

        AppError(Declaration + Chr(13) +
                'Parameterfout in parameter ' + i2s(aIndex + 1) + ': ' + VarTypeStr(aParam) + ' en ' +
                VTypeStr(ParamTypes[aIndex]) + ' zijn niet compatible');
      end;
    end;
  end;
end;

procedure TEvFunction.InternalGrow;
var
  Delta: Integer;
begin
  if fCapacity > 64 then Delta := fCapacity div 4 else
    if fCapacity > 8 then Delta := 16 else
      Delta := 8;
  SetLength(fParams, fCapacity + Delta);
  Inc(fCapacity, Delta);
end;

{ TEvFunctionInfoList }

constructor TEvFunctionInfoList.Create;
begin
  inherited Create(True);
  Sorted := True;
end;

{ TEvVar }

constructor TEvVar.Create(const aName: string; aType: integer);
begin
  fVarName := aName;
  fVariantType := aType;
  fValue := Unassigned;
  TVarData(fValue).VType := fVariantType;
end;

function TEvVar.GetValue: Variant;
begin
  Result := fValue;
  if Assigned(fOnGetValue) then
  begin
    fOnGetValue(Self, Result);
//    fValue := SetValue()

    // check type!
  end;
end;

procedure TEvVar.SetValue(const aValue: Variant);
begin
//  if TVarData(aValue).VType <> fVariantType then
//    AppError('tvarname.setvalue ' + 'fout type');
  // check type  system?
  fValue := aValue;
  try
    fValue := VarAsType(aValue, fVariantType);
  except
    AppError('varinfo.setvalue typefout')
  end;
end;

{ TEvVarList }

constructor TEvVarList.Create;
begin
  inherited Create(True);
  Sorted := True;
//  Duplicates := dupError;
end;

function TEvVarList.GetValueByName(const aName: string): Variant;
begin
  Result := GetVarByName(aName).Value;
end;

function TEvVarList.GetVar(N: integer): TEvVar;
begin
  Result := TEvVar(GetObject(N));
end;

function TEvVarList.GetVarByName(const aName: string): TEvVar;
var
  i: integer;
begin
  i := IndexOf(aName);
  if i >= 0 then
    Result := TEvVar(GetObject(i))
  else
    AppError('variabele niet gevonden');
end;

function TEvVarList.GetVarName(N: integer): string;
begin
  Result := Get(N);
end;

procedure TEvVarList.SetValueByName(const aName: string; const aValue: Variant);
var
  i: integer;
begin
  i := IndexOf(aName);
  if i >= 0 then
    TEvVar(GetObject(i)).Value := aValue
  else
    AppError('variabele niet gevonden');
end;

{ TEvStack }

function TEvStack.GetPeek: TEvFunction;
begin
  if fPeek = nil then
    AppError('TEvStack.GetPeek = nil');
  Result := fPeek;
end;

function TEvStack.Pop: Variant;
begin
  Assert(Count > 0, 'TEvStack.Pop: StackSize = 0');
  Result := TEvFunction(GetItem(Count - 1)).Value;
  Delete(Count - 1);
  if Count > 0 then fPeek := TEvFunction(GetItem(Count - 1)) else fPeek := nil;
  if fPeek <> nil then fPeek.AddParam(Result);
end;

function TEvStack.PushFunction(aInfo: TEvFunctionInfo): TEvFunction;
begin
  Result := TEvFunction.Create(aInfo);
  Add(Result);
  fPeek := Result;
end;

{ TEval }

procedure TEval.TestFunc;
var
  t,i: integer;
  Decl: TEvFunctionInfo;
  ItsDecl: TEvFunctionInfo;
  StiDecl: TEvFunctionInfo;
begin
  ItsDecl := DeclareFunction('its', _its, 1, [varInteger], varString);
  StiDecl := DeclareFunction('sti', _sti, 1, [varString], varInteger);


  with fStack do begin
  begin
    with PushFunction(ItsDecl) do AddParam(1);
    with PushFunction(StiDecl) do AddParam('10');
    Log([VarTypeStr(Pop)]);
    Log([VarTypeStr(Pop)]);
  end;
  end;

  Exit;


  t := gettickcount;
  for i := 1 to 100000 do
  begin
    with TEvFunction.Create do
      Free;
  end;
  t := gettickcount-t;
  Log(['100000 * create free', t]);

  with TEvFunction.Create do
  begin
    Info := DeclareFunction('its', _its, 1, [varInteger], varString);
    Decl := Info;
    AddParam(1);
    t := gettickcount;
    for i := 1 to 100000 do
      GetValue;
    t := gettickcount-t;
    Log([Value, VarTypeStr(Value)]);
    Log(['100000 x getvalue', t]);
    Free;
  end;

  with fStack do
  begin
    with PushFunction(Decl) do
      AddParam(1);
    Log([Pop]);
  end;

  with fStack do begin
  t := gettickcount;
  for i := 1 to 100000 do
  begin
    with PushFunction(Decl) do AddParam(1);
    Pop;
  end;
  t := gettickcount-t;
  end;
  Log([Value, VarTypeStr(Value)]);
  Log(['100000 x push pop', t]);

end;

constructor TEval.Create;
begin
  inherited Create;
  fStack := TEvStack.Create;
  fStack.Capacity := 16;
  fFunctions := TEvFunctionInfoList.Create;
  fVars := TEvVarList.Create;
  DeclareFunction('Date', @_Date, 1, [varString], varDate);
  DeclareFunction('Concat', @_Concat, -1, [varString], varString);
  DeclareFunction('RepStr', @_RepStr, 2, [varString, varInteger], varString);
  DeclareFunction('Its', @_Its, 1, [varInteger], varString);
  DeclareFunction('Sti', @_Sti, 1, [varString], varInteger);
  DeclareFunction('FloatToStr', @_FloatToStr, 1, [varDouble], varString);
end;

destructor TEval.Destroy;
begin
  fStack.Free;
  fFunctions.Free;
  fVars.Free;
  inherited Destroy;
end;

function TEval.DeclareFunction(const aFunctionName: string; const aFunctionRef: TEvFunctionRef;
      aParamCount: integer; const aParamTypes: array of integer; aResultType: integer): TEvFunctionInfo;
var
  FI: TEvFunctionInfo;
begin
  FI := TEvFunctionInfo.Create(aFunctionName, aFunctionRef, aParamCount, aParamTypes, aResultType);
  fFunctions.AddObject(aFunctionName, FI);
  Result := FI;
end;

function TEval.DeclareVariable(const aName: string; aType: integer): TEvVar;
var
  i: integer;
begin
  Result := TEvVar.Create(aName, aType);
  i := fVars.IndexOf(aName);
  if i < 0 then
    fVars.AddObject(aName, Result)
  else begin
    fVars[i].Free;
    fVars.AddObject(aName, Result)
  end;
end;

procedure TEval.ReleaseVariable(const aName: string);
var
  i: integer;
begin
  i := fVars.IndexOf(aName);
  if i >= 0 then
  begin
    fVars[i].Free;
    fVars.Delete(i)
  end
  else
    AppError('var "' + aName + '" niet gevonden');
end;

procedure TEval.InternalReset;
begin
  fValue := 0;
  fStack.Clear;
end;

procedure TEval.SetExpression(const Value: string);
begin
  if CompareText(fExpression, Value) = 0 then Exit;
  fExpression := Value;
  InternalEvaluate;
end;

function TEval.GetDeclaredFunction(N: integer): TEvFunctionInfo;
begin
  Result := TEvFunctionInfo(fFunctions.Objects[N]);
end;

function TEval.GetDeclaredFunctionCount: integer;
begin
  Result := fFunctions.Count;
end;

function TEval.GetDeclaredVar(N: integer): TEvVar;
begin
  Result := fVars[N];
end;

function TEval.GetDeclaredVarCount: integer;
begin
  Result := fVars.Count;
end;


(*

 REPSTR ( CONCAT ( ITS ( 1+1/6 + btw ) , ITS ( 22 ) ) , 123 )

 repstr ( ppppppppppppppppppppppppppppppppppppppppppp , ppp )

          concat ( ppppppppppppppppppp , pppppppppp )

                   its ( ppppppppppp ) , its ( pp )

term: getal, functionnaam, variabelenaam
symbol: haakje, quote, komma
value: getal, uitkomst functie, waarde variabele

func
  params
    terms ------------------> func
    operators                   params
                                  terms
                                  operators

// btw = 18

// repstr ("eric", 12 + 3)

function Parse: Variant;
function ParseFunction(const aFunc: TEvFunc): Variant;
function ParseParam(const aFunc: TEvFunc): Variant;

PARSE

"repstr" --> checkparamcount = 0 ---> PopFunctionResult --> continue
                             > 0 ---> checkhaakje --> niet ERROR
                                                  --> ParseFunction(its)

    ...PARSEFUNCTION(repstr, var nextparam: boolean) //tel brackets
    "(" --> while PARSEPARAM(var v:variant) = sComma do addvalue

        ...PARSEPARAM
        "eric" --> AddValue
        "," --> V := "eric", Result := sComma, EXIT

        ...PARSEPARAM
        12  : AlphaNum.AddValue --> alphanum := numeric
        "+" : AlphaNum.AddOperator
        3   : AlphaNum.AddValue
        ")" --> V := Num.Result = 12, Result := sCloseBracket, EXIT

    ... PARSEFUNCTION
    sCloseBracket: popresult exit

PARSE


*)



// evalparam (value of calc?)
// term
// evalfunc

const
  ReservedWords: array[0..4] of string =
    ('IF', 'THEN', 'ELSE', 'BEGIN', 'END');

  { =
    >
    >=
    <
    <=
    :=
    and
    or
    xor
    mod
    div
    -
    +
    *
    /
  }
  { functies
    trunc
    round
    cos
    tan
    sin
  }
type
  TState =
    (
      { states }
      sStart, sTerminator,
      { char states }
      sInvalid, sWhiteSpace, sComma, sOpenBracket, sCloseBracket, sOpenQuote, sCloseQuote,
      { tokenstates }
      sInteger, sFloat, sQuoted, sIdentifier,
      { operators }
      sAdd, sSubstract, sDivide, sMultiply,
      { uitgesplitste identifiers }
      tFunction, tVariable
    );

  TOperators = sAdd..sMultiply;

  TStateSet = set of TState;
  TMachineState = (mNone, mFunction, mCalculation);

  TTransitionRec = record
    Prev    : TStateSet;
    Next    : TStateSet;
    pErr    : TEvError;
    nErr    : TEvError;
  end;

const
  CAllStates = [Low(TState)..High(TState)] - [sInvalid, sWhiteSpace];
  COperators = [sAdd..sSubstract];
  CTransitionScheme : array[TState] of TTransitionRec =
    (
      {sStart}
      (Prev: CAllStates;
       Next: CAllStates - [sTerminator]),
      {sTerminator}
      (Prev: CAllStates;
       Next: CAllStates),
      {sInvalid}
      (Prev: CAllStates;
       Next: CAllStates),
      {sWhiteSpace}
      (Prev: CAllStates;
       Next: CAllStates),
      {sComma}
      (Prev : CAllStates;
       Next : [sOpenBracket, sOpenQuote, sCloseQuote, sIdentifier, sInteger, sFloat];
       pErr : eeUnknown;
       nErr : eeStatementExpected),
      {sOpenBracket}
      (Prev: CAllStates;
       Next: CAllStates),
      {sCloseBracket}
      (Prev: CAllStates - COperators;
       Next: [sTerminator, sCloseBracket, sComma]),
      {sOpenQuote}
      (Prev: CAllStates;
       Next: CAllStates),
      {sCloseQuote}
      (Prev: CAllStates;
       Next: CAllStates),
      {sInteger}
      (Prev: [sOpenBracket, sComma] + COperators;
       Next: [sComma, sCloseBracket] + COperators;
       pErr: eeMissingFactor;
       nErr: eeMissingFactor),
      {sFloat}
      (Prev: [sOpenBracket, sComma] + COperators;
       Next: [sComma, sCloseBracket] + COperators),
      {sQuoted}
      (Prev: [sOpenQuote];
       Next: [sCloseQuote]),
      {sIdentifier}
      (Prev: CAllStates;
       Next: CAllStates),
      {sAdd}
      (Prev: CAllStates;
       Next: CAllStates),
      {sSubstract}
      (Prev: CAllStates;
       Next: CAllStates),
      {sDivide}
      (Prev: CAllStates;
       Next: CAllStates),
      {sMultiply}
      (Prev: CAllStates;
       Next: CAllStates),
      {tFunction}
      (Prev: CAllStates;
       Next: [sOpenBracket];
       pErr: eeUnknown;
       nErr: eeOpenBracketExpected),
      {tVariable}
      (Prev: [sOpenBracket, sComma] + COperators;
       Next: [sComma, sCloseBracket] + COperators)
    );

function StateStr(State: TState): string;
begin
  Result := GetEnumName(TypeInfo(TState), Integer(State));
end;

function StatesStr(States: TStateSet): string;
var
  i: TState;
begin
  Result := '';
  for i := Low(TState) to High(TState) do
    if i in States then
      Result := Result + StateStr(i) + ', ';
  Result := CutRight(Result, 2);    
end;

function MachineStateStr(MachineState: TMachineState): string;
begin
  Result := GetEnumName(TypeInfo(TMachineState), Integer(MachineState));
end;


procedure TEval.InternalEvaluate;
var
  State, PrevState, PrevTransition: TState;
  Quoted: boolean;
  OrgPtr: PChar;
  CharPtr: PChar;
  TokenPtr: PChar;
  CurrentToken: string;
  Func: TEvFunctionInfo;
  CurrVar: TEvVar;
  Res: Variant;

    function TokenString: string;
    begin
      SetString(Result, TokenPtr, CharPtr - TokenPtr);
    end;

    function CurrentPosition: integer;
    begin
      Result := CharPtr - OrgPtr;
    end;

    function NextToken: TState;
    var
      P: PChar;
    begin
      if CharPtr^ = #0 then begin Result := sTerminator; Exit; end;
      P := CharPtr;
      TokenPtr := P;

      { check quotes }
      if P^ = '"' then
      begin
        Inc(P);
        Quoted := not Quoted;
        if Quoted then Result := sOpenQuote else Result := sCloseQuote;
      end

      { quoted? }
      else if Quoted then
      begin
        Inc(P);
        while (P^ <> '"') and (P^ in [#1..#255]) do Inc(P);
//        if P^ <> '"' then
  //        AppError('SluitQuotes niet gevonden');
        Result := sQuoted;
      end

      { rest }
      else case P^ of
        '+':
          begin
            Inc(P);
            Result := sAdd;
          end;
        '-':
          begin
            Inc(P);
            Result := sSubstract;
          end;
        '*':
          begin
            Inc(P);
            Result := sMultiply;
          end;
        '/', ':':
          begin
            Inc(P);
            Result := sDivide;
          end;
        ',':
          begin
            Inc(P);
            Result := sComma;
          end;
        'A'..'Z', 'a'..'z', '_':
          begin
            Inc(P);
            while P^ in ['A'..'Z', 'a'..'z', '0'..'9', '_'] do Inc(P);
            Result := sIdentifier;
          end;
        '0'..'9', '.':
          begin
            Result := sInteger;
            if P^ = '.' then Result := sFloat;
            Inc(P);
            while P^ in ['0'..'9', '.'] do
            begin
              if P^ = '.' then Result := sFloat;
              Inc(P);
            end;
          end;
        '(':
          begin
            Inc(P);
            Result := sOpenBracket;
          end;
        ')':
          begin
            Inc(P);
            Result := sCloseBracket;
          end;
        ' ', #13, #10, #9 : { spaties, linefeeds, cariage return, tab}
          begin
            Inc(P);
            Result := sWhiteSpace;
          end;
        else begin
            Inc(P);
            Result := sInvalid;
        end;
      end;
      CharPtr := P;
    end;

    function FindFunction(var aFunc: TEvFunctionInfo): boolean;
    var
      i: integer;
    begin
      i := fFunctions.IndexOf(CurrentToken);
      Result := i >= 0;
      if Result then
        aFunc := TEvFunctionInfo(fFunctions.Objects[i])
      else
        aFunc := nil;
    end;

    function FindVar(var aVar: TEvVar): boolean;
    var
      i: integer;
    begin
      i := fVars.IndexOf(CurrentToken);
      Result := i >= 0;
      if Result then
         aVar := fVars[i]
      else
        aVar := nil;
    end;

    procedure CheckTransition;

       procedure Err(const S: string; Reverse: boolean = False);
       begin
         AppError('Transitiefout ' + IIF(Reverse, 'omgekeerd ', '') + Chr(13) +
                  ' (' + StateStr(PrevTransition) + ' - ' + StateStr(State) + ')' + Chr(13) +
                  S);
       end;

       procedure Err2(Err: TEvError);
       var
         Inf: TParseInfoRec;
       begin
         Inf.Position := CharPtr - OrgPtr + 1;
         ParseError(Err, Inf, []);
       end;

    begin
      { check next }
      with CTransitionScheme[PrevTransition] do
        if not (State in Next) then
          Err2(nErr);
          //Err('Verwacht: ' + StatesStr(Next));
      { check curr}
      if not (PrevTransition in CTransitionScheme[State].Prev) then
        Err('Vorige verwacht: ' + StatesStr(CTransitionScheme[State].Prev), True);

    end;

    procedure DoEvaluate(MachineState: TMachineState);
    label
      ExitLevel;
    var
      OpenBrackets: integer;
      //CanAcceptParam: boolean;
    begin

      //CanAcceptParam := False;
      //Log(['in eval ', charptr-orgptr, charptr^]);
      //try

      while not (State in [sTerminator, sInvalid]) do
      begin

        if State <> sWhiteSpace then PrevTransition := State;
        PrevState := State;
        State := NextToken;
        if State = sWhiteSpace then Continue;
        CheckTransition;

        case State of
          Low(TOperators)..High(TOperators):
             begin
               Log(['operator']);
             end;
          sOpenBracket:
            begin
              Inc(OpenBrackets);
              {if MachineState = mFunction then
                if OpenBrackets = 1 then
                  Log(['Open functie', CharPtr^]); }
              //CanAcceptParam := True;
            end;
          sCloseBracket:
            begin
              Dec(OpenBrackets);
              if OpenBrackets < 0 then
                AppError('teveel sluithaakjes');
              if (OpenBrackets = 0) and (MachineState = mFunction) then
                begin
                  Res := fStack.Pop;
                  goto ExitLevel;
                end;
            end;
          sComma:
            begin
              //CanAcceptParam := True;
            end;
          sInteger:
            begin
              //if not CanAcceptParam then AppError('eerst een komma!');
              CurrentToken := TokenString;
              fStack.Peek.AddParam(Integer(s2i(CurrentToken)));
              //CanAcceptParam := False;
            end;
          sFloat:
            begin
              CurrentToken := TokenString;
              DecimalSeparator := '.';
              fStack.Peek.AddParam(StrToFloat(CurrentToken));
              //CanAcceptParam := False;
            end;
          sQuoted:
            begin
              CurrentToken := TokenString;
              fStack.Peek.AddParam(CurrentToken);
              //CanAcceptParam := False;
            end;
          sIdentifier:
            begin
              CurrentToken := TokenString;
              if FindFunction(Func) then
              begin
                fStack.PushFunction(Func);
                State := tFunction;
                DoEvaluate(mFunction);
              end
              else if FindVar(CurrVar) then
              begin
                fStack.Peek.AddParam(CurrVar.Value);
                State := tVariable;
              end
              else begin
                AppError('onbekende identifier ' + CurrentToken)
              end;
            end;
        end;
      end;

      ExitLevel:
        //Log(['uit eval ', charptr-orgptr, charptr^]);

      //finally
        //fStack.Clear;
        //Dec(Recursive);
      //end;

    end;

begin
  if fExpression = '' then Exit;
  InternalReset;
  State := sStart;
  OrgPtr := @fExpression[1];
  CharPtr := @fExpression[1];
  Res := Unassigned;
  DoEvaluate(mNone);
  fValue := Res;
end;

end.


