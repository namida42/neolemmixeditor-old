
{**************************************************************}
{                                                              }
{    (c) Eric Langedijk                                        }
{                                                              }
{    Debugfuncties                                             }
{                                                              }
{**************************************************************}

unit ULog;


interface

{$IFNDEF EXEVERSION}

uses
  Windows, Classes, Controls, Graphics, Sysutils, Forms,
  FDebug;

type
  PTextFile = ^TextFile;


procedure Deb(const AValues: array of const{; Dlg: boolean = False});
(*procedure ClearLog; *)
procedure DebList(const AList: TStrings);
(*procedure LogArray(const AArray: array of string);
procedure LogIntArray(const AArray: array of integer);
procedure WaitWindow(const S: string; DoWait: boolean = False);
//procedure DebugOptions;
function LogTxt: PTextFile;
*)

var
  LogOn: boolean;
  LogWidth: integer = 360;//500;//420;
  LogHeight: integer = 600;
  DebugFormAlign: TAlign = alRight;
//  DebugFormAlign: TAlign = alNone;
  LogToFile: boolean;
  Finalizing: boolean;
  WaitWin: TForm;
  LogDown: Boolean = True;
  FFrmDebug: TDebugForm; // globale voor debugschermpje

function FrmDebug: TDebugForm; // load on demand

{$ENDIF}

implementation

{$IFNDEF EXEVERSION}

uses
  UMisc, UFastStrings;

var
  _LogTxt: PTextFile;

function LogTxt: PTextFile; // load on demand
begin
  if _LogTxt = nil then
  begin
    New(_LogTxt);
    AssignFile(_LogTxt^, ApplicationPath + 'log.txt');
    Rewrite(LogTxt^);
  end;
  Result := _LogTxt;
end;

function FrmDebug: TDebugForm; // load on demand
var
{  LB: TListBox;}
  CurrentForm: TForm;
begin
  CurrentForm := nil;
  if FFrmDebug = nil then
  begin
    CurrentForm := Screen.ActiveForm;
    FFrmDebug := TDebugForm.Create(Application);
    FFrmDebug.Caption := 'Debug';
    FFrmDebug.Height := LogHeight;//FFrmDebug.Height * 2;
    FFrmDebug.Width := LogWidth;//FFrmDebug.Height * 2;

    { zet positie rechts van mainform}
    if not LogDown then
    begin
    if Application.MainForm <> nil then
    begin
      if Screen.Width - Application.MainForm.Width > 100 then
        FFrmDebug.Width := Screen.Width - Application.MainForm.Width - 1;
    end;

    FFrmDebug.Left := Screen.Width - FFrmDebug.Width;
    FFrmDebug.Top := 0;//Screen.Width - FFrmDebug.Width;
    end
    else begin


    (*
    { zet positie onder mainform }
    if Application.MainForm <> nil then
    begin
      if Screen.Height - Application.MainForm.Height > 100 then
      begin
        FFrmDebug.Height := Screen.Height - Application.MainForm.Height - 1;
        FFrmDebug.Top := Application.MainForm.Top + Application.MainForm.Height;
        FFrmDebug.Left := 0;
        FFrmDebug.Width := Application.MainForm.Width;//Screen.Width;
      end;
    end;

    *)

    end;

//    FFrmDebug.Left := 0;
//    FFrmDebug.Width := Screen.Width;
//    FFrmDebug.Top := 0;//Screen.Width - FFrmDebug.Width;


  end;
  if not FFrmDebug.Visible then
  begin
    FFrmDebug.Show;
    FFrmDebug.Align := DebugFormAlign;
    if Assigned(CurrentForm) and CurrentForm.Visible then CurrentForm.SetFocus;
  end;
  Result := FFrmDebug;
end;

procedure Deb(const AValues: array of const{; Dlg: boolean = False});

//var
  //Count: integer;

    function SetStr(Brackets: Boolean = True): string;
    var
      i: integer;
    begin
      Result := '';
{      if Length(aValues) = 1 then
      begin
        Result := TransForm(AValues[0]);
      end
      else}
      for i := Low (AValues) to High (AValues) do
      begin
        Result := Result + IIF(Brackets, '[', '') + TransForm(AValues[i]) + IIF(Brackets, ']', '');
      end;
      Result := FastReplace(Result, '#0', ' '{, [rfReplaceAll]});
    end;

var
  S: string;
  Lines: TStringList;
  i: Integer;
begin
//  Sep := '['IIF(Brackets, '[', ' ');
//  Count :=
  try
    S := SetStr;
    if LogOn then
    begin
//      if Dlg then WinDlg(S);
      if Assigned(Application) and not Application.Terminated then
      begin
        if Pos(CrLf, S) > 0 then
        begin
          S := SetStr(False);
          Lines := TStringList.Create;
          try
            Lines.Text := S;
            for i := 0 to Lines.Count - 1 do
            begin
              FrmDebug.AddStr(Lines[i]);
            end;
          finally
            Lines.Free;
          end;
        end
        else
          FrmDebug.AddStr(S);
      end;
    end;
    if LogToFile and not Finalizing then
    begin
      WriteLn(LogTxt^, S);
      Flush(LogTxt^);
    end;
  except
    on E: Exception do WinDlg('Fout in UMisc.Log: ' + E.Message);
  end;
end;

procedure ClearLog;
begin
  if Assigned(Application) and not Application.Terminated then
  begin
    FrmDebug.DoClear;
  end;
end;

procedure DebList(const AList: TStrings);
var
  i: integer;
begin
  if aList.Count > 1000 then
  begin
    for i := 0 to AList.Count - 1 do
      if i mod 100 = 0 then
        Deb([i, AList[i]]);

  end
  else
  for i := 0 to AList.Count - 1 do Deb([AList[i]]);
end;

procedure LogArray(const AArray: array of string);
var
  i: integer;
begin
  for i := 0 to Length(AArray) - 1 do Deb([AArray[i]]);
end;

procedure LogIntArray(const AArray: array of integer);
var
  i: integer;
begin
  for i := 0 to Length(AArray) - 1 do Deb([AArray[i]]);
end;

{function RectStr(const aRect: TRect): string;
begin
  with arect do
  Result := inttostr(left) + ',' + inttostr(top) + ',' + inttostr(right) + ','  + inttostr(bottom);

end;}

procedure WaitWindow(const S: string; DoWait: boolean = False);
var
  R: TRect;
begin
  if WaitWin = nil then
  begin
    WaitWin := TForm.Create(Application);
    with WaitWin do
    begin
      Left := Screen.Width - 200;
      Width := 196;
      Top := 4;
      Height := 80;
//      Enabled := False;
      Canvas.Brush.Color := Color;
      Canvas.Brush.Style := bsSolid;
      BorderIcons := [];
      BorderStyle := bsNone;
      FormStyle := fsStayOnTop;
      with Constraints do
      begin
        MaxWidth := 296;
        MinWidth := 100;
        MaxHeight := 196;
        MinHeight := 80;
      end;
    end;
  end;
  if not WaitWin.Visible then
    WaitWin.Show;
  with WaitWin, Canvas do
  begin
    R := WaitWin.ClientRect;
//    DrawText(Handle, PChar(S), Length(S), R, DT_TOP + DT_CALCRECT + DT_WORDBREAK);
//    Log([RectStr(R)]);
//    SetBounds(R.Left, R.Top, R.Right - R.Left, R.Top - R.Bottom);
    DrawText(Handle, PChar(S), Length(S), R, DT_TOP + DT_WORDBREAK);
    WaitWin.Update;
  end;

  if DoWait then
  begin
//    _WaitHandler.Wait;
//    Application.ProcessMessages;
  end;

end;

initialization
  _LogTxt := nil;
  LogOn := True;
  LogToFile := True;
//  SetLogProc(Log); umisc
finalization
  Finalizing := True;
  if _LogTxt <> nil then
  begin
    CloseFile(_LogTxt^);
    Dispose(_LogTxt);
    _LogTxt := nil;
  end;

{$ENDIF}

end.

//umisc
