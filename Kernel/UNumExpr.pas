unit UNumExpr;

interface

uses
  Classes, Controls, SysUtils,
  UMisc, UTools;

  { states }
type
  TState = (sNone, sReady, sError, sNumeric,
            sOpenBracket, sCloseBracket,
            sAdd, sSubstract, sDivide, sMultiply);

const
  MaxTokenLen = 254;

type
  { operators }
  TOperator = sAdd..sMultiply;

  TSeqRange = 0..31;
  TStackRange = 0..31;

  { record voor sequentiele berekening }
  TStackRec = record
    VCount        : Integer;
    OCount        : Integer;
    V             : array[TSeqRange] of Double;
    O             : array[TSeqRange] of TOperator;
  end;

  TStackArray = array[TStackRange] of TStackRec;

type
  TEvaluationError =
    (
      eeOK,
      eeEmptyExpression,
      eeEmptySubExpression,
      eeTokenTooLong,
      eeInvalidChar,

      eeFloatingPoint,
      eeResolve,
      eeExpressionExpected,
      eeMissingOperator,
      eeTooFewCloseBrackets,

      eeTooMuchCloseBrackets
    );

const
  SEvaluationError: array[TEvaluationError] of string =
    (
      'OK',
      'Lege expressie',
      'Lege subexpressie',
      'Expressie te lang',
      'Ongeldig karakter',

      'Floating point fout',
      'Resolvefout',
      'Expressie verwacht',
      'Te weinig operators',
      'Te weinig sluithaakjes',

      'Te veel sluithaakjes'
    );

type
  EEvaluationError = class(Exception)
  private
    fCode: TEvaluationError;
  public
    property Code: TEvaluationError read fCode;
  end;

type
  TNumericEvaluator = class
  private
    fStack                     : TStackArray;
    fStackSize                 : integer;
  { evaluate vars }
    fToken                     : ShortString;   { string buffer }
    fError                     : TEvaluationError;
  { properties }
    fExpression                : string;
    fValue                     : Double;
    procedure ShowStack;
        { debug }
    function GetExpr(const Rec: TStackRec): string;
        { converteert een StackRec naar een expressie }
    procedure Reset;
        { zet stacksize op 0 }
    procedure Error(aError: TEvaluationError);
        { genereert een exception }
    procedure ErrorFmt(aError: TEvaluationError; const Args: array of const);
        { genereert een exception }
    procedure AddValue(const aValue: Double);
        { voegt een Double value toe aan hoogste stackitem }
    procedure AddOperator(aOperator: TOperator);
        { voegt een operator toe aan hoogste stackitem }
    procedure Push;
        { maakt stacksize groter }
    procedure PopResult;
        { popt een berekening van stack en voegt het resultaat een niveau lager toe. als stacksize = 1 dan wordt er niet gepopt }
    procedure PopOperator;
        { popt een operator van stack }
    procedure ResolveStack;
        { einde berekening: PopResult tot stack op 1, result komt in V[0] }
    procedure CheckVanDalen(Op: TOperator);
        { checkt of er een value een niveau omhoog moet voordat er een operator toegevoegd wordt  }
    function DoCalcRec(const Rec: TStackRec): Double;
    procedure SetExpression(const Value: string);
        { berekent sequentieel of roept functie aan }
  protected
  public
    constructor Create;
    destructor Destroy; override;
    function EvaluateNumeric(const S: string): Double;
    property Expression: string read fExpression write SetExpression;
    property Value: Double read fValue;
  published
  end;

function OpStr(Op: TOperator): string;
function StateStr(State: TState): string;

implementation

uses
  TypInfo, Math,
  ULog,
  UExprFuncs;

{ functie types }
type
  TIntFunc = function(V1, V2: Integer): Integer;
  TDoubleFunc = function(const V1, V2: Double): Double;

{ Interne Integer functies }
function DoAddInts(V1, V2: Integer): Integer;
begin
  Result := V1 + V2;
end;

function DoSubstractInts(V1, V2: Integer): Integer;
begin
  Result := V1 - V2;
end;

function DoDivideInts(V1, V2: Integer): Integer;
begin
  Result := V1 div V2;
end;

function DoMultiplyInts(V1, V2: Integer): Integer;
begin
  Result := V1 * V2;
end;

{ Interne Double functies }
function DoAddDoubles(const V1, V2: Double): Double;
begin
  Result := V1 + V2;
end;

function DoSubstractDoubles(const V1, V2: Double): Double;
begin
  Result := V1 - V2;
end;

function DoDivideDoubles(const V1, V2: Double): Double;
begin
  Result := V1/V2;
end;

function DoMultiplyDoubles(const V1, V2: Double): Double;
begin
  Result := V1 * V2;
end;

const
  IntFuncs: array[TOperator] of TIntFunc =
    (DoAddInts, DoSubstractInts, DoDivideInts, DoMultiplyInts);

  DoubleFuncs: array[TOperator] of TDoubleFunc =
    (DoAddDoubles, DoSubstractDoubles, DoDivideDoubles, DoMultiplyDoubles);

function DoCalcInts(V1, V2: integer; Op: TOperator): integer;
begin
  Result := IntFuncs[Op](V1, V2);
end;

function DoCalcDoubles(const V1, V2: Double; Op: TOperator): Double;
begin
  Result := DoubleFuncs[Op](V1, V2);
end;

{ diversen }

function OpStr(Op: TOperator): string;
begin
  case Op of
    sAdd       : Result := '+';
    sSubStract : Result := '-';
    sDivide    : Result := '/';
    sMultiPly  : Result := '*';
  else
    Result := '?';
  end;
end;

function StateStr(State: TState): string;
begin
  Result := GetEnumName(TypeInfo(TState), Integer(State));
end;


{ TNumericEvaluator }

procedure TNumericEvaluator.AddOperator(aOperator: TOperator);

    {++  -->  +}  {+-  -->  -}  {-+  -->  +}  {--  -->  +}
    function MixOperators(O1, O2: TOperator): TOperator;
    var
      A, B: integer;
    begin
      case O1 of
        sAdd       : case O2 of
                       sAdd       : Result := sAdd;
                       sSubstract : Result := sSubStract;
                     end;
        sSubstract : case O2 of
                       sAdd       : Result := sSubstract;
                       sSubstract : Result := sAdd;
                     end;
      end;
    end;

var
  Mixed: boolean;
  LastOp: TOperator;

begin
  if fStackSize = 0 then Push;

  CheckVanDalen(aOperator);

  Mixed := False;

  with fStack[fStackSize - 1] do
  begin
    { check of expr begint met * of /}
    if (VCount = 0) and (aOperator in [sDivide, sMultiply]) then
      Error(eeExpressionExpected);

    { als er evenveel operators als values zijn, dan zullen er nu twee
      operators achter elkaar komen in de expressie }
    if (OCount > 0) and (OCount >= VCount) then
    begin
      case aOperator of
        sDivide, sMultiply:
          Error(eeExpressionExpected);
      end;

{     case O[OCount - 1] of
        sAdd, sSubstract, sDivide, sMultiply
      end; }

      if (aOperator in [sAdd, sSubstract])
      and (O[OCount - 1] in [sAdd, sSubstract]) then
      begin
        O[OCount - 1] := MixOperators(O[OCount - 1], aOperator);
        Mixed := True;
      end
//      else
  //      AppError('operator fout'); { - toevoegen, + negeren }
    end;

    if not Mixed then
    begin
      Inc(OCount);
      O[OCount - 1]:= aOperator;
    end;
  end;
end;

procedure TNumericEvaluator.AddValue(const aValue: Double);
begin
  if fStackSize = 0 then Push;

  with fStack[fStackSize - 1] do
  begin
    Inc(VCount);
    V[VCount - 1]:= aValue;
    { merge operator met - of + }
    if VCount = OCount then
    begin
      case O[OCount - 1] of
        sAdd:
          begin
            PopOperator; { negeer een losse + }
          end;
        sSubStract:
          begin
            PopOperator; { weg met die - }
            V[VCount - 1] := -V[VCount - 1];
          end;
      end;
    end;
  end;
end;

procedure TNumericEvaluator.CheckVanDalen(Op: TOperator);
var
  Temp: Double;
begin
  if fStackSize = 0 then Exit;

  with fStack[fStackSize - 1] do
  if (VCount > 1) and (OCount = VCount - 1) then
    case Op of
(*      { force push ORIGINEEL}
      sDivide, sMultiply:
        if (O[OCount - 1] < Op) then
        begin
          Temp := V[VCount - 1];
          Dec(VCount); { delete value }
          Push;
          AddValue(Temp); { insert value }
        end; *)
      sDivide, sMultiply:
        if (O[OCount - 1] < Op) and (O[OCount - 1] in [sAdd, sSubstract]) then
        begin
          Temp := V[VCount - 1];
          Dec(VCount); { delete value }
          Push;
          AddValue(Temp); { insert value }
        end;
      sAdd, sSubstract:
        { force pop }
        if (O[OCount - 1] > Op) and (O[OCount - 1] in [sDivide, sMultiply]) then
          PopResult;
    end; // case
end;


constructor TNumericEvaluator.Create;
begin
  inherited Create;
  SetLength(fToken, MaxTokenLen + 1);
  fToken[255] := #0;
end;

destructor TNumericEvaluator.Destroy;
begin
  inherited Destroy;
end;

function TNumericEvaluator.DoCalcRec(const Rec: TStackRec): Double;
var
  i: integer;
begin
  with Rec do
  begin
    if VCount = 0 then Error(eeEmptySubExpression);
    { check of expressie niet met een operator eindigt }
    if VCount < OCount + 1 then
      Error(eeExpressionExpected)
    { check of het aantal operators klopt }
    else if VCount > OCount + 1 then Error(eeMissingOperator);
    Result := V[0];
    { sequentieel }
    for i := 0 to VCount - 2 do
//      DoubleFuncs[Op](V1, V2);
      Result := DoCalcDoubles(Result, V[i + 1], O[i]);
  end;
end;

procedure TNumericEvaluator.PopResult;
var
  aTemp: Double;
begin
  aTemp := 0;
  if fStackSize > 0 then
  begin
    if fStack[fStackSize - 1].VCount = 0 then EXIT;
    { bereken sequentieel het stackrec }
    aTemp := DoCalcRec(fStack[fStackSize - 1]);
//      if fStackSize > 1 then
    with fStack[fStackSize - 1] do
    begin
      VCount := 0;
      OCount := 0;
      if fStackSize > 1 then
      begin
        Dec(fStackSize); { alleen poppen als we mogen poppen }
      end;
    end;
    { voeg niveau lager (of laagste niveau) param toe }
    AddValue(aTemp)
  end;
end;


procedure TNumericEvaluator.Push;
begin
  if fStackSize >= High(TStackRange) then
    AppError('push fout, te weinig stack');
  Inc(fStackSize);
//  Log([fstacksize]);
  with fStack[fStackSize - 1] do
  begin
//    FillDWord(fStack[fStackSize - 1], 5, 0);
    VCount := 0;
    OCount := 0;
  end;
end;

procedure TNumericEvaluator.ResolveStack;
var
  i: integer;
begin
  i := 0;
  while (fStackSize > 0) do
  begin
    PopResult;
    if (fStackSize = 1) and (fStack[fStackSize - 1].VCount  = 1) then Break;
    Inc(i);
    if i > 30 then
      AppError('Oneindige loop bij resolve stack');
  end;

  if not ((fStackSize = 1) and (fStack[0].VCount = 1)) then
  begin
    AppError('Geen resultaat gepopt bij resolve stack');
  end;
end;

procedure TNumericEvaluator.Reset;
begin
  fStackSize := 0;
  fError := eeOK;
end;

procedure TNumericEvaluator.ShowStack;
var
  i,j: integer;
  sp:string;
  vs,vo:string;
begin
  for i := fStackSize - 1 downto 0 do
  with fStack[i] do
  begin

    sp := stringofchar('.', i);
    Log([sp, getexpr(fStack[i])]);
(*    vs:='';
    vo:='';
//    for j:=0 to vcount - 1 do vs:=vs+':'+i2s(v[j]);
//   for j:=0 to ocount - 1 do vo:=vo+':'+opstr(o[j]);

    for j := 0 to vcount - 1 do
      vs := vs + ' ' + f2s(v[j]) + ' ' + IIF(j < ocount, opstr(o[j]), '.');

    Log(['st', PadR(i2s(i), 5), sp, vs]); *)
//    Log(['stack   ', PadR(i2s(i), 3), sp, vs]);
//    Log(['operator', PadR(i2s(i), 3), sp, vo]);
  end;
end;

procedure TNumericEvaluator.Error(aError: TEvaluationError);
var
  E: EEvaluationError;
begin
  E := EEvaluationError.Create(SEvaluationError[aError]);
  E.fCode := aError;
  raise E;
end;

procedure TNumericEvaluator.ErrorFmt(aError: TEvaluationError; const Args: array of const);
var
  S: string;
  E: EEvaluationError;
begin
  try
    S := Format(SEvaluationError[aError], Args)
  except
    S := SEvaluationError[aError];
  end;
  E := EEvaluationError.Create(S);
  E.fCode := aError;
  raise E;
end;

procedure TNumericEvaluator.PopOperator;
begin
  with fStack[fStackSize - 1] do
    Dec(OCount);
end;

function TNumericEvaluator.GetExpr(const Rec: TStackRec): string;
var
  i: integer;
begin
  Result := '';
  with Rec do
  begin


      if OCount > 0 then
      begin
        for i := 0 to Min(VCount - 1, OCount) do
        begin
          Result := Result + f2s(V[i]) + ' ' + IIF(i < OCount, OpStr(O[i]), '') + ' '
        end;
        if OCount >= VCount - 1 then
          for i := Max(0, VCount - 1) to OCount - 1 do
            Result := Result + OpStr(O[i]) + ' '
        else if VCount > OCount then
          for i := Max(0, OCount - 1) to VCount - 1 do
            Result := Result + f2s(V[i]) + ' '
      end
      else
        for i := 0 to VCount - 1 do
          Result := f2s(V[i]) + IIF(i < VCount - 1, ', ', '');
  end;
  Result := TrimRight(Result);
end;

function TNumericEvaluator.EvaluateNumeric(const S: string): Double;
var
  i: integer;
  C: Char;
  OpenCount, CloseCount: integer;
  State, PrevState: TState;
  TokenLen: integer;
  CurrVal: Extended;
  N: integer;
  OldSep: Char;

begin
  Reset;
  Result := 0;
  OpenCount := 0;
  CloseCount := 0;
  State := sNone;
  TokenLen := 0;

  OldSep := DecimalSeparator;
  DecimalSeparator := '.';

  try
(*  try *)

  if S = '' then
    Error(eeEmptyExpression);

  { push om te starten ��n record }
  Push;

  { we gaan aan het eind nog 1x de loop in vanwege prevstate }
  for i := 1 to Length(S) + 1 do
  begin

    PrevState := State;

    if i > Length(S) then
      State := sReady
    else begin
      C := S[i];
      case C of
        ' ', #13, #10          : Continue;
        '0'..'9'               : State := sNumeric;
        '.'                    : State := sNumeric;
        '('                    : State := sOpenBracket;
        ')'                    : State := sCloseBracket;
        '+'                    : State := sAdd;
        '-'                    : State := sSubstract;
        '/', ':'               : State := sDivide;
        '*'                    : State := sMultiply;
      else
        State := sError;
      end; { case }

    end; // not ready

    if State = sError then
      Error(eeInvalidChar);

    if (PrevState = sNumeric) and (State <> sNumeric) then
    begin
      { getal compleet: add aan huidige stackrec }
      CurrVal := StrToFloat(fToken);
      AddValue(CurrVal);
      TokenLen := 0;
    end;

    case State of
      sNumeric:
        begin
          Inc(TokenLen);
          if TokenLen > MaxTokenLen then Error(eeTokenTooLong);
          fToken[0] := Chr(TokenLen);
          fToken[TokenLen] := C;
        end;
      Low(TOperator) .. High(TOperator):
        begin
          AddOperator(State);
        end;
      sOpenBracket:
        begin
          Inc(OpenCount);
          if OpenCount < CloseCount then Error(eeTooFewCloseBrackets);
          Push;
        end;
      sCloseBracket:
        begin
          Inc(CloseCount);
          if CloseCount > OpenCount then Error(eeTooMuchCloseBrackets);
          PopResult;
        end;
      sReady:
        begin
          if CloseCount <> OpenCount then
          begin
            if CloseCount > OpenCount
            then Error(eeTooMuchCloseBrackets)
            else Error(eeTooFewCloseBrackets);
          end;
          ResolveStack;
          Result := fStack[0].V[0];
        end;
    end;
  end;

(*  except
    on E: Exception do
    begin
      if E is EEvaluationError then
      begin
        fError := EEvaluationError(E).fCode;
        windlg([e.classname, e.message + ' op positie ' + i2s(i), '(' + C + ')']);
      end
      else
        windlg([e.classname, e.message]);
    end;
  end; *)

  finally
    DecimalSeparator := OldSep;
  end;

end;

procedure TNumericEvaluator.SetExpression(const Value: string);
begin
  if CompareText(Value, fExpression) = 0 then Exit;
  fExpression := Value;
  fValue := EvaluateNumeric(fExpression);
end;

end.

