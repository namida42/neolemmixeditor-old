unit UMxStrings;

interface

uses
  Windows, Classes, SysUtils, Math, Graphics, Dialogs, Forms,
  Controls, DateUtils, // dates
  UMisc, UMxMisc, UMx;//, UGraphics;//, ulog;

type
  TCustomStringMatrix = class;

  TMatrixStringItem = record
    fString: string;
    fObject: TObject;
  end;

  TMatrixStringItemArray = array of TMatrixStringItem;

  TMatrixStrings = class
  private
    fMatrix: TCustomStringMatrix;
    fItems: array of TMatrixStringItemArray;
    fOwnsObjects: Boolean;
    procedure CheckCoord(aCol, aRow: integer);
    function CheckColRow(aCol, aRow: integer): boolean;
    procedure EnsureColRow(aCol, aRow: integer);
    procedure InternalDeleteCol(aCol, aRow: integer);
    procedure InternalInsertCol(aCol, aRow: integer);
    procedure InternalDeleteRow(aRow: integer);
    procedure InternalInsertRow(aRow: integer);
    procedure InternalSetString(aCol, aRow: integer; const S: string);
  public
    constructor Create(aMatrix: TCustomStringMatrix);
    destructor Destroy; override;
    procedure MoveColumn(A, B: integer);
    procedure MoveRow(A, B: integer);
    procedure SetString(aCol, aRow: integer; const S: string);
    function GetString(aCol, aRow: integer): string;
    procedure SetObject(aCol, aRow: integer; const O: TObject);
    function GetObject(aCol, aRow: integer): TObject;
    function LastColCount: integer;
    function GetRowCount: integer;
    procedure InsertRow(R: integer);
    procedure AssignCol(aCol: integer; const aStrings: TStrings);
    procedure Clear;
    property OwnsObjects: Boolean read fOwnsObjects write fOwnsObjects;
  end;


  TCustomStringMatrix = class(TSuperMatrix{CustomSuperMatrix})
  protected
    fOwnsObjects: Boolean;
    fMatrixStrings: TMatrixStrings;
  { property access }
    function GetCell(X, Y: Integer): string;
    procedure SetCell(X, Y: Integer; const Value: string);
    function GetObject(X, Y: Integer): TObject;
    procedure SetObject(X, Y: Integer; const Value: TObject);
    procedure SetOwnsObjects(const Value: Boolean);
  { protected overrides }
    procedure DoAfterColumnIndexChanged(Item: TMxColumn; OldIndex, NewIndex: Integer); override;
    procedure DoAfterDeletingColumn(aItem: TMxColumn); override;
    procedure DoGetCellData(aCol, aRow: Integer; out Data: TCellData); override;
    procedure DoSetCellData(aCol, aRow: Integer; Data: TCellData); override;
    property Cells[X, Y: Integer]: string read GetCell write SetCell;
    property Objects[X, Y: Integer]: TObject read GetObject write SetObject;
    property OwnsObjects: Boolean read fOwnsObjects write SetOwnsObjects;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
  published
  end;

  TStringMatrix = class(TCustomStringMatrix)
  private
  protected
  public
    property Cells;
    property Objects;
  published
    property OwnsObjects: Boolean read fOwnsObjects write SetOwnsObjects;
  end;

  TListBoxMatrix = class(TCustomStringMatrix)
  private
    fItems: TStrings;
    procedure SetItems(const Value: TStrings);
    function GetItemIndex: Integer;
    procedure SetItemIndex(const Value: Integer);
  protected
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex;
  published
    property OwnsObjects: Boolean read fOwnsObjects write SetOwnsObjects;
    property Items: TStrings read fItems write SetItems;
  end;

  TCalendarCellData = class(TCellData)
  private
    fDate: TDate;
  protected
  public
    property Date: TDate read fDate;
  published
  end;

  TCalendarMatrix = class(TBaseMatrix)
  private
    fDateOffset: Integer;
    fStartOfWeek: Integer;
    fMinimumDate: TDate;
    fMaximumDate: TDate;
    fCalendarCellData: TCalendarCellData;
    fOddStyle: TCellStyle;
    fShowDateHeader: Boolean;
    fShowDayHeaders: Boolean;
    fShowWeeks: Boolean;
//    fForeGround: TBitmap;
  //  fTemp: TBitmap;
    function CalcRowCount: Integer;
    function ColRowToDate(aCol, aRow: Integer): TDate;
    function DateToColRow(aDate: TDate): TPoint;
    function GetActiveDate: TDate;
    function ColToDay(aCol: Integer): Integer;
    procedure SetActiveDate(const Value: TDate);
    procedure SetShowDateHeader(const Value: Boolean);
    procedure SetShowDayHeaders(const Value: Boolean);
    procedure SetShowWeeks(const Value: Boolean);
  protected
    procedure CustomAfterPaint(const PaintInfo: TPaintInfo); override;
    procedure DoCellEnter(aCol, aRow: Integer); override;
    procedure DoGetCellData(aCol, aRow: Integer; out Data: TCellData); override;
    function DoGetHeaderText(aHeader: TMatrixHeader; aIndex: Integer): string; override;
    procedure PreparePaintCell(Params: TCellParams; Data: TCellData); override;
    function Navigation(aReason: TScrollReason; aCommand: TScrollCommand; XParam: Integer = -1; YParam: Integer = -1): TNavigationResult; override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  public
  published
  { inherited properties }
    property BevelWidth;
    property BorderWidth;
    property CellStyle;
    property DefaultLineColor;
    property DefaultHeaderLineColor;
    property Ctl3D;
    property BorderStyle;
    property Color;
    property Options;
    property TopHeaders;
    property LeftHeaders;
    property VisibleColsRequested;
    property VisibleRowsRequested;
  { inherited events }
    property OnCellDblClick;
    property OnSelectCell;
  { calendar properties }
    property MinimumDate: TDate read fMinimumDate;
    property MaximumDate: TDate read fMaximumDate;
    property ActiveDate: TDate read GetActiveDate write SetActiveDate;
    property ShowDayHeaders: Boolean read fShowDayHeaders write SetShowDayHeaders;
    property ShowDateHeader: Boolean read fShowDateHeader write SetShowDateHeader;
    property ShowWeeks: Boolean read fShowWeeks write SetShowWeeks;
  end;

  TMxNode = class(TMxRow)
  end;

  TTreeMatrix = class(TSuperMatrix)
  private
    function GetChildCount(aRow: Integer): Integer;
    procedure SetChildCount(aRow: Integer; const Value: Integer);
    procedure CustomPaintCell(Params: TCellParams; const PaintInfo: TPaintInfo; Data: TCellData); override;
  protected
    function DoCreateRows: TMxRows; override;
  public
    constructor Create(aOwner: TComponent); override;
    property ChildCount[aRow: Integer]: Integer read GetChildCount write SetChildCount;
    function AddNode(aParentNode: TMxNode): TMxNode;
  published
  end;

implementation

{ TMatrixStrings }

function TMatrixStrings.CheckColRow(aCol, aRow: integer): boolean;
begin
{  Result := (Length(fData) >= aRow + 1) and
            (Length(fData[aRow]) >= aCol + 1); }
  Result := (Length(fItems) >= aRow + 1) and
            (Length(fItems[aRow]) >= aCol + 1);
end;

procedure TMatrixStrings.CheckCoord(aCol, aRow: integer);
begin
  if (aCol < 0) or (aRow < 0) then
    raise Exception.CreateFmt('ColRow fout in ' + ClassName + ' (%d, %d) ', [aCol, aRow]);
end;

constructor TMatrixStrings.Create(aMatrix: TCustomStringMatrix);
begin
  inherited Create;
  fMatrix := aMatrix;
end;

destructor TMatrixStrings.Destroy;
begin
//  fData := nil;
  fItems := nil;
  inherited Destroy;
end;

procedure TMatrixStrings.EnsureColRow(aCol, aRow: integer);
var
  L, Delta: integer;
begin
  CheckCoord(aCol, aRow);

  { smart allocation }
{  L := Length(fData);
  if L > 64 then Delta := L div 4 else
    if L > 8 then Delta := 16 else
      Delta := 4;
  if L < aRow + 1 then
  begin
    SetLength(fData, aRow + 1 + Delta);
  end;
  if Length(fData[aRow]) < aCol + 1 then
    SetLength(fData[aRow], aCol + 1); }

  { smart allocation }
  L := Length(fItems);
  if L > 64 then Delta := L div 4 else
    if L > 8 then Delta := 16 else
      Delta := 4;
  if L < aRow + 1 then
  begin
    SetLength(fItems, aRow + 1 + Delta);
    // initialiseren op nil nodig?
  end;
  if Length(fItems[aRow]) < aCol + 1 then
    SetLength(fItems[aRow], aCol + 1);
end;

function TMatrixStrings.GetObject(aCol, aRow: integer): TObject;
begin
  if CheckColRow(aCol, ARow) then Result := fItems[aRow, aCol].fObject else Result := nil;
end;

function TMatrixStrings.GetString(aCol, aRow: integer): string;
begin
//  if CheckColRow(aCol, ARow) then Result := fData[aRow, aCol] else Result := '';
  if CheckColRow(aCol, ARow) then Result := fItems[aRow, aCol].fString else Result := '';
end;

procedure TMatrixStrings.InternalDeleteCol(aCol, aRow: integer);
var
  TempItems: TMatrixStringItemArray;
  i: integer;
begin
  TempItems := Copy(fItems[aRow]);
  for i := 0 to aCol - 1 do
    fItems[aRow][i] := TempItems[i];
  for i := aCol to Length(TempItems) - 2 do
    fItems[aRow][i] := TempItems[i + 1];
  SetLength(fItems[aRow], Length(fItems[aRow]) - 1);
end;

procedure TMatrixStrings.InternalInsertCol(aCol, aRow: integer);
var
  TempItems: TMatrixStringItemArray;
  i: integer;
begin
  TempItems := Copy(fItems[aRow]);
  SetLength(fItems[aRow], Length(fItems[aRow]) + 1);
  for i := 0 to aCol - 1 do
    fItems[aRow][i] := TempItems[i];
  for i := aCol + 1 to Length(TempItems) do
    fItems[aRow][i] := TempItems[i - 1];
end;

procedure TMatrixStrings.InternalDeleteRow(aRow: integer);
var
  //TempItems: TMatrixStringItemArray;
  i: integer;
begin
{  TempItems := Copy(fItems[aRow]);
  for i := 0 to aCol - 1 do
    fItems[aRow][i] := TempItems[i]; }
  for i := aRow to Length(fItems) - 2 do
    fItems[i] := fItems[i + 1];
  SetLength(fItems, Length(fItems) - 1);
end;

procedure TMatrixStrings.InternalInsertRow(aRow: integer);
var
 // TempItems: TMatrixStringItemArray;
  t,i: integer;

begin
//  EnsureColRow(0, GetRowCount + 1);

//  t:=gettickcount;
  for i := GetRowCount - 1 downto aRow + 1 do
    fItems[i] := fItems[i - 1];

//  SetLength(
  if aRow < GetRowCount then
  SetLength(fItems[aRow], 0);
  //for i := 0 to Length(fItems[aRow]) - 1 do
    //fItems[aRow][i].fstring := '';
{  t:=gettickcount-t;
  logon:=true;
  Log([t]);
  logon:=false;}




(*  TempItems := Copy(fItems[aRow]);
  SetLength(fItems[aRow], Length(fItems[aRow]) + 1);
  for i := 0 to aCol - 1 do
    fItems[aRow][i] := TempItems[i];
  for i := aCol + 1 to Length(TempItems) do
    fItems[aRow][i] := TempItems[i - 1]; *)
end;

function TMatrixStrings.LastColCount: integer;
begin
  if GetRowCount > 0 then
    Result := Length(fItems[GetRowCount - 1])
  else
    Result := 0;
end;

function TMatrixStrings.GetRowCount: integer;
begin
  Result := Length(fItems);
end;

procedure TMatrixStrings.MoveColumn(A, B: integer);
var
  y, x, GridCols, MaxCol: integer;
  Adjust, DoCopy: boolean;
  S: string;
begin
  if fMatrix <> nil then GridCols := fMatrix.ColCount else GridCols := 0;
  MaxCol := Max(A, B);
  for y := 0 to Length(fItems) - 1 do
  begin
    EnsureColRow(MaxCol, y);
    S := fItems[y][A].fString;
    InternalDeleteCol(A, y); { langzaam! }
    InternalInsertCol(B, y); { langzaam! }
    fItems[y][B].fString := S;
  end;
end;

procedure TMatrixStrings.MoveRow(A, B: integer);
begin
  if B <> -1 then
  begin
//    InternalDeleteRow(A);
    InternalInsertRow(A);
  end;

      
//  else  
end;

procedure TMatrixStrings.SetObject(aCol, aRow: integer; const O: TObject);
begin
  EnsureColRow(aCol, aRow);
  fItems[aRow, aCol].fObject := O;
  if fMatrix <> nil then
    fMatrix.InvalidateCell(aCol, aRow);
end;

procedure TMatrixStrings.SetString(aCol, aRow: integer; const S: string);
begin
  EnsureColRow(aCol, aRow);
//  fData[aRow, aCol] := S;
  fItems[aRow, aCol].fString := S;
  UniqueString(fItems[aRow, aCol].fString);
  if fMatrix <> nil then
    fMatrix.InvalidateCell(aCol, aRow);
end;

procedure TMatrixStrings.InternalSetString(aCol, aRow: integer; const S: string);
begin
  fItems[aRow, aCol].fString := S;
end;

procedure TMatrixStrings.Clear;
begin
  SetLength(fItems, 0);
  if fMatrix <> nil then
    fMatrix.Invalidate;
end;

procedure TMatrixStrings.AssignCol(aCol: integer; const aStrings: TStrings);
var
  i: integer;
begin
  EnsureColRow(aCol, aStrings.Count);
  fMatrix.PaintLock;
  try
    for i := 0 to aStrings.Count - 1 do
      SetString(aCol, i, aStrings[i])
  finally
    fMatrix.PaintUnlock;
  end;
end;

procedure TMatrixStrings.InsertRow(R: integer);
begin
  InternalInsertRow(R);
end;

{-------------------------------------------------------------------------------
  Interne klasse
-------------------------------------------------------------------------------}
type
  TMatrixStringList = class(TStrings)
  private
    fCount: Integer;
    fMatrix: TListBoxMatrix;
    fOnChanging: TNotifyEvent;
    fOnChange: TNotifyEvent;
  protected
    procedure Changed; virtual;
    procedure Changing; virtual;
    function GetCount: Integer; override;
    procedure Insert(Index: Integer; const S: string); override;
    function Get(Index: Integer): string; override;
    procedure PutObject(Index: Integer; AObject: TObject); override;
    function GetObject(Index: Integer): TObject; override;
    procedure Clear; override;
    procedure SetUpdateState(Updating: Boolean); override;
  public
    constructor Create(aMatrix: TListBoxMatrix);
    procedure Assign(Source: TPersistent); override;
    procedure Delete(Index: Integer); override;

    property OnChange: TNotifyEvent read fOnChange write fOnChange;
    property OnChanging: TNotifyEvent read fOnChanging write fOnChanging;
  published
  end;

{ TMatrixStringList }

procedure TMatrixStringList.Assign(Source: TPersistent);
begin
  if Source is TStrings then
  begin
    fMatrix.fMatrixStrings.AssignCol(0, TStrings(Source));
    fCount := TStrings(Source).Count;
    fMatrix.RowCount := fCount;
  end
  else inherited;
end;

procedure TMatrixStringList.Changed;
begin
  if (UpdateCount = 0) and Assigned(FOnChange) then
    FOnChange(Self);
end;

procedure TMatrixStringList.Changing;
begin
  if (UpdateCount = 0) and Assigned(FOnChanging) then
    FOnChanging(Self);
end;

procedure TMatrixStringList.Clear;
begin
//  inherited Clear;
  fMatrix.fMatrixStrings.Clear;
  fCount := 0;
  fMatrix.RowCount := 0;
end;

constructor TMatrixStringList.Create(aMatrix: TListBoxMatrix);
begin
  inherited Create;
  fMatrix := aMatrix;
end;

procedure TMatrixStringList.Delete(Index: Integer);
begin
  raise Exception.Create('TMatrixStringList did not implement Delete');
end;

function TMatrixStringList.Get(Index: Integer): string;
begin
  Result := fMatrix.fMatrixStrings.GetString(0, Index);
end;

function TMatrixStringList.GetCount: Integer;
begin
  Result := fCount;
end;

function TMatrixStringList.GetObject(Index: Integer): TObject;
begin
  Result := fMatrix.fMatrixStrings.GetObject(0, Index);
end;

procedure TMatrixStringList.Insert(Index: Integer; const S: string);
begin
  fMatrix.fMatrixStrings.InsertRow(Index);
  fMatrix.fMatrixStrings.SetString(0, Index, S);
  Inc(fCount);
  fMatrix.RowCount := fCount;
end;

procedure TMatrixStringList.PutObject(Index: Integer; AObject: TObject);
begin
  fMatrix.fMatrixStrings.SetObject(0, Index, aObject);
end;

procedure TMatrixStringList.SetUpdateState(Updating: Boolean);
begin
  if Updating then Changing else Changed;
end;

{ TCustomStringMatrix }

procedure TCustomStringMatrix.Clear;
begin
  fMatrixStrings.Clear;
end;

constructor TCustomStringMatrix.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fMatrixStrings := TMatrixStrings.Create(Self);
end;

destructor TCustomStringMatrix.Destroy;
begin
  fMatrixStrings.Free;
  inherited;
end;

procedure TCustomStringMatrix.DoAfterColumnIndexChanged(Item: TMxColumn; OldIndex, NewIndex: Integer);
begin
  fMatrixStrings.MoveColumn(OldIndex, NewIndex);
//  ShowMessage('stringgrid kol index changed ' + i2s(aoldindex) + 'naar'+ i2s(anewindex));
end;

procedure TCustomStringMatrix.DoAfterDeletingColumn(aItem: TMxColumn);
begin
//  fMatrixStrings.MoveColumn(aItem.Index, -1);//InternalDeleteCol(aItem.Index);
  //showmessage('delete');
end;

procedure TCustomStringMatrix.DoGetCellData(aCol, aRow: Integer; out Data: TCellData);
begin
  inherited DoGetCellData(aCol, aRow, Data);
  Data.Text := Cells[aCol, aRow];
end;

procedure TCustomStringMatrix.DoSetCellData(aCol, aRow: Integer; Data: TCellData);
begin
  Cells[aCol, aRow] := Data.Text;
end;

function TCustomStringMatrix.GetCell(X, Y: Integer): string;
begin
  Result := fMatrixStrings.GetString(X, Y);
end;

function TCustomStringMatrix.GetObject(X, Y: Integer): TObject;
begin
  Result := fMatrixStrings.GetObject(X, Y);
end;

procedure TCustomStringMatrix.SetCell(X, Y: Integer; const Value: string);
begin
  fMatrixStrings.SetString(X, Y, Value);
end;

procedure TCustomStringMatrix.SetObject(X, Y: Integer; const Value: TObject);
begin
  fMatrixStrings.SetObject(X, Y, Value);
end;

procedure TCustomStringMatrix.SetOwnsObjects(const Value: Boolean);
begin
  fMatrixStrings.OwnsObjects := Value;
end;

{ TListBoxMatrix }

constructor TListBoxMatrix.Create(aOwner: TComponent);
begin
  inherited;
//  Color := clWindow;
  RowCount := 0;
  fItems := TMatrixStringList.Create(Self);
  Options.UseOptions := [];
  Options.PaintOptions := [];
  Options.AutoSizeOptions := [aoAutoAdjustLastColumn];
  ColCount := 1;
  DefaultRowHeight := 19;
  with CellStyle.Colors do
  begin
    BackColor := clWindow;
    BackFocused := clHighlight;
    BackSelected := clWindow;
  end;
end;

destructor TListBoxMatrix.Destroy;
begin
  fItems.Free;
  inherited;
end;

function TListBoxMatrix.GetItemIndex: Integer;
begin
  Result := Row;
end;

procedure TListBoxMatrix.SetItemIndex(const Value: Integer);
begin
  Row := Value;
end;

procedure TListBoxMatrix.SetItems(const Value: TStrings);
begin
  fItems.Assign(Value);
end;

{ TCalendarMatrix }

function TCalendarMatrix.CalcRowCount: Integer;
begin
  Result := DaysBetween(fMaximumDate, fMinimumDate) div ColCount;
end;

function TCalendarMatrix.ColRowToDate(aCol, aRow: Integer): TDate;
begin
  Result := fMinimumDate + aRow * 7 + aCol;
end;

function TCalendarMatrix.ColToDay(aCol: Integer): Integer;
begin
  Result := 1;
  case aCol of
    0: Result := 2;
    1: Result := 3;
    2: Result := 4;
    3: Result := 5;
    4: Result := 6;
    5: Result := 7;
    6: Result := 1;
  end;
//  Result := aCol + 2;


  //1=zondag
  //2=maandag
end;

constructor TCalendarMatrix.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
//  fForeGround := TBitmap.Create;
//  fForeGround.Width := 300;
//  fForeGround.Height := 300;
//  fTemp := TBitmap.Create;
//  Color := clGray;
  fOddStyle := TCellStyle.Create(Self, ofMatrix);
  fCalendarCellData := TCalendarCellData.Create;
  fMinimumDate := EncodeDate(1900, 1, 1);
  fMaximumDate := EncodeDate(2100, 12, 31);
//  fMinimumDate := EncodeDate(2005, 1, 1);  // zaterdag
//  fMaximumDate := EncodeDate(2005, 12, 31);

//  showmessage(shortdaynames[1]);

  while DayOfTheWeek(fMinimumDate) <> 1 do
    fMinimumDate := fMinimumDate-1;
//  fDateOffset := DayOfTheWeek(fMinimumDate);
//  showmessage(i2s(fdateoffset));  umx

//  fStartOfWeek := 0
  DefaultColWidth := 24;
  DefaultRowHeight := 19;
  Options.PaintOptions := Options.PaintOptions - [poHorzLines, poVertLines, poHeaderVertLines, poHeaderHorzLines];
  Options.AutoSizeOptions := [aoAutoWidth];//, aoAutoHeight];
  ColCount := 7;
  RowCount := CalcRowCount;
  Height := 200;
  Width := 220;

  CellStyle.HorzAlign := taCenter;//RightJustify;
  CellStyle.MultiLine := false;
  Options.NavigateOptions := [noNextLine, noPrevLine];


  Options.UseTopHeaders := True;
  Options.UseLeftHeaders := True;

  with LeftHeaders.Add do
  begin
    Style.Flat := True;
    Style.Colors.BackColor := clGray;
    Style.Colors.TextColor := clWhite;
    Size := DefaultColWidth;
    Style.HorzAlign := taRightJustify;
    LineWidth := 0;
  end;

  with Topheaders.Add do
  begin
//    Style.Flat := True;
//    Style.Colors.BackColor := clGray;
//    Style.Colors.TextColor := clWhite;
    Style.HorzAlign := taCenter;
//    LineWidth := 0;
    Spanned := True;
  end;
  with Topheaders.Add do
  begin
    Style.Flat := True;
    Style.Colors.BackColor := clGray;
    Style.Colors.TextColor := clWhite;
    Style.HorzAlign := taCenter;
    LineWidth := 0;
  end;
  fShowDateHeader := True;
  fShowDayHeaders := True;
  fShowWeeks := True;
  ShowWeeks := False;
  ActiveDate := Now;
  VisibleRowsRequested := 7;

//  Ctl3D := True;
//  Ctl3D := False;
  BorderStyle := bsSingle;

end;

procedure TCalendarMatrix.CustomAfterPaint(const PaintInfo: TPaintInfo);
var
  D: TDateTime;
  Day: Word;
  R: TRect;
var
  BlendFunction: TBlendFunction;
  Alpha: byte;

begin
  Exit;
  (*
//  inherited;
//  Log([topleft.x, topleft.y]);
  with PaintInfo, TargetCanvas do
  begin
    D := ColRowToDate(TopLeft.X, TopLeft.Y);
    Day := DayOfTheMonth(D);
    if Between(Day, 8, 14) then
    begin
//      brush.color := clred;
  //    fillrect(rect(20, 20, 30,30));
      with fForeGround.Canvas do
      begin
        R := Rect(0, 0, 100, 40);
        Font.Size := 24;
        Font.Color := clBlack;
        DrawTextEx(fForeGround.Canvas.Handle, PChar('AUG'), 3, R, DT_SINGLELINE or DT_LEFT, nil); //ugraphics
//        MergeBitmaps(fForeGround, piMatrixBitmap, piMatrixBitmap, 70);

        Alpha := 200;//255 * Percentage div 100;
//        fTemp.Assign(MxBitmap);
        BlendFunction.BlendOp := AC_SRC_OVER;
        BlendFunction.BlendFlags := 0;
        BlendFunction.SourceConstantAlpha := Alpha;
        BlendFunction.AlphaFormat := 0;
        Windows.AlphaBlend(fForeGround.Canvas.Handle, 0, 0,
                           300,
                           300,
                           MxBitmap.Canvas.Handle, 0, 0,
                           300,
                           300,
                           BlendFunction);
        CopyCanvas(MxBitmap.Canvas, R, fForeGround.Canvas, R)                   



      end;

    end;
//    Log([day]);
//    if Day in []
  end;
  *)
end;

function TCalendarMatrix.DateToColRow(aDate: TDate): TPoint;
var
  Span: Integer;
begin
  Span := DaysBetween(aDate, fMinimumDate);

  Result.Y := Span div 7;
  Result.X := Span mod 7;
//  Result := fMinimumDate + aRow * 7 + aCol;

end;

destructor TCalendarMatrix.Destroy;
begin
  fCalendarCellData.Free;
  fOddStyle.Free;
//  fForeGround.Free;
//  fTemp.Free;
  inherited;
end;

procedure TCalendarMatrix.DoCellEnter(aCol, aRow: Integer);
begin
  inherited;
  if fShowDateHeader then
    InvalidateTopHeaders;
end;

procedure TCalendarMatrix.DoGetCellData(aCol, aRow: Integer; out Data: TCellData);
var
  i: Integer;
begin
  Data := fCalendarCellData;
  fCalendarCellData.fDate := ColRowToDate(aCol, aRow);
  fCalendarCellData.Text := i2s(DayOfTheMonth(fCalendarCellData.fDate));
end;

function TCalendarMatrix.DoGetHeaderText(aHeader: TMatrixHeader; aIndex: Integer): string;
{-------------------------------------------------------------------------------
  Haal HeaderText op van date- of daty-header (topheaders 0 en 1)
-------------------------------------------------------------------------------}
var
  D: TDateTime;
begin
//  result := inhe
//  if aCol > 1 then
  Result := '';
  case aHeader.HeaderType of
    htTop:
      case aHeader.Index of
        0:
          begin
            D := ActiveDate;
            Result := LongDayNames[DayOfWeek(D)] + ' ' + DateToStr(D);
          end;
        1: Result := ShortDayNames[ColToDay(aIndex)];
      end;
    htLeft:
      begin
        Result := i2s(WeekOfTheYear(ColRowToDate(0, aIndex)));
      end;
  end;
end;

function TCalendarMatrix.GetActiveDate: TDate;
begin
  Result := ColRowToDate(Col, Row);
end;

function TCalendarMatrix.Navigation(aReason: TScrollReason; aCommand: TScrollCommand; XParam, YParam: Integer): TNavigationResult;
var
  M: TDateTime;
begin

  case aCommand of
    scPgDn:
      begin
        M := IncMonth(ActiveDate, 1);
        if M <= fMaximumDate then
        begin
          aCommand := scAbsolute;
          with DateToColRow(M) do
          begin
            XParam := X;
            YParam := Y;
          end;
        end;
      end;
    scPgUp:
      begin
        M := IncMonth(ActiveDate, -1);
        if M >= fMinimumDate then
        begin
          aCommand := scAbsolute;
          with DateToColRow(M) do
          begin
            XParam := X;
            YParam := Y;
          end;
        end;
      end;
    scVertStart:  // ctrl + pgup
      begin
        M := IncYear(ActiveDate, -1);
        if M >= fMinimumDate then
        begin
          aCommand := scAbsolute;
          with DateToColRow(M) do
          begin
            XParam := X;
            YParam := Y;
          end;
        end;
      end;
    scVertEnd:  // ctrl + pgdn
      begin
        M := IncYear(ActiveDate, 1);
        if M >= fMinimumDate then
        begin
          aCommand := scAbsolute;
          with DateToColRow(M) do
          begin
            XParam := X;
            YParam := Y;
          end;
        end;
      end;
  end;

  Result := inherited Navigation(aReason, aCommand, XParam, YParam);
end;

procedure TCalendarMatrix.PreparePaintCell(Params: TCellParams; Data: TCellData);
begin
  inherited;
  with Params do
    if [dsFocused] * pDrawState = [] then
    if Odd(MonthOf(ColRowToDate(pCol, pRow))) then
      pBackColor := RGB(240,240,240);// clYellow;
end;

procedure TCalendarMatrix.SetActiveDate(const Value: TDate);
begin
  ColRow := DateToColRow(Value);
end;

procedure TCalendarMatrix.SetShowDateHeader(const Value: Boolean);
begin
  if fShowDateHeader = Value then Exit;
  fShowDateHeader := Value;
  case Value of
    False : TopHeaders[0].Hidden := True;//Size := 0;
    True  : TopHeaders[0].Hidden := False;//Size := DefaultRowHeight;
  end;
end;

procedure TCalendarMatrix.SetShowDayHeaders(const Value: Boolean);
begin
  if fShowDayHeaders = Value then Exit;
  fShowDayHeaders := Value;
  case Value of
    False : TopHeaders[1].Hidden := True;//Size := 0;
    True  : TopHeaders[1].Hidden := False;//Size := DefaultRowHeight;
  end;
end;

procedure TCalendarMatrix.SetShowWeeks(const Value: Boolean);
begin
  if fShowWeeks = Value then Exit;
  fShowWeeks := Value;
  case Value of
    False : LeftHeaders[0].Hidden := True;//Size := 0;
    True  : LeftHeaders[0].Hidden := False;//Size := DefaultColWidth;
  end;
end;

{ TTreeMatrix }

constructor TTreeMatrix.Create(aOwner: TComponent);
begin
  inherited;
  Options.UseRows := True;
  Options.UseColumns := True;
  CustomPaintStages := [pcsContents];
end;

function TTreeMatrix.GetChildCount(aRow: Integer): Integer;
var
  N, i: Integer;
begin
  Result := 0;
  with fRows[aRow] do
  begin
    N := Indent + 1;
    for i := aRow + 1 to fRowCount - 1 do
      if fRows[i].Indent = N then
        Inc(Result)
      else Break;
  end;
end;

procedure TTreeMatrix.CustomPaintCell(Params: TCellParams; const PaintInfo: TPaintInfo; Data: TCellData);

    procedure InitPen;
    var
      ALogBrush: LOGBRUSH;
  //    AStyle: array of integer{TIntegerDynArray};
      aStyle: array[0..1] of Integer;
    begin
      ALogBrush.lbStyle := BS_SOLID;
      ALogBrush.lbColor := DIB_RGB_COLORS;
    //  SetLength(AStyle, 2);
      AStyle[0] := 0;
      AStyle[1] := 2;
      Params.pCellCanvas.Pen.Handle := ExtCreatePen(PS_GEOMETRIC or PS_USERSTYLE, 1,
        ALogBrush, 2, @AStyle);
    end;

begin
  with Params do
    if pStage = pcsContents then
  begin
    initpen;
    //pCellCanvas.Pen.Color := clRed;
    pCellCanvas.MoveTo(10, 10);
    pCellCanvas.LineTo(0, 10);
  end;
end;

procedure TTreeMatrix.SetChildCount(aRow: Integer; const Value: Integer);
var
  Counter, N, i: Integer;

begin
  Counter := 0;
  with fRows[aRow] do
  begin
    N := Indent;
    for i := aRow + 1 to fRowCount - 1 do
      with fRows[i] do
      begin
        if Indent >= N then
        begin
          Indent := N + 1;
          Inc(Counter);
        end
        else Break;
        if Counter >= Value then Break;
      end;
  end;
  Invalidate;
end;

function TTreeMatrix.DoCreateRows: TMxRows;
begin
  Result := TMxRows.Create(Self, TMxNode);
end;

function TTreeMatrix.AddNode(aParentNode: TMxNode): TMxNode;
var
  N: Integer;
begin
  if aParentNode = nil then
    Result := TMxNode(fRows.Add)
  else begin
    N := aParentNode.Index;
    Result := TMxNode(fRows.Insert(N + 1));
    Result.Indent := aParentNode.Indent + 1;
  end;
    
end;

end.


procedure TTreeMatrix.PaintCell(Params: TCellParams;
  const aPaintInfo: TPaintInfo; Data: TCellData);
begin
  inherited;
{  with Params do
  begin
    pCellCanvas.Pen.Color := clRed;
    pCellCanvas.MoveTo(1,1);
    pCellCanvas.LineTo(4,4);
  end; }
end;

