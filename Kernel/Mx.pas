unit Mx;

{$define UseMatrixInspector}

interface

uses
  Windows, Classes, Graphics, Controls, Sysutils, Messages, Forms, Math,
  ShellApi, StdActns, Variants,
  UMessages,
  Db,
  TypInfo, {DsgnIntf, }Dialogs,
  ULog, UMisc, UGraphics,
  MxMisc;

type
  EMatrixException = class(Exception);

  TColorRange = 0..15; // aantal mogelijke state kleuren
  THeaderRange = False..True;
  TMultiLineRange = False..True;
  TMatrixBorderStyle = bsNone..bsSizeable;

{Xinclude mx.inc} // enumerated plaatjes VERHUISD naar kernel_resource

{-------------------------------------------------------------------------------
  kleur indexen
-------------------------------------------------------------------------------}
const
  MI_TEXTCOLOR                 = 0;  // normaal
  MI_BACKCOLOR                 = 1;  // normaal
  MI_TEXTFOCUSED               = 2;  // huidige cell, matrix focused
  MI_BACKFOCUSED               = 3;  // huidige cell, matrix focused
  MI_TEXTCURRENT               = 4;  // huidige cell, matrix niet focused
  MI_BACKCURRENT               = 5;  // huidige cell, matrix niet focused
  MI_TEXTSELECTED              = 6;  // cell behoort bij selectie
  MI_BACKSELECTED              = 7;  // cell behoort bij selectie
  MI_TEXTHYPERLINK             = 8;  // mousover
  MI_TEXTMATCH                 = 9;  // tekstkleur voor matched string
  MI_BACKMATCH                 = 10; // bk kleur voor matched string
  MI_DIFFBACKCOLOR             = 11; // normaal achtergrond kleur voor om de zoveel rijen
  MI_TEXTMATCH2                = 12; // tekstkleur 2 voor matched string
  MI_BACKMATCH2                = 13; // bk kleur 2 voor matched string
  MI_RESERVED14                = 14;
  MI_RESERVED15                = 15;

{-------------------------------------------------------------------------------
  untyped defaults
-------------------------------------------------------------------------------}
  MAX_COLCOUNT                 = 1024;
  MAX_ROWCOUNT                 = 16 * MegaByte;
  MAX_HIGHLIGHT_STRINGS        = 5;

  DEF_COLCOUNT                 = 5;
  DEF_ROWCOUNT                 = 5;
  DEF_ROWHEIGHT                = 21;
  DEF_COLWIDTH                 = 64;
  DEF_LINEWIDTH                = 1;
  DEF_LINECOLOR                = clSilver;
  DEF_HEADERLINECOLOR          = clBlack;

  AXISINFOFLAG_VERTICAL        = Bit0; // TAxisInfo.Flags: bit staat aan als vertikaal

{-------------------------------------------------------------------------------
  record types en enumerated types
-------------------------------------------------------------------------------}
type
  THighLightRange = 0..4;

  TVertAlignment  = (
    vaTop,
    vaBottom,
    vaCenter
  );

  TMatrixState = (
    mxPainting,               // matrix is bezig met tekenen
    mxFocusMoving,            // er wordt een cell gefocused
    mxDataNavigation,         // dataset move door besturing
    mxHandlingScrollMessage,  // er wordt een windows scrollmessage afgehandeld
    mxEditing,                // matrix is in editstate
    mxEditChar,               // edit wordt opgestart met karakter
    mxDataEvent,              // er wordt een event vanuit datalink afgehandeld
    mxChanging,               // change procedure bezig
    mxDragSizing              // er wordt gesized met de muis
  );
  TMatrixStates = set of TMatrixState;

  TAxisRange = record // intern
    First: Integer;
    Last: Integer;
  end;

  TAxisInfo = record // intern
    Flags            : Cardinal;        // zie AXISINFOFLAG_VERTICAL
    Offset           : Integer;         // offset coordinaat (Left of Top) waarvoor de info is berekend
    Indices          : TCachedIntegers; // indexen van de zichtbare rijen of kolommen
    VisibleCount     : Integer;         // aantal zichtbare rijen of kolommen
    HiddenCount      : Integer;         // aantal verborgen rijen of kolommen
    TotalCount       : Integer;         // aantal verborgen rijen of kolommen
    FullVisibleCount : Integer;         // aantal volledig zichtbare rijen of kolommen
    FirstVisible     : Integer;         // eerste zichtbare rij of kolom
    LastVisible      : Integer;         // laatste zichtbare rij of kolom
    LastFullVisible  : Integer;         // laatste volledig zichtbare rij of kolom
    GridExtent       : Integer;         // laatste pixel van cellen
  end;

  TPageInfo = record // intern
    Horz  : TAxisInfo;
    Vert  : TAxisInfo;
  end;

  TPageCache = record // intern
    PageInfo: TPageInfo;
    Valid: Boolean;
  end;

  TPaintInfo = record // intern
    TargetCanvas: TCanvas;
    PageInfo: TPageInfo;
    Target: TRect;
  end;

  TScrollReason = (
    srNone,
    srCommand,     // clamp afhankelijk van?
    srKeyDown,     // clamp
    srMouseClick,  // clamp
    srMouseWheel,  // clamp afhankelijk van scrolloptions
    srScrollBar,   // clamp afhankelijk van scrolloptions
    srDataEvent    // clamp
  );

  TScrollCommand = (
    scNone,
    scDelta,         // XParam en YParam zijn worden beschouwd als Delta t.o.v. huidige cell
    scAbsolute,      // XParam en YParam zijn absolute coordinaten
    scRight,         // 1 naar rechts
    scLeft,          // 1 naar links
    scDown,          // 1 naar beneden
    scUp,            // 1 naar boven
    scPgDown,        // pagina naar beneden
    scPgUp,          // pagina naar boven
    scHorzStart,     // eerste kolom
    scHorzEnd,       // laatste kolom
    scVertStart,     // eerste rij
    scVertEnd,       // laatste rij
    scBegin,         // eerste cell
    scEnd            // laatste cell
  );

  TScrollMode = (
    smDisable,
    smAbsolute,
    smDelta
  );

  TNavigationRec = record
    nReason  : TScrollReason;
    nCommand : TScrollCommand;
    nXParam  : Integer;
    nYParam  : Integer;
    nValid   : Boolean;
  end;

  TCellPosition = (
    cpAuto,
    cpLeft,
    cpTop,
    cpRight,
    cpBottom
  );

  TVerticalFocusPosition = (
    vfpAuto,
    vfpTop,
    vfpCenter,
    vfpBottom
  );

  THorizontalFocusPosition = (
    hfpAuto,
    hfpLeft,
    hfpCenter,
    hfpRight
  );

  TMouseDragState = (
    mdNone,
    mdColSizingPossible,  // bepaald door MouseMove als muis op lijn
    mdColSizing,          // colsizing is in actie
    mdRowSizingPossible,  // bepaald door MouseMove als muis op lijn
    mdRowSizing,          // rowsizing is in actie
    mdCellSizingPossible, // bepaald door MouseMove als muis op kruising
    mdCellSizing,         // cellsizing is in actie
    mdRectSelectingStart, // bepaald door MouseDown
    mdRectSelecting       // rectselecting in actie
  );

  TClickInfos = (
    ciDownCell,  // er is op een cell geklikt, TMouseInfo.miDownCell is gevuld (MouseDown)
    ciMoveCell
  );
  TClickInfo = set of TClickInfos;

  TMouseInfo = packed record
    miValid         : Boolean;
    miClickInfo     : TClickInfo;
    miState         : TMouseDragState;   // huidige dragstate
    miDown          : TPoint;            // MouseDown coordinaat
    miMove          : TPoint;            // MouseMove coordinaat
    miUp            : TPoint;            // MouseUp coordinaat
    miDownCell      : TPoint;            // aangeklikte cell in mousedown
    miMoveOverCell  : TPoint;            // muis moveover cell
    miResizeCell    : TPoint;            // valide als sizing mogelijk of sizing (MouseMove)
    miResizeRect    : TRect;             // rechthoek voor resizing
    miLastMoveTime  : Cardinal;
    miSelectionRect : TRect;             // PaintSelectionRect laatst getekende xor-rect
    miStartColWidth : Integer;
    miStartRowHeight: Integer;
  end;

  TLineHits = (
    lhHorz,  // horizontale lijn geraakt door muis
    lhVert   // verticale lijn geraakt door muis
  );
  TLineHit = set of TLineHits;

  THeaderModes = (
    hmLeft,  // cell is een leftheader
    hmTop    // cel is een topheader
  );
  THeaderMode = set of THeaderModes;

  TCellDrawStates = (
    dsMatrixFocused,   // matrix is gefocused
    dsFocused,         // actieve cell
    dsTopHeader,       // cell is top header
    dsLeftHeader,      // cell is left header
    dsHyperLink,       // cell is hyperlink
    dsHighlightRow,    // rowselect
    dsHighlightCol,    // colselect
    dsHighlightHeader, // headerhighlight
    dsHeaderDownClick, // aangeklikt
    dsDiffRow,
    dsHighlightString,
    dsSortImage
  );
  TCellDrawState = set of TCellDrawStates;

  TInternalState = (
    icTraversingCells,        // er wordt door de cellen heengelopen
    icTraversingCols,         // er wordt door de kolommen heengelopen
    icTraversingRows,         // er wordt door de rijen heengelopen
    icTraversingVisibleCells, // er wordt door de zichtbare cellen heengelopen
    icTraversingVisibleCols,  // er wordt door de zichtbare kolommen heengelopen
    icTraversingVisibleRows,  // er wordt door de zichtbare rijen heengelopen
    icResizeTimerActive,
    icSearchTimerActive,
    icSettingColCount,       // semafoor
    icChangingColumns,
    icKillingFocus
  );
  TInternalStates = set of TInternalState;

  TMatrixChange = (
    mcAxisMode,
    mcColCount,
    mcColLineWidth,        // info param = kolom index
    mcColWidth,            // info param = kolom index
    mcDefaultColWidth,
    mcDefaultLineWidth,
    mcDefaultRowHeight,
    mcFixedCols,
    mcFixedRows,
    mcLeftScrollLockCount,
    mcRowCount,
    mcRowHeight,           // info param = rij index
    mcRowLineWidth,        // info param = rij index
    mcTopScrollLockCount
  );

  TSelectMode = (
    smCellSelect,
    smRowSelect,
    smColSelect,
    smColRowSelect
  );

{-------------------------------------------------------------------------------
  enumerated matrixoptions
-------------------------------------------------------------------------------}
  TAxisOption = (
    aoUseColumns,
    aoUseRows,
    aoUseCells
  );
  TAxisOptions = set of TAxisOption;

  TSizingOption = (
    soColSizing,       // kolommen mogen geresized worden
    soRowSizing,       // rijen mogen geresized worden
    soCellSizing,      // cellen mogen geresized worden
    soColMoving,       // kolommen mogen verplaatst worden
    soRowMoving,       // rijen mogen verplaatst worden
    soLiveSizing,      // tijdens het slepen wordt de resize direct op het scherm uitgevoerd
    soHeaderSizingOnly // resizen alleen mogelijk dmv headers
  );
  TSizingOptions = set of TSizingOption;

  TPaintOption = (
    poColLines,
    poRowLines,
    poIgnoreColLines,
    poIgnoreRowLines,
    poHidePartialCells,
    poTransparentBackGround,   
    poDrawFocusRect,
    poHeaderHighlightStrings,
    poIncrementalSearchHighlight,
    poVirtualColumn           // tekent een nepkolom
  );
  TPaintOptions = set of TPaintOption;

    //tcomputeoptions
    //aoComputeRowHeights,
    //aoComputeColWidhts

  TAutoOption = (
    aoAutoWidth,
    aoAutoHeight,
    aoAutoSizeColumns
  );
  TAutoOptions = set of TAutoOption;

  TFunctionalOption = (
    foAcceptFiles
  );
  TFunctionalOptions = set of TFunctionalOption;

  TScrollOption = (
    soIgnorePartialCells,
    soIntelligentPageScroll, // bereken pagina pgup + pgdown
    soAlwaysCenterRow,
    soMouseWheelClamp,
    soScrollBarClamp,
    soKeyAcceleration,
    soAlwaysUpdateScreen
  );
  TScrollOptions = set of TScrollOption;

  TClickOption = (
    coFocusOnLeftBtn,
    coFocusOnMiddleBtn,
    coFocusOnRightBtn
  );
  TClickOptions = set of TClickOption;

  TEditOption = (
    eoCanEdit,             // edit mogelijk
    eoCanDeleteRow,
    eoCanAppendRow,
    eoStartOnChar,         // begin edit als een karakter ingetypt wordt
    // vervangen door: eoAppendOnChar, eoInsertOnChar, eoOverwriteOnChar
    eoAlwaysShow,          // edit altijd zichtbaar
    eoAutoSelectAll,       // edit selecteert hele woord
    eoAutoAccept,          // edit inhoud automatisch accepteren bij exit
    eoCopyPrevious,        // kopieer vorige cell bij aanhalingstekens als niet editmode
    eoDeleteCellContents,   // delete cell op key <delete> als niet in editmode
    eoDownAfterEdit
  );
  TEditOptions = set of TEditOption;

  TSearchOption = (
    soIncrementalSearch,
    soWaitForUser
  );
  TSearchOptions = set of TSearchOption;

{  TDBOption = ( mxinplace
    doCancelOnExit,
    doPostOnExit,
  ); }

{-------------------------------------------------------------------------------
  enumareted intern
-------------------------------------------------------------------------------}
  TInternalPaintOption = (
    ipPaintBackGround,
    ipPaintLines,
    ipPaintCells,
    ipFocusRect
  );
  TInternalPaintOptions = set of TInternalPaintOption;

  { diversen }

  TDisplayOption = (
    doUseHighlightStrings // cellstyle gebruikt highlightstrings
  );
  TDisplayOptions = set of TDisplayOption;

  TSortDirection = (
    sdAscending,
    sdDescending
  );

{-------------------------------------------------------------------------------
  typed defaults
-------------------------------------------------------------------------------}
const
  DEF_AXISOPTIONS          = [aoUseColumns];
  DEF_PAINTOPTIONS         = [poColLines, poRowLines];
  DEF_SCROLLOPTIONS        = [soMouseWheelClamp, soScrollBarClamp];
  DEF_SIZINGOPTIONS        = [soColSizing, soColMoving, soRowSizing, soRowMoving, soCellSizing, soLiveSizing];
  DEF_EDITOPTIONS          = [eoCanEdit, eoCanDeleteRow, eoCanAppendRow, eoStartOnChar, eoAutoAccept];
  ALL_INTERNALPAINTOPTIONS = [ipPaintBackGround, ipPaintLines, ipPaintCells, ipFocusRect];

  DEF_FLAT             : array [THeaderRange] of Boolean = (True, False);
  DEF_HORZALIGN        : array[THeaderRange] of TAlignment = (taLeftJustify, taLeftJustify);
  DEF_HYPERLINK        : array[THeaderRange] of Boolean = (False, False);
  DEF_VERTALIGN        : array[THeaderRange] of TVertAlignment = (vaCenter, vaCenter);
  DEF_MULTILINE        : array[THeaderRange] of Boolean = (False, False);
  DEF_BTNLOOK          : array[THeaderRange] of Boolean = (False, True);

  SID_MatrixEditLink = '{03A796E7-5849-44CF-B123-E9F742F7E4EC}';

type
  TCustomMatrix = class;
  TCustomDBMatrix = class;

{-------------------------------------------------------------------------------
  interfaces
-------------------------------------------------------------------------------}

  {$ifdef UseMatrixInspector}
  IMatrixInspector = interface
    procedure ConnectMatrix(aMatrix: TCustomMatrix);
    procedure DisconnectMatrix(aMatrix: TCustomMatrix);
    procedure PageInfoChanged(const aPageInfo: TPageInfo);
    procedure ColRowChanged(aCol, aRow: Integer);
  end;
  {$endif}

  IMatrixEditLink = interface
    [SID_MatrixEditLink]
    function BeginEdit: Boolean;
    function CancelEdit: Boolean;
    function EndEdit: Boolean;
    function PrepareEdit(Matrix: TCustomMatrix; aCol, aRow: Integer): Boolean;
    function GetBounds: TRect;
    procedure SetBounds(R: TRect);
    procedure ProcessMessage(var Message: TMessage);
  end;

  TOwnerFlag  = (ofNone, ofMatrix, ofColumn); // gebruikt om defaults te achterhalen
  TAssignedColorProps = TByteSet; // gebruikt om te bekijken welke kleuren veranderd zijn
  TStateColorArray = array[TColorRange] of TColor;
//  PStateColorArray = ^TStateColorArray;

  TStateColors = class(TOwnedPersistent)
  private
    fColorArray          : TStateColorArray;
    fIsHeader            : Boolean;
    fOwnerFlag           : TOwnerFlag;
    fAssignedColorProps  : TAssignedColorProps;
    fOnChange            : TNotifyEvent;
    function GetColorElement(const Index: Integer): TColor;
    procedure SetColorElement(const Index: Integer; const Value: TColor);
    function GetDefaultColorElement(const Index: Integer): TColor;
    function IsColorStored(const Index: Integer): Boolean;
    procedure Changed;
  protected
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  public
    constructor Create(aOwner: TPersistent; aHeader: Boolean; aOwnerFlag: TOwnerFlag);
        { aOwner moet TCellStyle zijn. aOwnerFlag: zie TCellStyle. deze wordt doorgegeven }
    property DefaultColorElement[const Index: Integer]: TColor read GetDefaultColorElement;
    procedure Assign(Source: TPersistent); override;
  published
    property TextColor: TColor index MI_TEXTCOLOR read GetColorElement write SetColorElement stored IsColorStored;
    property BackColor: TColor index MI_BACKCOLOR read GetColorElement  write SetColorElement stored IsColorStored;
    property TextFocused: TColor index MI_TEXTFOCUSED read GetColorElement write SetColorElement stored IsColorStored;
    property BackFocused: TColor index MI_BACKFOCUSED read GetColorElement write SetColorElement stored IsColorStored;
    property TextCurrent: TColor index MI_TEXTCURRENT read GetColorElement write SetColorElement stored IsColorStored;
    property BackCurrent: TColor index MI_BACKCURRENT read GetColorElement write SetColorElement stored IsColorStored;
    property TextSelected: TColor index MI_TEXTSELECTED read GetColorElement write SetColorElement stored IsColorStored;
    property BackSelected: TColor index MI_BACKSELECTED read GetColorElement write SetColorElement stored IsColorStored;
    property TextHyperLink: TColor index MI_TEXTHYPERLINK read GetColorElement write SetColorElement stored IsColorStored;
    property TextMatch: TColor index MI_TEXTMATCH read GetColorElement write SetColorElement stored IsColorStored;
    property BackMatch: TColor index MI_BACKMATCH read GetColorElement write SetColorElement stored IsColorStored;
    property TextMatch2: TColor index MI_TEXTMATCH2 read GetColorElement write SetColorElement stored IsColorStored;
    property BackMatch2: TColor index MI_BACKMATCH2 read GetColorElement write SetColorElement stored IsColorStored;
    property DiffBack: TColor index MI_DIFFBACKCOLOR read GetColorElement write SetColorElement stored IsColorStored;
  end;

  TMargin = packed record Left, Top, Right, Bottom: Byte; end; // text margin

  {-------------------------------------------------------------------------------
    tassignedcellprops wordt gebruikt om column/row/cell-properties "transparant"
    te laten werken tov de matrix
  -------------------------------------------------------------------------------}
  TAssignedCellProp = (
    acp_Font,
    acp_Edge,
    acp_Clip,
    acp_Flat,
    acp_HorzAlign,
    acp_HyperLink,
    acp_VertAlign,
    acp_MultiLine,
    acp_BtnLook
    );
  TAssignedCellProps = set of TAssignedCellProp;

  TCellCache = record
  { cached statecolors }
    Colors            : TStateColorArray;
  { cached font }
    Font              : TFont;
  { cached cellstyle }
    Flat              : Boolean;
    HorzAlign         : TAlignment;
    HyperLink         : Boolean;
    VertAlign         : TVertAlignment;
    MultiLine         : Boolean;
    TextMargin        : TMargin;
  end;

  TCellStyle = class(TOwnedPersistent)
  private
  { cached properties }
    fColors            : TStateColors;
    fFont              : TFont;
    fFlat              : Boolean;
    fHorzAlign         : TAlignment;
    fVertAlign         : TVertAlignment;
    fMultiLine         : Boolean;
//    fTextMargin        : TMargin;
    fHyperLink         : Boolean;
  { overig }
    fIsHeader          : Boolean;
    fCached            : Boolean;
    fCache             : TCellCache; // record voor snelle toegang properties
    fOwnerFlag         : TOwnerFlag; // gebruikt om defaults te achterhalen
    fAssignedCellProps : TAssignedCellProps; // gebruikt om te kijken welke cellstyle properties expliciet gezet zijn
    fOnChange          : TNotifyEvent;
    fUpdateFlag        : Integer;
  { intern }
    procedure ColorChanged(Sender: TObject);
//    procedure EdgeChanged(Sender: TObject);
    procedure FontChanged(Sender: TObject);
    procedure Changed;
  { getters }
    //function GetEdge: TEdge;
    function GetFlat: Boolean;
    function GetFont: TFont;
    function GetHorzAlign: TAlignment;
    function GetHyperLink: Boolean;
    function GetVertAlign: TVertAlignment;
    function GetMultiLine: Boolean;
  { setters }
    procedure SetColors(const Value: TStateColors);
    //procedure SetEdge(const Value: TEdge);
    procedure SetFlat(const Value: Boolean);
    procedure SetFont(const Value: TFont);
    procedure SetHorzAlign(const Value: TAlignment);
    procedure SetHyperLink(const Value: Boolean);
    procedure SetVertAlign(const Value: TVertAlignment);
    procedure SetMultiLine(const Value: Boolean);
  { default getters }
    //function GetDefaultEdge: TEdge;
    function GetDefaultFlat: Boolean;
    function GetDefaultFont: TFont;
    function GetDefaultHorzAlign: TAlignment;
    function GetDefaultHyperLink: Boolean;
    function GetDefaultVertAlign: TVertAlignment;
    function GetDefaultMultiLine: Boolean;
  { interne storage functies }
    function IsFlatStored: Boolean;
    function IsFontStored: Boolean;
    function IsHorzAlignStored: Boolean;
    function IsHyperLinkStored: Boolean;
    function IsVertAlignStored: Boolean;
    function IsMultiLineStored: Boolean;
  protected
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
    procedure SetCached(DoCache: Boolean);
  public
    constructor Create(aOwner: TPersistent; aHeader: Boolean);
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure BeginUpdate;
    procedure EndUpdate;
    property IsHeader: Boolean read fIsHeader;
  { default properties }
    property DefaultFlat: Boolean read GetDefaultFlat;
    property DefaultFont: TFont read GetDefaultFont;
    property DefaultHorzAlign: TAlignment read GetDefaultHorzAlign;
    property DefaultHyperLink: Boolean read GetDefaultHyperLink;
    property DefaultVertAlign: TVertAlignment read GetDefaultVertAlign;
    property DefaultMultiLine: Boolean read GetDefaultMultiLine;
  { cached properties }
    property Cached: Boolean read fCached write SetCached;
    property CachedColors: TStateColorArray read fCache.Colors;
    property CachedFlat: Boolean read fCache.Flat;
    property CachedFont: TFont read fCache.Font;
    property CachedHorzAlign: TAlignment read fCache.HorzAlign;
    property CachedHyperLink: Boolean read fCache.HyperLink;
    property CachedVertAlign: TVertAlignment read fCache.VertAlign;
    property CachedMultiLine: Boolean read fCache.MultiLine;
    property CachedTextMargin: TMargin read fCache.TextMargin;
  published
  { published }
    property Colors: TStateColors read fColors write SetColors;
    property Flat: Boolean read GetFlat write SetFlat stored IsFlatStored;
    property Font: TFont read GetFont write SetFont stored IsFontStored;
    property HorzAlign: TAlignment read GetHorzAlign write SetHorzAlign stored IsHorzAlignStored;
    property HyperLink: Boolean read GetHyperLink write SetHyperLink stored IsHyperLinkStored;
    property VertAlign: TVertAlignment read GetVertAlign write SetVertAlign stored IsVertAlignStored;
    property MultiLine: Boolean read GetMultiLine write SetMultiLine stored IsMultiLineStored;
  end;

  TColumnType = (
    ctDefault,
    ctData,
    ctCheckBox
  );

  TMatrixAxisItem = class(TCollectionItem)
  private
    fInitFlag      : Boolean;
    fAutoGenerated : Boolean;
    fStyle         : TCellStyle;
    fHeaderStyle   : TCellStyle;
    fDataField     : string;
    fField         : TField;
    fPrompt        : string;
    fIndex         : Integer; // opslag index voor directe toegang
    fHeaderLook    : Boolean;
//    fOnChange      : TNotifyEvent;
    fReadOnly      : Boolean;
    fTag: Integer;
    // displayopties
    procedure SetDataField(const Value: string); virtual;
    procedure SetField(const Value: TField); virtual;
    function GetMatrix: TCustomMatrix;
    procedure StyleChanged(Sender: TObject);
    procedure SetStyle(const Value: TCellStyle);
    procedure SetPrompt(const Value: string);
    procedure SetHeaderStyle(const Value: TCellStyle);
  protected
    procedure PropertyChanged; virtual;
    procedure SetIndex(Value: Integer); override;
    property HeaderStyle: TCellStyle read fHeaderStyle write SetHeaderStyle;
    property Style: TCellStyle read fStyle write SetStyle;
    property Matrix: TCustomMatrix read GetMatrix; // do not publish!
    property DataField: string read fDataField write SetDataField;
    property Field: TField read fField write SetField; // do not publish!
    property Prompt: string read fPrompt write SetPrompt;
    property AutoGenerated: Boolean read fAutoGenerated write fAutoGenerated; // do not publish!
    property ReadOnly: Boolean read fReadOnly write fReadOnly;
  public
    constructor Create(aCollection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
  published
    property Tag: Integer read fTag write fTag;
  end;

  TMatrixColumn = class(TMatrixAxisItem)
  private
    function GetWidth: Integer;
    procedure SetWidth(const Value: Integer);
    function GetVisible: Boolean;
  protected
    procedure PropertyChanged; override;
  public
  published
    property DataField;
    property Field;
    property Prompt;
    property HeaderStyle;
    property ReadOnly;
    property Style;
    property Visible: Boolean read GetVisible;
    property Width: Integer read GetWidth write SetWidth;
  end;

(*  TMatrixRow = class(TCollectionItem)
  private
    fRowNumber: Integer; // absolute rij index
    fStyle: TCellStyle; // stijl
    fHeaderStyle: TCellStyle; // header stijl
  protected
  public
  published
  end; *)

(*  TMatrixCell = class(TCollectionItem)
  private
    fAddCols: Integer; // aantal samengevoegde cellen rechts
    fAddRows: Integer; // aantal samengevoegde cellen omlaag
    fColumnNumber: Integer; // absolute kolom index
    fRowNumber: Integer; // absolute rij index
    fStyle: TCellStyle;
  protected
  public
  published
  end; *)

  TMatrixAxisCollection = class(TOwnedCollection)
  private
    fMatrix: TCustomMatrix;
    fMatrixLock: Integer;
    function GetCount: Integer;
    procedure SetCount(const Value: Integer);
    function GetAutoGenerated: Boolean;
    procedure SetAutoGenerated(const Value: Boolean);
    function InternalAdd: TMatrixAxisItem;
//    fDisableUpdateCount: Integer;
  protected
    procedure ClearStyleCache;
    property Matrix: TCustomMatrix read fMatrix;
    procedure UpdateIndices;
//    procedure BeginUpdate;
//    procedure EndUpdate;
  public
    constructor Create(aMatrix: TCustomMatrix; aItemClass: TCollectionItemClass);
    procedure LoadFromStream(S: TStream);
    procedure LoadFromFile(const aFileName: string);
    procedure SaveToStream(S: TStream);
    procedure SaveToFile(const aFileName: string);
//    property Items
    procedure LockMatrix;
    procedure UnlockMatrix;
    function MatrixLocked: Boolean;
    property Count: Integer read GetCount write SetCount;
    property AutoGenerated: Boolean read GetAutoGenerated write SetAutoGenerated;

//    function FindField()

  published
  end;

  TMatrixColumns = class(TMatrixAxisCollection)
  private
    function GetColumn(N: Integer): TMatrixColumn;
    function InternalAdd: TMatrixColumn;
  protected
  public
    constructor Create(aMatrix: TCustomMatrix);
    function Add: TMatrixColumn;
    function Insert(Index: Integer): TMatrixColumn;
    procedure BeginUpdate; override;
    procedure EndUpdate; override;
    procedure Update(Item: TCollectionItem); override;
    property Items[N: Integer]: TMatrixColumn read GetColumn; default;

    function FindColumnByFieldName(const aFieldName: string): TMatrixColumn;
  published
  end;

(*  TMatrixRows = class(TOwnedCollection)
  private
    fMatrix: TCustomMatrix;
  protected
  public
    constructor Create(aMatrix: TCustomMatrix);
  published
  end; *)

(*  TMatrixCells = class(TOwnedCollection)
  private
    fMatrix: TCustomMatrix;
  protected
  public
    constructor Create(aMatrix: TCustomMatrix);
  published
  end; *)

//  TCellArray = array of TMatrixCell;
//  TMatrix

(*
{ spreadsheet types }
  TMatrixDataType = (
    mdtInteger
  );

  TDataItem = record
    fValue: Variant;
    fDataType: TMatrixDataType;
    fCell: TMatrixCell; // backlink naar TCell
  end;

  TDataItemArray = array of TDataItem;

  { TMatrixData: dubbele array voor data }
  TMatrixData = class
  private
    fMatrix: TCustomMatrix;
    fData: array of TDataItemArray;
    procedure EnsureColRow(aCol, aRow: integer);
  protected
  public
  published
  end; *)

  TCacheOption = (
    coCacheColInfo,
    coCacheRowInfo
  );
  TCacheOptions = set of TCacheOption;

  TOptionsChange = (
    ocAxisOptions,
    ocCacheOptions,
    ocSizingOptions,
    ocPaintOptions,
    ocEditOptions,
    ocFunctionalOptions,
    ocAutoOptions
  );
  TOptionsChangeEvent = procedure(Sender: TObject; aChange: TOptionsChange) of object;

  TMatrixOptions = class(TOwnedPersistent)
  private
    fAxisOptions       : TAxisOptions;
    fCacheOptions      : TCacheOptions;
    fSizingOptions     : TSizingOptions;
    fPaintOptions      : TPaintOptions;
    fEditOptions       : TEditOptions;
    fFunctionalOptions : TFunctionalOptions;
    fScrollOptions     : TScrollOptions;
    fAutoOptions       : TAutoOptions;
    fOnChange          : TOptionsChangeEvent;
    procedure Changed(aChange: TOptionsChange);
    procedure SetAxisOptions(const Value: TAxisOptions);
    procedure SetCacheOptions(const Value: TCacheOptions);
    procedure SetFunctionalOptions(const Value: TFunctionalOptions);
    procedure SetPaintOptions(const Value: TPaintOptions);
    procedure SetScrollOptions(const Value: TScrollOptions);
    procedure SetSizingOptions(const Value: TSizingOptions);
    procedure SetEditOptions(const Value: TEditOptions);
    procedure SetAutoOptions(const Value: TAutoOptions);
  { property easy access }
    // sizing
    function GetColSizing: Boolean;
    procedure SetColSizing(const Value: Boolean);
    function GetHeaderSizingOnly: Boolean;
    procedure SetHeaderSizingOnly(const Value: Boolean);
    function GetRowSizing: Boolean;
    procedure SetRowSizing(const Value: Boolean);
    // paint
    function GetPaintColLines: Boolean;
    procedure SetPaintColLines(const Value: Boolean);
    function GetPaintRowLines: Boolean;
    procedure SetPaintRowLines(const Value: Boolean);
    // edit
    function GetCanEdit: Boolean;
    procedure SetCanEdit(const Value: Boolean);
    function GetDownAfterEdit: Boolean;
    procedure SetDownAfterEdit(const Value: Boolean);
    // auto
    function GetAutoSizeColumns: Boolean;
    procedure SetAutoSizeColumns(const Value: Boolean);
    // functional
    //scroll
    function GetAlwaysUpdateScreen: Boolean;
    procedure SetAlwaysUpdateScreen(const Value: Boolean);
  protected
  public
    constructor Create(aOwner: TPersistent);
    destructor Destroy; override;
    function GetOwner: TCustomMatrix; reintroduce;
    property Owner: TCustomMatrix read GetOwner;
  { public easy properties }
    // auto
    property AutoSizeColumns: Boolean read GetAutoSizeColumns write SetAutoSizeColumns;
    // edit
//    property AlwaysShowEditor: Boolean read
    property CanEdit: Boolean read GetCanEdit write SetCanEdit;
    property DownAfterEdit: Boolean read GetDownAfterEdit write SetDownAfterEdit;
    // sizing
    property ColSizing: Boolean read GetColSizing write SetColSizing;
    property RowSizing: Boolean read GetRowSizing write SetRowSizing;
    property HeaderSizingOnly: Boolean read GetHeaderSizingOnly write SetHeaderSizingOnly;
    // paint
    property PaintColLines: Boolean read GetPaintColLines write SetPaintColLines;
    property PaintRowLines: Boolean read GetPaintRowLines write SetPaintRowLines;
    //scroll
    property AlwaysUpdateScreen: Boolean read GetAlwaysUpdateScreen write SetAlwaysUpdateScreen;
  published
    property AxisOptions: TAxisOptions read fAxisOptions write SetAxisOptions default DEF_AXISOPTIONS;
    property CacheOptions: TCacheOptions read fCacheOptions write SetCacheOptions;
    property SizingOptions: TSizingOptions read fSizingOptions write SetSizingOptions default DEF_SIZINGOPTIONS;
    property PaintOptions: TPaintOptions read fPaintOptions write SetPaintOptions default DEF_PAINTOPTIONS;
    property EditOptions: TEditOptions read fEditOptions write SetEditOptions default DEF_EDITOPTIONS;
    property FunctionalOptions: TFunctionalOptions read fFunctionalOptions write SetFunctionalOptions;
    property ScrollOptions: TScrollOptions read fScrollOptions write SetScrollOptions default DEF_SCROLLOPTIONS;
    property AutoOptions: TAutoOptions read fAutoOptions write SetAutoOptions;
  end;

  TMatrixDataMode = (
    mdmCustom,
    mdmSpreadSheet,
    mdmDataSet
  );

  TCellParams = record
    pDrawState    : TCellDrawState;  // readonly!
    pTextColor    : TColor;
    pBackColor    : TColor;
    pCellStyle    : TCellStyle;      // readonly!
    pCellRect     : TRect;           // readonly!
    pDrawRect     : TRect;           // readonly!
    pDrawTextRect : TRect;
    pDrawImageRect: TRect;           // readonly!
//    pFont         : TFont;
    pTextParams   : TDrawTextParams; // readonly!
    pText         : string;
    pField        : TField;          // readonly!
    pFieldValue   : Variant;         // readonly!
    pColumn       : TMatrixColumn;   // readonly!
//    pRow          : TMatrixRow;      // readonly!
    pPaintRecord  : Integer;         // readonly!!!
    pActiveRecord : Integer;         // readonly!!!
    pTag          : Integer;
    pCanvas       : TCanvas;         // readonly!!
    pPainted      : Boolean;
  end;

  TCellFocusEvent = procedure(Sender: TObject; aCol, aRow: Integer) of object;
  TGetCellTextEvent = procedure (Sender: TObject; aCol, aRow: Integer; var Text: string) of object;
  TPaintCellEvent = procedure (Sender: TObject; aCol, aRow: Integer; var Params: TCellParams) of object;
  TCellClickEvent = procedure(Sender: TObject; aCol, aRow: Integer) of object;
  TTitleClickEvent = procedure(Sender: TObject; aCol, aRow: Integer; Mode: THeaderMode) of object;
  TDropFilesEvent = procedure(Sender: TObject; const DroppedFiles: TStrings) of object;
  TQuoteEvent = procedure(Sender: TObject; aCol, aRow: Integer; var Handled: Boolean) of object;

{-------------------------------------------------------------------------------
  interne matrix events
-------------------------------------------------------------------------------}
  TGetCellStyleMethod = function(aCol, aRow: Integer): TCellStyle of object;

  TLookAndFeel = Integer;



  TCustomMatrix = class(TCustomControl)
  private
//    DynGetCellStyle            : TGetCellStyleMethod; // dynamische methode om cellstyle op te halen
    fAutoSizeParentForm        : Boolean;
    fBeforePaintCell           : TPaintCellEvent;
    fBorderStyle               : TMatrixBorderStyle;
//    fBottomRight               : TPoint; // laatste scrollbare cell in beeld
    fClientHeight              : Integer;
    fClientWidth               : Integer;
    fCol                       : Integer; // actieve kolom
    fColCount                  : Integer;
    fColInfo                   : TInfoList;
    fColumns                   : TMatrixColumns;
//    fData                      : TMatrixData;
    fDefaultColWidth           : Integer;
    fDefaultHeaderLineColor    : TColor;
//    fDefaultHeaderStyle        : TCellStyle;
    fDefaultLineColor          : TColor;
    fDefaultLineWidth          : Integer;
    fDefaultRowHeight          : Integer;
    fEditChar                  : Char; // char waarmee editor opgestart wordt
    fEditLink                  : IMatrixEditLink; // cell editor interface
    fEditCol                   : Integer;
    fEditRow                   : Integer;
    fFixedCols                 : Integer; // altijd zichtbaar
    fFixedRows                 : Integer; // altijd zichtbaar
    fHeaderStyle               : TCellStyle;
    fHighlightFontStyle        : TFontStyles;
//    fHighLightString           : string;
    fHighLightStrings          : array[0..MAX_HIGHLIGHT_STRINGS - 1] of string;
    fHighLightStringsUsed      : Integer;
    fInspector                 : IMatrixInspector;
    fInternalPaintOptions      : TInternalPaintOptions;
    fInternalStates            : TInternalStates; // interne staat van matrix
    fLeftHeaders               : Integer;
    fLeftLock                  : Integer;
    fMatrixFocused             : Boolean;
    fMouseInfo                 : TMouseInfo;
    fOnCellClick               : TCellClickEvent;
    fOnCellDoubleClick         : TCellClickEvent;
    fOnDropFiles               : TDropFilesEvent;
    fOnGetCellText             : TGetCellTextEvent;
    fOnPaintCell               : TPaintCellEvent;
    fOnTitleClick              : TTitleClickEvent;
    fOptions                   : TMatrixOptions;
    fPageInfoCache             : TPageCache; // cache voor actieve pagina
    fPaintLock                 : Integer;
//    fReadOnly                  : Boolean;
    fRow                       : Integer; // actieve rij
    fRowCount                  : Integer;
    fRowInfo                   : TInfoList;
//    fRows                      : TMatrixRows;
    fSearchString              : string;
    fSelectMode                : TSelectMode; // bepaalt highlighting cell, rij of kolom
    fSizingLinesRect           : TRect; // drag sizing lines
    fSortCol                   : Integer; // op welke kolom is matrix gesorteerd (-1 als niet gesorteerd)
    fSortDirection             : TSortDirection; // sorteervolgorde van sortcol
    fStates                    : TMatrixStates;
    fStyle                     : TCellStyle;
    fTopHeaders                : Integer;
    fTopLeft                   : TPoint; // eerste scrollbare cell in beeld
    fTopLock                   : Integer;
    fUpdateRect                : TRect;
    fVirtualMatrix             : Boolean;
    fLookAndFeel               : TLookAndFeel;
    fAfterFocusCell: TCellFocusEvent;
    fOnQuote: TQuoteEvent;
  { private interne methods }
//    procedure AdjustDynamicMethods;
    procedure AxisItemIndexChanged(Sender: TMatrixAxisItem; OldIndex, NewIndex: Integer);
//    function BottomReached(const aVert: TAxisInfo): Boolean;
    procedure CacheClientSize;
    procedure CalcHorizontalInfo(var aInfo: TAxisInfo; aLeft: Integer = -1);
    //function CalcNewTop(aReason: TScrollReason; aCommand: TScrollCommand; YParam: Integer = 0): Integer;
    //procedure CalcPageDown(var aNewInfo: TAxisInfo);
    procedure CalcPageHorz(out aHorz: TAxisInfo; aNewCol: Integer; aPosition: THorizontalFocusPosition);
    procedure CalcPageInfo(var aPageInfo: TPageInfo; aLeft: integer = -1; aTop: Integer = -1; ForceRecalc: Boolean = False);
    //procedure CalcPageUp(var aVert: TAxisInfo);
    procedure CalcPageVert(out aVert: TAxisInfo; aNewRow: Integer; aPosition: TVerticalFocusPosition; Force: Boolean = False);
    procedure CalcVerticalInfo(var aInfo: TAxisInfo; aTop: Integer = -1);
    function CanPaint: Boolean;
    function CellRectToScreenRect(aCol, aRow: integer; var aRect: TRect): Boolean;
    //function CellVisible(aCol, aRow: integer; const aPageInfo: TPageInfo): Boolean;
    procedure CheckCol(aCol: Integer; const Proc: string = '');
    procedure CheckCoord(aCol, aRow: Integer; const Proc: string = ''); overload;
    procedure CheckCoord(aCoord: TPoint; const Proc: string = ''); overload;
    procedure CheckRow(aRow: Integer; const Proc: string = '');
    procedure ColumnPropertyChanged(Item: TMatrixColumn);
    procedure ColumnsChanged(Item: TCollectionItem);
    function ColVisible(aCol: Integer; const HorzInfo: TAxisInfo): Boolean;
    procedure DoAutoSizeParentForm;
    function DoEdit: Boolean;
//    function DynGetCellStyle_Default(aCol, aRow: Integer): TCellStyle;
//    function DynGetCellStyle_UseCols(aCol, aRow: Integer): TCellStyle;
//    function GetBottomRight: TPoint;
    function GetColWidth(aCol: Integer): Integer;
    function GetEffectiveColLineWidth(aCol: Integer): Integer;
//    function GetEffectiveColWidth(aCol: Integer): Integer;
//    function GetEffectiveRowHeight(aRow: Integer): Integer;
    function GetEffectiveRowLineWidth(aRow: Integer): Integer;
    function GetHeaderMode(aCol, aRow: Integer): THeaderMode; overload;
    function GetHeaderMode(const aCell: TPoint): THeaderMode; overload;
    function GetRowHeight(aRow: Integer): Integer;
    function GetStyles(IsHeader: Boolean): TCellStyle;
    function HitTest(X, Y: Integer; out aCell: TPoint; out aCellRect: TRect; out aLineHit: TLineHit): Boolean;
    function HitTestResize(X, Y: Integer; out aCellRect: TRect; out aCell: TPoint; out aLineHit: TLineHit): Boolean;
    procedure InternalIncrementalSearch(const aSearchString: string);
    procedure InternalSetHighLightString(Index: Integer; const Value: string);
    procedure InternalStateChange(Enter, Leave: TInternalStates);
    procedure InternalStateEnter(aState: TInternalState);
    procedure InternalStateLeave(aState: TInternalState);
    procedure InvalidateMouseInfo;
    procedure InvalidatePageInfo;
    function IsActiveControl: Boolean;
    function IsClickableCell(aCol, aRow: Integer): Boolean;
    procedure OptionsChanged(Sender: TObject; aChange: TOptionsChange);
    procedure RestrictCol(var aCol: Integer);
    function RestrictLeft(var aLeft: Integer): Boolean;
    function RestrictRow(var aRow: Integer): Boolean;
    function RestrictTop(var aTop: Integer): Boolean;
    function RestrictTopLeft(var aTopLeft: TPoint): Boolean;
    function RowVisible(aRow: Integer; const VertInfo: TAxisInfo): Boolean;
    function Scroll(aReason: TScrollReason; aScrollMode: TScrollMode; XParam: Integer = 0; YParam: Integer = 0; DoForceRepaint: Boolean = False): Boolean;
    function ScrollTopLeft(aReason: TScrollReason; aCommand: TScrollCommand; XParam: Integer = 0; YParam: Integer = 0; DoInvalidate: Boolean = True): Boolean;
    procedure SetVirtualMatrix(const Value: Boolean);
    procedure StateChange(Enter, Leave: TMatrixStates);
    procedure StateEnter(aState: TMatrixState);
    procedure StateLeave(aState: TMatrixState);
    procedure StyleChanged(Sender: TObject);
//    procedure TimerReset(ID: Integer; Interval: Cardinal);
    procedure TimerStart(ID: Integer; Interval: Cardinal);
    procedure TimerStop(ID: Integer);
    (* WORDT NIET GEBRUIKT procedure TraverseCols(LocalColProc: pointer; const aRange: TAxisRange); *)
    procedure TraverseRows(LocalRowProc: Pointer; const aRange: TAxisRange);
    (* WORDT NIET GEBRUIKT procedure TraverseCells(LocalCellProc: pointer; const aRange: TRect); *)
    procedure TraverseVisibleCells(LocalCellProc: pointer; const PageInfo: TPageInfo; const aRange: TRect);
    procedure TraverseVisibleCols(LocalColProc: pointer; const HorzInfo: TAxisInfo; const aRange: TAxisRange);
    procedure TraverseVisibleRows(LocalRowProc: Pointer; const VertInfo: TAxisInfo; const aRange: TAxisRange);
    function ValidCol(aCol: Integer): Boolean;
    function ValidCoord(aCol, aRow: Integer): Boolean; overload;
    function ValidCoord(aCoord: TPoint): Boolean; overload;
    function ValidRow(aRow: Integer): Boolean;
  { private streaming methods }
    (* WORDT NIET GEBRUIKT procedure ReadColInfo(Reader: TReader); *)
    (* WORDT NIET GEBRUIKT procedure ReadInfo(InfoList: TInfoList; aCount: Integer; Reader: TReader); *)
    (* WORDT NIET GEBRUIKT procedure WriteColInfo(Writer: TWriter); *)
    (* WORDT NIET GEBRUIKT procedure WriteInfo(InfoList: TInfoList; aCount: Integer; Writer: TWriter); *)
  { private property access }
    function CalcVisibleDataRows: Integer;
    function CalcVisibleRows: Integer;
    function GetColLineWidth(aCol: Integer): Integer;
    function GetColRow: TPoint;
    function GetColumns: TMatrixColumns;
    function GetEditing: Boolean;
    function GetHiddenCol(aCol: Integer): boolean;
    function GetHighLightString: string;
    function GetHighLightStringByIndex(Index: THighLightRange): string;
    function GetRowLineWidth(aRow: Integer): Integer;
    function GetUsingColumns: Boolean;
    procedure SetAutoSizeParentForm(const Value: Boolean);
    procedure SetBorderStyle(const Value: TMatrixBorderStyle);
    procedure SetCol(const Value: Integer);
    procedure SetColCount(Value: Integer);
    procedure SetColLineWidth(aCol: Integer; const Value: Integer);
    procedure SetColRow(const Value: TPoint);
    procedure SetColumns(const Value: TMatrixColumns);
    procedure SetColWidth(N: Integer; aValue: Integer);
    procedure SetDefaultColWidth(const Value: Integer);
    procedure SetDefaultHeaderLineColor(const Value: TColor);
    procedure SetDefaultLineColor(const Value: TColor);
    procedure SetDefaultLineWidth(const Value: Integer);
    procedure SetDefaultRowHeight(const Value: Integer);
    procedure SetEditing(const Value: Boolean);
    procedure SetFixedCols(const Value: Integer);
    procedure SetFixedRows(const Value: Integer);
    procedure SetHeaderStyle(const Value: TCellStyle);
    procedure SetHiddenCol(aCol: Integer; const Value: boolean);
    procedure SetHighlightFontStyle(const Value: TFontStyles);
    procedure SetHighLightString(const Value: string);
    procedure SetHighLightStringByIndex(Index: THighLightRange; const Value: string);
    procedure SetInspector(const Value: IMatrixInspector);
    procedure SetLeftHeaders(const Value: Integer);
    procedure SetLeftLock(const Value: Integer);
    procedure SetLookAndFeel(const Value: TLookAndFeel);
    procedure SetOptions(const Value: TMatrixOptions);
    procedure SetRowCount(Value: Integer);
    procedure SetRowHeight(N: Integer; aValue: Integer);
    procedure SetRowLineWidth(aRow: Integer; const Value: Integer);
    procedure SetSelectMode(const Value: TSelectMode);
    procedure SetSortCol(const Value: Integer);
    procedure SetSortDirection(const Value: TSortDirection);
    procedure SetStyle(const Value: TCellStyle);
    procedure SetTopHeaders(const Value: Integer);
    procedure SetTopLeft(const Value: TPoint);
    procedure SetTopLock(const Value: Integer);
    procedure SetVisibleRows(Value: Integer);
    procedure UpdateIncrementalSearch(aKey: Char = #0);
    ///////////////////
  { messages }
    procedure CMBorderChanged(var Message: TMessage); message CM_BORDERCHANGED;
    procedure CMExit(var Message: TMessage); message CM_EXIT;
    procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure WMCancelMode(var Message: TWMCancelMode); message WM_CANCELMODE;
    procedure WMChar(var Message: TWMChar); message WM_CHAR;
    procedure WMDropFiles(var Msg: TWMDropFiles); message WM_DROPFILES;
    procedure WMEraseBkgnd(var Message: TWmEraseBkgnd); message WM_ERASEBKGND;
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    {procedure WMHScroll(var Message: TWMHScroll); message WM_HSCROLL; }
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
    procedure WMSetCursor(var Message: TWMSetCursor); message WM_SETCURSOR;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure WMKillFocus(var Message: TWMKillFocus); message WM_KILLFOCUS;
    procedure WMNCDestroy(var Message: TWMNCDestroy); message WM_NCDESTROY;
    procedure WMNCPaint(var Message: TMessage); message WM_NCPAINT; // non client drawing
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    {procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL; }
    procedure WMTimer(var Message: TWMTimer); message WM_TIMER;
  protected
  { protected overrides }
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure DefineProperties(Filer: TFiler); override;
    function DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    function DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    procedure KeyDown(var Key: word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint; override;
    procedure PaintWindow(DC: HDC); override;
    procedure Resize; override;
  { protected methods }
    function CanEditCell(aCol, aRow: Integer): Boolean; virtual;
    function DataTopRow: Integer; virtual;
    procedure DoAfterFocusCell(aCol, aRow: Integer); virtual;
    procedure DoAfterPreparePaintCell(aCol, aRow: Integer; var CellParams: TCellParams); virtual;
    procedure DoAutoSizeColumns; virtual;
    procedure DoBeforePaintCell(aCol, aRow: Integer; var Params: TCellParams); virtual;
    procedure DoCellHint(aCol, aRow: Integer); virtual;
    function DoCreateColumns: TMatrixColumns; virtual;
    function DoCreateEditor(aCol, aRow: Integer): IMatrixEditLink; virtual;
    procedure DoEditCancel; virtual;
    function DoEditEnd: Boolean; virtual;
    function DoGetCellStyle(aCol, aRow: Integer): TCellStyle; virtual;
    function DoGetCellText(aCol, aRow: Integer): string; virtual;
    procedure DoSetCellText(aCol, aRow: Integer; const Value: string); virtual;
    procedure DoGetCellData(aCol, aRow: Integer; var Params: TCellParams); virtual;
    function DoGetColumnImage(aCol: Integer; var Bitmap: TBitmap): Boolean;
    procedure DoIncrementalSearch(const aSearchString: string); virtual;
    procedure DoCellClick(aCol, aRow: Integer); virtual;
    procedure DoCellDoubleClick(aCol, aRow: Integer); virtual;
    procedure DoTitleClick(aCol, aRow: Integer; Mode: THeaderMode); virtual;
    function DoQuote: Boolean; virtual;
    function Navigation(aReason: TScrollReason; aCommand: TScrollCommand; XParam: Integer = 0; YParam: Integer = 0): Boolean;
    procedure HandleNavigation(var aReason: TScrollReason; aCommand: TScrollCommand; var XParam, YParam: Integer; out Mode: TScrollMode); virtual;
    procedure PaintBackGround(const aPaintInfo: TPaintInfo);
    procedure PaintCell(aCol, aRow: Integer; var CellParams: TCellParams; const aPaintInfo: TPaintInfo);
    procedure PaintCellPrepare(aCol, aRow: Integer; aRect: TRect; var CellParams: TCellParams);
    procedure PaintLines(const aPaintInfo: TPaintInfo);
    procedure PaintMatrix(aCanvas: TCanvas; aWindow: TRect; aOptions: TInternalPaintOptions);
    procedure PaintSizingLines(MouseX, MouseY: Integer; DoHide: Boolean = False);
  { protected properties }
    property AutoSizeParentForm: Boolean read fAutoSizeParentForm write SetAutoSizeParentForm;
    property BorderStyle: TMatrixBorderStyle read fBorderStyle write SetBorderStyle;
    property Col: Integer read fCol write SetCol ; // do not publish
    property ColCount: Integer read fColCount write SetColCount;
    property ColLines[aCol: Integer]: Integer read GetColLineWidth write SetColLineWidth;
    property Color default clWhite;
    property ColRow: TPoint read GetColRow write SetColRow; // do not publish!
    property Columns: TMatrixColumns read GetColumns write SetColumns;
    property ColWidths[N: Integer]: Integer read GetColWidth write SetColWidth; // do not publish!
    property Ctl3D default False;
    property DefaultColWidth: Integer read fDefaultColWidth write SetDefaultColWidth default DEF_COLWIDTH;
    property DefaultLineWidth: Integer read fDefaultLineWidth write SetDefaultLineWidth default DEF_LINEWIDTH;
    property DefaultLineColor: TColor read fDefaultLineColor write SetDefaultLineColor default DEF_LINECOLOR;
    property DefaultHeaderLineColor: TColor read fDefaultHeaderLineColor write SetDefaultHeaderLineColor default DEF_HEADERLINECOLOR;
    property DefaultRowHeight: Integer read fDefaultRowHeight write SetDefaultRowHeight default DEF_ROWHEIGHT;
    property EditChar: Char read fEditChar;
    property EditCol: Integer read fEditCol;
    property Editing: Boolean read GetEditing write SetEditing;
    property EditRow: Integer read fEditRow;
    property FixedCols: Integer read fFixedCols write SetFixedCols;
    property FixedROws: Integer read fFixedRows write SetFixedRows;
    property HeaderStyle: TCellStyle read fHeaderStyle write SetHeaderStyle;
    property HiddenCols[aCol: Integer]: boolean read GetHiddenCol write SetHiddenCol;
    property HighlightFontStyle: TFontStyles read fHighlightFontStyle write SetHighlightFontStyle;
    property HighLightString: string read GetHighLightString write SetHighLightString;
    property HighLightStrings[Index: THighLightRange]: string read GetHighLightStringByIndex write SetHighLightStringByIndex;
    property LeftHeaders: Integer read fLeftHeaders write SetLeftHeaders;
    property LeftLock: Integer read fLeftLock write SetLeftLock;
    property LookAndFeel: TLookAndFeel read fLookAndFeel write SetLookAndFeel;
    property Options: TMatrixOptions read fOptions write SetOptions;
    property Row: Integer read fRow; // do not publish!
    property RowCount: Integer read fRowCount write SetRowCount;
    property RowHeights[aRow: Integer]: Integer read GetRowHeight write SetRowHeight; // do not publish!
    property RowLineWidths[aRow: Integer]: Integer read GetRowLineWidth write SetRowLineWidth; // do not publish!
    property SelectMode: TSelectMode read fSelectMode write SetSelectMode;
    property SortCol: Integer read fSortCol write SetSortCol;
    property SortDirection: TSortDirection read fSortDirection write SetSortDirection;
    property States: TMatrixStates read fStates;
    property Style: TCellStyle read fStyle write SetStyle;
    property Styles[IsHeader: Boolean]: TCellStyle read GetStyles; // do not publish!
    property TopHeaders: Integer read fTopHeaders write SetTopHeaders;
    property TopLeft: TPoint read fTopLeft write SetTopLeft; // do not publish!
    property TopLock: Integer read fTopLock write SetTopLock;
    property TopRow: Integer read fTopLeft.Y; // do not publish!
    property UsingColumns: Boolean read GetUsingColumns;
    property VirtualMatrix: Boolean read fVirtualMatrix write SetVirtualMatrix; // do not publish!
    property VisibleDataRows: Integer read CalcVisibleDataRows;
    property VisibleRows: Integer read CalcVisibleRows write SetVisibleRows; // do not publish!
  { protected events }
    property AfterFocusCell: TCellFocusEvent read fAfterFocusCell write fAfterFocusCell;
    property BeforePaintCell: TPaintCellEvent read fBeforePaintCell write fBeforePaintCell;
    property OnCellClick: TCellClickEvent read fOnCellClick write fOnCellClick;
    property OnCellDoubleClick: TCellClickEvent read fOnCellDoubleClick write fOnCellDoubleClick;
    property OnGetCellText: TGetCellTextEvent read fOnGetCellText write fOnGetCellText;
    property OnDropFiles: TDropFilesEvent read fOnDropFiles write fOnDropFiles;
    property OnPaintCell: TPaintCellEvent read fOnPaintCell write fOnPaintCell;
    property OnQuote: TQuoteEvent read fOnQuote write fOnQuote;
    property OnTitleClick: TTitleClickEvent read fOnTitleClick write fOnTitleClick;
  public
  { public methods }
    property Canvas; // do not publish!
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function ExecuteAction(Action: TBasicAction): Boolean; override;
    function GetCellRect(aCol, aRow: integer): TRect;
    procedure Invalidate; override;
    procedure InvalidateCell(aCol, aRow: Integer; DirectUpdate: Boolean = False);
    procedure InvalidateCol(aCol: Integer; DirectUpdate: Boolean = False);
    procedure InvalidateRow(aRow: Integer; DirectUpdate: Boolean = False);
    procedure SafeInvalidateCell(aCol, aRow: Integer; DirectUpdate: Boolean = False);
    procedure PaintLock;
    function PaintLocked: Boolean;
    function GetCellParams(aCol, aRow: Integer): TCellParams;
    function GetGridExtent: TPoint;

    procedure PaintUnlock;
    function GetCellRectVirtual(aCol, aRow: integer): TRect;
    procedure CopyCell(aCol, aRow: Integer; aBitMap: TBitMap);
    property Text[aCol, aRow: Integer]: string read DoGetCellText write DoSetCellText;

    procedure FlipTest;

    {$ifdef UseMatrixInspector}
    property Inspector: IMatrixInspector read fInspector write SetInspector;
    {$endif}
  published
  end;

  TDataScrollInfo = (
    siNone,
    siBufferScrolled
  );

  TDataLinkInfos = (
    diBof,
    diEof
  );
  TDataLinkInfo = set of TDataLinkInfos;


  TMatrixDataLink = class(TDataLink) // Delphi TDataSet ondersteuning
  private
    fMatrix: TCustomDBMatrix;
//    fDisableCount: Integer;
//    fPrevState: TDataSetState;
    fInfo: TDataLinkInfo;
  protected
    procedure ActiveChanged; override;
    procedure DataSetChanged; override;
    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
    procedure DataSetScrolled(Distance: Integer); override;
    procedure EditingChanged; override;
    procedure LayoutChanged; override;
    procedure RecordChanged(Field: TField); override;
    procedure Disable;
    procedure Enable;
    //procedure Next;
    //procedure First;
    //procedure MoveBy(Distance: Integer)
  public
    constructor Create(aMatrix: TCustomDBMatrix);
  end;

  TCustomDBMatrix = class(TCustomMatrix)
  private
    fDataLink: TMatrixDataLink;
    fNavigating: Boolean;
    fScrollCheck: Boolean;
    fOnDataSetScroll: TNotifyEvent;
    fAutoGenerateColumns: Boolean;
  { private intern }
    function ActiveRecordToRow(aRec: Integer): Integer;
    procedure AdjustColumns;
    procedure AdjustRows;
    procedure AdjustActive;
    function CalcRow: Integer;
    function CalcRowCount: Integer;
    function CoordToField(aCol, aRow: Integer): TField;
    function ColToField(aCol: Integer): TField;
    function DataLinkActive: Boolean;
    procedure DataSetActiveChanged; // datalink event
    procedure DataSetRecordChanged(Field: TField); // datalink event
    procedure DataSetScrolled(Distance: Integer); // datalink event
    procedure DataSetChanged; // datalink event
    procedure DataSetLayoutChanged; // datalink event
    function RowToActiveRecord(aRow: Integer): Integer;
    procedure AssertRowCount(const aProc: string);
  { private property access }
    function GetDataSource: TDataSource;
    procedure SetDataSource(const Value: TDataSource);
    function GetDataSet: TDataSet;
    function DidScroll: Boolean;
    function GetSelectedField: TField;
    function GetVisibleFields: string;
  protected
  { protected overrides }
    function CanEditCell(aCol, aRow: Integer): Boolean; override;
    function DataTopRow: Integer; override;
    procedure DoAfterFocusCell(aCol, aRow: Integer); override;
    procedure DoAfterPreparePaintCell(aCol, aRow: Integer; var CellParams: TCellParams); override;
    procedure DoBeforePaintCell(aCol, aRow: Integer; var Params: TCellParams); override;
    function DoCreateEditor(aCol, aRow: Integer): IMatrixEditLink; override;
    function DoGetCellText(aCol, aRow: Integer): string; override;
    procedure DoGetCellData(aCol, aRow: Integer; var Params: TCellParams); override;
    procedure DoSetCellText(aCol, aRow: Integer; const Value: string); override;
    procedure DoIncrementalSearch(const aSearchString: string); override;
    procedure HandleNavigation(var aReason: TScrollReason; aCommand: TScrollCommand; var XParam, YParam: Integer; out Mode: TScrollMode); override;
    procedure KeyDown(var Key: word; Shift: TShiftState); override;
  { protected props }
    function DataLeftRow: Integer;
    property AutoGenerateColumns: Boolean read fAutoGenerateColumns write fAutoGenerateColumns;
    property DataLink: TMatrixDataLink read fDataLink; // do not publish!
    property DataSet: TDataSet read GetDataSet; // do not publish!
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property SelectedField: TField read GetSelectedField; // do not publish!
    property VisibleFields: string read GetVisibleFields;
  { protected events }
    property OnDataSetScroll: TNotifyEvent read fOnDataSetScroll write fOnDataSetScroll;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure GetCellData(aCol, aRow: Integer; var Params: TCellParams);
    procedure DoRefresh;
    procedure DoAutoSizeColumns; override;
  published
  end;

  TMatrix = class(TCustomMatrix)
  private
  protected
  public
    property ColLines;
    property ColRow;
    property Col;
    property ColWidths;
    property Editing;
    property HiddenCols;
    property Row;
    property RowHeights;
    property RowLineWidths;
    property TopLeft;
    property VisibleRows;
  published
  { published properties }
    property AutoSizeParentForm;
    property BorderStyle;
    property BorderWidth;
    property ColCount;
    property Color;
    property Columns;
    property FixedCols;
    property FixedRows;
    property DefaultHeaderLineColor;
    property DefaultLineColor;
    property DefaultLineWidth;
    property DefaultRowHeight;
    property HeaderStyle;
    property HighlightFontStyle;
    property HighLightString;
    property LeftHeaders;
    property LeftLock;
    property LookAndFeel;
    property Options;
    property ParentFont;
    property RowCount;
    property SelectMode;
    property Style;
    property TopHeaders;
    property TopLock;
  { published events }
    property BeforePaintCell;
    property OnCellDoubleClick;
    property OnCellClick;
    property OnDblClick;
    property OnDropFiles;
    property OnGetCellText;
    property OnPaintCell;
    property OnQuote;
    property OnTitleClick;
  end;

  TDBMatrix = class(TCustomDBMatrix)
  private
  protected
  public
    property Col;
    property ColLines;
    property ColRow;
    property ColWidths;
    property DataSet;
    property HiddenCols;
    property HighLightStrings;
    property Row;
    property RowHeights;
    property TopLeft;
    property SelectedField;
    property VisibleFields;
    property VisibleRows;
  { public events }
    property AfterFocusCell;
    property OnDataSetScroll;
  published
  { published properties }
    property Align;
    property AutoGenerateColumns;
    property AutoSizeParentForm;
    property BorderStyle;
    property BorderWidth;
    property ColCount;
    property Color;
    property Columns;
    property Ctl3D;
    property DataSource;
    property DefaultColWidth;
    property DefaultHeaderLineColor;
    property DefaultLineWidth;
    property DefaultLineColor;
    property DefaultRowHeight;
    property DragMode;
    property FixedCols;
    property FixedRows;
    property HeaderStyle;
    property HighlightFontStyle;
    property HighLightString;
    property LeftHeaders;
    property LeftLock;
    property LookAndFeel;
    property OnMouseDown;
    property OnStartDrag;
    property Options;
    property ParentFont;
    property ParentShowHint;
    property ShowHint;
    property RowCount;
    property SelectMode;
    property SortCol;
    property SortDirection;
    property Style;
    property TopHeaders;
    property TopLock;
  { published events }
    property BeforePaintCell;
    property OnCellDoubleClick;
    property OnCellClick;
    property OnDblClick;
    property OnDropFiles;
    property OnKeyDown;
    property OnKeyPress;
    property OnQuote;
    property OnTitleClick;
  end;

implementation

{$ifndef UseMatrixInspector}
uses
  Kernel_Resource, MxInplace, Types;
{$else}
uses
  Kernel_Resource, MxInplace, MxInspector, Types;
{$endif}

{Xr Mx.res} // VERHUISD NAAR KERNEL_RESOURCE.RES

var
  Initialized: Boolean; // vlag of globale vars geinitialiseerd zijn
  MatrixBitmap: TBitmap; // gebruikt door PaintMatrix
  CellBitmap: TBitmap; // gebruikt door PaintCell
  SelectionBitmap: TBitmap;
  UserCount: Integer;
//  MxImages10: TImageList; // matrix system images 10x10 (Matrix.RES)
//  XpImages16: TImageList; // xp bitmaps 16x16 (Matrix.RES)
//  XpImages24: TImageList; // xp bitmaps 24x24 (Matrix.RES)
  WinFlags: array[TMultiLineRange, TAlignment, TVertAlignment] of DWORD; // snelle conversie voor Windows.DrawTextEx


{ interne constanten }
const
//  ErrorCell: TPoint = (X: -1; Y: -1);
  AllCells: TRect = (Left: MinInt; Top: MinInt; Right: MaxInt; Bottom: MaxInt);
  AllAxis: TAxisRange = (First: MinInt; Last: MaxInt);
  ErrorRect: TRect = (Left: -1; Top: -1; Right: -1; Bottom: -1);
  EmptyRect: TRect = (Left: 0; Top: 0; Right: 0; Bottom: 0);
{  DefaultGlyphInfo: TGlyphInfo = (Glyph: TMx_None; Position: gpLeft; Distance: 2; Spacing: 2); }
  DragSizingStates = [mdColSizingPossible, mdColSizing, mdRowSizingPossible, mdRowSizing,
    mdCellSizingPossible, mdCellSizing];
  EmptyCellParams: TCellParams = (
    pDrawState    : [];
    pTextColor    : clNone;
    pBackColor    : clNone;
    pCellStyle    : nil;
    pCellRect     : (Left: 0; Top: 0; Right: 0; Bottom: 0);
    pDrawRect     : (Left: 0; Top: 0; Right: 0; Bottom: 0);
    pDrawTextRect : (Left: 0; Top: 0; Right: 0; Bottom: 0);
    pDrawImageRect: (Left: 0; Top: 0; Right: 0; Bottom: 0);
    pTextParams   : (cbSize: SizeOf(TDrawTextParams); iTabLength: 0; iLeftMargin: 0; iRightMargin: 0; uiLengthDrawn: 0);
    pText         : '';
    pField        : nil;
    //pFieldValue   : Unassigned; // deze kan niet geinitialiseerd worden al constante
    pColumn       : nil;
//    pRow          : nil;
    pPaintRecord  : -1;
    pActiveRecord : -1;
    pTag          : 0;
    pCanvas       : nil;
    pPainted      : False;
  );

  { default kleuren }
  DEF_TEXTCOLOR     = clWindowText;
  DEF_BACKCOLOR     = clWindow;
  DEF_TEXTFOCUSED   = clHighLightText;
  DEF_BACKFOCUSED   = $00C66300; //clHighLight; // $00C66300 mooi blauw
  DEF_TEXTCURRENT   = clWindowText;
  DEF_BACKCURRENT   = $00FFDCB9; //$00FFDCB9; // mooi lichtblauw
  DEF_TEXTSELECTED  = clWindowText;
  DEF_BACKSELECTED  = clAqua;
  DEF_TEXTHYPERLINK = clBlue;
  DEF_TEXTMATCH     = clRed;
  DEF_BACKMATCH     = clnone;//Highlight;
  DEF_DIFFBACKCOLOR = clWindow;//$00F0F0F0 lichtgrijs
  DEF_TEXTMATCH2    = clNavy;
  DEF_BACKMATCH2    = clnone;//Red;

  { default header kleuren }
  DEF_HEADER_TEXTCOLOR     = clWindowText;
  DEF_HEADER_BACKCOLOR     = clBtnFace;
  DEF_HEADER_TEXTFOCUSED   = clHighLightText;
  DEF_HEADER_BACKFOCUSED   = clBtnFace;//clGray;
  DEF_HEADER_TEXTCURRENT   = clWindowText;
  DEF_HEADER_BACKCURRENT   = clBtnFace;//clSilver;
  DEF_HEADER_TEXTSELECTED  = clWindowText;
  DEF_HEADER_BACKSELECTED  = clAqua;
  DEF_HEADER_TEXTHYPERLINK = clBlue;
  DEF_HEADER_TEXTMATCH     = clWindowText;
  DEF_HEADER_BACKMATCH     = clBtnFace;
  DEF_HEADER_DIFFBACKCOLOR = clBtnFace;
  DEF_HEADER_TEXTMATCH2    = clWindowText;//Yellow;
  DEF_HEADER_BACKMATCH2    = clBtnFace;//Red;
//  DEF_HEADER_BACKODDROW    = clBtnFace;

  DEF_COLORS: array [THeaderRange] of TStateColorArray = (
    (
      DEF_TEXTCOLOR,             // 0
      DEF_BACKCOLOR,             // 1
      DEF_TEXTFOCUSED,           // 2
      DEF_BACKFOCUSED,           // 3
      DEF_TEXTCURRENT,           // 4
      DEF_BACKCURRENT,           // 5
      DEF_TEXTSELECTED,          // 6
      DEF_BACKSELECTED,          // 7
      DEF_TEXTHYPERLINK,         // 8
      DEF_TEXTMATCH,             // 9
      DEF_BACKMATCH,             // 10
      DEF_DIFFBACKCOLOR,         // 11
      DEF_TEXTMATCH2,            // 12
      DEF_BACKMATCH2,            // 13
      clNone, clNone
    ),
    (
      DEF_HEADER_TEXTCOLOR,      // 0
      DEF_HEADER_BACKCOLOR,      // 1
      DEF_HEADER_TEXTFOCUSED,    // 2
      DEF_HEADER_BACKFOCUSED,    // 3
      DEF_HEADER_TEXTCURRENT,    // 4
      DEF_HEADER_BACKCURRENT,    // 5
      DEF_HEADER_TEXTSELECTED,   // 6
      DEF_HEADER_BACKSELECTED,   // 7
      DEF_HEADER_TEXTHYPERLINK,  // 8
      DEF_HEADER_TEXTMATCH,      // 9
      DEF_HEADER_BACKMATCH,      // 10
      DEF_HEADER_DIFFBACKCOLOR,  // 11
      DEF_HEADER_TEXTMATCH2,     // 12
      DEF_HEADER_BACKMATCH2,     // 13
      clNone, clNone
    )

  );

  { grenzen }
type
  TPositiveRange  = 0..MaxInt;
  TLineWidthRange = 0..256;
  TColWidthRange  = 4..4096;
  TRowHeightRange = 4..4096;


const
  { timer constanten voor windows timers }
  CURSOR_TIMER = 1; // timer voor het zetten van resize-cursor (voorkomt knipperende muiscursor)
  SEARCH_TIMER = 2; // timer voor incremental search
  SCROLL_TIMER = 3; // timer voor scroll events

  SEARCH_TIMER_INTERVAL = 600;



{*******************************************************************************

  interne procs en funcs

*******************************************************************************}

procedure Error(const Msg: string; HelpContext: Integer = 0); overload;
begin
  raise EMatrixException.CreateHelp(Msg, HelpContext);
end;

procedure Error(const Msg: string; const Args: array of const; HelpContext: Integer = 0); overload;
var
  S: string;
//  var
  //  i: Integer;
begin
  try
//    for i := 0 to Length(Args) do
    S := Format(Msg, Args);

  except
    S := Msg;
  end;
  Error(S, HelpContext);
//  raise EMatrixException.CreateFmtHelp(Msg, Args, HelpContext);
end;

procedure InitWinFlags;
var
  Def: Integer;
begin
  Def := DT_NOPREFIX;
{or DT_NOPREFIX or DT_WORDBREAK or DT_WORD_ELLIPSIS or DT_NOCLIP}

  { singleline }
  WinFlags[False, taLeftJustify, vaTop]     := Def or DT_LEFT or DT_TOP or DT_SINGLELINE;
  WinFlags[False, taLeftJustify, vaCenter]  := Def or DT_LEFT or DT_VCENTER or DT_SINGLELINE;
  WinFlags[False, taLeftJustify, vaBottom]  := Def or DT_LEFT or DT_BOTTOM or DT_SINGLELINE;

  WinFlags[False, taCenter, vaTop]          := Def or DT_CENTER or DT_TOP or DT_SINGLELINE;
  WinFlags[False, taCenter, vaCenter]       := Def or DT_CENTER or DT_VCENTER or DT_SINGLELINE;
  WinFlags[False, taCenter, vaBottom]       := Def or DT_CENTER or DT_BOTTOM or DT_SINGLELINE;

  WinFlags[False, taRightJustify, vaTop]    := Def or DT_RIGHT or DT_TOP or DT_SINGLELINE;
  WinFlags[False, taRightJustify, vaCenter] := Def or DT_RIGHT or DT_VCENTER or DT_SINGLELINE;
  WinFlags[False, taRightJustify, vaBottom] := Def or DT_RIGHT or DT_BOTTOM or DT_SINGLELINE;

  { multiline }
  WinFlags[True, taLeftJustify, vaTop]      := Def or DT_EDITCONTROL or DT_WORDBREAK;
  WinFlags[True, taLeftJustify, vaCenter]   := Def or DT_EDITCONTROL or DT_WORDBREAK;
  WinFlags[True, taLeftJustify, vaBottom]   := Def or DT_EDITCONTROL or DT_WORDBREAK;

  WinFlags[True, taCenter, vaTop]           := Def or DT_EDITCONTROL or DT_WORDBREAK;
  WinFlags[True, taCenter, vaCenter]        := Def or DT_EDITCONTROL or DT_WORDBREAK;
  WinFlags[True, taCenter, vaBottom]        := Def or DT_EDITCONTROL or DT_WORDBREAK;

  WinFlags[True, taRightJustify, vaTop]     := Def or DT_EDITCONTROL or DT_WORDBREAK;
  WinFlags[True, taRightJustify, vaCenter]  := Def or DT_EDITCONTROL or DT_WORDBREAK;
  WinFlags[True, taRightJustify, vaBottom]  := Def or DT_EDITCONTROL or DT_WORDBREAK;
end;

procedure UsesBitmap;

begin
  if UserCount = 0 then
  begin
    MatrixBitmap := TBitmap.Create;
    CellBitmap := TBitmap.Create;
    SelectionBitmap := TBitmap.Create;
  end;
  Inc(UserCount);
end;

procedure ReleaseBitmap;
begin
  Dec(UserCount);
  if UserCount = 0 then
  begin
    MatrixBitmap.Free;
    CellBitmap.Free;
    SelectionBitmap.Free;
  end;
end;

procedure BitmapResize(aBitmap: TBitmap; aWidth, aHeight: Integer);
begin
  with aBitmap do
  begin
    Width := Max(Width, aWidth);
    Height := Max(Height, aHeight);
  end;
end;

procedure InitializeGlobals;
//var
//  mx10: TMxBitmap10;
//  xp16: TXpBitmap16;
//  xp24: TXpBitmap24;
//  ResName : string;
//  BMP: TBitmap;
begin
  if Initialized then Exit;
  Initialized := True;
  InitWinFlags;

  Exit;

//  MxImages10 := TImageList.Create(nil);
//  XpImages16 := TImageList.Create(nil);
//  XpImages24 := TImageList.Create(nil);

//  BMP := TBitmap.Create;

  try

(*
  with MxImages10 do
  begin
    Height := 10;
    Width := 10;
    for mx10 := Low(TMxBitmap10) to High(TMxBitmap10) do
    begin
      Resname := GetEnumName(TypeInfo(TMxBitmap10), Integer(mx10));
      { converteer typenaam naar resourcenaam }
      ResName := CutLeft(ResName, 4);
      BMP.LoadFromResourceName(HINSTANCE, ResName);
      if AddMasked(BMP, BMP.TransparentColor) < 0 then
        Error('InitializeGlobals ImageList Bitmap ' + Resname + ' kan niet geladen worden');
    end;
  end; *)

(*  with XpImages16 do
  begin
    Height := 16;
    Width := 16;
    for xp16 := Low(TXpBitmap16) to High(TXpBitmap16) do
    begin
      Resname := GetEnumName(TypeInfo(TXpBitmap16), Integer(xp16));
      { converteer typenaam naar resourcenaam }
      ResName := CutLeft(ResName, 4);
      BMP.LoadFromResourceName(HINSTANCE, ResName);
      if AddMasked(BMP, BMP.TransparentColor) < 0 then
        Error('InitializeGlobals ImageList Bitmap ' + Resname + ' kan niet geladen worden');
    end;
  end;
  with XpImages24 do
  begin
    Height := 24;
    Width := 24;
    for xp24 := Low(TXpBitmap24) to High(TXpBitmap24) do
    begin
      Resname := GetEnumName(TypeInfo(TXpBitmap24), Integer(xp24));
      { converteer typenaam naar resourcenaam }
      ResName := CutLeft(ResName, 4);
      BMP.LoadFromResourceName(HINSTANCE, ResName);
      if AddMasked(BMP, BMP.TransparentColor) < 0 then
        Error('InitializeGlobals ImageList Bitmap ' + Resname + ' kan niet geladen worden');
    end;
  end; *)

  finally
//    BMP.Free;
  end;
end;

procedure FinalizeGlobals;
begin
  if not Initialized then Exit;
  // heel belangrijk, als dit in een module zit. waarom PRECIES weet ik nog niet
  Initialized := False;
//  FreeAndNil(MxImages10);
{  if UserCount > 0 then
  begin
    UserCount := 0;
    ReleaseBitmap;
  end; }
(*  XpImages16.Free;
  XpImages24.Free; *)
end;


function PointInRect(X, Y: Integer; const R: TRect): Boolean;
begin
  Result := (X >= R.Left) and (X <= R.Right) and (Y >= R.Top) and (Y <= R.Bottom);
end;

(*
function IntersectRect(var R: TRect; R1, R2: TRect): Boolean;
begin
  if (R1.Left > R2.Right) or (R1.Top > R2.Bottom) or
     (R1.Bottom < R2.Top) or (R1.Right < R2.Left) then
  begin
    Result := False;
    R := EmptyRect;
    Exit;
  end
  else begin
    Result := True;
    R.Left := Max(R1.Left, R2.Left);
    R.Top := Max(R1.Top, R2.Top);
    R.Right := Min(R1.Right, R2.Right);
    R.Bottom := Min(R1.Bottom, R2.Bottom);
  end;
end;
*)

function RectsDoIntersect(const R1, R2: TRect): Boolean;
begin
  if (R1.Left > R2.Right) or (R1.Top > R2.Bottom) or
     (R1.Bottom < R2.Top) or (R1.Right < R2.Left)
  then
    Result := False
  else
    Result := True;
end;

function GetBkColor(const CellStyle: TCellStyle; DrawState: TCellDrawState): TColor;
begin
  with CellStyle do
  begin
    { 1: matrix gefocused }
    if dsMatrixFocused in DrawState then
    begin
      { 1a: highlight }
      if [dsHighlightRow, dsHighlightCol] * DrawState <> [] then
      begin
        Result := CachedColors[MI_BACKCURRENT];
        Exit;
      end;
      { 1b: niet huidige cell }
      if not (dsFocused in DrawState) then
      begin
        if (dsDiffRow in DrawState) {and (CachedColors[MI_DIFFBACKCOLOR] <> clNone)} then
//          Result := CachedColors[MI_DIFFBACKCOLOR]
          Result := CachedColors[MI_BACKCOLOR]
        else
          Result := CachedColors[MI_BACKCOLOR];
        Exit;
      end;
      { 1c: huidige cell}
      if dsFocused in DrawState then
      begin
        Result := CachedColors[MI_BACKFOCUSED];
        Exit;
      end;
      Result := clWhite; // voor de zekerheid
    end
    { 2: matrix niet gefocused }
    else begin
      { 1a: highlight }
      if [dsHighlightRow, dsHighlightCol] * DrawState <> [] then
      begin
        Result := CachedColors[MI_BACKCURRENT];
        Exit;
      end;
      { 1b: niet huidige cell }
      if not (dsFocused in DrawState) then
      begin
        if dsDiffRow in DrawState then
//          Result := CachedColors[MI_DIFFBACKCOLOR]
          Result := CachedColors[MI_BACKCOLOR]
        else
          Result := CachedColors[MI_BACKCOLOR];
        //Result := CachedColors[MI_BACKCOLOR];
        Exit;
      end;
      { 1c: huidige cell}
      if dsFocused in DrawState then
      begin
        Result := CachedColors[MI_BACKCURRENT];
        Exit;
      end;
      Result := clWhite; // voor de zekerheid
    end;
  end;

end;

function GetTxtColor(const CellStyle: TCellStyle; DrawState: TCellDrawState): TColor;
begin

  with CellStyle do
  begin
    { 1: matrix gefocused }
    if dsMatrixFocused in DrawState then
    begin
      { 1a: highlight }
      if [dsHighlightRow, dsHighlightCol] * DrawState <> [] then
      begin
        Result := CachedColors[MI_TEXTCURRENT];
        Exit;
      end;
      { 1b: niet huidige cell }
      if not (dsFocused in DrawState) then
      begin
        if dsHyperLink in DrawState then
          Result := CachedColors[MI_TEXTHYPERLINK]
        else
          Result := CachedColors[MI_TEXTCOLOR];
        Exit;
      end;
      { 1c: huidige cell}
      if dsFocused in DrawState then
      begin
        // hyperlink niet of nieuwe kleur maken
        Result := CachedColors[MI_TEXTFOCUSED];
        Exit;
      end;
      Result := clBlack; // voor de zekerheid
    end
    { 2: matrix niet gefocused }
    else begin
      { 1a: highlight }
      if [dsHighlightRow, dsHighlightCol] * DrawState <> [] then
      begin
        Result := CachedColors[MI_TEXTCURRENT];
        Exit;
      end;
      { 1b: niet huidige cell }
      if not (dsFocused in DrawState) then
      begin
        if dsHyperLink in DrawState then
          Result := CachedColors[MI_TEXTHYPERLINK]
        else
          Result := CachedColors[MI_TEXTCOLOR];
        Exit;
      end;
      { 1c: huidige cell}
      if dsFocused in DrawState then
      begin
        Result := CachedColors[MI_TEXTCURRENT];
        Exit;
      end;
      Result := clBlack; // voor de zekerheid
    end;
  end;

end;

procedure DrawHeader(const aCanvas: TCanvas; const aRect: TRect; Down: Boolean);
var
  R: TRect;
begin
  with aCanvas do
  begin
    R := aRect;
    { teken buttonlook header }
    begin
      SetBkMode(Handle, TRANSPARENT);
      if not Down then
      begin
        DrawEdge(Handle, R, BDR_RAISEDINNER, BF_TOPLEFT);
        DrawEdge(Handle, R, BDR_RAISEDINNER, BF_BOTTOMRIGHT);
      end
      else begin
        DrawEdge(Handle, R, BDR_SUNKENINNER, BF_TOPLEFT);
        DrawEdge(Handle, R, BDR_SUNKENINNER, BF_BOTTOMRIGHT);
      end;
    end;
  end
end;

{*******************************************************************************

  Interne procedure voor incremental search

*******************************************************************************}

function SearchDataSet(DS: TDataSet; const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions; RetryFromBeginning: Boolean = True): Boolean;
var
  fl: TList;
  CurBookmark: string;
  fld, val: Variant;
  i, fld_cnt: Integer;
begin
  with DS do
  begin
{    if Bof then
    begin
      Result := Locate(KeyFields, KeyValues, Options);
    end; }

    fl := TList.Create;
    try
      GetFieldList(fl, KeyFields);
      fld_cnt := fl.Count;
      CurBookmark := Bookmark;
//      Next;
      First;
      result := False;
      while ((not result) and (not EOF)) do
      begin
        i := 0;
        result := True;
        while (result and (i < fld_cnt)) do
        begin
          if fld_cnt > 1 then
            val := KeyValues[i]
          else
            val := KeyValues;
          fld := TField(fl[i]).Value;
          result := not (VarIsNull(val) or VarIsNull(fld));
          if result then
            try
              fld := VarAsType(fld, VarType(val));
            except
              on E: EVariantError do result := False;
            end;
          if result then
          begin
            if TField(fl[i]).DataType = ftString then
            begin
              if (loCaseInsensitive in Options) then
              begin
                fld := AnsiUpperCase(fld);
                val := AnsiUpperCase(val);
              end;
              fld := TrimRight(fld);
              val := TrimRight(val);
              if (loPartialKey in Options) then
                result := result and (AnsiPos(val, fld) = 1)
              else
                result := result and (val = fld);
          end else
            result := result and (val = fld);
          end;
          Inc(i);
        end;
        if not result then
          Next;
      end;
      if not result then
        Bookmark := CurBookmark
      else
        CursorPosChanged;
    finally
      fl.Free;
    end;






  end;

end;

function SplitRect(var R: TRect; X: Integer): TRect;
begin
  with Result do
  begin
    X := R.Right - X;
    Left := X;
    Right := R.Right;
    Top := R.Top;
    Bottom := R.Bottom;
    R.Right := X - 1;
  end;
end;

type
  TAxisCollectionWrapper = class(TComponent)
  private
    fAxisCollection: TMatrixAxisCollection;
  published
    property AxisCollection: TMatrixAxisCollection read fAxisCollection write fAxisCollection;
  end;


{ TStateColors }

procedure TStateColors.Assign(Source: TPersistent);
var
  St: TStateColors;
  i: Integer;
begin
  { header en ownerflag moeten behouden blijven }
  if Source is TStateColors then
  begin
    St := TStateColors(Source);
    fAssignedColorProps := TStateColors(Source).fAssignedColorProps;
    for i := Low(TColorRange) to High(TColorRange) do
      fColorArray[i] := St.fColorArray[i];
  end
  else inherited Assign(Source);
  //Changed;
end;

procedure TStateColors.Changed;
begin
  if Assigned(fOnChange) then fOnChange(Self);
end;

constructor TStateColors.Create(aOwner: TPersistent; aHeader: Boolean; aOwnerFlag: TOwnerFlag);
begin
  inherited Create(aOwner);
  fIsHeader := aHeader;
  fOwnerFlag := aOwnerFlag;
  fColorArray := DEF_COLORS[fIsHeader];
end;

function TStateColors.GetColorElement(const Index: Integer): TColor;
begin
  if Index in fAssignedColorProps then
    Result := fColorArray[Index]
  else
    Result := DefaultColorElement[Index];
end;

function TStateColors.GetDefaultColorElement(const Index: Integer): TColor;
var
  M: TCustomMatrix;
begin
  Result := DEF_COLORS[fIsHeader, Index];

  if fOwnerFlag in [ofNone, ofMatrix] then
    Exit;

//    Result := DEF_COLORS[fIsHeader, Index]
  //else begin
    M := TCustomMatrix(FindOwner(TCustomMatrix));
    if M <> nil then
    case fIsHeader of
      False: Result := M.fStyle.fColors.fColorArray[Index];
      True: Result := M.fHeaderStyle.fColors.fColorArray[Index];
    end { case }
  //  else

//  end
end;

function TStateColors.IsColorStored(const Index: Integer): Boolean;
begin
  Result := (Index in fAssignedColorProps) and (fColorArray[Index] <> DefaultColorElement[Index]);
end;

procedure TStateColors.SetColorElement(const Index: Integer; const Value: TColor);
begin
  if (Index in fAssignedColorProps) and (fColorArray[Index] = Value) then Exit;
  fColorArray[Index] := Value;
  Include(fAssignedColorProps, Index);
  Changed;
end;

{ TCellStyle }

procedure TCellStyle.Assign(Source: TPersistent);
var
  CS: TCellStyle;
begin
  if Source is TCellStyle then
  begin
    CS := TCellStyle(Source);
    fIsHeader  := CS.IsHeader;
    fHorzAlign := CS.fHorzAlign;
    fVertAlign := CS.fVertAlign;
    fMultiLine := CS.fMultiLine;
    fColors.Assign(CS.fColors);
    fFont.Assign(CS.fFont);
    fAssignedCellProps := CS.fAssignedCellProps;
  end
  else inherited Assign(Source);
  //SetCached(False);
  Changed;
end;

procedure TCellStyle.BeginUpdate;
begin
  Inc(fUpdateFlag);
end;

procedure TCellStyle.Changed;
begin
  fCached := False;
  if fUpdateFlag = 0 then
  begin
{    if fOwnerFlag = ofMatrix then
      if TCustomMatrix(Owner).UsingColumns then asdf sad }
    if Assigned(fOnChange) then
      fOnChange(Self);
  end;
end;

procedure TCellStyle.ColorChanged(Sender: TObject);
begin
  Changed;
end;

constructor TCellStyle.Create(aOwner: TPersistent; aHeader: Boolean);
begin
  inherited Create(aOwner);
  { ownerflag is om (met een case-statement) defaults te achterhalen }
  if aOwner is TCustomMatrix then
    fOwnerFlag := ofMatrix
  else if aOwner is TMatrixColumn then
    fOwnerFlag := ofColumn;
  fFont := TFont.Create;
  fFont.OnChange := FontChanged;
  fIsHeader := aHeader;
  fColors := TStateColors.Create(Self, aHeader, fOwnerFlag);
  fColors.OnChange := ColorChanged;
  fFlat := DEF_FLAT[fIsHeader];
  fVertAlign := DEF_VERTALIGN[fIsHeader];
  fCache.Font := TFont.Create;
end;

destructor TCellStyle.Destroy;
begin
  fColors.Free;
  fFont.Free;
  fCache.Font.Free;
  inherited;
end;

(*procedure TCellStyle.EdgeChanged(Sender: TObject);
begin
  Include(fAssignedCellProps, acp_Edge);
  Changed;
end;*)

procedure TCellStyle.EndUpdate;
begin
  if fUpdateFlag > 0 then
  begin
    Dec(fUpdateFlag);
    if fUpdateFlag = 0 then
      Changed;
  end;
end;

procedure TCellStyle.FontChanged(Sender: TObject);
begin
  Include(fAssignedCellProps, acp_Font);
  Changed;
end;

{function TCellStyle.GetDefaultEdge: TEdge;
var
  M: TCustomMatrix;
begin
  M := TCustomMatrix(FindOwner(TCustomMatrix));
  if M <> nil then
  begin
    Result := M.Styles[fIsHeader].fEdge;
  end
  else Error('DefaultEdge niet gevonden');
end;}

function TCellStyle.GetDefaultFlat: Boolean;
var
  M: TCustomMatrix;
begin
  Result := DEF_FLAT[fIsHeader];
  case fOwnerFlag of
    ofColumn:
      begin
        M := TCustomMatrix(FindOwner(TCustomMatrix));
        if M <> nil then Result := M.Styles[fIsHeader].Flat;
      end;
  end;
end;

function TCellStyle.GetDefaultFont: TFont;
var
  M: TCustomMatrix;
begin
  M := TCustomMatrix(FindOwner(TCustomMatrix));
  if M <> nil then
  begin
    Result := M.Styles[fIsHeader].fFont;
  end
  else begin
    Result := nil;
    Error('DefaultFont niet gevonden');
  end;
end;

function TCellStyle.GetDefaultHorzAlign: TAlignment;
var
  M: TCustomMatrix;
begin
  Result := DEF_HORZALIGN[fIsHeader];
  case fOwnerFlag of
    ofColumn:
      begin
        M := TCustomMatrix(FindOwner(TCustomMatrix));
        if M <> nil then Result := M.Styles[fIsHeader].HorzAlign
      end;
  end;
end;

function TCellStyle.GetDefaultHyperLink: Boolean;
var
  M: TCustomMatrix;
begin
  Result := DEF_HYPERLINK[fIsHeader];
  case fOwnerFlag of
    ofColumn:
      begin
        M := TCustomMatrix(FindOwner(TCustomMatrix));
        if M <> nil then Result := M.Styles[fIsHeader].HyperLink;
      end;
  end;
end;

function TCellStyle.GetDefaultMultiLine: Boolean;
var
  M: TCustomMatrix;
begin
  Result := DEF_MULTILINE[fIsHeader];
  case fOwnerFlag of
    ofColumn:
      begin
        M := TCustomMatrix(FindOwner(TCustomMatrix));
        if M <> nil then Result := M.Styles[fIsHeader].MultiLine;
      end;
  end;
end;

function TCellStyle.GetDefaultVertAlign: TVertAlignment;
var
  M: TCustomMatrix;
begin
  Result := DEF_VERTALIGN[fIsHeader];
  case fOwnerFlag of
    ofColumn:
      begin
        M := TCustomMatrix(FindOwner(TCustomMatrix));
        if M <> nil then Result := M.Styles[fIsHeader].VertAlign
      end;
  end;
end;

(*function TCellStyle.GetEdge: TEdge;
var
  Save: TNotifyEvent;
begin
  if not (acp_Edge in fAssignedCellProps) then
  begin
    Save := fEdge.OnChange;
    fEdge.OnChange := nil;
    fEdge.Assign(DefaultEdge);
    fEdge.OnChange := Save;
  end;
  Result := fEdge;
end;*)

function TCellStyle.GetFlat: Boolean;
begin
  if fCached then Result := fCache.Flat
  else if acp_Flat in fAssignedCellProps then Result := fFlat
  else Result := DefaultFlat;
end;

function TCellStyle.GetFont: TFont;
var
  Save: TNotifyEvent;
begin
//  if fCached then Result := fCache.Font
//  else
  if not (acp_Font in fAssignedCellProps) and (fFont.Handle <> DefaultFont.Handle) then
  begin
    Save := fFont.OnChange;
    fFont.OnChange := nil;
    fFont.Assign(DefaultFont);
    fFont.OnChange := Save;
  end;
  Result := fFont;
end;

function TCellStyle.GetHorzAlign: TAlignment;
begin
  if fCached then Result := fCache.HorzAlign
  else if acp_HorzAlign in fAssignedCellProps then Result := fHorzAlign
  else Result := DefaultHorzAlign;
end;

function TCellStyle.GetHyperLink: Boolean;
begin
  if fCached then Result := fCache.HyperLink else
  if acp_HyperLink in fAssignedCellProps then Result := fHyperLink
  else Result := DefaultHyperLink;
end;

function TCellStyle.GetMultiLine: Boolean;
begin
  if fCached then Result := fCache.MultiLine else
  if acp_MultiLine in fAssignedCellProps then Result := fMultiline
  else Result := DefaultMultiLine;
end;

function TCellStyle.GetVertAlign: TVertAlignment;
begin
  if fCached then Result := fCache.VertAlign else
  if acp_VertAlign in fAssignedCellProps then Result := fVertALign
  else Result := DefaultVertAlign;
end;

function TCellStyle.IsFlatStored: Boolean;
begin
  Result := (acp_Flat in fAssignedCellProps) and (fFlat <> DefaultFlat);
end;

function TCellStyle.IsFontStored: Boolean;
begin
  Result := acp_Font in fAssignedCellProps;
end;

function TCellStyle.IsHorzAlignStored: Boolean;
begin
  Result := (acp_HorzAlign in fAssignedCellProps) and (fHorzAlign <> DefaultHorzAlign);
end;

function TCellStyle.IsHyperLinkStored: Boolean;
begin
  Result := (acp_HyperLink in fAssignedCellProps) and (fHyperLink <> DefaultHyperLink);
end;

function TCellStyle.IsMultiLineStored: Boolean;
begin
  Result := (acp_MultiLine in fAssignedCellProps) and (fMultiLine <> DefaultMultiLine);
end;

function TCellStyle.IsVertAlignStored: Boolean;
begin
  Result := (acp_VertAlign in fAssignedCellProps) and (fVertAlign <> DefaultVertAlign);
end;

procedure TCellStyle.SetCached(DoCache: Boolean);
var
  i: integer;
begin
  if fCached = DoCache then Exit;

  {-------------------------------------------------------------------------------
    zet fcached tijdelijk uit om te voorkomen dat de property-getters
    bij het assignen van fcache NIET waarden uit de cache teruggeven.
  -------------------------------------------------------------------------------}
  fCached := False;

  if DoCache then
  begin
    for i := Low(TColorRange) to High(TColorRange) do
      fCache.Colors[i] := fColors.GetColorElement(i);
    fCache.Flat        := Flat;
    fCache.Font.Assign(Font);
    fCache.HorzAlign   := HorzAlign;
    fCache.HyperLink   := HyperLink;
    fCache.VertAlign   := VertAlign;
    fCache.MultiLine   := MultiLine;
  end;

  fCached := DoCache;
end;

procedure TCellStyle.SetColors(const Value: TStateColors);
begin
  fColors.Assign(Value);
end;

{procedure TCellStyle.SetEdge(const Value: TEdge);
begin
  fEdge.Assign(Value);
end;}

procedure TCellStyle.SetFlat(const Value: Boolean);
begin
  if (fFlat = Value) and (acp_Flat in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_Flat);
  fFlat := Value;
  Changed;
end;

procedure TCellStyle.SetFont(const Value: TFont);
begin
  fFont.Assign(Value);
end;

procedure TCellStyle.SetHorzAlign(const Value: TAlignment);
begin
  if (fHorzAlign = Value) and (acp_HorzAlign in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_HorzAlign);
  fHorzAlign := Value;
  Changed;
end;

procedure TCellStyle.SetHyperLink(const Value: Boolean);
begin
  if (fHyperLink = Value) and (acp_HyperLink in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_HyperLink);
  fHyperLink := Value;
  Changed;
end;

procedure TCellStyle.SetMultiLine(const Value: Boolean);
begin
  if (fMultiLine = Value) and (acp_MultiLine in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_MultiLine);
  fMultiLine := Value;
  Changed;
end;

procedure TCellStyle.SetVertAlign(const Value: TVertAlignment);
begin
  if (fVertAlign = Value) and (acp_VertAlign in fAssignedCellProps) then Exit;
  Include(fAssignedCellProps, acp_VertAlign);
  fVertAlign := Value;
  Changed;
end;

{ TMatrixAxisItem }

procedure TMatrixAxisItem.Assign(Source: TPersistent);
var
  S: TMatrixAxisItem absolute Source;
begin
  if Source is TMatrixAxisItem then
  begin
    fInitFlag      := S.fInitFlag;
    fAutoGenerated := S.AutoGenerated;
    fStyle.Assign(S.fStyle);
    fHeaderStyle.Assign(fHeaderStyle);
    fDataField     := S.fDataField;
    fField         := S.fField;
    fPrompt        := S.fPrompt;
    fIndex         := S.fIndex;
    fHeaderLook    := S.fHeaderLook;
  end else inherited;
end;

constructor TMatrixAxisItem.Create(aCollection: TCollection);
begin
  inherited Create(aCollection);
  fIndex := Collection.Count - 1; // dit is initieel altijd waar
  fStyle := TCellStyle.Create(Self, False);
  fHeaderStyle := TCellStyle.Create(Self, True);
  fStyle.fOnChange := StyleChanged;
  fHeaderStyle.fOnChange := StyleChanged;
end;

destructor TMatrixAxisItem.Destroy;
begin
  fStyle.Free;
  fHeaderStyle.Free;
  inherited Destroy;
end;

function TMatrixAxisItem.GetMatrix: TCustomMatrix;
begin
  Result := (Collection as TMatrixColumns).Matrix;
end;

procedure TMatrixAxisItem.PropertyChanged;
begin
  Matrix.Invalidate;
end;

procedure TMatrixAxisItem.SetDataField(const Value: string);
var
  M: TCustomDBMatrix;
begin
  if CompareText(fDataField, Value) = 0 then Exit;
  fDataField := Value;
  M := TCustomDBMatrix(Matrix);
  if Assigned(M)
  and (M is TCustomDBMatrix)
  and not (csLoading in M.ComponentState)
  and M.DataLinkActive then
    SetField(M.fDataLink.DataSet.FindField(fDataField));
  Changed(False);
end;

procedure TMatrixAxisItem.SetField(const Value: TField);
begin
  if fField = Value then Exit;
  fField := Value;
  if fField <> nil then
  begin
    fDataField := fField.FieldName;
    if fField.Alignment <> Style.HorzAlign then
      Style.HorzAlign := fField.Alignment;
    //if fField.Readonly then
    if not ReadOnly then
      Readonly := fField.Readonly;
  end;
  Changed(False);
end;

{ TMatrixColumn }

function TMatrixColumn.GetVisible: Boolean;
begin
  Result := not Matrix.HiddenCols[fIndex];
end;

function TMatrixColumn.GetWidth: Integer;
begin
  Result := Matrix.ColWidths[fIndex];
end;

procedure TMatrixColumn.PropertyChanged;
begin
  Matrix.ColumnPropertyChanged(Self);
end;

procedure TMatrixAxisItem.SetHeaderStyle(const Value: TCellStyle);
begin
  fHeaderStyle.Assign(Value);
end;

procedure TMatrixAxisItem.SetIndex(Value: Integer);
var
  OldIndex: Integer;
begin
  Restrict(Value, 0, Collection.Count - 1);

  OldIndex := fIndex;
  inherited SetIndex(Value);
  TMatrixAxisCollection(Collection).UpdateIndices; // update alle findex
  Matrix.AxisItemIndexChanged(Self, OldIndex, fIndex);
end;

procedure TMatrixAxisItem.SetPrompt(const Value: string);
begin
  if fPrompt = Value then Exit;
  fPrompt := Value;
  PropertyChanged;
end;

procedure TMatrixAxisItem.SetStyle(const Value: TCellStyle);
begin
  fStyle.Assign(Value);
end;

procedure TMatrixAxisItem.StyleChanged(Sender: TObject);
begin
  PropertyChanged;
end;

procedure TMatrixColumn.SetWidth(const Value: Integer);
begin
  Matrix.ColWidths[fIndex] := Value;
end;

{ TMatrixColumns }

function TMatrixColumns.Add: TMatrixColumn;
begin
  Result := TMatrixColumn(inherited Add);
end;

procedure TMatrixColumns.BeginUpdate;
begin
  inherited BeginUpdate;
end;

constructor TMatrixColumns.Create(aMatrix: TCustomMatrix);
begin
  inherited Create(aMatrix, TMatrixColumn);
end;

(*procedure TMatrixColumns.ForceUpdate(Item: TCollectionItem);
begin
//  EndUpdate
end;*)

procedure TMatrixColumns.EndUpdate;
begin
  inherited EndUpdate;
end;

function TMatrixColumns.FindColumnByFieldName(const aFieldName: string): TMatrixColumn;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    if CompareText(Items[i].DataField, aFieldName) = 0 then
    begin
      Result := Items[i];
      Exit;
    end;
  Result := nil;
end;

function TMatrixColumns.GetColumn(N: Integer): TMatrixColumn;
begin
  Result := TMatrixColumn(inherited GetItem(N));
end;

function TMatrixColumns.Insert(Index: Integer): TMatrixColumn;
begin
  Result := TMatrixColumn(inherited Insert(Index));
end;

function TMatrixColumns.InternalAdd: TMatrixColumn;
begin
  Result := TMatrixColumn(inherited InternalAdd);
  //TMatrixAxisItem(Result).fAutoGenerated := True;  dbgrids
end;

procedure TMatrixColumns.Update(Item: TCollectionItem);
begin
//  if fDisableUpdateCount = 0 then
  if Matrix <> nil then
    Matrix.ColumnsChanged(Item);
end;

{ TMatrixRows }

(*constructor TMatrixRows.Create(aMatrix: TCustomMatrix);
begin
  inherited Create(aMatrix, TMatrixRow);
  fMatrix := aMatrix;
end;*)

{ TMatrixCells }

(*
constructor TMatrixCells.Create(aMatrix: TCustomMatrix);
begin
  inherited Create(aMatrix, TMatrixCell);
  fMatrix := aMatrix;
end;

{ TMatrixData }

procedure TMatrixData.EnsureColRow(aCol, aRow: integer);
var
  L, Delta: integer;
begin
//  CheckCoord(aCol, aRow);
  { smart allocation rijen }
  L := Length(fData);
  if L > 64 then Delta := L div 4 else
    if L > 8 then Delta := 16 else
      Delta := 4;
  { rijen}
  if L < aRow + 1 then
  begin
    SetLength(fData, aRow + 1 + Delta); // initialiseren op nil nodig?
  end;
  { kolommen }
  if Length(fData[aRow]) < aCol + 1 then
  begin
    SetLength(fData[aRow], aCol + 1); // initialiseren op nil nodig?
  end;
end;
*)

{ TMatrixAxisCollection }

procedure TMatrixAxisCollection.ClearStyleCache;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    with TMatrixAxisItem(Items[i]).fStyle do SetCached(False);
end;

constructor TMatrixAxisCollection.Create(aMatrix: TCustomMatrix; aItemClass: TCollectionItemClass);
begin
  inherited Create(aMatrix, aItemClass);
  fMatrix := aMatrix;
end;


function TMatrixAxisCollection.GetAutoGenerated: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i := 0 to Count - 1 do
    with TMatrixAxisItem(Items[i]) do
      if not fAutoGenerated then
        Exit;
  Result := True;
end;

function TMatrixAxisCollection.GetCount: Integer;
begin
  Result := inherited Count;
end;

function TMatrixAxisCollection.InternalAdd: TMatrixAxisItem;
begin
  Result := TMatrixAxisItem(inherited Add);
  TMatrixAxisItem(Result).fAutoGenerated := True;
end;

procedure TMatrixAxisCollection.LockMatrix;
begin
  Inc(fMatrixLock);
end;

procedure TMatrixAxisCollection.LoadFromFile(const aFileName: string);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFilename, fmOpenRead);
  try
    LoadFromStream(F);
  finally
    F.Free;
  end;
end;

procedure TMatrixAxisCollection.LoadFromStream(S: TStream);
var
  Wrapper: TAxisCollectionWrapper;
begin
  fMatrix.PaintLock;//LayOutBegin;
  try
    Wrapper := TAxisCollectionWrapper.Create(nil);
    try
      Wrapper.AxisCollection := fMatrix.DoCreateColumns;
      S.ReadComponent(Wrapper);
      Assign(Wrapper.AxisCollection);
    finally
      Wrapper.AxisCollection.Free;
      Wrapper.Free;
    end;
  finally
    //fMatrix.LayoutUnlock(False);
    fMatrix.PaintUnlock;//LayoutEnd;
    Changed;
  end;
end;

function TMatrixAxisCollection.MatrixLocked: Boolean;
begin
  Result := fMatrixLock > 0;
end;

procedure TMatrixAxisCollection.SaveToFile(const aFileName: string);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFilename, fmCreate);
  try
    SaveToStream(F);
  finally
    F.Free;
  end;
end;

procedure TMatrixAxisCollection.SaveToStream(S: TStream);
var
  Wrapper: TAxisCollectionWrapper;
begin
  Wrapper := TAxisCollectionWrapper.Create(nil);
  try
    Wrapper.AxisCollection := Self;
    S.WriteComponent(Wrapper);
    //ComponentToTextFile(Wrapper, 'kanweg.txt'); dbgrids
  finally
    Wrapper.Free;
  end;
end;

procedure TMatrixAxisCollection.SetAutoGenerated(const Value: Boolean);
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
    with TMatrixAxisItem(Items[i]) do
      AutoGenerated := True;
end;

procedure TMatrixAxisCollection.SetCount(const Value: Integer);
begin
  if Value = Count then
    Exit;
  BeginUpdate;
  try
    while Count > Value do
    begin
      Delete(Count - 1);
    end;
    while Count < Value do
    begin
      InternalAdd;
    end;
  finally
    EndUpdate;
  end;
end;

procedure TMatrixAxisCollection.UnlockMatrix;
begin
  if fMatrixLock > 0 then
    Dec(fMatrixLock);
end;

procedure TMatrixAxisCollection.UpdateIndices;
var
  i: Integer;
begin
  for i := 0 to Count - 1 do
  begin
    TMatrixAxisItem(Items[i]).fIndex := i;
  end;
end;

{ TMatrixOptions }

procedure TMatrixOptions.Changed(aChange: TOptionsChange);
begin
  if Assigned(fOnChange) then
    fOnChange(Self, aChange);
end;

constructor TMatrixOptions.Create(aOwner: TPersistent);
begin
  inherited Create(aOwner);
  fAxisOptions := DEF_AXISOPTIONS;
  fPaintOptions := DEF_PAINTOPTIONS;
  fScrollOptions := DEF_SCROLLOPTIONS;
  fSizingOptions := DEF_SIZINGOPTIONS;
  fEditOptions := DEF_EDITOPTIONS;
end;

destructor TMatrixOptions.Destroy;
begin
  inherited Destroy;
end;

function TMatrixOptions.GetAlwaysUpdateScreen: Boolean;
begin
  Result := soAlwaysUpdateScreen in fScrollOptions;
end;

function TMatrixOptions.GetAutoSizeColumns: Boolean;
begin
  Result := aoAutoSizeColumns in fAutoOptions;
end;

function TMatrixOptions.GetCanEdit: Boolean;
begin
  Result := eoCanEdit in fEditOptions;
end;

function TMatrixOptions.GetColSizing: Boolean;
begin
  Result := soColSizing in fSizingOptions;
end;

function TMatrixOptions.GetDownAfterEdit: Boolean;
begin
  Result := eoDownAfterEdit in fEditOptions;
end;

function TMatrixOptions.GetHeaderSizingOnly: Boolean;
begin
  Result := soHeaderSizingOnly in fSizingOptions;
end;

function TMatrixOptions.GetOwner: TCustomMatrix;
begin
  Result := TCustomMatrix(inherited Owner);
end;

function TMatrixOptions.GetPaintColLines: Boolean;
begin
  Result := poColLines in fPaintOptions;
end;

function TMatrixOptions.GetPaintRowLines: Boolean;
begin
  Result := poRowLines in fPaintOptions;
end;

function TMatrixOptions.GetRowSizing: Boolean;
begin
  Result := soRowSizing in fSizingOptions;
end;

procedure TMatrixOptions.SetAlwaysUpdateScreen(const Value: Boolean);
begin
  if Value then SetScrollOptions(fScrollOptions + [soAlwaysUpdateScreen]) else SetScrollOptions(fScrollOptions - [soAlwaysUpdateScreen]);
end;

procedure TMatrixOptions.SetAutoOptions(const Value: TAutoOptions);
begin
  if fAutoOptions = Value then Exit;
  fAutoOptions := Value;
  Changed(ocAutoOptions);
end;

procedure TMatrixOptions.SetAutoSizeColumns(const Value: Boolean);
begin
  if Value then SetAutoOptions(fAutoOptions + [aoAutoSizeColumns]) else SetAutoOptions(fAutoOptions - [aoAutoSizeColumns]);
end;

procedure TMatrixOptions.SetAxisOptions(const Value: TAxisOptions);
begin
  if fAxisOptions = Value then Exit;
  fAxisOptions := Value;
  Changed(ocAxisOptions);
end;

procedure TMatrixOptions.SetCacheOptions(const Value: TCacheOptions);
begin
  if fCacheOptions = Value then Exit;
  fCacheOptions := Value;
  Changed(ocCacheOptions);
end;

procedure TMatrixOptions.SetCanEdit(const Value: Boolean);
begin
  if Value then SetEditOptions(fEditOptions + [eoCanEdit]) else SetEditOptions(fEditOptions - [eoCanEdit]);
end;

procedure TMatrixOptions.SetColSizing(const Value: Boolean);
begin
  if Value then SetSizingOptions(fSizingOptions + [soColSizing]) else SetSizingOptions(fSizingOptions - [soColSizing])
end;

procedure TMatrixOptions.SetDownAfterEdit(const Value: Boolean);
begin
  if Value then SetEditOptions(fEditOptions + [eoDownAfterEdit]) else SetEditOptions(fEditOptions - [eoDownAfterEdit]);
end;

procedure TMatrixOptions.SetEditOptions(const Value: TEditOptions);
begin
  if fEditOptions = Value then Exit;
  fEditOptions := Value;
  Changed(ocEditOptions);
end;

procedure TMatrixOptions.SetFunctionalOptions(const Value: TFunctionalOptions);
begin
  if fFunctionalOptions = Value then Exit;
  fFunctionalOptions := Value;
  Changed(ocFunctionalOptions);
end;

procedure TMatrixOptions.SetHeaderSizingOnly(const Value: Boolean);
begin
  if Value then SetSizingOptions(fSizingOptions + [soHeaderSizingOnly]) else SetSizingOptions(fSizingOptions - [soHeaderSizingOnly]);
end;

procedure TMatrixOptions.SetPaintColLines(const Value: Boolean);
begin
  if Value then SetPaintOptions(fPaintOptions + [poColLines]) else SetPaintOptions(fPaintOptions - [poColLines])
end;

procedure TMatrixOptions.SetPaintOptions(const Value: TPaintOptions);
begin
  if fPaintOptions = Value then Exit;
  fPaintOptions := Value;
  Changed(ocPaintOptions);
//  Owner.Invalidate; //#slimmer
end;

procedure TMatrixOptions.SetPaintRowLines(const Value: Boolean);
begin
  if Value then SetPaintOptions(fPaintOptions + [poRowLines]) else SetPaintOptions(fPaintOptions - [poRowLines])
end;

procedure TMatrixOptions.SetRowSizing(const Value: Boolean);
begin
  if Value then SetSizingOptions(fSizingOptions + [soRowSizing]) else SetSizingOptions(fSizingOptions - [soRowSizing]);
end;

procedure TMatrixOptions.SetScrollOptions(const Value: TScrollOptions);
begin
  fScrollOptions := Value;
end;

procedure TMatrixOptions.SetSizingOptions(const Value: TSizingOptions);
begin
  fSizingOptions := Value;
end;

{ TCustomMatrix }

(*procedure TCustomMatrix.AdjustDynamicMethods;
var
  O: TAxisOptions;
begin
  O := Options.AxisOptions;
  if O = [] then
    DynGetCellStyle := DynGetCellStyle_Default
  else if O = [aoUseColumns] then
    DynGetCellStyle := DynGetCellStyle_UseCols;
end;*)

procedure TCustomMatrix.AxisItemIndexChanged(Sender: TMatrixAxisItem; OldIndex, NewIndex: Integer);
begin
  if Sender is TMatrixColumn then
  begin
    fColInfo.Move(OldIndex, NewIndex);
  end;
end;

(*function TCustomMatrix.BottomReached(const aVert: TAxisInfo): Boolean;
begin
  Result := (aVert.LastVisible >= fRowCount) and
            (aVert.GridExtent < fClientHeight - 1);
end;*)

procedure TCustomMatrix.CacheClientSize;
begin
  fClientHeight := ClientHeight;
  fClientWidth := ClientWidth;
end;

{*******************************************************************************

  CalcHorizontalInfo:
    o Vult <aInfo> voor een visuele pagina vanaf kolom <aLeft>
    o verborgen kolommen komen niet in Indices terecht

*******************************************************************************}

procedure TCustomMatrix.CalcHorizontalInfo(var aInfo: TAxisInfo; aLeft: Integer = -1);
var
  x, cx, W: Integer;
begin
  if aLeft < 0 then aLeft := fTopLeft.x;
  W := 0;
  with aInfo do
  begin
    Indices := nil; // laat Delphi de array opschonen
    ZeroMemory(@aInfo, SizeOf(TAxisInfo));
    OffSet := aLeft;
    for x := 0 to fColCount - 1 do
    begin
      cx := x;
      if cx >= fFixedCols then cx := x + aLeft - fFixedCols;
      if not HiddenCols[cx] then //#hidden
      begin
        if x = 0 then FirstVisible := cx;
        Inc(W, GetColWidth(cx) + GetEffectiveColLineWidth(cx));
        Inc(VisibleCount);
        PutInteger(Indices, VisibleCount - 1, cx);
      end
      else Inc(HiddenCount);
      if (W >= fClientWidth) or (cx >= fColCount - 1) then
      begin
        LastVisible := cx;
        Break;
      end;
    end;
    GridExtent         := Min(W - 1, fClientWidth - 1);
    TotalCount         := VisibleCount + HiddenCount;
    FullVisibleCount   := VisibleCount - Ord(W > fClientWidth);
    LastFullVisible    := LastVisible - Ord(FullVisibleCount <> VisibleCount);
  end;

end;

(*function TCustomMatrix.CalcNewTop(aReason: TScrollReason; aCommand: TScrollCommand; YParam: Integer = 0): Integer;
var
  NewTop: Integer;

    { berekent de beste top wanneer het einde van het grid is bereikt }
    function DoRestrict: Integer;
    var
      Vert: TAxisInfo;
      {i, }Top: Integer;
      {$ifdef assertions on}
      Check: Integer;
      {$endif}
    begin
      {$ifdef assertions on}
      Check := 0;
      {$endif}
      Top := NewTop;
      Result := Top;
      CalcVerticalInfo(Vert, Top);
      if (fRowCount > Vert.VisibleCount) and (Vert.GridExtent < fClientHeight - 1) then
      begin
        repeat
          {$ifdef assertions on}
          Inc(Check);
          Assert(Check < 200, ClassName + '.CalcNewTop: oneindige loop');
          {$endif}
          CalcVerticalInfo(Vert, Top);
          if Vert.GridExtent >= fClientHeight - 1 then
            Break;
          Result := Top;
          Dec(Top);
          if Top = 0 then
            Break;
        until False;
      end;
    end;

begin
  case aCommand of
    scDelta: NewTop := fTopLeft.y + YParam;
    scAbsolute: NewTop := YParam;
  end;

  Restrict(NewTop, 0, fRowCount - 1);
  Result := DoRestrict;
end; *)

(*
procedure TCustomMatrix.CalcPageDown(var aNewInfo: TAxisInfo);
var
  Vert: TAxisInfo;
  aTop{, aBottom}: Integer;
begin
  CalcVerticalInfo(Vert);
  aTop := Vert.LastVisible + 1;
  RestrictTop(aTop);
  CalcVerticalInfo(aNewInfo, aTop);
end;
*)

{*******************************************************************************

  CalcPageHorz berekent een horizontale pagescroll (als nodig)

*******************************************************************************}

procedure TCustomMatrix.CalcPageHorz(out aHorz: TAxisInfo; aNewCol: Integer; aPosition: THorizontalFocusPosition);
var
  PgInfo: TPageInfo;
  NewLeft: Integer;
  DeltaX: Integer;

begin
//  Assert(ValidCol(aNewCol), 'calcpagehorz ' + i2s(aNewCol) + ' is een ongeldige kolom');
  CalcPageInfo(PgInfo);

  { de col is zichtbaar }
  if ColVisible(aNewCol, PgInfo.Horz) then
  begin
    aHorz := PgInfo.Horz;
    Exit;
  end;

  DeltaX := aNewCol - fCol;
  if aPosition = hfpAuto then
    begin
      with PgInfo.Horz do
      begin
        if aNewCol < FirstVisible then
        begin
          if DeltaX > 0 then
          begin
            aPosition := hfpLeft;
          end
          else begin
            aPosition := hfpRight;
          end;
        end
        else if aNewCol > LastVisible then
        begin
          if DeltaX > 0 then
          begin
            aPosition := hfpRight;
          end
          else begin
            aPosition := hfpLeft;
          end;
        end;
      end;
    end;

  { de rij is niet zichtbaar }
  case aPosition of
    hfpLeft:
      begin
        NewLeft := aNewCol;
        CalcHorizontalInfo(aHorz, NewLeft);
      end;
    hfpCenter:
      begin
      end;
    hfpRight:
      begin
        NewLeft := aNewCol;
        repeat
          CalcHorizontalInfo(aHorz, NewLeft);
          if aHorz.LastVisible = aNewCol then
            Break;
          if aHorz.LastVisible < aNewCol then
          begin
            Inc(NewLeft);
            CalcHorizontalInfo(aHorz, NewLeft);
            Break;
          end;
          Dec(NewLeft);
          if RestrictLeft(NewLeft) then
            Break;
        until False;
      end;
  end;
end;

procedure TCustomMatrix.CalcPageInfo(var aPageInfo: TPageInfo; aLeft: Integer = -1; aTop: Integer = -1; ForceRecalc: Boolean = False);
begin
  if aLeft < 0 then aLeft := fTopLeft.x;
  if aTop < 0 then aTop := fTopLeft.y;

  if not ForceRecalc and fPageInfoCache.Valid and (aLeft = fTopLeft.x) and (aTop = fTopLeft.y) then
  begin
    aPageInfo := fPageInfoCache.PageInfo;
    Exit;
  end;

  CalcVerticalInfo(aPageInfo.Vert, aTop);
  CalcHorizontalInfo(aPageInfo.Horz, aLeft);

  with fPageInfoCache do
//  if (aLeft = fTopLeft.x) and (aTop = fTopLeft.y) then
  begin
    PageInfo := aPageInfo;
    Valid := True;
    {$ifdef UseMatrixInspector}
    if fInspector <> nil then fInspector.PageInfoChanged(PageInfo);
    {$endif}
  end;
end;

(*
procedure TCustomMatrix.CalcPageUp(var aVert: TAxisInfo);
var
  aTop: Integer;
begin
  aTop := fTopLeft.Y - 1;
  if RestrictTop(aTop) then
  begin
    CalcVerticalInfo(aVert, aTop);
    Exit;
  end;

  repeat
    CalcVerticalInfo(aVert, aTop);
    Dec(aTop);
    if RestrictTop(aTop) then
      Break;
    if aVert.LastVisible <= fTopLeft.Y then
      Break;
  until False;
end;
*)

{*******************************************************************************

  CalcPageVertical berekent een vertikale pagescroll (als nodig)

*******************************************************************************}

procedure TCustomMatrix.CalcPageVert(out aVert: TAxisInfo; aNewRow: Integer; aPosition: TVerticalFocusPosition; Force: Boolean = False);
var
  PgInfo: TPageInfo;
  NewTop: Integer;
  DeltaY: Integer;

begin
//  Assert(ValidRow(aNewRow), 'CalcPageVert ' + i2s(aNewRow) + ' is een ongeldige rij');
  CalcPageInfo(PgInfo);

  { de rij is zichtbaar }
  if RowVisible(aNewRow, PgInfo.Vert) then
  begin
    aVert := PgInfo.Vert;
    Exit;
  end;

  { de rij is niet zichtbaar }
  DeltaY := aNewRow - fRow;
  if aPosition = vfpAuto then
    begin
      with PgInfo.Vert do
      begin
        if aNewRow < FirstVisible then
        begin
          if DeltaY > 0 then
          begin
            aPosition := vfpTop;
          end
          else begin
            aPosition := vfpBottom;
          end;
        end
        else if aNewRow > LastVisible then
        begin
          if DeltaY > 0 then
          begin
            aPosition := vfpBottom;
          end
          else begin
            aPosition := vfpTop;
          end;
        end;
      end;
    end;

  case aPosition of
    vfpTop:
      begin
        NewTop := aNewRow;
        CalcVerticalInfo(aVert, NewTop);
      end;
    vfpCenter:
      begin
      end;
    vfpBottom:
      begin
        { zet top op nieuwe cel en scroll virtueel omhoog totdat de nieuwe cel zo laag mogelijk staat }
        NewTop := aNewRow;
        repeat
          CalcVerticalInfo(aVert, NewTop);
          if aVert.LastVisible = aNewRow then
            Break;
          if aVert.LastVisible < aNewRow then
          begin
            Inc(NewTop);
            CalcVerticalInfo(aVert, NewTop);
            Break;
          end;
          Dec(NewTop);
          if RestrictTop(NewTop) then
            Break;
        until False;
      end;
  end;
end;

procedure TCustomMatrix.CalcVerticalInfo(var aInfo: TAxisInfo; aTop: Integer = -1);
var
  y, cy, H: Integer;
begin
  if aTop < 0 then aTop := fTopLeft.y;
//  Assert
  H := 0;
  with aInfo do
  begin
    { laat Delphi de array opschonen }
    Indices := nil;
    ZeroMemory(@aInfo, SizeOf(TAxisInfo));
    Flags := AXISINFOFLAG_VERTICAL;
    OffSet := aTop;
    for y := 0 to fRowCount - 1 do
    begin
      cy := y;

      { zijn we voorbij fixedrows? }
      if cy >= fFixedRows then cy := y + aTop - fFixedRows;
      { voeg zichtbare rij toe aan array }
      PutInteger(Indices, y, cy);
      { zet eerste zichtbare rij }
      if y = 0 then FirstVisible := cy;
      { houdt hoogte bij }
      Inc(H, GetRowHeight(cy) + GetEffectiveRowLineWidth(cy));
      Inc(VisibleCount);
      { als we bijna out of range zijn stoppen we ermee }
      if (H >= fClientHeight) or (cy >= fRowCount - 1) then
      begin
        LastVisible := cy;
        Break;
      end;
    end;

    GridExtent         := Min(H - 1, fClientHeight - 1);
    TotalCount         := VisibleCount + HiddenCount; 
    FullVisibleCount   := VisibleCount - Ord(H > fClientHeight);
    LastFullVisible    := LastVisible - Ord(FullVisibleCount <> VisibleCount);
  end;

end;

function TCustomMatrix.CalcVisibleDataRows: Integer;
begin
  Result := CalcVisibleRows - DataTopRow;
  if Result < 0 then Result := 0;
end;

function TCustomMatrix.CalcVisibleRows: Integer;
var
  P: TPageInfo;
begin
  CalcPageInfo(P);
  Result := P.Vert.VisibleCount;
end;

function TCustomMatrix.CanEditCell(aCol, aRow: Integer): Boolean;
begin
  Result := False;
  // test readonly
  if not Options.CanEdit then
    Exit;
  // test column readonly
  if UsingColumns then
    if fColumns[aCol].ReadOnly then
      Exit;
  Result := True;    
end;

function TCustomMatrix.CanPaint: Boolean;
begin
  Result := HandleAllocated and not PaintLocked;
end;

function TCustomMatrix.CellRectToScreenRect(aCol, aRow: integer; var aRect: TRect): Boolean;
var
  PgInfo: TPageInfo;

  procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer;
    var Stop: Boolean); far;
  begin
    aRect := aCellRect;
  end;

begin
  aRect := ErrorRect;
  CalcPageInfo(PgInfo);
  TraverseVisibleCells(@DoCell, PgInfo, Rect(aCol, aRow, aCol, aRow));
  Result := aRect.Left >= 0;
end;

(*function TCustomMatrix.CellVisible(aCol, aRow: integer; const aPageInfo: TPageInfo): Boolean;
begin
  Result := ColVisible(aCol, aPageInfo.Horz) and RowVisible(aRow, aPageInfo.Vert);
end;
*)

procedure TCustomMatrix.CheckCol(aCol: Integer; const Proc: string = '');
begin
  if not ValidCol(aCol) then
    Error(IIF(Proc <> '', Proc + ': ', '') + 'CheckCol %d', [aCol]);
end;

procedure TCustomMatrix.CheckCoord(aCol, aRow: Integer; const Proc: string = '');
begin
  CheckCol(aCol, Proc);
  CheckRow(aRow, Proc);
//  if not ValidCol(aCol) then Error(IIF(Proc <> '', Proc + ': ', '') + 'CheckCol %d', [aCol]);
//  if not ValidRow(aRow) then Error(IIF(Proc <> '', Proc + ': ', '') + 'CheckRow %d', [aRow]);
end;

procedure TCustomMatrix.CheckCoord(aCoord: TPoint; const Proc: string = '');
begin
  CheckCoord(aCoord.x, aCoord.y);
//  CheckRow(aC
//  if not ValidCol(aCoord.x) then Error(IIF(Proc <> '', Proc + ': ', '') + 'CheckCol %d', [aCoord.x]);
//  if not ValidRow(aCoord.y) then Error(IIF(Proc <> '', Proc + ': ', '') + 'CheckRow %d', [aCoord.y]);
end;

procedure TCustomMatrix.CheckRow(aRow: Integer; const Proc: string = '');
begin
  if not ValidRow(aRow) then
    Error(IIF(Proc <> '', Proc + ': ', '') + 'CheckRow %d', [aRow]);
end;

procedure TCustomMatrix.CMBorderChanged(var Message: TMessage);
begin
  inherited;
  CacheClientSize;
//  recreatewnd;
end;

procedure TCustomMatrix.CMExit(var Message: TMessage);
begin
  InternalStateEnter(icKillingFocus);
  try
    inherited;
  finally
    InternalStateLeave(icKillingFocus);
  end;
end;

procedure TCustomMatrix.CMFontChanged(var Message: TMessage);
begin
  Style.Font := Font;
  HeaderStyle.Font := Font;
  inherited;
end;

procedure TCustomMatrix.CMMouseLeave(var Message: TMessage);
begin
  inherited;
end;

procedure TCustomMatrix.ColumnPropertyChanged(Item: TMatrixColumn);
begin
  InvalidateCol(Item.Index);
end;

procedure TCustomMatrix.ColumnsChanged(Item: TCollectionItem);
//var
  //i: Integer;
begin
  {-------------------------------------------------------------------------------
    voorkom recursie
  -------------------------------------------------------------------------------}
  if (Item = nil) and (fColCount <> Columns.Count) then
  begin
    SetColCount(Columns.Count);
  end;
  Invalidate;
end;

function TCustomMatrix.ColVisible(aCol: Integer; const HorzInfo: TAxisInfo): Boolean;
begin
  Assert(HorzInfo.Flags and AXISINFOFLAG_VERTICAL = 0, 'ColVisible');
  with HorzInfo do
    Result :=
      (VisibleCount > 0) and
      ExistInteger(Indices, aCol, VisibleCount - 1)
end;

procedure TCustomMatrix.CopyCell(aCol, aRow: Integer; aBitMap: TBitMap);
var
  P: TPaintInfo;
begin
  with P do
  begin
    TargetCanvas := aBitmap.Canvas;

  end;
//  PaintCell(aCol, aROw, GetCellRectVirtual(aCol, aRow), P);
end;

constructor TCustomMatrix.Create(aOwner: TComponent);
const
  GridStyle = [csCaptureMouse, csOpaque, csDoubleClicks];
begin
  InitializeGlobals;
  UsesBitmap; // InitializeGlobals

  inherited Create(aOwner);


//  fMgr := TMessageHandler.Create;stdctrls  db
//  DoMsg;
  Ctl3D := False;


  { TCustomControl }
  if NewStyleControls then ControlStyle := GridStyle else ControlStyle := GridStyle + [csFramed];
  ControlStyle := ControlStyle + [csReplicatable];
  ParentColor := False;
  Color := clWhite;
  TabStop := True;
(*
  Bits := @LineBitsDotted;
  LinePatternBitmap := CreateBitmap(8, 8, 1, 1, Bits);
  FDotBrush := CreatePatternBrush(LinePatternBitmap); //windlg([fdotbrush]);
  DeleteObject(LinePatternBitmap); *)

  { simple }
  fDefaultColWidth := DEF_COLWIDTH;
  fDefaultRowHeight := DEF_ROWHEIGHT;
  //fColCount := DEF_COLCOUNT;
  fRowCount := DEF_ROWCOUNT;
  fDefaultLineColor := DEF_LINECOLOR;
  fDefaultHeaderLineColor := DEF_HEADERLINECOLOR;
  fDefaultLineWidth := DEF_LINEWIDTH;
  fSortCol := -1;
  // fActiveHyperLink := ErrorCell;

  { objecten }
  fOptions := TMatrixOptions.Create(Self);
  fOptions.fOnChange := OptionsChanged;

  // fBackGround := TPicture.Create;
  fStyle := TCellStyle.Create(Self, False);
  fHeaderStyle := TCellStyle.Create(Self, True);
  fStyle.OnChange := StyleChanged;
  fHeaderStyle.OnChange := StyleChanged;

//  fStyle.Font := Self.Font;

  fColInfo := TInfoList.Create('ColInfo');
  fRowInfo := TInfoList.Create('RowInfo');

  fColumns := DoCreateColumns;

  Height := 188;
  Width := 380;

  fClientWidth := Width;
  fClientHeight := Height;

  { interne events }
  fStyle.OnChange := StyleChanged;
  fHeaderStyle.OnChange := StyleChanged;


  SetColCount(DEF_COLCOUNT);


  fColInfo.DefaultSize := fDefaultColWidth;
  fColInfo.DefaultLineSize := fDefaultLineWidth;
  fColInfo.Count := fColCount;
  fColInfo.ResetAllLineWidths;
  fColInfo.ResetAllSizes;

  fRowInfo.DefaultSize := fDefaultRowHeight;
  fRowInfo.DefaultLineSize := fDefaultLineWidth;
  fRowInfo.Count := fRowCount;
  fRowInfo.ResetAllLineWidths;
  fRowInfo.ResetAllSizes;

  InvalidatePageInfo;
end;

procedure TCustomMatrix.CreateParams(var Params: TCreateParams);
begin
  inherited;
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style {or WS_THICKFRAME} or WS_CLIPCHILDREN or WS_CLIPSIBLINGS or WS_TABSTOP;
//    if fScrollBars in [ssVertical, ssBoth] then Style := Style or WS_VSCROLL;
//    if fScrollBars in [ssHorizontal, ssBoth] then Style := Style or WS_HSCROLL;
    WindowClass.style := CS_DBLCLKS;

    //Style := Style or (WS_CAPTION or WS_THICKFRAME);
    case fBorderStyle of
      bsNone:
        begin
          //Style := Style or WS_BORDER;
        end;
      bsSingle:
        begin
          Style := Style or WS_BORDER;
          if FBorderStyle = bsSingle then
            if NewStyleControls and Ctl3D then
            begin
              Style := Style and not WS_BORDER;
              ExStyle := ExStyle or WS_EX_CLIENTEDGE;
              //if moAcceptFiles in fOptions then
              //ExStyle := ExStyle or WS_EX_ACCEPTFILES;
            end
            else
              Style := Style or WS_BORDER;

        end;
    end;

//    if fBorderStyle = bsSizeable then
  //    Style := Style or WS_THICKFRAME;
//   Style := Style or WS_BORDER;

  end;
(*GRIDS
  inherited CreateParams(Params);
  with Params do
  begin
    Style := Style or WS_TABSTOP;
    if FScrollBars in [ssVertical, ssBoth] then Style := Style or WS_VSCROLL;
    if FScrollBars in [ssHorizontal, ssBoth] then Style := Style or WS_HSCROLL;
    WindowClass.style := CS_DBLCLKS;
    if FBorderStyle = bsSingle then
      if NewStyleControls and Ctl3D then
      begin
        Style := Style and not WS_BORDER;
        ExStyle := ExStyle or WS_EX_CLIENTEDGE;
      end
      else
        Style := Style or WS_BORDER;
  end; *)


end;

procedure TCustomMatrix.CreateWnd;
begin
  inherited;
  CacheClientSize;
end;

function TCustomMatrix.DataTopRow: Integer;
begin
  Result := 0;
end;

procedure TCustomMatrix.DefineProperties(Filer: TFiler);

  function DoColInfo: Boolean;
  //var
    //i: Integer;
  begin
    Result := True;
    //Result := (fColWidths <> nil) and (mcoCacheColWidths in fCacheOptions)
    //and not AllDefault(fColWidths, fDefaultColWidth);
  end;

  function DoRowInfo: Boolean;
  begin
    Result := True;
    //Result := (fRowHeights <> nil) and (mcoCacheRowHeights in fCacheOptions)
    //and not AllDefault(fRowHeights, fDefaultRowHeight);
  end;


begin
  inherited DefineProperties(Filer);

  with Filer do
  begin
//    DefineProperty('ColInfo', ReadColInfo, WriteColInfo, DoColInfo);
//    DefineProperty('ColLines', ReadColLineWidths, WriteColLineWidths, DoColLineWidths);
    //DefineProperty('RowHeights', ReadRowHeights, WriteRowHeights, DoRowHeights);
//    DefineProperty('RowLineWidths', ReadRowHeights, WriteRowHeights, DoRowLineWidths);
{    if fAxisState = masColumns then
      Filer.DefineProperty('Columns', ReadColumns, WriteColumns, True); }
  end;


end;

destructor TCustomMatrix.Destroy;
begin
  fPaintLock := 1;
  fEditLink := nil;
  Destroying;
  TimerStop(CURSOR_TIMER);
  TimerStop(SEARCH_TIMER);
  TimerStop(SCROLL_TIMER);
  {$ifdef UseMatrixInspector}
  fInspector := nil;
  {$endif}
  fColumns.Free;
  fOptions.Free;
//  fBackGround.Free;
  fStyle.Free;
  fHeaderStyle.Free;
  fColInfo.Free;
  fRowInfo.Free;
//  fChangeList.Free;
//  fLineCache := nil;
  inherited Destroy;
  ReleaseBitmap;
end;

{procedure TCustomMatrix.DirectSetColCOunt(aCount: Integer);
begin
  fColCount := aCount;
  fColInfo.Count := fColCount;
end;}

procedure TCustomMatrix.DoAfterFocusCell(aCol, aRow: Integer);
begin
  if Assigned(fAfterFocusCell) then
    fAfterFocusCell(Self, aCol, aRow);//
end;

procedure TCustomMatrix.DoAfterPreparePaintCell(aCol, aRow: Integer; var CellParams: TCellParams);
begin
  //
end;

procedure TCustomMatrix.DoAutoSizeColumns;
begin
  //
end;

procedure TCustomMatrix.DoAutoSizeParentForm;
var
  F: TCustomForm;
  A: TAlign;
begin
  if not (csDesigning in ComponentState) then
  begin


    A := Align;
    Align := alNone;

    F := GetParentForm(Self);
    if F <> nil then
      F.Perform(CM_AUTOSIZE, 0, 0);

    Align := A;

  end;
end;

procedure TCustomMatrix.DoBeforePaintCell(aCol, aRow: Integer; var Params: TCellParams);
begin
  if Assigned(fBeforePaintCell) then fBeforePaintCell(Self, aCol, aRow, Params);
end;

procedure TCustomMatrix.DoCellClick(aCol, aRow: Integer);
begin
  if Assigned(fOnCellClick) then fOnCellClick(Self, aCol, aRow);
end;

procedure TCustomMatrix.DoCellDoubleClick(aCol, aRow: Integer);
begin
  if Assigned(fOnCellDoubleClick) then fOnCellDoubleClick(Self, aCol, aRow);
end;

procedure TCustomMatrix.DoCellHint(aCol, aRow: Integer);
begin
  //
  if ShowHint then
  begin
    Hint := Text[aCol, aRow];
    Application.CancelHint;//(mouse.cursorpos);
  end;
//  if
end;

function TCustomMatrix.DoCreateColumns: TMatrixColumns;
begin
  Result := TMatrixColumns.Create(Self);
end;

function TCustomMatrix.DoCreateEditor(aCol, aRow: Integer): IMatrixEditLink;
{var
  Inst: IUnknown; }
begin
//  Result := nil;
  { geef app kans een interface te implementeren }
//  if Assigned(fOnCreateEditor) then fOnCreateEditor(Self, aCol, aRow, Result);

//  if Result.QueryInterface(IMatrixInpaceControl) then




  { anders default editor }
  //if Result = nil then
//  case fDataMode of
    Result := TStringEditLink.Create;
//    dmDataSet: Result := TDBStringEditLink.Create;
  //end;

end;

function TCustomMatrix.DoEdit: Boolean;
var
  R: TRect;
  CanEdit: Boolean;
begin
  Result := False;
  if not HandleAllocated then
    Exit;

  if (mxEditing in fStates) then
    Exit;

  if not CanEditCell(fCol, fRow) then
    Exit;
//  if not (eoCanEdit in fOptions.fEditOptions) then
  //  Exit;

//  if fEditLink <> nil then
  //  fEditLink := nil;

//  if

  if fEditLink = nil then
    fEditLink := DoCreateEditor(fCol, fRow);


  if fEditLink <> nil then
  begin

    try
      CanEdit := fEditLink.PrepareEdit(Self, fCol, fRow);
    except
      //CanEdit := False;
      DoEditCancel;
      raise;
    end;

    if CanEdit then//fEditLink.PrepareEdit(Self, fCol, fRow) then
    begin
      StateEnter(mxEditing);
      fEditCol := fCol;
      fEditRow := fRow;
      //fEditLink.SetBounds(GetCellClientRect(fEditCol, fEditRow));
      R := GetCellRect(fEditCol, fEditRow);
//      InflateRect(R, -2, -2);
      fEditLink.SetBounds(R{GetCellRect(fEditCol, fEditRow)});
      fEditLink.BeginEdit;
    end else begin
  //    fEditLink := nil;
//      StateLeave(mxEditing);
      DoEditCancel;
  //    fEditLink := nil;
//      fEditLink.Release;// := nil;
    end;
  end;
end;

procedure TCustomMatrix.DoEditCancel;
begin
//  if not (mxEditing in fStates) then Exit; //and (fEditLink = nil) then Exit;
  if fEditLink = nil then Exit;
//  if not (csDestroying in ComponentState) then


  fEditLink.CancelEdit;
  if mxEditing in fStates then
    StateLeave(mxEditing);
//  SetFocus;
  if not (csDestroying in ComponentState) then
    fEditLink := nil;
end;

function TCustomMatrix.DoEditEnd: Boolean;
begin
  Result := False;

  if not (mxEditing in fStates) then Exit;
  if fEditLink = nil then Exit;


  try
    Result := fEditLink.EndEdit;
  except
    //Result := False;   mxinplace
    raise;
  end;

  StateLeave(mxEditing);


//  if not (icKillingFocus in fInternalStates) then
  //  SetFocus; // #volgorde in samenwerkeing met edits anders

  if not (csDestroying in ComponentState) then
    fEditLink := nil;
end;

procedure TCustomMatrix.DoGetCellData(aCol, aRow: Integer; var Params: TCellParams);
begin
  //
end;

function TCustomMatrix.DoGetCellStyle(aCol, aRow: Integer): TCellStyle;
begin
//  Result := fStyle;

  if aoUseColumns in Options.AxisOptions then
  begin
    if GetHeaderMode(aCol, aRow) = [] then
      Result := fColumns[aCol].fStyle
    else
      Result := fColumns[aCol].fHeaderStyle;
  end
  else begin
    if GetHeaderMode(aCol, aRow) <> [] then
      Result := fHeaderStyle
    else
      Result := fStyle;
  end;


  {  if fCells <> nil then
  begin
  end; }

  {
    als cellstyle van TMatrixCell gezet dan cells[x,y].cell

  }
end;

function TCustomMatrix.DoGetCellText(aCol, aRow: Integer): string;
begin
  Result := i2s(aCol) + ':' + i2s(aRow);
  if Assigned(fOnGetCellText) then
    fOnGetCellText(Self, aCol, aRow, Result);
end;

function TCustomMatrix.DoGetColumnImage(aCol: Integer; var Bitmap: TBitmap): Boolean;
begin
  Result := False;
end;

procedure TCustomMatrix.DoIncrementalSearch(const aSearchString: string);
begin
end;

function TCustomMatrix.DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
  if soMouseWheelClamp in fOptions.fScrollOptions then
    Result := Navigation(srMouseWheel, scDown)
  else
    Result := ScrollTopLeft(srMouseWheel, scDown);
end;

function TCustomMatrix.DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean;
begin
  if soMouseWheelClamp in fOptions.fScrollOptions then
    Result := Navigation(srMouseWheel, scUp)
  else
    Result := ScrollTopLeft(srMouseWheel, scUp);
end;

function TCustomMatrix.DoQuote: Boolean;
var
  Handled: Boolean;
begin

  Result := False;

  if fRow > DataTopRow then
  begin

    if Assigned(fOnQuote) then
    begin
      Handled := False;
      fOnQuote(Self, fCol, fRow, Handled);
      Result := Handled;
      if Handled then
        Exit;
    end;

    Result := True;
    Text[fCol, fRow] := Text[fCol, fRow - 1];
  end;
end;

procedure TCustomMatrix.DoSetCellText(aCol, aRow: Integer; const Value: string);
begin
end;

procedure TCustomMatrix.DoTitleClick(aCol, aRow: Integer; Mode: THeaderMode);
begin
  if Assigned(fOnTitleClick) then fOnTitleClick(Self, aCol, aRow, Mode);
end;

(*
function TCustomMatrix.DynGetCellStyle_Default(aCol, aRow: Integer): TCellStyle;
begin
  Assert(Options.AxisOptions = [], 'DynamicGetCellStyle_Default');
  if GetHeaderMode(aCol, aRow) = [] then Result := fStyle else Result := fHeaderStyle;
end;


function TCustomMatrix.DynGetCellStyle_UseCols(aCol, aRow: Integer): TCellStyle;
begin
  Assert(Options.AxisOptions = [aoUseColumns], 'DynamicGetCellStyle_UseCols');
  if GetHeaderMode(aCol, aRow) = [] then Result := fColumns[aCol].fStyle else Result := fColumns[aCol].fHeaderStyle;
end;
*)

function TCustomMatrix.ExecuteAction(Action: TBasicAction): Boolean;
begin
//  Result := inherited ExecuteAction(Action);
  if Action is THintAction then
  begin
    Result := True;
    //pnlHintDisplay.Caption := THintAction(Action).Hint
  end else Result := inherited ExecuteAction(Action);
end;

procedure TCustomMatrix.FlipTest;
var
  OldColCount: Integer;
//  OldColInfo: TInfoList;
begin

  PaintLock;

//  OldColInfo := TInfoList.Create('templist');

  try
    OldColCount := fColCount;
    SetColCount(fRowCount);
    SetRowCount(OldColCount);

{    OldColInfo.Assign(fColInfo);
    fColInfo.Assign(fRowInfo);
    fRowInfo.Assign(oldColInfo);

    windlg([fcolinfo.count, frowinfo.count]);

    SwapInts(fColCount, fRowCount); }

//    fRowInfo.Assign(OldColInfo);

  finally
//    OldColInfo.Free;
    PaintUnlock;
  end;

end;

(*
function TCustomMatrix.GetBottomRight: TPoint;
var
  P: TPageInfo;
begin
  CalcPageInfo(P);
  Result.X := P.Horz.LastVisible;
  Result.Y := P.Vert.LastVisible;
end;
*)

function TCustomMatrix.GetCellParams(aCol, aRow: Integer): TCellParams;
begin
  CheckCoord(aCol, aRow, 'GetCellParams');
  PaintCellPrepare(aCol, aRow, GetCellRect(aCol, aRow), Result);
end;

function TCustomMatrix.GetCellRect(aCol, aRow: integer): TRect;
begin
  CellRectToScreenRect(aCol, aRow, Result);
end;

function TCustomMatrix.GetCellRectVirtual(aCol, aRow: integer): TRect;
begin
  with Result do
  begin
    Left := 0;
    Top := 0;
    Right := GetColWidth(aCol);
    Bottom := GetRowHeight(aRow);
  end;
end;

function TCustomMatrix.GetColLineWidth(aCol: Integer): Integer;
begin
  Result := fColInfo.Get(aCol)^.LineWidth
end;

function TCustomMatrix.GetColRow: TPoint;
begin
  Result.X := fCol;
  Result.Y := fRow;
end;

function TCustomMatrix.GetColumns: TMatrixColumns;
begin
  if not UsingColumns then
    Error('GetColumns: Options.AxisOptions');
  if not Assigned(fColumns) then
    Error('GetColumns: Columns is nil');
  Result := fColumns;
end;

function TCustomMatrix.GetColWidth(aCol: Integer): Integer;
begin
  Result := fColInfo.Get(aCol)^.Size;
end;

function TCustomMatrix.GetEditing: Boolean;
begin
  Result := mxEditing in fStates;
end;

function TCustomMatrix.GetEffectiveColLineWidth(aCol: Integer): Integer;
begin
  Result := fColInfo.Get(aCol)^.LineWidth * Ord(poColLines in fOptions.fPaintOptions);
end;

(*
function TCustomMatrix.GetEffectiveColWidth(aCol: Integer): Integer;
begin
  Result := fColInfo.Get(aCol)^.Size;
end;

function TCustomMatrix.GetEffectiveRowHeight(aRow: Integer): Integer;
begin
  Result := fRowInfo.Get(aRow)^.Size;
end;
*)

function TCustomMatrix.GetEffectiveRowLineWidth(aRow: Integer): Integer;
begin
  Result := fRowInfo.Get(aRow)^.LineWidth * Ord(poRowLines in fOptions.fPaintOptions);
end;

function TCustomMatrix.GetHeaderMode(aCol, aRow: Integer): THeaderMode;
begin
  Result := [];
  if aCol < fLeftHeaders then
    Include(Result, hmLeft);
  if aRow < fTopHeaders then
    Include(Result, hmTop);
end;

function TCustomMatrix.GetGridExtent: TPoint;
var
  PgInfo: TPageInfo;
begin
  CalcPageInfo(PgInfo);
  Result.x := PgInfo.Horz.GridExtent;
  Result.y := PgInfo.Vert.GridExtent;
end;

function TCustomMatrix.GetHeaderMode(const aCell: TPoint): THeaderMode;
begin
  Result := GetHeaderMode(aCell.X, aCell.Y);
end;

function TCustomMatrix.GetHiddenCol(aCol: Integer): boolean;
begin
  Result := fColInfo.Get(aCol)^.Flags and IR_HIDDEN <> 0;
end;

function TCustomMatrix.GetHighLightString: string;
begin
  Result := fHighlightStrings[0];
end;

function TCustomMatrix.GetHighLightStringByIndex(Index: THighLightRange): string;
begin
  Assert(Between(Index, 0, MAX_HIGHLIGHT_STRINGS - 1), 'GetHighLightStringByIndex');
  Result := fHighLightStrings[Index];
end;

function TCustomMatrix.GetRowHeight(aRow: Integer): Integer;
begin
  Result := fRowInfo.Get(aRow)^.Size;
end;

function TCustomMatrix.GetRowLineWidth(aRow: Integer): Integer;
begin
  Result := fRowInfo.Get(aRow)^.LineWidth;
end;

function TCustomMatrix.GetStyles(IsHeader: Boolean): TCellStyle;
begin
  if not IsHeader then
    Result := fStyle
  else
    Result := fHeaderStyle;
end;

function TCustomMatrix.GetUsingColumns: Boolean;
begin
  Result := aoUseColumns in fOptions.fAxisOptions;
end;

procedure TCustomMatrix.HandleNavigation(var aReason: TScrollReason; aCommand: TScrollCommand; var XParam, YParam: Integer; out Mode: TScrollMode);
begin
  case aCommand of
    scNone:
      begin
        Mode := smAbsolute;
      end;
    scDelta:
      begin
        Mode := smDelta;
      end;
    scAbsolute:
      begin
        Mode := smAbsolute;
      end;
    scRight:
      begin
        Mode := smDelta;
        XParam := 1;
        YParam := 0;
      end;
    scLeft:
      begin
        Mode := smDelta;
        XParam := -1;
        YParam := 0;
      end;
    scDown:
      begin
        Mode := smDelta;
        XParam := 0;
        YParam := 1;
      end;
    scUp:
      begin
        Mode := smDelta;
        XParam := 0;
        YParam := -1;
      end;
    scPgDown:
      begin
//        if TopLeft.
      end;
    scPgUp:
      begin
      end;
    scHorzStart:
      begin
        Mode := smAbsolute;
        XParam := 0;
        YParam := fRow;
      end;
    scHorzEnd:
      begin
        Mode := smAbsolute;
        XParam := fColCount - 1;
        YParam := fRow;
      end;
    scVertStart:
      begin
        Mode := smAbsolute;
        XParam := fCol;
        YParam := 0;
      end;
    scVertEnd:
      begin
        Mode := smAbsolute;
        XParam := fCol;
        YParam := fRowCount - 1;
      end;
    scBegin:
      begin
        Mode := smAbsolute;
        XParam := 0;
        YParam := 0;
      end;
    scEnd:
      begin
        Mode := smAbsolute;
        XParam := fColCount - 1;
        YParam := fRowCount - 1;
      end;
  end;
end;

function TCustomMatrix.HitTest(X, Y: Integer; out aCell: TPoint; out aCellRect: TRect; out aLineHit: TLineHit): Boolean;

    procedure DoTest(aCol, aRow: Integer; const aRect: TRect; HorzLn, VertLn: Integer;
      var Stop: Boolean); far;
    var
      R: TRect;
    begin
      R := aRect;
      Inc(R.Right, VertLn);
      Inc(R.Bottom, HorzLn);
      Stop := PointInRect(X, Y, R);
      { check lijn }
      if Stop then
      begin
        if X > aRect.Right then Include(aLineHit, lhVert);
        if Y > aRect.Bottom then Include(aLineHit, lhHorz);
      end;
      Result := Stop;
      if Result then
      begin
        aCell := Point(aCol, aRow);
        aCellRect := aRect;
      end;
    end;

var
  PgInfo: TPageInfo;
begin
  CalcPageInfo(PgInfo);
  aLineHit := [];
  TraverseVisibleCells(@DoTest, PgInfo, AllCells);
end;

function TCustomMatrix.HitTestResize(X, Y: Integer; out aCellRect: TRect; out aCell: TPoint; out aLineHit: TLineHit): Boolean;

    procedure DoTest(aCol, aRow: Integer; const aRect: TRect; HorzLn, VertLn: Integer;
      var Stop: Boolean); far;
    var
      R: TRect;
    begin
      R := aRect;
      Inc(R.Right, VertLn + 2);
      Inc(R.Bottom, HorzLn + 2);
      Stop := PointInRect(X, Y, R);
      { check lijn }
      if Stop then
      begin
        if Between(X, R.Right - 4 - HorzLn, R.Right) then Include(aLineHit, lhVert);
        if Between(Y, R.Bottom - 4 - VertLn, R.Bottom) then Include(aLineHit, lhHorz);
      end;
      Result := aLineHit <> [];
      if Result then
      begin
        aCellRect := aRect;
        aCell := Point(aCol, aRow);
        //aCellRect := aRect;
      end;
    end;

var
  PgInfo: TPageInfo;
begin
  CalcPageInfo(PgInfo);
  aLineHit := [];
  TraverseVisibleCells(@DoTest, PgInfo, AllCells);
end;

procedure TCustomMatrix.InternalIncrementalSearch(const aSearchString: string);
begin
  DoIncrementalSearch(aSearchString);
end;

procedure TCustomMatrix.InternalSetHighLightString(Index: Integer; const Value: string);
var
  i: Integer;
begin
  Assert(Between(Index, 0, MAX_HIGHLIGHT_STRINGS - 1), 'InternalSetHighLightString');
  if fHighLightStrings[Index] = Value then
    Exit;
  fHighLightStrings[Index] := Value;
  for i := 0 to MAX_HIGHLIGHT_STRINGS - 1 do
    if fHighLightStrings[i] <> '' then
      fHighLightStringsUsed := i + 1;
  Invalidate;               
end;

procedure TCustomMatrix.InternalStateChange(Enter, Leave: TInternalStates);
begin
  fInternalStates := fInternalStates + Enter - Leave;
end;

procedure TCustomMatrix.InternalStateEnter(aState: TInternalState);
begin
  InternalStateChange([aState], []);
end;

procedure TCustomMatrix.InternalStateLeave(aState: TInternalState);
begin
  InternalStateChange([], [aState]);
end;

procedure TCustomMatrix.Invalidate;
begin
  if not CanPaint then Exit;
  InvalidatePageInfo;
  fInternalPaintOptions := ALL_INTERNALPAINTOPTIONS;
  inherited Invalidate;
end;

procedure TCustomMatrix.InvalidateCell(aCol, aRow: Integer; DirectUpdate: Boolean);
var
//  PaintInfo: TPaintInfo;
  R: TRect;

  //PgInfo: TPageInfo;

begin
  if not CanPaint then Exit;
  CheckCoord(aCol, aRow);
  if CellRectToScreenRect(aCol, aRow, R) then
  begin
    { #windows rect shit }
    Inc(R.Right);
    Inc(R.Bottom);
    Include(fInternalPaintOptions, ipPaintCells);
    Windows.InvalidateRect(Handle, @R, False);
    if DirectUpdate then Update;
  end;
end;

procedure TCustomMatrix.InvalidateCol(aCol: Integer; DirectUpdate: Boolean);
var
  PgInfo: TPageInfo;
  TopRect, BottomRect, R: TRect;
begin
  if not CanPaint then Exit;
  CheckCol(aCol);
  CalcPageInfo(PgInfo);
  if  CellRectToScreenRect(aCol, PgInfo.Vert.FirstVisible, TopRect)
  and CellRectToScreenRect(aCol, PgInfo.Vert.LastVisible, BottomRect) then
  begin
    Include(fInternalPaintOptions, ipPaintCells);
    Include(fInternalPaintOptions, ipPaintLines);
    R.TopLeft := TopRect.TopLeft;
    R.BottomRight := BottomRect.BottomRight;
    { #windows rect shit }
    Inc(R.Right);
    Inc(R.Bottom);
    Windows.InvalidateRect(Handle, @R, False);
    if DirectUpdate then Update;
  end;
end;

procedure TCustomMatrix.InvalidateMouseInfo;
begin
  FillChar(fMouseInfo, SizeOf(TMouseInfo), 0);
end;

procedure TCustomMatrix.InvalidatePageInfo;
begin
  fPageInfoCache.Valid := False;
end;

procedure TCustomMatrix.InvalidateRow(aRow: Integer; DirectUpdate: Boolean);
var
  PgInfo: TPageInfo;
  LeftRect, RightRect, R: TRect;
begin
  if not CanPaint then Exit;
  CheckRow(aRow);
  CalcPageInfo(PgInfo);
  if  CellRectToScreenRect(PgInfo.Horz.FirstVisible, aRow, LeftRect)
  and CellRectToScreenRect(PgInfo.Horz.LastVisible, aRow, RightRect) then
  begin
    Include(fInternalPaintOptions, ipPaintCells);
    Include(fInternalPaintOptions, ipPaintLines);
    R.TopLeft := LeftRect.TopLeft;
    R.BottomRight := RightRect.BottomRight;
    { #windows rect shit }
    Inc(R.Right);
    Inc(R.Bottom);
    Windows.InvalidateRect(Handle, @R, False);
    if DirectUpdate then Update;
  end;
end;

function TCustomMatrix.IsActiveControl: Boolean;
var
  H: Hwnd;
  ParentForm: TCustomForm;
begin
  { check active control: kopie Borland grids unit. zie ook mousedown }
  Result := False;
  ParentForm := GetParentForm(Self);
  if Assigned(ParentForm) then
  begin
    if (ParentForm.ActiveControl = Self) then
      Result := True
  end
  else
  begin
    H := GetFocus;
    while IsWindow(H) and (Result = False) do
    begin
      if H = WindowHandle then
        Result := True
      else
        H := GetParent(H);
    end;
  end;
end;

function TCustomMatrix.IsClickableCell(aCol, aRow: Integer): Boolean;
begin
  Result := GetHeaderMode(aCol, aRow) <> [];
end;

procedure TCustomMatrix.KeyDown(var Key: word; Shift: TShiftState);
var
  Command: TScrollCommand;

    procedure DoDelete;
    begin
      if CanEditCell(fCol, fRow) then
        Text[fCol, fRow] := '';
    end;

begin
  {-------------------------------------------------------------------------------
    geef app een kans de onkeydown af te handelen. wanneer app key op 0 zet is de
    keydown afgehandeld
  -------------------------------------------------------------------------------}
  inherited KeyDown(Key, Shift);
  if Key = 0 then Exit;
  Command := scNone;

  {-------------------------------------------------------------------------------
     genereer navigatie commando
  -------------------------------------------------------------------------------}
  if Shift = [] then
  begin
    case Key of
      VK_DOWN   : Command := scDown;
      VK_UP     : Command := scUp;
      VK_RIGHT  : Command := scRight;
      VK_LEFT   : Command := scLeft;
      VK_NEXT   : Command := scPgDown;
      VK_PRIOR  : Command := scPgUp;
      VK_HOME   : Command := scHorzStart;
      VK_END    : Command := scHorzEnd;
//      VK_ESCAPE : DoCancel;
      VK_DELETE : DoDelete;
      VK_F2     : DoEdit;//SetEditing(not Editing);//if not Editing then DoEdit else Editing := False;  mxinplace

    end;
  end
  else if Shift = [ssCtrl] then
  begin
    case Key of
      //VK_ESCAPE : DoCancel;
      //VK_F2     : DoEdit;
{      VK_DOWN   : Command := scDown;
      VK_UP     : Command := scUp;
      VK_RIGHT  : Command := scRight;
      VK_LEFT   : Command := scLeft; }
      VK_NEXT   : Command := scVertEnd;
      VK_PRIOR  : Command := scVertStart;
{      VK_HOME   : Command := scHorzStart; }
      VK_END    : Command := scEnd;
    end
  end
  else if Shift = [ssShift] then
  begin
    case Key of
      VK_RIGHT  : if soColSizing in fOptions.fSizingOptions then
                     SetColWidth(fCol, GetColWidth(fCol) + 1);
      VK_LEFT  : if soColSizing in fOptions.fSizingOptions then
                     SetColWidth(fCol, GetColWidth(fCol) - 1);
    end;
  end;

  if Command <> scNone then
    Navigation(srKeyDown, Command);
end;

procedure TCustomMatrix.KeyPress(var Key: Char);
begin
  {-------------------------------------------------------------------------------
    app event. wanneer key door app op #0 wordt gezet wordt keypress als afgehandeld
    beschouwd.
  -------------------------------------------------------------------------------}
  inherited KeyPress(Key);//controls
  if Key = #0 then Exit;

  if (mxEditing in fStates) then
    Exit;

  if Options.CanEdit then
  begin
    { aanhalen }
(*    if Key in ['"', ''''] then
    begin
      if CanEditCell(fCol, fRow) then
      begin
        if DoQuote then
          Exit;
      end;
    end; *)

    fEditChar := Key;   //umisc
    { edit cell }
    if (Key in EditChars) and CanEditCell(fCol, fRow) and (eoStartOnChar in fOptions.fEditOptions) then
    begin
      StateEnter(mxEditChar);
      try
        DoEdit;
        Exit;
      finally
        StateLeave(mxEditChar);
      end;
    end;

  end;

  if Key in SearchChars then
    UpdateIncrementalSearch(Key);

end;

procedure TCustomMatrix.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Cell: TPoint;
  CellRect: TRect;
  LineHit: TLineHit;
  CellHit: Boolean;
begin
  { focus matrix: kopie Borland grids unit }
  if not (csDesigning in ComponentState) and
    (CanFocus or (GetParentForm(Self) = nil)) then
  begin
    SetFocus;
    if not IsActiveControl then
    begin
      MouseCapture := False;
      Exit;
    end;
  end;

(*  CellHit := HitTest(X, Y, HittedCell, R, LineHit);
  if CellHit then
  begin
    Navigation(srMouseClick, scAbsolute, HittedCell.X, HittedCell.Y);
  end;

  inherited; *)




  with fMouseInfo do
  begin

    miDown := Point(X, Y);

    { check resizestate }
    if (ssLeft in Shift) then
    case miState of
      mdColSizingPossible, mdRowSizingPossible, mdCellSizingPossible:
      begin
        TimerStop(CURSOR_TIMER);
        miStartColWidth := ColWidths[miResizeCell.x];
        miStartRowHeight := RowHeights[miResizeCell.y];
        //PaintSizingLines(X, Y, False);
      end;
    end;

    { check hit }
    if not (miState in DragSizingStates) then
    begin

      CellHit := HitTest(X, Y, Cell, CellRect, LineHit);
      if CellHit then begin
        Include(miClickInfo, ciDownCell);
        miDownCell := Cell;
        if IsClickableCell(Cell.x, Cell.y) then
          InvalidateCell(Cell.x, Cell.y);
      end;

      { check selectionrect }
      if Shift = [ssCtrl, ssLeft] then
      begin
        miState := mdRectSelectingStart;
      end;

      if CellHit and (Shift = [ssLeft]) then
      begin
        Navigation(srMouseClick, scAbsolute, Cell.x, Cell.y);
      end;

    end;

  end;

  if ssDouble in Shift then DblClick;

  { inherited event }
  inherited MouseDown(Button, Shift, X, Y);

end;

procedure TCustomMatrix.MouseMove(Shift: TShiftState; X, Y: Integer);
var
//  CellHit: Boolean;
  Cell: TPoint;
//  CellRect: TRect;
//  LineHit: TLineHit;

  ResizeCellHit: Boolean;
  ResizeCellRect: TRect;
  ResizeCell: TPoint;
  ResizeHit: TLinehit;

  CellChanged: Boolean;

  PrevState: TMouseDragState;
  HeaderMode: THeaderMode;
begin
  inherited MouseMove(Shift, X, Y);

  with fMouseInfo do
  begin
    PrevState := miState;
    miMove := Point(X, Y);

    { check hit }
//    CellHit := HitTest(X, Y, Cell, CellRect, LineHit);
    CellChanged := not EqualPoints(miMoveOverCell, Cell);
//    if not EqualPoints(miMoveOverCell, Cell) then
    miMoveOverCell := Cell;

    if CellChanged then
      if ValidCoord(cell.x, cell.y) then
        docellhint(cell.x, cell.y);


    { check resizestate } //#verhuizen naar mousedown
    if (ssLeft in Shift) then
    case miState of
      mdColSizingPossible:
        miState := mdColSizing;
      mdRowSizingPossible:
        miState := mdRowSizing;
      mdCellSizingPossible:
        miState := mdCellSizing;
    end;

{  TShiftState = set of (ssShift, ssAlt, ssCtrl,
    ssLeft, ssRight, ssMiddle, ssDouble); }

    {-------------------------------------------------------------------------------
       bepaal initiele resizestate
    -------------------------------------------------------------------------------}
    if Shift * [ssLeft, ssRight, ssMiddle, ssDouble] = [] then
    if not (miState in [mdColSizing, mdRowSizing, mdCellSizing]) then
    with fOptions do
      if fSizingOptions <> [] then
      begin
        HeaderMode := GetHeaderMode(Cell);
        ResizeCellHit := HitTestResize(X, Y, ResizeCellRect, ResizeCell, ResizeHit);
        if not ResizeCellHit then
        begin
(*          if CellHit and (hmTop in HeaderMode) then
          begin
            //miState := mdColSizingPossible;
          end
          else *)
            miState := mdNone;
        end
        else begin
          if (HeaderMode <> [])
          or ((HeaderMode = []) and not (soHeaderSizingOnly in fSizingOptions)) then
          begin
            if (ResizeHit = [lhVert]) and (soColSizing in fSizingOptions) then
              miState := mdColSizingPossible
            else if (ResizeHit = [lhHorz]) and (soRowSizing in fSizingOptions) then
              miState := mdRowSizingPossible
            else if (ResizeHit = [lhHorz, lhVert]) and (soCellSizing in fSizingOptions) then
              miState := mdCellSizingPossible;
          end
        end;
      end;

(*    if miState in DragSizingStates then
      UpdateHyperLink(ErrorCell, True)
    else
    { hyperlink button niet down }
    if (Shift = []) and (CellHit) and (LineHit = []) and IsHyperLink(Cell.x, Cell.y) then
      UpdateHyperLink(Cell, True)
    else
      UpdateHyperLink(ErrorCell, True); *)


  (*  case miState of
      mdNone:
        begin
        end;
    end; *)

    { zet cursor }
    if PrevState <> miState then //#verhuizen naar mousedown
    case miState of
      mdNone:
        begin
        end;
      mdColSizingPossible:
        begin
          miResizeCell := ResizeCell;
          miResizeRect.Left := ResizeCellRect.Left;
          miResizeRect.Top := ResizeCellRect.Top;
          if GetHeaderMode(ResizeCell) = [] then
            TimerStart(CURSOR_TIMER, 200);
        end;
      mdRowSizingPossible:
        begin
          miResizeCell := ResizeCell;
          miResizeRect.Left := ResizeCellRect.Left;
          miResizeRect.Top := ResizeCellRect.Top;
          if GetHeaderMode(ResizeCell) = [] then
            TimerStart(CURSOR_TIMER, 200);
        end;
      mdCellSizingPossible:
        begin
          miResizeCell := ResizeCell;
          miResizeRect.Left := ResizeCellRect.Left;
          miResizeRect.Top := ResizeCellRect.Top;
          if GetHeaderMode(ResizeCell) = [] then
            TimerStart(CURSOR_TIMER, 200);
        end;
    end;

    case miState of
      mdColSizing:
        begin
          TimerStop(CURSOR_TIMER);
          miResizeRect.Right := miMove.x;
          if soLiveSizing in fOptions.fSizingOptions then
            ColWidths[miResizeCell.x] := miStartColWidth + miMove.x - miDown.x
          else
            PaintSizingLines(X, Y, False);
        end;
      mdRowSizing:
        begin
          TimerStop(CURSOR_TIMER);
          miResizeRect.Bottom := miMove.y;
          if soLiveSizing in fOptions.fSizingOptions then
            RowHeights[miResizeCell.y] := miStartRowHeight + miMove.y - miDown.y
          else
            PaintSizingLines(X, Y, False);
        end;
      mdCellSizing:
        begin
          TimerStop(CURSOR_TIMER);
          miResizeRect.Right := miMove.x;
          miResizeRect.Bottom := miMove.y;
          if soLiveSizing in fOptions.fSizingOptions then
          begin
            ColWidths[miResizeCell.x] := miStartColWidth + miMove.x - miDown.x;
            RowHeights[miResizeCell.y] := miStartRowHeight + miMove.y - miDown.y
          end
          else
            PaintSizingLines(X, Y, False);
        end;
      mdRectSelectingStart, mdRectSelecting:
        begin
          miState := mdRectSelecting;
//        PaintSelectionRect(Rect(miDown.x, miDown.y, miMove.x, miMove.y));
        end;
    end; // case

  //miPrevMove := miMove;
  end;


end;

procedure TCustomMatrix.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  CellHit: Boolean;
  Cell: TPoint;
  CellRect: TRect;
  LineHit: TLineHit;
  HeaderMode: THeaderMode;
//  DeltaX: Integer;
begin
  with fMouseInfo do
  begin
    try
      miUp := Point(X, Y);
      CellHit := HitTest(X, Y, Cell, CellRect, LineHit);

      if (ciDownCell in miClickInfo) and IsClickableCell(miDownCell.x, miDownCell.y) then
      begin
        //Exclude(miClickInfo, ciDownCell);// := Point(-1, -1);
        Exclude(miClickInfo, ciDownCell);
        InvalidateCell(miDownCell.x, miDownCell.y, True);
        Include(miClickInfo, ciDownCell);
//        windlg('');
      end;

      case miState of
        mdNone:
          begin
            if CellHit and (ciDownCell in miClickInfo) then
              if EqualPoints(miDownCell, Cell) then
              begin
                HeaderMode := GetHeaderMode(Cell.x, Cell.y);
                { normale cell }
                if HeaderMode = [] then
                begin
                  DoCellClick(Cell.x, Cell.y)
                end
                { title }
                else if (hmTop in HeaderMode) and (Cell.y = 0) then // ftitlerow
                  DoTitleClick(Cell.x, Cell.y, HeaderMode)
                { andere header }
                //else
                  //DoHeaderClick(Cell.x, Cell.y, HeaderMode);
              end;
          end;
        mdColSizing:
          begin
            if not (soLiveSizing in fOptions.fSizingOptions) then
            begin
              PaintSizingLines(X, Y, True);
              ColWidths[miResizeCell.x] := miStartColWidth + miUp.x - miDown.x;
            end;
          end;
        mdRowSizing:
          begin
            if not (soLiveSizing in fOptions.fSizingOptions) then
            begin
              PaintSizingLines(X, Y, True);
              RowHeights[miResizeCell.y] := miStartRowHeight + miUp.y - miDown.y
            end;
          end;
        mdCellSizing:
          begin
            if not (soLiveSizing in fOptions.fSizingOptions) then
            begin
              PaintSizingLines(X, Y, True);
              ColWidths[miResizeCell.x] := miStartColWidth + miUp.x - miDown.x;
              RowHeights[miResizeCell.y] := miStartRowHeight + miUp.y - miDown.y
            end;


            if [ssCtrl, ssShift] * Shift <> [] then
            begin
              PaintLock;
              SetDefaultColWidth(miStartColWidth + miUp.x - miDown.x);
              SetDefaultRowHeight(miStartRowHeight + miUp.y - miDown.y);
              PaintUnLock;
            end;

          end;
        mdRectSelecting:
          begin
            { hide selectierect }
            //PaintSelectionRect(Rect(miDown.x, miDown.y, miUp.x, miUp.y), True);
          end;
      end;
    finally
      InvalidateMouseInfo;
    end;
  end;

  inherited MouseUp(Button, Shift, X, Y);
// if Cursor <> crDefault then
  // Cursor := crDefault;

//  ReleaseCapture;
end;


function TCustomMatrix.Navigation(aReason: TScrollReason; aCommand: TScrollCommand; XParam: Integer = 0; YParam: Integer = 0): Boolean;
var
  Mode: TScrollMode;
begin
  Result := False;
  DoEditEnd; // misschien moet dit beter. nu stopt edit altijd

  // hier verdere restrict toepassen (verhuizen uit scroll??)
  if aReason = srMouseClick then
  begin
    if YParam < fTopLock then
      Exit;
    if XParam < fLeftLock then
      Exit;
//    RestrictCol(XParam);
//    RestrictRow(YParam);
  end;

  HandleNavigation(aReason, aCommand, XParam, YParam, Mode);
  if Mode <> smDisable then
    Result := Scroll(aReason, Mode, XParam, YParam);
end;

procedure TCustomMatrix.OptionsChanged(Sender: TObject; aChange: TOptionsChange);
begin

//    ocAxisOptions,
//    ocCacheOptions,
//    ocSizingOptions,
//    ocPaintOptions,      db
//    ocEditOptions
   with Options do
   begin

     if aChange = ocFunctionalOptions then
     begin
       DragAcceptFiles(Handle, foAcceptFiles in fOptions.fFunctionalOptions);
     end;

     case aChange of
       ocAutoOptions:
         begin
           if AutoSizeColumns then
             DoAutoSizeColumns;
         end;
     end;

   end;

   Invalidate;
end;

procedure TCustomMatrix.Paint;
var
  Window: TRect;
begin
  // updaterect, canvas, options

//  if not CanPaint then
  //  Exit;
  if not IsRectEmpty(fUpdateRect) then
  begin
    Window := fUpdateRect;
    PaintMatrix(Canvas, Window, []{fInternalPaintOptions});
  end;
end;

procedure TCustomMatrix.PaintBackGround(const aPaintInfo: TPaintInfo);
var
  Org{, Dest, Source}: TRect;
{  W, H: integer;
  x, y: integer;
  DC: HDC;}
begin

  with aPaintInfo, TargetCanvas, PageInfo do
  begin


    begin
      Brush.Color := Self.Color;
      Org := GetClientRect;
      Inc(Org.Right);
      Inc(Org.Bottom);
      FillRect(Org);
      Exit;
    end;

(*    if Vert.GridExtent + 1 < fClientHeight then
    begin
      Brush.Color := Self.Color;
      Rectangle(0, Vert.GridExtent + 1, fClientWidth + 1{#winshit}, fClientHeight + 1{#winshit});
    end; *)
  end;

(*
  W := fBackGround.Width;
  H := fBackGround.Height;

  with aPaintInfo.TargetCanvas do
  begin
    if (W = 0) or (H = 0) then
    begin
      Brush.Color := Self.Color;
      Org := GetClientRect;
      Inc(Org.Right);
      Inc(Org.Bottom);
      FillRect(Org);
      Exit;
    end;

    Org := Rect(0, 0, W, H{W + 1, H + 1}); { ? die verdomde rect-routines! }
    Dest := Org;
    Source := Org;
    y := 0;
    repeat
      if RectsDoIntersect(fUpdateRect, Dest) then
      begin
        CopyRect(Dest, fBackGround.Bitmap.Canvas, Source);
      end;
      { ga naar rechts }
      RectMove(Dest, W, 0);
      { als buiten x-bounds dan ga naar beneden }
      if Dest.Left > fClientWidth then
      begin
        Inc(y);
        Dest := Org;
        RectMove(Dest, 0, H * y);
      end;
      if (Dest.Top > fClientWidth) then Break;
    until False;
  end; *)
end;



procedure TCustomMatrix.PaintCell(aCol, aRow: Integer; var CellParams: TCellParams; const aPaintInfo: TPaintInfo);
var
  CC: TCanvas;
  Flags: DWORD;
  X, Y: Integer;
begin
  if mxEditing in fStates then if fEditCol = aCol then if fEditRow = aRow then Exit;

  with CellParams do
  begin


    CC := CellBitmap.Canvas;
    CC.Lock;


    with CC do
    try


      {-------------------------------------------------------------------------------
        zorg ervoor dat bitmap groot genoeg is
      -------------------------------------------------------------------------------}
      BitmapResize(CellBitmap, pDrawRect.Right, pDrawRect.Bottom);
      Font := pCellStyle.CachedFont;

      DoBeforePaintCell(aCol, aRow, CellParams);
      if not pPainted then
      BEGIN

      {-------------------------------------------------------------------------------
        achtergrond
      -------------------------------------------------------------------------------}
      Brush.Color := pBackColor;
      Brush.Style := bsSolid;
      FillRect(pDrawRect);

      {-------------------------------------------------------------------------------
        header
      -------------------------------------------------------------------------------}
      if [dsTopHeader, dsLeftHeader] * pDrawState <> [] then
        if not pCellStyle.CachedFlat then
        begin
          DrawHeader(CC, pDrawRect, dsHeaderDownClick in pDrawState);
        end;

      {-------------------------------------------------------------------------------
        sorteringsplaatje
      -------------------------------------------------------------------------------}
      if dsSortImage in pDrawState then
      begin
         with MxImages10 do
         begin
           with pDrawImageRect do
           begin
             X := Left + ((Right - Left) - 10) div 2;
             Y := Top + ((Bottom - Top) - 10) div 2;
           end;
           case fSortDirection of
             sdAscending:
               begin
                 Draw(CellBitmap.Canvas, X, Y, Ord(TMx_sort_asc10), True);
               end;
             sdDescending:
               begin
                 Draw(CellBitmap.Canvas, X, Y, Ord(TMX_sort_desc10), True);
               end;
           end;
         end;
      end;

      {-------------------------------------------------------------------------------
        tekst
      -------------------------------------------------------------------------------}
      if pText <> '' then
      begin
        Font.Color := pTextColor;
        SetBkMode(CC.Handle, TRANSPARENT);

        with pCellStyle do
        begin
          Flags := WinFlags[CachedMultiLine, CachedHorzAlign, CachedVertAlign];
          if not (dsHighLightString in pDrawState) then
            DrawTextEx(CC.Handle, PChar(pText), Length(pText), pDrawTextRect, Flags, @pTextParams)
          else                             //ugraphics
            ExtTextOutColored(
              CC, pText, pDrawTextRect, Flags,
              fHighlightStrings,
              [CachedColors[MI_TEXTMATCH], CachedColors[MI_TEXTMATCH2]],
              [CachedColors[MI_BACKMATCH], CachedColors[MI_BACKMATCH2]], []);
        end;
      end;

      END;

      aPaintInfo.TargetCanvas.CopyRect(pCellRect, CC, pDrawRect); //### eric eric toch

    finally
      CC.Unlock;
    end;

  end; // with CellParams

end;

procedure TCustomMatrix.PaintCellPrepare(aCol, aRow: Integer; aRect: TRect; var CellParams: TCellParams);
var
  HeaderMode: THeaderMode;
begin
  with CellParams do
  begin
    {-------------------------------------------------------------------------------
      init
    -------------------------------------------------------------------------------}
    CellParams := EmptyCellParams;
    pFieldValue := Unassigned; // kan niet geinitialiseerd worden

    {-------------------------------------------------------------------------------
      bepaal rects
    -------------------------------------------------------------------------------}
      pCellRect := aRect;
      Inc(pCellRect.Right); // # windows rect shit
      Inc(pCellRect.Bottom); // # windows rect shit
      pDrawRect := ZeroTopLeftRect(pCellRect);
      pDrawTextRect := pDrawRect;
      InflateRect(pDrawTextRect, -2, -2);

    {-------------------------------------------------------------------------------
      bepaal textparams
    -------------------------------------------------------------------------------}
    with pTextParams do
    begin
      cbSize := SizeOf(pTextParams);
      iTabLength := 0;
      iLeftMargin := 0;
      iRightMargin := 0;
    end;

    {-------------------------------------------------------------------------------
      bepaal drawstate
    -------------------------------------------------------------------------------}
    HeaderMode := GetHeaderMode(aCol, aRow);

    if fMatrixFocused then
      Include(pDrawState, dsMatrixFocused);
    if (aCol = fCol) and (aRow = fRow) then
      Include(pDrawState, dsFocused);
    case fSelectMode of
      smRowSelect:
        if (aRow = fRow) and (aCol <> fCol) then
          Include(pDrawState, dsHighlightRow);
      smColRowSelect:
        if ((aRow = fRow) or (aCol = fCol)) and not ((aCol = fCol) and (aRow = fRow)) then
          Include(pDrawState, dsHighlightRow);
    end;
    if hmTop in HeaderMode then
      Include(pDrawState, dsTopHeader);
    if hmLeft in HeaderMode then
      Include(pDrawState, dsLeftHeader);
    if (aCol = fCol) and (aRow = fRow) then
      Include(pDrawState, dsFocused);
    if aRow mod 4 = 0 then
      Include(pDrawState, dsDiffRow);

    {-------------------------------------------------------------------------------
      bepaal of deze cell een aangeklikte header is
    -------------------------------------------------------------------------------}
    if [dsTopHeader, dsLeftHeader] * pDrawState <> [] then
      if (ciDownCell in fMouseInfo.miClickInfo) and (EqualPoints(fMouseInfo.miDownCell, Point(aCol, aRow))) then
      begin
        Include(pDrawState, dsHeaderDownClick);
        RectMove(pDrawTextRect, 1, 1);
      end;

    {-------------------------------------------------------------------------------
      kijk of we een sorteringsplaatje moeten laten zien
    -------------------------------------------------------------------------------}
    if (aRow = 0) and (aCol = fSortCol) and (dsTopHeader in pDrawState) then
    begin
      Include(pDrawState, dsSortImage);
  //    Dec(pDrawTextRect.Right, 10 + 2 + 2);
      pDrawImageRect := SplitRect(pDrawTextRect, 12);
      //pDrawImageRect := Rect(pDrawTextRect.Right + 1, )
//      GlyphInfo.Position := gpRight;
//      GlyphInfo.Distance := 3;
(*      with CalcGlyphPosition(GlyphInfo, 10, 10, BitmapRect{ClientRect}, DummyRect{TxtRect}) do
      begin
         with MxImages10 do
           case fSortDirection of
             sdAscending : Draw(CellBitmap.Canvas, x, y, Ord(TMx_sort_asc10), True);
             sdDescending: Draw(CellBitmap.Canvas, x, y, Ord(TMX_sort_desc10), True);
           end;
        ///Dec(TxtRect.Right, 20);
      end; *)
    end;

    {-------------------------------------------------------------------------------
      haal juiste cellstyle op en cache zijn instellingen voor snellere toegang
    -------------------------------------------------------------------------------}
    pCellStyle := DoGetCellStyle(aCol, aRow);
    pCellStyle.SetCached(True);

    {-------------------------------------------------------------------------------
      bepaal achtergrond- en tekstkleur
    -------------------------------------------------------------------------------}
    pBackColor := GetBkColor(pCellStyle, pDrawState);
    pTextColor := GetTxtColor(pCellStyle, pDrawState);

    {-------------------------------------------------------------------------------
      tekst of data
    -------------------------------------------------------------------------------}
    DoGetCellData(aCol, aRow, CellParams);

    if pText <> '' then
      if fHighLightStringsUsed > 0 then
        Include(pDrawState, dsHighlightString);

    {-------------------------------------------------------------------------------
      canvas
    -------------------------------------------------------------------------------}
    pCanvas := CellBitmap.Canvas;


    {-------------------------------------------------------------------------------
      geef afstammelingen de kans iets extra's te doen
    -------------------------------------------------------------------------------}
    DoAfterPreparePaintCell(aCol, aRow, CellParams);

  end;

end;

procedure TCustomMatrix.PaintLines(const aPaintInfo: TPaintInfo);
var
  R: TRect;
//  ResetColor: Boolean; // kleurenreset bij rijen
  OffsetRect: TRect;
  FirstNormalRow, FirstNormalCol: Integer;
  NormalColLineX, NormalColLineY: Integer;
//  LineCacheIndex: Integer;

  function GetFirstNormalCol: Integer;
  var
    i: integer;
  begin
    with aPaintInfo.PageInfo.Horz do
    begin
      for i := 0 to VisibleCount - 1 do
        if Indices[i] >= fLeftHeaders then
        begin
          Result := Indices[i];
          Exit;
        end;
      Result := FirstVisible; // als niet gevonden
    end;
  end;

  function GetFirstNormalRow: Integer;
  var
    i: integer;
  begin
    with aPaintInfo.PageInfo.Vert do
    begin
      for i := 0 to VisibleCount - 1 do
        if Indices[i] >= fTopHeaders then
        begin
          Result := Indices[i];
          Exit;
        end;
      Result := FirstVisible; // als niet gevonden
    end;
  end;

  procedure DoRowLine(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far;
  begin
    with aPaintInfo, PageInfo, TargetCanvas do
    begin
      R := Rect(0, aBottom + 1, Horz.GridExtent {#windows rect shit}, aBottom + 1 + aLineWidth);
      if R.Top > fUpdateRect.Bottom then Stop := True;
      if aLineWidth > 0 then
      if RectsDoIntersect(R, fUpdateRect) then
      begin
        if aRow < fTopHeaders then
        begin
          {TargetCanvas.}Brush.Color := fDefaultHeaderLineColor;
          FillRect(R)
        end
        else begin
          if NormalColLineX = 0 then
          begin
            {TargetCanvas.}Brush.Color := fDefaultLineColor;
            FillRect(R)
          end else
          begin
            R.Right := NormalColLineX - 1;
            {TargetCanvas.}Brush.Color := fDefaultHeaderLineColor;
            FillRect(R);
            R.Left := NormalColLineX;
            R.Right := Horz.GridExtent;// + 1; {#windows rect shit}
            {TargetCanvas.}Brush.Color := fDefaultLineColor;
            FillRect(R)
          end;
        end;

      end;
    end;
  end;

  procedure DoColLine(aCol, aLeft, aRight, aLineWidth: Integer; var Stop: Boolean); far;
  begin
    with aPaintInfo, PageInfo, TargetCanvas do
    begin
      R := Rect(aRight + 1, 0, aRight + 1 + aLineWidth, Vert.GridExtent{ + 1} {#windows rect shit});
      if aLineWidth > 0 then
      if RectsDoIntersect(R, fUpdateRect) then
      begin
        if aCol < fLeftHeaders then
        begin
          {TargetCanvas.}Brush.Color := fDefaultHeaderLineColor;
          FillRect(R);
        end
        else begin
          if NormalColLineY = 0 then
          begin
            {TargetCanvas.}Brush.Color := fDefaultLineColor;
            FillRect(R)
          end else
          begin
            R.Bottom := NormalColLineY;// - 1;    ###
            {TargetCanvas.}Brush.Color := fDefaultHeaderLineColor;
            FillRect(R);
            R.Top := NormalColLineY;
            R.Bottom := Vert.GridExtent;// + 1; {#windows rect shit}
            {TargetCanvas.}Brush.Color := fDefaultLineColor;
            FillRect(R)
          end;
        end;
      end;
    end;
  end;

begin
  // #optimaliseren, hij doet het nu goed maar een beetje lomp

//  if [poRowLines, poIgnoreRowLines, poIgnoreColLines, poIgnoreRowLines] * fOptions.PaintOptions =
  //  [poRowLines, poIgnoreRowLines, poIgnoreColLines, poIgnoreRowLines] then
    //Exit; //STATEMENT DEUGT?

  if [poRowLines, poColLines] * fOptions.PaintOptions = [] then
    Exit;


  if [poIgnoreRowLines, poIgnoreColLines] * fOptions.PaintOptions = [poIgnoreRowLines, poIgnoreColLines] then
    Exit;

  NormalColLineX := 0;
  NormalColLineY := 0;
//  LineCacheIndex := 0;

  with aPaintInfo, PageInfo do
  begin

    { bepaal horizontale offset van normale horzontale lijnen }
    FirstNormalCol := GetFirstNormalCol;
    if FirstNormalCol > Horz.FirstVisible then
    begin
      OffSetRect := GetCellRect(FirstNormalCol, Vert.FirstVisible);
      NormalColLineX := OffSetRect.Left;
    end;

    { bepaal verticale offset van normale verticale lijnen }
    FirstNormalRow := GetFirstNormalRow;
    if FirstNormalRow > Vert.FirstVisible then
    begin
      OffSetRect := GetCellRect(Horz.FirstVisible, FirstNormalRow);
      NormalColLineY := OffSetRect.Top;
    end;

    { horizontale lijnen (rijen) }
    if [poRowLines, poIgnoreRowLines] * fOptions.PaintOptions <> [] then
      TraverseVisibleRows(@DoRowLine, Vert, AllAxis);

    { vertikale lijnen (kolommen) }
    if [poColLines, poIgnoreColLines] * fOptions.PaintOptions <> [] then
      TraverseVisibleCols(@DoColLine, Horz, AllAxis);
  end;
end;

procedure TCustomMatrix.PaintLock;
begin
  Inc(fPaintLock);
end;

function TCustomMatrix.PaintLocked: Boolean;
begin
  Result := fPaintLock > 0
end;

procedure TCustomMatrix.PaintMatrix(aCanvas: TCanvas; aWindow: TRect; aOptions: TInternalPaintOptions);
var
  PaintInfo: TPaintInfo;
  CellParams: TCellParams;

  procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer;
    var Stop: Boolean); far;
  begin
    if RectsDoIntersect(aCellRect, aWindow) then
    begin
      PaintCellPrepare(aCol, aRow, aCellRect, CellParams);
      PaintCell(aCol, aRow, CellParams, PaintInfo);
    end;
  end;

  procedure DoVirtualColumn;
  begin
    with Canvas do
    begin
      Brush.Color := Self.Color;
      with PaintInfo.PageInfo do
      begin
        { rechts }
        FillRect(Rect(Horz.GridExtent + 1, 0, fClientWidth, Vert.GridExtent + 1));
        { onder }
        FillRect(Rect(0, Vert.GridExtent + 1, fClientWidth, fClientHeight));
      end;
    end;
  end;

begin
  MatrixBitmap.Canvas.Lock;
//  StateEnter(mxPainting);
  try
//    fInternalOptions := aOptions;
//    if fInternalPaintOptions = [] then
//      fInternalPaintOptions := ALL_INTERNALPAINTOPTIONS;
    if aOptions = [] then
      aOptions := ALL_INTERNALPAINTOPTIONS;
    BitmapResize(MatrixBitmap, RectWidth(aWindow), RectHeight(aWindow));
    PaintInfo.TargetCanvas := MatrixBitmap.Canvas;
    CalcPageInfo(PaintInfo.PageInfo);
    if ipPaintBackGround in aOptions then
      PaintBackGround(PaintInfo);
    if ipPaintLines in aOptions then
      PaintLines(PaintInfo);
    if ipPaintCells in aOptions then
      TraverseVisibleCells(@DoCell, PaintInfo.PageInfo, AllCells);
//    DoVirtualColumn;
    aCanvas.CopyRect(aWindow, MatrixBitmap.Canvas, aWindow);
  finally
//    StateLeave(mxPainting);
  //  fInternalPaintOptions := [];
    MatrixBitmap.Canvas.Unlock;
  end;

end;

procedure TCustomMatrix.PaintSizingLines(MouseX, MouseY: Integer; DoHide: Boolean = False);
{ wordt alleen gebruikt bij muis resizing van rij, kolomm of cel}
var
  OldPen: TPen;
begin
  OldPen := TPen.Create;
  with Canvas do
  try
    { save pen van canvas }
    OldPen.Assign(Pen);
    try
    Pen.Color := clBlack;
    Pen.Style := psDot;
    Pen.Mode := pmXor;
    Pen.Width := 1;

    { hide vorige }
    if not EqualRects(fSizingLinesRect, EmptyRect) then
    begin
      with fSizingLinesRect do
      begin
        if Left = Right then // colsizing
        begin
          MoveTo(Left, Top);
          LineTo(Left, Bottom);
        end
        else if Top = Bottom then  // rowsizing
        begin
          MoveTo(Left, Top);
          LineTo(Right, Top);
        end
        else begin
          MoveTo(Left, Top);
          LineTo(Right, Top);
          LineTo(Right, Bottom);
          LineTo(Left, Bottom);
          LineTo(Left, Top);
        end;
      end;
//      Update;
    end;

    { show nieuwe }
    if DoHide then
      fSizingLinesRect := EmptyRect
    else begin
      case fMouseInfo.miState of
        mdColSizing:
          begin
            with fSizingLinesRect do
            begin
              Left := MouseX;  Top := 0;  Right := MouseX;  Bottom := fClientWidth;
              MoveTo(Left, Top);
              LineTo(Left, Bottom);
            end;
          end;
        mdRowSizing:
          begin
            with fSizingLinesRect do
            begin
              Left := 0; Top := MouseY;  Right := fClientWidth;  Bottom := MouseY;
              MoveTo(Left, Top);
              LineTo(Right, Top);
            end;
          end;
        mdCellSizing:
          begin
            fSizingLinesRect := fMouseInfo.miResizeRect;
            with fSizingLinesRect do
            begin
              MoveTo(Left, Top);
              LineTo(Right, Top);
              LineTo(Right, Bottom);
              LineTo(Left, Bottom);
              LineTo(Left, Top);
            end;
          end;
      end;
  //    Update;
    end
    finally
      { zet pen terug }
      Pen.Assign(OldPen);
    end;
  finally
    { free lokale pen }
    OldPen.Free;
  end;

end;

procedure TCustomMatrix.PaintUnlock;
begin
  if fPaintLock > 0 then
  begin
    Dec(fPaintLock);
    if fPaintLock = 0 then
      Invalidate;
  end;
end;

procedure TCustomMatrix.PaintWindow(DC: HDC);
begin
  inherited;
end;

(* WORDT NIET GEBRUIKT
procedure TCustomMatrix.ReadColInfo(Reader: TReader);
begin
  ReadInfo(fColInfo, fColCount, Reader);
end; *)


(* WORDT NIET GEBRUIKT
procedure TCustomMatrix.ReadInfo(InfoList: TInfoList; aCount: Integer; Reader: TReader);
var
  i: Integer;
begin
//  Assert(List <> nil);
//  if List.Count < Count then
  //  Exit;
  if InfoList.Count <> aCount then
    InfoList.Count := aCount;//SetCapacityAndCount(Count, 0);
  with Reader do
  begin
    ReadListBegin;
    for i := 0 to aCount - 1 do
    begin
      InfoList.Get(i)^.Size := ReadInteger;
      InfoList.Get(i)^.LineWidth := ReadInteger;
      InfoList.Get(i)^.Flags := ReadInteger;
    end;
    ReadListEnd;
  end;
end;
*)

procedure TCustomMatrix.Resize;
begin
  CacheClientSize;
  InvalidatePageInfo;
  inherited Resize;
  Invalidate; // #el: dit vindt ik niet helemaal ok
end;

procedure TCustomMatrix.RestrictCol(var aCol: Integer);
begin
  Restrict(aCol, fLeftLock, fColCount - 1);
end;

function TCustomMatrix.RestrictLeft(var aLeft: Integer): Boolean;
begin
  Result := Restricted(aLeft, fFixedCols, fColCount - 1);
end;

function TCustomMatrix.RestrictRow(var aRow: Integer): Boolean;
begin
  Result := Restricted(aRow, fTopLock, fRowCount - 1);
end;

function TCustomMatrix.RestrictTop(var aTop: Integer): Boolean;
begin
  Result := Restricted(aTop, fFixedRows, fRowCount - 1);
end;

function TCustomMatrix.RestrictTopLeft(var aTopLeft: TPoint): Boolean;
begin
  Result := Restricted(aTopLeft.x, fFixedCols, fColCount - 1) or
            Restricted(aTopLeft.y, fFixedRows, fRowCount - 1);
end;

function TCustomMatrix.RowVisible(aRow: Integer; const VertInfo: TAxisInfo): Boolean;
begin
  Assert(VertInfo.Flags and AXISINFOFLAG_VERTICAL <> 0, 'RowVisible');
  with VertInfo do
    Result :=
      (VisibleCount > 0) and
      ExistInteger(Indices, aRow, VisibleCount - 1)
end;

procedure TCustomMatrix.SafeInvalidateCell(aCol, aRow: Integer; DirectUpdate: Boolean);
begin
  if not CanPaint then Exit;
  if ValidCoord(aCol, aRow) then InvalidateCell(aCol, aRow, DirectUpdate);
end;

function TCustomMatrix.Scroll(aReason: TScrollReason; aScrollMode: TScrollMode; XParam: Integer = 0; YParam: Integer = 0; DoForceRepaint: Boolean = False): Boolean;
var
  PgInfo: TPageInfo;
  Horz, Vert: TAxisInfo;
  OldColVisible, OldRowVisible, NewColVisible, NewRowVisible, OldCellVisible, NewCellVisible: Boolean;
  {A, B, }OldCol, OldRow, NewCol, NewRow, OldLeft, OldTop, NewLeft, NewTop : Integer;
  {PageCalculated,} TopLeftChanged, MustRepaint, ForceRepaint: Boolean;
  DeltaX, DeltaY: Integer;
//  VPosition: TVerticalFocusPosition;
  DirectUpdate: Boolean;

    function NextVisibleCol(var C: Integer; Forwards: boolean): boolean;
    begin
      Result := False;
      if not Between(C, 0, fColCount - 1) then Exit;
      case Forwards of
        False: while (C > 0) and HiddenCols[C] do Dec(C);
        True : while (C < fColCount - 1) and HiddenCols[C] do Inc(C);
      end;
      //Result := not fColFlags[C];
      Result := not HiddenCols[C];//fColFlags[C] and COL_ISHIDDEN = 0;
    end;


    function GetVPosition: TVerticalFocusPosition;
    begin
      if OldRow < PgInfo.Vert.FirstVisible then Result := vfpTop else Result := vfpBottom;
    end;

    function GetHPosition: THorizontalFocusPosition;
    begin
      if OldCol < PgInfo.Horz.FirstVisible then Result := hfpLeft else Result := hfpRight;
    end;

begin

  //  focuscell, restrictparams, (return request when out of bounds?), calcparams (pagedown), scroll
  //

  Result := False;
//  if Navi then Exit;
  if aScrollMode = smDisable then
    Exit;

  Result := False;
  OldLeft := fTopLeft.X;
  OldTop := fTopLeft.Y;
  NewLeft := OldLeft;
  NewTop := OldTop;
  OldCol := fCol;
  OldRow := fRow;
  NewCol := fCol;
  NewRow := fRow;
//  TopLeftChanged := False;
  MustRepaint := False;
  ForceRepaint := False;
//  PageCalculated := False;

  case aScrollMode of
    smAbsolute:
      begin
        NewCol := XParam;
        NewRow := YParam;
      end;
    smDelta:
      begin
        Inc(NewCol, XParam);
        Inc(NewRow, YParam);
      end;
  end;


  RestrictCol(NewCol);
  RestrictRow(NewRow);
  DeltaX := NewCol - OldCol;
//  DeltaY := NewRow - OldRow;

  if not NextVisibleCol(NewCol, DeltaX >= 0)
  and not NextVisibleCol(NewCol, not (DeltaX >= 0)) then
    Exit;

  RestrictCol(NewCol);
  RestrictRow(NewRow);
  DeltaX := NewCol - OldCol;
  DeltaY := NewRow - OldRow;


  { huidige page situatie }
  CalcPageInfo(PgInfo);
  OldColVisible := ColVisible(OldCol, PgInfo.Horz);
  OldRowVisible := RowVisible(OldRow, PgInfo.Vert);
  OldCellVisible := OldColVisible and OldRowVisible;
  NewColVisible := ColVisible(NewCol, PgInfo.Horz);
  NewRowVisible := RowVisible(NewRow, PgInfo.Vert);
  NewCellVisible := NewColVisible and NewRowVisible;


  {-------------------------------------------------------------------------------
    scroll down
  -------------------------------------------------------------------------------}
  if DeltaY > 0 then
  begin
    case OldRowVisible of
      False:
        case NewRowVisible of
          False:
            begin
              CalcPageVert(Vert, NewRow, GetVPosition);
              NewTop := Vert.FirstVisible;
            end;
          True:
            begin
              // calc new page
            end;
        end;
      True:
        case NewRowVisible of
          False:
            begin
              CalcPageVert(Vert, NewRow, vfpBottom);
              NewTop := Vert.Offset;//FirstVisible;
            end;
          True:
            begin
              // niks
            end;
        end;
    end;
  end
  {-------------------------------------------------------------------------------
    scroll up
  -------------------------------------------------------------------------------}
  else if DeltaY < 0 then
  begin
    case OldRowVisible of
      False:
        case NewRowVisible of
          False:
            begin
              CalcPageVert(Vert, NewRow, GetVPosition);
              NewTop := Vert.Offset;//FirstVisible;
            end;
          True:
            begin
              // calc new page
            end;
        end;
      True:
        case NewRowVisible of
          False:
            begin
              CalcPageVert(Vert, NewRow, vfpTop);
              NewTop := Vert.Offset;//FirstVisible;
            end;
          True:
            begin
              // niks
            end;
        end;
    end;
  end;

  {-------------------------------------------------------------------------------
    scroll right
  -------------------------------------------------------------------------------}
  if DeltaX > 0 then
  begin
    case OldColVisible of
      False:
        case NewColVisible of
          False:
            begin
              CalcPageHorz(Horz, NewCol, GetHPosition);
              NewLeft := Horz.Offset;//Horz.FirstVisible;
            end;
          True:
            begin
              // calc new page
            end;
        end;
      True:
        case NewColVisible of
          False:
            begin
              CalcPageHorz(Horz, NewCol, hfpRight);
              NewLeft := Horz.Offset;//Horz.FirstVisible;
            end;
          True:
            begin
              // niks
            end;
        end;
    end;
  end
  {-------------------------------------------------------------------------------
    scroll left
  -------------------------------------------------------------------------------}
  else if DeltaX < 0 then
  begin
    case OldColVisible of
      False:
        case NewColVisible of
          False:
            begin
              CalcPageHorz(Horz, NewCol, GetHPosition);
              NewLeft:= Horz.FirstVisible;
            end;
          True:
            begin
              // calc new page
            end;
        end;
      True:
        case NewColVisible of
          False:
            begin
              CalcPageHorz(Horz, NewCol, hfpRight);
              NewLeft := Horz.FirstVisible;
            end;
          True:
            begin
              // niks
            end;
        end;
    end;
  end;

//  DeltaX := NewCol - OldCol;
//  DeltaY := NewRow - OldRow;

  Result := (OldLeft <> NewLeft) or (OldTop <> NewTop) or (NewRow <> OldRow) or (NewCol <> OldCol);


  if not DoForceRepaint then
  if not NewRowVisible and not OldRowVisible then
  if {not NewRowVisible and }fVirtualMatrix and (aReason = srDataEvent) then
  begin
    Result := True;
    ForceRepaint := True;
  end
  else ForceRepaint := DoForceRepaint;

  if fVirtualMatrix and (aReason = srDataEvent) and (NewRow = OldRow) then
  begin
    Result := True;
    ForceRepaint := True;
  end;





{  if not Result then
  begin
  end; }

  if not Result then
    Exit;

  {-------------------------------------------------------------------------------
    tot hier zijn er alleen berekeningen gedaan. Nu gaan we tekenen.
  -------------------------------------------------------------------------------}

  TopLeftChanged := ScrollTopLeft(srNone, scAbsolute, NewLeft, NewTop, False);
  fCol := NewCol;
  fRow := NewRow;

  { bereken page }
  if TopLeftChanged or ForceRepaint then
  begin
    MustRepaint := True;
    { nieuwe page situatie }
    CalcPageInfo(PgInfo);
    OldColVisible := ColVisible(OldCol, PgInfo.Horz);
    OldRowVisible := RowVisible(OldRow, PgInfo.Vert);
    OldCellVisible := OldColVisible and OldRowVisible;
    NewColVisible := ColVisible(NewCol, PgInfo.Horz);
    NewRowVisible := RowVisible(NewRow, PgInfo.Vert);
    NewCellVisible := NewColVisible and NewRowVisible;
  end;

  DirectUpdate := aReason in [srKeyDown, srMouseClick, srMouseWheel];

  { herteken }
  case MustRepaint of
    False:
      begin
        case fSelectMode of
          smCellSelect:
            begin
              if OldCellVisible then
                InvalidateCell(OldCol, OldRow, DirectUpdate);
              if NewCellVisible then
                InvalidateCell(fCol, fRow, DirectUpdate);
            end;
          smRowSelect:
            begin
              if OldRowVisible then
                InvalidateRow(OldRow, DirectUpdate);
              if NewRowVisible then
              InvalidateRow(fRow, DirectUpdate);
            end;
          smColRowSelect:
            begin
              if OldRowVisible then
                InvalidateRow(OldRow, DirectUpdate);
              if OldColVisible then
                InvalidateCol(OldCol, DirectUpdate);
              if NewRowVisible then
                InvalidateRow(fRow, DirectUpdate);
              if NewColVisible then
                InvalidateCol(fCol, DirectUpdate);
            end;
        end;
      end;
    True:
      begin
        Invalidate;
      end;
  end;

  if eoAlwaysShow in fOptions.EditOptions then
    DoEdit;

  DoAfterFocusCell(NewCol, NewRow);

  if soAlwaysUpdateScreen in fOptions.fScrollOptions then
    Update;

  {$ifdef UseMatrixInspector}
  if Result then if fInspector <> nil then fInspector.ColRowChanged(fCol, fRow);
  {$endif}

end;

function TCustomMatrix.ScrollTopLeft(aReason: TScrollReason; aCommand: TScrollCommand; XParam, YParam: Integer; DoInvalidate: Boolean): Boolean;
var
  NewTopLeft: TPoint;
//  PgInfo: TPageInfo;

    { berekent de beste topleft.y wanneer het einde van het grid is bereikt }
    function CalcMaxTop: Integer;
    var
      Vert: TAxisInfo;
      {i, }Top: Integer;
      {$ifdef assertions on}
      Check: Integer;
      {$endif}
    begin
      {$ifdef assertions on}
      Check := 0;
      {$endif}
      Top := NewTopLeft.y;
      Result := Top;
      CalcVerticalInfo(Vert, Top);
      if (fRowCount > Vert.VisibleCount) and (Vert.GridExtent < fClientHeight - 1) then
      begin
        repeat
          {$ifdef assertions on}
          Inc(Check);
          Assert(Check < 200, ClassName + '.ScrollTopLeft(CalcMaxTop): oneindige loop');
          {$endif}
          CalcVerticalInfo(Vert, Top);
          if Vert.GridExtent >= fClientHeight - 1 then
            Break;
          Result := Top;
          Dec(Top);
          if Top <= 0 then
            Break;
        until False;
      end;
    end;

    { berekent de beste topleft.x wanneer het einde van het grid is bereikt }
    function CalcMaxLeft: Integer;
    var
      Horz: TAxisInfo;
      {i, }Left: Integer;
      {$ifdef assertions on}
      Check: Integer;
      {$endif}
    begin
      {$ifdef assertions on}
      Check := 0;
      {$endif}
      Left := NewTopLeft.x;
      Result := Left;
      CalcHorizontalInfo(Horz, Left);
      if (fColCount > Horz.VisibleCount) and (Horz.GridExtent < fClientWidth - 1) then
      begin
        repeat
          {$ifdef assertions on}
          Inc(Check);
          Assert(Check < 200, ClassName + '.ScrollTopLeft(CalcMaxLeft): oneindige loop');
          {$endif}
          CalcHorizontalInfo(Horz, Left);
          if Horz.GridExtent >= fClientWidth - 1 then Break;
          Result := Left;
          Dec(Left);
          if Left <= 0 then Break; //#hiddencols kunnen voor ellende zorgen!
        until False;
      end;
    end;

begin
  Result := False;
  NewTopLeft := fTopLeft;

  case aReason of
    srNone, srCommand:
      begin
        case aCommand of
          scNone, scAbsolute:
            begin
              NewTopLeft.X := XParam;
              NewTopLeft.y := YParam;
            end;
          scDelta:
            begin
              NewTopLeft.X := fTopLeft.X + XParam;
              NewTopLeft.y := fTopLeft.Y + YParam;
            end;
        end;
      end;
    srKeyDown:
      begin
        case aCommand of
          scNone, scAbsolute:
            begin
              NewTopLeft.X := XParam;
              NewTopLeft.y := YParam;
            end;
          scDelta:
            begin
              NewTopLeft.X := fTopLeft.X + XParam;
              NewTopLeft.y := fTopLeft.Y + YParam;
            end;
        end;
       end;
    srMouseClick:
      begin
      end;
    srMouseWheel:
      begin
        case aCommand of
          scDown:
            begin
              NewTopLeft.Y := fTopLeft.Y + 1;
            end;
          scUp:
            begin
              NewTopLeft.Y := fTopLeft.Y - 1;
            end;
        end;
      end;
    srScrollBar:
      begin
      end;
    srDataEvent:
      begin
      end;
  end;

{  case aCommand of
    scNone, scAbs:
      begin
        NewTopLeft.X := XParam;
        NewTopLeft.y := YParam;
      end;
    scDelta:
      begin
        NewTopLeft.X := fTopLeft.X + XParam;
        NewTopLeft.y := fTopLeft.Y + YParam;
      end;
  end; }


//  NewTopLeft.X := XParam;
//  NewTopLeft.y := YParam;
  RestrictTopLeft(NewTopLeft);


  NewTopLeft.X := CalcMaxLeft;
  NewTopLeft.Y := CalcMaxTop;

  if EqualPoints(NewTopLeft, fTopLeft) then Exit;

  Result := not EqualPoints(NewTopLeft, fTopLeft);
  if Result then
  begin
    fTopLeft := NewTopLeft;
    if DoInvalidate then
      Invalidate
    else
      InvalidatePageInfo;
//    CalcPageInfo(PgInfo);
  end;
end;

procedure TCustomMatrix.SetAutoSizeParentForm(const Value: Boolean);
begin
  if fAutoSizeParentForm = Value then Exit;
  fAutoSizeParentForm := Value;
  DoAutoSizeParentForm;
end;

procedure TCustomMatrix.SetBorderStyle(const Value: TMatrixBorderStyle);
begin
  if fBorderStyle = Value then Exit;
  fBorderStyle := Value;
  RecreateWnd;
end;

procedure TCustomMatrix.SetCol(const Value: Integer);
begin
  SetColRow(Point(Value, fRow));
end;

procedure TCustomMatrix.SetColCount(Value: Integer);
var
  DoColumns: Boolean;
begin

  DoColumns := UsingColumns;
  if DoColumns and fColumns.MatrixLocked then
    Exit;

  {-------------------------------------------------------------------------------
    voorkom recursie
  -------------------------------------------------------------------------------}
  if icSettingColCount in fInternalStates then
    Exit;

  {-------------------------------------------------------------------------------
    check value
  -------------------------------------------------------------------------------}
  Restrict(Value, 0, MAX_COLCOUNT);
  if fColCount = Value then Exit;

  if DoColumns then fColumns.LockMatrix;

  {-------------------------------------------------------------------------------
    zet interne vlag ter voorkoming van recursie
  -------------------------------------------------------------------------------}
  InternalStateEnter(icSettingColCount);
  try
    fColCount := Value;
    fColInfo.Count := Value;
    if DoColumns then fColumns.Count := Value;
  finally
    InternalStateLeave(icSettingColCount);
    if DoColumns then fColumns.UnlockMatrix;
  end;
  Invalidate;
end;

procedure TCustomMatrix.SetColLineWidth(aCol: Integer; const Value: Integer);
begin
  with fColInfo.Get(aCol)^ do
    if LineWidth <> Value then
    begin
      LineWidth := Value;
      Invalidate;
    end;
end;


procedure TCustomMatrix.SetColRow(const Value: TPoint);
begin
  Navigation(srCommand, scAbsolute, Value.X, Value.Y);
end;

procedure TCustomMatrix.SetColumns(const Value: TMatrixColumns);
begin
  fColumns.Assign(Value);
end;

procedure TCustomMatrix.SetColWidth(N, aValue: Integer);
begin
  with fColInfo.Get(N)^ do
  begin
    Restrict(aValue, Low(TColWidthRange), High(TColWidthRange));
    if Size = aValue then Exit;
    Size := aValue;
    Invalidate;
  end;
end;

procedure TCustomMatrix.SetDefaultColWidth(const Value: Integer);
begin
  if fDefaultColWidth = Value then Exit;
  fDefaultColWidth := Value;
  //InvalidatePageInfo;
  fColInfo.DefaultSize := fDefaultColWidth;
  fColInfo.ResetAllSizes;
  Invalidate;
end;

procedure TCustomMatrix.SetDefaultHeaderLineColor(const Value: TColor);
begin
  if fDefaultHeaderLineColor = Value then Exit;
  fDefaultHeaderLineColor := Value;
  Invalidate;
end;

procedure TCustomMatrix.SetDefaultLineColor(const Value: TColor);
begin
  if fDefaultLineColor = Value then Exit;
  fDefaultLineColor := Value;
  Invalidate;
end;

procedure TCustomMatrix.SetDefaultLineWidth(const Value: Integer);
begin
  if fDefaultLineWidth = Value then Exit;
  fDefaultLineWidth := Value;
  fColInfo.DefaultLineSize := fDefaultLineWidth;
  fColInfo.ResetAllLineWidths;
  fRowInfo.DefaultLineSize := fDefaultLineWidth;
  fRowInfo.ResetAllLineWidths;
  Invalidate;
end;

procedure TCustomMatrix.SetDefaultRowHeight(const Value: Integer);
begin
  if fDefaultRowHeight = Value then Exit;
  fDefaultRowHeight := Value;
//  InvalidatePageInfo;
  fRowInfo.DefaultSize := fDefaultRowHeight;
  fRowInfo.ResetAllSizes;
  Invalidate;
end;

procedure TCustomMatrix.SetEditing(const Value: Boolean);
begin
  if Value then
    DoEdit
  else
    DoEditEnd;
end;

procedure TCustomMatrix.SetFixedCols(const Value: Integer);
var
  NewTopLeft: TPoint;
begin
  if fFixedCols = Value then Exit;
  fFixedCols := Value;
  NewTopLeft := fTopLeft;
  if RestrictTopLeft(NewTopLeft) then
    ScrollTopLeft(srCommand, scAbsolute, NewTopLeft.X, NewTopLeft.Y);
  Invalidate;
end;

procedure TCustomMatrix.SetFixedRows(const Value: Integer);
var
  NewTopLeft: TPoint;
begin
  if fFixedRows = Value then Exit;
  fFixedRows := Value;
  NewTopLeft := fTopLeft;
  if RestrictTopLeft(NewTopLeft) then
    ScrollTopLeft(srCommand, scAbsolute, NewTopLeft.X, NewTopLeft.Y);
  Invalidate;
end;

procedure TCustomMatrix.SetHeaderStyle(const Value: TCellStyle);
begin
//  fHeaderStyle := Value;
  fHeaderStyle.Assign(Value);
end;

procedure TCustomMatrix.SetHiddenCol(aCol: Integer; const Value: boolean);
var
  IsHidden: Boolean;
begin
  with fColInfo.Get(aCol)^ do
  begin
    IsHidden := Flags and IR_HIDDEN <> 0;
    if IsHidden = Value then Exit;
    if Value then Flags := Flags or IR_HIDDEN else Flags := Flags and not IR_HIDDEN;
    // check huidige cell
    Invalidate;
  end;
end;

procedure TCustomMatrix.SetHighlightFontStyle(const Value: TFontStyles);
begin
  if fHighlightFontStyle = Value then Exit;
  fHighlightFontStyle := Value;
  Invalidate;
end;

procedure TCustomMatrix.SetHighLightString(const Value: string);
begin
  InternalSetHighLightString(0, Value);
//  SetHighLightStringByIndex(1, Value);
end;

procedure TCustomMatrix.SetHighLightStringByIndex(Index: THighLightRange; const Value: string);
begin
  if not Between(Index, 0, MAX_HIGHLIGHT_STRINGS - 1) then
    Error('SetHighLightStringByIndex');
  InternalSetHighLightString(Index, Value);
end;

procedure TCustomMatrix.SetInspector(const Value: IMatrixInspector);
var
  PgInfo: TPageInfo;
begin
  //if fStates <> [] then Error('SetMatrixInspector');
  if fInspector = Value then Exit;
  if fInspector <> nil then
  begin
    fInspector.DisconnectMatrix(Self);
    fInspector := nil;
  end;
  fInspector := Value;
  if fInspector <> nil then
  begin
    fInspector.ConnectMatrix(Self);
    CalcPageInfo(PgInfo, -1, -1, True);
    fInspector.PageInfoChanged(PgInfo);
    fInspector.ColRowChanged(fCol, fRow);
  end;
end;

procedure TCustomMatrix.SetLeftHeaders(const Value: Integer);
begin
  if fLeftHeaders = Value then Exit;
  fLeftHeaders := Value;
  Invalidate;
end;

procedure TCustomMatrix.SetLeftLock(const Value: Integer);
var
  NewCol: Integer;
begin
  if fLeftLock = Value then Exit;
  fLeftLock := Value;
  NewCol := fCol;
  RestrictCol(NewCol);
  if NewCol <> fCol then
    Navigation(srCommand, scAbsolute, NewCol, fRow);
end;

procedure TCustomMatrix.SetLookAndFeel(const Value: TLookAndFeel);
begin
  fLookAndFeel := Value;
  case fLookAndFeel of
    1:
      begin
        PaintLock;
        DefaultRowHeight := 17;
        FixedRows := 1;
        TopHeaders := 1;
        TopLock := 1;
        Options.RowSizing := False;
        SelectMode := smRowSelect;
        Options.AutoSizeColumns := True;
        PaintUnlock;
      end;
    2:
      begin
        PaintLock;
        DefaultRowHeight := 17;
        FixedRows := 0;
        TopHeaders := 0;
        TopLock := 0;
        Options.PaintRowLines := False;
        Options.RowSizing := False;
        SelectMode := smRowSelect;
        Options.AutoSizeColumns := True;
        PaintUnlock;
      end;
  end;
end;

procedure TCustomMatrix.SetOptions(const Value: TMatrixOptions);
begin
  fOptions.Assign(Value);
end;

procedure TCustomMatrix.SetRowCount(Value: Integer);
begin
  Restrict(Value, 0, MAX_ROWCOUNT);
  if fRowCount = Value then Exit;
  fRowCount := Value;
  fRowInfo.Count := fRowCount;
  {#EL om de een of andere waanzinnige reden kan de volgende code niet:
    die leidt er weer toe dat er 2 headers worden getekend. de restrictopleft is toeg
    voegd en lijkt het op te lossen }

  {#EL bug en zo zullen er meer zijn! }

  if fTopLeft.Y > fRowCount - 1 then
    fTopLeft.Y := 0;
  RestrictTopLeft(fTopLeft);

  Invalidate;
end;

procedure TCustomMatrix.SetRowHeight(N, aValue: Integer);
//var
  //R: TRect;
begin
  with fRowInfo.Get(N)^ do
  begin
    Restrict(aValue, Low(TRowHeightRange), High(TRowHeightRange));
    if Size = aValue then Exit;
    Size := aValue;
(*  InvalidatePageInfo;
    if mxEditing in fStates then
    begin
      R := GetCellRect(EditCol, EditRow);
      fEditLink.SetBounds(R);//R.Left, R.Top, RectWidth(R), RectHeight(R));
    end; *)
    Invalidate;
  end;
end;

procedure TCustomMatrix.SetRowLineWidth(aRow: Integer; const Value: Integer);
begin
  if fRowInfo.Get(aRow)^.LineWidth = Value then Exit;
  fRowInfo.Get(aRow)^.LineWidth := Value;
  Invalidate;
end;

procedure TCustomMatrix.SetSelectMode(const Value: TSelectMode);
begin
  if fSelectMode = Value then Exit;
  fSelectMode := Value;
  Invalidate;
end;

procedure TCustomMatrix.SetSortCol(const Value: Integer);
var
  Old: Integer;
begin
  if fSortCol = Value then Exit;
  Old := fSortCol;
  fSortCol := Value;
  if fTopHeaders > 0 then
  begin
    if (Old >= 0) and ValidCol(Old) then
      InvalidateCell(Old, 0);
    if (Value >= 0) and ValidCol(Value) then
      InvalidateCell(Value, 0);
  end;
end;

procedure TCustomMatrix.SetSortDirection(const Value: TSortDirection);
begin
  fSortDirection := Value;
  if (fTopHeaders > 0) and (fSortCol >= 0) and ValidCol(fSortCol) then
  begin
    InvalidateCell(fSortCol, 0);
  end;
end;

procedure TCustomMatrix.SetStyle(const Value: TCellStyle);
begin
//  fStyle := Value;
  fStyle.Assign(Value);
end;

procedure TCustomMatrix.SetTopHeaders(const Value: Integer);
begin
  if fTopHeaders = Value then Exit;
  fTopHeaders := Value;
  Invalidate;
end;

procedure TCustomMatrix.SetTopLeft(const Value: TPoint);
begin
  ScrollTopLeft(srCommand, scAbsolute, Value.X, Value.Y, True);
end;

procedure TCustomMatrix.SetTopLock(const Value: Integer);
var
  NewRow: Integer;
begin
  if fTopLock = Value then Exit;
  fTopLock := Value;
  NewRow := fRow;
  RestrictRow(NewRow);
  if NewRow <> fRow then
    Navigation(srCommand, scAbsolute, fCol, NewRow);
end;

procedure TCustomMatrix.SetVirtualMatrix(const Value: Boolean);
begin
  if fVirtualMatrix = Value then Exit;
  fVirtualMatrix := Value;
{  case fVirtualMatrix of
    False:
      begin
        fBufferedDataCols := fColCount;
        fBufferedDataRows := fRowCount;
      end;
    True:
      begin
      end;
  end; }
end;

procedure TCustomMatrix.SetVisibleRows(Value: Integer);
var
  Range: TAxisRange;
  H: Integer;
//    procedure LocalRowProc is een locale(!) procedure met de volgende declaratie:

    procedure DoRow(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far;
    begin
      Inc(H, aBottom - aTop + 1 + aLineWidth);
    end;

begin
  RestrictRow(Value);
  H := 0;
  Range.First := 0;
  Range.Last := Value;
  TraverseRows(@DoRow, Range);
  ClientHeight := H;
end;

procedure TCustomMatrix.StateChange(Enter, Leave: TMatrixStates);
//var
  //ActualEnter, ActualLeave: TMatrixStates;
begin
//  if Assigned(fOnStateChange) then
//  begin
    //ActualEnter := Enter - fStates;
    //ActualLeave := fStates * Leave;
//    if (ActualEnter + ActualLeave) <> [] then
  //    fOnStateChange(Self, ActualEnter, ActualLeave);
//  end;
  fStates := fStates + Enter - Leave;
end;

procedure TCustomMatrix.StateEnter(aState: TMatrixState);
begin
  StateChange([aState], []);
end;

procedure TCustomMatrix.StateLeave(aState: TMatrixState);
begin
  StateChange([], [aState]);
end;

procedure TCustomMatrix.StyleChanged(Sender: TObject);
begin
  if UsingColumns then
    fColumns.ClearStyleCache;
  Invalidate;
end;

(*
procedure TCustomMatrix.TimerReset(ID: Integer; Interval: Cardinal);
begin
  KillTimer(Handle, ID);
  SetTimer(Handle, ID, Interval, nil);
end;
*)

procedure TCustomMatrix.TimerStart(ID: Integer; Interval: Cardinal);
begin
  if not HandleAllocated or (csDesigning in ComponentState) then
    Exit;

  SetTimer(Handle, ID, Interval, nil);
  case ID of
    CURSOR_TIMER: Include(fInternalStates, icResizeTimerActive);
    SEARCH_TIMER: Include(fInternalStates, icSearchTimerActive);
    //scroll_t
  end;
end;

procedure TCustomMatrix.TimerStop(ID: Integer);
begin
  if not HandleAllocated or (csDesigning in ComponentState) then
    Exit;

  case ID of
    CURSOR_TIMER:
      begin
        KillTimer(Handle, ID);
        if icResizeTimerActive in fInternalStates then
        begin
          Exclude(fInternalStates, icResizeTimerActive);
          //if fMouseInfo.miState <> mdNone then
            //Perform(WM_SETCURSOR, Handle, HTCLIENT); // forceer cursor   extctrls
        end;
      end;
    SEARCH_TIMER:
      begin
        KillTimer(Handle, ID);
        if icSearchTimerActive in fInternalStates then
        begin
          Exclude(fInternalStates, icSearchTimerActive);
          //fSearchString := '';
        end;
      end;
  else
    begin
      KillTimer(Handle, ID);
    end;
  end;
end;

(* WORDT NIET GEBRUIKT
procedure TCustomMatrix.TraverseCells(LocalCellProc: pointer; const aRange: TRect);

{ LocalCellProc is een locale(!) procedure met de volgende declaratie:
  procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far; }

var
  x, aCol, y, aRow, HLine, VLine: Integer;
  R, DrawRect: TRect;
  Stop: Boolean;
  CallerBP: Cardinal;
  DrawRectPtr: pointer;
  StopPtr: pointer;
  DoAllRects: Boolean;

begin

  { Set up stack frame for local }
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;


  DoAllRects := aRange.Left = MinInt;
  Stop := False;
  { pointers om in registers te zetten, moet anders kunnen zie asm hieronder }
  StopPtr := @Stop;
  DrawRectPtr := @DrawRect;

  R := EmptyRect;
  { vertikaal }
//  with PageInfo do
  //for y := 0 to Vert.VisibleCount - 1 do
  for y := 0 to fRowCount - 1 do //#hidden
  begin
    aRow := y;//Vert.Indices[y];
    R.Left := 0;
    R.Bottom := R.Top + GetRowHeight(aRow) - 1;
    HLine := GetEffectiveRowLineWidth(aRow);
    { horizontaal }
    for x := 0 to fColCount - 1 do
    //for x := 0 to Horz.TotalCount - 1 do //#hidden
    begin
      aCol := x;//Horz.Indices[x];
//      if HiddenCols[aCol] then Continue; // #hidden
      VLine := GetEffectiveColLineWidth(aCol);
      R.Right := R.Left + GetColWidth(aCol) - 1;
      DrawRect := R;
      if DoAllRects or PointInRect(aCol, aRow, aRange) then
      asm
        // = LocalCellProc(aCol, aRow, DrawRect, Stop)
        PUSH CallerBP                     { bewaar stack, deze wordt na de call automatisch gepopt }
        MOV EAX, aCol                     { param 1 in EAX }
        MOV EDX, aRow                     { param 2 in EDX }
        MOV ECX, DWORD PTR [DrawRectPtr]  { param 3 in ECX }
        PUSH DWORD PTR [HLine]            { param 4 op stack }
        PUSH DWORD PTR [VLine]            { param 5 op stack }
        PUSH DWORD PTR [StopPtr]          { variabele param 6 op stack }
        CALL LocalCellProc
        POP ECX // ??? zie borland grids ???
      end;
      if Stop then Exit;
      R.Left := R.Right + VLine + 1;
    end;
    R.Top := R.Bottom + HLine + 1;
  end;

end; *)

(* WORDT NIET GEBRUIKT
procedure TCustomMatrix.TraverseCols(LocalColProc: pointer; const aRange: TAxisRange);

{ LocalColProc is een locale(!) procedure met de volgende declaratie:
  procedure DoCol(aCol, aLeft, aRight, aLineWidth: Integer; var Stop: Boolean); far; }

var
  CallerBP: Cardinal;
  x, aCol, aLeft, aRight, aLineWidth: Integer;
  Stop: Boolean;
  aStopPtr: pointer;
  DoAll: Boolean;
begin

  if aRange.Last < aRange.First then Exit;

  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  DoAll := aRange.First = AllAxis.First;

  Stop := False;
  aStopPtr := @Stop;
  aLeft := 0;

  for x := 0 to fColCount - 1 do
  begin
    aCol := x;
    aRight := aLeft + GetColWidth(aCol) - 1;
    aLineWidth := GetEffectiveColLineWidth(aCol);
    if DoAll or Between(aCol, aRange.First, aRange.Last) then
    asm
      PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
      MOV EAX, aCol               { param 1 in EAX }
      MOV EDX, aLeft              { param 2 in EDX }
      MOV ECX, aRight             { param 3 in ECX }
      PUSH DWORD PTR aLineWidth   { param 4 op stack }
      PUSH DWORD PTR [aStopPtr]   { variabele param 5 op stack }
      CALL LocalColProc
      POP ECX
    end;
    if Stop then Exit;
    aLeft := aRight + aLineWidth + 1;
  end;

end; *)

procedure TCustomMatrix.TraverseRows(LocalRowProc: Pointer; const aRange: TAxisRange);

{ LocalRowProc is een locale(!) procedure met de volgende declaratie:
  procedure DoRow(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far; }

var
  CallerBP: Cardinal;
  y, aRow, aTop, aBottom, aLineWidth: Integer;
  Stop: Boolean;
  aStopPtr: pointer;
  DoAll: Boolean;

begin

  if aRange.Last < aRange.First then Exit;

  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  DoAll := aRange.First = AllAxis.First;
  Stop := False;
  aStopPtr := @Stop;
  aTop := 0;

  for y := 0 to fRowCount - 1 do
  begin
    aRow := y;
    aBottom := aTop + GetRowHeight(aRow) - 1;
    aLineWidth := GetEffectiveRowLineWidth(aRow);
    if DoAll or Between(aRow, aRange.First, aRange.Last) then
    asm
      PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
      MOV EAX, aRow               { param 1 in EAX }
      MOV EDX, aTop               { param 2 in EDX }
      MOV ECX, aBottom            { param 3 in ECX }
      PUSH DWORD PTR aLineWidth   { param 4 op stack }
      PUSH DWORD PTR [aStopPtr]   { variabele param 5 op stack }
      CALL LocalRowProc
      POP ECX
    end;
    if Stop then Exit;
    aTop := aBottom + aLineWidth + 1;
  end;


end;

procedure TCustomMatrix.TraverseVisibleCells(LocalCellProc: pointer; const PageInfo: TPageInfo; const aRange: TRect);

{ LocalCellProc is een locale(!) procedure met de volgende declaratie:
  procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far; }

var
  x, aCol, y, aRow, HLine, VLine: Integer;
  R, DrawRect: TRect;
  Stop: Boolean;
  CallerBP: Cardinal;
  DrawRectPtr: pointer;
  StopPtr: pointer;
  DoAllRects: Boolean;

begin

  { Set up stack frame for local }
  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;


  DoAllRects := aRange.Left = MinInt;
  Stop := False;
  { pointers om in registers te zetten, moet anders kunnen zie asm hieronder }
  StopPtr := @Stop;
  DrawRectPtr := @DrawRect;

  R := EmptyRect;
  { vertikaal }
  with PageInfo do
  //for y := 0 to Vert.VisibleCount - 1 do
  for y := 0 to Vert.VisibleCount - 1 do //#hidden
  begin
    aRow := Vert.Indices[y];
    R.Left := 0;
    R.Bottom := R.Top + GetRowHeight(aRow) - 1;
    HLine := GetEffectiveRowLineWidth(aRow);
    { horizontaal }
    for x := 0 to Horz.VisibleCount - 1 do
    //for x := 0 to Horz.TotalCount - 1 do //#hidden
    begin
      aCol := Horz.Indices[x];
//      if HiddenCols[aCol] then Continue; // #hidden
      VLine := GetEffectiveColLineWidth(aCol);
      R.Right := R.Left + GetColWidth(aCol) - 1;
      DrawRect := R;
      if DoAllRects or PointInRect(aCol, aRow, aRange) then
      asm
        // = LocalCellProc(aCol, aRow, DrawRect, Stop)
        PUSH CallerBP                     { bewaar stack, deze wordt na de call automatisch gepopt }
        MOV EAX, aCol                     { param 1 in EAX }
        MOV EDX, aRow                     { param 2 in EDX }
        MOV ECX, DWORD PTR [DrawRectPtr]  { param 3 in ECX }
        PUSH DWORD PTR [HLine]            { param 4 op stack }
        PUSH DWORD PTR [VLine]            { param 5 op stack }
        PUSH DWORD PTR [StopPtr]          { variabele param 6 op stack }
        CALL LocalCellProc
        POP ECX // ??? zie borland grids ???
      end;
      if Stop then Exit;
      R.Left := R.Right + VLine + 1;
    end;
    R.Top := R.Bottom + HLine + 1;
  end;

end;

procedure TCustomMatrix.TraverseVisibleCols(LocalColProc: pointer; const HorzInfo: TAxisInfo; const aRange: TAxisRange);

{ LocalColProc is een locale(!) procedure met de volgende declaratie:
  procedure DoCol(aCol, aLeft, aRight, aLineWidth: Integer; var Stop: Boolean); far; }

var
  CallerBP: Cardinal;
  x, aCol, aLeft, aRight, aLineWidth: Integer;
  Stop: Boolean;
  aStopPtr: pointer;
  DoAll: Boolean;
begin

  Assert(HorzInfo.Flags and AXISINFOFLAG_VERTICAL = 0, 'TraverseVisibleCols');
  if aRange.Last < aRange.First then Exit;

  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  DoAll := aRange.First = AllAxis.First;

  Stop := False;
  aStopPtr := @Stop;
  aLeft := 0;

  with HorzInfo do
    for x := 0 to VisibleCount - 1 do // #hidden
//    for x := 0 to TotalCount - 1 do // #hidden
    begin
      aCol := Indices[x];
//      if HiddenCols[aCol] then Continue; // #hidden
      aRight := aLeft + GetColWidth(aCol) - 1;
      aLineWidth := GetEffectiveColLineWidth(aCol);
      if DoAll or Between(aCol, aRange.First, aRange.Last) then
      asm
        PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
        MOV EAX, aCol               { param 1 in EAX }
        MOV EDX, aLeft              { param 2 in EDX }
        MOV ECX, aRight             { param 3 in ECX }
        PUSH DWORD PTR aLineWidth   { param 4 op stack }
        PUSH DWORD PTR [aStopPtr]   { variabele param 5 op stack }
        CALL LocalColProc
        POP ECX
      end;
      if Stop then Exit;
      aLeft := aRight + aLineWidth + 1;
    end;

end;

procedure TCustomMatrix.TraverseVisibleRows(LocalRowProc: Pointer; const VertInfo: TAxisInfo; const aRange: TAxisRange);

{ LocalRowProc is een locale(!) procedure met de volgende declaratie:
  procedure DoRow(aRow, aTop, aBottom, aLineWidth: Integer; var Stop: Boolean); far; }

var
  CallerBP: Cardinal;
  y, aRow, aTop, aBottom, aLineWidth: Integer;
  Stop: Boolean;
  aStopPtr: pointer;
  DoAll: Boolean;

begin

  Assert(VertInfo.Flags and AXISINFOFLAG_VERTICAL <> 0, 'TraverseVisibleRows');
  if aRange.Last < aRange.First then Exit;

  asm
    MOV EAX, [EBP]
    MOV callerBP, EAX
  end;

  DoAll := aRange.First = AllAxis.First;

  Stop := False;
  aStopPtr := @Stop;
  aTop := 0;

  with VertInfo do
    for y := 0 to VisibleCount - 1 do
    begin
      aRow := Indices[y];
      aBottom := aTop + GetRowHeight(aRow) - 1;
      aLineWidth := GetEffectiveRowLineWidth(aRow);
      if DoAll or Between(aRow, aRange.First, aRange.Last) then
      asm
        PUSH CallerBP               { bewaar stack, deze wordt na de call automatisch hersteld }
        MOV EAX, aRow               { param 1 in EAX }
        MOV EDX, aTop               { param 2 in EDX }
        MOV ECX, aBottom            { param 3 in ECX }
        PUSH DWORD PTR aLineWidth   { param 4 op stack }
        PUSH DWORD PTR [aStopPtr]   { variabele param 5 op stack }
        CALL LocalRowProc
        POP ECX
      end;
      if Stop then Exit;
      aTop := aBottom + aLineWidth + 1;
    end;

end;

procedure TCustomMatrix.UpdateIncrementalSearch(aKey: Char = #0);
begin
  TimerStop(SEARCH_TIMER);

  if aKey = #0 then
  begin
    fSearchString := '';
  end
  else if aKey in SearchChars then
  begin
    if aKey = #8 then
      fSearchString := CutLeft(fSearchString, 1)
    else
      fSearchString := fSearchString + aKey;
  end;

//  TimerReset(SEARCH_TIMER, SEARCH_TIMER_INTERVAL);

  InternalIncrementalSearch(fSearchString);  // thread?

  TimerStart(SEARCH_TIMER, SEARCH_TIMER_INTERVAL);

  (*
{  if Length(fSearchString) = 1 then
    TimerStart(SEARCH_TIMER, 0)
  else }if fSearchString <> '' then
    TimerStart(SEARCH_TIMER, 700)
  else
    TimerStop(SEARCH_TIMER);
  *)


end;

function TCustomMatrix.ValidCol(aCol: Integer): Boolean;
begin
  Result := (aCol >= 0) and (aCol < fColCount);
end;

function TCustomMatrix.ValidCoord(aCol, aRow: Integer): Boolean;
begin
  Result := (aCol >= 0) and (aCol < fColCount) and (aRow >= 0) and (aRow < fRowCount);
end;

function TCustomMatrix.ValidCoord(aCoord: TPoint): Boolean;
begin
  Result := ValidCoord(aCoord.x, aCoord.y);
end;

function TCustomMatrix.ValidRow(aRow: Integer): Boolean;
begin
  Result := (aRow >= 0) and (aRow < fRowCount);
end;

procedure TCustomMatrix.WMCancelMode(var Message: TWMCancelMode);
begin
  inherited;
end;

procedure TCustomMatrix.WMChar(var Message: TWMChar);
begin
  inherited;
end;

procedure TCustomMatrix.WMDropFiles(var Msg: TWMDropFiles);
var
  CFileName: array[0..MAX_PATH] of Char;
  Files: TStringList;
  i: integer;
begin
  i := 0;
//  windlg('drop iets');
  Files := TStringList.Create;
  try
    Msg.Result := 0;
    while DragQueryFile(Msg.Drop, i, CFileName, MAX_PATH) > 0 do
    begin
      Files.Add(string(CFileName));
      Inc(i);
    end;
    if Assigned(fOnDropFiles) then
      fOnDropFiles(Self, Files);
  finally
    DragFinish(Msg.Drop);
    Files.Free;
  end;
end;

procedure TCustomMatrix.WMEraseBkgnd(var Message: TWmEraseBkgnd);
begin
  Message.Result := 1; // we tekenen alles zelf
end;

procedure TCustomMatrix.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
  Message.Result := DLGC_WANTCHARS or DLGC_WANTARROWS;// or DLGC_WANTTAB;
end;

procedure TCustomMatrix.WMKeyDown(var Message: TWMKeyDown);
begin
  inherited;
end;

procedure TCustomMatrix.WMKillFocus(var Message: TWMKillFocus);
begin
  InternalStateEnter(icKillingFocus);
  try
    //DoEditEnd; dbgrids mxinplace
    inherited;

    fMatrixFocused := False;
    SafeInvalidateCell(fCol, fRow);
  finally
    InternalStateLeave(icKillingFocus);
  end;
end;

procedure TCustomMatrix.WMNCDestroy(var Message: TWMNCDestroy);
begin
  inherited;
end;

procedure TCustomMatrix.WMNCPaint(var Message: TMessage);
begin
  inherited;
end;

procedure TCustomMatrix.WMPaint(var Message: TWMPaint);
begin
  // updaterect, canvas, options

  if not CanPaint then Exit;
  if csPaintCopy in ControlState then
  begin
//    Invalidate;
    fUpdateRect := ClientRect
  end
  else
    GetUpdateRect(Handle, fUpdateRect, False);
  inherited;
  fInternalPaintOptions := [];
end;

procedure TCustomMatrix.WMSetCursor(var Message: TWMSetCursor);
var
  Cur: HCURSOR;
begin
  Cur := 0;

(*
  TWMSetCursor = packed record
    Msg: Cardinal;
    CursorWnd: HWND;
    HitTest: Word;
    MouseMsg: Word;
    Result: Longint;
  end;
  *)


//  if Message.HitTest = HTCLIENT then
  if not (icResizeTimerActive in fInternalStates) then
  case fMouseInfo.miState of
    mdColSizingPossible, mdColSizing:
      begin
        Cur := Screen.Cursors[crSizeWE];
      end;
    mdRowSizingPossible, mdRowSizing:
      begin
        Cur := Screen.Cursors[crSizeNS];
      end;
    mdCellSizingPossible, mdCellSizing:
      begin
        Cur := Screen.Cursors[crSizeNWSE];
      end;
  end;

  if Cur <> 0 then SetCursor(Cur)
  else inherited;
end;

procedure TCustomMatrix.WMSetFocus(var Message: TWMSetFocus);
begin
  inherited;
  fMatrixFocused := True;
  SafeInvalidateCell(fCol, fRow);
end;

procedure TCustomMatrix.WMSize(var Message: TWMSize);
begin
//  CacheClientSize;
//  InvalidatePageInfo;
  inherited;
//  if fAutoSizeParentForm then
  //  DoAutoSizeParentForm;
end;

procedure TCustomMatrix.WMTimer(var Message: TWMTimer);
//var
  //Msg: TWMSetCursor;
begin
  with Message do
  begin
    case TimerId of
      CURSOR_TIMER:
        begin
          TimerStop(CURSOR_TIMER); //Perform(WM_SETCURSOR, Integer(Handle), HTCLIENT);
          if fMouseInfo.miState <> mdNone then
            Perform(WM_SETCURSOR, Handle, HTCLIENT); // forceer cursor
        end;
      SEARCH_TIMER:
        begin
          TimerStop(SEARCH_TIMER);
          if fSearchString <> '' then
          begin
            //DoIncrementalSearch(fSearchString);
            fSearchString := '';
          end;
        end;
      SCROLL_TIMER:
        begin
          //TimerStop(TimerId);
        end;
    end;
  end;
end;


(* NIET GEBRUIKT
procedure TCustomMatrix.WriteColInfo(Writer: TWriter);
begin
  WriteInfo(fColInfo, fColCount, Writer);
end; *)

(* NIET GEBRUIKT
procedure TCustomMatrix.WriteInfo(InfoList: TInfoList; aCount: Integer; Writer: TWriter);
var
  i: integer;
begin
  Assert(Assigned(InfoList), ClassName + '.WriteInfo InfoList = nil');
  Assert(InfoList.Count = aCount, ClassName + '.WriteInfo Count error');
  with Writer do
  begin
    WriteListBegin;
    with InfoList do
      for i := 0 to aCount - 1 do
        with Get(i)^ do
        begin
          WriteInteger(Size);
          WriteInteger(LineWidth);
        end;
    WriteListEnd;
  end;
end; *)

{ TMatrixDataLink }

procedure TMatrixDataLink.ActiveChanged;
begin
  fMatrix.DataSetActiveChanged;
end;

constructor TMatrixDataLink.Create(aMatrix: TCustomDBMatrix);
begin
  inherited Create;
  fMatrix := aMatrix;
  VisualControl := True;
end;

procedure TMatrixDataLink.DataEvent(Event: TDataEvent; Info: Integer);
begin
//  Log(['dataevent', getenumname(typeinfo(tdatasetstate), integer(dataset.state))]);
  inherited DataEvent(Event, Info);
  if DataSet.Bof then Include(fInfo, diBof) else Exclude(fInfo, diBof);
  if DataSet.Eof then Include(fInfo, diEof) else Exclude(fInfo, diEof);
end;

procedure TMatrixDataLink.DataSetChanged;
//var
  //aInfo: TDataScrollInfo;
//  Changes: TDataSetStateChanges;
begin
  {-------------------------------------------------------------------------------
    dedatasetchange wordt afgevuurd na:
    a) append
    b) insert
    c) first
    d) last
  -------------------------------------------------------------------------------}
  //fMatrix.DataSetScrolled(0);
  //Changes := [DataSet.State] - [fPrevState];

//  if (DataSet.Bof and not (diBof in fInfo)) or (DataSet.Eof and not (diEof in fInfo)) then
  //  fMatrix.DataSetScrolled(0)
    fMatrix.DataSetChanged;
//  else


{    aInfo := siBufferScrolled;

  fMatrix.DataSetChanged(aInfo);

  fPrevState := DataSet.State; }
end;

procedure TMatrixDataLink.DataSetScrolled(Distance: Integer);
begin
  fMatrix.DataSetScrolled(Distance);
end;

procedure TMatrixDataLink.Disable;
begin

end;

procedure TMatrixDataLink.EditingChanged;
begin
//  fMatrix.DataSetEditingChanged;
end;

procedure TMatrixDataLink.Enable;
begin

end;

procedure TMatrixDataLink.LayoutChanged;
begin
  fMatrix.DataSetLayoutChanged;
end;



procedure TMatrixDataLink.RecordChanged(Field: TField);
begin
  fMatrix.DataSetRecordChanged(Field);
//  XLog(['recordchanged']);
end;



{ TCustomDBMatrix }

function TCustomDBMatrix.ActiveRecordToRow(aRec: Integer): Integer;
begin
//  if not DataLinkActive then error('activerec');
  Result := aRec + DataTopRow;// - fSkip - DataTop;
//  RestrictRow(Result);
  //if DataSet.Eof then Dec(Result) else if DataSet.Bof then Inc(Result);
{  if Result >= DataLink.BufferCount then
    Result := -1;
  if Result >= DataLink.RecordCount then
    Result := -2; }
end;

procedure TCustomDBMatrix.AdjustActive;
//var
  //Y: Integer;
begin
  {
     mode: initialize (dataset opened of connected)
           finalize (dataset closed)
           navigation
           dataevent

  }

  if not DataLinkActive then
    Exit;

  if fRow <> CalcRow then
    DataSetScrolled(0);

//  Y := ActiveRecordToRow(DataLink.ActiveRecord);

//  Invalidate;
  //Scroll(srCommand, scAbs, fCol, Y);

(*  Assert(fRowCount = fDataLink.BufferCount, 'AdjustActive');

  if mxDataEvent in fStates then
    Reason := srDataEvent
  else
    Reason := srCommand;
  ScrollVirtual(Reason, scAbs, aInfo, fCol, fDataLink.ActiveRecord + DataTop); //#of re-focus
  end; *)
end;

procedure TCustomDBMatrix.AdjustColumns;
var
  i: Integer;
  F: TField;
  C: TMatrixColumn;
  DS: TDataSet;

    procedure ResetColumnFields;
    begin
    end;


begin
  case DataLinkActive of
    False:
      begin
       if fColumns.AutoGenerated then
         ColCount := 0;
      end;
    True:
      begin
        {-------------------------------------------------------------------------------
          zonder columns
        -------------------------------------------------------------------------------}
        if not (aoUseColumns in fOptions.AxisOptions) then
        begin
          ColCount := DataSet.FieldCount;
        end
        {-------------------------------------------------------------------------------
          met columns
        -------------------------------------------------------------------------------}
        else begin
          DS := DataSet;
          fColumns.BeginUpdate;
          try
            {-------------------------------------------------------------------------------
              autogenerate
            -------------------------------------------------------------------------------}
//            if fAutoGenerateColumns then
            if fColumns.AutoGenerated then
            begin
               ColCount := 0;
               for i := 0 to DS.FieldCount - 1 do
               begin
                 F := DS.Fields[i];
                 C := fColumns.InternalAdd;
                 C.Field := F;
                 //C.W
               end;
            end
            {-------------------------------------------------------------------------------
              niet autogenerate
            -------------------------------------------------------------------------------}
            else begin
              for i := 0 to ColCount - 1 do
                with Columns[i] do
                begin
                  Field := DS.FindField(DataField);

                  //Style.HorzAlign := taRightJustify;
//                  Alig
                end;
            end;
          finally
            fColumns.EndUpdate;
          end; // try
        end; // met columns
      end; // datalinkactive = true
  end; // case
end;

procedure TCustomDBMatrix.AdjustRows;
begin
  // ### Aanpassingen
  case DataLinkActive of
    False:
      begin
        RowCount := 1;//0;//1;
      end;
    True:
      begin
        RowCount := CalcRowCount;//DataLink.RecordCount + DataTopRow;
        //Log(['rowcount', rowcount, 'fixedrows', fixedrows, 'headers', topheaders]);
      end;
  end;
end;

procedure TCustomDBMatrix.AssertRowCount(const aProc: string);
begin
  if DataLinkActive then
  begin
    if fRowCount <> CalcRowCount then
    begin
      //Log(['ASSERTROWS', aproc]);
      AdjustRows;
//      AdjustActive;
      Invalidate;
    end;
//    Assert(fRowCount = CalcRowCount, 'AssertRowCount error in:' + aProc);
  end;
end;

function TCustomDBMatrix.CalcRow: Integer;
begin
  Result := ActiveRecordToRow(DataLink.ActiveRecord);
end;

function TCustomDBMatrix.CalcRowCount: Integer;
begin
  Result := DataLink.RecordCount + DataTopRow;
end;

function TCustomDBMatrix.CanEditCell(aCol, aRow: Integer): Boolean;
var
  Fld: TField;
begin
  if not DataLinkActive then
  begin
    Result := False;
    Exit;
  end;

  Result := inherited CanEditCell(aCol, aRow);

  { check of dataset ge-edit kan worden }
  if Result then
  begin
    if not DataLinkActive then
    begin
      Result := False;
      Exit;
    end;
    if not DataSet.CanModify then
    begin
      Result := False;
      Exit;
    end;
    Fld := CoordToField(aCol, aRow);
    if Fld = nil then
    begin
      Result := False;
      Exit;
    end;
    if Fld.ReadOnly then
    begin
      Result := False;
      Exit;
    end;
  end;
end;

function TCustomDBMatrix.ColToField(aCol: Integer): TField;
begin
  Result := nil;

//  CheckCol()

  if not DataLinkActive then
    Exit;

  if Columns <> nil then
  begin
    Result := Columns[aCol].Field;
//    if (aCol <= DataSet.FieldCount - 1) then
  //    Result := DataSet.Fields[aCol];
  end
  else begin
    if (aCol <= DataSet.FieldCount - 1) then
      Result := DataSet.Fields[aCol];
  end;
end;

function TCustomDBMatrix.CoordToField(aCol, aRow: Integer): TField;
begin
  Result := nil;
  if aRow < DataTopRow then Exit;
  Result := ColToField(aCol);
end;

constructor TCustomDBMatrix.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fVirtualMatrix := True;
  fDataLink := TMatrixDataLink.Create(Self);
  fDataLink.BufferCount := 64;
end;

function TCustomDBMatrix.DataLeftRow: Integer;
begin
  Result := fFixedCols;
end;

function TCustomDBMatrix.DataLinkActive: Boolean;
begin
  {-------------------------------------------------------------------------------
    matrix reageert nergens op als controls zijn disabled
  -------------------------------------------------------------------------------}
  Result := Assigned(DataLink) and DataLink.Active and not DataSet.ControlsDisabled;
end;

procedure TCustomDBMatrix.DataSetActiveChanged;
begin
  PaintLock;
  try
    AdjustColumns;
    AdjustRows;
    if DataLinkActive then
    begin
      AdjustActive;
//      DataSetScrolled(0);     // ###bugcheck
      if Options.AutoSizeColumns then
        DoAutoSizeColumns;

//      AssertRowCount('');
  //    DoRefresh;
{      if DatExt(DataSet) <> nil then  db
        with DatExt(DataSet) do
        begin
          HighLightStrings[0] := QuickFilter;
          HighLightStrings[1] := QuickFilterEx;
        end; }
    end;
  finally
    PaintUnlock;
  end;
end;

procedure TCustomDBMatrix.DataSetChanged;
begin
  AdjustRows;
  AdjustActive;
end;

procedure TCustomDBMatrix.DataSetLayoutChanged;
begin
  PaintLock;
  try
//    windlg('layoutchange');
//    Log(['layoutchange']);
    AdjustColumns;
    AdjustRows;
    //DataSetScrolled(0);
    AdjustActive;
//    Scro
  finally
    PaintUnlock;
  end;
end;

procedure TCustomDBMatrix.DataSetRecordChanged(Field: TField);
var
  i: integer;
  Fld: TField;
begin
  AssertRowCount('DataSetRecordChanged');
  if (Field <> nil) {and DataSetSynchronized} then
  begin
    for i := 0 to ColCount - 1 do
    begin
      Fld := CoordToField(i, fRow);
      if Fld = Field then
        InvalidateCell(i, fRow);
    end;
  end;
end;

procedure TCustomDBMatrix.DataSetScrolled(Distance: Integer);
begin
{  if Distance = 0 then
    AdjustActive
  else
    AdjustActive(siBufferScrolled); }

//  AdjustActive;

(*  Log(['scroll *****************']);
  Log(['activerecord', datalink.activerecord, '(' +  dataset.fields[1].displaytext + ')']);
  Log(['matrix row  ', fRow]);
  Log(['buffercount ', datalink.buffercount]);
  Log(['distance    ', distance]); *)

//  Navigation(srDataEvent, scDelta, 0, Distance);
//  Navigation(srDataEvent, scAbsolute, fCol, ActiveRecordToRow(DataLink.ActiveRecord)); //dit is beter

  AssertRowCount('DataSetScrolled');//(fRowCount = Calc)
  fScrollCheck := True;

  if not fNavigating then
    Scroll(srDataEvent, smAbsolute, fCol, ActiveRecordToRow(DataLink.ActiveRecord));

  if Assigned(fOnDataSetScroll) then
    fOnDataSetScroll(Self);


  //Log(['datasetscrolled']);

//  Log(['act',datalink.activerecord, 'row', frow, 'conv', ActiveRecordToRow(DataLink.ActiveRecord)]);

end;

function TCustomDBMatrix.DataTopRow: Integer;
begin
  Result := fFixedRows;
end;

destructor TCustomDBMatrix.Destroy;
begin
  FreeAndNil(fDataLink);
  inherited Destroy;
end;

function TCustomDBMatrix.DidScroll: Boolean;
begin
  Result := fScrollCheck;
  fScrollCheck := False;
end;

procedure TCustomDBMatrix.DoAfterFocusCell(aCol, aRow: Integer);
begin
  if DataLinkActive then
  begin
//    Exit;
    if DataSet.ControlsDisabled then Exit;
    if arow >= datatoprow then
      if ActiveRecordToRow(DataLink.ActiveRecord) <> aRow  then
        Error('DoAfterFocusCell: fout in sync.' + //Chr(13) +
               'datalink.activerecord = %d ' + //Chr(13) +
               'activerecordtorow = %d ' + //Char(13) +
               'row = %d',
                [datalink.activerecord, activerecordtorow(datalink.activerecord), row], 0);
//        Log(['fout in sync', 'act', datalink.activerecord, 'conv', activerecordtorow(datalink.activerecord), 'row', arow]);
//    Log([dataset.recno]);
  end;
  inherited;
end;

procedure TCustomDBMatrix.DoAfterPreparePaintCell(aCol, aRow: Integer; var CellParams: TCellParams);
begin
  //
end;

procedure TCustomDBMatrix.DoAutoSizeColumns;
var
  i, W, x, y: Integer;
  CellParams: TCellParams;
  DummyRect: TRect;
  NewWidths: array of Integer;

    procedure DoCell(aCol, aRow: Integer; const aCellRect: TRect; HorzLn, VertLn: Integer; var Stop: Boolean); far;
    begin
      PaintCellPrepare(aCol, aRow,  aCellRect, CellParams);
    end;

begin

  SetLength(NewWidths, ColCount);
  for i := 0 to ColCount - 1 do
    NewWidths[i] := 16;

  DummyRect := Rect(0,0,10,10);//EmptyRect;
//  TraverseCells(@DoCell, AllCells);


    for y := 0 to RowCount - 1 do
    begin
      for x := 0 to ColCount - 1 do
      begin
        PaintCellPrepare(x, y, DummyRect, CellParams);
        W := Canvas.TextWidth(CellParams.pText) + 8;
        if fsBold in CellParams.pCellStyle.Font.Style then
          inc(W, 12);

        NewWidths[x] := Max(W, NewWidths[x]);
        if NewWidths[x] > 200 then NewWidths[x] := 200;
        //ColWidths[x] := Max(W, ColWidths[x]);
        //if ColWidths[x] > 200 then ColWidths[x] := 200;
      end;
    end;

  for i := 0 to ColCount - 1 do
    ColWidths[i] := NewWidths[i];

end;

procedure TCustomDBMatrix.DoBeforePaintCell(aCol, aRow: Integer; var Params: TCellParams);
begin
  if DataLinkActive and (Params.pPaintRecord >= 0) and (Params.pActiveRecord >= 0) then
  begin
    try
      DataLink.ActiveRecord := Params.pPaintRecord;
      inherited;
    finally
      DataLink.ActiveRecord := Params.pActiveRecord;
    end;
  end;
end;

function TCustomDBMatrix.DoCreateEditor(aCol, aRow: Integer): IMatrixEditLink;
begin
  Result := TDBStringEditLink.Create;
end;

procedure TCustomDBMatrix.DoGetCellData(aCol, aRow: Integer; var Params: TCellParams);
var
  OldRec, GetRec: Integer;
  Fld: TField;
begin

  //Result := '';
  if not DataLinkActive then
    Exit;

  { fieldnames }
  if (aRow = 0) and (aRow < DataTopRow) then
  begin

    if UsingColumns then
      Params.pText := fColumns[aCol].Prompt; // pak default prompt van column

    if Params.pText = '' then
    begin
      Fld := ColToField(aCol);
      if Fld <> nil then
      begin
        Params.pText := Fld.DisplayLabel;
        //Params.pField := nil;
        //Result := Fld.DisplayLabel;//FieldName;
      end;
    end;
    Exit;
  end;

//  Params.pPaintRecord := GetRec;
//  Params.pActiveRecord := OldRec;

  Fld := CoordToField(aCol, aRow);
//  if Fld = nil then Exit;

  if not DataLinkActive then
    Exit;

  with DataLink do
  begin
    OldRec := ActiveRecord;
    GetRec := RowToActiveRecord(aRow);
    if GetRec >= 0 then
    begin
      ActiveRecord := GetRec;
      try
        if Fld <> nil then
        begin
          Params.pText := Fld.DisplayText;
          //if arow=1 then if acol=1 then Log(['1,1', params.ptext, top]);
          Params.pField := Fld;
          Params.pFieldValue := Fld.Value;
        end;
        Params.pPaintRecord := GetRec;
        Params.pActiveRecord := OldRec;
        //Result := Fld.DisplayText;
      finally
        ActiveRecord := OldRec;
      end;
    end;{
    else case GetRec of
      -1: Result := '<beyond buffer>';
      -2: Result := '<beyond data>';
    end; }
  end;

{  except
    Result := '<error>';

  end; }
end;

function TCustomDBMatrix.DoGetCellText(aCol, aRow: Integer): string;
var
  OldRec, GetRec: Integer;
  Fld: TField;
begin

//  CheckCoord(aCol, aRow);

  Result := '';
  if not DataLinkActive then
    Exit;

  { fieldnames header }
  if (aRow = 0) and (aRow < DataTopRow) then
  begin
    if UsingColumns then
      Result := fColumns[aCol].Prompt; // pak default prompt van column
    if Result = '' then
    begin
      Fld := ColToField(aCol);
      if Fld <> nil then
        Result := Fld.DisplayLabel;//FieldName;
    end;
    Exit;
  end;

  Fld := CoordToField(aCol, aRow);
  if Fld = nil then Exit;

  if not DataLinkActive then
    Exit;

  with DataLink do
  begin
    OldRec := ActiveRecord;
    GetRec := RowToActiveRecord(aRow);
    if GetRec >= 0 then
    begin
      ActiveRecord := GetRec;
      try
        Result := Fld.DisplayText;
      finally
        ActiveRecord := OldRec;
      end;
    end
    else case GetRec of
      -1: Result := '<beyond buffer>';
      -2: Result := '<beyond data>';
    end;
  end;

{  except
    Result := '<error>';

  end; }

end;

procedure TCustomDBMatrix.DoIncrementalSearch(const aSearchString: string);
var
  Fld: TField;
//  Bk: string;
begin
  if not DataLinkActive then
    Exit;
  Fld := ColToField(fCol);
  if Fld = nil then
    Exit;
  try
    DataSet.DisableControls;
(*    if DataSet is TIBCustomDataSet then
    begin
      Bk := DataSet.BookMark;
      DataSet.Next;
      if not TIBCustomDataSet(DataSet).LocateNext(Fld.FieldName, aSearchString, [loCaseInsensitive, loPartialKey]) then
        DataSet.Bookmark := Bk;
    end
    else *)
      SearchDataSet(DataSet, Fld.FieldName, fSearchString, [loCaseInsensitive, loPartialKey]);
//      DataSet.Locate(Fld.FieldName, aSearchString, [loCaseInsensitive, loPartialKey]);
    //ibcustomdataset ibtable
  finally
    DataSet.EnableControls;
  end;
end;

procedure TCustomDBMatrix.DoRefresh;
begin
  DataSetChanged;
end;

procedure TCustomDBMatrix.DoSetCellText(aCol, aRow: Integer; const Value: string);
var
  OldRec, GetRec: Integer;
  Fld: TField;
begin

//  Result := '';
  if not DataLinkActive then
    Exit;

  if aRow < DataTopRow then
    Exit;

(*  { fieldnames }
  if (aRow = 0) and (aRow < DataTopRow) then
  begin
    Fld := ColToField(aCol);
    if Fld <> nil then
      Result := Fld.DisplayLabel;//FieldName;
    Exit;
  end; *)

  Fld := CoordToField(aCol, aRow);
  if Fld = nil then Exit;

  with DataLink do
  begin
    OldRec := ActiveRecord;
    GetRec := RowToActiveRecord(aRow);
    if GetRec >= 0 then
    begin
      ActiveRecord := GetRec;
      try
        DataSet.Edit;
        Fld.AsString := Value;
//        Result := Fld.DisplayText;
      finally
        ActiveRecord := OldRec;
      end;
    end
    else case GetRec of
      -1: ;//Result := '<beyond buffer>';
      -2: ;//Result := '<beyond data>';
    end;
  end;

{  except
    Result := '<error>';

  end; }

end;

procedure TCustomDBMatrix.GetCellData(aCol, aRow: Integer;
  var Params: TCellParams);
begin
  DoGetCellData(aCol, aRow, Params);
end;

function TCustomDBMatrix.GetDataSet: TDataSet;
begin
  Result := fDataLink.DataSet;
end;

function TCustomDBMatrix.GetDataSource: TDataSource;
begin
  if fDataLink <> nil then
    Result := fDataLink.DataSource
  else
    Result := nil;
end;

function TCustomDBMatrix.GetSelectedField: TField;
begin
  Result := CoordToField(fCol, fRow);
end;

function TCustomDBMatrix.GetVisibleFields: string;
var
  i: Integer;
begin
  Result := '';
  if UsingColumns then
  begin
    for i := 0 to fColumns.Count - 1 do
    begin
      with fColumns[i] do
        if Field <> nil then
        begin
          if Result <> '' then Result := Result + ',';
          Result := Result + Field.FieldName;
        end;
    end;
  end;
end;

procedure TCustomDBMatrix.HandleNavigation(var aReason: TScrollReason; aCommand: TScrollCommand; var XParam, YParam: Integer; out Mode: TScrollMode);

  function CheckDataScrolled: Boolean;
  begin
    Result := DidScroll;
    if not Result then Mode := smDisable;
  end;

//var
  //Distance: Integer;

begin

  if not DataLinkActive then
  begin
    Mode := smDisable;
    Exit;
  end;

  if DataSet.ControlsDisabled then
  begin
    Mode := smDisable;
    Exit;
  end;

  // als scroll en pageverandering dan repaint anders niet.
  // dit kan door actualdistance mee te geven aan scroll()

  fScrollCheck := False;
  fNavigating := True;
  Mode := smDisable;

  try


  case aReason of
    srCommand:
      begin
        Mode := smAbsolute;
//        XPar
      end;
    {-------------------------------------------------------------------------------
      reason = keydown
    -------------------------------------------------------------------------------}
    srKeyDown, srMouseWheel:
      begin
(*        if DataSet.State in [dsInsert] then
        begin
          //if aCommand in
          Mode := smDisable;
          DataSet.Cancel;
          Exit;
        end; *)

        case aCommand of
          scDown:
            begin
              if fRow >= DataTopRow then // scroll binnen data gebied
              begin
                aReason := srDataEvent;
                Mode := smAbsolute;
                DataSet.Next;
                CheckDataScrolled;
                YParam := CalcRow;
                XParam := fCol;
              end else if (fRow = DataTopRow - 1) then // scroll van header naar data gebied
              begin
                aReason := srDataEvent;
                Mode := smAbsolute;
                DataSet.First;
//                CheckDataScrolled;
                YParam := CalcRow;
                XParam := fCol;
              end
              else inherited HandleNavigation(aReason, aCommand, XParam, YParam, Mode); // scroll binnen niet data
            end;
          scUp:
            begin
              if (fRow > DataTopRow) or not (DataSet.RecNo = 1){(Dataset.Bof} then
              begin
                aReason := srDataEvent;
                Mode := smAbsolute;
                DataSet.Prior;
                CheckDataScrolled;
                YParam := CalcRow;
                XParam := fCol;
              end
              else inherited HandleNavigation(aReason, aCommand, XParam, YParam, Mode);
            end;
          scRight     : inherited HandleNavigation(aReason, aCommand, XParam, YParam, Mode);
          scLeft      : inherited HandleNavigation(aReason, aCommand, XParam, YParam, Mode);
          scPgDown    :
            begin
              aReason := srDataEvent;
              Mode := smAbsolute;
              DataSet.MoveBy(VisibleDataRows);
              CheckDataScrolled;
              YParam := CalcRow;
              XParam := fCol;
            end;
          scPgUp      :
            begin
              aReason := srDataEvent;
              Mode := smAbsolute;
              DataSet.MoveBy(-VisibleDataRows);
              CheckDataScrolled;
              YParam := CalcRow;
              XParam := fCol;
            end;
          scHorzStart : ;
          scHorzEnd   : inherited HandleNavigation(aReason, aCommand, XParam, YParam, Mode);
          scVertEnd   :
            begin
              aReason := srDataEvent;
              Mode := smAbsolute;
              DataSet.Last;
              CheckDataScrolled;
              YParam := CalcRow;
              XParam := fCol;
            end;
          scVertStart :
            begin
              aReason := srDataEvent;
              Mode := smAbsolute;
              DataSet.First;
              CheckDataScrolled;
              YParam := CalcRow;
              XParam := fCol;
            end;
        end;
      end;
    {-------------------------------------------------------------------------------
      reason = mouseclick
    -------------------------------------------------------------------------------}
    srMouseClick:
      begin
//        if YParam < fTopLocks then
        begin
//          Mode := smDisable;
//          Exit;
        end;
        Mode := smAbsolute;
        begin
          if (YParam < DataTopRow) then
          begin
            // laat dataset staan
            //DataSet.First;
            //YParam :=
          end
          else begin
{            if fRow < DataTopRow then
              Distance := YParam - TopRow
            else
              Distance := YParam - fRow; }

            { voorkom scroll na een append !!! }
            if RowToActiveRecord(YParam) - DataLink.ActiveRecord <> 0 then
              DataSet.MoveBy(RowToActiveRecord(YParam) - DataLink.ActiveRecord);
            //DataSet.MoveBy(Distance);
            YParam := CalcRow;
          end;

  //        CheckDataScrolled;

        end;

      end;
    {-------------------------------------------------------------------------------
      reason = dataevent
    -------------------------------------------------------------------------------}
    srDataEvent:
      begin
        case aCommand of
          scDelta:
            begin
              Mode := smAbsolute;
              XParam := fCol;
              YParam := CalcRow;
            end;
          scAbsolute:
            begin
              Mode := smAbsolute;
              XParam := fCol;
              YParam := CalcRow;
            end;
        end;
      end;
  end;

  //  DoScroll;

  finally
    fNavigating := False;
    fScrollCheck := False;
  end;

end;

procedure TCustomDBMatrix.KeyDown(var Key: word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if not DataLinkActive then Exit;
  if mxEditing in fStates then Exit;
  case Key of //dbgrids
    VK_INSERT:
        if eoCanAppendRow in Options.EditOptions then
        if DataSet.CanModify then
        begin
          if DataSet.State = dsInsert then
            DataSet.Post; // ????????????????
          DataSet.Append;
          Invalidate;
        end;
    VK_DELETE:
      if ssCtrl in Shift then
      begin
        //if MessageDlg('record verwijderen', mtConfirmation, [mbNo, mbOK], 0) = mrOK then
        if eoCanDeleteRow in Options.EditOptions then
        if DataSet.CanModify then
        if not DataSet.IsEmpty then
        begin
          DataSet.Delete;
          Invalidate;
        end;
      end;
    VK_ESCAPE:
      if DataSet.State in [dsEdit, dsInsert] then
        DataSet.Cancel;
  end;

end;

function TCustomDBMatrix.RowToActiveRecord(aRow: Integer): Integer;
begin
  Result := aRow - DataTopRow;
  if Result < 0 then
    Result := -1;
  if Result >= DataLink.RecordCount then
    Result := -2;
end;

procedure TCustomDBMatrix.SetDataSource(const Value: TDataSource);
begin
  if Value = DataSource then Exit;//## misschien moet dit wel niet
  fDataLink.DataSource := Value;
//  DoRefresh;
//  windlg('matrix.setdatasource')
end;


initialization
  {-------------------------------------------------------------------------------
    InitializeGlobals wordt aangeroepen door TCustomMatrix.Create
    dus alleen wanneer nodig
  -------------------------------------------------------------------------------}
finalization
  FinalizeGlobals;
end.


