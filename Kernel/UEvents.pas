unit UEvents;

{ Unit om een master-slave objectrelatie te leggen.
  De master kan dmv messages of events de slave op de hoogte houden }

interface

uses
  Classes, Controls, Sysutils, Messages,
  ULog, UMisc, UTools;

type

  TMessageLink = class;
{ een collection voor de messagemanager maken voor meer type control is veiliger
  een messagelink zou dan een tcollectionitem moeten zijn }
  {custom protected versie maken }

  TCheckMessageEvent = procedure(Sender: TObject; var DoSend: Boolean) of object;
  TMessageEvent = procedure(var Message) of object;

  TMessageManager = class
  private
    fOnSendMessage: TMessageEvent;
    fBeforeSendMessage: TCheckMessageEvent;
  protected
    fMaster        : TObject;
    fLinks         : TList;  { een lijst van messagelinks }
    fDisableCount  : integer;
    fSending       : boolean;
//    FDisabledMsg   : TIntegerList;
    function CanSend: boolean;
    procedure AddLink(const ALink: TMessageLink); //virtual;
    procedure RemoveLink(const ALink: TMessageLink); //virtual;
  public
    constructor Create(aMaster: TObject); // altijd vullen graag
    destructor Destroy; override;
    procedure Send(var Message); virtual;{ Roept Receive aan van alle messagelinks }
    function Perform(Msg: Cardinal; WParam, LParam: Longint): Longint; { Roept Send aan, Perform kan gebruikt worden voor "standaard messages" }
    function MessagesEnabled: boolean;
    function FindSlave(aObject: TObject): integer;
    procedure Disable;
    procedure Enable;
    procedure DisableMessages(const MsgArray: TOpenIntegerArray);
    property Master: TObject read FMaster write FMaster;
    property Links: TList read FLinks;
    property Sending: boolean read FSending;
    property BeforeSendMessage: TCheckMessageEvent read fBeforeSendMessage write fBeforeSendMessage;
    property OnSendMessage: TMessageEvent read fOnSendMessage write fOnSendMessage;
  end;

  { er zijn eigenlijk meerdere manieren om events door te geven
    receive
    dispatch
    eventhandler (custom messages)
  }

  TMessageLink = class
  private
  protected
    fManager             : TMessageManager;
    fVisual              : Boolean;
    fDisableCount        : Integer;
    fSlave               : TObject;
    fOnManagerChanging   : TNotifyEvent;
    fOnManagerChange     : TNotifyEvent;
    fOnReceiveMessage    : TMessageEvent;
    fReceiving           : Boolean;
    procedure SetManager(const Value: TMessageManager); virtual; {?}
    function GetMaster: TObject;
  public
    constructor Create(aManager: TMessageManager; aSlave: TObject);
    destructor Destroy; override;
    procedure Receive(var Message); virtual; { sluist dispatch door naar FSlave of roept onmessage aan }
    procedure Redirect(aObject: TObject; var Msg);
    function Disabled: boolean;
    procedure Disable;
    procedure Enable;
    property Manager: TMessageManager read FManager write SetManager;
    property Slave: TObject read FSlave;
    property OnReceiveMessage: TMessageEvent read FOnReceiveMessage write FOnReceiveMessage;
    property OnManagerChanging: TNotifyEvent read fOnManagerChanging write fOnManagerChanging;
    property OnManagerChange: TNotifyEvent read fOnManagerChange write fOnManagerChange;
    property Receiving: boolean read FReceiving;
    property Master: TObject read GetMaster;
    property Visual: boolean read FVisual write FVisual;
  end;





  (*
  ISubject = interface
//    procedure Notify(var Message);
  end;

  TSubject = class(TInterfacedObject, ISubject)
  private

  protected
  public
  published
  end;

  IObserver = interface
    procedure Dispatch(var Message);
  end;//core_umodules
  *)

implementation

{ TMessageManager }

procedure TMessageManager.AddLink(const ALink: TMessageLink);
begin
  ALink.FManager := Self; { niet de property wegens recursie! }
  if FLinks.IndexOf(ALink) = -1 then
    FLinks.Add(ALink);
end;

constructor TMessageManager.Create(AMaster: TObject);
begin
  inherited Create;
  FLinks := TList.Create;
  FMaster := AMaster;
end;

destructor TMessageManager.Destroy;
begin
  while FLinks.Count > 0 do RemoveLink(FLinks.Last); { ! }
  FLinks.Free;
end;

procedure TMessageManager.Disable;
begin
  Inc(FDisableCount);
end;

procedure TMessageManager.Enable;
begin
  if FDisableCount > 0 then
    Dec(FDisableCount);
end;

function TMessageManager.MessagesEnabled: boolean;
begin
  Result := FDisableCount = 0;
end;

function TMessageManager.Perform(Msg: Cardinal; WParam, LParam: Longint): Longint;
var
  Message: TMessage;
begin
  Message.Msg := Msg;
  Message.WParam := WParam;
  Message.LParam := LParam;
  Message.Result := 0;
  Send(Message);
  Result := Message.Result;
end;

procedure TMessageManager.RemoveLink(const ALink: TMessageLink);
begin
  ALink.FManager := nil; { niet de property wegens recursie ! }
  FLinks.Remove(ALink);
end;

function TMessageManager.CanSend: boolean;
begin
  CanSend := (FDisableCount = 0) and (FLinks.Count > 0)
end;

procedure TMessageManager.Send(var Message);
var
  i: integer;
  DoSend: Boolean;
begin
  if FSending then
    Exit;
  { te doen: houdt message cardinal vast: er mogen wel andere messages recursief verzonden worden, maar dezelfde niet }
  FSending := True;
  try
    if CanSend then
    begin
      { check event voor master }
      if Assigned(fBeforeSendMessage) then
      begin
        DoSend := True;
        fBeforeSendMessage(Self, DoSend);
        if not DoSend then
          Exit;
      end;

      with FLinks do
      begin
        { eerst non visueel }
        for i := 0 to Count - 1 do
          with TMessageLink({Items[i]}List^[i]) do
            if not FVisual then
              Receive(Message);
        { dan visueel }
        for i := 0 to Count - 1 do
          with TMessageLink({Items[i]}List^[i]) do
            if FVisual then
              Receive(Message);
        { eventhandler }
        if Assigned(fOnSendMessage) then
          fOnSendMessage(Message);
      end;
    end;
  finally
    FSending := False;
  end;

  (*  if FStackCount > 16 then Exit; // recursief tot 16
  try
    Inc(FStackCount);
    {if FStackCount > 1 then Log(['UPDATECOUNT > 1']);}
    //Log(['MANAGER SEND ', Longint(Message)]);
    if MessagesEnabled then
      with FLinks do
        for i := 0 to Count - 1 do
          TMessageLink(Items[i]).Receive(Message);
  finally
    Dec(FStackCount);
  end; *)
end;

function TMessageManager.FindSlave(AObject: TObject): integer;
begin
  with FLinks do
    for Result := 0 to Count - 1 do
      if TMessageLink(Items[Result]).Slave = AObject then
        Exit;
  Result := -1;
end;

procedure TMessageManager.DisableMessages(const MsgArray: TOpenIntegerArray);
var
  i: integer;
begin
  for i := Low(MsgArray) to High(MsgArray) do
    //
end;

{ TMessageLink }

{constructor TMessageLink.Create;
begin
  inherited;
end;}

constructor TMessageLink.Create(AManager: TMessageManager; ASlave: TObject);
begin
  inherited Create;
  if AManager <> nil then
    AManager.AddLink(Self);
  FSlave := ASlave;
end;

destructor TMessageLink.Destroy;
begin
  if FManager <> nil then
    FManager.RemoveLink(Self);
  inherited;
end;

function TMessageLink.GetMaster: TObject;
begin
  if FManager <> nil then
    Result := FManager.Master
  else
    Result := nil;
end;

procedure TMessageLink.Disable;
begin
  Inc(FDisableCount);
end;

function TMessageLink.Disabled: boolean;
begin
  Result := FDisableCount > 0;
end;

procedure TMessageLink.Receive(var Message);
begin
  if (FDisableCount = 0) and (FSlave <> nil) then
  begin
    //Log([tmessage(message).msg]);
    FReceiving := True;
    //Log([tmessage(message).msg, slave.ClassName]);
    FSlave.Dispatch(Message);
    if Assigned(FOnReceiveMessage) then
      FOnReceiveMessage(Message);
    FReceiving := False;
  end;

(*  if (FDisableCount = 0) and (FSlave <> nil) then
  begin
    FReceiving := True;
    if Assigned(FOnMessage) then
      FOnMessage(Message)
    else
      FSlave.Dispatch(Message);
    FReceiving := False;
  end; *)
end;

procedure TMessageLink.Enable;
begin
  if FDisableCount > 0 then Dec(FDisableCount);
end;

procedure TMessageLink.SetManager(const Value: TMessageManager);
begin
  { als zelfde manager dan exit }
  if fManager = Value then Exit;
  { geef app de kans de reageren }
  if Assigned(fOnManagerChanging) then fOnManagerChanging(Self);
  { als andere manager dan disconnect van oude manager }
  if fManager <> nil then fManager.RemoveLink(Self);
  { connect aan nieuwe manager }
  fManager := Value;
  if fManager <> nil then
    fManager.AddLink(Self);
  { geef app de kans de reageren }
  if Assigned(fOnManagerChange) then fOnManagerChange(Self);
end;

procedure TMessageLink.Redirect(aObject: TObject; var Msg);
begin
  aObject.Dispatch(Msg);
end;

end.




(*


 Chris Lichti - Home | BLOG | Resume | Lab
Observer Design Pattern

The Observer Design Pattern is a simple but frequently overlooked solution to seemingly complicate
problems. Simply put, the idea is that objects called Observers ask objects called Subjects to let
them know when certain things happen, such as a property of the Subject changes. The Observer Design
Pattern is described in many books and websites on the subject of Object-Oriented Design Patterns,
so I refer you to the about.com page on the subject here:

http://compsci.about.com/science/compsci/cs/observerpattern/

Now that you're an expert on the theory of the Observer Design Pattern, here is a simple example in
Delphi. This example implements the Observer Design Pattern using a simple delegate and the message
dispatching built into Delphi. This way, any class in Delphi can be made into an Observer or a
Subject with very little effort.

To get started, take a look at the "Observer_Imp.pas" file. It has all the Subject and Observer classes,
including two simple base classes and a bunch of concrete examples that can be used to form mathematical
equations.

Then take a look at the example project "ObserverDemo.dpr". Don't bother running it. The demo is meant
to be seen at Design-Time. On that note, you'll need to install the package "ObserverDemoPack.dpk"
It's a simple package, which you can uninstall when you're done.

Here is a code-snippet showing the base class that implements the Subject.
Observers can be any TObject here, but the Observers are responsible for notifying the
TSubject when they are destroyed. See "Observer_Imp.pas" for details.

type

  {
  TSubject 

  This class encapsulates the bare essentials of a Subject that is Observable.
  To make any existing class observable, either have it descend from TSubject,
  or it that's not possible, simple add a published property called "Subject"
  of type TSubject and instantiate it in the constructor of your class.  This
  published property will be used as a Subject Delegate by the
  TObserverDelegate class.  Examples follow.
  }
  TSubject = class
  private
    Observers: TList;
  public
    constructor Create; virtual; 
    destructor Destroy; override;
    procedure Attach(Observer: TObject); virtual;
    procedure Detach(Observer: TObject); virtual;
    procedure Notify(var Message); overload; virtual;
    procedure Notify(MsgConst: Cardinal; 
                     WParam, LParam: integer); overload; virtual;
  end;

implementation

{ TSubject }

procedure TSubject.Attach(Observer: TObject); 
begin
  if Observers.IndexOf(Observer) < 0 then
    Observers.Add(Observer);
end;

constructor TSubject.Create;
begin
  Observers := TList.Create;
end;

destructor TSubject.Destroy;
begin
  Notify(WM_SUBJECTDESTROYED, Integer(self), 0);
  Observers.Free;
  inherited;
end;

procedure TSubject.Detach(Observer: TObject);
begin
  Observers.Remove(Observer);
end;

procedure TSubject.Notify(var Message);
var
  i: integer;
begin
  for i := 0 to Observers.Count - 1 do
    TObject(Observers[i]).Dispatch(Message);
end;

procedure TSubject.Notify(MsgConst: Cardinal; WParam, LParam: integer);
var
  Msg: TMessage;
begin
  Msg.Msg := MsgConst;
  Msg.WParam := WParam;
  Msg.LParam := LParam;
  Notify(Msg);
end;



Copyright � 2002 Christopher C. Lichti. All rights reserved.
Last updated 3.3.2002

