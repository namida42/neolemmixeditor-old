unit UCalendar;

interface

uses
  Classes, Windows, Messages, Controls, Graphics, Grids, StdCtrls, SysUtils,
  UMisc, UWinTools;

type
  TCalendarDialog = class(TComponent)
  private
    FSelDate: TDate;
    FPosition: TPoint;
    FCheckScreen: boolean;
    procedure SetSelDate(const Value: TDate);
    //procedure DateChange(Sender: TObject; ADate: TDate);
    procedure SetPosition(const Value: TPoint);
  public
    constructor Create(AOwner: TComponent); override;
    function Execute: boolean;
    property Position: TPoint read FPosition write SetPosition;
    property SelDate: TDate read FSelDate write SetSelDate;
    property CheckScreen: boolean read FCheckScreen write FCheckScreen;
  end;

function SelectDate(var ADate: TDate; X: integer = -1; Y: integer = -1;
  ACheckScreen: boolean = True): boolean;

implementation

uses
  Forms;

{ TCalendarDialog }

constructor TCalendarDialog.Create(AOwner: TComponent);
begin
  inherited;
  with FPosition do
  begin
    X := -1;
    Y := -1;
  end;
end;

{procedure TCalendarDialog.DateChange(Sender: TObject; ADate: TDate);
begin
  FSelDate := ADate;
end;}

function TCalendarDialog.Execute: boolean;
//var
//  CalForm: TCalendarForm;
  //ARect: TRect;
  //P: TPoint;
begin
  Windlg('Kalender');
  Result := False;
(*  CalForm := TCalendarForm.Create(Application);
  try
    CalForm.OnDateChange := Self.DateChange;
    if FSelDate = 0 then
      FSelDate := Date();
    CalForm.SelDate := FSelDate;
    with FPosition do
      if (X <> -1) and (Y <> -1) then
      begin
        CalForm.Position := poDesigned; { voorkom de ontworpen poDesktopCenter }
        CalForm.Left := X;
        CalForm.Top := Y;
      end;
    if FCheckScreen then
    begin
      ARect.Top := FPosition.Y; ARect.Left := FPosition.X;
      ARect.BottomRight := ARect.TopLeft;
      //Getpo
      //AdjustWindowPosition(CalForm, ARect);
      P := GetBestPopupPosition(aRect, CalForm.Width, CalForm.Height);
      CalForm.Top := P.y;
      CalForm.Left := P.x;
    end;
    Result := CalForm.ShowModal = mrOK;
  finally
    CalForm.Free;
  end; *)
end;

procedure TCalendarDialog.SetPosition(const Value: TPoint);
begin
  FPosition := Value;
end;

procedure TCalendarDialog.SetSelDate(const Value: TDate);
begin
  FSelDate := Value;
end;

function SelectDate(var ADate: TDate; X: integer = -1; Y: integer = -1;
  ACheckScreen: boolean = True): boolean;
var
  Dlg: TCalendarDialog;
begin
  Dlg := TCalendarDialog.Create(nil);
  try
    Dlg.Position := Point(X, Y);
    Dlg.CheckScreen := ACheckScreen;
    if ADate <> 0 then Dlg.SelDate := ADate else Dlg.SelDate := Date();
    Result := Dlg.Execute;
    if Result then ADate := Dlg.SelDate;
  finally
    Dlg.Free;
  end;
end;

end.
