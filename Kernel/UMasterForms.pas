unit UMasterForms;

interface

uses
  Classes, Forms, Controls, SysUtils, Graphics, Messages, Windows,
  UMisc, UWinTools, UMessages, Math;

//const utools uwintools umisc
//  DEF_SHADOWCOLOR = RGB(20,20,20);
{ TCustomMasterForm is niet als instantie beschikbaar, alleen als ParentClass }

type

  TFormDropFilesEvent = procedure(Sender: TObject; const DroppedFiles: TStrings) of object;

  TCustomMasterForm = class(TForm)
  private
  { intern }
    fDoShow: boolean;
  { properties }
    fShadowed: boolean;
    fShadowTransparence: byte;
    fShadowX: integer;
    fShadowY: integer;
    fShadowColor: TColor;
    fShadowWin: TForm;
    fIsChild: boolean;
    fTransparancy: Byte;
    fCaptionBar: boolean;
    fAutoSizing: Boolean;
    fAcceptDraggedFiles: Boolean;
    fEraseBk: Boolean;
    fOnDropFiles: TFormDropFilesEvent;
  { intern }
    procedure FreeShadowWin;
    procedure CreateShadowWin;
    procedure AdjustShadowPosition;
    procedure CheckShow;
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
    procedure WMEraseBkgnd(var Message: TWmEraseBkgnd); message WM_ERASEBKGND;
    procedure WMMove(var Message: TWMMOVE); message WM_MOVE;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure WMDropFiles(var Msg: TWMDropFiles); message WM_DROPFILES;
  { properties }
    procedure SetShadowed(const Value: boolean);
    procedure SetShadowTransparence(const Value: byte);
    procedure SetShadowColor(const Value: TColor);
    procedure SetShadowX(const Value: integer);
    procedure SetShadowY(const Value: integer);
    procedure SetIsChild(const Value: boolean);
    procedure SetTransparancy(const Value: Byte);
    procedure SetCaptionBar(const Value: boolean);
    procedure CMAutoSize(var Message: TMessage); message CM_AUTOSIZE;
    procedure SetAcceptDraggedFiles(const Value: Boolean);
  protected
    procedure Activate; override;
    procedure Deactivate; override;
    procedure Loaded; override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure SetZOrder(TopMost: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Shadowed: boolean read fShadowed write SetShadowed;
    property ShadowTransparence: byte read fShadowTransparence write SetShadowTransparence default 176;
    property ShadowColor: TColor read fShadowColor write SetShadowColor;
    property ShadowX: integer read fShadowX write SetShadowX default 14;
    property ShadowY: integer read fShadowY write SetShadowY default 14;
    property Transparancy: Byte read fTransparancy write SetTransparancy;
  { style }
    property IsChild: boolean read fIsChild write SetIsChild;
    property CaptionBar: boolean read fCaptionBar write SetCaptionBar default True;
    property AutoScroll default False;

    property AcceptDraggedFiles: Boolean read fAcceptDraggedFiles write SetAcceptDraggedFiles;
    property EraseBk: Boolean read fEraseBk write fEraseBk;

    property OnDropFiles: TFormDropFilesEvent read fOnDropFiles write fOnDropFiles;

  end;

  { Vervanger voor TFrame }
  TCustomMasterChildForm = class(TForm)
  protected
   procedure Loaded; override;
   procedure CreateParams(var Params: TCreateParams); override;
  end;

{  TCustomDBMasterForm = class(TCustomMasterForm)  umatrix
  private
    fDataModule: TDataModule;
    procedure SetDataModule(const Value: TDataModule);
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property DataModule: TDataModule read fDataModule write SetDataModule;
  published
  end; }

  TFlatForm = class(TCustomMasterForm)
  private
    procedure WMNCHitTest(var Message: TWMNCHitTest); message WM_NCHITTEST;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  public
    constructor Create(aOwner: TComponent); override;
  published
  end;

implementation

uses
  ULog, ShellApi, ComCtrls;

//var
  //MasterFormCreating: boolean;

{ TCustomMasterForm }

constructor TCustomMasterForm.Create(AOwner: TComponent);
begin
  inherited Create(aOwner);
 autoScroll := False;
  fShadowTransparence := 176;
  fShadowX := 8;
  fShadowY := 8;
  fTransparancy := 255;
  fCaptionBar := True;
  fEraseBk := True;
//  AutoScroll := False;
//  KeyPreview := True;
end;

destructor TCustomMasterForm.Destroy;
begin
//  FreeShadowWin;
  inherited Destroy;
end;

procedure TCustomMasterForm.CreateShadowWin;
begin
  if fShadowWin = nil then
  begin
    fShadowWin := TForm.Create(Self{Application});
    with fShadowWin do
    begin
      BorderIcons := [];
      BorderStyle := bsNone;
      Enabled := False;
      Color := fShadowColor;
      SetTransparentForm(Handle, fShadowTransparence);
      AdjustShadowPosition;
      CheckShow;
    end;
  end;
end;

procedure TCustomMasterForm.FreeShadowWin;
begin
  if fShadowWin <> nil then FreeAndNil(fShadowWin);
end;

procedure TCustomMasterForm.AdjustShadowPosition;
begin
  if fShadowWin <> nil then
    fShadowWin.SetBounds(Left + fShadowX, Top + fShadowX, Width, Height);
end;

procedure TCustomMasterForm.CMVisibleChanged(var Message: TMessage);
begin
//  if fShadowWin <> nil then
//    fShadowWin.Visible := Visible;
//  Log(['visiblechanged']);
  
  inherited;
end;

procedure TCustomMasterForm.WMMove(var Message: TWMMOVE);
begin
  AdjustShadowPosition;
  inherited;
end;

procedure TCustomMasterForm.WMSize(var Message: TWMSize);
begin
  AdjustShadowPosition;
//  Log(['size']);
  inherited;
end;

procedure TCustomMasterForm.SetShadowed(const Value: boolean);
begin
  if fShadowed = Value then Exit;
  fShadowed := Value;
  case fShadowed of
    True: CreateShadowWin;
    False: FreeShadowWin;
  end;
end;

procedure TCustomMasterForm.SetShadowTransparence(const Value: byte);
begin
  if fShadowTransparence = Value then Exit;
  fShadowTransparence := Value;
  if fShadowWin <> nil then
    fShadowWin.Invalidate;
end;

procedure TCustomMasterForm.SetShadowColor(const Value: TColor);
begin
  if fShadowColor = Value then Exit;
  fShadowColor := Value;
  if fShadowWin <> nil then
    fShadowWin.Color := fShadowColor;
end;

procedure TCustomMasterForm.Activate;
begin
  fDoShow := true;
  CheckShow;
  inherited;
end;

procedure TCustomMasterForm.Deactivate;
begin
  fDoShow := False; //windowstate???
//  CheckShow;
  inherited Deactivate;
end;

procedure TCustomMasterForm.CheckShow;
begin
  if fDoShow and (fShadowWin <> nil) and not fShadowWin.Visible then
  begin
    fShadowWin.Show;
    BringToFront;
  end
  else if not fDoShow and (fShadowWin <> nil) and fShadowWin.Visible then
  begin
//    if fShadowWin.Showing then
//    fShadowWin.Hide;
//    Log(['verberg schaduw']);
  end;
end;

procedure TCustomMasterForm.SetShadowX(const Value: integer);
begin
  fShadowX := Value;
  if fShadowWin <> nil then
    fShadowWin.Left := Left + fShadowX;
end;

procedure TCustomMasterForm.SetShadowY(const Value: integer);
begin
  fShadowY := Value;
  if fShadowWin <> nil then
    fShadowWin.Top := Top + fShadowY;
end;

(*
{ TCustomDBMasterForm }

constructor TCustomDBMasterForm.Create(AOwner: TComponent);
begin
  inherited Create(aOwner);
end;

destructor TCustomDBMasterForm.Destroy;
begin
  FreeAndNil(fDataModule);
  inherited Destroy;
end;

procedure TCustomDBMasterForm.SetDataModule(const Value: TDataModule);
begin
  if Value = fDataModule then
    Exit;
  if fDataModule <> nil then
    FreeAndNil(fDataModule);
  fDataModule := Value;
end;
*)
procedure TCustomMasterForm.SetIsChild(const Value: boolean);
begin
  if fIsChild = Value then Exit;
  fIsChild := Value;
  RecreateWnd;
end;

procedure TCustomMasterForm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  { check afwijkende waarden }
  if fIsChild then
  begin
    //Params.WndParent := (Owner as TTabSheet).Handle; ???
    Params.Style := WS_CHILD or WS_CLIPSIBLINGS;
    Params.X := 0;
    Params.Y := 0;
  end;
  (*if not fCaptionBar then
  begin
    Params.Style := Params.Style and not (WS_CAPTION or WS_BORDER);
//    Log(['geen caption']);
  end;*)

end;

procedure TCustomMasterForm.Loaded;
begin
  inherited Loaded;
  if fIsChild then
  begin
    Visible := False;
    BorderIcons := [];
    BorderStyle := bsNone;
    HandleNeeded;
    SetBounds(0, 0, Width, Height);
  end;
end;

procedure TCustomMasterForm.SetTransparancy(const Value: Byte);
begin
  if fTransparancy = Value then Exit;
  fTransparancy := Value;
  SetTransparentForm(Self.Handle, fTransparancy)
end;

procedure TCustomMasterForm.SetCaptionBar(const Value: boolean);
begin
  if fCaptionBar = Value then Exit;
  fCaptionBar := Value;
  RecreateWnd;
end;

procedure TCustomMasterForm.CMAutoSize(var Message: TMessage);
var
  i: Integer;
  W, H: Integer;
begin
//  if not HandleAllocated then Exit;
  //Log(['autosize']);
  if fAutoSizing then Exit;
  fAutoSizing := True;

  W := 0;
  H := 0;
  for i := 0 to ControlCount - 1 do
  begin
    W := Max(W, Controls[i].Width);
    H := Max(H, Controls[i].Height);
  end;

  fAutoSizing := False;
  ClientWidth := W;
  ClientHeight := H;

//  Log([w,h]);
end;

procedure TCustomMasterForm.WMSetFocus(var Message: TWMSetFocus);
begin
  inherited;
//  Log(['ffffffff']);
end;


procedure TCustomMasterForm.SetZOrder(TopMost: Boolean);
begin
  inherited;
//  Log(['zzzzz']);
end;

procedure TCustomMasterForm.SetAcceptDraggedFiles(const Value: Boolean);
begin
  if fAcceptDraggedFiles = Value then
    Exit;
  fAcceptDraggedFiles := Value;
  DragAcceptFiles(Handle, fAcceptDraggedFiles);
end;

procedure TCustomMasterForm.WMEraseBkgnd(var Message: TWmEraseBkgnd);
begin
  if fEraseBk then
    inherited
  else
    Message.Result := 1;
end;

procedure TCustomMasterForm.WMDropFiles(var Msg: TWMDropFiles);
var
  CFileName: array[0..MAX_PATH] of Char;
  Files: TStringList;
  i: integer;
begin
  i := 0;
//  windlg('drop iets');
  Files := TStringList.Create;
  try
    Msg.Result := 0;
    while DragQueryFile(Msg.Drop, i, CFileName, MAX_PATH) > 0 do
    begin
      Files.Add(string(CFileName));
      Inc(i);
    end;
    if Assigned(fOnDropFiles) then
      fOnDropFiles(Self, Files);
  finally
    DragFinish(Msg.Drop);
    Files.Free;
  end;
end;

{ TCustomMasterChildForm }

procedure TCustomMasterChildForm.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  //Params.WndParent := (Owner as TTabSheet).Handle; ???
  Params.Style := WS_CHILD or WS_CLIPSIBLINGS;
  Params.X := 0;
  Params.Y := 0;
end;

procedure TCustomMasterChildForm.Loaded;
begin
  inherited Loaded;
  Visible := False;
  BorderIcons := [];
  BorderStyle := bsNone;
  HandleNeeded;
  SetBounds(0, 0, Width, Height);
end;

{ TFlatForm }

constructor TFlatForm.Create(aOwner: TComponent);
begin
  inherited;
  ControlStyle := [];
  BorderStyle := bsNone;
//  BorderW
end;

procedure TFlatForm.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if BorderStyle = bsNone then
    Params.style := params.style or ws_border;
end;

procedure TFlatForm.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
//  Log(['mouse']);
end;

procedure TFlatForm.WMNCHitTest(var Message: TWMNCHitTest);
begin
  Message.Result := HTCAPTION;
{  with Message do
  begin
    if (csDesigning in ComponentState) and (FParent <> nil) then
      Result := HTCLIENT
    else
      inherited;
  end; }
end;

end.


