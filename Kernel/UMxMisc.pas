unit UMxMisc;

interface

uses
  Classes, Types, Graphics, SysUtils, Windows,
  UMisc;

{-------------------------------------------------------------------------------
  Misc consts
-------------------------------------------------------------------------------}
const //umisc
  EditChars   = ['''', '"', #32..#255];
  SearchChars = ['a'..'z', 'A'..'Z', ' ', '1'..'9', '0'];

  MinInt      = Low(Integer);
  CrLf        = Chr(13) + Chr(10);

(*  Bit0  = 1;         {1}
  Bit1  = 1 shl 1;   {2}
  Bit2  = 1 shl 2;   {4}
  Bit3  = 1 shl 3;   {8}
  Bit4  = 1 shl 4;   {16}
  Bit5  = 1 shl 5;   {32}
  Bit6  = 1 shl 6;   {64}
  Bit7  = 1 shl 7;   {128}
  Bit8  = 1 shl 8;   {256}
  Bit9  = 1 shl 9;   {512}
  Bit10 = 1 shl 10;  {1024}
  Bit11 = 1 shl 11;  {2048}
  Bit12 = 1 shl 12;  {4096}
  Bit13 = 1 shl 13;  {8192}
  Bit14 = 1 shl 14;  {16384}
  Bit15 = 1 shl 15;  {32768}
  Bit16 = 1 shl 16;
  Bit17 = 1 shl 17;
  Bit18 = 1 shl 18;
  Bit19 = 1 shl 19;
  Bit20 = 1 shl 20;
  Bit21 = 1 shl 21;
  Bit22 = 1 shl 22;
  Bit23 = 1 shl 23;
  Bit24 = 1 shl 24;
  Bit25 = 1 shl 25;
  Bit26 = 1 shl 26;
  Bit27 = 1 shl 27;
  Bit28 = 1 shl 28;
  Bit29 = 1 shl 29;
  Bit30 = 1 shl 30;
  Bit31 = 1 shl 31; *)

{-------------------------------------------------------------------------------
  Misc classes
-------------------------------------------------------------------------------}
type
  TPersistentWithOwner = class(TPersistent)
  private
    fOwner: TPersistent;
  protected
    function GetOwner: TPersistent; override;
    procedure CheckOwnerType(aOwner: TPersistent; const Allowed: array of TClass);
    function FindOwner(aClass: TClass): TPersistent;
    function GetMaster: TPersistent;
  public
    constructor Create(aOwner: TPersistent);
    property Owner: TPersistent read fOwner;
  end;

{-------------------------------------------------------------------------------
  Misc functions
-------------------------------------------------------------------------------}
(*function Between(X, A, B: integer): boolean;
function CutLeft(const S: string; aLen: integer): string;
function EqualPoints(const P1, P2: TPoint): boolean;
function EqualRects(const R1, R2: TRect): boolean;
procedure RectGrow(var aRect: TRect; DeltaX, DeltaY: integer);
function RectHeight(const aRect: TRect): integer;
function RectWidth(const aRect: TRect): integer;
procedure RectMove(var R: TRect; X, Y: integer); {windows.offsetrect}
procedure SwapInts(var A, B: integer);
function ZeroTopLeftRect(const aRect: TRect): TRect;
procedure Restrict(var aValue: integer; aMin, aMax: integer);
function RestrictValue(aValue: integer; aMin, aMax: integer): Integer;
function Restricted(var aValue: integer; aMin, aMax: integer): Boolean;
function IIF(aExpr: boolean; const aTrue, aFalse: integer): integer;

function i2s(i: integer): string;
function RectStr(const R: TRect): string; *)
procedure ExtTextOutColored(Canvas: TCanvas;
                            const Text: string;
                            R: TRect;
                            WinFlags: DWORD;
                            const HighLightStrings: array of string;
                            const TextColors: array of TColor;
                            const BackColors: array of TColor;
                            const FontStyles: array of TFontStyles);

implementation

//uses
  //UMx;

(*
function Between(X, A, B: integer): boolean;
begin
  Result := (X >= A) and (X <= B);
end;

function CutLeft(const S: string; aLen: integer): string;
begin
  Result := Copy(S, aLen + 1, Length(S));
end;

function EqualPoints(const P1, P2: TPoint): boolean;
begin
  Result := (P1.X = P2.X) and (P1.Y = P2.Y);//CompareMem(@P1, @P2, SizeOf(TPoint));
end;

function EqualRects(const R1, R2: TRect): boolean;
begin
  Result := CompareMem(@R1, @R2, SizeOf(TRect));
end;

procedure RectGrow(var aRect: TRect; DeltaX, DeltaY: integer);
begin
  with aRect do
  begin
    Inc(Right, DeltaX); Dec(Left, DeltaX);
    Inc(Bottom, DeltaY);  Dec(Top, DeltaY);
  end;
end;

function RectHeight(const aRect: TRect): integer;
begin
  with aRect do Result := Bottom - Top + 1;
end;

function RectWidth(const aRect: TRect): integer;
begin
  with aRect do Result := Right - Left + 1;
end;

procedure RectMove(var R: TRect; X, Y: integer);
begin
  with R do
  begin
    if X <> 0 then
    begin
      Inc(R.Left, X);
      Inc(R.Right, X);
    end;
    if Y <> 0 then
    begin
      Inc(R.Top, Y);
      Inc(R.Bottom, Y);
    end;
  end;
end;

procedure SwapInts(var A, B: integer);
begin
  a := a xor b;
  b := b xor a;
  a := a xor b;
end;

function ZeroTopLeftRect(const aRect: TRect): TRect;
begin
  Result := aRect;
  with Result do
  begin
    Dec(Right, Left);
    Dec(Bottom, Top);
    Left := 0;
    Top := 0;
  end;
end;

procedure Restrict(var aValue: integer; aMin, aMax: integer);
begin
  if aMin > aMax then SwapInts(aMin, aMax);
  if aValue < aMin then
    aValue := aMin else
  if aValue > aMax then
    aValue := aMax
{  else
    aValue := aMin; }
end;

function RestrictValue(aValue: integer; aMin, aMax: integer): Integer;
begin
  Result := aValue;
  Restrict(Result, aMin, aMax);
//  if aMin > aMax then SwapInts(aMin, aMax);
//  if aValue < aMin then aValue := aMin else if aValue > aMax then aValue := aMax;
end;

function Restricted(var aValue: integer; aMin, aMax: integer): Boolean;
begin
  Result := False;
  if aMin > aMax then SwapInts(aMin, aMax);
  if aValue < aMin then
  begin
    Result := True;
    aValue := aMin
  end
  else if aValue > aMax then begin
    Result := True;
    aValue := aMax;
  end;
end;

function IIF(aExpr: boolean; const aTrue, aFalse: integer): integer; 
begin
  if aExpr then Result := aTrue else Result := aFalse;
end;
*)

{ TPersistentWithOwner }

procedure TPersistentWithOwner.CheckOwnerType(aOwner: TPersistent; const Allowed: array of TClass);
var
  i: integer;
  OK: boolean;
  S: string;
begin
  OK := False;
  for i := 0 to Length(Allowed) - 1 do
  begin
    if Allowed[i] <> nil then
      OK := OK or (aOwner is Allowed[i])
    else
      OK := True;
    if OK then
      Break;  
  end;

  if not OK then
  begin
    if aOwner <> nil then S := aOwner.ClassName else S := 'nil';
    raise Exception.CreateFmt('CheckOwnerType: %s is een ongeldig Owner type',[S]);
  end;
end;

constructor TPersistentWithOwner.Create(aOwner: TPersistent);
begin
  fOwner := aOwner;
end;

function TPersistentWithOwner.GetOwner: TPersistent;
begin
  Result := fOwner;
end;

type
  THackedPersistent = class(TPersistent);

function TPersistentWithOwner.FindOwner(aClass: TClass): TPersistent;
var
  O: TPersistent;
begin
  O := Self;
  Result := nil;
  while O <> nil do
  begin
    O := THackedPersistent(O).GetOwner;
    if O = nil then
      Exit;
    if O is aClass then
    begin
      Result := O;
      Exit;
    end;
  end;
end;

function TPersistentWithOwner.GetMaster: TPersistent;
var
  O: TPersistent;
begin
  Result := nil;
  O := Self;
  while O <> nil do
  begin
    O := THackedPersistent(O).GetOwner;
    if O <> nil then
      Result := O
    else
      Exit;
  end;
  Result := nil;
end;
(*

function i2s(i: integer): string;
begin
  Str(i, Result);
end;

function RectStr(const R: TRect): string;
begin
  Result := i2s(R.Left) + ', ' + i2s(R.Top) + ', ' + i2s(R.Right)+ ', ' + i2s(R.Bottom)
end;
*)

type
  TDrawState = (dsNone, dsIgnore, dsChar, dsNextLine, dsReady);

procedure ExtTextOutColored(Canvas: TCanvas;
                            const Text: string;
                            R: TRect;
                            WinFlags: DWORD;
                            const HighLightStrings: array of string;
                            const TextColors: array of TColor;
                            const BackColors: array of TColor;
                            const FontStyles: array of TFontStyles);
type
  THighLightInfo = record
    HighLightStr : PString;
    HLen         : Integer;
    MatchPos     : Integer;
    Match        : Boolean;
    TC           : TColor;
    BC           : TColor;
    Style        : TFontStyles;
  end;

const
  MaxInfo = 4;                        //ugraphics

var
  Info: array[0..MaxInfo - 1] of THighLightInfo;
  Count, i, {BkMode, }TextLen, NormalLen, X, Y, First : Integer;
  StartChar, CurrChar, BeginNormal: PChar;
  SaveBrushColor, SaveFontColor: TColor;
  SaveFontStyle: TFontStyles;
  State: TDrawState;
  TextSize: TSize;

    procedure DoDraw(const P: PChar;  L, i: Integer; DoHighlight: Boolean);
    var
      S: TSize;
    begin
      if L <= 0 then Exit;
      with Info[i], Canvas do
      begin
        GetTextExtentPoint32(Handle, P, L, S); // bereken size
        if not DoHighLight then
        begin
          //Log(['draw normal', getstring(p,l)]);
          SetBkMode(Canvas.Handle, TRANSPARENT);
          Font.Color := SaveFontColor;
        end
        else begin
          //Log(['draw highlight', getstring(p,l)]);
          if BC <> clNone then
          begin
            SetBkMode(Canvas.Handle, OPAQUE);
            Brush.Color := BC;
          end
          else  // niet gedefinieerde achtergrondkleur: neem default
            SetBkMode(Canvas.Handle, TRANSPARENT);
          if TC <> clNone then
            Font.Color := Info[i].TC
          else  // niet gedefinieerde textkleur: neem default
            Font.Color := SaveFontColor;
{          if Style <> [] then
            Font.Style := Style
          else
            Font.Style := SaveFontStyle; }
        end;
        ExtTextOut(Canvas.Handle, X, Y, ETO_CLIPPED, @R, P, L, nil); // en display die hap
      end;
      Inc(X, S.cx); // pas x positie aan
    end;


begin
  TextLen := Length(Text);
  if TextLen = 0 then  Exit;
  Count := Length(HighlightStrings);
  First := -1;
  //Log([count]);
  for i := 0 to Count - 1 do
    if i < MaxInfo then
      with Info[i] do
      begin
        HighlightStr := @HighLightStrings[i];
        HLen := Length(HighLightStr^);
        MatchPos := 1;
        Match := False;
        if Length(TextColors) > i then TC := TextColors[i] else TC := clNone;
        if Length(BackColors) > i then BC := BackColors[i] else BC := clNone;
        if Length(FontStyles) > i then Style := FontStyles[i] else Style := [];
        if First = -1 then
          if HighlightStr^ <> '' then
            First := i;
      end;

//  Assert(First >= 0, 'UGraphics error');
  if First = -1 then First := 0;    

  StartChar := PChar(Text);
  CurrChar := StartChar;
  BeginNormal := StartChar;
  X := R.Left;
  Y := R.Top;
  GetTextExtentPoint32(Canvas.Handle, PChar(Text), TextLen, TextSize);

  { zet x }
  if WinFlags and DT_RIGHT <> 0 then
  begin
    X := R.Right - TextSize.cx;
  end
  else if WinFlags and DT_CENTER <> 0 then
  begin
    X := (R.Right + R.Left) div 2 - TextSize.cx div 2;
  end;

  { zet y }
  if WinFlags and DT_VCENTER <> 0 then
  begin
    Y := (R.Bottom + R.Top) div 2 - TextSize.cy div 2;
  end
  else if WinFlags and DT_BOTTOM <> 0 then
  begin
    Y := R.Bottom - TextSize.cy;
  end;

  with Canvas do
  begin
    SaveFontColor := Font.Color;
    SaveBrushColor := Brush.Color;
    SaveFontStyle := Font.Style;
    State := dsNone;

    while State <> dsReady do
    begin
      case CurrChar^ of
        #32..#255:
          begin
            State := dsChar; // #EL todo check wordbreak spatie/tab
          end;
        #13:
          begin
            State := dsNextLine;
          end;
        #0:
          begin
            State := dsReady;
          end;
      else
        State := dsIgnore;
      end;

      case State of
        dsChar:
          begin
            for i := First to Count - 1 do
              with Info[i] do
                if HLen > 0 then
                begin
                  { test charmatch }
                  Match := UpCase(CurrChar^) = Upcase(HighLightStr^[MatchPos]);
                  if not Match and (MatchPos > 1) then
                  begin
                    MatchPos := 1;
                    Match := UpCase(CurrChar^) = Upcase(HighLightStr^[MatchPos]);
                  end;
                  if Match then Break;
                end;


            for i := First to Count - 1 do
              with Info[i] do
              begin
                { match }
                if Match then
                begin
                  if MatchPos = HLen then
                  begin
                    { teken het voorafgaande stuk normaal }
                    NormalLen := CurrChar - HLen + 1 - BeginNormal;
                    if NormalLen > 0 then
                      DoDraw(BeginNormal, NormalLen, i ,False);
                    { teken daarna het stuk highlight }
                    DoDraw(CurrChar - HLen + 1, HLen, i, True);
                    BeginNormal := CurrChar + 1;
                    MatchPos := 0;
                  end
                end
                { non match }
                else begin
                  MatchPos := 0;
                end;

              Inc(MatchPos);
              if MatchPos > HLen then
                MatchPos := 1;
            end; //for i
          end; // dsChar
        dsReady:
          begin
            NormalLen := CurrChar - BeginNormal;
            DoDraw(BeginNormal, NormalLen, 0{i}, False);
          end; // dsReady
      end;
      Inc(CurrChar);
    end; // while not Ready

    Brush.Color := SaveBrushColor;
    Font.Color := SaveFontColor;
    Font.Style := SaveFontStyle;
  end; // with Canvas

end;

end.


