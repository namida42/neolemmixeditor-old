unit MxColumns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, Db,
  Mx;

type
  TFormColumns = class(TForm)
    ListboxColumns: TCheckListBox;
    BitBtn1: TBitBtn;
  private
    fMatrix: TDBMatrix;
    procedure SetMatrix(const Value: TDBMatrix);
    procedure AdjustMatrix;
  public
    property Matrix: TDBMatrix read fMatrix write SetMatrix;
  end;

//var
//  FormColumns: TFormColumns;

function FormColumnsExecute(aMatrix: TDBMatrix): word;

implementation

{$R *.DFM}

uses
  ULog;

function FormColumnsExecute(aMatrix: TDBMatrix): word;
var
  F: TFormColumns;
begin
  F := TFormColumns.Create(Application);
  try
    F.Matrix := aMatrix;
    Result := F.ShowModal;
    if Result = mrOK then
    begin
      F.AdjustMatrix;
//      Write
    end;
  finally
    F.Free;
  end;
end;

{ TFormColumns }

procedure TFormColumns.AdjustMatrix;
var
  i: Integer;
  Fld: TField;
begin
  with ListboxColumns do
  begin
    for i := 0 to Items.Count - 1 do
    begin
      Fld := TField(Items.Objects[i]);
      // checked kolommen toevoegen
      if Checked[i] then
      begin
        if fMatrix.Columns.FindColumnByFieldName(Fld.FieldName) = nil then
        begin
          with fMatrix.Columns.Add do
          begin
            Field := Fld;
          end;
        end;
      end
      // unchecked kolommen weghalen
      else begin
{        if fMatrix.Columns.FindColumnByFieldName(Fld.FieldName) <> nil then
        begin
          with fMatrix.Columns.Add do
          begin
            Field := Fld;
          end;
        end; }
      end;
    end;
  end;
end;

procedure TFormColumns.SetMatrix(const Value: TDBMatrix);
var
  L: TStringList;
  i: Integer;
  aCol: TMatrixColumn;
  Fld: TField;
begin
  fMatrix := Value;
  if not fMatrix.DataSet.Active then
    Exit;
  with fMatrix, DataSet do
  begin
    for i := 0 to FieldCount - 1 do
    begin
      Fld := Fields[i];
      aCol := Columns.FindColumnByFieldName(Fld.FieldName);
      ListboxColumns.Items.AddObject(Fld.DisplayName, Fld); // beschikbare kolom (veld)
      if (aCol <> nil) and (aCol.Visible) then     
      begin
        ListboxColumns.Checked[i] := True; // zichtbare kolom (veld)
      end;
    end;
  end;
end;

end.

