unit uDebug;

{*************************************************************

  Unit Debug (c) Eric Langedijk

  Als deze unit als eerste gedeclareerd wordt in
  de de project-source dan kan er een betrouwbare
  memory-lek-check gedaan worden

**************************************************************}

interface

{$IFNDEF EXEVERSION}

var
  StartMem   : Cardinal;
  MemoryLeak : Cardinal;

{$ENDIF}

implementation

{$IFNDEF EXEVERSION}

uses
  Sysutils, Windows;

initialization
  StartMem := GetHeapStatus.TotalAllocated;
finalization
  MemoryLeak := GetHeapStatus.TotalAllocated - StartMem;
  { gebruik windows messagebox, omdat vcl al dicht kan zijn }
  {$ifdef mlcheck}if MemoryLeak <> 0 then
    MessageBox(0, PChar('memory leak: ' + IntToStr(MemoryLeak)), PChar('error'), mb_OK){$endif}

{$ENDIF}
end.
