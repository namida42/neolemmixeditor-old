unit FCodeEditCollection;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,
  UCollection;

type
  TFormEditCollection = class(TForm)
    EditItemName: TEdit;
    BtnCancel: TBitBtn;
    BtnInterface: TBitBtn;
    BtnImplementation: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    ComboListType: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BtnInterfaceClick(Sender: TObject);
    procedure BtnImplementationClick(Sender: TObject);
  private
    fEditResult : TCollectionCreateMode;
    function GetItemClassName: string;
    function GetListType: TListType;
    procedure SetItemClassName(const Value: string);
    procedure SetListType(const Value: TListType);
  public
    property ItemClassName: string read GetItemClassName write SetItemClassName;
    property ListType: TListType read GetListType write SetListType;
  end;


function InputCollectionItem(var aItemClassName: string;
  var aListType: TListType; var EditResult: TCollectionCreateMode): Boolean;

implementation

{$R *.dfm}

uses
  TypInfo;

var
  GlobalLastItem: string = 'TMyItem';
  GlobalLastType: TListType = Low(TListType);

function InputCollectionItem(var aItemClassName: string;
  var aListType: TListType; var EditResult: TCollectionCreateMode): Boolean;
var
  FormEditCollection: TFormEditCollection;
begin
  Result := False;
  EditResult := ccmNone;

  // init result
  aItemClassName := GlobalLastItem;
  aListType := GlobalLastType;

  FormEditCollection := TFormEditCollection.Create(nil);
  try
    with FormEditCollection do
    begin
      ItemClassName := aItemClassName;
      ListType := aListType;

      Result := ShowModal = mrOK;
      if Result then
      begin
        aItemClassName := ItemClassName;
        aListType := ListType;
        EditResult := fEditResult;
        GlobalLastItem := aItemClassname;
        GlobalLastType := aListType;
      end;
    end;
  finally
    FormEditCollection.Free;
  end;
end;



procedure TFormEditCollection.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then
    if EditItemName.Text = '' then
    begin
      ShowMessage('Invalid input');
      CanClose := False;
    end;
end;

procedure TFormEditCollection.BtnInterfaceClick(Sender: TObject);
begin
  fEditResult := ccmInterface;
end;

procedure TFormEditCollection.BtnImplementationClick(Sender: TObject);
begin
  fEditResult := ccmImplementation;
end;

procedure TFormEditCollection.FormCreate(Sender: TObject);
var
  T: TListType;
  S: string;
begin
  for T := Low(TListType) to High(TListType) do
  begin
    S := GetEnumName(TypeInfo(TListType), Integer(T));
    S := Copy(S, 3, Length(S));
    ComboListType.Items.Add(S);
  end;
  ComboListType.ItemIndex := 0;
end;

function TFormEditCollection.GetItemClassName: string;
begin
  Result := EditItemName.Text;
end;

function TFormEditCollection.GetListType: TListType;
begin
  Result := TListType(ComboListType.ItemIndex)
end;

procedure TFormEditCollection.SetItemClassName(const Value: string);
begin
  EditItemName.Text := Value;
end;

procedure TFormEditCollection.SetListType(const Value: TListType);
begin
  ComboListType.ItemIndex := Integer(Value);
end;

end.

