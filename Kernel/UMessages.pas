unit UMessages;

interface

uses
  Messages;

type
  TCMAutoSize = packed record
    Msg: Cardinal;
    Unused: array[0..3] of Word;
    Result: Longint;
  end;

const
  CM_AUTOSIZE = WM_APP + 32;


implementation

end.

