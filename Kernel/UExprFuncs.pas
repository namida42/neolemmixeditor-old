unit UExprFuncs;

interface

uses
  Classes;

{ TEvaluator default functies }
function _Date(aParamCount: integer; const aParams: array of Variant): Variant;
function _Concat(aParamCount: integer; const aParams: array of Variant): Variant;
function _RepStr(aParamCount: integer; const aParams: array of Variant): Variant;
function _Its(aParamCount: integer; const aParams: array of Variant): Variant;
function _Sti(aParamCount: integer; const aParams: array of Variant): Variant;
function _FloatToStr(aParamCount: integer; const aParams: array of Variant): Variant;
//function _Add(aParamCount: integer; const aParams: array of Variant): Variant;

type
  TFunctionWrapper = class(TPersistent)
  private
    fx: TNotifyEvent;
  published
    procedure DoIets(const S: string);
    property EvDoeIets: TNotifyEvent read fx write fx;
  end;

implementation

uses
  UMisc, SysUtils;

{ paramcount : 1
  param 1    : varString
  result     : varDate }
function _Date(aParamCount: integer; const aParams: array of Variant): Variant;
begin
  Result := StrToDate(string(aParams[0]));
  TVarData(Result).VType := varDate;
end;

{ paramcount : -1
  params     : array of varString
  result     : varString }
function _Concat(aParamCount: integer; const aParams: array of Variant): Variant;
var
  i: integer;
begin
  Result := '';
  for i := 0 to aParamCount - 1 do
    Result := Result + string(aParams[i]);
end;

{ paramcount : 2
  param 1    : varString
  param 2    : varInteger
  result     : varString }
function _RepStr(aParamCount: integer; const aParams: array of Variant): Variant;
var
  i, S, L: integer;
  St: string;
  A1: pointer;
begin
  L := integer(aParams[1]);
  S := Length(string(aParams[0]));
  if (L <= 0) or (S = 0) then
    begin Result := ''; Exit; end;
  A1 := @string(aParams[0])[1];
  SetLength(St, S * L);
  for i := 0 to L - 1 do
    Move(A1^, St[i * S + 1], S);
  Result := St;
end;

{ paramcount : 1
  param 1    : varInteger
  result     : varString }
function _Its(aParamCount: integer; const aParams: array of Variant): Variant;
var
  i: integer;
begin
  Result := i2s(integer(aParams[0]){.VInteger});
  TVarData(Result).VType := varString;
end;

{ paramcount : 1
  param 1    : varString
  result     : varInteger }
function _Sti(aParamCount: integer; const aParams: array of Variant): Variant;
begin
  Result := s2i(string(aParams[0]))
//  Result := s2i(string(TVarData(aParams[0]).VString))
end;

{ paramcount : 1
  param 1    : varDouble
  result     : varString }
function _FloatToStr(aParamCount: integer; const aParams: array of Variant): Variant;
begin
  Result := FloatToStr(Double(aParams[0]))
end;

{ TFunctionWrapper }

procedure TFunctionWrapper.DoIets(const S: string);
begin

end;

end.

