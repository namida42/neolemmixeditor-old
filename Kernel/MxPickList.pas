unit MxPickList;

interface

uses
  Windows, Forms, Classes, Db,
  Mx, UMisc, UMasterForms;

type
  TMxPickList = class(TComponent)
  private
    fForm: TCustomMasterForm;
    fMatrix: TDBMatrix;
    fDataSource: TDataSource;
    fVisibleColumns: string;
    fReturnColumns: string;
    fReturnValues: TOpenVariantArray;
  protected
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function Execute: Boolean;
    property ReturnValues: TOpenVariantArray read fReturnValues;
  published
    property DataSource: TDataSource read fDataSource write fDataSource;
    property VisibleColumns: string read fVisibleColumns write fVisibleColumns;
    property ReturnColumns: string read fReturnColumns write fReturnColumns;
  end;


procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Matrix', [TMxPickList]);
end;


{ TMxPickList }

constructor TMxPickList.Create(aOwner: TComponent);
begin
  inherited;
  fForm := TCustomMasterForm.Create(Application);
  fMatrix := TDBMatrix.Create(fForm);
end;

destructor TMxPickList.Destroy;
begin
  fForm.Free;
  inherited;
end;

function TMxPickList.Execute: Boolean;
begin
  Result := False;
  if (fDataSource = nil) or (fVisibleColumns = '') then Exit;
  try
    fForm.ShowModal;
    Result := True;
  finally
  end;
end;

end.

