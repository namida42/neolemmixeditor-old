with Card do
  begin
    AddCompoundDBEdit(1,0,AGENDA_ID.Fieldname);
    AddCompoundDBEdit(1,1,DATUM.Fieldname);
    AddCompoundDBEdit(1,2,TIJD.Fieldname);
    AddCompoundDBEdit(1,3,DUUR.Fieldname);
    AddCompoundDBEdit(1,4,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,5,MELDING_ID.Fieldname);
    AddCompoundDBEdit(1,6,HERHALING_ID.Fieldname);
    AddCompoundDBEdit(1,7,VERWIJDERING_ID.Fieldname);
    AddCompoundDBEdit(1,8,STATUS_ID.Fieldname);
    AddCompoundDBEdit(1,9,WANNEER.Fieldname);
    AddCompoundDBEdit(1,10,WANNEER_ID.Fieldname);
    AddCompoundDBEdit(1,11,AGENDA_GROEP_ID.Fieldname);
    AddCompoundDBEdit(1,12,COUNTER_ID.Fieldname);
    AddCompoundDBEdit(1,13,RESULTAAT_ID.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,AGENDA_GROEP_ID.Fieldname);
    AddCompoundDBEdit(1,1,GROEPNAAM.Fieldname);
    AddCompoundDBEdit(1,2,DEF_MELDING_ID.Fieldname);
    AddCompoundDBEdit(1,3,DEF_HERHALING_ID.Fieldname);
    AddCompoundDBEdit(1,4,DEF_VERWIJDERING_ID.Fieldname);
    AddCompoundDBEdit(1,5,HEEFTDUUR.Fieldname);
    AddCompoundDBEdit(1,6,HEEFTTIJD.Fieldname);
    AddCompoundDBEdit(1,7,ISZAKELIJK.Fieldname);
    AddCompoundDBEdit(1,8,ISPRIVE.Fieldname);
    AddCompoundDBEdit(1,9,HERHALING_ID.Fieldname);
    AddCompoundDBEdit(1,10,PICTOGRAM.Fieldname);
    AddCompoundDBEdit(1,11,BESTAND.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,AR_BOEKCODE_ID.Fieldname);
    AddCompoundDBEdit(1,1,CODE.Fieldname);
    AddCompoundDBEdit(1,2,OMSCHRIJVING.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,AR_GROEP_ID.Fieldname);
    AddCompoundDBEdit(1,1,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,2,SLEUTELS_OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,3,SLEUTELS_TEGENNAAM.Fieldname);
    AddCompoundDBEdit(1,4,SLEUTELS_TEGENREKENING.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,AR_REKENING_ID.Fieldname);
    AddCompoundDBEdit(1,1,NUMMER.Fieldname);
    AddCompoundDBEdit(1,2,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,3,BEGINSALDO.Fieldname);
    AddCompoundDBEdit(1,4,HUIDIGSALDO.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,AR_TRANSACTIE_ID.Fieldname);
    AddCompoundDBEdit(1,1,AR_REKENING_ID.Fieldname);
    AddCompoundDBEdit(1,2,INS.Fieldname);
    AddCompoundDBEdit(1,3,MUT.Fieldname);
    AddCompoundDBEdit(1,4,BEDRAG.Fieldname);
    AddCompoundDBEdit(1,5,TEGENREKENING.Fieldname);
    AddCompoundDBEdit(1,6,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,7,RENTEDATUM.Fieldname);
    AddCompoundDBEdit(1,8,BOEKDATUM.Fieldname);
    AddCompoundDBEdit(1,9,TEGENNAAM.Fieldname);
    AddCompoundDBEdit(1,10,AR_BOEKCODE_ID.Fieldname);
    AddCompoundDBEdit(1,11,OMSCHRIJVING_KEY.Fieldname);
    AddCompoundDBEdit(1,12,BATCH_STAMP.Fieldname);
    AddCompoundDBEdit(1,13,AR_GROEP_ID.Fieldname);
    AddCompoundDBEdit(1,14,INFO.Fieldname);
    AddCompoundDBEdit(1,15,AUTOGROEP.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,AR_WEBSITES_ID.Fieldname);
    AddCompoundDBEdit(1,1,URL.Fieldname);
    AddCompoundDBEdit(1,2,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,3,ZOEKSLEUTELS.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,WIJZIGEN.Fieldname);
    AddCompoundDBEdit(1,1,UITVOEREN.Fieldname);
    AddCompoundDBEdit(1,2,CONTROLLER_ID.Fieldname);
    AddCompoundDBEdit(1,3,DATASETNAAM.Fieldname);
    AddCompoundDBEdit(1,4,STATEMENT.Fieldname);
    AddCompoundDBEdit(1,5,LEZEN.Fieldname);
    AddCompoundDBEdit(1,6,TOEVOEGEN.Fieldname);
    AddCompoundDBEdit(1,7,VERWIJDEREN.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,TABEL.Fieldname);
    AddCompoundDBEdit(1,1,LOOKUPDATASET.Fieldname);
    AddCompoundDBEdit(1,2,LINKASSIGN.Fieldname);
    AddCompoundDBEdit(1,3,DECIMALEN.Fieldname);
    AddCompoundDBEdit(1,4,VELDNAAM.Fieldname);
    AddCompoundDBEdit(1,5,LOOKUPDATASETNAAM.Fieldname);
    AddCompoundDBEdit(1,6,LINKKEY.Fieldname);
    AddCompoundDBEdit(1,7,SEGMENT_GROOTTE.Fieldname);
    AddCompoundDBEdit(1,8,ALIAS.Fieldname);
    AddCompoundDBEdit(1,9,LOOKUPWHERE.Fieldname);
    AddCompoundDBEdit(1,10,MINIMUM.Fieldname);
    AddCompoundDBEdit(1,11,TABVOLGORDE.Fieldname);
    AddCompoundDBEdit(1,12,PROMPT.Fieldname);
    AddCompoundDBEdit(1,13,LINKTABEL.Fieldname);
    AddCompoundDBEdit(1,14,MAXIMUM.Fieldname);
    AddCompoundDBEdit(1,15,DEFAULTWAARDE.Fieldname);
    AddCompoundDBEdit(1,16,CONTROLLERVELD_ID.Fieldname);
    AddCompoundDBEdit(1,17,DBTYPE.Fieldname);
    AddCompoundDBEdit(1,18,LINKTITEL.Fieldname);
    AddCompoundDBEdit(1,19,STARTDT.Fieldname);
    AddCompoundDBEdit(1,20,DEFAULTMETHODE.Fieldname);
    AddCompoundDBEdit(1,21,CONTROLLER_ID.Fieldname);
    AddCompoundDBEdit(1,22,MASTERTYPE.Fieldname);
    AddCompoundDBEdit(1,23,LINKFILTER.Fieldname);
    AddCompoundDBEdit(1,24,STOPDT.Fieldname);
    AddCompoundDBEdit(1,25,VERWACHTVELD.Fieldname);
    AddCompoundDBEdit(1,26,LEZEN.Fieldname);
    AddCompoundDBEdit(1,27,FIELDFUNC.Fieldname);
    AddCompoundDBEdit(1,28,LINKSELECT.Fieldname);
    AddCompoundDBEdit(1,29,HINTKORT.Fieldname);
    AddCompoundDBEdit(1,30,VEREISTVELD.Fieldname);
    AddCompoundDBEdit(1,31,TOEVOEGEN.Fieldname);
    AddCompoundDBEdit(1,32,INPUTFORMAAT.Fieldname);
    AddCompoundDBEdit(1,33,LINKSHOW.Fieldname);
    AddCompoundDBEdit(1,34,HINTLANG.Fieldname);
    AddCompoundDBEdit(1,35,VERWIJDEREN.Fieldname);
    AddCompoundDBEdit(1,36,WEERGAVEFORMAAT.Fieldname);
    AddCompoundDBEdit(1,37,LINKPROMPT.Fieldname);
    AddCompoundDBEdit(1,38,OPMERKINGEN.Fieldname);
    AddCompoundDBEdit(1,39,WIJZIGEN.Fieldname);
    AddCompoundDBEdit(1,40,LEGALEWAARDEN.Fieldname);
    AddCompoundDBEdit(1,41,LINKRETURN.Fieldname);
    AddCompoundDBEdit(1,42,GROOTTE.Fieldname);
    AddCompoundDBEdit(1,43,UITVOEREN.Fieldname);
    AddCompoundDBEdit(1,44,ALLEENLEZEN.Fieldname);
    AddCompoundDBEdit(1,45,RANGECHECK.Fieldname);
    AddCompoundDBEdit(1,46,VERPLICHT.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_BASIS_ID.Fieldname);
    AddCompoundDBEdit(1,1,PRAKTIJKNAAM.Fieldname);
    AddCompoundDBEdit(1,2,PRAKTIJKHOUDER.Fieldname);
    AddCompoundDBEdit(1,3,PRAKTIJKCODE.Fieldname);
    AddCompoundDBEdit(1,4,AGBCODE.Fieldname);
    AddCompoundDBEdit(1,5,REGISTRATIENUMMER.Fieldname);
    AddCompoundDBEdit(1,6,ADRES.Fieldname);
    AddCompoundDBEdit(1,7,POSTCODE.Fieldname);
    AddCompoundDBEdit(1,8,PLAATS.Fieldname);
    AddCompoundDBEdit(1,9,LAND.Fieldname);
    AddCompoundDBEdit(1,10,TELEFOON.Fieldname);
    AddCompoundDBEdit(1,11,FAXNUMMER.Fieldname);
    AddCompoundDBEdit(1,12,MOBIELNUMMER.Fieldname);
    AddCompoundDBEdit(1,13,EMAIL.Fieldname);
    AddCompoundDBEdit(1,14,WEBSITE.Fieldname);
    AddCompoundDBEdit(1,15,LOGO.Fieldname);
    AddCompoundDBEdit(1,16,BANKNUMMER.Fieldname);
    AddCompoundDBEdit(1,17,GIRONUMMER.Fieldname);
    AddCompoundDBEdit(1,18,KVKNUMMER.Fieldname);
    AddCompoundDBEdit(1,19,BTWNUMMER.Fieldname);
    AddCompoundDBEdit(1,20,BTWPLICHTIG.Fieldname);
    AddCompoundDBEdit(1,21,BIJZONDERHEDEN.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_CODETABELHEAD_ID.Fieldname);
    AddCompoundDBEdit(1,1,NAAM.Fieldname);
    AddCompoundDBEdit(1,2,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,3,DATUM.Fieldname);
    AddCompoundDBEdit(1,4,VERSIE.Fieldname);
    AddCompoundDBEdit(1,5,LENGTE.Fieldname);
    AddCompoundDBEdit(1,6,VELDTYPE.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_CODETABELITEM_ID.Fieldname);
    AddCompoundDBEdit(1,1,DDM_CODETABELHEAD_ID.Fieldname);
    AddCompoundDBEdit(1,2,EI_CODE.Fieldname);
    AddCompoundDBEdit(1,3,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,4,USECOUNT.Fieldname);
    AddCompoundDBEdit(1,5,TOELICHTING.Fieldname);
    AddCompoundDBEdit(1,6,BIJZONDERHEDEN.Fieldname);
    AddCompoundDBEdit(1,7,BRON.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_DAGBOEK_ID.Fieldname);
    AddCompoundDBEdit(1,1,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,2,REKENINGNUMMER.Fieldname);
    AddCompoundDBEdit(1,3,BANKNAAM.Fieldname);
    AddCompoundDBEdit(1,4,BIJZONDERHEDEN.Fieldname);
    AddCompoundDBEdit(1,5,SOORT.Fieldname);
    AddCompoundDBEdit(1,6,DATUMBEGINSALDO.Fieldname);
    AddCompoundDBEdit(1,7,BEGINSALDO.Fieldname);
    AddCompoundDBEdit(1,8,HUIDIGEDATUM.Fieldname);
    AddCompoundDBEdit(1,9,HUIDIGESALDO.Fieldname);
    AddCompoundDBEdit(1,10,PERIODE_ID.Fieldname);
    AddCompoundDBEdit(1,11,PERIODETOTAALBIJ.Fieldname);
    AddCompoundDBEdit(1,12,PERIODETOTAALAF.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_DAGBOEKMUT_ID.Fieldname);
    AddCompoundDBEdit(1,1,DDM_DAGBOEK_ID.Fieldname);
    AddCompoundDBEdit(1,2,DATUM.Fieldname);
    AddCompoundDBEdit(1,3,BEDRAG.Fieldname);
    AddCompoundDBEdit(1,4,BIJAF_ID.Fieldname);
    AddCompoundDBEdit(1,5,GROEP_ID.Fieldname);
    AddCompoundDBEdit(1,6,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,7,BIJZONDERHEDEN.Fieldname);
    AddCompoundDBEdit(1,8,BIJWIE.Fieldname);
    AddCompoundDBEdit(1,9,TEGENREKENING.Fieldname);
    AddCompoundDBEdit(1,10,SALDO.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_EIEXPORT_ID.Fieldname);
    AddCompoundDBEdit(1,1,BATCH.Fieldname);
    AddCompoundDBEdit(1,2,DATUM.Fieldname);
    AddCompoundDBEdit(1,3,DDM_FACTUUR_ID.Fieldname);
    AddCompoundDBEdit(1,4,DDM_FACTREG_ID.Fieldname);
    AddCompoundDBEdit(1,5,RECORDTYPE_ID.Fieldname);
    AddCompoundDBEdit(1,6,UITSTRING.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_EITABEL_ID.Fieldname);
    AddCompoundDBEdit(1,1,TOELICHTING.Fieldname);
    AddCompoundDBEdit(1,2,RECORDTYPE_ID.Fieldname);
    AddCompoundDBEdit(1,3,LENGTE.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_EIVELD_ID.Fieldname);
    AddCompoundDBEdit(1,1,DDM_EITABEL_ID.Fieldname);
    AddCompoundDBEdit(1,2,RUBRIEKNR.Fieldname);
    AddCompoundDBEdit(1,3,GEGEVEN.Fieldname);
    AddCompoundDBEdit(1,4,LENGTE.Fieldname);
    AddCompoundDBEdit(1,5,EI_TYPE.Fieldname);
    AddCompoundDBEdit(1,6,DDM_TYPE.Fieldname);
    AddCompoundDBEdit(1,7,FORMAAT.Fieldname);
    AddCompoundDBEdit(1,8,CODETABEL.Fieldname);
    AddCompoundDBEdit(1,9,DEFAULTWAARDE.Fieldname);
    AddCompoundDBEdit(1,10,STARTPOS.Fieldname);
    AddCompoundDBEdit(1,11,STOPPOS.Fieldname);
    AddCompoundDBEdit(1,12,DDM_TABEL.Fieldname);
    AddCompoundDBEdit(1,13,DDM_VELD.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_FACTBASIS_ID.Fieldname);
    AddCompoundDBEdit(1,1,NUMMERFORMAAT_ID.Fieldname);
    AddCompoundDBEdit(1,2,NUMMERPLAN_ID.Fieldname);
    AddCompoundDBEdit(1,3,NUMMERRANGE_ID.Fieldname);
    AddCompoundDBEdit(1,4,VOLGENDENUMMER.Fieldname);
    AddCompoundDBEdit(1,5,FACTUURNUMMERSUFFIX.Fieldname);
    AddCompoundDBEdit(1,6,FACTUURNUMMERPREFIX.Fieldname);
    AddCompoundDBEdit(1,7,INCASSONAAM.Fieldname);
    AddCompoundDBEdit(1,8,INCASSOCONTACT.Fieldname);
    AddCompoundDBEdit(1,9,INCASSOEMAIL.Fieldname);
    AddCompoundDBEdit(1,10,INCASSOTELEFOON.Fieldname);
    AddCompoundDBEdit(1,11,INCASSOFAX.Fieldname);
    AddCompoundDBEdit(1,12,INCASSOADRES.Fieldname);
    AddCompoundDBEdit(1,13,INCASSOPOSTCODE.Fieldname);
    AddCompoundDBEdit(1,14,INCASSOPLAATS.Fieldname);
    AddCompoundDBEdit(1,15,FACTUURCONTACTPERSOON.Fieldname);
    AddCompoundDBEdit(1,16,FACTUURDIRECTTELEFOON.Fieldname);
    AddCompoundDBEdit(1,17,BETALINGSTERMIJN_ID.Fieldname);
    AddCompoundDBEdit(1,18,BETAALWIJZE_ID.Fieldname);
    AddCompoundDBEdit(1,19,VERZENDING_ID.Fieldname);
    AddCompoundDBEdit(1,20,CONDITIES.Fieldname);
    AddCompoundDBEdit(1,21,HERINNERING_NA_ID.Fieldname);
    AddCompoundDBEdit(1,22,LAATSTE_HER_NA_ID.Fieldname);
    AddCompoundDBEdit(1,23,AANMANING_NA_ID.Fieldname);
    AddCompoundDBEdit(1,24,INCASSO_NA_ID.Fieldname);
    AddCompoundDBEdit(1,25,ADMINISTRATIE_KOSTEN_ID.Fieldname);
    AddCompoundDBEdit(1,26,AANMANING_KOSTEN_ID.Fieldname);
    AddCompoundDBEdit(1,27,INCASSO_KOSTEN_ID.Fieldname);
    AddCompoundDBEdit(1,28,BOETERENTEPERCENTAGE.Fieldname);
    AddCompoundDBEdit(1,29,MINIMUMBOETERENTE.Fieldname);
    AddCompoundDBEdit(1,30,HERINNERING_TEKST.Fieldname);
    AddCompoundDBEdit(1,31,LAATSTE_HER_TEKST.Fieldname);
    AddCompoundDBEdit(1,32,AANMANING_TEKST.Fieldname);
    AddCompoundDBEdit(1,33,INCASSO_TEKST.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_FACTREG_ID.Fieldname);
    AddCompoundDBEdit(1,1,DDM_FACTUUR_ID.Fieldname);
    AddCompoundDBEdit(1,2,DDM_PORACT_ID.Fieldname);
    AddCompoundDBEdit(1,3,DATUM.Fieldname);
    AddCompoundDBEdit(1,4,DUUR.Fieldname);
    AddCompoundDBEdit(1,5,ACTSOORT_ID.Fieldname);
    AddCompoundDBEdit(1,6,PRESTATIE_ID.Fieldname);
    AddCompoundDBEdit(1,7,OMSCHRIJVING1.Fieldname);
    AddCompoundDBEdit(1,8,OMSCHRIJVING2.Fieldname);
    AddCompoundDBEdit(1,9,AANTAL.Fieldname);
    AddCompoundDBEdit(1,10,BEDRAG.Fieldname);
    AddCompoundDBEdit(1,11,BTWCODE.Fieldname);
    AddCompoundDBEdit(1,12,BTWBEDRAG.Fieldname);
    AddCompoundDBEdit(1,13,REGELEXCL.Fieldname);
    AddCompoundDBEdit(1,14,KORTINGPERC.Fieldname);
    AddCompoundDBEdit(1,15,KORTINGBEDRAG.Fieldname);
    AddCompoundDBEdit(1,16,REGELINCL.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_FACTUUR_ID.Fieldname);
    AddCompoundDBEdit(1,1,DDM_POR_ID.Fieldname);
    AddCompoundDBEdit(1,2,KLANT_ID.Fieldname);
    AddCompoundDBEdit(1,3,HISTORIE.Fieldname);
    AddCompoundDBEdit(1,4,TAV.Fieldname);
    AddCompoundDBEdit(1,5,AFDELING.Fieldname);
    AddCompoundDBEdit(1,6,ADRES.Fieldname);
    AddCompoundDBEdit(1,7,POSTCODE.Fieldname);
    AddCompoundDBEdit(1,8,PLAATS.Fieldname);
    AddCompoundDBEdit(1,9,LAND.Fieldname);
    AddCompoundDBEdit(1,10,UWREF.Fieldname);
    AddCompoundDBEdit(1,11,ONSREF.Fieldname);
    AddCompoundDBEdit(1,12,FACTUURDATUM.Fieldname);
    AddCompoundDBEdit(1,13,BETAALSTATUS.Fieldname);
    AddCompoundDBEdit(1,14,AFDRUKDATUM.Fieldname);
    AddCompoundDBEdit(1,15,FACTUURNUMMER.Fieldname);
    AddCompoundDBEdit(1,16,PERIODEVAN.Fieldname);
    AddCompoundDBEdit(1,17,PERIODETOT.Fieldname);
    AddCompoundDBEdit(1,18,BIJVRAGEN.Fieldname);
    AddCompoundDBEdit(1,19,BETAALWIJZE_ID.Fieldname);
    AddCompoundDBEdit(1,20,VERZENDING_ID.Fieldname);
    AddCompoundDBEdit(1,21,BETALINGSTERMIJN_ID.Fieldname);
    AddCompoundDBEdit(1,22,BIJZONDERHEDEN.Fieldname);
    AddCompoundDBEdit(1,23,CONDITIES.Fieldname);
    AddCompoundDBEdit(1,24,SUBTOTAALINCL.Fieldname);
    AddCompoundDBEdit(1,25,TOTAALINCL.Fieldname);
    AddCompoundDBEdit(1,26,SUBTOTAALEXCL.Fieldname);
    AddCompoundDBEdit(1,27,TOTAALEXCL.Fieldname);
    AddCompoundDBEdit(1,28,TOTAALBTW.Fieldname);
    AddCompoundDBEdit(1,29,KORTINGPERC.Fieldname);
    AddCompoundDBEdit(1,30,KORTINGBEDRAG.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_MUTGROEPEN_ID.Fieldname);
    AddCompoundDBEdit(1,1,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,2,INKOMSTEN.Fieldname);
    AddCompoundDBEdit(1,3,UITGAVEN.Fieldname);
    AddCompoundDBEdit(1,4,KRUISPOST.Fieldname);
    AddCompoundDBEdit(1,5,ZAKELIJK.Fieldname);
    AddCompoundDBEdit(1,6,PRIVE.Fieldname);
    AddCompoundDBEdit(1,7,BIJZONDERHEDEN.Fieldname);
    AddCompoundDBEdit(1,8,PERIODE_ID.Fieldname);
    AddCompoundDBEdit(1,9,PERIODETOTAALBIJ.Fieldname);
    AddCompoundDBEdit(1,10,PERIODETOTAALAF.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_PERIODEN_ID.Fieldname);
    AddCompoundDBEdit(1,1,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,2,VANDATUM.Fieldname);
    AddCompoundDBEdit(1,3,TOTDATUM.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_POR_ID.Fieldname);
    AddCompoundDBEdit(1,1,STARTDATUM.Fieldname);
    AddCompoundDBEdit(1,2,EINDDATUM.Fieldname);
    AddCompoundDBEdit(1,3,STATUS_ID.Fieldname);
    AddCompoundDBEdit(1,4,CLIENT_ID.Fieldname);
    AddCompoundDBEdit(1,5,HOOFDBEHANDELAAR_ID.Fieldname);
    AddCompoundDBEdit(1,6,HUISARTS_ID.Fieldname);
    AddCompoundDBEdit(1,7,VERWIJZER_ID.Fieldname);
    AddCompoundDBEdit(1,8,VERZEKERAAR_ID.Fieldname);
    AddCompoundDBEdit(1,9,TIJDBUDGET2.Fieldname);
    AddCompoundDBEdit(1,10,BESCHIKBAAR2.Fieldname);
    AddCompoundDBEdit(1,11,VOORGESCHIEDENIS.Fieldname);
    AddCompoundDBEdit(1,12,DIAGNOSE.Fieldname);
    AddCompoundDBEdit(1,13,BIJZONDERHEDEN.Fieldname);
    AddCompoundDBEdit(1,14,EETVERSLAG.Fieldname);
    AddCompoundDBEdit(1,15,ANAMNESE.Fieldname);
    AddCompoundDBEdit(1,16,EETADVIES.Fieldname);
    AddCompoundDBEdit(1,17,RAPPORTAGE.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_PORACT_ID.Fieldname);
    AddCompoundDBEdit(1,1,DDM_POR_ID.Fieldname);
    AddCompoundDBEdit(1,2,RELATIE_ID.Fieldname);
    AddCompoundDBEdit(1,3,DATUM.Fieldname);
    AddCompoundDBEdit(1,4,TIJDSTIP.Fieldname);
    AddCompoundDBEdit(1,5,ACTSOORT_ID.Fieldname);
    AddCompoundDBEdit(1,6,SOORT_ID.Fieldname);
    AddCompoundDBEdit(1,7,DUUR.Fieldname);
    AddCompoundDBEdit(1,8,PRESTATIE_ID.Fieldname);
    AddCompoundDBEdit(1,9,TOELICHTING.Fieldname);
    AddCompoundDBEdit(1,10,DECLARABEL.Fieldname);
    AddCompoundDBEdit(1,11,DECLARABEL_BIJ.Fieldname);
    AddCompoundDBEdit(1,12,BETALER_ID.Fieldname);
    AddCompoundDBEdit(1,13,BEHANDELAAR_ID.Fieldname);
    AddCompoundDBEdit(1,14,GEDECLAREERD.Fieldname);
    AddCompoundDBEdit(1,15,GEDECLAREERD_OP.Fieldname);
    AddCompoundDBEdit(1,16,DDM_FACTUUR_ID.Fieldname);
    AddCompoundDBEdit(1,17,ONTVANGEN.Fieldname);
    AddCompoundDBEdit(1,18,ONTVANGEN_OP.Fieldname);
    AddCompoundDBEdit(1,19,ONTVANGEN_VIA.Fieldname);
    AddCompoundDBEdit(1,20,BEDRAGEXCL.Fieldname);
    AddCompoundDBEdit(1,21,BEDRAG.Fieldname);
    AddCompoundDBEdit(1,22,AANTAL.Fieldname);
    AddCompoundDBEdit(1,23,SUBTOTAAL.Fieldname);
    AddCompoundDBEdit(1,24,KORTINGPERC.Fieldname);
    AddCompoundDBEdit(1,25,KORTINGBEDRAG.Fieldname);
    AddCompoundDBEdit(1,26,BEDRAGBTW.Fieldname);
    AddCompoundDBEdit(1,27,BTWCODE.Fieldname);
    AddCompoundDBEdit(1,28,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,29,KILOMETERS.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_PORACTSOORT_ID.Fieldname);
    AddCompoundDBEdit(1,1,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,2,DUUR.Fieldname);
    AddCompoundDBEdit(1,3,ACTCODE.Fieldname);
    AddCompoundDBEdit(1,4,PRESTATIE_ID.Fieldname);
    AddCompoundDBEdit(1,5,TARIEF.Fieldname);
    AddCompoundDBEdit(1,6,KILOMETERS.Fieldname);
    AddCompoundDBEdit(1,7,DECLARABEL.Fieldname);
    AddCompoundDBEdit(1,8,FACTUREREN.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DDM_RELATIE_ID.Fieldname);
    AddCompoundDBEdit(1,1,NAAM.Fieldname);
    AddCompoundDBEdit(1,2,CONTACT.Fieldname);
    AddCompoundDBEdit(1,3,SOORT_ID.Fieldname);
    AddCompoundDBEdit(1,4,ADRES.Fieldname);
    AddCompoundDBEdit(1,5,POSTCODE.Fieldname);
    AddCompoundDBEdit(1,6,TELEFOON.Fieldname);
    AddCompoundDBEdit(1,7,PLAATS.Fieldname);
    AddCompoundDBEdit(1,8,LAND.Fieldname);
    AddCompoundDBEdit(1,9,ZOEK.Fieldname);
    AddCompoundDBEdit(1,10,SEXE_ID.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,CODE.Fieldname);
    AddCompoundDBEdit(1,1,NEDERLANDS.Fieldname);
    AddCompoundDBEdit(1,2,ENGELS.Fieldname);
    AddCompoundDBEdit(1,3,DESIGN.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,TOEVOEGEN.Fieldname);
    AddCompoundDBEdit(1,1,VERWIJDEREN.Fieldname);
    AddCompoundDBEdit(1,2,GEBRUIKER_ID.Fieldname);
    AddCompoundDBEdit(1,3,WIJZIGEN.Fieldname);
    AddCompoundDBEdit(1,4,INLOGNAAM.Fieldname);
    AddCompoundDBEdit(1,5,UITVOEREN.Fieldname);
    AddCompoundDBEdit(1,6,NAAM.Fieldname);
    AddCompoundDBEdit(1,7,WACHTWOORD.Fieldname);
    AddCompoundDBEdit(1,8,INLOGDATUM.Fieldname);
    AddCompoundDBEdit(1,9,GELDIGTOT.Fieldname);
    AddCompoundDBEdit(1,10,GEBRUIKERSGROEP_ID.Fieldname);
    AddCompoundDBEdit(1,11,LEZEN.Fieldname);
    AddCompoundDBEdit(1,12,ACTIEF.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,DEF_WIJZIGEN.Fieldname);
    AddCompoundDBEdit(1,1,DEF_UITVOEREN.Fieldname);
    AddCompoundDBEdit(1,2,LEZEN.Fieldname);
    AddCompoundDBEdit(1,3,TOEVOEGEN.Fieldname);
    AddCompoundDBEdit(1,4,VERWIJDEREN.Fieldname);
    AddCompoundDBEdit(1,5,GEBRUIKERSGROEP_ID.Fieldname);
    AddCompoundDBEdit(1,6,WIJZIGEN.Fieldname);
    AddCompoundDBEdit(1,7,NAAM.Fieldname);
    AddCompoundDBEdit(1,8,UITVOEREN.Fieldname);
    AddCompoundDBEdit(1,9,DEF_LEZEN.Fieldname);
    AddCompoundDBEdit(1,10,DEF_TOEVOEGEN.Fieldname);
    AddCompoundDBEdit(1,11,DEF_VERWIJDEREN.Fieldname);
    AddCompoundDBEdit(1,12,DEELDATA.Fieldname);
    AddCompoundDBEdit(1,13,ACTIEF.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,HELP_ID.Fieldname);
    AddCompoundDBEdit(1,1,MODULE_ID.Fieldname);
    AddCompoundDBEdit(1,2,MODULE.Fieldname);
    AddCompoundDBEdit(1,3,FORM.Fieldname);
    AddCompoundDBEdit(1,4,COMPONENT.Fieldname);
    AddCompoundDBEdit(1,5,HOOFDSTUK_ID.Fieldname);
    AddCompoundDBEdit(1,6,PARAGRAAF_ID.Fieldname);
    AddCompoundDBEdit(1,7,SECTIE_ID.Fieldname);
    AddCompoundDBEdit(1,8,VOLGNUMMER.Fieldname);
    AddCompoundDBEdit(1,9,HOOFDSTUK.Fieldname);
    AddCompoundDBEdit(1,10,PARAGRAAF.Fieldname);
    AddCompoundDBEdit(1,11,SECTIE.Fieldname);
    AddCompoundDBEdit(1,12,HYPERLINKS.Fieldname);
    AddCompoundDBEdit(1,13,SHORTHINT.Fieldname);
    AddCompoundDBEdit(1,14,LONGHINT.Fieldname);
    AddCompoundDBEdit(1,15,NEEMAUTO.Fieldname);
    AddCompoundDBEdit(1,16,AUTOTEKST.Fieldname);
    AddCompoundDBEdit(1,17,TEKST.Fieldname);
    AddCompoundDBEdit(1,18,ALINEA2.Fieldname);
    AddCompoundDBEdit(1,19,TEKSTNA.Fieldname);
    AddCompoundDBEdit(1,20,TABEL.Fieldname);
    AddCompoundDBEdit(1,21,NIVEAU.Fieldname);
    AddCompoundDBEdit(1,22,VELDPROP.Fieldname);
    AddCompoundDBEdit(1,23,BLADZIJDE.Fieldname);
    AddCompoundDBEdit(1,24,REGEL.Fieldname);
    AddCompoundDBEdit(1,25,TITEL.Fieldname);
    AddCompoundDBEdit(1,26,IMAGEREF.Fieldname);
    AddCompoundDBEdit(1,27,IMAGEPROP.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,HISTORIE_ID.Fieldname);
    AddCompoundDBEdit(1,1,MODULE_ID.Fieldname);
    AddCompoundDBEdit(1,2,REFERENTIE.Fieldname);
    AddCompoundDBEdit(1,3,REF_ID.Fieldname);
    AddCompoundDBEdit(1,4,STRINGWAARDE.Fieldname);
    AddCompoundDBEdit(1,5,INTEGERWAARDE.Fieldname);
    AddCompoundDBEdit(1,6,FLOATWAARDE2.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,IMPORTBATCH_ID.Fieldname);
    AddCompoundDBEdit(1,1,AANMAAKDATUM.Fieldname);
    AddCompoundDBEdit(1,2,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,3,TOELICHTING.Fieldname);
    AddCompoundDBEdit(1,4,DATUM.Fieldname);
    AddCompoundDBEdit(1,5,TIJD.Fieldname);
    AddCompoundDBEdit(1,6,AUTOSTART.Fieldname);
    AddCompoundDBEdit(1,7,FREQUENTIE.Fieldname);
    AddCompoundDBEdit(1,8,GESTARTDOOR_ID.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,IMPORTSEQUENCE_ID.Fieldname);
    AddCompoundDBEdit(1,1,AANMAAKDATUM.Fieldname);
    AddCompoundDBEdit(1,2,IMPORTBATCH_ID.Fieldname);
    AddCompoundDBEdit(1,3,IMPORTTABEL_ID.Fieldname);
    AddCompoundDBEdit(1,4,VOLGORDE.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,IMPORTTABEL_ID.Fieldname);
    AddCompoundDBEdit(1,1,AANMAAKDATUM.Fieldname);
    AddCompoundDBEdit(1,2,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,3,TOELICHTING.Fieldname);
    AddCompoundDBEdit(1,4,BESTAND.Fieldname);
    AddCompoundDBEdit(1,5,PAD.Fieldname);
    AddCompoundDBEdit(1,6,DRIVE.Fieldname);
    AddCompoundDBEdit(1,7,EXTENSIE.Fieldname);
    AddCompoundDBEdit(1,8,INLEESDATUM.Fieldname);
    AddCompoundDBEdit(1,9,INGELEZENDOOR_ID.Fieldname);
    AddCompoundDBEdit(1,10,BESTANDSTYPE_ID.Fieldname);
    AddCompoundDBEdit(1,11,VELDSCHEIDING_ID.Fieldname);
    AddCompoundDBEdit(1,12,RECORDSCHEIDING_ID.Fieldname);
    AddCompoundDBEdit(1,13,AANTALVELDEN.Fieldname);
    AddCompoundDBEdit(1,14,AANTALREGELS.Fieldname);
    AddCompoundDBEdit(1,15,KOPPELTABEL.Fieldname);
    AddCompoundDBEdit(1,16,SKIPCHAR.Fieldname);
    AddCompoundDBEdit(1,17,SKIPLINES.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,IMPORTVELD_ID.Fieldname);
    AddCompoundDBEdit(1,1,AANMAAKDATUM.Fieldname);
    AddCompoundDBEdit(1,2,IMPORTTABEL_ID.Fieldname);
    AddCompoundDBEdit(1,3,HEADER.Fieldname);
    AddCompoundDBEdit(1,4,VELD.Fieldname);
    AddCompoundDBEdit(1,5,VELDLENGTE.Fieldname);
    AddCompoundDBEdit(1,6,VELDNUMMER.Fieldname);
    AddCompoundDBEdit(1,7,VELDTYPE_ID.Fieldname);
    AddCompoundDBEdit(1,8,VANPOS.Fieldname);
    AddCompoundDBEdit(1,9,TOTPOS.Fieldname);
    AddCompoundDBEdit(1,10,KOPPELVELD.Fieldname);
    AddCompoundDBEdit(1,11,KOPPELVELD_ID.Fieldname);
    AddCompoundDBEdit(1,12,LOOKUP_ID.Fieldname);
    AddCompoundDBEdit(1,13,LOOKUP_REF.Fieldname);
    AddCompoundDBEdit(1,14,MEENEMEN.Fieldname);
    AddCompoundDBEdit(1,15,DATA.Fieldname);
    AddCompoundDBEdit(1,16,TRIM.Fieldname);
    AddCompoundDBEdit(1,17,DSET.Fieldname);
    AddCompoundDBEdit(1,18,CONVERT.Fieldname);
    AddCompoundDBEdit(1,19,POSITIE.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,INBOEDEL_ID.Fieldname);
    AddCompoundDBEdit(1,1,AANKOOPDATUM.Fieldname);
    AddCompoundDBEdit(1,2,AANKOOPPRIJS.Fieldname);
    AddCompoundDBEdit(1,3,TOTAALAANKOOPPRIJS.Fieldname);
    AddCompoundDBEdit(1,4,MARKTPRIJS.Fieldname);
    AddCompoundDBEdit(1,5,LOCATIE.Fieldname);
    AddCompoundDBEdit(1,6,MERK.Fieldname);
    AddCompoundDBEdit(1,7,INBOEDEL_RUBRIEK_ID.Fieldname);
    AddCompoundDBEdit(1,8,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,9,FOTO.Fieldname);
    AddCompoundDBEdit(1,10,FOTOBESTAND.Fieldname);
    AddCompoundDBEdit(1,11,TYPENUMMER.Fieldname);
    AddCompoundDBEdit(1,12,SERIENUMMER.Fieldname);
    AddCompoundDBEdit(1,13,OUDERDOM.Fieldname);
    AddCompoundDBEdit(1,14,KWALITEIT_ID.Fieldname);
    AddCompoundDBEdit(1,15,STAAT_ID.Fieldname);
    AddCompoundDBEdit(1,16,GEKOCHTBIJ.Fieldname);
    AddCompoundDBEdit(1,17,BIJZONDERHEDEN.Fieldname);
    AddCompoundDBEdit(1,18,DAGWAARDE.Fieldname);
    AddCompoundDBEdit(1,19,VERVANGINGSWAARDE.Fieldname);
    AddCompoundDBEdit(1,20,VERZEKERINGSWAARDE.Fieldname);
    AddCompoundDBEdit(1,21,AANTAL.Fieldname);
    AddCompoundDBEdit(1,22,PRIJSINDEX_ID.Fieldname);
    AddCompoundDBEdit(1,23,RESTWAARDE_ID.Fieldname);
    AddCompoundDBEdit(1,24,AFSCHRIJVING_ID.Fieldname);
    AddCompoundDBEdit(1,25,ZOEK.Fieldname);
    AddCompoundDBEdit(1,26,EIGENAAR.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,INBOEDEL_OPTIONS_ID.Fieldname);
    AddCompoundDBEdit(1,1,DEF_PRIJSINDEX.Fieldname);
    AddCompoundDBEdit(1,2,DEF_AFSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,3,DEF_RESTWAARDE.Fieldname);
    AddCompoundDBEdit(1,4,DEF_EIGENAAR.Fieldname);
    AddCompoundDBEdit(1,5,DEF_STAAT.Fieldname);
    AddCompoundDBEdit(1,6,DEF_KWALITEIT.Fieldname);
    AddCompoundDBEdit(1,7,AUTOMERK.Fieldname);
    AddCompoundDBEdit(1,8,AUTORUBRIEK.Fieldname);
    AddCompoundDBEdit(1,9,AUTOINDEX.Fieldname);
    AddCompoundDBEdit(1,10,AUTOAFSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,11,AUTORESTWAARDE.Fieldname);
    AddCompoundDBEdit(1,12,ZOEKDEFINITIE.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,INBOEDEL_RUBRIEK_ID.Fieldname);
    AddCompoundDBEdit(1,1,RUBRIEKNAAM.Fieldname);
    AddCompoundDBEdit(1,2,PRIJSINDEX_ID.Fieldname);
    AddCompoundDBEdit(1,3,AFSCHRIJVING_ID.Fieldname);
    AddCompoundDBEdit(1,4,RESTWAARDE_ID.Fieldname);
    AddCompoundDBEdit(1,5,PICTOBESTAND.Fieldname);
    AddCompoundDBEdit(1,6,PICTOGRAM.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,KENMERKDEF_ID.Fieldname);
    AddCompoundDBEdit(1,1,AANMAAKDATUM.Fieldname);
    AddCompoundDBEdit(1,2,WIJZIGDATUM.Fieldname);
    AddCompoundDBEdit(1,3,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,4,GROEP_ID.Fieldname);
    AddCompoundDBEdit(1,5,MASTERTYPE_ID.Fieldname);
    AddCompoundDBEdit(1,6,KENMERKCODE.Fieldname);
    AddCompoundDBEdit(1,7,EENHEID_ID.Fieldname);
    AddCompoundDBEdit(1,8,FIELDFUNC_ID.Fieldname);
    AddCompoundDBEdit(1,9,PICKLIST_ID.Fieldname);
    AddCompoundDBEdit(1,10,REFERENTIE.Fieldname);
    AddCompoundDBEdit(1,11,VASTEKEUZES.Fieldname);
    AddCompoundDBEdit(1,12,PARAM1.Fieldname);
    AddCompoundDBEdit(1,13,DECIMALEN.Fieldname);
    AddCompoundDBEdit(1,14,DOMAINTYPE.Fieldname);
    AddCompoundDBEdit(1,15,BIJWERKEN.Fieldname);
    AddCompoundDBEdit(1,16,VEREIST.Fieldname);
    AddCompoundDBEdit(1,17,MULTIINSTANTIE.Fieldname);
    AddCompoundDBEdit(1,18,BEOORDELING_ID.Fieldname);
    AddCompoundDBEdit(1,19,GRENS1.Fieldname);
    AddCompoundDBEdit(1,20,GRENS2.Fieldname);
    AddCompoundDBEdit(1,21,GRENS3.Fieldname);
    AddCompoundDBEdit(1,22,GRENS4.Fieldname);
    AddCompoundDBEdit(1,23,GRENS5.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,KENMERKMATRIX_ID.Fieldname);
    AddCompoundDBEdit(1,1,AANMAAKDATUM.Fieldname);
    AddCompoundDBEdit(1,2,WIJZIGDATUM.Fieldname);
    AddCompoundDBEdit(1,3,TABEL_ID.Fieldname);
    AddCompoundDBEdit(1,4,MODULE_ID.Fieldname);
    AddCompoundDBEdit(1,5,TABELNAAM.Fieldname);
    AddCompoundDBEdit(1,6,SOORT_ID.Fieldname);
    AddCompoundDBEdit(1,7,DEFAULTWAARDE.Fieldname);
    AddCompoundDBEdit(1,8,KENMERKDEF_ID.Fieldname);
    AddCompoundDBEdit(1,9,VERPLICHT.Fieldname);
    AddCompoundDBEdit(1,10,ALLEENLEZEN.Fieldname);
    AddCompoundDBEdit(1,11,FREQUENT.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,KENMERKWAARDE_ID.Fieldname);
    AddCompoundDBEdit(1,1,AANMAAKDATUM.Fieldname);
    AddCompoundDBEdit(1,2,WIJZIGDATUM.Fieldname);
    AddCompoundDBEdit(1,3,MODULE_ID.Fieldname);
    AddCompoundDBEdit(1,4,TABEL_ID.Fieldname);
    AddCompoundDBEdit(1,5,TABELNAAM.Fieldname);
    AddCompoundDBEdit(1,6,RECORD_ID.Fieldname);
    AddCompoundDBEdit(1,7,KENMERKDEF_ID.Fieldname);
    AddCompoundDBEdit(1,8,DATUM.Fieldname);
    AddCompoundDBEdit(1,9,STRINGWAARDE.Fieldname);
    AddCompoundDBEdit(1,10,INTEGERWAARDE.Fieldname);
    AddCompoundDBEdit(1,11,LOCALEWAARDE.Fieldname);
    AddCompoundDBEdit(1,12,VOLGNUMMER.Fieldname);
    AddCompoundDBEdit(1,13,DATEWAARDE.Fieldname);
    AddCompoundDBEdit(1,14,FLOATWAARDE.Fieldname);
    AddCompoundDBEdit(1,15,DOUBLEWAARDE.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,MODULE_ID.Fieldname);
    AddCompoundDBEdit(1,1,AANMAAKDATUM.Fieldname);
    AddCompoundDBEdit(1,2,WIJZIGDATUM.Fieldname);
    AddCompoundDBEdit(1,3,INSTALLATIEDATUM.Fieldname);
    AddCompoundDBEdit(1,4,NAAM.Fieldname);
    AddCompoundDBEdit(1,5,TYPESTRING.Fieldname);
    AddCompoundDBEdit(1,6,INFO_MAJOR.Fieldname);
    AddCompoundDBEdit(1,7,INFO_MINOR.Fieldname);
    AddCompoundDBEdit(1,8,INFO_RELEASE.Fieldname);
    AddCompoundDBEdit(1,9,INFO_BUILD.Fieldname);
    AddCompoundDBEdit(1,10,VERSIESTRING.Fieldname);
    AddCompoundDBEdit(1,11,PACKAGEDESCRIPTION.Fieldname);
    AddCompoundDBEdit(1,12,OMSCHRIJVING.Fieldname);
    AddCompoundDBEdit(1,13,STATUS.Fieldname);
    AddCompoundDBEdit(1,14,AUTOSTART.Fieldname);
    AddCompoundDBEdit(1,15,PARAMETERS.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,PICKLIST_ID.Fieldname);
    AddCompoundDBEdit(1,1,MODULE_ID.Fieldname);
    AddCompoundDBEdit(1,2,REFERENTIE.Fieldname);
    AddCompoundDBEdit(1,3,REF_ID.Fieldname);
    AddCompoundDBEdit(1,4,STRINGWAARDE.Fieldname);
    AddCompoundDBEdit(1,5,INTEGERWAARDE.Fieldname);
    AddCompoundDBEdit(1,6,FLOATWAARDE2.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,PICKLISTKOP_ID.Fieldname);
    AddCompoundDBEdit(1,1,TITEL.Fieldname);
    AddCompoundDBEdit(1,2,REFERENTIE.Fieldname);
    AddCompoundDBEdit(1,3,SORTEREN_OP.Fieldname);
    AddCompoundDBEdit(1,4,MASTERTYPE.Fieldname);
    AddCompoundDBEdit(1,5,UITBREIDBAAR.Fieldname);
    AddCompoundDBEdit(1,6,MODULE_ID.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,SETTINGS_ID.Fieldname);
    AddCompoundDBEdit(1,1,UNIEKENAAM.Fieldname);
    AddCompoundDBEdit(1,2,PARENTKLASSE.Fieldname);
    AddCompoundDBEdit(1,3,CHILDKLASSE.Fieldname);
    AddCompoundDBEdit(1,4,SETTINGKLASSE.Fieldname);
    AddCompoundDBEdit(1,5,CHILDNAAM.Fieldname);
    AddCompoundDBEdit(1,6,DATA.Fieldname);
    AddCompoundDBEdit(1,7,DATATEXT.Fieldname);
  end;
  
with Card do
  begin
    AddCompoundDBEdit(1,0,NEDERLANDS.Fieldname);
    AddCompoundDBEdit(1,1,ENGELS.Fieldname);
    AddCompoundDBEdit(1,2,DESIGN.Fieldname);
  end;
  
