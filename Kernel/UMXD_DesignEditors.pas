unit UMXD_DesignEditors;

interface

uses
  DesignIntf, DesignEditors;

type
  TMatrixOptionsPropertyEditor = class(TPropertyEditor)
  private
  protected
  public
    procedure Edit; override;
  published
  end;

  // property editor voor (create on demand) cellstyle
  TCellStylePropertyEditor = class(TPropertyEditor)
  private
  protected
  public
    function GetAttributes: TPropertyAttributes; override;
  published
  end;

implementation

uses
  UMXD_FOptions;

{ TCellStylePropertyEditor }

function TCellStylePropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := inherited GetAttributes;
  Include(Result, paVolatileSubProperties);
end;

{ TMatrixOptionsPropertyEditor }

procedure TMatrixOptionsPropertyEditor.Edit;
var
  F: TFormEditMatrixOptions;
begin
  F := TFormEditMatrixOptions.Create(nil);
  try
    F.ShowModal;
  finally
    F.Free;
  end;
end;

end.

