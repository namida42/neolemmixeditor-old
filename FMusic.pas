unit FMusic;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFMusicForm = class(TForm)
    Label1: TLabel;
    ListBox1: TListBox;
    Button1: TButton;
    Button2: TButton;
    cbSMS: TCheckBox;
    procedure cbSMSClick(Sender: TObject);
  private
    function GetMusicNumber: Integer;
  public
    constructor Create(aOwner: TComponent; SelValue: Integer = 0);
    property MusicNumber: Integer read GetMusicNumber;
  end;

var
  FMusicForm: TFMusicForm;

implementation

{$R *.dfm}

constructor TFMusicForm.Create(aOwner: TComponent; SelValue: Integer = 0);
begin
  inherited Create(aOwner);
  {if SelValue <= 23 then
    ListBox1.ItemIndex := SelValue
  else
    case SelValue of
      254: ListBox1.ItemIndex := 24;
      255: ListBox1.ItemIndex := 25;
      else ListBox1.ItemIndex := 0; 
    end;}
end;

function TFMusicForm.GetMusicNumber: Integer;
begin
  //if ListBox1.ItemIndex <= 23 then
    Result := ListBox1.ItemIndex
  {else
    case ListBox1.ItemIndex of
      24: Result := 253;
      25: Result := 254;
      26: Result := 255;
    end;}
end;

procedure TFMusicForm.cbSMSClick(Sender: TObject);
begin
  if cbSMS.Checked then
  begin
    ListBox1.Items[13] := 'SMS-Exclusive Original';
    ListBox1.Items[14] := 'Miniature Overture';
    ListBox1.Items[15] := 'Scotland The Brave';
    ListBox1.Items[16] := 'Don''t Dilly-Dally On The Way';
  end else begin
    ListBox1.Items[13] := 'Tim 1 (DOS 7)';
    ListBox1.Items[14] := 'Forest Green';
    ListBox1.Items[15] := 'Tim 4 (DOS 10)';
    ListBox1.Items[16] := 'Ten Green Bottles';
  end;
end;

end.
