{$include lem_directives.inc}

unit FAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UMasterControls, StdCtrls,
  UMisc, LemStrings, Buttons;

type
  TAboutForm = class(TForm)
    Label1: TLabel;
    BitBtn1: TBitBtn;
    procedure Form_Create(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure ShowAboutForm;

implementation

{$R *.dfm}

procedure ShowAboutForm;
var
  F: TAboutForm;
begin
  MessageDlg(SAboutStr + CrLf + CrLf + SThanksStr, mtCustom, [mbOK], 0);
  (*ShowMessage()
  F := TAboutForm.Create(nil);
  try
    F.ShowModal;
  finally
    F.Free;
  end; *)
end;

{ TAboutForm }

procedure TAboutForm.Form_Create(Sender: TObject);
begin
  Label1.Caption := SAboutStr + CrLf + CrLf + SThanksStr;
(*
  with TicketBar1 do
  begin
    with BarText.Add do
    begin
      Text := '   Thanks to....   ';
      Color := clRed;
    end;
    with BarText.Add do
    begin
      Text := '   DMA for creating the original lemmings game   ';
      Color := clLime;
    end;
    with BarText.Add do
    begin
      Text := '   Alex A. Denisov and contributors for the Graphics32 units   ';
      Color := clYellow;
    end;
    with BarText.Add do
    begin
      Text := '   Mike Lischke for the GraphicEx unit   ';
      Color := clLime;
    end;
    with BarText.Add do
    begin
      Text := '   Erik Turner for the ZLibEx unit   ';
      Color := clYellow;
    end;
    with BarText.Add do
    begin
      Text := '   Peter Morris for the FastStrings unit   ';
      Color := clLime;
    end;
    with BarText.Add do
    begin
      Text := '   Volker Oth, Mindless and Ccexplore for technical information about lemmings   ';
      Color := clYellow;
    end;
  end;
  *)

  (*
Thanks to...
DMA for creating the original lemmings game.
Alex A. Denisov and contributors for Graphics32 units.
Mike Lischke for the GraphicEx unit.
Erik Turner for the ZLibEx unit.
Peter Morris for the FastStrings unit.
Volker Oth, ccexplore and MindLess for technical information about lemmings.
*)


end;

end.

