object LevelListEditorForm: TLevelListEditorForm
  Left = 237
  Top = 227
  BorderStyle = bsToolWindow
  Caption = 'Level List'
  ClientHeight = 225
  ClientWidth = 321
  Color = clBtnFace
  UseDockManager = True
  DragKind = dkDock
  DragMode = dmAutomatic
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lvLevelItems: TListView
    Left = 0
    Top = 0
    Width = 321
    Height = 225
    Columns = <
      item
        Caption = 'Piece'
        MaxWidth = 75
        MinWidth = 75
        Width = 75
      end
      item
        Caption = 'X'
        MaxWidth = 40
        MinWidth = 40
        Width = 40
      end
      item
        Caption = 'Y'
        MaxWidth = 40
        MinWidth = 40
        Width = 40
      end
      item
        Caption = 'Info'
        MaxWidth = 162
        MinWidth = 162
        Width = 162
      end>
    GridLines = True
    HideSelection = False
    MultiSelect = True
    RowSelect = True
    TabOrder = 0
    ViewStyle = vsReport
    OnSelectItem = lvLevelItemsSelectItem
  end
end
