{$include lem_directives.inc}

unit LemFiles;

interface

uses
  Windows, Classes, SysUtils, Graphics, GR32,
  UMisc;

type
  TColorArray = array of DWORD;

const
  LVL_MAXOBJECTCOUNT  = 32;
  LVL_MAXTERRAINCOUNT = 400;
  LVL_MAXSTEELCOUNT   = 32;

  GRAPHICSET_DIRT      = 0;
  GRAPHICSET_FIRE      = 1;
  GRAPHICSET_MARBLE    = 2;
  GRAPHICSET_PILLAR    = 3;
  GRAPHICSET_CRYSTAL  = 4;
  GRAPHICSET_BRICK     = 5;
  GRAPHICSET_ROCK      = 6;
  GRAPHICSET_SNOW      = 7;
  GRAPHICSET_BUBBLE    = 8;
  GRAPHICSET_XMAS      = 9;
  GRAPHICSET_TREE      = 10;
  GRAPHICSET_PURPLE    = 11;
  GRAPHICSET_PSYCH     = 12;
  GRAPHICSET_METAL     = 13;
  GRAPHICSET_DESERT    = 14;
  GRAPHICSET_SKY       = 15;
  GRAPHICSET_CIRCUIT   = 16;
  GRAPHICSET_MARTIAN   = 17;
  GRAPHICSET_LAB       = 18;
  GRAPHICSET_SEGA      = 19;

  EXTGRAPHICSET_01     = 1;
  EXTGRAPHICSET_02     = 2;
  EXTGRAPHICSET_03     = 3;
  EXTGRAPHICSET_04     = 4;

//  OBJECT_EXIT

  // game object terrain modifier consts
  OBJECTMODIFIER_NOOVERWRITE         = $80; // do not overwrite existing terrain
  OBJECTMODIFIER_NEEDSTERRAIN        = $40; // must have terrain underneath to be visible
  OBJECTMODIFIER_FACELEFT            = $20;
  OBJECTMODIFIER_FAKE                = $10;
  OBJECTMODIFIER_ALWAYSDRAW          = $00; // always draw full graphic

  OBJECTDISPLAY_NORMAL         = $0F;
  OBJECTDISPLAY_UPSIDEDOWN     = $8F;

  //graphics set 0
  OBJECT_G00_EXIT                          = 0;
  OBJECT_G00_START           	             = 1;
  OBJECT_G00_WAVING_GREEN_FLAG	           = 2;
  OBJECT_G00_ONE_WAY_BLOCK_LEFT            = 3;
  OBJECT_G00_ONE_WAY_BLOCK_RIGHT	         = 4;
  OBJECT_G00_WATER               	         = 5;
  OBJECT_G00_BEAR_TRAP          	         = 6;
  OBJECT_G00_EXIT_DECORATION_FLAMES        = 7;
  OBJECT_G00_ROCK_SQUISHING_TRAP           = 8;
  OBJECT_G00_WAVING_BLUE_FLAG           	 = 9;
  OBJECT_G00_10_TON_SQUISHING_TRAP         = 10;

  //graphics set 1
  OBJECT_G01_EXIT                          = 0;
  OBJECT_G01_START                         = 1;
  OBJECT_G01_WAVING_GREEN_FLAG             = 2;
  OBJECT_G01_ONE_WAY_BLOCK_LEFT            = 3;
  OBJECT_G01_ONE_WAY_BLOCK_RIGHT           = 4;
  OBJECT_G01_RED_LAVA                      = 5;
  OBJECT_G01_EXIT_FLAMES                   = 6; //
  OBJECT_G01_FIRE_PIT_TRAP                 = 7;
  OBJECT_G01_FIRE_SHOOTER_TRAP_FROM_LEFT   = 8;
  OBJECT_G01_WAVING_BLUE_FLAG              = 9;
  OBJECT_G01_FIRE_SHOOTER_TRAP_FROM_RIGHT  = 10;

  //graphics set 2
	OBJECT_G02_EXIT                          = 0;
	OBJECT_G02_START                         = 1;
	OBJECT_G02_WAVING_GREEN_FLAG             = 2;
	OBJECT_G02_ONE_WAY_BLOCK_LEFT            = 3;
	OBJECT_G02_ONE_WAY_BLOCK_RIGHT           = 4;
	OBJECT_G02_GREEN_LIQUID                  = 5;
	OBJECT_G02_EXIT_FLAMES                   = 6; //
	OBJECT_G02_WAVING_BLUE_FLAG              = 7;
	OBJECT_G02_PILLAR_SQUISHING_TRAP         = 8;
	OBJECT_G02_SPINNING_DEATH_TRAP           = 9;

  OBJECT_G03_EXIT                         = 0;
  OBJECT_G03_START                        = 1;
  OBJECT_G03_WAVING_GREENFLAG             = 2;
  OBJECT_G03_ONEWAY_BLOCK_LEFT            = 3;
  OBJECT_G03_ONEWAY_BLOCK_RIGHT           = 4;
  OBJECT_G03_WATER                        = 5;
  OBJECT_G03_EXIT_FLAMES                  = 6; //
  OBJECT_G03_WAVING_BLUE_FLAG             = 7;
  OBJECT_G03_SPINNY_ROPE_TRAP             = 8;
  OBJECT_G03_SPIKES_FROM_LEFT_TRAP        = 9;
  OBJECT_G03_SPIKES_FROM_RIGHT_TRAP       = 10;


type
  TGraphicSetRange = GRAPHICSET_DIRT..GRAPHICSET_SEGA;
//  TExtendedGraphicSetRange
const
  TheGraphicSets = [GRAPHICSET_DIRT..GRAPHICSET_SEGA];

  dtSecMarker = $FF;

  dtEof = $00;      //not strictly required
  dtComment = $01;
  dtHeader = $02;
  dtObject = $03;
  dtTerrain = $04;
  dtSound = $05;

type

  NeoLemmixColorEntry = packed record
    case Byte of
      0: (A, R, G, B: Byte); // don't think this structure is ever needed, but nice to have just in case
      1: (ARGB: TColor32);
  end;

  NeoLemmixHeader = packed record
    VersionNumber: Byte;
    Resolution: Byte;
    IsUpdated: Byte;
    Reserved: Array[0..12] of Byte;
    KeyColors: Array[0..7] of NeoLemmixColorEntry;
  end;

  NeoLemmixObjectData = packed record
    ObjectFlags: Word;
    FrameCount: Word;
    PreviewFrame: Word;
    KeyFrame: Word;
    BaseLoc: LongWord;
    TriggerEff: Byte;
    TriggerSound: Byte;
    PTriggerX: SmallInt;
    PTriggerY: SmallInt;
    PTriggerW: SmallInt;
    PTriggerH: SmallInt;
    Reserved: Array[0..1] of Byte;
    STriggerX: SmallInt;
    STriggerY: SmallInt;
    STriggerW: SmallInt;
    STriggerH: SmallInt;
    Reserved2: Array[0..7] of Byte;
  end;

  NeoLemmixTerrainData = packed record
    TerrainFlags: Word;
    BaseLoc: LongWord;
    Reserved: Array[0..9] of Byte;
  end;


  // Seperate defs for the new format, due to slight differences in the Object and Terrain datas
  TNewNeoLVLObject = packed record
          XPos              : LongInt;
          YPos              : LongInt;
          ObjectID          : Word;
          SValue            : Byte;
          LValue            : Byte;
          ObjectFlags       : Word;
          Graph             : Byte;
          Reserved          : Array[0..3] of Byte;
          Locked            : Byte;
  end;

  TNewNeoLVLTerrain = packed record
          XPos              : LongInt;
          YPos              : LongInt;
          TerrainID         : Word;
          TerrainFlags      : Word;
          Graph             : Byte;
          Reserved          : Array[0..1] of Byte;
          Locked            : Byte;
  end;

  TNewNeoLVLSteel = packed record
         XPos              : LongInt; // 9 bits
         YPos              : LongInt; // 7 bits bits 1..7
         SteelWidth        : LongWord;
         SteelHeight       : LongWord;
         SteelFlags        : Byte;
         Reserved          : Array[0..1] of Byte;
         Locked            : Byte;
  end;



  TNeoLVLObject = packed record
  case Byte of
    0: ( AsInt64: Int64 );
    1: ( D0, D1: DWord);
    2: ( W0, W1, W2, W3: Word);
    3: ( B0, B1, B2, B3, B4, B5, B6, B7: Byte);
    4: (
          XPos              : SmallInt;
          YPos              : SmallInt;
          ObjectID          : Byte;
          SValue            : Byte;
          LValue            : Byte;
          ObjectFlags       : Byte;
        );

  end;

  TNeoLVLTerrain = packed record
  case Byte of
    0: ( AsInt64: Int64);
    1: ( D0, D1: DWord );
    2: ( W0, W1, W2, W3: Word );
    3: ( B0, B1, B2, B3, B4, B5, B6, B7: Byte );
    4: (
          XPos              : SmallInt;
          YPos              : SmallInt;
          TerrainID         : Byte;
          TerrainFlags      : Byte;
          TerReserved       : Word;
       );
  end;

  TNeoLVLSteel = packed record
  case Byte of
    0: ( AsInt64: Int64);
    1: ( D0, D1: DWord );
    2: ( W0, W1, W2, W3: Word );
    3: ( B0, B1, B2, B3, B4, B5, B6, B7: Byte );
    4: (
         XPos              : SmallInt; // 9 bits
         YPos              : SmallInt; // 7 bits bits 1..7
         SteelWidth        : Byte;
         SteelHeight       : Byte;
         SteelFlags        : Byte;
         SteReserved       : Byte;
       );
  end;

  TNeoLVLObjects = array[0..127] of TNeoLVLObject;
  TNeoLVLTerrains = array[0..999] of TNeoLVLTerrain;
  TNeoLVLSteels = array[0..127] of TNeoLVLSteel;

  TNeoLVLHeader = packed record
    {0000}  FormatTag                  : Byte;
            MusicNumber                : Byte;
            LemmingsCount              : Word;
            RescueCount                : Word;
            TimeLimit                  : Word;
    {0008}  ReleaseRate                : Byte;
            LevelOptions               : Byte;
            Resolution                 : Byte;
            BgIndex                    : Byte;
            ScreenPosition             : Word;
            ScreenYPosition            : Word;
    {0010}  WalkerCount                : Byte;
            ClimberCount               : Byte;
            SwimmerCount               : Byte;
            FloaterCount               : Byte;
            GliderCount                : Byte;
            MechanicCount              : Byte;
            BomberCount                : Byte;
            StonerCount                : Byte;
    {0018}  BlockerCount               : Byte;
            PlatformerCount            : Byte;
            BuilderCount               : Byte;
            StackerCount               : Byte;
            BasherCount                : Byte;
            MinerCount                 : Byte;
            DiggerCount                : Byte;
            ClonerCount                : Byte;
    {0020}  Gimmick                    : LongWord;
            Skillset                   : Word;
            RefSection                 : Byte;
            RefLevel                   : Byte;
    {0028}  Width                      : LongWord;
            Height                     : LongWord;
            VgaspecX                   : LongInt;
            VgaspecY                   : LongInt;
            LevelID                    : LongWord;
            LevelOptions2              : Byte;
            FencerCount                : Byte;
            Reserved2                  : array[0..1] of Byte;
    {0040}  LevelAuthor                : array[0..15] of Char;
    {0050}  LevelName                  : array[0..31] of Char;
    {0070}  StyleName                  : array[0..15] of Char;
    {0080}  VgaspecName                : array[0..15] of Char;
    {0090}  FreeBytes                  : array[0..31] of Char;
  end;

  TNeoLVLSubHeader = packed record
    ScreenStartX: LongInt;
    ScreenStartY: LongInt;
    Gimmicks2: LongWord;
    Gimmicks3: LongWord;
    MusicName: Array[0..15] of Char;
    PostSecRank: Byte;
    PostSecLevel: Byte;
    BnsRank: Byte;
    BnsLevel: Byte;
    ClockStart: Word;
    ClockEnd: Word;
    ClockCount: Word;
  end;

  TNeoLVLRec = packed record
    {0000}  FormatTag                  : Byte;
            MusicNumber                : Byte;
            LemmingsCount              : Word;
            RescueCount                : Word;
            TimeLimit                  : Word;
    {0008}  ReleaseRate                : Byte;
            LevelOptions               : Byte;
            GraphicSet                 : Byte;
            GraphicSetEx               : Byte;
            ScreenPosition             : Word;
            ScreenYPosition            : Word;
    {0010}  WalkerCount                : Byte;
            ClimberCount               : Byte;
            SwimmerCount               : Byte;
            FloaterCount               : Byte;
            GliderCount                : Byte;
            MechanicCount              : Byte;
            BomberCount                : Byte;
            StonerCount                : Byte;
    {0018}  BlockerCount               : Byte;
            PlatformerCount            : Byte;
            BuilderCount               : Byte;
            StackerCount               : Byte;
            BasherCount                : Byte;
            MinerCount                 : Byte;
            DiggerCount                : Byte;
            ClonerCount                : Byte;
    {0020}  Gimmick                    : LongWord;
            Skillset                   : Word;
            RefSection                 : Byte;
            RefLevel                   : Byte;
    {0028}  WidthAdjust                : SmallInt;
            HeightAdjust               : SmallInt;
            VgaspecX                   : SmallInt;
            VgaspecY                   : SmallInt;
    {0030}  LevelAuthor                : array[0..15] of Char;
    {0040}  LevelName                  : array[0..31] of Char;
    {0060}  StyleName                  : array[0..15] of Char;
    {0070}  VgaspecName                : array[0..15] of Char;
    {0080}  WindowOrder                : array[0..31] of Byte;
    {00A0}  FreeBytes                  : array[0..31] of Char;

    {0060}  Objects                    : TNeoLVLObjects;
    {????}  Terrain                    : TNeoLVLTerrains;
    {????}  Steel                      : TNeoLVLSteels;

  end;

const
  NEO_LVL_SIZE = SizeOf(TNeoLVLRec);

type
  TLVLFile = class;

  TGenericFileFormat = class(TPersistent)
  private
  protected
  public
    procedure LoadFromFile(const aFileName: string); virtual;
    procedure LoadFromStream(S: TStream); virtual;
  published
  end;

  

  {-------------------------------------------------------------------------------
    .LVL files (WinLemmings)
  -------------------------------------------------------------------------------}

  // sometimes swapping needed of Lo Hi!!!!!!!!!!!!!!

  // sub record interactive objects
  TLVLObject = packed record
    case Byte of
      0: (
            XPos              : Word; // swap lo and hi
            YPos              : Word; // swap lo and hi
            ObjectID          : Word;
            Modifier          : Byte;  // objectmodifier_xxx
            DisplayMode       : Byte;
//            DisplayMode       : Byte;  // objectdisplay_xxx
  //          _NotUsed          : array[0..2] of Byte
          );
      1: (
           AsInt64: Int64;
         );
      2: (
            B0, B1, B2, B3, B4, B5, B6, B7: Byte;
         );
  end;

  // sub record terrain
  {
    bits 0..3    = modifier
    bits 4..7    shl 8 for xpos
    bits 8..15   add to xpos
    bits 16..24  9 bits number YPos


  }
  TLVLTerrain = packed record
    case Byte of
      0: (
            XPos              : Word;
            YPos              : Byte; // 9 bits
            TerrainID         : Byte;
         );
      1: (
            AsDWORD: DWORD;
         );
      2: (
            W0: Word;//Lo: Word;
            W1: Word;//Hi: Word;
         );
      3: (
            B0, B1, B2, B3: Byte;
         );
  end;


  TLVLSteel = packed record
    case Byte of
      0: (
           XPos              : Byte; // 9 bits
           YPos              : Byte; // 7 bits bits 1..7
           Area              : Byte; // bits 0..3 is height in 4 pixel units (then add 4)
                                     // bit 4..7 is width in 4 pixel units (then add 4)
           b                 : Byte; // always zero
         );
      1: (
            AsDWORD: DWORD;
         );
      2: (
            W0: Word;
            W1: Word;
         );
      3: (
            B0, B1, B2, B3: Byte;
         );
  end;

  // the main record 2048 bytes
  PLVLFileRec = ^TLVLFileRec;
  TLVLFileRec = packed record
    {0000}  _ReleaseRateNotUsed        : Byte; // little endian's
    {    }  ReleaseRate                : Byte;
    {    }  _LemmingsCountNotUsed      : Byte;
    {    }  LemmingsCount              : Byte;
    {    }  _RescueCountNotUsed        : Byte;
    {    }  RescueCount                : Byte;
    {    }  _TimeLimitNotUsed          : Byte;
    {    }  TimeLimit                  : Byte;
    {0008}  _ClimberNotUsed            : Byte;
    {    }  ClimberCount               : Byte;
    {    }  _FloaterNotUsed            : Byte;
    {    }  FloaterCount               : Byte;
    {    }  _BomberNotUsed             : Byte;
    {    }  BomberCount                : Byte;
    {    }  _BlockerNotUsed            : Byte;
    {    }  BlockerCount               : Byte;
    {    }  _BuilderNotUsed            : Byte;
    {    }  BuilderCount               : Byte;
    {    }  _BasherNotUsed             : Byte;
    {    }  BasherCount                : Byte;
    {    }  _MinerNotUsed              : Byte;
    {    }  MinerCount                 : Byte;
    {    }  _DiggerNotUsed             : Byte;
    {    }  DiggerCount                : Byte;
    {0018}  ScreenPosition             : Word;
    {001A}  _GraphicSetNoUsed          : Byte;
    {    }  GraphicSet                 : Byte; // graphicset_xxx
    {    }  _GraphicSetExNotUsed       : Byte;
    {    }  GraphicSetEx               : Byte;
    {001E}  Reserved1                  : Byte; // I think it is a filler until the 32 bytes
    {    }  Reserved2                  : Byte; // I think it is a filler until the 32 bytes
                                               // And it was, but for the superlemminglevel
                                               // in "ohno more lemmings" both bytes are $FF
    {0020}  Objects                    : array[0..LVL_MAXOBJECTCOUNT - 1] of TLVLObject;
    {0120}  Terrain                    : array[0..LVL_MAXTERRAINCOUNT - 1] of TLVLTerrain;
    {0760}  Steel                      : array[0..LVL_MAXSTEELCOUNT - 1] of TLVLSteel;
    {07E0}  LevelName                  : array[0..31] of Char;
  end;

  TLVLFile = class(TGenericFileFormat)
  private
    function GetLevelName: string;
  protected
  public
    fRec: TLVLFileRec;
    procedure LoadFromStream(S: TStream); override;
    //function
  published
    property ReleaseRate: Byte read fRec.ReleaseRate;
    property LemmingsCount: Byte read fRec.LemmingsCount;
    property RescueCount: Byte read fRec.RescueCount;
    property TimeLimit: Byte read fRec.TimeLimit;
    property TimeLimitSecs: Byte read fRec._TimeLimitNotUsed;
    property ClimberCount: Byte read fRec.ClimberCount;
    property FloaterCount: Byte read fRec.FloaterCount;
    property BomberCount: Byte read fRec.BomberCount;
    property BlockerCount: Byte read fRec.BlockerCount;
    property BuilderCount: Byte read fRec.BuilderCount;
    property BasherCount: Byte read fRec.BasherCount;
    property MinerCount: Byte read fRec.MinerCount;
    property DiggerCount: Byte read fRec.DiggerCount;
    property GraphicSet: Byte read fRec.GraphicSet;
    property LevelName: string read GetLevelName;
  end;


implementation

uses
  LemMisc;

{ TGenericFileFormat }

procedure TGenericFileFormat.LoadFromFile(const aFileName: string);
var
  F: TFileStream;
begin
  F := TFileStream.Create(aFileName, fmOpenRead);
  try
    LoadFromStream(F);
  finally
    F.Free;
  end;
end;

procedure TGenericFileFormat.LoadFromStream(S: TStream);
begin
  //raise Exception.Create('TGenericFileFormatL');
end;


{ TLVLFile }

function TLVLFile.GetLevelName: string;
begin
  Result := Trim(fRec.LevelName);
end;

procedure TLVLFile.LoadFromStream(S: TStream);
var
  R: Integer;
  Buf: TLVLFileRec;
begin
  R := S.Read(Buf, 2048);
  if R <> 2048 then
    raise Exception.Create('LVL read error');
  fRec := Buf;
end;



end.
