
{$include lem_directives.inc}

unit FValidationReport;

interface

uses
  LemEdit, LemCore, LemLemminiStyles, LemDosStyles,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Contnrs;

type

  TError = class
    private
      fErrorID: Integer;
      fReference: Integer;
      function GetErrorText: String;
    public
      constructor Create(aErrorID, aReference: Integer); overload;
      constructor Create(aErrorID: Integer); overload;
      property ErrorID: Integer read fErrorID write fErrorID;
      property Reference: Integer read fReference write fReference;
      property Text: String read GetErrorText;
  end;

  TErrorList = class(TObjectList)
  private
    function GetItem(Index: Integer): TError;
  protected
  public
    function Add(aErrorID, aReference: Integer): Integer; overload;
    function Add(aErrorID: Integer): Integer; overload;
    function Add(Item: TError): Integer; overload;
    procedure Insert(Index: Integer; Item: TError);
    property Items[Index: Integer]: TError read GetItem; default;
    property List;
  published
  end;

  TFormValidationReport = class(TForm)
    BtnOK: TButton;
    lbLevelErrors: TListBox;
    Label1: TLabel;
    btnFix: TButton;
    Button1: TButton;
    procedure btnFixClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    //procedure BtnOKClick(Sender: TObject);
  private
    fEditor: TLevelEditor;
    fErrorList: TErrorList;
    procedure FixIssue(iNum: Integer; Silent: Boolean = false);
  public
    constructor Create(aOwner: TComponent; aEditor: TLevelEditor);
    destructor Destroy;
    procedure DoDetectErrors;
    procedure DisplayList;
  end;


procedure ExecuteFormValidationReport(aEditor: TLevelEditor; AutoFix: Boolean = false);

const
  // The numbers of these values may safely be modified as long as no two are the same

  // Stats
  errLemmingCount = 1;
  errSaveCount = 2;
  errNoTime = 3;
  errScreenStartBounds = 4;
  errSkillsetCount = 6;
    gscHardworkLazy = 1; // 7, 9
    gscSolidWrap = 2; // 15, 23
    gscInvincible = 3; // 12
    gscClassicZombie = 4; // not26, 27
    gscUnused = 5; // 29

  // Objects
  errObjectQuantity = 20;
  errObjectInvalid = 21;
  errObjectX8 = 22;
  errObjectY4 = 23;
  errObjectBounds = 24;
  errObjectProperty = 25;
  errObjectOneWay = 26;
  errObjectTriggerInvert = 27;
  errObjectBackground = 28;

  // Terrains
  errTerrainQuantity = 40;
  errTerrainInvalid = 41;
  errTerrainBounds = 42;
  errTerrainProperty = 43;

  // Steels
  errSteelQuantity = 60;
  errSteelX4 = 61;
  errSteelY4 = 62;
  errSteelW4 = 63;
  errSteelH4 = 64;
  errSteelWMax = 65;
  errSteelHMax = 66;
  errSteelBounds = 67;
  errSteelProperty = 68;

implementation

{$R *.dfm}

constructor TFormValidationReport.Create(aOwner: TComponent; aEditor: TLevelEditor);
begin
  inherited Create(aOwner);
  fEditor := aEditor;
  fErrorList := TErrorList.Create;
end;

destructor TFormValidationReport.Destroy;
begin
  fErrorList.Free;
  inherited;
end;

procedure TFormValidationReport.DoDetectErrors;
var
  i: Integer;
  fLemmini, fNeoLemmix, fSuperLemmini: Boolean;
  Obj: TInteractiveObject;
  MO: TMetaObject;
  Ter: TTerrain;
  MT: TMetaTerrain;
  Stl: TSteel;
  GS: TBaseGraphicSet;
  LW, LH: Integer;

  TempBool: Boolean;
  TempInt: Integer;

  function CheckOutsideLowerBound(aObj: TInteractiveObject): Boolean;
  begin
    Result := (Obj.YPosition >= LH);
  end;

begin

  fErrorList.Clear;

  //Some preparation
  GS := fEditor.Graph;
  fLemmini := (fEditor.Style is TLemminiStyle);
  if fLemmini then
  begin
    fNeoLemmix := false;
    fSuperLemmini := TLemminiStyle(fEditor.Style).SuperLemmini;
  end else begin
    fSuperLemmini := false;
    fNeoLemmix := TCustLemmStyle(fEditor.Style).NeoLemmix;
  end;
  if (fNeoLemmix) or (fSuperLemmini) then
  begin
    LW := fEditor.Statics.Width;
    LH := fEditor.Statics.Height;
  end else if fLemmini then
  begin
    LW := 3200;
    LH := 320
  end else begin
    LW := 1584;
    LH := 160;
  end;


  //Level properties
  with fEditor, Statics do
  begin
    if LemmingsCount >= 500 then fErrorList.Add(errLemmingCount, LemmingsCount);
    TempInt := LemmingsCount;
    if (fNeoLemmix) and ((SkillTypes and $8000) <> 0) then TempInt := TempInt + ClonerCount;
    if RescueCount > TempInt then fErrorList.Add(errSaveCount, RescueCount - TempInt);
    if HasTimeLimit and (TimeLimit = 0) then fErrorList.Add(errNoTime);

    if fLemmini then
      TempInt := 2
    else
      TempInt := 1;
    if (ScreenPosition > LW - 1)
    or (ScreenYPosition > LH - 1)
    or (ScreenPosition < 0)
    or (ScreenYPosition < 0)
    then fErrorList.Add(errScreenStartBounds);

    if fNeoLemmix then
    begin
      TempInt := 0;
      for i := 0 to 15 do
        if (SkillTypes and (1 shl i)) <> 0 then TempInt := TempInt + 1;
      if TempInt > 8 then fErrorList.Add(errSkillsetCount);
    end;
  end;

  //Piece quantities
  if (not fLemmini) and
  (((not fNeoLemmix) and (fEditor.InteractiveObjectCollection.Count > 32))
  {or (fNeoLemmix and (fEditor.InteractiveObjectCollection.Count > 128))})
  then fErrorList.Add(errObjectQuantity);

  if (not fLemmini) and
  (((not fNeoLemmix) and (fEditor.TerrainCollection.Count > 400))
  {or (fNeoLemmix and (fEditor.TerrainCollection.Count > 1000))})
  then fErrorList.Add(errTerrainQuantity);

  if (not fLemmini) and
  (((not fNeolemmix) and (fEditor.SteelCollection.Count > 32))
  {or (fNeoLemmix and (fEditor.SteelCollection.Count > 128))})
  then fErrorList.Add(errSteelQuantity);

  // Objects
  for i := 0 to fEditor.InteractiveObjectCollection.Count-1 do
  begin
    Obj := fEditor.InteractiveObjectCollection[i];

    if not Obj.ItemActive then Continue;

    GS := Obj.Graph;

    if (Obj.ObjectID > Obj.Graph.MetaObjectCollection.Count-1) or (Obj.Graph.GraphicSetInternalName = 'placeholder') then
    begin
      fErrorList.Add(errObjectInvalid, i);
      Continue;
    end else
      MO := Obj.Graph.MetaObjectCollection[Obj.ObjectID];

    if MO.TriggerEffect = 32 then
    begin
      fErrorList.Add(errObjectBackground, i);
      Continue;
    end;

    if not (fLemmini or fNeoLemmix) then
    begin
      if Obj.XPosition mod 8 <> 0 then
        fErrorList.Add(errObjectX8, i);
      if Obj.YPosition mod 4 <> 0 then
        fErrorList.Add(errObjectY4, i);
    end;

    if (Obj.XPosition <= 0 - MO.Width)
    or (Obj.XPosition >= LW)
    or (Obj.YPosition <= 0 - MO.Height)
    or CheckOutsideLowerBound(Obj) then
        fErrorList.Add(errObjectBounds, i);

    if (not (fNeoLemmix or (odfOnlyShowOnTerrain in Obj.ObjectDrawingFlags)))
    and (MO.TriggerEffect in [7, 8, 19]) then
      fErrorList.Add(errObjectOneWay, i);
  end;


  //Terrains
  for i := 0 to fEditor.TerrainCollection.Count-1 do
  begin
    Ter := fEditor.TerrainCollection[i];
    if not Ter.ItemActive then Continue;

    GS := Ter.Graph;

    if (Ter.TerrainID > GS.MetaTerrainCollection.Count-1) or (Ter.Graph.GraphicSetInternalName = 'placeholder') then
    begin
      fErrorList.Add(errTerrainInvalid, i);
      Continue;
    end else
      MT := GS.MetaTerrainCollection[Ter.TerrainID];

    if (Ter.XPosition <= 0 - MT.Width)
    or (Ter.XPosition >= LW)
    or (Ter.YPosition <= 0 - MT.Height)
    or (Ter.YPosition >= LH) then
      fErrorList.Add(errTerrainBounds, i);

    //tdfErase
    //tdfInvert
    //tdfNoOverwrite
    //tdfInvisible
    //tdfFake
    //tdfFlip
    //tdfNoOneWay

    TempBool := false;
    if not (fNeoLemmix or fSuperLemmini) then
    begin
      if tdfFlip in Ter.TerrainDrawingFlags then TempBool := true;
      if tdfNoOneWay in Ter.TerrainDrawingFlags then TempBool := true;
    end;
    if not fSuperLemmini then
    begin
      if tdfInvisible in Ter.TerrainDrawingFlags then TempBool := true;
      if tdfFake in Ter.TerrainDrawingFlags then TempBool := true;
    end;
    if TempBool then fErrorList.Add(errTerrainProperty, i);

  end;


  //Steels
  for i := 0 to fEditor.SteelCollection.Count-1 do
  begin
    Stl := fEditor.SteelCollection[i];
    if not Stl.ItemActive then Continue;

    if not (fLemmini or fNeoLemmix) then
    begin
      if Stl.XPosition mod 4 <> 0 then fErrorList.Add(errSteelX4, i);
      if Stl.YPosition mod 4 <> 0 then fErrorList.Add(errSteelY4, i);
      if Stl.SteelWidth mod 4 <> 0 then fErrorList.Add(errSteelW4, i);
      if Stl.SteelHeight mod 4 <> 0 then fErrorList.Add(errSteelH4, i);
    end;

    if (Stl.XPosition <= 0 - Stl.SteelWidth)
    or (Stl.XPosition >= LW)
    or (Stl.YPosition <= 0 - Stl.SteelHeight)
    or (Stl.YPosition >= LH) then
      fErrorList.Add(errSteelBounds, i);

    if not fLemmini then
    begin
      if fNeoLemmix then
        TempInt := 256
      else
        Tempint := 64;
      if Stl.SteelWidth > TempInt then fErrorList.Add(errSteelWMax);
      if Stl.SteelHeight > TempInt then fErrorList.Add(errSteelHMax);
    end;

    TempBool := false;
    if not ((fNeoLemmix or fSuperLemmini) or (Stl.SteelType = 0)) then TempBool := True;
    if not (fNeoLemmix or (Stl.SteelType in [0, 1])) then TempBool := True;
    if TempBool then fErrorList.Add(errSteelProperty, i);
  end;


  //Display the list
  DisplayList;
end;

procedure TFormValidationReport.DisplayList;
var
  i: Integer;
begin
  lbLevelErrors.Clear;
  for i := 0 to fErrorList.Count-1 do
    lbLevelErrors.Items.Add(fErrorList[i].GetErrorText);
  if lbLevelErrors.Items.Count = 0 then
    lbLevelErrors.Items.Add('No errors found!');
end;

procedure ExecuteFormValidationReport(aEditor: TLevelEditor; AutoFix: Boolean = false);
var
  F: TFormValidationReport;
  i: Integer;
begin
  F := TFormValidationReport.Create(nil, aEditor);
  try
    //F.Memo.Lines := Report;

    F.DoDetectErrors;

    if AutoFix then
      F.Button1Click(nil)
    else
      F.ShowModal;
  finally
    F.Free;
  end;
end;

{procedure TFormValidationReport.BtnOKClick(Sender: TObject);
begin
  Close;
end;}

function TErrorList.Add(aErrorID, aReference: Integer): Integer;
var
  TempErr: TError;
begin
  TempErr := TError.Create(aErrorID, aReference);
  Add(TempErr);
end;

function TErrorList.Add(aErrorID: Integer): Integer;
var
  TempErr: TError;
begin
  TempErr := TError.Create(aErrorID);
  Add(TempErr);
end;

function TErrorList.Add(Item: TError): Integer;
begin
  Result := inherited Add(Item);
end;

function TErrorList.GetItem(Index: Integer): TError;
begin
  Result := inherited Get(Index);
end;

procedure TErrorList.Insert(Index: Integer; Item: TError);
begin
  inherited Insert(Index, Item);
end;

constructor TError.Create(aErrorID, aReference: Integer);
begin
  fErrorID := aErrorID;
  fReference := aReference;
end;

constructor TError.Create(aErrorID: Integer);
begin
  fErrorID := aErrorID;
  fReference := 0;
end;

function TError.GetErrorText: String;
  function EID: String; // to allow writing the output strings more lazily
  begin
    Result := IntToStr(fErrorID);
  end;
  function REF: String;
  begin
    Result := IntToStr(fReference)
  end;
begin
  case fErrorID of
    errLemmingCount: Result := 'The level has ' + REF + ' lemmings; this is a very high number and may result in laggy gameplay';
    errSaveCount: Result := 'The level requires saving ' + REF + ' more lemming(s) than exist in the level';
    errNoTime: Result := 'The level has a time limit of zero';
    errScreenStartBounds: Result := 'The screen start position is not entirely within the play area';
    errSkillsetCount: Result := 'The level has more than eight types of skills in the skillset; extra ones will be ignored';

    errObjectQuantity: Result := 'The number of objects is more than allowed in the current format; excess will be cut when saving';
    errObjectInvalid: Result := 'Object ' + REF + ' refers to an object not found in the graphic set';
    errObjectX8: Result := 'Object ' + REF + ' X position not divisible by 8; will be rounded down to nearest multiple of 8';
    errObjectY4: Result := 'Object ' + REF + ' Y not divisible by 4; trigger area may be higher than expected';
    errObjectBounds: Result := 'Object ' + REF + ' is positioned completely outside the play area';
    errObjectProperty: Result := 'Object ' + REF + ' has a property not supported by the current format; it will be lost when saving';
    errObjectOneWay: Result := 'Object ' + REF + ' is a one-way wall and does not have "Only on terrain" set; may have graphical oddities';
    errObjectTriggerInvert: Result := 'Object ' + REF + ' is inverted, but its trigger area is not';
    errObjectBackground: Result := 'Object ' + REF + ' is a background image; these should not be placed as objects.';

    errTerrainQuantity: Result := 'The number of terrain pieces is more than allowed in the current format; excess will be cut when saving';
    errTerrainInvalid: Result := 'Terrain ' + REF + ' refers to a terrain piece not found in the current graphic set';
    errTerrainBounds: Result := 'Terrain ' + REF + ' is positioned completely outside the play area';
    errTerrainProperty: Result := 'Terrain ' + REF + ' has a property not supported by the current format; it will be lost when saving';

    errSteelQuantity: Result := 'The number of steel areas is more than allowed in the current format; excess will be cut when saving';
    errSteelX4: Result := 'Steel ' + REF + ' X position not divisible by 4; will be rounded down to nearest multiple of 4';
    errSteelY4: Result := 'Steel ' + REF + ' Y position not divisible by 4; will be rounded down to nearest multiple of 4';
    errSteelW4: Result := 'Steel ' + REF + ' width not divisible by 4; will be rounded down to nearest multiple of 4';
    errSteelH4: Result := 'Steel ' + REF + ' height not divisible by 4; will be rounded down to nearest multiple of 4';
    errSteelWMax: Result := 'Steel ' + REF + ' exceeds the maximum width';
    errSteelHMax: Result := 'Steel ' + REF + ' exceeds the maximum height';
    errSteelBounds: Result := 'Steel ' + REF + ' is positioned completely outside the play area';
    errSteelProperty: Result := ' Steel ' + REF + ' has a property not supported by the current format; it will be lost when saving';

    else Result := 'Unknown error (' + EID + ':' + REF + ')'; //catch-all if I forgot to write a message for something
  end;
end;

procedure TFormValidationReport.FixIssue(iNum: Integer; Silent: Boolean = false);
var
  Err: TError;
  fLemmini, fNeoLemmix, fSuperLemmini: Boolean;
  i: Integer;
  TempInt, TempInt2: Integer;
begin
  if (iNum < 0) or (iNum > fErrorList.Count-1) then Exit;

  if not Silent then
    fEditor.BeginUpdate;

  try
    fEditor.SelectorCollection.Clear;

    fLemmini := (fEditor.Style is TLemminiStyle);
    if fLemmini then
    begin
      fNeoLemmix := false;
      fSuperLemmini := TLemminiStyle(fEditor.Style).SuperLemmini;
    end else begin
      fNeoLemmix := TCustLemmStyle(fEditor.Style).NeoLemmix;
      fSuperLemmini := false;
    end;

    Err := fErrorList[iNum];
    case Err.ErrorID of
      errSaveCount: fEditor.Statics.RescueCount := fEditor.Statics.RescueCount - Err.Reference;
      errNoTime: if (fNeoLemmix or fSuperLemmini) then
                   fEditor.Statics.HasTimeLimit := false
                 else
                   fEditor.Statics.TimeLimit := 9;
      errScreenStartBounds: begin
                              fEditor.Statics.ScreenPosition := 0;
                              fEditor.Statics.ScreenYPosition := 0;
                            end;
      errSkillsetCount: begin
                          TempInt := 0;
                          TempInt2 := 0;
                          for i := 15 downto 0 do
                          begin
                            if fEditor.Statics.SkillTypes and (1 shl i) <> 0 then
                            begin
                              TempInt := TempInt + 1;
                              TempInt2 := TempInt2 + (1 shl i);
                            end;
                            if TempInt = 8 then Exit;
                          end;
                          fEditor.Statics.SkillTypes := TempInt2;
                        end;

      errObjectInvalid: if fLemmini or fNeoLemmix then
                          fEditor.InteractiveObjectCollection.Delete(Err.Reference)
                        else
                          fEditor.InteractiveObjectCollection[Err.Reference].ItemActive := false;
      errObjectX8: fEditor.InteractiveObjectCollection[Err.Reference].XPosition := fEditor.InteractiveObjectCollection[Err.Reference].XPosition and not 7;
      errObjectY4: fEditor.InteractiveObjectCollection[Err.Reference].YPosition := fEditor.InteractiveObjectCollection[Err.Reference].YPosition and not 3;
      errObjectBounds: if fLemmini or fNeoLemmix then
                          fEditor.InteractiveObjectCollection.Delete(Err.Reference)
                        else
                          fEditor.InteractiveObjectCollection[Err.Reference].ItemActive := false;
      errObjectOneWay: fEditor.InteractiveObjectCollection[Err.Reference].ObjectDrawingFlags := fEditor.InteractiveObjectCollection[Err.Reference].ObjectDrawingFlags + [odfOnlyShowOnTerrain];
      errObjectBackground: fEditor.InteractiveObjectCollection.Delete(Err.Reference);
      errTerrainInvalid: if fLemmini or fNeoLemmix then
                          fEditor.TerrainCollection.Delete(Err.Reference)
                        else
                          fEditor.TerrainCollection[Err.Reference].ItemActive := false;
      errTerrainBounds: if fLemmini or fNeoLemmix then
                          fEditor.TerrainCollection.Delete(Err.Reference)
                        else
                          fEditor.TerrainCollection[Err.Reference].ItemActive := false;
      errTerrainProperty: begin
                            fEditor.TerrainCollection[Err.Reference].TerrainDrawingFlags := fEditor.TerrainCollection[Err.Reference].TerrainDrawingFlags - [tdfInvisible];
                            fEditor.TerrainCollection[Err.Reference].TerrainDrawingFlags := fEditor.TerrainCollection[Err.Reference].TerrainDrawingFlags - [tdfFake];
                            if not fNeoLemmix then
                            begin
                              fEditor.TerrainCollection[Err.Reference].TerrainDrawingFlags := fEditor.TerrainCollection[Err.Reference].TerrainDrawingFlags - [tdfFlip];
                              fEditor.TerrainCollection[Err.Reference].TerrainDrawingFlags := fEditor.TerrainCollection[Err.Reference].TerrainDrawingFlags - [tdfNoOneWay];
                            end;
                          end;

      errSteelX4: fEditor.SteelCollection[Err.Reference].XPosition := fEditor.SteelCollection[Err.Reference].XPosition and not 3;
      errSteelY4: fEditor.SteelCollection[Err.Reference].YPosition := fEditor.SteelCollection[Err.Reference].YPosition and not 3;
      errSteelW4: fEditor.SteelCollection[Err.Reference].SteelWidth := fEditor.SteelCollection[Err.Reference].SteelWidth and not 3;
      errSteelH4: fEditor.SteelCollection[Err.Reference].SteelHeight := fEditor.SteelCollection[Err.Reference].SteelHeight and not 3;
      errSteelWMax: if fNeoLemmix then
                      fEditor.SteelCollection[Err.Reference].SteelWidth := 256
                    else
                      fEditor.SteelCollection[Err.Reference].SteelWidth := 64;
      errSteelHMax: if fNeoLemmix then
                      fEditor.SteelCollection[Err.Reference].SteelHeight := 256
                    else
                      fEditor.SteelCollection[Err.Reference].SteelHeight := 64;
      errSteelBounds: if fLemmini or fNeoLemmix then
                        fEditor.SteelCollection.Delete(Err.Reference)
                      else
                        fEditor.SteelCollection[Err.Reference].ItemActive := false;
      errSteelProperty: if fLemmini or fNeoLemmix then
                          fEditor.SteelCollection.Delete(Err.Reference)
                        else
                          fEditor.SteelCollection[Err.Reference].ItemActive := false;

      else if not Silent then ShowMessage('This error will need to be fixed manually.');
    end;
    if not Silent then
      DoDetectErrors;
  finally
    if not Silent then
      fEditor.EndUpdate;
  end;
end;

procedure TFormValidationReport.btnFixClick(Sender: TObject);
begin
  if (lbLevelErrors.ItemIndex = -1) or (fErrorList.Count = 0) then Exit;
  fEditor.BeginUpdate;
  try
    FixIssue(lbLevelErrors.ItemIndex);
    DoDetectErrors;
  finally
    fEditor.EndUpdate;
  end;
end;

procedure TFormValidationReport.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  if (fErrorList.Count = 0) then Exit;
  fEditor.BeginUpdate;
  try
    for i := fErrorList.Count-1 downto 0 do
      FixIssue(i, true);
    DoDetectErrors;
    if fErrorList.Count <> 0 then ShowMessage('Some errors could not be auto-fixed.');
  finally
    fEditor.EndUpdate;
  end;
end;

end.

