{$include lem_directives.inc}

{-------------------------------------------------------------------------------
  unit with develop testroutines
-------------------------------------------------------------------------------}


unit LemDevelop;

interface

{$ifdef develop}
uses
  Classes, SysUtils,
  UMisc, UFiles, UFastStrings,
  LemCore, LemStyles, LemTypes, LemDosCmp, LemDosArc;

procedure LevelPackVerifyBatchTest(const aDirMask: string);
procedure SearchLevelPackMachine(const aDirMask, aLevelTitle: string);
{$endif}

implementation


{$ifdef develop}
procedure LevelPackVerifyBatchTest(const aDirMask: string);
{-------------------------------------------------------------------------------
  Verifies if all levelpacks in dir can be decompressed.
-------------------------------------------------------------------------------}
var
  L: TStringList;
  i, j: Integer;
  Arc: TDosDatArchive;
  Mem: TMemoryStream;
begin
  L := TStringList.Create;
  Arc := TDosDatArchive.Create;
  Mem := TMemoryStream.Create;
  try
    CreateFileList(L, aDirMask, faAllFiles and not faDirectory);
    for i := 0 to L.Count - 1 do
    begin
      deb(['checking', extractfilename(l[i])]);
      Arc.LoadFromFile(L[i]);
      try
        for j := 0 to Arc.SectionList.Count - 1 do
        begin
          Mem.Clear;
          try
            Arc.ExtractSection(j, Mem);
            deb(['  section ok', j]);
          except
            on E: EDecompressError do
              deb(['ERROR: ', extractfilename(l[i]), 'section', j]);
          end;
        end;
      finally
        Arc.CloseFile;
      end;
    end;
  finally
    L.Free;
    Arc.Free;
    Mem.Free;
  end;
end;

procedure SearchLevelPackMachine(const aDirMask, aLevelTitle: string);
var
  L: TStringList;
  i, j: Integer;
  Arc: TDosDatArchiveEx;
  Level: TLevel;
begin
  L := TStringList.Create;
  Arc := TDosDatArchiveEx.Create;
  Level := TLevel.Create(nil);
  try
    CreateFileList(L, aDirMask, faAllFiles and not faDirectory);
    for i := 0 to L.Count - 1 do
    begin
      try
        deb(['checking', extractfilename(l[i])]);
        try
          Arc.LoadFromFile(L[i]);
          for j := 0 to Arc.SectionList.Count - 1 do
          begin
            Level.LoadFromStream(Arc.SectionList[j].DecompressedData,
              CustLemmStyle, lffLVL);
            if FastPosNoCase(level.levelname, aleveltitle,
              length(level.LevelName), length(aleveltitle), 1) > 0 then
            deb(['  found at', j, level.levelname])
          end;
        finally
          Arc.Close;
        end;
      except
        //on exception do
          //deb(['invalid', extractfilename(l[i])])
      end;
    end;
  finally
    L.Free;
    Arc.Free;
    Level.Free;
  end;
end;

{$endif}


end.

