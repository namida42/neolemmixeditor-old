unit UBasePlayingForm;

{-------------------------------------------------------------------------------
  This unit contains a base ancestor for a fake fullscreen form.
-------------------------------------------------------------------------------}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  umisc, ulog,  Dialogs;

type
  TBasePlayingForm = class(TForm)
  private
    fMainRect: TRect;
    fDisplayScale: Integer;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure InitializeScaleAndRect(NeededWidth, NeededHeight: Integer);
    property MainRect: TRect read fMainRect;
    property DisplayScale: Integer read fDisplayScale;
  end;

implementation

procedure TBasePlayingForm.InitializeScaleAndRect(NeededWidth, NeededHeight: Integer);
var
  //i,
  W, H, MaxW, MaxH: Integer;
begin
  W := NeededWidth;
  H := NeededHeight;
  MaxW := Screen.Width;
  MaxH := Screen.Height;

  // auto calculate
  fDisplayScale := 1;
  repeat
    if (W * (fDisplayScale + 1) > MaxW) or (H * (fDisplayScale + 1) > MaxH) then
      Break;
    Inc(fDisplayScale);
  until False;

  fMainRect.Left := (MaxW - W * DisplayScale) div 2;
  fMainRect.Top := (MaxH - H * DisplayScale) div 2;
  fMainRect.Right := fMainRect.Left + W * DisplayScale - 1;
  fMainRect.Bottom := fMainRect.Top + H * DisplayScale - 1;

end;

constructor TBasePlayingForm.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  BorderStyle := bsNone;
  BorderIcons := [];
  WindowState := wsMaximized;
  Color := $00595959;
end;

destructor TBasePlayingForm.Destroy;
begin
  inherited Destroy;
end;

end.

