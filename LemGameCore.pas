unit LemGameCore;

interface

uses
  UMisc;

  {

      -load level
      -load objects
      -load terrains
      -load animations
      -render terrain
      -free terrain bitmaps
      -play game
  }


type
  TLevelStaticsRec = record
    rReleaseRate                : Integer;
    rLemmingsCount              : Integer;
    rRescueCount                : Integer;
    rTimeLimit                  : Integer;
    rClimberCount               : Integer;
    rFloaterCount               : Integer;
    rBomberCount                : Integer;
    rBlockerCount               : Integer;
    rBuilderCount               : Integer;
    rBasherCount                : Integer;
    rMinerCount                 : Integer;
    rDiggerCount                : Integer;
    rGraphicSet                 : Integer;
    rGraphicSetEx               : Integer;
    rScreenPosition             : Integer;
    rTitle                      : String32;
  end;




implementation

end.

