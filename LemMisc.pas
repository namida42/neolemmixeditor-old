{$include lem_directives.inc}

unit LemMisc;

interface

uses
  Classes, Controls, Forms, Graphics, ExtCtrls, GR32, GR32_IMAGE;

procedure ShowImageForm(Bitmap: TPersistent; ascale: integer = 1);

procedure ReplaceColor(B: TBitmap32; FromColor, ToColor: TColor32);

function ConvertBits(var Data; NumBytes, StartBit, Numbits: Byte): Integer;

function CalcFrameRect(Bmp: TBitmap32; FrameCount, FrameIndex: Integer): TRect;

type
  TDebugMode = (
    dmWinSprLoading, // debugging spr-files of winlemmings
    dmWinLvlBuilding // debugging level building from lvl file
  );

type
  TLogger = class
  private
  protected
  public
//    procedure RegisterLogFile(ID: O: TClass; const aFileName: string);
  published
  end;

procedure Debug(aDebugMode: TDebugMode; const Ar: array of const;
  Indent: Integer = 0);
function AppPath: string;
function PtInRectIncl(const Rect: TRect; const P: TPoint): Boolean;

implementation

uses
  SysUtils, UMisc, UFastStrings,
  LemFiles, LemTypes,
  LemStrings;

var
  _DebugWinSprFile: TextFile;
  _DebuggedWinSprLines: Integer;

  _DebugWinLvlBuildFile: TextFile;
  _DebuggedWinLvlBuildLines: Integer;


function PtInRectIncl(const Rect: TRect; const P: TPoint): Boolean;
begin
  Result := (P.X >= Rect.Left) and (P.X <= Rect.Right) and (P.Y >= Rect.Top)
    and (P.Y <= Rect.Bottom);
end;

procedure ShowImageForm(Bitmap: TPersistent; ascale: integer = 1);
var
  f:tform;
  img: TImgView32;
  //bmp: tbitmap32;
begin
//  bmp := tbitmap32.create;
  //bmp.assign(bitmap);

  f:=tform.create(nil);
  begin
    img:=timgview32.create(f);

//    img.width := 200;
//    img.height := 200;
    img.bitmap.Assign(bitmap);
    img.parent:=f;
    img.scale := ascale;
//    img.autosize:=true;
    img.ScrollBars.visibility:=svHidden;
//    img.ScaleMode:=smStretch;
    f.autosize:=true;
    f.autosize:=false;
//    img.autosize:=false;
    img.Align:=alclient;
    f.showmodal;
    f.free;
  end;

//  bmp.free;
end;

procedure ReplaceColor(B: TBitmap32; FromColor, ToColor: TColor32);
var
  P: PColor32;
  i, x, y: Integer;
begin
  P := B.PixelPtr[0, 0];
  for i := 0 to B.Height * B.Width - 1 do
//  for y := 0 to B.Height  - 1 do
  //  for x := 0 to B.Width - 1 do
    begin
      if P^ = FromColor then
        P^ := ToColor;
      Inc(P);
    end;
end;

function ConvertBits(var Data; NumBytes, StartBit, Numbits: Byte): Integer;
var
  i: Integer;
  D: Cardinal absolute Data;
  Ar: TBytes absolute Data;
  StartByte, CurrByte, CurrBit, ByteMask, IntMask: Byte;
  {
    76543210 76543210 76543210 76543210
    Byte 0   Byte 1   Byte 2   Byte 3
    Bit 0........................Bit 31
  }
begin
  Result := 0;
  StartByte := StartBit div 8;
  for i := Startbit to StartBit + NumBits do
  begin
    CurrBit := 7 - (i mod 8);
    ByteMask := 1 shl CurrBit;
    IntMask := Integer(ByteMask) shl (i - StartBit);
    Result := Result or IntMask;
  end;
end;

function CalcFrameRect(Bmp: TBitmap32; FrameCount, FrameIndex: Integer): TRect;
var
  Y, H, W: Integer;
begin
  W := Bmp.Width;
  H := Bmp.Height div FrameCount;
  Y := H * FrameIndex;

//  Assert(Bmp.Height mod FrameCount = 0)
  Result.Left := 0;
  Result.Top := Y;
  Result.Right := W;
  Result.Bottom := Y + H;

end;

function AppPath: string;
begin
  Result := ExtractFilePath(Application.ExeName);
end;

procedure Debug(aDebugMode: TDebugMode; const Ar: array of const;
  Indent: Integer = 0);

    function SetStr: string;
    var
      i: integer;
    begin
      Result := '';
      for i := Low (Ar) to High (Ar) do
      begin
        Result := Result + '[' + TransForm(Ar[i]) + ']';
      end;
      Result := FastReplace(Result, '#0', ' ');
    end;

var
  S: string;

begin
  S := SetStr;
   case aDebugMode of
     dmWinSprLoading:
       begin
         {$ifdef debug_winspr_loading}
         if _DebuggedWinSprLines = 0 then
           Rewrite(_DebugWinSprFile);
         Inc(_DebuggedWinSprLines);
         if _DebuggedWinSprLines > 100000 then
           Exit;
         if Indent > 0 then
           S := StringOfChar(' ', Indent * 2) + S;
         WriteLn(_DebugWinSprFile, {LeadZeroStr(_DebuggedWinSprLines, 8) + ':' + }
           S);
         Flush(_DebugWinSprFile);
        {$endif}
       end;
     dmWinLvlBuilding:
       begin
         {$ifdef debug_winlvl_building}
         if _DebuggedWinLvlBuildLines = 0 then
           Rewrite(_DebugWinLvlBuildFile);
         Inc(_DebuggedWinLvlBuildLines);
         if _DebuggedWinLvlBuildLines > 100000 then
           Exit;
         if Indent > 0 then
           S := StringOfChar(' ', Indent * 2) + S;
         WriteLn(_DebugWinLvlBuildFile, S);
         Flush(_DebugWinLvlBuildFile);
        {$endif}
       end;
   end;
end;

initialization
  {$ifdef debug_winspr_loading}
  AssignFile(_DebugWinSprFile, AppPath + '_debug_winspr_loading.pas');
  {$endif}

  {$ifdef debug_winlvl_building}
  AssignFile(_DebugWinLvlBuildFile, AppPath + '_debug_winlvlbuilding.pas');
  {$endif}


finalization
  {$ifdef debug_winspr_loading}
  if _DebuggedWinSprLines > 0 then CloseFile(_DebugWinSprFile);
  {$endif}

  {$ifdef debug_winlvl_building}
  if _DebuggedWinLvlBuildLines > 0 then CloseFile(_DebugWinLvlBuildFile);
  {$endif}
end.


//nction GraphicSetToDescription(aGraphicSet: Integer): string;
    // returns description of graphic set
//function GraphicObjectToDescription(aGraphicSet, aGraphicObject: Integer): string;
    // returns interactive object description depending of the graphic set
    (*
    function GraphicSetToDescription(aGraphicSet: Integer): string;
begin
  (*
  Result := 'Unknown';
  case aGraphicSet of
    GRAPHICSET_DIRT      : Result := SGraphicSetDirt;
    GRAPHICSET_FIRE      : Result := SGraphicSetFire;
    GRAPHICSET_MARBLE    : Result := SGraphicSetMarble;
    GRAPHICSET_PILLAR    : Result := SGraphicSetPillar;
    GRAPHICSET_CRYSTAL  : Result := SGraphicSetCrystal;
    GRAPHICSET_BRICK     : Result := SGraphicSetBrick;
    GRAPHICSET_ROCK      : Result := SGraphicSetRock;
    GRAPHICSET_SNOW      : Result := SGraphicSetSnow ;
    GRAPHICSET_BUBBLE    : Result := SGraphicSetBubble;

  end;
    *)
//  if Result = '' then
  //  raise Exception.Create('GraphicSetToDescription error');
end;

function GraphicObjectToDescription(aGraphicSet, aGraphicObject: Integer): string;
begin
  (*
  Result := '';

  case aGraphicSet of
    GRAPHICSET_DIRT:
      case aGraphicObject of
        OBJECT_G00_EXIT                              : Result := SObject_G00_Exit;
        OBJECT_G00_START                             : Result := SObject_G00_Start;
        OBJECT_G00_WAVING_GREEN_FLAG                 : Result := SObject_G00_WavingGreenFlag;
        OBJECT_G00_ONE_WAY_BLOCK_LEFT                : Result := SObject_G00_OneWayBlockLeft;
        OBJECT_G00_ONE_WAY_BLOCK_RIGHT               : Result := SObject_G00_OneWayBlockRight;
        OBJECT_G00_WATER                             : Result := SObject_G00_Water;
        OBJECT_G00_BEAR_TRAP                         : Result := SObject_G00_BearTrap;
        OBJECT_G00_EXIT_DECORATION_FLAMES            : Result := SObject_G00_ExitDecorationFlames;
        OBJECT_G00_ROCK_SQUISHING_TRAP               : Result := SObject_G00_RockSquishingTrap;
        OBJECT_G00_WAVING_BLUE_FLAG                  : Result := SObject_G00_WavingBlueFlag;
        OBJECT_G00_10_TON_SQUISHING_TRAP             : Result := SObject_G00_10_TonSquishingTrap;
      end;

    GRAPHICSET_FIRE:
      case aGraphicObject of
        OBJECT_G01_EXIT                              : Result := SObject_G01_Exit;
        OBJECT_G01_START                             : Result := SObject_G01_Start;
        OBJECT_G01_WAVING_GREEN_FLAG                 : Result := SObject_G01_WavingGreenFlag;
        OBJECT_G01_ONE_WAY_BLOCK_LEFT                : Result := SObject_G01_OneWayBlockLeft;
        OBJECT_G01_ONE_WAY_BLOCK_RIGHT               : Result := SObject_G01_OneWayBlockRight;
        OBJECT_G01_RED_LAVA                          : Result := SObject_G01_RedLava;
        OBJECT_G01_EXIT_FLAMES                       : Result := SObject_G01_ExitFlames;
        OBJECT_G01_FIRE_PIT_TRAP                     : Result := SObject_G01_FirePitTrap;
        OBJECT_G01_FIRE_SHOOTER_TRAP_FROM_LEFT       : Result := SObject_G01_FireShooterTrapFromLeft;
        OBJECT_G01_WAVING_BLUE_FLAG                  : Result := SObject_G01_WavingBlueFlag;
        OBJECT_G01_FIRE_SHOOTER_TRAP_FROM_RIGHT      : Result := SObject_G01_FireShooterTrapFromRight;
      end;
    GRAPHICSET_MARBLE:
      case aGraphicObject of
        OBJECT_G02_EXIT                              : Result := SObject_G02_Exit;
        OBJECT_G02_START                             : Result := SObject_G02_Start;
        OBJECT_G02_WAVING_GREEN_FLAG                 : Result := SObject_G02_WavingGreenFlag;
        OBJECT_G02_ONE_WAY_BLOCK_LEFT                : Result := SObject_G02_OneWayBLockLeft;
        OBJECT_G02_ONE_WAY_BLOCK_RIGHT               : Result := SObject_G02_OneWayBlockRight;
        OBJECT_G02_GREEN_LIQUID                      : Result := SObject_G02_GreenLiquid;
        OBJECT_G02_EXIT_FLAMES                       : Result := SObject_G02_ExitDecorationFlames;
        OBJECT_G02_WAVING_BLUE_FLAG                  : Result := SObject_G02_WavingBlueFlag;
        OBJECT_G02_PILLAR_SQUISHING_TRAP             : Result := SObject_G02_PillarSquishingTrap;
        OBJECT_G02_SPINNING_DEATH_TRAP               : Result := SObject_G02_SpinningDeathTrap;
      end;
    GRAPHICSET_PILLAR:
      case aGraphicObject of
        OBJECT_G03_EXIT                              : Result := SObject_G03_Exit;
        OBJECT_G03_START                             : Result := SObject_G03_Start;
        OBJECT_G03_WAVING_GREENFLAG                  : Result := SObject_G03_WavingGreenFlag;
        OBJECT_G03_ONEWAY_BLOCK_LEFT                 : Result := SObject_G03_OneWayBlockLeft;
        OBJECT_G03_ONEWAY_BLOCK_RIGHT                : Result := SObject_G03_OneWwayBlockRight;
        OBJECT_G03_WATER                             : Result := SObject_G03_Water;
        OBJECT_G03_EXIT_FLAMES                       : Result := SObject_G03_ExitFlames;
        OBJECT_G03_WAVING_BLUE_FLAG                  : Result := SObject_G03_WavingBlueFlag;
        OBJECT_G03_SPINNY_ROPE_TRAP                  : Result := SObject_G03_SpinnyRopeTrap;
        OBJECT_G03_SPIKES_FROM_LEFT_TRAP             : Result := SObject_G03_SpikesFromLeftTrap;
        OBJECT_G03_SPIKES_FROM_RIGHT_TRAP            : Result := SObject_G03_SpikesFromRightTrap;
      end;

    GRAPHICSET_CRYSTAL  :;
    GRAPHICSET_BRICK     :;
    GRAPHICSET_ROCK      :;
    GRAPHICSET_SNOW      :;
    GRAPHICSET_BUBBLE    :;
  end;

//  if Result = '' then
  //  raise Exception.Create('GraphicObjectToDescription error');

//  Result := Result + '(%s)';
  *)
end;


