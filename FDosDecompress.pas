{$include lem_directives.inc}

unit FDosDecompress;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, LemDosCmp, LemDosArc,
  UMisc, ExtCtrls;

type
  TFormDosDecompress = class(TForm)
    CheckListBoxArchive: TCheckListBox;
    BtnSelectFile: TButton;
    BtnClose: TButton;
    EditOutputDir: TEdit;
    EditFileName: TEdit;
    BtnSelectDir: TButton;
    RadioGroupGen: TRadioGroup;
    CheckBoxOverwriteExisting: TCheckBox;
    BtnExtractFiles: TButton;
    Label1: TLabel;
    Label2: TLabel;
    SaveDialog: TSaveDialog;
    procedure BtnSelectFileClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnExtractFilesClick(Sender: TObject);
    procedure EditFileNameExit(Sender: TObject);
    procedure EditFileNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CheckListBoxArchiveKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    fArchive: TDosDatArchive;
    fCurrentFileName: string;
    procedure OpenArc;
    procedure RefreshList;
    procedure ExtractFiles;
  public
  end;

procedure ExecuteFormDosDecompress;

implementation

{$R *.dfm}


procedure ExecuteFormDosDecompress;
var
  F: TFormDosDecompress;
begin
  F := TFormDosDecompress.Create(nil);
  try
    F.ShowModal;
  finally
    F.Free;
  end;
end;

{ TFormDosDecompress }

procedure TFormDosDecompress.BtnSelectFileClick(Sender: TObject);
var
  Dlg: TOpenDialog;
begin
  Dlg := TOpenDialog.Create(nil);
  try
    if Dlg.Execute then
    begin
      fCurrentFileName := Dlg.FileName;
      EditFileName.Text := fCurrentFileName;
      OpenArc;
    end;
  finally
    Dlg.Free;
  end;
end;

procedure TFormDosDecompress.FormCreate(Sender: TObject);
begin
  fArchive := TDosDatArchive.Create;
  EditOutputDir.Text := ApplicationPath;
end;

procedure TFormDosDecompress.OpenArc;
begin
  fArchive.LoadFromFile(fCurrentFileName);
  RefreshList;
end;

procedure TFormDosDecompress.RefreshList;
var
  i: Integer;
  A: TDosDatSection;
  S: string;
begin
  with CheckListBoxArchive do
  begin
    Items.BeginUpdate;
    try
      Items.Clear;
      for i := 0 to fArchive.SectionList.Count - 1 do
      begin
        A := fArchive.SectionList[i];
        S := 'Section ' + LeadZeroStr(i, 2) + ' (' + i2s(A.DecompressedSize) + ' bytes)';
        Items.AddObject(S, A);
        Checked[i] := True;
      end;
    finally
      Items.EndUpdate;
    end;
  end;
end;

procedure TFormDosDecompress.FormDestroy(Sender: TObject);
begin
  fArchive.Free;
end;

procedure TFormDosDecompress.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFormDosDecompress.ExtractFiles;
{-------------------------------------------------------------------------------
  Extract all checked file sections
-------------------------------------------------------------------------------}
var
  i: Integer;
  A: TDosDatSection;
  OutputPath: string;
  CheckOverwrite: Boolean;
  Prompt: Boolean;

    function GetFn: string;
    begin
      SaveDialog.InitialDir := EditOutputDir.Text;
      if SaveDialog.Execute then
      begin
        Result := SaveDialog.FileName;
      end
      else Result := '';
    end;

    procedure DoExtract(Ix: Integer; A: TDosDatSection);
    var
      F: TFileStream;
      Fn: string;
      Ext: string;
    begin
      ForceDirectories(OutputPath);

      if Prompt then
      begin
        Fn := GetFn;
//        if Fn = '' then Exit;

        if Fn = '' then
          if MessageDlg('Continue with next?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
            Abort
          else  
            Exit;

      end
      else begin
        Ext := ExtractFileExt(fCurrentFileName);
        Fn := OutputPath + ChangeFileExt(ExtractFileName(fCurrentFileName), '') + '(' + i2s(Ix) + ')' + Ext;
      end;

      if CheckOverwrite then
        if FileExists(Fn) then
          if MessageDlg('File ' + fn + 'does already exist. Overwrite it?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
            Exit;


//      ForceDirec
      F := TFileStream.Create(Fn, fmCreate);
      try
        fArchive.ExtractSection(Ix, F);
      finally
        F.Free;
      end;
    end;

begin
  OutputPath := IncludeTrailingBackslash(EditOutputDir.Text);
  Prompt := RadioGroupGen.ItemIndex = 1;
  CheckOverwrite := not CheckBoxOverwriteExisting.Checked;

  Screen.Cursor := crHourGlass;
  try

    with CheckListBoxArchive do
      for i := 0 to Items.Count - 1 do
        if Checked[i] then
      begin
        A := TDosDatSection(Items.Objects[i]);
        DoExtract(i, A);
      end;

  finally

    Screen.Cursor := crDefault;
  end;
//
end;

procedure TFormDosDecompress.BtnExtractFilesClick(Sender: TObject);
begin
  ExtractFiles;
  ShowMessage('Extraction ready');
end;

procedure TFormDosDecompress.EditFileNameExit(Sender: TObject);
begin
  if CompareText(EditFileName.Text, fCurrentFileName) <> 0 then
  begin
    fCurrentFileName := EditFileName.Text;
    if FileExists(fCurrentFileName) then
      OpenArc;
  end;
end;

procedure TFormDosDecompress.EditFileNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    if CompareText(EditFileName.Text, fCurrentFileName) <> 0 then
    begin
      fCurrentFileName := EditFileName.Text;
      if FileExists(fCurrentFileName) then
        OpenArc;
    end;
  end;
end;

procedure TFormDosDecompress.CheckListBoxArchiveKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  i: Integer;
begin
  if Shift = [ssCtrl] then
  begin
    case Key of
      Ord('A'):
        begin
          for i := 0 to CheckListBoxArchive.Items.Count - 1 do
            CheckListBoxArchive.Checked[i] := True;
        end;
      Ord('D'):
        begin
          for i := 0 to CheckListBoxArchive.Items.Count - 1 do
            CheckListBoxArchive.Checked[i] := False;
        end;
    end;
  end;
end;

end.

