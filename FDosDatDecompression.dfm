object DosDatDecompressionForm: TDosDatDecompressionForm
  Left = 471
  Top = 190
  Width = 504
  Height = 313
  Caption = 'DOS Dat File decompression'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  Position = poMainFormCenter
  object Lab1: TMasterLabel
    Left = 8
    Top = 48
    Width = 89
    Height = 17
    Caption = 'Output directory'
  end
  object MasterLabel1: TMasterLabel
    Left = 8
    Top = 16
    Width = 89
    Height = 17
    Caption = 'Input file'
  end
  object Button1: TButton
    Left = 384
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object EditOutputDir: TEditEx
    Left = 102
    Top = 48
    Width = 329
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    CaseSensitive = False
    IsPickList = False
  end
  object BitBtn1: TBitBtn
    Left = 440
    Top = 48
    Width = 19
    Height = 19
    Caption = '...'
    TabOrder = 2
  end
  object EditInputFile: TEditEx
    Left = 102
    Top = 16
    Width = 329
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    CaseSensitive = False
    IsPickList = False
  end
  object BitBtn2: TBitBtn
    Left = 440
    Top = 16
    Width = 19
    Height = 19
    Caption = '...'
    TabOrder = 4
  end
  object Memo1: TMemo
    Left = 8
    Top = 80
    Width = 233
    Height = 201
    Ctl3D = False
    Lines.Strings = (
      'Memo1')
    ParentCtl3D = False
    TabOrder = 5
  end
end
