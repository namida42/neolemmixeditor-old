unit LemInterfaces;

interface

uses
  Gr32,
  LemTypes;

type
  {-------------------------------------------------------------------------------
    interface to enable direct drawing by the game
  -------------------------------------------------------------------------------}
  IGameInfoView = interface
    procedure DrawSkillCount(aButton: TButtonSkill; aNumber: Integer);
    procedure DrawButtonSelector(aButton: TButtonSkill; SelectorOn: Boolean);
    procedure DrawMinimap(Map: TBitmap32);
    procedure SetInfoCursorLemming(const Lem: string; HitCount: Integer);
    procedure SetInfoLemmingsOut(Num: Integer);
    procedure SetInfoLemmingsIn(Num, Max: Integer);
    procedure SetInfoMinutes(Num: Integer);
    procedure SetInfoSeconds(Num: Integer);
  end;

  IGamePainter = interface
    //procedure RestoreBackGround(const aRect: TRect);

  end;


implementation

end.

